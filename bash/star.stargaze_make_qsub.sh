#!/bin/bash
# 2015 July 13 Lisa F.
# Script to create sin file and submit to cluster

temp_min=350
temp_max=450
temp_incr=50

star_home=$(echo "/home/lfelberg/star_diblock_copolymer/")
star_ana=$(echo "${star_home}/analysis")

for stars in 37  ; do
  star_dir=$(echo "STAR${stars}")

  for ((t=$temp_min;t<=$temp_max;t+=$temp_incr)) ; do
    starg_dir=$(echo "${star_home}/${star_dir}/${t}Kw/stargaze")
    cd ${starg_dir}
    
   #mkdir equil
   #mv S* s* d* g* p* equil/
   #mv equil/sin_builder.py ./
   #mv equil/generic.vin ./
    
    python sin_builder.py
    qsub -l h=compute-0-8 *qsub
  done
done

exit 0
