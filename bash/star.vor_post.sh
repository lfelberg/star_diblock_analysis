#!/bin/bash

# 2015 July 13 Lisa F.
# Script to grep information about water penetration and water
# cluster count and distribution from voronoi run

temp_min=350
temp_max=450
temp_incr=50

star_home=$(echo "/home/lfelberg/star_diblock_copolymer")
star_ana=$(echo "${star_home}/analysis")
agua=$(echo "lfelberg@agua.qb3.berkeley.edu:")

gelcore=0

intlines=43
corlines=68

#for stars in 8 12 25 ; do
for stars in 35 36  ; do
#for stars in 33 ; do
  star_dir=$(echo "star${stars}")
  if [ $gelcore -eq 1 ] ; then
    star_dir=$(echo "starg${stars}")
  elif [ $stars -gt 20 ] ; then
    star_dir=$(echo "STAR${stars}")
  fi

  for ((t=$temp_min;t<=$temp_max;t+=$temp_incr)) ; do
    starg_name=$(echo "stargaze_${star_dir}_${t}Kw")
    vordir=$(echo "${star_home}/${star_dir}/${t}Kw/voronoi/")
    cd $vordir
    echo $vordir

    # creating summary of data for all temps in 350 dir
    if [ $t -eq 350 ] ; then
      all_dir=$(echo "$vordir")
      nlines=${intlines}
      sed "s/NU/${stars}/g" ${star_ana}/zeros_wat.dat | head -n $nlines - \
                      > ${all_dir}vor_clust.dat
      cut -c 8- ${star_ana}/zeros_wat.dat | head -n $nlines - >\
                        ${all_dir}vor_clust2.dat

      nlines=${corlines}
      sed "s/NU/${stars}/g" ${star_ana}/zeros_depth.dat | head -n $nlines - \
                      > ${all_dir}vor_dep.dat
      cut -c 9- ${star_ana}/zeros_depth.dat | head -n $nlines - >\
                        ${all_dir}vor_dep2.dat
    fi
  
    # interior water size and count
    grep -A $intlines " count:" star${stars}_vorres.out | tail -n $intlines \
     | cut -c 1- - | paste -d " " ${all_dir}vor_clust.dat - > ${all_dir}vor_clust1.dat
    grep -A $intlines "size:" star${stars}_vorres.out | tail -n $intlines \
     | cut -c 1- - | paste -d " " ${all_dir}vor_clust2.dat - > ${all_dir}vor_clust21.dat
    mv ${all_dir}vor_clust1.dat ${all_dir}vor_clust.dat
    mv ${all_dir}vor_clust21.dat ${all_dir}vor_clust2.dat
  
    # interior distance to core and bulk
    grep -A $corlines "bulk :" star${stars}_vorres.out | tail -n $corlines \
     | cut -c 1- - | paste -d " " ${all_dir}vor_dep.dat - > ${all_dir}vor_dep1.dat
    grep -A $corlines "core :" star${stars}_center.out | tail -n $corlines \
     | cut -c 1- - | paste -d " " ${all_dir}vor_dep2.dat - > ${all_dir}vor_dep21.dat
    mv ${all_dir}vor_dep1.dat ${all_dir}vor_dep.dat
    mv ${all_dir}vor_dep21.dat ${all_dir}vor_dep2.dat
  
  done
  cut -c 1- ${all_dir}vor_clust2.dat | paste -d " " ${all_dir}vor_clust.dat - \
         > ${all_dir}vor_star${stars}_350Kw.wat_sum

  cut -c 1- ${all_dir}vor_dep2.dat | paste -d " " ${all_dir}vor_dep.dat - \
         > ${all_dir}vor_star${stars}_350Kw.depth
  rm ${all_dir}vor_clust.dat  ${all_dir}vor_clust2.dat \
     ${all_dir}vor_dep.dat  ${all_dir}vor_dep2.dat
done

exit 0
