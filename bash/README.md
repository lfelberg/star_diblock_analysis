# Bash scripts

This is a collection of bash scripts that cull and run shorter fortran
and python programs, attempting to make analysis of multiple systems all
at once for easier runs.

*usage:* *.sh

## star.analysis.sh

A program to cull the data from a stargaze run and a voronoi run for a 
given list of stars at 350, 400 and 450K. This is what I usually run when
a trajectory is completed. These two programs (stargaze and voronoi) comprise
the bulk of the analysis.

The shell script will do the following if you want to look at stargaze, voronoi and voronoi_int outputs:

1. Run rgyr.f, orientation.f, persistence.py for all 
2. Take outputs of the rgyr program and put it into one file for rgyr, and one
for anisotropy.
3. Take the persistence results for the 3 temperatures and make them into one
file in the 350K stargaze dir.
4. Create qsub files for radiusofgyr and pegdihedral
5. Take torout data and concatenate them into one *.dihed file
6. In voronoi dir, print general results, using voronoi_results.py
7. Make a sain file and run the program, computing water neighbors
8. Concatenate information at all temperatures about the distribution of interior
   water clusters.
9. Concatenate information about interior water penetration profiles.

## star.restime.sh

A program to run the python script water_restime.py for a given set of stars
at 350, 400 and 450K. Comcatenates the results for each star at 3 temps into 
the 350Kw/voronoi/interior_water directory.

## star.stargaze.sh

Program to generate stargaze input file: sin and qsub. Also submits to the 
queue.

## star.vol.sh

Script to find average volume of each star simulation. Concatenates
vol files together and then runs python script on them. 

## star.vor_make.sh

Script to take stargaze generated vin file and break it up for use with voronoi 
run

## star.vor_int_make.sh

Script to take voronoi outputs and create voronoi interior and voronoi solute
runs from OG voronoi

## star.vor_interfacial_area.sh

Grep out information from voronoi output for different groups' interfacial areas

## star.vor_post.sh

Script to grep information about water penetration and water 
cluster count and distribution from voronoi run

## star.water_make.sh

Program to make interior water analysis inputs: wain files

## voronoi_interior_check.sh

Script to check to see if either voronoi or vor solute and vor interior
have completed successfully.
