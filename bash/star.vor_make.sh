#!/bin/bash
# 2015 July 13 Lisa F.
# Script to take stargaze generated
# vin file and break it up for use with voronoi
# run

temp_min=350
temp_max=450
temp_incr=50 

star_home=$(echo "/home/lfelberg/star_diblock_copolymer/")
star_ana=$(echo "${star_home}/analysis")
#star_home=$(echo "/home/lfelberg/star_diblock_copolymer/paper02")

for stars in 37  ; do
  star_dir=$(echo "star${stars}")
  if [ $stars -gt 31 ] ; then
    star_dir=$(echo "STAR${stars}")
  fi

  for ((t=$temp_min;t<=$temp_max;t+=$temp_incr)) ; do
    # Copying requisite files, grabbing basic setup scripts
    starg_dir=$(echo "${star_home}/${star_dir}/${t}Kw/stargaze")
    cd ${starg_dir}

   #mkdir ../voronoi/
   #mkdir  ../voronoi/old
   #mv ../voronoi/* ../voronoi/old/

    read max words chars filename <<< $(wc sample.vin)
    num=0;vin_cmd_nlines=3;vin_ncat=100
    min=$((vin_cmd_nlines*vin_ncat))
    echo  "nlines  $max" 
    for ((f=$min;f<=$max;f+=$(($vin_ncat*$vin_cmd_nlines)) )) ; do
      nlines=$(($vin_ncat*$vin_cmd_nlines))
      echo "#!/bin/bash" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "#$ -S /bin/bash" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "#$ -cwd" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "#$ -o ${star_dir}_${t}.out" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "#$ -N ${star_dir}_${t}.out" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "#$ -q all*" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "#$ -j y" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "/home/lfelberg/star_diblock_copolymer/analysis/bin/voronoi ${star_dir}_${t}Kw.${num}.vin > ${star_dir}_${t}Kw.${num}.vout " >> ${star_dir}_${t}Kw.${num}.qsub

      echo " head $f and tail $nlines max $max"
      head -n $f sample.vin | tail -n $nlines >> ${star_dir}_${t}Kw.${num}.vin
      num=$((num+1))
    done

    if [ $f -gt $max ] ; then
      # For the last extra bits, make another file
      nlines=$(($max-$f+$vin_ncat*$vin_cmd_nlines))
      echo "#!/bin/bash" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "#$ -S /bin/bash" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "#$ -cwd" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "#$ -o ${star_dir}_${t}.out" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "#$ -N ${star_dir}_${t}.out" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "#$ -q all*" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "#$ -j y" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "" >> ${star_dir}_${t}Kw.${num}.qsub
      echo "/home/lfelberg/star_diblock_copolymer/analysis/bin/voronoi ${star_dir}_${t}Kw.${num}.vin > ${star_dir}_${t}Kw.${num}.vout " >> ${star_dir}_${t}Kw.${num}.qsub
 
      echo " head $max and tail $nlines"
      head -n $f sample.vin | tail -n $nlines >> ${star_dir}_${t}Kw.${num}.vin
    fi

    mv ${star_dir}_${t}Kw.[0-9]*.vin ../voronoi/
    mv ${star_dir}_${t}Kw.[0-9]*.qsub ../voronoi/
  done
done

exit 0
