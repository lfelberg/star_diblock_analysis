#!/bin/bash

# 2015 July 13 Lisa F.
# Script to calculate a histogram of the residence time 
# of interior waters. concatenates for each star at 3 temperatures

temp_min=350
temp_max=450
temp_incr=50

star_home=$(echo "/home/lfelberg/star_diblock_copolymer")
star_ana=$(echo "${star_home}/analysis")
res_exec=$(echo "${star_ana}/python/water_restime.py")

gelcore=1

#for stars in 8 12 25 ; do
for stars in 15 ; do
  star_dir=$(echo "star${stars}")
  if [ $gelcore -eq 1 ] ; then
    star_dir=$(echo "starg${stars}")
  elif [ $stars -gt 20 ] ; then
    star_dir=$(echo "STAR${stars}")
  fi

  for ((t=$temp_min;t<=$temp_max;t+=$temp_incr)) ; do
    vor_dir=$(echo "${star_home}/${star_dir}/${t}Kw/voronoi/interior_water/")
    cd $vor_dir

    python ${res_exec} wattime.out > wattime.hist
    
    if [ $t -eq 350 ] ; then
      nlines=$( wc -l < wattime.hist )
      hist_dir=$(echo "$vor_dir")
      echo "$nlines"
      sed "s/NU/${stars}/g" ${star_ana}/zeros_watrest.dat | head -n $nlines - > ${vor_dir}wattime.all
    fi
  
    cut -c 7- wattime.hist | paste -d " " ${hist_dir}wattime.all - > ${hist_dir}wattime1.all
    mv ${hist_dir}wattime1.all ${hist_dir}wattime.all
  
  done
done

exit 0
