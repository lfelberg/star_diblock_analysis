#!/bin/bash

# 2017 Jan 27 Lisa F.
# Script to find average volume of each star simulation

temp_min=350
temp_max=450
temp_incr=50

star_home=$(echo "/home/lfelberg/star_diblock_copolymer")
star_ana=$(echo "${star_home}/analysis")
agua=$(echo "lfelberg@agua.qb3.berkeley.edu:")

gelcore=0

for stars in 30 31 ; do # 12 25 ; do
#for stars in 15; do # 12 25 ; do
  star_dir=$(echo "star${stars}")

  for ((t=$temp_min;t<=$temp_max;t+=$temp_incr)) ; do
    starg_dir=$(echo "${star_home}/paper02/${star_dir}/${t}Kw/data/")

    echo ${starg_dir}
    cd ${starg_dir}
    for k in *.vol ; do
        cat ${k} >> vol.dat
    done 
    python ${star_home}/star.vol.py
  done
done

exit 0
