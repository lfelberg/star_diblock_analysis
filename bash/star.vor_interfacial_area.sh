#!/bin/bash

# 2015 July 13 Lisa F.
# Script to look at voronoi outputs and grep out the interfacial area
# between two groups

temp_min=350
temp_max=450
temp_incr=50

star_home=$(echo "/home/lfelberg/star_diblock_copolymer")
star_ana=$(echo "${star_home}/analysis")
agua=$(echo "lfelberg@agua.qb3.berkeley.edu:")

#for stars in 32 33 34 35; do
#for stars in 35 36 ; do
for stars in 25 ; do #12 15 25 ; do
  star_dir=$(echo "STAR${stars}")

  for ((t=$temp_min;t<=$temp_max;t+=$temp_incr)) ; do
    starg_name=$(echo "stargaze_${star_dir}_${t}Kw")

    # Copying requisite files, grabbing basic setup scripts
    starg_dir=$(echo "${star_home}/${star_dir}/${t}Kw/voronoi/")
    cd ${starg_dir}
    
    for i in 1 2 3 4 ; do
      min=`expr $i + 1`
      for j in $(seq ${min} 4) ; do
         grep -h "between site types  ${i}- ${j}" *.vout | grep -v "(sol" > group_${i}_${j}.vol
      done
    done
  done
done

exit 0
