#!/bin/bash

# 2015 July 13 Lisa F.
# Script to analyze stargaze output

temp_min=350
temp_max=450
temp_incr=50

star_home=$(echo "/home/lfelberg/star_diblock_copolymer")
star_ana=$(echo "${star_home}/analysis")
agua=$(echo "lfelberg@agua.qb3.berkeley.edu:")

orientation_exec=$(echo "${star_ana}/bin/orientation")
per_exec=$(echo "${star_ana}/python/persistence.py")
vorr_gel=$(echo "${star_home}/vor.gel")
rgyr_all=$(echo "${star_home}/rgyr.all")
anis_all=$(echo "${star_home}/anis.all")

rm -rf $rgyr_all $anis_all $vorr_gel
touch $rgyr_all $anis_all $vorr_gel

gelcore=0
stargaze_do=1
vor_do=0
vin_do=0

#for stars in 8 33 34 35 36 ; do # 37; do
for stars in 8 35 36 ; do
#for stars in 8 12 25 ; do
#for stars in 8  ; do
  star_dir=$(echo "star${stars}")
  if [ $gelcore -eq 1 ] ; then
    star_dir=$(echo "starg${stars}")
  elif [ $stars -gt 31 ] ; then
    star_dir=$(echo "STAR${stars}")
  fi

  rgyr_exec=$(echo "${star_ana}/bin/rgyr")
  if [ $stars -eq 30 ] ;   then
    rgyr_exec=$(echo "${star_ana}/bin/rgyr_star30")
  elif [ $gelcore -eq 1 ] ; then
    rgyr_exec=$(echo "${star_ana}/bin/rgyr_gel")
  elif [ $stars -eq 12 ] ; then
    rgyr_exec=$(echo "${star_ana}/bin/rgyr_den")
  fi

  for ((t=$temp_min;t<=$temp_max;t+=$temp_incr)) ; do
    starg_name=$(echo "stargaze_${star_dir}_${t}Kw")

    # Copying requisite files, grabbing basic setup scripts
    starg_dir=$(echo "${star_home}/${star_dir}/${t}Kw/stargaze/")
    cd ${starg_dir}

    if [ $stargaze_do -eq 1 ] ; then
        # Generating RGYR files for star_all
        echo "$rgyr_exec"
        $rgyr_exec ${starg_name}.sprp > ${starg_name}.rgyr
        tail ${starg_name}.rgyr | grep "Rgyr(ALL ) " | cut -d: -f2- | sed "s/^/${stars}   ${t}/" >> $rgyr_all
        tail ${starg_name}.rgyr | grep "Anis(ALL ) " | cut -d: -f2- | sed "s/^/${stars}   ${t}/" >> $anis_all
 
        #orientation function
        $orientation_exec ${starg_name}.sori > ${starg_name}.sori.out
        rm *.sori.out

        # making qsub for torsion and Rg calcs
        cp *sin stargaze_${star_dir}_${t}Kw.torin
        cp *sin stargaze_${star_dir}_${t}Kw.rgin
        cp ${star_dir}_${t}Kw.stargaze.qsub ${star_dir}_${t}Kw.torsion.qsub
        sed -i "s/\/stargaze/\/dihedralcheck/g" *torsion.qsub 
        arm_f=`ls ../../setup/${star_dir}*arm`
        fline=$(echo "../../setup/${star_dir}wat.lmp   ${arm_f}")
        sed -i "1c\${fline}" *.torin *.rgin
        echo "Submit torsion file"
 
        # Search and replace relevant items for correlations
        sed "s/star_number/${stars}/g" ${per_exec}  > persistence.py
        sed -i "s/sori_file/${starg_name}.sori/g"  persistence.py
        sed -i "s/star_temperature/${t}/g"  persistence.py
        python persistence.py > ${starg_name}.perst
        
        # Make a single file for persistence data in 350 directory
        if [ $t -eq 350 ] ; then
          nlines=$( wc -l < ${starg_name}.perst )
          perst_dir=$(echo "$starg_dir")
          echo "$nlines"
          sed "s/NU/${stars}/g" ${star_ana}/zeros.dat | head -n $nlines - > ${starg_dir}stargaze_perst
          cut -c 8- ${star_ana}/zeros.dat | head -n $nlines - > ${starg_dir}stargaze_perst2
        fi
      
        cut -c 1-17 ${starg_name}.perst | paste -d " " ${perst_dir}stargaze_perst - > ${perst_dir}stargaze_perst1
        cut -c 19- ${starg_name}.perst | paste -d " " ${perst_dir}stargaze_perst2 - > ${perst_dir}stargaze_perst21
        mv ${perst_dir}stargaze_perst1 ${perst_dir}stargaze_perst
        mv ${perst_dir}stargaze_perst21 ${perst_dir}stargaze_perst2

        # Make a single file for torsion data in 350 directory
        if [ $t -eq 350 ] ; then
          nlines=10
          dihed_dir=$(echo "$starg_dir")
          echo "$nlines"
          tail -n $nlines ${starg_name}.torout | cut -c 1-29 | sed "s/Torsion distribution/${stars} /g" > ${starg_dir}dihed
        fi
      
        tail -n $nlines ${starg_name}.torout | cut -c 29- | paste -d " " ${dihed_dir}dihed - > ${dihed_dir}dihed1
        mv ${dihed_dir}dihed1 ${dihed_dir}dihed
    fi
  
    if [ $vor_do -eq 1 ] ; then
        vor_dir=$(echo "${star_home}/${star_dir}/${t}Kw/voronoi/")
        cd ${vor_dir}
        # general voro results
        python ${star_ana}/python/voronoi_results.py ${stars} 10
        grep -A1 "Core/Hphob"  star${stars}_vorres.out | grep -v "Core/Hphob" >> $vorr_gel
 
        ls -1v *.[1-9]*vout > ${star_dir}_${t}Kw.sain
        ${star_ana}/bin/surface_analysis_PHOB_PHIL_WAT ${star_dir}_${t}Kw.sain > ${star_dir}_${t}Kw.saout
        python ${star_ana}/python/surface_area_vol_stdev.py ${star_dir}_${t}Kw.saout

        # concatenating the results for water clusters
        if [ $t -eq 350 ] ; then
          nlines=$( wc -l < star${stars}_watclu.dat )
          nl1=$(( $nlines + 1))
          wat_dir=$(echo "$vor_dir")
          sed "s/NU/${stars}/g" ${star_ana}/zeros.dat | cut -c 1-14 - | head -n $nl1 - | tail -n $nlines - > ${vor_dir}vr_clu
          cut -c 15- ${star_ana}/zeros.dat | head -n $nl1 - | tail -n $nlines -  > ${vor_dir}vr_clu2
        fi
      
        cut -c 1-7 star${stars}_watclu.dat | paste -d " " ${wat_dir}vr_clu - > ${wat_dir}vr_clu1
        cut -c 8-  star${stars}_watclu.dat | paste -d " " ${wat_dir}vr_clu2 - > ${wat_dir}vr_clu3
        mv ${wat_dir}vr_clu1 ${wat_dir}vr_clu
        mv ${wat_dir}vr_clu3 ${wat_dir}vr_clu2
    fi
  
    if [ $vin_do -eq 1 ] ; then
        vor_dir=$(echo "${star_home}/${star_dir}/${t}Kw/voronoi_int/")
        cd ${vor_dir}
        nlines=50
        tail -n 100 *.waout | head -n $nlines > ${star_dir}_${t}Kw.waout.dat
 
        # concatenating the results for water distance histograms
        if [ $t -eq 350 ] ; then
          wat_dir=$(echo "$vor_dir")
          sed "s/NU/${stars}/g" ${star_ana}/zeros.dat | cut -c 1-3 - | head -n $nlines - > ${vor_dir}wat
          head -n $nlines ${star_dir}_${t}Kw.waout.dat | cut -c 2-10 - | paste -d " " ${wat_dir}wat - > ${wat_dir}wat1
          mv ${wat_dir}wat1 ${wat_dir}wat
          head -n $nlines ${star_ana}/zeros.dat | cut -c 8- - | paste -d " " ${wat_dir}wat - > ${wat_dir}wat1
          mv ${wat_dir}wat1 ${wat_dir}wat
          cut -c 8- ${star_ana}/zeros.dat | head -n $nlines -  > ${vor_dir}wat2
        fi
      
        cut -c 60-85 ${star_dir}_${t}Kw.waout.dat | paste -d " " ${wat_dir}wat - > ${wat_dir}wat1
        cut -c 135-  ${star_dir}_${t}Kw.waout.dat | paste -d " " ${wat_dir}wat2 - > ${wat_dir}wat3
        mv ${wat_dir}wat1 ${wat_dir}wat
        mv ${wat_dir}wat3 ${wat_dir}wat2
    fi
  
  done
  if [ $stargaze_do -eq 1 ] ; then
    cut -c 1- ${perst_dir}stargaze_perst2 | paste -d " " ${perst_dir}stargaze_perst - > ${perst_dir}stargaze_star${stars}_350Kw.perst_sum
    rm ${perst_dir}stargaze_perst  ${perst_dir}stargaze_perst2

    sed -i "s/T /t/g" ${dihed_dir}dihed 
    sed -i "s/G' /g'/g" ${dihed_dir}dihed 
    sed -i "s/G'/g'/g" ${dihed_dir}dihed 
    sed -i "s/G /g/g" ${dihed_dir}dihed 
    mv ${dihed_dir}dihed ${dihed_dir}stargaze_star${stars}_350Kw.dihed_sum
  fi

  if [ $vor_do -eq 1 ] ; then
    cut -c 1- ${wat_dir}vr_clu2 | paste -d "" ${wat_dir}vr_clu - > ${wat_dir}star${stars}_350Kw.watclu
    rm ${wat_dir}vr_clu ${wat_dir}vr_clu2
  fi

  if [ $vin_do -eq 1 ] ; then
    cut -c 1- ${wat_dir}wat2 | paste -d "" ${wat_dir}wat - > ${wat_dir}star${stars}_350Kw.watdis
    rm ${wat_dir}wat ${wat_dir}wat2
  fi
done

exit 0

