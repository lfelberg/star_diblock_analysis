#!/bin/bash

# 2015 July 13 Lisa F.
# Script to check to see if either voronoi or vor solute and vor interior
# have completed successfully.

temp_min=350
temp_max=450
temp_incr=50

star_home=$(echo "/home/lfelberg/star_diblock_copolymer")
#star_home=$(echo "/home/lfelberg/star_diblock_copolymer/paper02")

gelcore=0

#for stars in 8 33 34 35 36; do
#for stars in    36 ; do
#for stars in          30 31 ; do
 for stars in          37    ; do
  star_dir=$(echo "star${stars}")
  if [ $gelcore -eq 1 ] ; then
    star_dir=$(echo "starg${stars}")
  elif [ $stars -gt 31 ] ; then
    star_dir=$(echo "STAR${stars}")
  fi  

  for ((t=$temp_min;t<=$temp_max;t+=$temp_incr)) ; do
    echo "This is ${star_dir} and temp: $t"
   #vint_dir=$(echo "${star_home}/${star_dir}/${t}Kw/voronoi_side/")
		vint_dir=$(echo "${star_home}/${star_dir}/${t}Kw/voronoi_int/")
   #vint_dir=$(echo "${star_home}/${star_dir}/${t}Kw/voronoi/")
    cd  ${vint_dir}

    for i in *vin ;  do
      vin_ct=`grep -c classificationfile $i | cut -d: -f2- `
      vout=$(echo $i | rev | cut -c 4- | rev)
      vout_ct=`grep -c DISTRIBUTION ${vout}vout | cut -d: -f2- `
     #echo " $vout in : $vin_ct and out : $vout_ct "
      if [ ${vout_ct} -lt ${vin_ct} ]  ; then
        echo "$vout has different ct! vin: $vin_ct vout: $vout_ct"
        ls -lat ${vout}vout
      fi
    done
  done
done
