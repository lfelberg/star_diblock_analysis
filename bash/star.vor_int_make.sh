#!/bin/bash

# 2016 September 13 Lisa F.
# Script to make voronoi interior 
# and solute runs from original voronoi

temp_min=350
temp_max=450
temp_incr=50

star_home=$(echo "/home/lfelberg/star_diblock_copolymer")
star_ana=$(echo "${star_home}/analysis")
gelcore=0

for stars in     37   ; do
  star_dir=$(echo "star${stars}")
  if [ $gelcore -eq 1 ] ; then
    star_dir=$(echo "starg${stars}")
  elif [ $stars -gt 31 ] ; then
    star_dir=$(echo "STAR${stars}")
  fi

  for ((t=$temp_min;t<=$temp_max;t+=$temp_incr)) ; do
    # Copying requisite files, grabbing basic setup scripts
    starg_dir=$(echo "${star_home}/${star_dir}/${t}Kw")
    cd ${starg_dir}
   #rm -rf voronoi_int
    cp  voronoi/* voronoi_int/
    cd voronoi_int

    for i in *[0-9].vin; do
      v2=$(echo $i | rev | cut -c 5- | rev)
      mv $i ${v2}.solute.vin
    done 

    sed -i "s/distancecutoff/solute distancecutoff/g" ${star_dir}*solute.vin
    sed -i "s/voronoi /voronoi_new /g" ${star_dir}*qsub
    sed -i "s/\.vin/\.solute.vin/g" ${star_dir}*qsub
    sed -i "s/\.vout/\.solute.vout/g" ${star_dir}*qsub

    # classifying interior neighbors
    python ${star_ana}/python/interior_neigh.py
    echo "cd ${star_home}/${star_dir}/${t}Kw/voronoi_int/ "

   #rm -rf *neigh *.out *.vout 
  done
done

exit 0
