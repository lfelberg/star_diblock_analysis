#!/bin/bash
# 2015 July 13 Lisa F.
# Script to take sain file and use it to make interior water script input

temp_min=350
temp_max=450
temp_incr=50 

star_home=$(echo "/home/lfelberg/star_diblock_copolymer/")
star_ana=$(echo "${star_home}/analysis")
 star_home=$(echo "/home/lfelberg/star_diblock_copolymer/paper02")

#for stars in 8     ; do
#for stars in 33 34 35 36     ; do
 for stars in 24 26 27 30 31     ; do
  star_dir=$(echo "star${stars}")
  if [ $stars -gt 31 ] ; then
    star_dir=$(echo "STAR${stars}")
  fi

  for ((t=$temp_min;t<=$temp_max;t+=$temp_incr)) ; do
    # Copying requisite files, grabbing basic setup scripts
    int_dir=$(echo "${star_home}/${star_dir}/${t}Kw/voronoi_int")
    cd ${int_dir}

    rm -rf ${star_dir}_${t}Kw.int.qsub
    echo "#$ -S /bin/bash" >> ${star_dir}_${t}Kw.int.qsub
    echo "#$ -cwd" >> ${star_dir}_${t}Kw.int.qsub
    echo "#$ -o in${star_dir}_${t}.out" >> ${star_dir}_${t}Kw.int.qsub
    echo "#$ -N in${star_dir}_${t}.out" >> ${star_dir}_${t}Kw.int.qsub
    echo "#$ -q all*" >> ${star_dir}_${t}Kw.int.qsub
    echo "#$ -j y" >> ${star_dir}_${t}Kw.int.qsub
    echo "" >> ${star_dir}_${t}Kw.int.qsub
    echo "/home/lfelberg/star_diblock_copolymer/analysis/bin/interior_water_analysis_ADM ${star_dir}_${t}Kw.wain > ${star_dir}_${t}Kw.waout " >> ${star_dir}_${t}Kw.int.qsub
 
    head -n2 *.1.*ute.vin | tail -n1 | rev |  cut -d" " -f 1 | rev | cat - ../voronoi/${star_dir}_${t}Kw.sain > ${star_dir}_${t}Kw.wain
    
   #cat ${star_dir}_${t}Kw.int.qsub ${star_dir}_${t}Kw.wain
    qsub ${star_dir}_${t}Kw.int.qsub
  done
done

exit 0
