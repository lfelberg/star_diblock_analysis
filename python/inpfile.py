import numpy as np

from pdbfile import PDBFile
from lmpfile import LMPFile

class InpFile:
    '''A class for input files'''
    inpfname = ''
        
    def __init__(self, fname):
        self.inpfname = fname
        self.get_input(fname)

        self.pdb = PDBFile(self.setup_pref+".pdb")
        self.lmp = LMPFile(self.setup_pref+".lmp")

    def get_input(self, filename):
        '''Method to open input file and save info'''
        self.xyzs = []
        f=open(filename, "r").readlines()
        self.inpfname = filename
        self.setup_pref = f[0].split()[0]
        for line in f[2:]:
            tmp = line.split()
            self.xyzs.append(tmp[0])
 
    def get_type_at_i(self, i):
        '''Get indices of type i'''
        return self.types == i
    
