import numpy as np
import sys

"""
 MAIN
"""

## The format of the file to read is as follows:
# i  99116 time in  10060 time out      10120.0000 duration         60.0000
# i 107997 time in  10120 time out      10140.0000 duration         20.0000
# i 107998 time in  10120 time out      10140.0000 duration         20.0000
# i 107999 time in  10120 time out      10140.0000 duration         20.0000

ct = 0
rest = []

with open(sys.argv[1]) as f:
  for line in f:
    if (ct % 3 == 0):
      rest.append(float(line.split()[9]))

# Printing out data:
rest = np.array(rest)
n, bins = np.histogram(rest,
                       bins=np.arange(20.0,400.0, 40.0), 
                       normed=True)
for a in range(len(n)):
  print("{0:>6.1f}  {1:>.9f}".format( bins[a], n[a]))


