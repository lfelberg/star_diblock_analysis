"""
   sin_builder.py 
   builds .sin files for stargaze analysis
   
  Inputs: location of directory containing .xyz files 
                   relative to location of this .py file
      location of the .lmp file relative to this .py file
      .arm file 	
      number to skip (2 means take every other 
             .xyz file listed)
      desired name of output file
  Outputs: a .sin file with the proper structure
  """

import subprocess
import sys

#INPUTS:
in_directory = '../data'
starno='star26'
temp='350Kw'
out_file = 'stargaze_%s_%s.sin' % (starno, temp)
lmp_loc = '../../setup/%swat.lmp' % (starno)
arm_file = '../../setup/staradam16pdvlpoxb24.arm'
num_skip = '40  1 2 3 0'

qsub = open('%s_%s.stargaze.qsub' % 
                    (starno, temp), 'w')
qsub.write('#!/bin/bash\n#$ -S /bin/bash\n#$')
qsub.write(' -cwd\n#$ -o stargaze.out\n')
qsub.write('#$ -j y\n\n/home/lfelberg/star_diblock')
qsub.write('_copolymer/analysis/bin/stargaze ')
qsub.write('%s\n' % out_file[0:-4])
qsub.close()

# Finding the # of snapshots in each xyz file
command_string = 'grep -c Atom ' + in_directory 
command_string+= '/*.xyz > temp_file.txt'
subprocess.check_output(command_string, 
                                              shell=True)

temp_file = open('temp_file.txt')
raw_xyz_files = temp_file.readlines()
subprocess.call(['rm','temp_file.txt'])

# Finding the # of volumes in the corresponding
# *.vol file
command_string = 'wc -l ' + in_directory 
command_string+= '/*.vol > temp_file.txt'
subprocess.check_output(command_string, 
                                              shell=True)

temp_file = open('temp_file.txt')
raw_vol_files = temp_file.readlines()
subprocess.call(['rm','temp_file.txt'])

xyz_files = []
for i in range(0, len(raw_xyz_files)):
    line = raw_xyz_files[i]
    colon_index = line.find(':')
    #subtracts one off the grepped value
    snaps = str(int(line[(colon_index+1):])-1) 
    new_line = line[0:colon_index] +' ' + snaps
    xyz_files.append(new_line)
    
    lnsplit = line.split('.')
    for j in range(len(raw_vol_files)-1):
        volsplit = raw_vol_files[j].split('.')
        if int(lnsplit[3]) == int(volsplit[3]) :
            if int(snaps)+1 > int(volsplit[0]) :
                print "This needs moro vol lines!"
                print new_line
                print raw_vol_files[j]


#sorting the file:
  #the function below takes line from xyz_files 
  #and finds the xyz file number (just before .xyz)
def xyz_sort_key(line):
    end_index = line.rfind('.')
    start_index = 1 + line[:end_index].rfind('.')
    xyz_number = int(line[start_index : end_index])	
    return xyz_number

#this is a sorted version of xyz_files
xyz_files.sort(key=xyz_sort_key)  

#building the output:
sin_file = open(out_file, 'w')

sin_file.write(lmp_loc + '   ' + arm_file + '\n')
sin_file.write('  ' + num_skip + '       ' + '!# to skip; ' 
          + '2 means to take every other one\n')
for i in range(0, len(xyz_files)):
	sin_file.write(xyz_files[i] + '\n')

sin_file.close()
