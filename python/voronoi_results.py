import numpy as np
import sys
import re
import subprocess

if sys.version_info[0] < 3 and sys.version_info[1] < 7:
    files = subprocess.Popen(["ls"], 
                     stdout=subprocess.PIPE).communicate()[0]
    temp, voutfiles, clusfiles = files.split(), [], []
    for i in range(len(temp)):
        if re.search('[0-9].vout', temp[i]): voutfiles.append(temp[i])
        elif re.search('[0-9].cluster', temp[i]): clusfiles.append(temp[i])
else:
    vouttemp = subprocess.check_output('ls *[0-9].vout', shell=True)
    clustemp = subprocess.check_output('ls *[0-9].cluster', shell=True)
    voutfiles = vouttemp.split()
    clusfiles = clustemp.split()

#print(voutfiles)
star_no = int(sys.argv[1])

intwat, watedge, clusize,watcore = [], [], [], []
flag, natoms, classify, xyz = 0,0,0, ' '
ngroups, nrecords, pairct, groupNam = 0, 0, 0, []
npairs = [[] for x in range(3)]


volStr= 'Cell dimensions determined'
volStr+=' from coordinates'
interfStr = 'Interfacial area between site types  '
solvStr = '(solvent/solute; halved)'

"""
 MAIN
"""
for names in voutfiles:
    with open(names) as f:
        for line in f:
            # Store info: timestep, xyz filename
            if ((natoms == 0) and 
                    line.startswith('Input file line 1:')):
                temp = line.split()
                natoms = int(temp[6]) + int(temp[7])  

            # finding the number of atom groups
            if ((ngroups == 0) and
                    line.startswith('For scheme ')):
                ngroups = int(line.split()[5])
                npairs = [[] for x in range(int((ngroups*
                                        (ngroups-1))/2))]
            # storing the name of each atom group    
            if ((len(groupNam) < ngroups) and
                    line.startswith('Class ')):
                groupNam.append(line.split()[-1])
              
            # finding the xyz filename and config #   
            if (line.startswith('File name *')):
                temp = line.split()
                nrecords+=1
                xyz, config = temp[2][1:-1], int(temp[4])   
            
            if (line.startswith(volStr)): 
                temp = line.split()
                xyzmax = float(temp[10])   
            
            for i in range(0, ngroups-1):
                for j in range(i+1, ngroups):
                    infPair = interfStr+str(i+1)+'- ' + str(j+1)
                    revPair = interfStr+str(j+1)+'- ' + str(i+1)
                    if ((line.startswith(revPair) or 
                         line.startswith(infPair)) and 
                        (line.find(solvStr)==-1)):
                            npairs[pairct].append(float(line.split()[8]))
                    pairct += 1
            pairct = 0
      
            if line.startswith('Number of distinguishab'):
                temp = line.split()
                intwat.append(float(temp[6])-1.0)

            if line.startswith('Cluster index'):
                temp = line.split()
                if float(temp[6]) < 100:
                    clusize.append(float(temp[6])/3.0)      
                  
            if line.startswith('Closest distance '):
                temp, flag = line.split(), 1
                if len(temp) == 12:
                    watedge.append(float(temp[8]))
                else:
                    watedge.append(float(temp[-1]))


# Printing out data:

#keywords of each region [[CORE], [HPHOB], 
#           [LINK], [HPHIL], [WAT]]
#keygroups = [ 'Core', 'Hphob', 'Linker','Hphil', 'Hphil', 'Water']
keygroups = [ 'Core', 'Hphob', 'Linker','Hphil', 'Water']
keywords = [ ['CORE','COR', 'EKE'] , 
                    ['Hphob', 'PHOB', 'HPHOBE', 'HPHOB', 'PVL', 'PDVL'] ,
                    ['MEOX', 'MOX', 'MOXE', 'LINK', 'PHILS'], 
                    ['Hphil','PHIL', 'HPHIL', 'DME'],  
                    ['Wat','WAT','WATER'] ]
 
 # Finding the voro class name in our list above
groupClass = np.zeros(ngroups) 
for i in range(len(groupNam)):
    for j in range(len(keywords)):
        if (groupNam[i] in keywords[j]):
            groupClass[i] = j

ngrpPair = len(keygroups)*(len(keygroups)-1)/2
grpMap = np.empty((len(keygroups),len(keygroups)))
grpMap.fill(-1)

# Mapping the pairs of keywords to an array
# 0: core+phob, 1: core+link, 2: core+phil, 3: cor+wat
# 4: phob+link, 5: phob+phil, 6: phob+wat
# 7: link+phil, 8: link+wat, 9: phil+wat
ct=0
for i in range(0, len(keygroups)-1):
    for j in range(i+1, len(keygroups)):
        grpMap[i][j] = ct
        ct+=1

groupPair = np.zeros((ngrpPair,len(npairs[0])))
npairs = np.array(npairs)
ct = 0

for i in range(ngroups-1):
    for j in range(i+1, ngroups):
        ind = grpMap[groupClass[i]][groupClass[j]]
        groupPair[ind] = np.add(groupPair[ind],
                                        npairs[ct])
        ct+=1

#writing out groups for gelcore
# 0: core+phob, 1: core+link, 2: core+phil, 3: cor+wat
# 4: phob+link, 5: phob+phil, 6: phob+wat
# 7: link+phil, 8: link+wat, 9: phil+wat
vorres = open('star%d_vorres.out' % star_no, 'w')
vorres.write('\n Total interfacial area, stdev for: ')

allAvgDev=[]

for i in range(len(keygroups)-1):
    total = np.zeros(len(npairs[0]))
    for j in range(len(keygroups)):     
        ind = grpMap[min(i,j)][max(i,j)]
        if ind > -1:
            if (j>i):
                vorres.write('%s/%s, ' %(keygroups[i], keygroups[j]))
                allAvgDev.append(np.mean(groupPair[ind]))
                allAvgDev.append(np.std(groupPair[ind]))
            total = np.add(total, groupPair[ind])
    
    vorres.write('%s/All, ' %(keygroups[i]))
    allAvgDev.append(np.mean(total))
    allAvgDev.append(np.std(total))

vorres.write('\n')
for i in range(len(allAvgDev)):
    vorres.write("%.1f " % (allAvgDev[i]))
vorres.write('\n')

# writing out original format in XLS file
vorres.write('\n Total interfacial area, stdev for: ')
vorres.write('Hphobe/Wat + Hphobe/Hphil')
vorres.write(', Hphil/Wat + Hphil/Hphobe,')
vorres.write(' Hphobe/Wat, ')
vorres.write( ' Hphobe/Hphil , phil/Wat\n')

phob_all = allAvgDev[16]; obAllD = allAvgDev[17]
phil_all = allAvgDev[26]; ilAllD = allAvgDev[27]

vorres.write("%.4f %.4f %.4f %.4f "
        % (phob_all, obAllD, phil_all, ilAllD))
vorres.write("%.4f %.4f %.4f %.4f " 
        %(allAvgDev[14], allAvgDev[15], allAvgDev[12], allAvgDev[13]))                        
vorres.write("%.4f %.4f\n" % (allAvgDev[24], allAvgDev[25]))

vorres.write('\n Avg # of clusters size, stdev:\n')
vorres.write("%5.4f %5.4f " % (np.mean(intwat), np.mean(clusize)))
vorres.write("%5.4f,%5.4f \n" % (np.std(intwat), np.std(clusize)))

# Computing histogram of int water ct, and print
nct, bins = np.histogram(intwat, 
            bins=np.arange(0.0,45.0, 1), 
            normed=True)

# Computing histogram of int water size, and print
nsz, bins = np.histogram(clusize, 
                bins=np.arange(1.0,46.0, 1), 
                normed=True)          

wat_clu = open("star{0}_watclu.dat".format(star_no),'w')
for a in range(len(nct)):
    wat_clu.write("{0:5.4f} {1:5.4f}\n".format(nct[a], nsz[a]))

## Binning the water's distance from bulk water
n, bins = np.histogram(watedge, 
                    bins=np.arange(0.0,35.0, 0.5), 
                    normed=True)
vorres.write('\n Hist of water distance to bulk :\n')
for a in n:
    vorres.write("%5.4f\n" % a)

vorres.close()

