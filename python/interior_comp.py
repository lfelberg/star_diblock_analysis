
import numpy as np

"""
 MAIN
"""
percent_hphob = []

with open('neigh.dat') as f:
	for line in f:
		percent_hphob.append(100.0-float(line.split()[-1]))
		

# Printing out data:
percent_hphob = np.array(percent_hphob)

print ('\n Average number of hphil neighbors, stdev:\n')
print ("%5.2f \t %5.2f" % (100.0-np.mean(percent_hphob), np.std(percent_hphob)))

print ('\n Average number of hphob neighbors, stdev:\n')
print ("%5.2f \t %5.2f" % (np.mean(percent_hphob), np.std(percent_hphob)))

n, bins = np.histogram(percent_hphob, bins=np.arange(0.0,105.0, 5.0), normed=True)
print('\n Histogram of interior water neighbors percentage hphob:\n')
for a in n:
    print("%5.4f" % a)

