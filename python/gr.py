import sys
import numpy as np
#import matplotlib
#import matplotlib.pyplot as plt 

from inpfile import InpFile
from xyzfile import XYZFile
from volfile import VolFile

from util import translate_pbc, d_pbc

NUM_DENSITY_H2O = 1728./52534.8456042 # num of wat mols per A^3 (TIP4P EW 298K)

class GRCal:
    '''A class for pair correlation calcs'''
    def __init__(self):
        self.gr = ""; self.dr = 0.05; self.dim = 3
        self.bns = np.arange( 0., 15., self.dr)
        self.radi = self.dr/2.+self.bns[:-1] # center of each histogram bin

    def cal_gr(self, inC, grPr):
        ''' For an input, calculate the pair correlation for a given pair '''
        his_all = np.zeros(len(self.radi)); snap_ct = 0.0

        for i in range(len(inC.xyzs)):
            vC = VolFile(inC.xyzs[i][:-3]+"vol") # getting vol and coords for
            xyzC = XYZFile(inC.xyzs[i], vC)      # each xyz
            vol = vC.get_vol(); 

            for t in range(len(xyzC.atom)):
                snap_ct += 1.0
                rng = vC.get_edge_i(vC.time[t]); rg_vals = []
                dists = self.cal_d(grPr, xyzC.atom[t], xyzC.types, rng) 
                his_all += self.g_of_r(dists) #vol)

        his_all /= snap_ct
        g_f = inC.xyzs[0][:inC.xyzs[0].index("w")+1]+"." 
        g_f += str(grPr[0])+"_"+str(grPr[1])+"_gr"; 
        self.print_gr(his_all, g_f);#self.plot_gr(his_all, g_f)

    def cal_d(self, pair, dat, types, rang):
        '''For an array of data, find the desired types and return distances'''
        n0 = np.sum((types == pair[0]).astype(int)); dist = []
        n1 = n0 if pair[0]==pair[1] else np.sum((types==pair[1]).astype(int))
        crd0 = dat[types == pair[0]]; crd1 = dat[types == pair[1]]
        for fm in range(int(n0)): # for each of type 0, find dist to each type 1
            to_arr = crd1 if pair[0]!=pair[1] else np.delete(crd0,fm,0)
            frm_ar = np.repeat(crd0[np.newaxis,fm],len(to_arr),axis=0)
            dist.append(d_pbc(to_arr, frm_ar, rang))
        return dist

    def g_of_r(self, dist, density = 1.0):
        ''' Given an array of distances, histogram to compute g(r) '''
        hist, bins = np.histogram(dist, bins = self.bns)
        upp, low = self.bns[1:], self.bns[:-1]
        ndens = float(len(dist)) #/ density
        if self.dim==3: nfact=4.0/3.0*np.pi*(np.power(upp,3.)-np.power(low,3.)) 
        else:        nfact = np.pi*(np.power(upp, 2.) - np.power(low,2.))
        return hist /(nfact * ndens)
                    
    def print_gr(self, grs, fn):
        '''Print time averaged g-r'''
        f = open(fn+'.dat', 'w'); f.write("Bin,GR\n")
        for i in range(len(self.radi)):
            f.write("{0:.4f},{1:.7f}\n".format(self.radi[i], grs[i]))
        f.close()

    def plot_gr(self, gr_av, fn):
        '''Plot the computed gr'''
        matplotlib.rcParams.update({'font.size': 8})
        f = plt.figure(1, figsize = (1.5, 1.5));  ax = f.add_subplot(111)
        ax.plot(self.radi, gr_av); ax.set_ylim(0, 0.1);
       #ax.set_xlim(0, RMAX)
        ax.yaxis.labelpad = -0.6; ax.xaxis.labelpad = -0.6
        ax.set_ylabel("g(R)",fontsize=10)
        ax.set_xlabel("Distance ($\AA$)",fontsize=10)
        plt.savefig(fn+'.png',format='png', bbox_inches='tight',dpi=300)
        plt.close()


def main():
    filename=sys.argv[1]; grPr = [int(sys.argv[2]),int(sys.argv[3])]
    inC = InpFile(filename)
    gC = GRCal(); gC.cal_gr(inC, grPr)

if __name__=="__main__":
    main()
