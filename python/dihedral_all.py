# -*- coding: utf-8 -*-
'''
Module for gathering dihedral angles for POX backbone atoms. 
Different from other bc it is saving all dihedrals
'''

import sys
import math
import numpy as np
import subprocess
from numpy import linalg as LA

starSpecs = {
     4  : ([0, 0, 1], ['S-PEG', '^', 0, 'PVL', 'PEG', 8, 3, 6], 
           [26, 27, 28, 29, 30, 31], []), 
     6  : ([1, 0, 0], ['S-PMeOX', 'o', 0, 'PVL', 'PMeOX', 8, 3, 10], 
           [26, 27, 28], []), 
     8  : ([0, 0.5, 0], ['L-APEG', 's', 1, 'PVL', 'PEG', 16,12, 50], 
           [26, 27, 28, 29, 30, 31], []),
     9  : ([.93, .53, .18], ['L-PC1', 's', 1, 'PVL', 'PC1', 16, 24, 60]),
     12 : ([0.6, 0.2, 0], ['L-DenPEG', 'D', 1, 'PVL', 'PEG', 16, 12, 60], 
           [60, 61, 62, 63, 64, 65], []),
     15 : ([0.6, 0.2, 0], ['L-DenPEG', 'D', 1, 'PVL', 'PEG', 16, 12, 60], 
           [27, 28, 29, 30, 31, 32], []),
     24 : ([0.749, 0, 0.749], ['L-PMeOX', 'D', 1, 'PVL', 'PMeOX', 16, 12, 10], 
           [26, 27, 28], []),
     25 : ([72./255., 108./255., 186./255.], ['L-GelPEG', '^', 1, 'Linker', 
           'PEG', 1, 12, 40],[ 27, 28, 29, 30, 31, 32]),
     26 : ([102./255., 0, 51./255.], ['L-PEtOX', 's', 1, 'PVL', 'PEtOX', 
           16, 12, 10], [26, 27, 28], []),
     27 : ([0, 0, 1], ['L-PIBuOX', 'v', 1, 'PVL', 'PIBuOX', 16, 12, 10], 
           [26, 27, 28], []),
     30 : ([0, 0, 1], ['L-PIBuOX', 'v', 1, 'PVL', 'PIBuOX', 16, 12, 10,1], 
           [26, 27, 28], [36,39,42,45]),
     31 : ([0, 0, 1], ['L-PIBuOX', 'v', 1, 'PVL', 'PIBuOX', 16, 12, 10,5], 
           [26, 27, 28], [36,39,42,45,48,51,54,57]),
     35 : ([0, 0, 1], ['L-PEtVL', 'v', 1, 'PEtVL', 'PEG', 16, 12, 30,5], 
           [47, 48, 49], []),
     36 : ([0, 0, 1], ['L-PIButVL', 'v', 1, 'PIButVL', 'PEGX', 16, 12, 30,5], 
           [53, 54, 55], []),
      }

xyztemp = subprocess.check_output('ls ../data/*.xyz', shell=True)
xyzNames = xyztemp.split()

star_no = int(sys.argv[1])

# Given the coordinates of the four points, obtain the vectors b1,
# b2, and b3 by vector subtraction.

# Let me use the nonstandard notation ⟨v⟩ to denote v/∥v∥, 
# the unit vector in the direction of the vector v. 
# Compute n1=⟨b1×b2⟩ and n2=⟨b2×b3⟩, the normal vectors to 
# the planes containing b1 and b2, and b2 and b3 respectively. 
# The angle we seek is the same as the angle between n1 and n2.

#The three vectors n1, ⟨b2⟩, and m1:=n1×⟨b2⟩ form an orthonormal frame. 
# Compute the coordinates of n2 in this frame: x=n1⋅n2 and y=m1⋅n2. 
# (You don't need to compute ⟨b2⟩⋅n2 as it should always be zero.)

#The dihedral angle, with the correct sign, is atan2(y,x).

def getDihed(xs, ys, zs):
    """ Compute a dihedral angle with a vector of
         x, y and z coordinates """
    b1 = np.array([xs[1]-xs[0], ys[1]-ys[0], zs[1]-zs[0]])
    b2 = np.array([xs[2]-xs[1], ys[2]-ys[1], zs[2]-zs[1]])
    b3 = np.array([xs[3]-xs[2], ys[3]-ys[2], zs[3]-zs[2]])
    
    b1b2 = np.cross(b1,b2);b2b3 = np.cross(b2,b3) 
    n1 = b1b2/LA.norm(b1b2);n2 = b2b3/LA.norm(b2b3);
    nb2 = b2/LA.norm(b2)
    m1 = np.cross(n1, nb2)
    X = np.dot(n1, n2);Y = np.dot(m1,n2)
    
    return math.atan2(Y,X)
    
    
def getData(fileNames, starNum):
    natoms, narms, dihed_number, n_peg = 0, 16, [], starSpecs[starNum][1][6]
    backb_at = 6;
    records, bb_ct = 0, 0
    pox_bool = "OX" in starSpecs[starNum][1][0];
    bb_x, bb_y, bb_z = [],[],[]; COCC, OCCO, CCOC = [],[],[]; 
    side_x, side_y, side_z, sideCNCC = [],[],[],[];
    
    # Gathering atom coords from xyz files, for PEG, ats are in correct
    # order, just need to pull out specific mass types
    for fil in fileNames:
        print( fil)
        count, interval = 0, starSpecs[starNum][1][7]
        with open(fil) as f:
            for line in f:
                temp = line.split()
                if ("Atom" in line):
                    count+=1
                if (((count % interval)==0) and (np.size(temp)==4) 
                    and (int(temp[0]) in starSpecs[starNum][2])):
                    records+=1; 
                    bb_x.append(float(temp[1]));bb_y.append(float(temp[2]));
                    bb_z.append(float(temp[3]))
                if (pox_bool and ((count % interval)==0) 
                     and (np.size(temp)==4) 
                     and (int(temp[0]) in starSpecs[starNum][3])):
                    side_x.append(float(temp[1]));
                    side_y.append(float(temp[2]));
                    side_z.append(float(temp[3]))
                    if (int(temp[0]) == starSpecs[starNum][3][0]):
                        bb_ct+=1
        print("This is my ct {0} and records {1} and {2}".format( count, records, backb_at))

    dihed_mat = np.zeros((3,3,3)); 
    side_pos = np.zeros((3)) 
    if starSpecs[starNum][3] != []:
        side_pos = np.zeros((starSpecs[starNum][1][8],3)) 
    
    
    # Computing the dihedral angles and sorting them into groups
    for i in range(records/backb_at):
        # POX backbone ordered differently in LMP file than 
        # peg, order is N, C1, C3!!
        temp = [getDihed([bb_x[backb_at*i+1],bb_x[backb_at*i],bb_x[backb_at*i+2],bb_x[backb_at*i+4]],
                [bb_y[backb_at*i+1],bb_y[backb_at*i],bb_y[backb_at*i+2],bb_y[backb_at*i+4]],
                [bb_z[backb_at*i+1],bb_z[backb_at*i],bb_z[backb_at*i+2],bb_z[backb_at*i+4]]),
                getDihed([bb_x[backb_at*i],bb_x[backb_at*i+2],bb_x[backb_at*i+4],bb_x[backb_at*i+3]],
                [bb_y[backb_at*i],bb_y[backb_at*i+2],bb_y[backb_at*i+4],bb_y[backb_at*i+3]],
                [bb_z[backb_at*i],bb_z[backb_at*i+2],bb_z[backb_at*i+4],bb_z[backb_at*i+3]]),
                getDihed([bb_x[backb_at*i+2],bb_x[backb_at*i+4],bb_x[backb_at*i+3],bb_x[backb_at*i+5]],
                [bb_y[backb_at*i+2],bb_y[backb_at*i+4],bb_y[backb_at*i+3],bb_y[backb_at*i+5]],
                [bb_z[backb_at*i+2],bb_z[backb_at*i+4],bb_z[backb_at*i+3],bb_z[backb_at*i+5]])]
                    
        COCC.append(temp[0]); OCCO.append(temp[1]); CCOC.append(temp[2])
        
    for i in range(len(COCC)):
        print('{0}   {1:.3f}   {2:.3f}  {3:.3f}'.format(i, 
                      COCC[i], OCCO[i], CCOC[i]))
  
    
getData(xyzNames, star_no)
