#interior_neigh.py
"""This python script that builds an input for fortran interior programs into a 
qsub file that will perform all analysis
"""

import subprocess
import sys

in_directory = '../data'
cat_num = 7  #this is the number of of files to be concatenated 

command_string = 'ls *[0-9].vout'
temp_vout = subprocess.check_output(command_string, shell=True).split()


xyz_files = []
time = 0
for i in range(len(temp_vout)):
  qsub = open('interior_wat'+temp_vout[i][:-5]+'.qsub', 'w')
  qsub.write('#!/bin/bash\n#$ -S /bin/bash\n#$ -cwd\n')
  qsub.write('#$ -N s{0}in{1}\n#$ -o st{0}i{1}.out'.format(temp_vout[i][4:-7],i)) 
  qsub.write('#$ -q all*\n')
  qsub.write('#$ -j y\n\n')
  cat_line = 'cat '
  new_filename = temp_vout[i][:-5]
  clust_filename = new_filename + '.cluster'
  neigh_filename = new_filename + '.neigh '
  vout_filename = new_filename + '.interior.vout'
  vin_filename = new_filename + '.interior.vin'
  qsub.write('/home/lfelberg/star_diblock_copolymer/analysis/bin/interiorcheck %s\n' % (clust_filename))
  qsub.write('/home/lfelberg/star_diblock_copolymer/analysis/bin/voronoi_new %s > %s\n' % (vin_filename, vout_filename))
  cat_line+=neigh_filename
  qsub.close()

