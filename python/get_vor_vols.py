'''From a list of vout files, find the volume occupied by each
class and average'''

import numpy as np
import sys
import re
import subprocess

star_no = int(sys.argv[1])
tmp = int(sys.argv[2])
nclass = int(sys.argv[3])
vouttemp = subprocess.check_output('ls *[0-9].vout', shell=True)
voutfiles = vouttemp.split()

volStr= "Volume, interfacial area based on classification file"
#Total volume for sites of type  1 is         24574.240989  (CORE)

"""
 MAIN
"""

vols = [ [] for x in range(nclass)]

for names in voutfiles:
    flag = -1
    with open(names) as f:
        for line in f:
            if line.startswith(volStr):
                flag = 1
            if (line.startswith('Total volume for sites of type') and
               flag == 1):
               temp = line.split()
               vols[int(temp[-4])-1].append(float(temp[-2]))
               if ( int(temp[-4]) == nclass ) :
                   flag = -1


# Printing out data:
vols_arr = np.array(vols)
vols_avg = np.average(vols_arr, axis = 1)
strin = "{0} {1} ".format(star_no, tmp)
for i in range(len(vols_avg)):
    strin += " {0:.1f} ".format(vols_avg[i])

print(strin)

