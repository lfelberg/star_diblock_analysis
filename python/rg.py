import sys
import numpy as np

from inpfile import InpFile
from xyzfile import XYZFile
from volfile import VolFile

from util import translate_pbc, d_pbc

class RGCal:
    '''A class for RG calcs'''

    def __init__(self):
        self.gr = ""

    def cal_chain_rg(self,inC):
        ''' For an input, calculate the Rg of many chains in solution '''
        mols = inC.lmp.non_wat_mols(); mol_bs, ats, rg_lst = [], [], []
        rg_f = inC.inpfname[:inC.inpfname.index(".")]+".rg"; rf=open(rg_f,"w")
        for ml in range(len(mols)):
            mol_bs.append(inC.lmp.bonds_mol_i(ml))
            ats.append(inC.lmp.mol_i(ml))
        for i in range(len(inC.xyzs)):
            vC = VolFile(inC.xyzs[i][:-3]+"vol") # getting vol and coords for
            xyzC = XYZFile(inC.xyzs[i], vC)      # each xyz

            for t in range(len(xyzC.atom)):
                rng = vC.get_edge_i(vC.time[t]); rg_vals = []
               #print("{0}\nAtoms".format(np.sum(ats).astype(int)))
               
                for ml in range(len(mols)):
                    coords = np.zeros((np.sum(ats[ml]).astype(int),3))
                    ml_lst = np.where(ats[ml] == True)[0]
                    coords[0] = xyzC.atom[t][ml_lst[0]]; frst = ml_lst[0]
                   #print("{0} {1} {2} {3}".format(ml,*(coords[0])))
                    for b in range(len(mol_bs[ml])):
                        to, fr = mol_bs[ml][b][0]-frst,mol_bs[ml][b][1]
                        coords[fr-frst]=translate_pbc(coords[to],
                                                      xyzC.atom[t][fr],rng)
                       #print("{0} {1} {2} {3}".format(ml,*(coords[fr-frst])))
                    rg_vals.append(self.rg_comp(coords))
                rg_lst.append(rg_vals)
        for i in range(len(rg_lst)):
            st = "" 
            for j in range(len(rg_lst[i])): st+="{0:.5f},".format(rg_lst[i][j])
            rf.write(st[:-1]+"\n")

    def rg_comp(self, coords):
        '''Given a list of coords, compute Rg^2 = 1/N * sum(r_i - r_mean)^2'''
        r_mn = np.mean(coords, axis = 0)[np.newaxis]
        rg = (1./float(coords.shape[0]))*np.sum(np.power(coords-r_mn,2.0))
        return np.sqrt(rg)
                    

def main():
    filename=sys.argv[1]
    inC = InpFile(filename)
    rgC = RGCal()
    rgC.cal_chain_rg(inC)

if __name__=="__main__":
    main()
