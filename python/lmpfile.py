import numpy as np

class LMPFile:
    '''A class for lmp files'''
    lmpfname = ''
        
    def __init__(self, fname):
        self.lmpfname = fname
        self.get_types(fname)
        print("lmp: {0}, atsh {1}, molshp {2}, bdsh {3}".
              format(fname,len(self.atoms),
              self.mol.shape, self.bonds.shape))

    def get_types(self, filename):
        '''Method to open lmp file and save coords and types'''
        self.atoms, self.types, self.mol, ms, at = [], [], [], -1, -1
        self.bonds, bd, ang = [], -1, -1
        f=open(filename, "r")
        for line in f:
            if "Pair Coeffs" in line: ms = -1
            if "Velocities" in line:  at = -1
            if "Angles" in line: break
            if at == 0 and len(line) > 6:
                tmp = line.split()
                self.types.append(int(tmp[2])-1)
                self.mol.append(int(tmp[1])-1)
            if ms == 0 and len(line) > 6:
                tmp = line.split()
                self.atoms.append(tmp[-1])
            if bd == 0 and len(line) > 6:
                tmp = line.split()
                self.bonds.append([int(tmp[-2])-1,int(tmp[-1])-1])
            if "Masses" in line: ms =  0
            if "Atoms" in line:  at =  0
            if "Bonds" in line:  bd =  0
        f.close()
        self.mol = np.array(self.mol)
        self.bonds = np.array(self.bonds)
        self.types = np.array(self.types)
    
    def get_atm_at_i(self, i):
        '''Get indices of element type i'''
        return self.atoms == i
    
    def get_atm_i(self, i):
        '''Get element of atom number i'''
        return self.atoms[i]
    
    def non_wat_mols(self): 
        '''Get the numbers of non-water molecules '''
        nml = max(self.mol)
        nwat = []
        for i in range(nml):
            if sum((self.mol == i).astype(int)) > 3: nwat.append(i)
            elif sum((self.mol == i).astype(int)) == 3: break
        return nwat

    def bonds_mol_i(self, i):
        ''' Get the bondlist for molecule i'''
        bds = []
        for j in range(self.bonds.shape[0]):
            if self.mol[self.bonds[j][0]] == i: bds.append(self.bonds[j])
            if self.mol[self.bonds[j][0]] > i: break
        return np.array(bds)

    def mol_i(self, i):
        '''Get list of atoms in molecule i'''
        return self.mol == i

