import sys

side_class = sys.argv[1] # class for sidechain
norm_class = sys.argv[2] # class for extended system
starno = int(sys.argv[3])


# get the org of the side class from side_class file
s = open(side_class, 'r').readlines()
phob_f, phil_f, phil_side = -1, -1, []

for line in s:
    if "  PHOB" in line: phob_f = 0
    if "  PHIL" in line: 
        phil_f = 0
        phil_side.append(line)

    # If you have already run thru first arm, break    
    if "  PHOB" in line and phil_f == 0: break

print(len(phil_side))

nm = open(norm_class, 'r').readlines()
nm_side = open("star" + str(starno) + "wat_side.norm.class", "w")

phil_arms = []
phil_ct, phil_old, inn, phil_not_s = 0, 0, -1, 18

for line in nm:
    if "  PHIL" in line:
        tmp = line.split()
        phil_ct = int(tmp[0])
        if phil_ct != phil_old: inn = 0
        lin = phil_side[inn].split()
        
        if "S" in phil_side[inn]: 
            cl_no = phil_ct; cl_nm = tmp[1]
        else: 
            cl_no = phil_not_s; cl_nm = "PHIL"
        stn = "    {0:2d}  {1}\n".format(cl_no, cl_nm)

        phil_old = phil_ct
        inn += 1
        
    elif line.startswith("WAT"): stn = "PHIL         18\n"+ line[:-3] + "19\n"
    elif "  WAT" in line: stn = "     19  WAT\n"
    elif "of classes" in line: stn = "   19 number of classes in this scheme\n"
    else: stn = line    

    nm_side.write(stn)

nm_side.close()

qsub = open("st"+str(starno)+"_norm_side.qsub", "w")
qsub.write("#!/bin/bash\n")
qsub.write("#$ -S /bin/bash\n")
qsub.write("#$ -cwd\n")
qsub.write("#$ -N vsid_st{0}\n".format(starno))
qsub.write("#$ -o vsid_st{0}\n".format(starno))
qsub.write("#$ -pe threaded 1\n")
qsub.write("#$ -j y\n")

exe = "/home/lfelberg/star_diblock_copolymer/analysis/bin/voronoi" 
exe += "  voro_norm_side.vin > voro_norm_side.vout\n"
qsub.write(exe)
qsub.close()
