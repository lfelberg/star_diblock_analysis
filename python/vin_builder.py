#vin_builder.py
"""This python script that runs the vin_builder codes for all .xyz files in a directory.  
Concatenates .vin files based on a user input #.
  Inputs: # of atoms (water and polymer separate), volume, directory of .xyz files, 
      the desired name of the final .vin files, number of the first run, and the start time
  Output: a .vin file with the proper structure"""

import subprocess
import sys

in_directory = '../data'
cat_num = 1  #this is the number of of files to be concatenated at once 
#(so if it = 10, the first 10 will be concatenated, then the next 10, etc)

#STAR34:
star = '31'
num_poly = '13088'
num_water = '52380'

class_f = "CLASSNM"
cmd = "ls -1v " + in_directory + "/run.*.out > temp_file.txt"
subprocess.check_output(cmd, shell=True)
temp_file = open('temp_file.txt')
raw_out_files = temp_file.readlines()
subprocess.call(['rm','temp_file.txt'])

def find_inp_info(fname):
    f = open(fname, "r").readlines(); xyz, freq, out_f = "", 0, 0
    for line in f:
        if "dump" in line:
            tmp = line.split()
            xyz, freq = tmp[5], int(tmp[4])
        if "thermo " in line: out_f = int(line.split()[1])
    return xyz, freq, out_f

def find_times_frm_vol(fname):
    f = open(fname, "r").readlines();
    tims = []
    for lin in range(len(f)): tims.append(int(f[lin].split()[0]))
    return tims

s_vin = open("sample.vin", "w")
xyzn, num, tim = [],[],[]
t_step,last_end,curr_start = 0, 0, 0

for i in range(len(raw_out_files)):
    out_f = raw_out_files[i].split()[0]
    in_f = out_f[:-3]+"inp"
    xyz, freq, out_f = find_inp_info(in_f)
    times = find_times_frm_vol(in_directory+"/"+xyz[:-3]+"vol")    
    idx = [range(1, len(times)+1)]
    curr_start = times[0]
    if i == 0: last_end = curr_start
    if t_step == 0: t_step = times[1]-times[0]
    if curr_start > last_end - t_step: # data is missing
        print("Er! Gap btw {0} and {1}".format(xyz, raw_out_files[i-1][:-1]))
        break
    elif curr_start < last_end: # overlap btw curr and prev xyz
        print("Overlap, btw {0} t = {1} and {2} t = {3}".format(
              xyz, curr_start, raw_out_files[i-1][:-1], last_end))
        st = times.index(last_end)
        n_snp = len(times[st:])-1
        xy_l  = [xyz] * n_snp
        num_l = range(st+2, len(times)+1)
        tim_l = list(times[st+1:])
    else: # everything is good, adding to list, except first = last of prev
        n_snp = len(times)-1
        xy_l  = [xyz] * n_snp
        num_l = range(2, n_snp+2)
        tim_l = list(times[1:])
       #print(len(xy_l), len(num_l), len(tim_l))
       #print(xy_l[0], num_l[0], tim_l[0])
       #print(xy_l[-1], num_l[-1], tim_l[-1])

    last_end = times[-1]
    xyzn += xy_l; num += num_l; tim += tim_l

for i in range(len(tim)):
    l1 = "{0}  {1:>d}   2   {2} {3} Time   {4:>d}\n".format(
           xyzn[i], num[i], num_poly, num_water, tim[i])
    l2 = "distancecutoff 15.   classificationfile " + class_f + "\n"
    l3 = "clusters\n"
    s_vin.write(l1+l2+l3)
s_vin.close()

