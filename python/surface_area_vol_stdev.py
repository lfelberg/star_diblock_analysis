import numpy as np
import sys
import re

saout = sys.argv[1]
st_t = re.findall(r'\d+',saout)
sa_to_v = ""

with open(saout) as f:
  for line in f:
    if (("vg surf ratio" not in line) and ("voronoi filename" not in line)):
      temp = line.split()
      if sa_to_v == "": 
        nvals = len(temp)
        sa_to_v = [ [] for x in range(3, nvals)]
      if (float(temp[2]) >= 10000):
        for i in range(3, nvals):
          sa_to_v[i-3].append(float(temp[i]))


stn = st_t[0] + "  " + st_t[1]
for i in range(nvals-3):
    sa = np.array(sa_to_v[i])
    stn += " {0:.4f}  {1:.4f}".format(np.mean(sa), np.std(sa))

print(stn)
