'''
Module for obtaining and plotting the persistence data
'''

import numpy as np
#import matplotlib.pyplot as plt
#from matplotlib.backends.backend_pdf import PdfPages

"""
Below is a dictionary for storing the names and
necessary colors for a given star.
key is int and value is a tuple: ([color],
[Star name, marker shape, 300(y=0), Hphob, hphil, nphob, nphil])
"""
starSpecs = {
4 : ([0, 0, 1], ['S-PEG', '^', 0, 'PVL', 'PEG', 8,3]),
6 : ([1, 0, 0], ['S-PMeOX', 'o', 0, 'PVL', 'PMeOX', 8,6]),
8 : ([0, 0.5, 0], ['L-APEG', 's', 1, 'PVL', 'PEG', 16,12]),
9 : ([.93, .53, .18], ['L-PC1', 's', 1, 'PVL', 'PC1', 16,24]),
12 : ([0.6, 0.2, 0], ['L-DenPEG', 'D', 1, 'PVL', 'PEG', 17, 13]),
15 : ([.282, .424, .729], ['L-Gel', '^', 1, 'PVL', 'PEG', 16, 12]),
24 : ([0.749, 0, 0.749], ['L-PMeOX', 'D', 1, 'PVL', 'PMeOX', 16, 24]),
25 : ([.282, .424, .729], ['L-GelPEG', '^', 1, 'Linker', 'PEG', 1, 12]),
26 : ([.4, 0, .2], ['L-PEtOX', 's', 1, 'PVL', 'PEtOX', 16,24]),
27 : ([0, 0, 1], ['L-PIBuOX', 'v', 1, 'PVL', 'PIBuOX', 16,24]),
30 : ([0, 0, 0], ['L-PNBuOX', 'o',1, 'PVL', 'PNBuOX', 16,24]),
31 : ([0, .502, .502], ['L-PNOctOX', 'v',1, 'PVL', 'PNOctOX', 16,24]),
32 : ([0, 0, 0], ['L-PEGOX', 'o',1, 'PVL', 'PEGOX', 16,24]),
33 : ([0, 0, 0], ['L-PNBuOX', 'o',1, 'PVL', 'PNBuOX', 16,24]),
34 : ([0, .502, .502], ['L-PMeOX-PEE', 'v',1, 'PVL', 'PMeOX-PEE', 16,12]),
35 : ([0, .502, .502], ['L-PEtVL-PVL', 'v',1, 'PEtVL-PVL', 'PEG', 16, 12]),
36 : ([0, .502, .502], ['L-PTBuVL-PVL', 'v',1, 'PTBuVL-PVL', 'PEG', 16, 12]),
37 : ([0, .502, .502], ['L-PNBuVL-PVL', 'v',1, 'PNBuVL-PVL', 'PEG', 16, 12]),
300 : ( [0, 0, 0], ['300K'] ),
350 : ( [0, 0, 1], ['350K']  ),
400 : ( [ 0.6, 0.4, 0.8], ['400K']  ),
450 : ( [1, 0, 0], ['450K']  )
}

#starList = [6,24,26,27,30,31]
starList = [star_number]
paper, temp =  2, star_temperature
testFile = 'sori_file'
outFile = '/Users/lfelberg/star_diblock_copolymer/'\
             'fig/paper' + str(paper)+ '/'


def getData(fileName, starNumList):
    '''Retrieves data from a manually formatted
    .dat file or from a .sori file'''
    lines = open(testFile).readlines()
    narms, time, endonarm, firstonarm, tmax, told = 16, -1, 0, 1, 200, 0
    hphil, hphob = starSpecs[starList[0]][1][5], starSpecs[starList[0]][1][6]
    end, first, corx, cory, corz = hphil+hphob, 1, [], [], []

    dot_end = np.zeros((tmax,narms,end))
    dot_end_time, end_dev = np.zeros((end)), np.zeros((end))
    dot_first = np.zeros((tmax,narms,end))
    dot_first_time, first_dev = np.zeros((end)), np.zeros((end))

    for lin in lines:
        temp = lin.split()
        timestep, armnumber, resid = int(temp[0]), int(temp[3]), int(temp[4])
        resx, resy, resz = float(temp[5]), float(temp[6]), float(temp[7])
        if ((told != timestep) and (timestep > 30)):
            time+=1; told = timestep
        if ((armnumber>0) and (timestep > 30) and (time < tmax)):
            if (resid == end):
                endref, endrefx, endrefy, \
                endrefz = resid,float(temp[5]), \
                float(temp[6]),float(temp[7])
            if (resid == 1):
                firstref, firstonarm, frefx, \
                frefy, frefz = resid, armnumber, \
                float(temp[5]),float(temp[6]),float(temp[7])
            if (armnumber == firstonarm):
                corx.append(resx);cory.append(resy);corz.append(resz)
                if (resid == end):
                    for i in range(end):
                        dot_end[time][armnumber-1][i] = \
                                    endrefx*corx[i]+ \
                                    endrefy*cory[i]+endrefz*corz[i]
                    corx, cory, corz = [], [], []
                dot_first[time][armnumber-1][resid-1] = \
                                    resx*frefx+resy*frefy+resz*frefz

    dot_first_time = np.average(
                                np.average(dot_first,axis=1),
                                axis=0)
    first_dev = np.std(np.average(dot_first,axis=1),
                                axis=0)
    dot_end_time = np.average(np.average(dot_end,
                                                    axis=1), axis=0)
    end_dev = np.std(np.average(dot_end,axis=1),
                                                    axis=0)

    #print "Perst wrt first, firstdev\n"
    for i in range(end):
        print '% 3.4f  % 3.4f  % 3.4f  % 3.4f  ' % \
                    (dot_first_time[i], first_dev[i],
                    dot_end_time[i],end_dev[i])

    return dot_first_time

data = getData(testFile, starList)
