import sys
from pdbfile import PDBFile

def class_make(pdbC):
    ''' Make classification file for mixture file '''
    nat = len(pdbC.chains); classn, cl_st, nclass = [],[],-1
    for i in range(nat):
        print(pdbC.chains[i], pdbC.types[i])
        if "POX" in pdbC.types[i]:
            if pdbC.chains[i] not in classn: classn.append(pdbC.chains[i])
            cl_st.append("     {0}   POX{0}".format(pdbC.chains[i]))
        else: # water atoms
            if nclass == -1:
                classn.append(pdbC.chains[i]); nclass = len(classn)
            cl_st.append("    {0}   WAT".format(nclass))

    cl = open("classification.classify", "w")
    cl.write("   1 number of classification schemes\n")
    cl.write("   {0} number of classes in this scheme\n".format(nclass))
    for i in range(nclass-1): cl.write("POX{0}      {0}\n".format(classn[i]))
    cl.write("WAT        {0}\n".format(nclass))
    cl.write("     {0}\n".format(nat))
    for j in range(nat): cl.write("    {0}\n".format(cl_st[j]))
    cl.close()

def main():
    filename=sys.argv[1]
    pdbC = PDBFile(filename, False)
    class_make(pdbC)

if __name__=="__main__":
    main()
