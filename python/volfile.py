import sys
import re
import numpy as np

class VolFile:
    '''A class for volume files'''
    volfname = ''
        
    def __init__(self, fname):
         self.volfname = fname
         if '.vol' in fname:   self.get_dim_from_vol(fname)
         else:                 self.volfname = ""

    def get_dim_from_vol(self, fname):
        '''Method to get dimensions from volume file'''
        f = open(fname, "r"); time, dims = [], []
        for line in f:
            tmp = line.split()
            time.append(int(tmp[0])); dims.append(float(tmp[1]))
        f.close(); self.time = np.array(time); self.dims = np.array(dims)

    def get_vol_i(self, i):
        '''Return volume for ith snap'''
        idx = np.where(self.time == i)[0][0]
        return self.time[idx]

    def get_vol(self):
        '''Return average volume'''
        return np.mean(self.dims)

    def get_edge_i(self, i):
        '''Return array with edge for time i'''
        return np.array([self.get_len_i(i),self.get_len_i(i),self.get_len_i(i)])

    def get_len_i(self, i):
        '''Return length of edge for time i'''
        idx = np.where(self.time == i)[0][0]
        return np.power(self.dims[idx], 1./3.)

    def get_len(self):
        '''Return average length of y dim'''
        return np.power(np.mean(self.dims), 1./3.)

