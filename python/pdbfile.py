import numpy as np

class PDBFile:
    '''A class for pdb files'''
    pdbfname = ''
        
    def __init__(self, fname, typ = True):
        self.pdbfname = fname
        if typ == True: self.get_types(fname)
        else:           self.get_chains(fname)

    def get_chains(self, filename):
        '''Method to open pdb file and save coords and types'''
        self.chains, self.types = [], []
        f=open(filename, "r").readlines()
        for line in f:
            if "ATOM" in line: 
                tmp = line.split()
                self.chains.append(int(tmp[-6]))
                self.types.append(tmp[-7])
 
    def get_types(self, filename):
        '''Method to open pdb file and save coords and types'''
        self.atoms, self.types = [], []
        f=open(filename, "r").readlines()
        for line in f:
            if "ATOM " in line: 
                tmp = line.split()
                self.atoms.append(tmp[2])
                self.types.append(tmp[3])
                                                      
    def get_type_at_i(self, i):
        '''Get indices of type i'''
        return self.types == i
    
    def get_type_i(self, i):
        '''Get type of atom number i'''
        return self.types[i]
    
    def get_atm_at_i(self, i):
        '''Get indices of element type i'''
        return self.atoms == i
    
    def get_atm_i(self, i):
        '''Get element of atom number i'''
        return self.atoms[i]
    
