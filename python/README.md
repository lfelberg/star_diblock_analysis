# Python analysis tools

This directory contains scripts that are used in analysis or post-processing
or preparing analysis for star diblock project.

## average_gelcore.py
Takes data from ensemble of gelcore runs and averages mass density, radius
of gyration etc across the ensemble.
### Inputs:
None
### Outputs:
stargaze_starg13_TempKw.fextension: an ensemble average

## dihedral_all.py
Program to calculate dihedral angles on the backbone of a PEG or POX block
### Usage:
```
>>> python dihedral_all.py star_no
```
### Outputs:
Prints dihedral information out to stdout

## dihedral.py: 
A python script that computes the dihedral angles between backbone (POX and PEG monomers) and
sidechain dihedral angles (POX) for use in LCST analysis. Looks for trios of 
angles for TGT trends.
### Usage:
```
>>> python dihedral.py star_no
```
### Outputs:
Prints the distribution of trios to stdout. If sidechain is there makes a histogram
of sidechain angle distribution.

## get_vor_vols.py
From a list of vout files, find the volume occupied by each class and average
### Usage:
```
>>> python get_vor_vols.py star_no temperature nclasses
```
### Outputs:
Prints to stdout the average volume occupied by each class

## gr.py
Compute the pair correlation function for two types of atoms in the simulation
### Usage:
```
>>> python gr.py inputfilename grAtom1 grAtom2
```
### Outputs:
A time averaged pair correlation function. Can also plot it if desired.

## inpfile.py
Class that opens and gets data from an input file. Contains the location of
pdb and lmp files as well as a list of xyz files to analyze.

## interior_comp.py
Using the neighbor analysis, compute the interior water environment
### Inputs:
None
### Outputs:
Prints stats of interior water neighbors to stdout

## interior_neigh.py: 
A python script that creates a qsub file to use Bill's interior slice programs
### Inputs:
Nothing
### Outputs:
A qsub file that will run interiorcheck, voronoi and then interiortable for a single
star system and concatenate the resulting tables into one file

## lmpfile.py
A class to read information about atoms and their type numbers from a lmpfile

## make_class_side.py
Make a classification file for systems of linear polymers in solvent
### Inputs:
inp.pdb: Input pdb style file will be looking for POX names to make class for 
polymer and solvent
### Outputs:
classification.classify: Classification file for voronoi

## make_class_side.py
Script to make a classification file for the sidechains separately.
### Usage:
```
>>> python make_class_side.py side_class norm_class star_no
```
### Outputs:
star#wat_side.norm.class : A classification scheme that makes a separate class
for the sidechain.
qsub: will make a qsub file to submit a voronoi analysis for this class.

## make_torsion_input.py
Make a pdb file only has a list of POX backbone heavy atoms for a torsion analysis
### Usage:
```
>>> python make_torsion_input.py pdbfile armfile star_no
```
### Outputs:
star#wat_tor.pdb: A truncated pdb file that only contains POX heavy bb atoms

## pdbfile.py
A class to read a pdb file and save information about each "residue".

## persistence.py: 
A python script to compute the correlation between monomers of a star polymer,
from the outside in and the inside out. 
### Inputs:
A sori file
### Outputs:
Prints out in table form the correlations and errors onto workspace for in -> out and reverse

## rg.py
Script to compute the radius of gyration of linear polymers in solution.
### Usage:
```
>>> python rg.py inputfilename
```
### Outputs:
A file with the radius of gyration of each chain for all chains in solution
at each step in the trajectory files designated in the input file.

## sin_builder.py: 
A python script that creates a .sin file for use in stargaze analysis tools
### Inputs:
All inputs are in the script! The location of directory containing batch .xyz 
files, name of .arm file, number to skip (2 means take every 
other listed .xyz file), and the desired name of the output file

Line 20: starno='starXX', should be in the format of the name in the XYZ file, 
so maybe all caps: STAR33 or starg15 etc

Line 21: temp='XX0Kw', temperature, usually 350Kw, 400Kw or 450Kw

Line 23: lmp_loc = '../../setup/%swat.lmp' % (starno), the lmp file should be in 
the setup dir and should have the name "starno"wat.lmp, for example, star9wat.lmp or STAR33wat.lmp. 

Line 24: arm_file = '../../setup/staradam16pdvlpoxb24.arm', need to change 
the name of the arm file to whatever is in the setup dir
### Outputs:
a .sin file with the proper structure and a qsub file

## surface_area_vol_stdev.py
Script that reads in an saout file (output from surface_analysis.f) file and computes
the mean and average of the following ratio. For various shells in the polymer (core,
hydrophilic, all), depending on the roughness of the shell, the polymer will to one extreme
be very smooth, and therefore its surface area to volume ratio will be close to that of a 
sphere. Otherwise, if it is very tortous in nature, there will be much more surface area for
the occupied volume. The program prints the mean of the ratio of actual SA/ideal (sphere) SA.
This value is 1 or greater.
### Usage:
```
>>> python surface_area_vol_stdev.py saout_file
```
### Outputs:
Prints to command line the mean and standard deviation of actual to ideal surface area.

## util.py
Contains two functions for dealing with periodic boundary wrapping. See
file for description of each function.

## vin_builder.py: 
**This program is deprecated! Now the stargaze program makes better vin files and 
a shell script breaks it up and generates a qsub**
A python script that creates many .vin files for use in voronoi analysis tools
It makes a .vin file for every .xyz file, but then allows the user
to concatenate them based on an input value (number of files to concatenate into a single file).
### Inputs:
They are all defined in the script! A directory of .xyz files and also requires the user
to input the number of .vin files they would like concatenated into a single file

Line 11: The relative location of the xyz and vol files to current directory. 

Line 12: The number of xyz files to include in 1 vin file. It's a tradeoff, less 
vin files means less jobs to keep track of, but a longer total running time.

Line 14-109: There is a block of 4 lines for each star, the 1st is the name commented 
out, followed by the numerical id as a string, then the number of polymer atoms then a 
variable for the number of water atoms. If your star is not there, you will need to add it in.

Line 154: The classfication file name is currently in the setup dir and called 
classification.classify, you may want to change that.
### Outputs:
.vin files with the proper structure

## volfile.py
Class to read in .vol files and save periodic box volume at given time
steps.

## voro_arm_analysis.py:
This script takes a .vout file from a voronoi analysis and calculates the
total surface area between every hydrophilic arm and water, other hphil areas and
hphobe areas for every time step, normalized with respect to the total hphil area.
### Usage:
```
>>> python voro_arm_analysis.py starXXX.vout
```
### Outputs:
three .dat files that correspond to hphil-hphil, hphil-hphobe and hphil-wat interactions.
Each has a column for each star arm and a line for each time.

## voronoi_results.py: 
Python script that collects and computes easily grabable outputs
of a voronoi analysis, like interfacial areas, water size analysis etc
### Inputs:
Star number, will look up normalization factors in stored dictionary
### Outputs:
star[#]_vorres.out - a file with results of many analyses, interfacial, water calcs etc

## water_restime.py
Make a histogram of interior water residence times.
### Usage:
```
>>> python water_restime.py watout
```
### Outputs:
Prints to a command line a histogram of residence times.

## xyzfile.py
Contains a class to read in a trajectory from an xyz file.

