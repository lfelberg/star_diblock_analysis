#voro_arm_analysis.py

import sys

"""This script takes a .vout file from a voronoi analysis and calculates the
total surface area between every hydrophilic arm and water, other hphil areas and
hphobe areas for every time step, normalized with respect to the total hphil area.
For example, in the the hphil output file, the value for each arm (in each timestep)
is the normalized area between that arm and all other hphil regions

This script requires a command-line input .vout file with the following example
structure:
	star4_300Kw.0.vout

Input: *.vout file 
Output:  three .dat file with the structure:


			arm1 arm2 arm3 . . .
time1
time2

The three .dat files correspond to hphil-hphil, hphil-hphobe and hphil-wat interactions
"""

#Inputs:
in_filename = sys.argv[1]
out_filename = in_filename[:in_filename.find('.')] + '_arm_'  #will have hphil.dat, water.dat, or hphobe.dat added to the end
#THIS NEXT LINE MUST CHANGE IF STAR NUMBER BECOMES 2 DIGITS!!!
star_number = in_filename[4]

in_file_lines = open(in_filename).readlines()

#output files:
hphil_out = open(out_filename + 'hphil.dat' , 'w')
hphobe_out = open(out_filename + 'hphobe.dat', 'w')
wat_out = open(out_filename + 'wat.dat', 'w')

hphil_out.write('         ')
hphobe_out.write('         ')
wat_out.write('         ')


#column headers:
arms = ['PHILA','PHILB','PHILC','PHILD','PHILE','PHILF','PHILG','PHILH','PHILI','PHILJ','PHILK','PHILL','PHILM','PHILN','PHILO','PHILP',]
for arm in arms:
	hphil_out.write(arm + '     ')
	hphobe_out.write(arm + '     ')
	wat_out.write(arm + '     ')

hphil_out.write('\n')
hphobe_out.write('\n')
wat_out.write('\n')


#normalization factors (total hphil area)
if star_number == '4':
	norm_factor = 413.2 #total accesible surface area of hphil region
elif star_number == '8':
	norm_factor = 1742.7


column_index = 0;
times = []
#indices of blocks of text for each snapshot
block_indices = []
#loop through every line in the input file to find how may snapshots
for i in range(len(in_file_lines)-1):
	line = in_file_lines[i];
	if line.find('Input file line 1') != -1 :
		block_indices.append(i)
		#get time:
		line_split = line.split();
		time = line_split[len(line_split)-1];
		while (len(time) < 6):
			time = ' ' + time
		times.append(time.rstrip('\n'))


#sum up interfacial areas, print lines

for j in range(len(block_indices)-1):
	time = times[j]
	#these will be the lines to print to the .dat files:
	line_for_hphil = time
	line_for_hphobe = time
	line_for_wat = time

	if j == len(block_indices) - 1:
		block = in_file_lines[block_indices[j] :]
	else:
		block = in_file_lines[block_indices[j]:block_indices[j+1]]

	for arm in arms:
		hphobe_sum = 0;
		hphil_sum = 0;
		wat_sum = 0;

		for line in block:
			if line.find(arm) != -1 :
				line_split = line.split()
				#PHOB
				if line.find('PHOB') != -1 :
					if line_split[7] != 'is':
						value_string = line_split[7]
					else:
						value_string = line_split[8]

					hphobe_sum = float(value_string)/norm_factor
				#WAT
				elif line.find('WAT') != -1 :
					if line_split[7] != 'is':
						value_string = line_split[7]
					else:
						value_string = line_split[8]

					wat_sum = float(value_string)/norm_factor
				#HPHIL
				else:
					if line_split[0] == 'Interfacial':
						if line_split[7] != 'is':
							value_string = line_split[7]
						else:
							value_string = line_split[8]

						hphil_sum += float(value_string)/norm_factor


		hphobe_sum_string = '%.5f' % hphobe_sum
		line_for_hphobe += '   ' + hphobe_sum_string

		wat_sum_string = '%.5f' % wat_sum
		line_for_wat += '   ' + wat_sum_string

		hphil_sum_string = '%.5f' % hphil_sum
		line_for_hphil += '   ' + hphil_sum_string
	
	hphil_out.write(line_for_hphil + '\n')
	hphobe_out.write(line_for_hphobe+ '\n')
	wat_out.write(line_for_wat + '\n')

