import numpy as np

from volfile import VolFile

class XYZFile:
    '''A class for xyz files'''
    xyzfname = ''
        
    def __init__(self, fname, VolFile = VolFile(""), xyz = [], ty = []):
        self.xyzfname = fname
        if xyz == []:    self.get_coords_types(fname,VolFile.time)
        else:            self.print_coords(fname, xyz, ty)

    def get_coords_types(self, filename, times):
        '''Method to open xyz file and save coords and types'''
        time, atoms, atom, types = [], [], [], []
        grab_snap, t_ct = 0, 0;  f=open(filename, "r")
        f.readline(); line = f.readline()
        if times == [] or times[0] == int(line.split()[-1]):
            time.append(int(line.split()[-1]))
            t_ct += 1; grab_snap = 1

        '''First, we read in the coordinates from the trajectory file and
           save them into atom=[[coords],[coords]...,[coords]]'''
        for line in f:
            #Gets past # and atoms lines
            if line[0]=="A":
                if grab_snap == 1:
                    atom+=[atoms]
                   #print(len(atoms), len(atom), filename, t_ct, times[t_ct])
                    atoms = []
                if t_ct >= len(times): break

                if times == [] or times[t_ct] == int(line.split()[-1]):
                    time.append(int(line.split()[-1]))
                    t_ct += 1; grab_snap = 1
                else: grab_snap = 0

            if times != [] and t_ct > len(times): break
            # Only grab snapshots that have volume data
            elif len(line.split())>1 and grab_snap==1 and "Atom" not in line:
                #crds is a list of the coordinates in string formats
                crds=line.split(); tmp = []
                if t_ct == 1: types.append(int(crds[0]))
                for i in range(1,len(crds)): tmp.append(float(crds[i]))
                atoms.append(tmp)
        #For last timestep, since it doesn't have a line about Atoms after it.
        if atoms != []: atom += [atoms]
        self.time  = np.array(time); self.atom = np.array(atom)
        self.types = np.array(types)

        print(self.atom.shape)
        for t in range(self.atom.shape[0]):
            for i in range(3): self.atom[t,:,i] -= min(self.atom[t,:,i])

        print("xyz: ",filename," nsnaps: ",len(times), " atoms, shape: ",
              self.atom.shape, " type shape: ",self.types.shape)
        f.close()

    def print_coords(self, fname, xyz = [], ty = []):
        '''If given coordinates, write them to file'''
        if xyz == []: xyz = self.atoms
        if ty == []:  ty  = self.types
        f, nat = open(fname, 'a'), 0

        for ti in range(len(xyz)):
            if nat == 0: nat = len(xyz[ti])
            if nat != len(xyz[ti]) : print("Different number of atoms")
            f.write("{0}\nAtoms\n".format(nat))
            for i in range(nat):
                strn = "{0}".format(ty[i])
                for j in range(len(xyz[ti][i])): 
                    strn+=" {0}".format(xyz[ti][i][j])
                strn+="\n"
                f.write(strn)

        f.close()

    def get_coord_type_i(self, i):
        '''Get indices of type i'''
        return self.atom[self.types == i,:]
    
    def get_coord_atm_i(self, i):
        '''Get indices of atom i'''
        return self.atom[i]
    
    def get_type_i(self, i):
        '''Get indices of type i'''
        return self.types == i
    
    def get_type_atm_i(self, i):
        '''Get indices of atom i'''
        return self.types[i]
    
