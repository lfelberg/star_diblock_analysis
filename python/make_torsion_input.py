import sys

pdbfile = sys.argv[1] # name of pdb file that will be copied and modified
armfile = sys.argv[2] # name of arm file that will be copied and modified
starno = int(sys.argv[3])

# get the org of the side class from side_class file
a = open(armfile, 'r').readlines()
b = open(armfile[:-4]+"bb.arm", "w")
nres, nend = -1, -1

for line in a:
    tmp = line.split()
    if nres == -1: nres = int(tmp[3])-int(tmp[2]) + 1
    if (nres != -1) and (nend == -1): nend = int(tmp[3])-int(tmp[2]) + 1
    b.write("    {0:>4d}       B{1}           {2:>5d}          {3:>5d}          {4:>4d}\n".format(
             int(tmp[0]), tmp[1], int(tmp[2]), int(tmp[3]), int(tmp[4])))

b.close()

pdb = open(pdbfile, 'r').readlines()
pdbn = open("star" + str(starno) + "wat_tor.pdb", "w")
rct = 0

for line in pdb:
    stn = line
    if " POX" in line:
        tmp = line.split()
        # First 3 heavy atoms are what we want
        if rct < 3 and "H POX" not in line: stn = line.replace("POX ", "BPOX") 
        rct += 1 # keeping track of heavy atom # in residue

        if rct == nres: rct = 0
    else: 
        stn = line    
        rct = 0

    pdbn.write(stn)
pdbn.close()

