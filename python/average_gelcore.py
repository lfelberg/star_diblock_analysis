# -*- coding: utf-8 -*-
"""
Created on Thu May 19 11:59:19 2016
@author: felb315

This file was used to take data from the gelcore ensemble and average it to
generate data for the set.
"""
import numpy as np

base = '/Users/lfelberg/star_diblock_copolymer/MATLAB/gelcore/'
files = ['star_all/anis_all.gel', 'star_all/rgyr_all.gel', \
         'star_all/voronoi.gel']
temps = [350, 400, 450]
gel_av = 13

rd_all = False

if rd_all:
  ''' For files in the star_all dir '''
  for file in files:
    ''' 
        Reading files and getting data to average
        across all of the gelcore systems
    '''
    print (file)
    lines = open(base+file).readlines()
    dat = [ [] for x in range(3)]
    for line in lines:
      info = [float(x) for x in line.split()]
      cttmp = 0
      for temp in temps:
        if info[1] == temp:
          dat[cttmp].append(info[2:])
        cttmp += 1
      
    cttmp = 0  
    for temp in temps:
      dat_mat = np.array(dat[cttmp])
      dat_avg = np.average(dat_mat, axis=0)
      pr_dat = (" ".join("{0:8.1f}".format(i) for i in dat_avg))
  
      print (str(gel_av), str(temp), pr_dat)
      cttmp += 1
    
files = [ 'scor', ]# 'perst_sum', 'scor',]
#stars = [ 1, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ]
stars = [ 1, 3, 4, 5, 6, 7, 8, 9, 12, 13, 14, 16, 17, 18, 20 ] # for his1

for file in files:
  ''' For stargaze files average '''
  stct = 0
  temps = [350] if file == 'perst_sum' else [350, 400, 450]
  dat = [ [[] for y in range(len(stars))] for x in range(len(temps))]
  g = '' if file == 'perst_sum' else 'g'
  for star in stars:
    tpct = 0
    for temp in temps:
      gelnam = str(star) if star > 9 else '0'+str(star)
      fname = 'starg'+gelnam+'/'+str(temp)+'Kw/stargaze/stargaze_star'\
               + g + gelnam + '_' + str(temp) + 'Kw.' + file
      lines = open(base+fname).readlines()
      for line in lines:
        if file == 'his1':
          info = [float(x) for x in line.split()[8:]]
        elif ((file == 'scor') and (float(line.split()[0]) == 0)) or \
           (file == 'perst_sum'):
          info = [float(x) for x in line.split()]
        else:
          continue
        dat[tpct][stct].append(info)
      tpct += 1
    stct += 1
  
  tpct = 0
  for temp in temps:
    outnam = 'stargaze_starg13_' + str(temp) + 'Kw.' + file
    ot = open( outnam, 'w')
    dat_mat = np.array(dat[tpct])
    dat_avg = np.average(dat_mat, axis=0)
    dat_dev = np.std(dat_mat, axis=0)
    print("Dev")
    print(dat_dev[0])
    print("Avg")
    print(dat_avg[0])
    print("")
    for i in range(len(dat_avg)):
      if file == 'his1':
        pre = 'Hist(    {0:>3d}) at R =   {1:>8.5f} rho = '.format( 
                        i+1, (0.2*i)+0.1)
        pr_dat = (" ".join("{0:8.4f}  {1:8.4f}".format(dat_avg[i][x], dat_dev[i][x]) 
                           for x in range(len(dat_avg[i]))))
        pr_dat = pre + pr_dat
      else:
        pr_dat = (" ".join("{0:8.4f}".format(i) for i in dat_avg[i]))
      ot.write(pr_dat+"\n")
    ot.close()
    tpct += 1
  
