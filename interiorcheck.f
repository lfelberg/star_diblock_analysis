c
c    program to setup inputs for voronoi program to
c    provide further analysis on interior clusters
c    the program reads cluster information from a cluster file
c    an input file for a voronoi analysis is produced along
c    with a set of subset files each of which contains a list
c    of the sites in one interior cluster
c
      character*256 fname,line,line1,line2,line3
      character*4 string
      dimension neighbors(1000),list(1000)

      call getarg(1,line)
      is=index(line,'cluster')
      if(is.eq.0) then
        write(*,'(''Error:  name of a *.cluster '',
     x    ''needed on command line '')')
        stop 'name of cluster file expected on command line'
      endif

      fname=line
      open(10,file=fname)
      open(11,file=fname(1:is-1)//'interior.vin')

      ifile=0
      isubset=0
10    continue
      read(10,'(a256)',end=500) line1
      ifile=ifile+1
      is=index(line1,' ')-1
      read(line1(is+1:),*) iconf
      write(*,'(''XYZ File '',i4,2x,a,'' Configuration '',i5)')
     x  ifile,line1(1:is),iconf
      read(10,'(a256)') line
      is=index(line,'clusters')+8
      read(line(is:),*) nclust
      write(*,'(''Number of interior clusters '',i5)') nclust
      do ic=1,nclust
        read(10,'(a256)') line
        is=index(line,'sites')+5
        read(line(is:),*) nsite
        write(*,'(''Number of sites in cluster '',i5,'' is '',i5)')
     x      ic,nsite
        read(10,*)
        do isite=1,nsite
          read(10,'(a256)') line
          is=index(line,'index')+5
          read(line(is:),* ) isindex   !site index
          list(isite)=isindex
          is=index(line,'neighbors')+9
          read(line(is:),* ) nneigh  !site index
          read(10,'(10i8)')   (neighbors(i),i=1,nneigh)
        enddo
        isubset=isubset+1
        write(string,'(i4.4)') isubset
        is=index(fname,'cluster')
        write(11,'(a)') line1(1:100)
        write(11,'(''distance cutoff 15.  '',
     x    ''classificationfile ../../setup/classification.classify '',
     x    ''subsetfile '',a,a,''.subset'')') fname(1:is-1),string
        write(11,'(''clusters '')')
        open(12,file=fname(1:is-1)//string//'.subset')
        write(12,'(i8)') nsite
        write(12,'(15i8)') (list(i),i=1,nsite)
        close(12)
      enddo
      go to 10


500   continue
      close(10)
      close(11)
      end
