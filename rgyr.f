c
c   program to extract Rgyr and shape data 
c   this data comes from a *.sprp file (e.g., xyz001149.sprp)
c   
c   compute averages rmsd, correlation time and uncertainties
c

      implicit real*8 (a-h,o-z)

      character*256 argline,line
      dimension data(6,100000),dat(100),facf(1000) 
      dimension out(4,4)

      ndatmx=100000
c     ndat=10  !appropriate for STAR3,4
c     ndat=12  !appropriate for STAR2
c     nshell=5 ! appropriate for STAR12
      nshell=3 ! appropriate for STAR25 and STAR3*
c     nshell=4 ! appropriate for gelcore
      
      call getarg(1,argline)

      is=index(argline,'sprp')
      if(is.eq.0) then
        stop 'Can only read .sprp filetype'
      else
        write(*,'(''Assuming input file is of type'',
     x  '' *.sprp'')')
        ifsprp=1   !flag for file type; =1 means *.sprp file from stargaze program
      endif

      is=index(argline,' ')-1
      open(10,file=argline(1:is))
      write(*,'(''Input file *'',a,''*'')')
     x    argline(1:is)

      nline=0


c  structure of sprp file
c234567890123456789012345678901234567890123456789012345678901234567890
c   2   40.0000   64.1543   47.0010   45.4918   50.1756    2.0320     .0004   15.0894     .1576   15.1584     .1599   16.8048     .2017
c   4   80.0000   65.0165   46.5144   48.3745   49.9586    2.0244     .0004   15.1170     .1636   15.1795     .1663   16.8586     .2182

c   structure of Shape lines from *.sout file
cConfig     2 Time       40.000 Frame     3 Mol indx      1 Rg    16.8048 Shape    55.1424   16.4212     .2017 E2E    66.2108   (sites   1907  2478) Max dx,dy,dz    47.0010   45.4918   50.1756


10    continue

      read(10,'(a256)',end=200) line
c     write(*,'(''Input line *'',a,''*'')') line(1:30)
      if(ifsprp.eq.0) then
        if(index(line,'Shape').eq.0) go to 10
        read(line,'(7x,i6,5x,f13.3)') iconfig,time
        is=index(line,'Rg')+2
        read(line(is:),*) rgyr
        is=index(line,'Shape')+5
        read(line(is:),*) shape1,shape2,shape3
      else
C        read(line,'(i5,20f10.4)') iconfig,time,(dat(i),i=1,ndat) 
        read(line, * ) iconfig,time,(dat(i),i=1,4+2*nshell) 
        rgyrt=dat(4+2*nshell-1)    !total Rg
        shapet=dat(4+2*nshell)**2  !total k**2
        rgyrc=dat(4+2*nshell-3)    !core  Rg
        shapec=dat(4+2*nshell-2)**2  !core  k**2
      endif

      if(time.gt.10000) then
        nline=nline+1
        data(1,nline)=time
        data(2,nline)=iconfig
        data(3,nline)=rgyrc   
        data(4,nline)=rgyrt
c       data(4,nline)=shape3  
        data(5,nline)=shapec
        data(6,nline)=shapet
      endif

      go to 10

200   continue

      Write(*,'(''Data points read in '',i5)') nline
 
      deltime=data(1,2)-data(1,1)
      nlag=200
      do iset=3,6
        call acf(data,nline,6,iset,facf,nlag,ave,rmsd)
        write(*,'(''Ndata '',i5,'' Row number '',i3,
     x  '' Total time '',f10.2)')
     x     nline,iset,nline*deltime
        write(*,'(''Ave/RMSD from ACF '',2f10.5)')
     x    ave,rmsd
        sum=0.d0
        do i=1,nlag
          if(facf(i+1).lt.0.d0) go to 300
          sum=sum+0.5d0*(facf(i)+facf(i+1))
          uncert=rmsd*dsqrt(2.d0*sum/nline)
          write(*,'('' FACF('',i3,2x,f10.4,'') = '',
     x      f10.4,'' Area(ps) '',f10.4,'' Uncert '',
     x      f10.4)')
     x      i,(i-1)*deltime,facf(i),deltime*sum,uncert
        enddo
300     continue
        out(1,iset-2)=ave
        out(2,iset-2)=rmsd
        out(3,iset-2)=deltime*sum/1000.d0 !convert to nanosec
        out(4,iset-2)=uncert
      enddo
      write(*,'('' Rgyr(ALL ) ave/rms/tau/unc :'',8f10.4)') 
     x    (out(j,2),j=1,4),
     x    (out(j,1),j=1,4)
      write(*,'('' Anis(ALL ) ave/rms/tau/unc :'',8f10.4)') 
     x    (out(j,4),j=1,4),
     x    (out(j,3),j=1,4)
      write(*,'('' Rgyr(tot,cor) for XLS :'',17f10.4)') 
     x   out(1,2),out(1,2)*dsqrt(5.d0/3.d0),(out(j,2),j=3,4),
     x   out(1,1),out(1,1)*dsqrt(5.d0/3.d0),(out(j,1),j=3,4),
     x   out(1,4),out(4,4), out(1,3),out(4,3)
      write(*,'('' Rgyr(tot,cor) ave/unc :'',4f10.4)') 
     x   out(1,2),out(4,2),
     x   out(1,1),out(4,1)
      write(*,'('' Anis(tot,cor) ave/unc :'',4f10.4)') 
     x   out(1,4),out(4,4),
     x   out(1,3),out(4,3)
      write(*,'('' Rgyr(tot) ave/rms/tau/unc :'',4f10.4)') 
     x     (out(m,2),m=1,4)
      write(*,'('' Rgyr(core ) ave/rms/tau/unc :'',4f10.4)')
     x    (out(j,1),j=1,4)
      write(*,'('' Anis(total) ave/rms/tau/unc :'',4f10.4)')
     x    (out(j,4),j=1,4)
      write(*,'('' Anis(core ) ave/rms/tau/unc :'',4f10.4)')
     x    (out(j,3),j=1,4)
  
      end
c
c     subroutine to compute facf
c
      subroutine acf(data,ndata,nrow,irow,facf,
     x  nlag,ave,rms)
      implicit real*8 (a-h,o-z)
      dimension data(nrow,ndata)
      dimension facf(nlag) 

      ave=0.d0
      do i=1,ndata
        ave=ave+data(irow,i)  
      enddo
      ave=ave/ndata
      rms=0.d0
      do i=1,ndata
        rms=rms+(data(irow,i)-ave)**2 
      enddo
      rms=dsqrt(rms/ndata)




      do ilag=0,nlag-1
        sum=0.d0
        do j=1,ndata-ilag
          sum=sum+
     x      (data(irow,j)-ave)*(data(irow,j+ilag)-ave)
        enddo
        facf(ilag+1)=sum/(ndata-ilag)
        facf(ilag+1)=facf(ilag+1)/(rms*rms)
      enddo

      end



