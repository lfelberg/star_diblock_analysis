c
c     program to read in a LAMMPS xyz file and apply a
c     centering operation to each frame so that a selected
c     molecule or site will be situated in the the center of the
c     cell
c
      implicit real*8 (a-h,o-z)
      character*80 fname
      character*120 line
      logical ifexist

c     specify the (contiguous) set of sites that will define the new center
c     note that this can be a single site, in 
c     merge:  1898 + 3*32001 = 1898 + 96003 = 97901
c     star2_tip4pew: 3*39304 + 1898 = 117912 + 1898 = 119810
c     ifirst=117913 !star2
c     ifirst=1      !merge

      ifirst=1   !default is that the first atomic site will be used for centering
      ifimage=1  !set to 1 to apply periodic imaging after translation

c
c     we assume the box is cubic, but it might not follow the convention
c     of -L/2 to +L/2 or of 0 to L.  I.e., it could have drifted somehow.
c
c     First, we estimate the bounding box from the coordinates based on 
c     x.y and z ranges.  
c     Second, the shift is applied to place the selected site
c     at the center of the box.  Imaging is done, which should leave the 
c     molecule in a single image, although it might be hanging out of
c     the box.
c     Third, the entire system is translated so that one corner of the box
c     is at the origin and all coordinates should be positive.
c

      call getarg(1,fname)
c     call lstchr(fname,is)
      is=index(fname,' ')-1
      inquire(file=fname(1:is),exist=ifexist)
      if(.not.ifexist) then
        write(*,'(''Invoke program with command line:'',
     x    '' ./center.exe star.xyz'' )')
        write(*,'(''The file name you entered does not exist: '',a)')
     x    fname(1:is)
        stop 'nonexistant file'
      endif 
      open(10,file=fname)
      write(*,'('' Input:  LAMMPS xyz file *'',a,''*'')') fname(1:is)

      is=index(fname,'.xyz')
      open(11,file=fname(1:is)//'c.xyz')
      write(*,'('' Output: LAMMPS xyz file *'',a,''*'')') 
     x  fname(1:is)//'c.xyz'

      nframe=0
10    continue   
      read(10,*,end=50) nsites
      nframe=nframe+1
      if(nframe.eq.1) write(*,'('' Number of sites '',i7)') nsites
      write(*,'('' Begin processing frame '',i3)') nframe
      read(10,'(a120)',end=50) line   !Atoms line
      read(10,'(a120)') line 
      read(line,*) itp,x,y,z
      xmn=x
      xmx=x
      ymn=y
      ymx=y
      zmn=z
      zmx=z
      if(ifirst.eq.1) then  !ifirst is the site index to be used for centering
        xsave=x
        ysave=y
        zsave=z
      endif
      do i=2,nsites
        read(10,'(a120)') line 
        read(line,*) itp,x,y,z
        xmn=min(xmn,x)
        xmx=max(xmx,x)
        ymn=min(ymn,y)
        ymx=max(ymx,y)
        zmn=min(zmn,z)
        zmx=max(zmx,z)
        if(ifirst.eq.i) then
          xsave=x
          ysave=y
          zsave=z
        endif
      enddo

      write(*,'('' Observed Coordinate x-range '',2f10.5,
     x    '' Delta '',f10.5)') 
     x    xmn,xmx,xmx-xmn
      write(*,'('' Observed coordinate y-range '',2f10.5,
     x    '' Delta '',f10.5)') 
     x    ymn,ymx,ymx-ymn
      write(*,'('' Observed coordinate z-range '',2f10.5,
     x    '' Delta '',f10.5)') 
     x    zmn,zmx,zmx-zmn

c     assume cubic, but not necessarily with 0 to L or -L/2 to +L/2
      edge=max(xmx-xmn,ymx-ymn,zmx-zmn)
      xmx=xmn+edge
      ymx=ymn+edge
      zmx=zmn+edge

c     assume cubic, and that box is either from 0 to L or -L/2 to +L/2
c      xmn=min(xmn,ymn,zmn)
c      xmx=max(xmx,ymx,zmx)
c      if(dabs(xmn).lt.1.0d0) then
c        xmn=0.d0
c        ymn=0.d0
c        zmn=0.d0
c        ymx=xmx 
c        zmx=xmx 
c      elseif(dabs(xmn+xmx).lt.1.0d0) then
c        xmx=max(xmx,-xmn)
c        xmn=-xmx
c        ymx=max(xmx,-xmn)
c        ymn=-xmx
c        zmx=max(xmx,-xmn)
c        zmn=-xmx
c      endif
c      edge=xmx-xmn

c     establish convention that coordinates go from 0 to L
      write(*,'('' Shifting coordinates to range from'',
     x  '' 0 to '',f10.5,'' with site '',i6,'' at center.'')')
     x  edge,ifirst  
      xmx=edge    
      xmn=0.d0

c     reposition file to first particle of this frame
      do i=1,nsites
        backspace(10)
      enddo

      xshft=(xmx+xmn)/2.d0 - xsave  !assume cubic
c      xshft=edge/2.d0 - xsave  !assume cubic
      yshft=(ymx+ymn)/2.d0 - ysave  !assume cubic
c      yshft=edge/2.d0 - ysave  !assume cubic
      zshft=(zmx+zmn)/2.d0 - zsave  !assume cubic
c      zshft=edge/2.d0 - zsave  !assume cubic
      write(11,'(i8)') nsites
      write(11,'(''Atoms'')') 
      do i=1,nsites
        read(10,'(a120)') line
        read(line,*) itp,x,y,z
        x=x+xshft
        y=y+yshft
        z=z+zshft
        if(ifimage.eq.1) then
          if(x.gt.xmx) x=x-(xmx-xmn)
          if(x.le.xmn) x=x+(xmx-xmn)
          if(y.gt.ymx) y=y-(ymx-ymn)
          if(y.le.ymn) y=y+(ymx-ymn)
          if(z.gt.zmx) z=z-(zmx-zmn)
          if(z.le.zmn) z=z+(zmx-zmn)
        endif
c       x=x-xmn
c       y=y-ymn
c       z=z-zmn

c       find nearest image of each site to the ifirst site
c       ifirst site is at (xsave,ysave,zsave)
c       x=x-xsave
c       if(x.gt. 0.5d0*(xmx-xmn)) x=x-(xmx-xmn)
c       if(x.le.-0.5d0*(xmx-xmn)) x=x+(xmx-xmn)
c       y=y-ysave
c       if(y.gt. 0.5d0*(ymx-ymn)) y=y-(ymx-ymn)
c       if(y.le.-0.5d0*(ymx-ymn)) y=y+(ymx-ymn)
c       z=z-zsave
c       if(z.gt. 0.5d0*(zmx-zmn)) z=z-(zmx-zmn)
c       if(z.le.-0.5d0*(zmx-zmn)) z=z+(zmx-zmn)
c       x=x+0.5d0*(xmx-xmn)
c       y=y+0.5d0*(ymx-ymn)
c       z=z+0.5d0*(zmx-zmn)

        write(11,'(i5,3f10.4)') itp,x,y,z
        if(i.eq.1) then
          x1mn=x
          x1mx=x
          y1mn=y
          y1mx=y
          z1mn=z
          z1mx=z
        else
          x1mn=min(x1mn,x)
          x1mx=max(x1mx,x)
          y1mn=min(y1mn,y)
          y1mx=max(y1mx,y)
          z1mn=min(z1mn,z)
          z1mx=max(z1mx,z)
        endif
      enddo
      write(*,'('' After shift  x-range        '',2f10.5,
     x    '' Delta '',f10.5)') 
     x    x1mn,x1mx,x1mx-x1mn
      write(*,'('' After shift  y-range        '',2f10.5,
     x    '' Delta '',f10.5)') 
     x    y1mn,y1mx,y1mx-y1mn
      write(*,'('' After shift  z-range        '',2f10.5,
     x    '' Delta '',f10.5)') 
     x    z1mn,z1mx,z1mx-z1mn


      go to 10
      



50    continue
      write(*,'('' Number of frames in this file '',i4)') nframe

      close(10)
      close(11)

      end
  



c
c     left justify a string
c
      subroutine leftjust(string)
      character*(*) string

      is=0
      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 10
        endif
      enddo

10    continue
      if(is.eq.0) return  !all blank string

      do i=1,len(string)-is+1
        string(i:i)=string(is+i-1:is+i-1)
      enddo
c     blank out the last is-1 characters
      do i=len(string)-is+2,len(string)
        string(i:i)=' '
      enddo

      end


c
c     subroutine lstchr returns the index of the last nonblank character in a string
c
      subroutine lstchr(string,ilst)
      character*(*) string 

      ilst=0  !if string is all blank, return zero
      do i=len(string),1,-1
        if(string(i:i).ne.' ') then
          ilst=i
          go to 10
        endif
      enddo
10    continue

      end
