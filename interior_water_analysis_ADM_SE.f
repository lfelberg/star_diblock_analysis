c
c
c Using this program to analyze surface distances for interior waters
c 5/24/2016 A.Carr
c
c     Program will look at each frame analyzed in a voronoi output file *.vout.
c     These frames may or may not have interior water molecules, but each
c     frame has equal weight when trying to compute average water loading.
c
c     Each frame has a corresponding analysis in a *.solute file with the same
c     name.  This gives the volumes of each site of the polymer as well as a list
c     of the solvent sites that are in contact with any of them, which includes 
c     both the frontier solvent sites and the interior solvent sites.
c     This file is produced from a voronoi analysis with the "solute" keyword
c     in line 2 of the input file.  Note that this should be with the exact same
c     sequence of files as the main *.vout file.
c
c     If interior water molecules are present, details about them will be found
c     in a corresponding voronoi output file *.interior.vout of the same name.
c     Note that there may be several interior water clusters, each with one or
c     more water molecules.  These *.interior files were produced after running
c     Lisa's scripts with produce one subset file for each cluster in each
c     configuration.
c
c     Using a classification file to identify the core sites, we can compute
c     the COG of the polymer once we have the coordinates.  
c
c     For the "height" analysis, we develop two histograms.  One is the number of
c     water molecules at each height interval above the COG.  The second is the
c     sum of the voronoi volumes of all the polymer sites in that height interval.
c     The ratio of these will be a local water number density as a function of 
c     height.  The hieght of a water molecule will be determined by its oxygen
c     coordinate (but could also be COG, or COM of the molecule).
c
c     For the depth analysis, we develop two histograms.  One is hte number of 
c     water molecules at each depth interval, where depth is the distance from the 
c     oxygen to the nearest frontier water oxygen.  (This could be the nearest 
c     pair of sites irrespective of whether they are oxygens or hydrogens, or it could
c     be distances to COGs.  Note that what we had was the distance from the COG of 
c     the water cluster to the nearest frontier site.  I don't think we wnat to use
c     this  but should discuss the options.)
c
c     Changes:
c
c
c
c     6/16/16:  read name of input file (*.wain) from command line
c               Structure of *.wain file:
c
c          1)   name of classification file (same for all subsequent analysis)
c               (classification file tells us which sites make up the core
c               since these will be used to compute the COG)
c
c          Multiple lines of the following:
c          2)   name of a *.vout file  (with the original voronoi output)
c               Note that we assume there will be *.solute.solute and *.interior.vout
c               files in the same directory as the *.vout file
c               These file names will be constructed
c
c     9/15/2016: changed to allow one to omit a few "trapped" waters whose indices
c     are listed on the command line.  These would be identified by running 
c     the program that gives the residence time correlation function.
c
c
c
      implicit real*8(a-h,o-z) 
      character*256 fname,line,xyzname,classfile,xyzcurrent
      character*20 classname(100)
      real*8 coord(3,200 000), hist(6,100)
      integer iclass(200 000), imask(200 000), isites(500)
      integer ncsize(200) !dimension to max num of solvent clusters in a polymer
      integer member(200,200) !dimension to max num of clusters by largest cluster
      real*8 volsolute(200 000)
      integer memcontact(200 000) !list of contacting solvent sites 
      integer memwatcontact(100 000) !list of contacting water molecules
      integer meminterior(2000) !list of interior solvent sites
      real*8 volinterior(2000) !volumes for the interior solvent sites
      integer iomit(100)  !omission list for trapped water
      natmx=200 000


      dhist=1.0d0     !histogram bin width
      nhismx=100      !set to dimension of hist (or less)
      nhiscall=0      !counter for number of times histogram taken
      nhiscallmx=10000  !target number of histogram takes
      do i=1,nhismx
      hist(1,i)=0.d0    !accumulate volumes by height of polymer elements
      hist(2,i)=0.d0    !accumulate volumes by height of interior water
      hist(3,i)=0.d0    !accumulate water molecule counts by height    
      hist(4,i)=0.d0    !accumulate volumes by depth of polymer elements
      hist(5,i)=0.d0    !accumulate volumes by depth of interior water
      hist(6,i)=0.d0    !accumulate water molecule counts by depth     
      enddo

c     get input file name (*.wain) from command line
      call getarg(1,fname)
      if(index(fname,'.wain').eq.0) then
        write(*,'(''Error:  expect file name on '',
     x    ''command line, *.wain'')')
        stop 'Error: expect input file name on command line'
      endif
      is=index(fname,' ')-1
c     open(5,file='test.wdin')
      open(5,file=fname(1:is))            !open input file
      open(6,file=fname(1:is-2)//'out')   !open output file




c     code to handle a trapped water omission list from the command line
      ifomit=0    !default is don't omit anything
      if(iargc().ge.4) then
        ifomit=1
        call getarg(3,line) !argument 3 is the number to omit
        read(line,*) numomit
        do i=1,numomit
          call getarg(i+3,line)
          read(line,*) iomit(i)
        enddo 
        write(*,'(''From command line, need to omit '',i2,
     x    '' water molecules with indices: '',20i7)') 
     x    numomit,(iomit(j),j=1,numomit)
      endif



c
c
      
c      averatio=0.d0
c      naveratio=0
c      timeave=40000.d0

      
c     open and read the classification file
      read(5,100) classfile
      write(6,'(''Classfile in use: '',a)')
     x  classfile(1:index(classfile,' ')-1)
      open(7,file=classfile(1:index(classfile,' ')-1))
      read(7,*)
      read(7,120) line
      read(line(1:),*) nclass
      do i=1,nclass
      read(7,100) line
      classname(i)=line(1:index(line,' ')-1)
      enddo
      read(7,*) n
      do i=1,n
      read(7,*) iclass(i)
      enddo
      close(7)
      nclasssites=n
      write(*,'(''Total number of sites (from *.classify) '',
     x  i8)') nclasssites


c     set a mask vector to identify which sites will
c     be used to compute the COG
      ifirst=0
      do i=1,n
      imask(i)=0
      if(iclass(i).eq.1) imask(i)=1   !hydrophobic (linker)
c      if(iclass(i).eq.2) imask(i)=1   !hydrophobic (arm)
      if(ifirst.eq.0.and.imask(i).eq.1) ifirst=i
      enddo


10    read(5,100,end=1000) fname  !name of a *.vout voronoi output file
      is=index(fname,' ')-1
100   format(a256)
      write(*,110) fname(1:is)
110   format('Main voronoi output filename ', a)
      open(10,file=fname(1:is))

c     these are the other files that will be used
      write(*,'(''Gelcore details from file    '',a)') 
     x  fname(1:index(fname(1:is),'.vout'))//'solute.solute'
      open(11,file=fname(1:index(fname(1:is),'.vout'))//'solute.solute',
     x  form='UNFORMATTED')
      write(*,'(''Interior water from file     '',a)') 
     x  fname(1:index(fname(1:is),'.vout'))//'interior.vout'
      open(12,file=fname(1:index(fname(1:is),'.vout'))//'interior.vout')

c     read(11,'(26x,i8)') nsolute !read the number of solute sites
      read(11) nsolute
      write(*,'(''Number of solute sites is '',i8)') nsolute


20    read(10,120,end=800) line   !skip to where the coordinate filename is mentioned
120   format(a256)
      if(index(line,'Input file line 1:').eq.0) go to 20
130   format(a80)
      is=index(line,':')+1
      is2=index(line(is:),' ')-1+(is-1)
c      write (*,131) line(is:is2)  !this substring holds the coordinate (*.xyz) file name
131   format('xyzname is ', a)
      xyzname=line(is:is2)
      read(line(is2+1:),*) iConfig
      is=index(line,'Time')+4
      read(line(is:),*) Time
c      write(*,132) Time,iConfig
132   format('Time is ', f15.4,' Config ',i10) 



c Read in the volume 
45    read(10,120,end=800) line
       if(index(line,'Cell dimensions taken from').eq.0) go to 45
      read(10,150,end=800) volume
150   format(8x,f20.10)
      write(*,151) volume
151   format('volume is ', f20.10)
      side=(volume)**(1.d0/3.d0)

c For sanity check get the volume of the solute from this run
47    read(10,'(a256)',end=800) line
      if(index(line,'Total volume for sites of type  1 is ').eq.0) 
     x  go to 47
      read(line(37:),*) vorovolume1  !this is the gelcore voronoi volume
      write(*,'(''Polymer volume '',f10.4)') vorovolume1

c Find the interior cluster information
49    read(10,'(a256)',end=800) line
      if(index(line,'distinguishable solvent clusters is ').eq.0) 
     x  go to 49
      read(line(46:),*) nclus
      write(*,'(''Number of solvent clusters is '',i4)') nclus
c     skip the first (bulk solvent) but read the rest
      do i=1,nclus
        read(10,'(30x,i7)',end=800) ncsize(i)
        write(*,'(''Size of cluster '',i3,'' is '',i8)') 
     x    i,ncsize(i)
      enddo
c Read the number of frontier solvent sites
51    read(10,'(a256)',end=800) line
      if(index(line,'frontier').eq.0) go to 51
      is=index(line,'sites')+5
      read(line(is:),*) nfront
      write(*,'(''Number of frontier solvent sites '',i8)') 
     x  nfront
      read(10,'(a256)',end=800) line
      read(10,'(a256)',end=800) line
      read(10,'(a256)',end=800) line
      read(10,'(a256)',end=800) line
      read(10,'(a256)',end=800) line

c Read in the members of the interior clusters 
      ninterior=0
      do i=2,nclus
        read(10,'(a256)',end=800) line
        read(10,'(a256)',end=800) line
        read(10,'(a256)',end=800) line
        read(10,'(10i8)',end=800) (member(i,j),j=1,ncsize(i))
        write(*,'(''Cluster '',i3,'' members '',20i7)')
     x    i,(member(i,j),j=1,ncsize(i))
        read(10,'(a256)',end=800) line
        ninterior=ninterior+ncsize(i)
      enddo
c Generate an unrolled list of the interior sites for use later
      k=0
      do i=2,nclus
        do j=1,ncsize(i)
          k=k+1
          meminterior(k)=member(i,j)
        enddo
      enddo



c
c     At this point we have all we need from the main voronoi analysis
c     Now read coorresponding information from the *.solute file
c     What is needed is the voronoi volume for each polymer site and
c     the identities of the contacting solvent sites.  These include interior
c     solvent, and note that some of the contacting solvent might just be a 
c     single H or O.

220   continue
c     read(11,'(a256)',end=900) line   !skip to where the coordinate filename is mentioned
      read(11,end=900) linelen         !skip to where the coordinate filename is mentioned
      line=' ' 
      read(11,end=900) line(1:linelen) !skip to where the coordinate filename is mentioned
      write(*,'(''Unformatted file data *'',a,''*  nchar '',i6)')
     x line(1:linelen),linelen
c     if(index(line,'Line(1):').eq.0) go to 220
c     is=index(line,':')+2
      is=1
      is2=index(line(is:),' ')-1+(is-1)
      write (*,'(''xyzname from solute file is '',a)') 
     x  line(is:is2)  !this substring holds the coordinate (*.xyz) file name
      read(line(is2+1:),*) iCfg
      if(xyzname(1:is2-is+1).ne.line(is:is2)
     x  .or.iconfig.ne.icfg) then
        write(*,'(''Error: mismatch in *.vout and *.solute analyses'')')
        write(*,'(''Coord file from *solute.solute *'',a,''*'',
     x    '' configuration '',i5)')
     x    line(is:is2),icfg
        write(*,'(''Coord file from       *.vout   *'',a,''*'',
     x    '' configureation '',i5)')
     x    xyzname(1:is2-is+1),iconfig
        stop 'Mismatch in *.vout and *.solute files'        
      endif
c     is=index(line,'Time')+4
c     read(line(is:),*) Time
c      write(*,132) Time,iConfig
c132   format('Time is ', f15.4,' Config ',i10) 

c Read the voronoi volume of the solute sites
      solutevol=0.d0
      do i=1,nsolute
c       read(11,'(43x,f10.4)',end=900) volsolute(i)
        read(11) idummy,volsolute(i)
        solutevol=solutevol+volsolute(i)
      enddo
      write(*,'(''Solute volume from main voronoi analysis  '',f15.4)')
     x  vorovolume1
      write(*,'(''Computed solute volume from *.solute file '',f15.4)')
     x  solutevol
c     read(11,'(17x,f15.5)',end=900) vorovolume2
      read(11) vorovolume2
      write(*,'(''Solute volume from *.solute file          '',f15.4)')
     x  vorovolume2
c     read(11,'(33x,i8)',end=900) nfront2     
c     read(11,'(10i8)',end=900) (memcontact(j),j=1,nfront2)
      read(11) nfront2     
      read(11) (memcontact(j),j=1,nfront2)

      write(*,'(''Number of frontier sites from *.solute file '',i8)') 
     x  nfront2
      write(*,'(''Number of frontier sites from main *.vout   '',i8)') 
     x  nfront 
      write(*,'(''Number of interior solvent sites            '',i8)')
     x  ninterior

c     look for interior solvent sites that are in the contacting solvent list
c     removing these leaves the frontier sites
      nfound=0
      do i=1,ninterior
        do j=1,nfront2
          if(memcontact(j).eq.meminterior(i)) then
            nfound=nfound+1
            memcontact(j)=0  !set this interior site index to zero
          endif
        enddo
      enddo
      write(*,'(''Number of interior solvent sites found '',
     x  ''in the contacting solvent site  list '',i5)') nfound

c
c     The list of frontier sites should be augmented to include all three
c     sites of each water molecule
c
c     replace each site index in memcontact with a water molecule index, 
c     then expand it
c
      nwat=(nclasssites-nsolute)/3
      write(*,'(''Total number of water molecules '',i6)') 
     x  nwat
      do i=1,nwat    
        memwatcontact(i)=0
      enddo
      do i=1,nfront2
        if(memcontact(i).ne.0) then
          iwat=1+(memcontact(i)-nsolute-1)/3
c         write(*,'(''Site index '',i8,
c    x      '' has water index '',i6)') memcontact(i),iwat
          memwatcontact(iwat)=1    
        endif
      enddo
      nwatcontact=0
      do i=1,nwat
        if(memwatcontact(i).eq.1) nwatcontact=nwatcontact+1
      enddo
      write(*,'(''Number of bulk water molecules in contact '',
     x  ''with polymer'',i6,'' with '',i8,'' sites from '',i8,
     x  '' frontier sites.'')') 
     x  nwatcontact,3*nwatcontact,nfront2
      nfront3=0
      do i=1,nwat
        if(memwatcontact(i).eq.1) then
          memcontact(nfront3+1)=1+3*(i-1)+nsolute
          memcontact(nfront3+2)=2+3*(i-1)+nsolute
          memcontact(nfront3+3)=3+3*(i-1)+nsolute
          nfront3=nfront3+3
        endif
      enddo
      write(*,'(''Size of augmented frontier site list '',i8)') 
     x  nfront3
      do i=1,21
        iwat=1+(memcontact(i)-nsolute-1)/3
        write(*,'(''Aug site list '',i2,'' water index '',i6,
     x    '' site index '',i8)') i,iwat,memcontact(i)
      enddo






c
c     At this point we have all we need from the main *.vout file and from the *.solute
c     file.  Now we read what is needed from the *.interior.vout file, which contains
c     volume information about the interior solvent sites.
c
c     each interior cluster is processed on a separate invocation of the voronoi program
c     I am hoping these are in the same order in which they were reported in the main
c     analysis.
c
c

c for each interior cluster
      k=0
      write(*,'('' This is nclus '', i5)'), nclus
      do iclus=2,nclus
c       Loop until you find the next "Line 1" entry in the *.interior.vout file
c       verify that it is correct, look for the volumes of the interior sites
73      read(12,'(a256)',end=950) line
        if(index(line,'Input file line 1:').eq.0) go to 73
        is1=index(line,':')+1
        is2=index(line(is1:),' ')+is1-2
        read(line(is2+1:),*) icfg
        if(line(is1:is2).ne.xyzname(1:is2-is1+1)
     x    .or.icfg.ne.iconfig) then
          write(*,'(''Error: mismatch between main and '',
     x      ''interior vout files'')')
          stop 'Error: mismatch between main *.vout, *.interior.vout'
        endif
c       find the volumes of the sites for this cluster
75      read(12,'(a256)',end=950) line
        if(index(line,'Subset site  ').eq.0) go to 75
        backspace(12)
        do j=1,ncsize(iclus)
          read(12,'(27x,i8,8x,f10.4)',end=950) intindex,volumeint
c         check that the index is what is expected
          if(member(iclus,j).ne.intindex) then
            write(*,'(''Error: interior solvent index '',
     x        ''not as expected'')')
            write(*,'(''from main: '',i20,'' from interior: '',i8)')
     x        member(iclus,j),intindex
            stop 'Error:  interior solvent index mismatch'
          endif
          write(*,'(''Interior cluster '',i2,'' Site '',
     x       i2,'' Index '',i8,'' Volume '',f10.4)') 
     x       iclus,j,intindex,volumeint
          k=k+1
          volinterior(k)=volumeint
        enddo
      enddo

79    continue  !special case for when interior file is still being built 

      do i=1,ninterior
        write(*,'(''Interior site '',i3,'' index '',i8,
     x    '' volume '',f15.5)')
     x    i,meminterior(i),volinterior(i)
      enddo













c Now get the coordinates from the xyz using subroutine getcoord 
      write(6,'(''Reading coordinates from '',a,'' config '',i5)') 
     x  xyzname(1:index(xyzname,' ')-1), iconfig
      nframe=iConfig      
      call getcoord(xyzname,nsites,coord,natmx,nframe)
c     nsites=n  !remember to remove after uncommenting call to getcoord
      if(nsites.ne.n) then
        write(*,'(''Site number mismatch'', 2i10)') n, nsites
        stop 'Error: site number mismatch error'
      endif

c
c     at this point we have enough info to compute the location of the 
c     COG of the core 
c     house keeping:  
c       compute location of COG of core
c       shift so that COG is the center of the box
c       make consistent image for each water molecule (H sites close to their O)
c       verify that after doing this we have an in tact version of the polymer (not sure how)
c
c


c translate all coordinates so that first atom of core is at the origin
c after doing this we hope that all core sites are within half a box length of the 
c first site, allowing the subsequent computation of the COG to work
      xtrans=coord(1,ifirst)      !ifirst is the index of the first atom of the core
      ytrans=coord(2,ifirst)      !it is from looking at the classificaition file.
      ztrans=coord(3,ifirst)        
      do i=1,nsites
      coord(1,i)=coord(1,i)-xtrans
      coord(2,i)=coord(2,i)-ytrans
      coord(3,i)=coord(3,i)-ztrans
      if(coord(1,i).gt.0.5d0*side) coord(1,i)=coord(1,i)-side
      if(coord(1,i).lt.-0.5d0*side) coord(1,i)=coord(1,i)+side
      if(coord(2,i).gt.0.5d0*side) coord(2,i)=coord(2,i)-side
      if(coord(2,i).lt.-0.5d0*side) coord(2,i)=coord(2,i)+side
      if(coord(3,i).gt.0.5d0*side) coord(3,i)=coord(3,i)-side
      if(coord(3,i).lt.-0.5d0*side) coord(3,i)=coord(3,i)+side 
      enddo

c Compute center of geometry of the core
      xcog=0
      ycog=0
      zcog=0
      ncog=0
      do i=1,nsites
       if(imask(i).eq.1) then
       xcog=xcog+coord(1,i)
       ycog=ycog+coord(2,i)
       zcog=zcog+coord(3,i)
       ncog=ncog+1 
       endif
      enddo
      xcog=xcog/ncog
      ycog=ycog/ncog
      zcog=zcog/ncog
c      write(*,'(''core coords are '', 3f10.4)') xcog,ycog,zcog

c translate all coordinates so that the center of geometry of the core is at the origin
      do i=1,nsites
      coord(1,i)=coord(1,i)-xcog
      coord(2,i)=coord(2,i)-ycog
      coord(3,i)=coord(3,i)-zcog
      if(coord(1,i).gt.0.5d0*side) coord(1,i)=coord(1,i)-side
      if(coord(1,i).lt.-0.5d0*side) coord(1,i)=coord(1,i)+side
      if(coord(2,i).gt.0.5d0*side) coord(2,i)=coord(2,i)-side
      if(coord(2,i).lt.-0.5d0*side) coord(2,i)=coord(2,i)+side
      if(coord(3,i).gt.0.5d0*side) coord(3,i)=coord(3,i)-side
      if(coord(3,i).lt.-0.5d0*side) coord(3,i)=coord(3,i)+side 
      enddo
   
c
c  we are hoping the polymer is "in tact".  A simple test is to see if 
c  any site is within 2A of a bounding edge of the box; if so print a warning and stop
      do i=1,nsolute
        iflag=0 
        if(dabs(coord(1,i)).gt.0.5d0*side-2.d0) iflag=1
        if(dabs(coord(2,i)).gt.0.5d0*side-2.d0) iflag=1
        if(dabs(coord(3,i)).gt.0.5d0*side-2.d0) iflag=1
        if(iflag.eq.1)  then
          write(*,'(''Error: Polymer site '',i8,
     x      '' has coords close to box face after shifting COG '',
     x      3f10.4)') i,coord(1,i),coord(2,i),coord(3,i)
c         stop 'might not have intact construction of polymer'
        endif
      enddo
c

c

c     once we have the list of frontier sites, we can get a depth (distance to nearest
c     frontier water oxygen site) as well as a height (above
c     the COG) for each polymer site and for each interior water site
      do i=1,nsolute
        height=dsqrt(coord(1,i)**2+coord(2,i)**2+coord(3,i)**2)
        depth=1000.d0                 
        idepth=0
        do j=1,nfront3,3   !skip 3 to measure distance to nearest oxygen
          k=memcontact(j)
          dx=coord(1,i)-coord(1,k)
          if(dx.ge. 0.5d0*side) then dx=dx-side
          if(dx.lt.-0.5d0*side) then dx=dx+side
          dy=coord(2,i)-coord(2,k)
          if(dy.ge. 0.5d0*side) then dy=dy-side
          if(dy.lt.-0.5d0*side) then dy=dy+side
          dz=coord(3,i)-coord(3,k)
          if(dz.ge. 0.5d0*side) then dz=dz-side
          if(dz.lt.-0.5d0*side) then dz=dz+side
          d=dsqrt(dx**2+dy**2+dz**2)
          if(d.lt.depth) then
            depth=d     
            idepth=k
          endif
        enddo
        iwat=1+(idepth-nsolute-1)/3
        itype=idepth-nsolute - 3*(iwat-1)
c       write(*,'(''Solute index '',i5,'' H '',f10.4,
c    x    '' D '',f10.4,'' V '',f10.4,
c    x    '' closest water '',i6,'' type '',i1)') 
c    x    i,height,depth,volsolute(i),iwat,itype
        nbin=1+int(height/dhist)
        nbin=min(nbin,nhismx)
        hist(1,nbin)=hist(1,nbin)+volsolute(i)
        nbin=1+int(depth/dhist)
        nbin=min(nbin,nhismx)
        hist(4,nbin)=hist(4,nbin)+volsolute(i)
      enddo


c     now get the depth and height for each interior solvent site
      do i=1,ninterior,3 !skip 3 to measure distance to nearest oxygen
        j=meminterior(i)  !following code makes sure we use the closest image of the solvent
        dx=coord(1,j)     !probably don't really have to do this
        if(dx.ge. 0.5d0*side) then dx=dx-side
        if(dx.lt.-0.5d0*side) then dx=dx+side
        dy=coord(2,j)
        if(dy.ge. 0.5d0*side) then dy=dy-side
        if(dy.lt.-0.5d0*side) then dy=dy+side
        dz=coord(3,j)
        if(dz.ge. 0.5d0*side) then dz=dz-side
        if(dz.lt.-0.5d0*side) then dz=dz+side
        height=dsqrt(dx**2+dy**2+dz**2)
        depth=1000.d0                 
        idepth=0
        do k=1,nfront3,3   !skip 3 to measure distance to nearest oxygen
          l=memcontact(k)
          dx=coord(1,j)-coord(1,l)
          if(dx.ge. 0.5d0*side) then dx=dx-side
          if(dx.lt.-0.5d0*side) then dx=dx+side
          dy=coord(2,j)-coord(2,l)
          if(dy.ge. 0.5d0*side) then dy=dy-side
          if(dy.lt.-0.5d0*side) then dy=dy+side
          dz=coord(3,j)-coord(3,l)
          if(dz.ge. 0.5d0*side) then dz=dz-side
          if(dz.lt.-0.5d0*side) then dz=dz+side
          d=dsqrt(dx**2+dy**2+dz**2)
          if(d.lt.depth) then
            depth=d     
            idepth=l
          endif
        enddo
        iwat=1+(idepth-nsolute-1)/3
        itype=idepth-nsolute - 3*(iwat-1)
        write(*,'(''Interior solvent '',i3,'' index '',i6,
     x    '' H '',f10.4,
     x    '' D '',f10.4,'' V '',f10.4,
     x    '' closest water '',i6,'' type '',i1)') 
     x    i,j,height,depth,volinterior(i),iwat,itype


c       code to check if this is a trapped water 
        ifremove=0  !default is don't remove this water
        if(ifomit.eq.1) then
          iwatindex=1+(j-nsolute-1)/3
          do j=1,numomit
            if(iwatindex.eq.iomit(j)) ifremove=1
          enddo
        endif



c       oxygen
        nbinh=1+int(height/dhist)
        nbinh=min(nbinh,nhismx)
        nbind=1+int(depth/dhist)
        nbind=min(nbind,nhismx)
        if(ifremove.eq.0) then
          hist(2,nbinh)=hist(2,nbinh)+volinterior(i)
          hist(3,nbinh)=hist(3,nbinh)+1.d0   !water is "located" based on oxygen location        
          hist(5,nbind)=hist(5,nbind)+volinterior(i)
          hist(6,nbind)=hist(6,nbind)+1.d0   !water is "located" based on oxygen location        
        else
          hist(1,nbinh)=hist(1,nbinh)+volinterior(i)
          hist(4,nbind)=hist(4,nbind)+volinterior(i)
        endif

c       first hydrogen
        j=meminterior(i+1)!following code makes sure we use the closest image of the solvent
        dx=coord(1,j)     !probably don't really have to do this
        if(dx.ge. 0.5d0*side) then dx=dx-side
        if(dx.lt.-0.5d0*side) then dx=dx+side
        dy=coord(2,j)
        if(dy.ge. 0.5d0*side) then dy=dy-side
        if(dy.lt.-0.5d0*side) then dy=dy+side
        dz=coord(3,j)
        if(dz.ge. 0.5d0*side) then dz=dz-side
        if(dz.lt.-0.5d0*side) then dz=dz+side
        height=dsqrt(dx**2+dy**2+dz**2)
        depth=1000.d0                 
        idepth=0
        do k=1,nfront3,3   !skip 3 to measure distance to nearest oxygen
          l=memcontact(k)
          dx=coord(1,j)-coord(1,l)
          if(dx.ge. 0.5d0*side) then dx=dx-side
          if(dx.lt.-0.5d0*side) then dx=dx+side
          dy=coord(2,j)-coord(2,l)
          if(dy.ge. 0.5d0*side) then dy=dy-side
          if(dy.lt.-0.5d0*side) then dy=dy+side
          dz=coord(3,j)-coord(3,l)
          if(dz.ge. 0.5d0*side) then dz=dz-side
          if(dz.lt.-0.5d0*side) then dz=dz+side
          d=dsqrt(dx**2+dy**2+dz**2)
          if(d.lt.depth) then
            depth=d     
            idepth=l
          endif
        enddo
        nbinh=1+int(height/dhist)
        nbinh=min(nbinh,nhismx)
        nbind=1+int(depth/dhist)
        nbind=min(nbind,nhismx)
        if(ifremove.eq.0) then
          hist(2,nbinh)=hist(2,nbinh)+volinterior(i+1)
          hist(5,nbind)=hist(5,nbind)+volinterior(i+1)
        else
          hist(1,nbinh)=hist(1,nbinh)+volinterior(i+1)
          hist(4,nbind)=hist(4,nbind)+volinterior(i+1)
        endif

c       second hydrogen
        j=meminterior(i+2)!following code makes sure we use the closest image of the solvent
        dx=coord(1,j)     !probably don't really have to do this
        if(dx.ge. 0.5d0*side) then dx=dx-side
        if(dx.lt.-0.5d0*side) then dx=dx+side
        dy=coord(2,j)
        if(dy.ge. 0.5d0*side) then dy=dy-side
        if(dy.lt.-0.5d0*side) then dy=dy+side
        dz=coord(3,j)
        if(dz.ge. 0.5d0*side) then dz=dz-side
        if(dz.lt.-0.5d0*side) then dz=dz+side
        height=dsqrt(dx**2+dy**2+dz**2)
        depth=1000.d0                 
        idepth=0
        do k=1,nfront3,3   !skip 3 to measure distance to nearest oxygen
          l=memcontact(k)
          dx=coord(1,j)-coord(1,l)
          if(dx.ge. 0.5d0*side) then dx=dx-side
          if(dx.lt.-0.5d0*side) then dx=dx+side
          dy=coord(2,j)-coord(2,l)
          if(dy.ge. 0.5d0*side) then dy=dy-side
          if(dy.lt.-0.5d0*side) then dy=dy+side
          dz=coord(3,j)-coord(3,l)
          if(dz.ge. 0.5d0*side) then dz=dz-side
          if(dz.lt.-0.5d0*side) then dz=dz+side
          d=dsqrt(dx**2+dy**2+dz**2)
          if(d.lt.depth) then
            depth=d     
            idepth=l
          endif
        enddo
        nbinh=1+int(height/dhist)
        nbinh=min(nbinh,nhismx)
        nbind=1+int(depth/dhist)
        nbind=min(nbind,nhismx)
        if(ifremove.eq.0) then
          hist(2,nbinh)=hist(2,nbinh)+volinterior(i+2)
          hist(5,nbind)=hist(5,nbind)+volinterior(i+2)
        else
          hist(1,nbinh)=hist(1,nbinh)+volinterior(i+2)
          hist(4,nbind)=hist(4,nbind)+volinterior(i+2)
        endif
      enddo

      nhiscall=nhiscall+1  !increment the histogram call counter
      if(nhiscall.ge.nhiscallmx) go to 900



c Loop over all the interior clusters that are in this configuration
c Read in the sites of the interior clusters from the voronoi interior output file
cc30    read(10,120) line
c        if(index(line,'Indices will be obtained from').eq.0) go to 30
c      ncount=0
c35    continue
c      read(10,120) line
c        if(index(line,'Site indices from subset').eq.0) go to 40 
c      nfield=0
c37    continue
c        if(line(32+8*nfield:31+8*(nfield+1)).eq.'        ') go to 39
c      nfield=nfield+1
c      ncount=ncount+1
c      read(line(32+(nfield-1)*8:31+8*nfield),'(i8)') isites(ncount)
c      go to 37
c39    continue
c      go to 35
c40    continue
c




c      write(*,133) ncount
c133   format('Number of sites found is ', i5) 
c      write(*,134) (isites(j),j=1,ncount)
c 134   format(20i8)



c Calculate the center of geometry of the interior water cluster
c      xcogclus=0
c      ycogclus=0
c      zcogclus=0
c      do i=1,ncount
c       j=isites(i)
cc       write(*,'(''i and j are '', 2i10)') i,j
c       xcogclus=xcogclus+coord(1,isites(i))
c       ycogclus=ycogclus+coord(2,isites(i))
c       zcogclus=zcogclus+coord(3,isites(i))
c      enddo 
c      xcogclus=xcogclus/ncount
c      ycogclus=ycogclus/ncount
c      zcogclus=zcogclus/ncount
cc      write(*,'(''clus coords are '', 3f10.4)')
cc     x xcogclus,ycogclus,zcogclus
c
c      distance=sqrt(xcogclus**2+ycogclus**2
c     x +zcogclus**2) 
c      write(*,'(''time, clustsize, distance are '', 
c     x f10.4, i10, f10.4)') Time, ncount, distance
c
c
c      nbin=1+distance/dhist
c      nbin=min(nbin,nhismx)
c      hist(1,nbin)=hist(1,nbin)+1.d0

c Calcuate depth of oxygen atoms in the cluster
c      do i=1,ncount,3
c      j=isites(i)
c      distance=sqrt(coord(1,j)**2+coord(2,j)**2+coord(3,j)**2) 
c      write(*,'(''distance to water '', i3, 5x, f10.4)')
c     x 1+(i-1)/3, distance 
c      nbin=1+distance/dhist
c      nbin=min(nbin,nhismx)
c      hist(2,nbin)=hist(2,nbin)+1.d0
c      enddo
c

      go to 20    !go back to *.voro file to find the next coordinate file

800   continue    !branch to here when done with current *.voro file
      close(10)   !close the *.voro file
      close(11)   !close the *.solute file
      close(12)   !close the *.interior file
      go to 10    !go to top and read the name of the next *.voro file from *.wain


1000  continue   !branch to here when done (EOF) reading *.wain file
      close(5)   !close the *.wain file





      xn3=0
      xn6=0
      do i=1,nhismx
      xn3=xn3+hist(3,i)
      xn6=xn6+hist(6,i)
      enddo


      write(*,'(''After '',i5,'' entries to histograms'')')
     x  nhiscall
      write(*,'(''Water load from COG      '',f10.5)')  xn3/nhiscall
      write(*,'(''Water load from surface  '',f10.5)')  xn6/nhiscall
      rhowat=0.033274d0  
c     density of TIP4PEw water at 298K
       do i=1,nhismx
      write(*,'(''R '', f6.2, 10f15.5)')
     x  0.5*dhist+(i-1)*dhist, 
     x  hist(1,i)/nhiscall, 
     x  hist(2,i)/nhiscall,
     x  hist(3,i)/nhiscall,
     x  (hist(3,i)/(max(0.01d0,hist(1,i)+hist(2,i))))/rhowat,
     x  (dsqrt(hist(3,i))/(max(0.01d0,hist(1,i)+hist(2,i))))/
     x   rhowat,
     x  hist(4,i)/nhiscall,
     x  hist(5,i)/nhiscall,
     x  hist(6,i)/nhiscall,
     x  (hist(6,i)/(max(0.01d0,hist(4,i)+hist(5,i))))/rhowat,
     x  (dsqrt(hist(6,i))/(max(0.01d0,hist(4,i)+hist(5,i))))/
     x   rhowat
      enddo

      go to 5000


c     some error handling 
900   write(*,'(''Error reading the *solute file '')')
      stop 'Error reading solute file'
950   write(*,'(''Error reading the *interior file '')')
      stop 'Error reading interior file'



5000  continue   !path to normal termination

      end



c     subroutine to read a set of coordinates from an xyz file
c     expect this to have a set of coords, we are usually interested
c     in either the first or the last
c     read in the frame number given by nframe
c
      subroutine getcoord(fname,nsites,coord,natmx,nframe)
      implicit real*8 (a-h,o-z)
      character*120 line
      character*(*) fname
      dimension coord(3,natmx)

c      write(*,'('' Reading coordinates from file *'',a,''*'')')
c     x     fname(1:40)

      open(31,file=fname)
 
      read(31,*) nsites
      rewind(31)

c      write(*,1000) nsites
1000  format('nsites ',i10)

c
c     file format is
c     line 1: number
c     line 2: Atoms
c     line 3,... for each atom a type and three coords
c     this is repeated for as many frames
c
c


        iframe=nframe  !this takes the last frame
c       write(*,'('' Reading frame '',i5)') iframe
c       position file to right place
        do i=1,(2+nsites)*(iframe-1)
          read(31,'(a120)') line
        enddo
c       now read into coord
        read(31,'(a120)') line
        read(31,'(a120)') line
        do i=1,nsites
          read(31,'(a120)') line
          read(line,*) it,(coord(k,i),k=1,3)
        enddo

      close(31)



      end



