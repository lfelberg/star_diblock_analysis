c
c Using this program to analyze the surface-area-to-volume ratio 
c for star polymers core+hydrophobic region
c 5/24/2016 A.Carr and Bill
c
      implicit real*8(a-h,o-z) 
      character*256 inname, fname,line,xyzname

      averatio=0.d0
      naveratio=0
      timeave=10000.d0

      call getarg(1,inname)
      open(5,file=inname)
10    read(5,100,end=900)fname
      is=index(fname,' ')-1
100   format(a256)
      write(*,110) fname(1:is)
110   format('voronoi filename ', a)
      open(10,file=fname(1:is))

20    read(10,120,end=800) line
120   format(a256)
        if(index(line,'Input file line 1:').eq.0) go to 20
130   format(a80)
      is=index(line,':')+1
      is2=index(line(is:),' ')-1+(is-1)
c      write (*,131) line(is:is2)
131   format('xyzname is ', a)
      xyzname=line(is:is2)
      read(line(is2+1:),*) iConfig
      is=index(line,'Time')+4
      read(line(is:),*) Time
c      write(*,132) Time,iConfig
132   format('Time is ', f15.4,' Config ',i10) 


30    read(10,120) line
        if(index(line,'based on classification file').eq.0) go to 30
      read(10,133) v1, v2, v3
c     v5,v6
133   format(36x,f20.6)      
c     write(*,134) v1+v2
134   format('volumes are ',5f15.4)
      read(10,*)
      read(10,*)
      read(10,*)
      read(10,135) a12
      read(10,135) a13
c     read(10,135) a14
c     read(10,135) a15
c     read(10,135) a16
      read(10,135) a23
c     read(10,135) a24
c     read(10,135) a25
c     read(10,135) a26
c     read(10,135) a34
c     read(10,135) a35
c     read(10,135) a36
c     read(10,135) a45
c     read(10,135) a46
c     read(10,135) a56
135   format(45x,f20.6)
c      write(*,136) a12, a14, a15  
136   format('LAC Areas ',3f20.6)
c     write(*,137) a12, a13, a23
137   format('PDV Areas ',3f20.6)
      
      pi=4.d0*datan(1.0d0)

cccccccccccccccccc
c Addition section
cccccccccccccccccc

c     for gelcore
c     aideal=4.d0*pi*((v1+v2)*3.d0/(4.d0*pi))**(2.d0/3.d0)     
c     aactual=a13+a14+a15+a23+a24+a25     

c     for the star8
      aideal=4.d0*pi*((v1)*3.d0/(4.d0*pi))**(2.d0/3.d0)     
      aactual=a12+a13

c     for dendrimer star (6 groups)
c     aideal=4.d0*pi*((v1+v2+v3)*3.d0/(4.d0*pi))**(2.d0/3.d0)     
c     aactual=a14+a15+a16+a24+a25+a26+a34+a35+a36

c     for STAR25 = Gelcore+PEG (4 groups)
c     aideal=4.d0*pi*((v1)*3.d0/(4.d0*pi))**(2.d0/3.d0)     
c     aactual=a12+a13+a14


c     write(*,140) Time, aactual/aideal
c140   format('time,  ratio ',4f15.4)
      write(*,140) Time, aactual/aideal, a12, a13, a23, v1, v2, v3
140   format('time,  ratio ',10f15.4)

      if(time.ge.timeave) then
        naveratio=naveratio+1
c       averatio=averatio+aactual/aideal
c       averatio=averatio+a13+a14+a23+a24
        averatio=averatio+v1+v2
      endif

      go to 20
800   continue    
      close(10)
      go to 10
900   continue
      close(5)

      write(*,905) averatio/naveratio, Time-timeave
905   format('avg surf ratio ',f10.4,'avgd over ',f10.4)
     

      end
