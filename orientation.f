c
c     program to read a file *.ori produced by stargaze.f program
c     to analyze the orientational correlation of the residue vectors
c
c
c

      implicit real*8 (a-h,o-z)
      character*130 line,argstring
      dimension orient(7,1500000)

      dimension iresidue(1500000),ichain(1500000),isequence(1500000)
      integer chain(5,100)
      dimension buffer(3,200000),corel(300)
      dimension csave(200,50)   !correlation functions for 200 lag times, 20 chains
      dimension caver(200,60)   !averaged correlation functions for 200 lag times, 30 sequences

      maxdat=1500000


      write(*,'(''Program to analyze orientational history'')')
c     open(10,file='stargaze.ori')
      call getarg(1,argstring)
      is=index(argstring,' ')-1
      write(*,'(''Orientation history input file '',a)') argstring(1:is)
      is1=index(argstring(1:is),'sori')
      if(is1.eq.0) then
        write(*,'(''Expecting file name with type *.sori'')')
        stop 'incorrect input file type'
      endif
      open(10,file=argstring(1:is))
      open(15,file=argstring(1:is1-1)//'scor')

c
c     sample of *.ori file
c     records are a frame number, time, residue number, chain number, sequence of residue in chain,
c     normalized orientation vector in frame of molecule (3-vector)
c     distance from reference frame center to center of residue,
c     vector from center to residue in fram of molecule (3-vector)
c     here, the orientation vector goes from the first residue site to the last nonhydrogen one
c     and the center of the residue is at the midpoint between these two sites
c
c   2     39305    0    0    -.9897     .1429     .0108     .3181    -.1064     .2989     .0226
c   2     39306    1    1     .6729    -.7358     .0768    3.9116    3.6353   -1.0635    -.9769
c   2     39307    1    2     .3162     .7901    -.5251    6.9257    5.1828     .1645   -4.5909

      nrec=0    !number of records
      nconf=1   !number of configurations

10    continue
      read(10,'(a120)',end=100) line
      nrec=nrec+1
      if(nrec.gt.maxdat) then
        write(*,'(''Out of storage for data '')')
        stop 'increase array size'
      endif
c
c     need to figure out the time spacing between successive configurations
c     how many configurations, how many chains, how many residues per chain
c     so can locate data for an arbitrary time/residue in the orient array
c
      read(line,*)  icon,time,ires,ich,iseq,(orient(j,nrec),j=1,7)

      if(nrec.eq.1) then
        icon0=icon  !configuration number of current block of records
        time0=time  !time stamp for first frame read in
        nconf0=icon !first configuration number
      endif

c      write(*,'(''Test print1'')')

      if(icon.ne.icon0) then  !have encountered a new block of records
        if(nconf.eq.1) then
          idconf=icon-icon0  !establish interval between conformations
          deltime=time-time0 !establish time interval between conformations
          iblock=nrec-1      !save number of records per time block
        endif
        nconf=nconf+1       !running count of number of conformations 
        icon0=icon
      endif

      if(nconf.eq.1) then
        iresidue(nrec)=ires
        ichain(nrec)=ich
        isequence(nrec)=iseq
      endif

      go to 10

100   continue

      write(*,'('' Total number of records in input file '',
     x   i8)') nrec

      write(*,'('' Starting configuration index = '',i5,/,
     x          '' Delta configuration index    = '',i5,/,
     x          '' Number of configurations     = '',i5)')
     x    nconf0,idconf,nconf
      write(*,'('' Starting time                = '',f10.2,/,
     x          '' Time between conifurations   = '',f10.2)')
     x   time0,deltime

      write(*,'('' Number of records per conf   = '',i5)')
     x  iblock

      close(10)


c
c     look in iresidue,ichain,isequence arrays for 
c     find number of chains, number of residues per chain
c
c     chain(5,100)
c     chain(1,i) pointer to first record of chain i 
c     chain(2,i) pointer to last record of chain i 
c     chain(3,i) label of chain i (e.g., 0,1,2...16)  
c
      nchain=1
      chain(1,1)=1
      chain(3,1)=ichain(1)
      do i=2,iblock
        if(ichain(i).ne.chain(3,nchain)) then
          chain(2,nchain)=i-1
          nchain=nchain+1 
          chain(1,nchain)=i
          chain(3,nchain)=ichain(i)
        endif
      enddo
      chain(2,nchain)=iblock

      write(*,'('' Number of chains identified is '',i5)')
     x  nchain
      maxlen=1
      do i=1,nchain
        write(*,'('' Chain '',i2,'' (label is '',i2,'')'')')
     x    i,chain(3,i)
        do j=chain(1,i),chain(2,i)
          write(*,'('' Residue number '',i3,'' (label is '',
     x      i10,'')'')') j-chain(1,i)+1,iresidue(j)
        enddo
        if(i.ge.2) then
          maxlen=max(maxlen,(chain(2,i)-chain(1,i)+1))
        endif
      enddo


      ncor=600   !number of points to evaluate the correlation function
      do iseq=1,maxlen
        do i=1,ncor
          do j=1,nchain
            csave(i,j)=0.d0  !clear correlation function storage
          enddo
        enddo
        do ich=1,nchain
          if(iseq.le.(chain(2,ich)-chain(1,ich)+1)) then
c           compute correlation function for residue number iseq of chain ich
            write(*,'('' Compute correlation function for residue '',
     x        i2,'' of chain '',i2,
     x        '' (labelled residue '',i6,'' chain '',i3,'')'')') 
     x        iseq,ich,  iresidue(iseq-1+chain(1,ich)),chain(3,ich)
            do idat=1,nconf
c             compute time, find data record
              itime=nconf0+(idat-1)*idconf
              time=time0+deltime*(idat-1)
              irec=((idat-1)*iblock)+(chain(1,ich)-1)+iseq
              buffer(1,idat)=orient(1,irec)
              buffer(2,idat)=orient(2,irec)
              buffer(3,idat)=orient(3,irec)
            enddo
            call corfun(nconf,buffer,ncor,csave(1,ich))
          endif
        enddo !end of loop over chains for a given sequence
        do i=1,ncor
          sum=0.d0
          ncount=0
          do ich=1,nchain
            if(iseq.le.(chain(2,ich)-chain(1,ich)+1)) then
              sum=sum+csave(i,ich)
              ncount=ncount+1
            endif
          enddo
          caver(i,iseq)=sum/ncount
          write(*,'(''For sequence '',i2,'' average is over '',i2,
     x      '' correlation functions'')') iseq,ncount
        enddo
        do i=1,ncor
          write(15,'(2i5,f10.2,20f10.4)') iseq,
     x    (i-1)*idconf,(i-1)*deltime,(csave(i,j),j=1,nchain)
        enddo
      enddo

      do i=1,ncor
        write(15,'(2i5,f10.2,40f10.4)') 0,   
     x  (i-1)*idconf,(i-1)*deltime,(caver(i,j),j=1,maxlen)
      enddo
      end

c
c
      subroutine corfun(ndat,data,ncor,corel)
      implicit real*8 (a-h,o-z)
      dimension data(3,ndat),corel(ncor)

      do ilag=0,ncor-1
        dot=0.d0
        do i=1,ndat-ilag
          dot=dot+data(1,i)*data(1,i+ilag)
     x           +data(2,i)*data(2,i+ilag)
     x           +data(3,i)*data(3,i+ilag)
        enddo
        dot=dot/(ndat-ilag)
        corel(ilag+1)=dot
      enddo

      end
