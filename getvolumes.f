c
c    program to read run.*.inp and run.*.out files
c    to produce a file of times and volumes that goes with
c    the *.xyz files
c
c    the file of times and volumes should have a name that
c    matches that of the xyz file
c
c    this information is in the dump line of the run.*.inp file.
c
c
c    November 2015 - modify code to check that the *.xyz file
c    is valid.  Report if it is missing, does not have the
c    expected number of configurations, or has a partial
c    configuration.
c
c
      implicit real*8 (a-h,o-z)
      character*256 line,fxyz,oline
      logical ifexist1,ifexist2,ifexist3

      open(6,file='getvolumes.out')

      nlastold=0

      do irun=0,1000
c     do irun=0,10
        line=' '
c       look for the file run.irun.out
c       if it exists, look for corresponding run.irun.inp
        write(line,'(i4)') irun
        call leftjust(line)
        is=index(line,' ')-1
        inquire(file='run.'//line(1:is)//'.out',exist=ifexist1)
        inquire(file='run.'//line(1:is)//'.inp',exist=ifexist2)
        if(ifexist1.and.ifexist2)  then
c         write(6,'(''Input/output pair found'',i4)') irun
          open(10,file='run.'//line(1:is)//'.inp')
10        continue
          read(10,'(a256)',end=20) fxyz 
c         line structure:  dump 1 all xyz 10000  xxx.45.xyz
          if(fxyz(1:4).eq.'dump'.and.index(fxyz,' xyz ').ne.0) then
            is1=index(fxyz,' xyz ')+5
            call leftjust(fxyz(is1:))
            read(fxyz(is1:),*) nperiod 
            is2=index(fxyz(is1:),' ')+is1
            call leftjust(fxyz(is2:))
            is3=index(fxyz(is2:),' ')+is2-2
            write(6,'(''Period '',i8,
     x        '' ; XYZ file name is *'',a,''*'')') 
     x        nperiod,fxyz(is2:is3)
            write(6,'(''Line *'',a,''*'')') fxyz(1:is3)
            write(6,'(''is1,is2,is3: '',3i5)') is1,is2,is3
c           look for run line
30          continue
            read(10,'(a256)') oline
            if(oline(1:3).ne.'run') go to 30
            read(oline(4:),*) nsteptot

            open(11,file=fxyz(is2:is3-3)//'vol') 
            open(12,file='run.'//line(1:is)//'.out')
c
c           check for existence of the corresponding *.xyz file
            inquire(file=fxyz(is2:is3-3)//'xyz',exist=ifexist3)
            if(ifexist3) then
c             write(6,'(''Corresponding xyz file exists '',a)')
c    x        fxyz(is2:is3-3)//'xyz'
              open(13,file=fxyz(is2:is3-3)//'xyz')
            else
              write(6,'(''Missing xyz file '',
     x          a)') fxyz(is2:is3-3)//'xyz'
            endif
     
c
            nset=0 !count the sets in this pair of run files

50          continue
            read(12,'(a256)',end=55) oline
            if(index(oline,'--- Step ').eq.0) go to 50
            is1=index(oline,'--- Step ')+8
            read(oline(is1:),*) nstep
            if(mod(nstep,nperiod).eq.0) then
              if(nset.eq.0) nfirst=nstep
              nlast=nstep
              nset=nset+1
52            read(12,'(a256)') oline
              if(index(oline,'Volume').eq.0) go to 52
              read(oline(11:),*) volume
c             read a frame of coordinates
              if(ifexist3) then
                ifframe=1 !assume frame is present
54              continue            
                read(13,*,end=60,err=60) natom
                read(13,*,end=60,err=60) !read the line with "Atom" on it
                do i=1,natom
                  read(13,*,end=60,err=60)
                enddo
                go to 65
60              ifframe=0 !frame is absent or incomplete
65              if(ifframe.eq.1) then
c                 write(6,'(''Frame '',i4,'' present in xyz file '')')
c    x              nset
                else 
                  write(6,'(''Frame '',i4,'' absent in xyz file '')')
     x              nset
                    nset=nset-1
                    nlast=nlast-nperiod
                  go to 55
                endif
              endif
              write(11,'(i10,5x,f25.10)') 
     x          nstep,volume
            endif
            go to 50
55          continue
            write(6,'(''run.'',a,'' Time '',i10,'' to '',i10,
     x        '' with '',i5,'' samples in file '',a)')
     x        line(1:is),nfirst,nlast,
     x        1+(nlast-nfirst)/nperiod,
     x        fxyz(is2:is3)

            if (nlastold.lt.nfirst) then
                write(6,'(''Gap between runs: last ti'',i10,'' new: '',
     x            i10)')
     x            nlastold,nfirst
            endif

            nlastold=nlast
      
            close(11)
            close(12)
            if(ifexist3) close(13)
            go to 20
          endif
          go to 10
20        close(10)
        elseif(ifexist1.or.ifexist2) then
          write(*,'(''Error: only one of run.*.inp'',
     x      '' or run.*.out exists'')')
          if(ifexist1) then
             write(*,'(''Input file exists: '',a)')
     x         'run.'//line(1:is)//'.inp'
          else
             write(*,'(''Input file does not exist: '',a)')
     x         'run.'//line(1:is)//'.inp'
          endif
          if(ifexist2) then
             write(*,'(''Output file exists: '',a)')
     x         'run.'//line(1:is)//'.out'
          else
             write(*,'(''Output file does not exist: '',a)')
     x         'run.'//line(1:is)//'.out'
          endif
          stop 'missing inp/out file pairs'
        endif
      enddo
      end


c
c     left justify a string
c
      subroutine leftjust(string)
      character*(*) string

      is=0
      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 10
        endif
      enddo

10    continue
      if(is.eq.0) return  !all blank string

      do i=1,len(string)-is+1
        string(i:i)=string(is+i-1:is+i-1)
      enddo
c     blank out the last is-1 characters
      do i=len(string)-is+2,len(string)
        string(i:i)=' '
      enddo
      end


c
c     subroutine lstchr returns the index of the last nonblank character in a string
c
      subroutine lstchr(string,ilst)
      character*(*) string 

      ilst=0  !if string is all blank, return zero
      do i=len(string),1,-1
        if(string(i:i).ne.' ') then
          ilst=i
          go to 10
        endif
      enddo
10    continue
      end


