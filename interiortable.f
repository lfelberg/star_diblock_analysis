c
c    program to read a voronoi.class output file and to match it
c    with information used to make it so that surface area
c    composition can be computed for each interior cluster
c    
c    input a *.cluster file name and look for *.interior.class file
c    that results after running the voronoi program on subsets of 
c    sites.

      character*256 fname,line,line1,line2,line3
      character*4 string
      dimension neighbors(1000),list(1000)

      call getarg(1,line)
      is=index(line,'cluster')
      if(is.eq.0) then
        write(*,'(''Error:  name of a *.cluster '',
     x    ''needed on command line '')')
        stop 'name of cluster file expected on command line'
      endif

      fname=line
      open(10,file=fname)
      open(11,file=fname(1:is-1)//'interior.class')

      ifile=0
      isubset=0
10    continue
      read(10,'(a256)',end=500) line1
      ifile=ifile+1
      is=index(line1,' ')-1
      read(line1(is+1:),*) iconf
      read(10,'(a256)') line
      is=index(line,'clusters')+8
      read(line(is:),*) nclust
      do ic=1,nclust
        read(10,'(a256)') line
        is=index(line,'sites')+5
        read(line(is:),*) nsite
        read(10,*)
        do isite=1,nsite
          read(10,'(a256)') line
          is=index(line,'index')+5
          read(line(is:),* ) isindex   !site index
          list(isite)=isindex
          is=index(line,'neighbors')+9
          read(line(is:),* ) nneigh  !site index
          read(10,'(10i8)')   (neighbors(i),i=1,nneigh)
        enddo
        isubset=isubset+1
20      read(11,'(a256)') line
        is=index(line,'(PHOB-WAT)')
        if(is.eq.0) go to 20
        read(line(is-20:),*) aphob1
C        read(11,'(a256)') line ! for St 25, have linker
C        read(11,'(a256)') line
C        read(line(is-20:),*) aphob2
        read(11,'(a256)') line
        read(line(is-20:),*) aphil
        aphob = aphob1  ! for ST25 +aphob2
        write(*,'('' HPHOB area: '',f15.6,
     x    '' HPHIL area: '',f15.6,2x,'' Percent HPHOB '',f10.2,
     x    '' Perent HPHIL '',f10.2)')
     x    0.50*aphob,0.50*aphil,100.d0*aphob/(aphob+aphil),
     x                100.d0*aphil/(aphob+aphil)

      enddo
      go to 10


500   continue
      close(10)
      close(11)
      end
