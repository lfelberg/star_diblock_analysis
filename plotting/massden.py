'''
This module is for plotting
mass density functions given .his1 files
(output of stargaze program)
'''

import numpy as np

import importlib
import starDat
importlib.reload( starDat )
getData = starDat.oneDat

import starPlots
importlib.reload( starPlots )
makePlot = starPlots.tempPlot

import util
importlib.reload( util )

##############################
# Getting data and plotting
##############################
def main(arg1, arg2, arg3):
    starList, starGel, starName, term = arg1, arg2, arg3, 'dat'
    if 1 in starGel: term = 'gel'
    ct = 0
    mdNames = ['All', 'Core', 'Hphob', 'Hphil', 'Water']

    for i in starList:
        st = 'star'
        if starGel[ct]:
            st += 'g'
        hisFile = util.datDir + st + str(i)
        hisFile += '/' + str(util.temp)+ 'K/stargaze_' + st
        hisFile += str(i)+ '_' + str(util.temp)+'Kw.his1'
        dirOuput = util.outDir + '/hist_' + st
        dirOuput +=  str(i)+'_' + str(util.temp) + 'K'

        titlPlt = 'Mass density, ' + starName[ct] + ' 350K'

        radius, massDen = getData(hisFile, i, starGel[ct])
        makePlot(mdNames, starGel[ct],
                        'one',
                        radius,
                        massDen,
                        yErr = [],
                        yphob = i,
                        lege = util.legOpt,
                        title = titlPlt if util.tiOpt else '',
                        outFile = dirOuput,
                        xran = [0, 40])
        ct += 1


    mdNames = ['All', 'Core+Hphob', 'Hphil', 'Water']

    ct = 0
    for i in starList:
        st = 'star'
        if starGel[ct]:
            st += 'g'
        hisFile = util.datDir + st + str(i)
        hisFile += '/' + str(util.temp)+ 'K/stargaze_' + st
        hisFile += str(i)+ '_' + str(util.temp)+'Kw.his1'
        dirOuput = util.outDir + '/hist_all_' + st
        dirOuput +=  str(i)+'_' + str(util.temp) + 'K'

        titlPlt = 'Mass density, ' + starName[ct] + ' 350K'

        radius, massDen = getData(hisFile, i, starGel[ct])
        added = np.zeros((4, massDen.shape[1]))   
        added[0] = massDen[0]
        added[1] = massDen[1]+massDen[2]
        added[2] = massDen[3]
        added[3] = massDen[4]
        makePlot(mdNames, starGel[ct],
                        'one',
                        radius,
                        added,
                        yErr = [],
                        yphob = i,
                        lege = util.legOpt,
                        title = titlPlt if util.tiOpt else '',
                        outFile = dirOuput,
                        xran = [0, 40])
        ct += 1

if __name__=='__main__':
    main(sys.arg[1], sys.arg[2], sys.arg[3])
