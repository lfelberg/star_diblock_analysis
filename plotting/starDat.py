'''
 File to get data for plotting all functions
'''
import importlib
import numpy as np

import starDics
importlib.reload( starDics )

def oneDat(datFile, starNum, starGel):
    """function for retrieving data
    from a file with info for one star at 1 temp
    given file extension and a star number (int)
        Works with : his, scor files """
    starSpecs =  starDics.starAll if (starGel == 0) \
                                         else starDics.gelAll

    if (datFile.find('scor')!=-1):
        xDat = 2
        runType = 'sco'
        columns = starSpecs[starNum][2]
        lastStart = 8000.
    elif (datFile.find('his')!=-1):
        xDat = 5
        runType = 'his'
        columns = []
        lastStart = 0.
    else:
        print ('Only scor, his files recognised!')
        return

    lines = open(datFile).readlines()
    yData, xVals = [[] for i in range(len(columns))], []
    for line in lines:
        tsv = line.split()
        if ((runType == 'sco') and (float(tsv[0]) != 0)):
            continue
        if columns == []:
            columns = range( 8, len(tsv))
            yData = [[] for i in range(len(columns))]

        xVals.append(float(tsv[xDat])-lastStart)
        for i in range(len(columns)):
            yData[i].append(float(tsv[columns[i]]))

    if (runType == 'his') and ("err" not in datFile):
        yArr = np.zeros( (5, np.shape(yData)[1]) )
        massSpecs = starSpecs[starNum][3]
        yArr[0] = yData[0]
        totLoc = 8
        if massSpecs != []:
            for i in range(4): # 4 regions: cor, ob, il, wat
                for j in range(len(massSpecs[i])):
                    yArr[i+1] += yData[massSpecs[i][j] - totLoc]
            return( xVals, yArr)

    if (runType == 'his') and ("err" in datFile):
        yArr = np.zeros( (10, np.shape(yData)[1]) )
        massSpecs = starSpecs[starNum][3]
        yArr[0] = yData[0]
        totLoc = 8
        if massSpecs != []:
            for i in range(4): # 4 regions: cor, ob, il, wat
                for j in range(len(massSpecs[i])):
                    yArr[i+1] += yData[massSpecs[i][j] - totLoc]

            return( xVals, yArr)

    return (xVals, np.array(yData))


def manyDat(fileName, starNumList):
    '''used for Voronoi, anis, rgyr, water files
    retrieves data from a manually formatted .dat file '''
    lines = open(fileName).readlines()
    starDict = {}
    for line in lines:
        vals = line.split()
        starNum = int(vals[0])
        if (fileName.find('dihedral')!=-1):
            temp = vals[1]
        else:
            temp = float(vals[1])
        allDat = np.array(vals[2:])
        allDat = allDat.astype(np.float)
        valTup = (temp,allDat)

        if starNum not in starDict:
            starDict[starNum] = [valTup]
        else:
            starDict[starNum].append(valTup)

    ctr = 0
    data = [[[0]*len(starDict[x]) \
                  for x in starNumList] \
                  for y in range(len(vals)-1)]
    for num in starNumList:
        for j in range(len(starDict[num])):
            data[0][ctr][j] =  starDict[num][j][0]
            for i in range(len(starDict[num][0][1])):
                data[i+1][ctr][j] =  starDict[num][j][1][i]
        ctr+=1

    return (np.array(data))
