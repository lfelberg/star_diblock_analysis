'''
Module for making surface area to volume ratio plots
'''
import importlib
import starPlots
importlib.reload( starPlots )
tempPlot = starPlots.tempPlot

import starDat
importlib.reload( starDat )
getData = starDat.manyDat

import util
importlib.reload( util )

def main(arg1, arg2):
    starList, starGel, term = arg1, arg2, 'dat'
    if 1 in starGel: term = 'gel'
    yran = [1.0, 4.0]

    ##############################
    # Getting data and plotting
    ##############################
    outFile = util.outDir  + 'sav'
    datFile = util.allDir + 'surface_area.' + term
    data = getData(datFile, starList)

    temp = [min(min(p) for p in data[0])-25,
                max(max(p) for p in data[0])+25]
    tempPlot(starList, starGel,
                'many',
                data[0],
                data[1],
                data[2],
                False,
                lege = util.legOpt,
                title = 'Surface area to volume ratio' if util.tiOpt else '',
                outFile = outFile,
                xran = temp,
                yran = yran)


if __name__=='__main__':
    main(sys.arg[1], sys.arg[2])
