# Star Polymer Plotting

To run, simply change any of the parameters below in util.py, then execute the script runPlot.py. 
This should produce the plots in the specified outDir for the stars and plots requested. 

## Running the plotting tools 
In theory, the only file you should need to modify is util.py

This has all of the logistical information about the stars you want to study

* ind = A number that references a list of stars by number. The current list is

```
  starGrps = [ [ 8, 12, 25, 15],          #0  
                    [ 12, 15, 25],          #1
                    [ 24, 26, 27, 30, 31],  #2
                    [ 6, 24, 26, 27],       #3
                    [ 8, 35],               #4
                    [ 24, 31, 32, 33, 34],  #5
                    [ 30, 31],              #6
                    [ 24, 31, 32, 34],      #7
                    [ 8, 12, 15] ,          #8 
                    [ 8, 12] ,              #9 
                    [ 12, 15],              #10 
                    [ 24, 26, 30, 31]]      #11
```

So if I want stars [8, 12, 15], I would change ind to 8
 
* pltS = A list of the plots you want to create. The current available plots are:

```
  #starPlt =  'dihedral_plot',
                #'hydrogen_bonds'
                #'massden',
                #'orient_autocorr',
                #'persistence',
                #'rgyr',
                #'sidechain_len'
                #'voronoi',
                #'water_clust',
                #'water_neigh',
                #'water_penetration'
```
               
So, if I wanted to run voronoi and water_clust, I would change:
            pltS = [ 'voronoi', 'water_clust']

* legOpt = a boolean for whether you want a legend on your figure (True = yes)

* tiOpt = a boolean for whether you want a title on your figure (True = yes)

* temp = a temperature for individual plots (mainly used for mass density curves
   and orientational autocorrelation functions)

* homeDir = the location on your machine for these python scripts and the data

* scriptDir = the location on your machine for the dir where the plotting python scripts reside

* datDir = the location on your machine for the data (see below)

* outDir = the location on your machine where you want the plots saved


## DATA locations and COMBINED DATA

So the general file tree that I have set up is as follows:

datDir contains the following:

* star_all
* starXX: (starXX is all information I have from stargaze output for starXX)

starXX contains

* 350K
* 400K
* 450K
    
each of which have the outputs from stargaze and its 
following programs ( rgyr, orientation, rg_vs_time)
For this, you really only need: *his1, *sprp and *scor
BUT Their names MUST be as follows:
stargaze_star[star#]_[temp]Kw.*

star_all is a sort of catchall that has the following:

* anis_all.*  
* dihedral_bb.*
* dihedral_sidePOX.*
* hbonds.*
* persistence.*
* persistence_sidePOX.*
* rgyr_all.*
* sidelen.*
* voronoi.*
* water_clu.*
* water_depth.*
* water_neigh.*
      
 The * extension is either going to be .dat or .gel (for gelcore systems)
 The contents for each are described below, but they generally have one
 of the following 2 forms:
        
        [star#] [temp1] [val1] [val2] [val3] ...
        [star#] [temp2] [val1] [val2] [val3] ...
        
 For example, anis_all.dat looks like this:
        
        4    300    0.0243    0.0135    0.9925    0.0041    0.0098    0.0048    1.0991    0.0015
        4    350    0.0265    0.0122    0.3538    0.0022    0.0160    0.0070    0.3081    0.0012
        4    400    0.0314    0.0201    0.4416    0.0042    0.0196    0.0111    0.3044    0.0019
        4    450    0.0360    0.0256    0.6717    0.0077    0.0229    0.0148    0.2660    0.0028
        6    350    0.0279    0.0156    1.6905    0.0028    0.0200    0.0101    3.4488    0.0026
        6    400    0.0348    0.0221    1.6097    0.0038    0.0212    0.0137    1.6737    0.0024
        6    450    0.0314    0.0206    0.9379    0.0026    0.0167    0.0103    0.7818    0.0012
        8    350    0.0179    0.0092    0.5583    0.0019    0.0033    0.0020    0.7120    0.0005
        8    400    0.0157    0.0107    0.4701    0.0020    0.0055    0.0034    0.2588    0.0005
        
 or
        
        [star#] [bin1] [temp1 val] [temp1 err] [temp2 val] [temp2 err] ...
        [star#] [bin2] [temp1 val] [temp1 err] [temp2 val] [temp2 err] ...
        [star#] [bin3] [temp1 val] [temp1 err] [temp2 val] [temp2 err] ...
        
 For example, water_clu.dat:
        
        4  1    0.9838  0.7611  0.5786  0.3279  1.0000  0.9484  0.7755  0.5847
        4  2    0.0162  0.2265  0.3272  0.3941  0.0000  0.0516  0.1875  0.3004
        4  3    0.0000  0.0123  0.0786  0.2011  0.0000  0.0000  0.0324  0.0948
        4  4    0.0000  0.0000  0.0136  0.0634  0.0000  0.0000  0.0046  0.0202
        4  5    0.0000  0.0000  0.0019  0.0135  0.0000  0.0000  0.0000  0.0000
        4  6    0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000
        4  7    0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000
        4  8    0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000
        4  9    0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000
        6  1    0.3108  0.3170  0.1599  0.1022  0.7355  0.7130  0.4760  0.3620
        6  2    0.4247  0.3960  0.3161  0.2597  0.1997  0.2277  0.2744  0.2888
        6  3    0.1997  0.2277  0.2744  0.2888  0.0477  0.0520  0.1606  0.1951
        6  4    0.0477  0.0520  0.1606  0.1951  0.0153  0.0062  0.0646  0.1047
        6  5    0.0153  0.0062  0.0646  0.1047  0.0018  0.0011  0.0181  0.0339
        
        
*Note: for systems that don't have a Temperature = 300K, just fill them with zeros!*


### anis_all = Anisotropy output from rygr.f 

    line: [star#] [temp] [anis_whole_star] [rms_whole] [tau_whole] [unc_whole] [anis_hphob] [rms_hphob] [tau_hphob] [unc_hphob] 
        

### dihedral_bb = Dihedral backbone information from (used for LCST of PEG)

    line: [star#] [dihedral conformer] [histogram value @ 300K] [histogram value @ 350K] [histogram value @ 400K] [histogram value @ 450K]
    
ex:

```
4  ttt    0.059  0.044  0.054  0.123
4  ttg    0.014  0.018  0.022  0.051
4  tgt    0.726  0.685  0.640  0.513
4  tgg    0.126  0.151  0.190  0.167
4  gtg    0.000  0.002  0.000  0.000
 
31 ggg    0.000  0.001  0.000  0.002
31 tgg'   0.000  0.104  0.120  0.163
31 ggg'   0.000  0.004  0.002  0.003
31 gg'g   0.000  0.001  0.002  0.003
```

##############################

### dihedral_sidePOX = Same as dihedral_bb but for the POX sidechain (this isn't really used!)

    line: [star#] [dihedral conformer] [histogram value @ 300K] [histogram value @ 350K] 
                   [histogram value @ 400K] [histogram value @ 450K]

##############################

### hbonds = Hydrogen bonding information from VMD hbond calculations

    line = [star#] [temperature] [average # hbonds/PEG monomer] [error]
    
ex:

```
 6  300  0.381  0.034
 6  350  0.311  0.032
 6  400  0.267  0.029
 6  450  0.228  0.029
 8  350  0.367  0.012
 8  400  0.300  0.011
 8  450  0.215  0.008
12  350  0.366  0.027
12  400  0.288  0.022
12  450  0.217  0.021
```

##############################

### persistence = Persistence length calculations (more precisely the intermonomer correlation)

    line: [star#] [monomer number from core] [Corr wrt innermost @ 300K] [Err wrt innermost @ 300K]
                 [Corr wrt innermost @ 350K] [Err wrt innermost @ 350K] [Corr wrt innermost @ 400K] 
                 [Err wrt innermost @ 400K]  [Corr wrt innermost @ 450K] [Err wrt innermost @ 450K] 
                 [Corr wrt outermost @ 300K] [Err wrt outermost @ 300K] [Corr wrt outermost @ 350K] 
                 [Err wrt outermost @ 350K]  [Corr wrt outermost @ 350K] [Err wrt outermost @ 350K]    
                  
ex:

```
4   0   1.000   0.000   1.000   0.000   1.000   0.000   1.000   0.000  0.000    0.067   0.008   0.041   -0.020  0.042   -0.005  0.049
4   1   0.388   0.061   0.436   0.080   0.407   0.088   0.405   0.078  0.008    0.040   0.004   0.047   -0.010  0.043   -0.010  0.047 
```

##############################

### persistence_sidePOX = same as Persistence but with respect to sidechain region of POX

    line: [star#] [monomer number from core] [Corr wrt innermost @ 300K] [Err wrt innermost @ 300K]
                 [Corr wrt innermost @ 350K] [Err wrt innermost @ 350K] [Corr wrt innermost @ 400K] 
                 [Err wrt innermost @ 400K]  [Corr wrt innermost @ 450K] [Err wrt innermost @ 450K] 
                 [Corr wrt outermost @ 300K] [Err wrt outermost @ 300K] [Corr wrt outermost @ 350K] 
                 [Err wrt outermost @ 350K]  [Corr wrt outermost @ 350K] [Err wrt outermost @ 350K]    
                  
##############################

### rgyr_all = Rg output from rgyr.f

    line: [star#] [temp] [rgyr_whole_star] [rms_whole] [tau_whole] [unc_whole] 
                         [rgyr_hphob] [rms_hphob] [tau_hphob] [unc_hphob] 

##############################

### sidelen = Histogram of length of side group for POXA variants with long alkane chains.

    line: [star#] [length bin] [histogram value @ 350K] [histogram value @ 400K] [histogram value @ 450K]

##############################

### voronoi = Interfacial area between different components (core, hydrophobic, linker, hphil) of the system. 

If there is none of that component present, the norm fact should be set to 1.0. 
For a linker between the core and the hydrophobic material, I just include it as 
core material. This is a pretty long line, so I use the program 
analysis/python/voronoi_results.py to get it all one one line (except for the first 6 columns)

    line: [star#] [temp] [norm fact core] [norm fact hphob] [norm fact link] [norm fact hphil ] [Core/Hphob area] [Core/Hphob err] 
                    [Core/Linker area] [Core/Linker err] [Core/Hphil area] [Core/Hphil err] [Core/Water area] [Core/Water err] 
                    [Core/All area] [Core/All err] [Hphob/Linker area] [Hphob/Linker err] [Hphob/Hphil area]  [Hphob/Hphil err] 
                    [Hphob/Water area] [Hphob/Water err] [Hphob/All area] [Hphob/All err] [Linker/Hphil area] [Linker/Hphil err] 
                    [Linker/Water area] [Linker/Water err] [Linker/All area] [Linker/All err] [Hphil/Water area] [Hphil/Water err] 
                    [Hphil/All area] [Hphil/All err]

##############################

### water_clu 

Information about the water clusters inside the star. The third-seventh column have information
about the presence of water. So by count, there could be zero waters in it. The binning (col 2)
goes from 1 upward, so for this plot, the program subtracts one. The last 4 columns are the interior
water cluster sizes at 4 temperatures.

    line: [star#] [count bin] [ interior water cluster count @ 300K] [ count @ 350K] [ count @ 400K] [ count @ 450K] 
                [ interior water cluster size @ 300K] [ size @ 350K] [ size @ 400K] [ size @ 450K]

##############################

### water_depth = Information about the penetration depth of the waters.

    line: [star#] [distance bin] [ interior water distance from bulk water @ 300K] [ bulk @ 350K] [ bulk @ 400K] [ bulk @ 450K] 
                [ interior water distance to core @ 300K] [ core @ 350K] [ core @ 400K] [ core @ 450K]

##############################

### water_neigh  

Information about the water's environment inside the star polymer, and 
how deep it is compared to the polymer's approximate radius (estimated from Rg, R = sqrt(5/3) X Rg)

    line: [star#] [temp] [fraction hydrophobic neighbors] [Int. water average distance to core] 
                    [dist to core err] [R of total star] [R total err] [R of hydrophobic part] [R hphob err]



## ADDING A NEW STAR TO THE PROGRAM:

If you want to add a new star, first, you should look in the starDics.py file,
in the dictionaries called starAll and gelAll, and check that it is not there.

It still is a work in progress, but for now, put the gelcore stars in the gelAll 
dictionary and the others in starAll.

These dictionaries have a specific format, so you can access details with a key value,
given before the colon. Here the key is the star number. Then the information that 
corresponds to that key is given after in the following format:

```
KEY : ( [ color code ], [Star name, marker shape, 300 dat?,
        hphob name, phil name, number of phob, number of phil],
        [mono for orient autocor],
        [array of comps of system in his1 file:
            [core], [hphob], [hphil], [wat] )
```

### starAll explanation:

* color code : a python array of 3 values, each ranging from 0 to 1. Each
              is a fraction of 255, for the RGB color codes:

```
    ex: [ 1, 0, 0] = red
        [ 0, 1, 0] = green
        [ 60./255., 179./255., 113/255.] = Medium sea green
```

* star name = The name that you want to appear in the legend for
            this star

* marker shape = Shape of marker on plots, some examples include
                '^', 'o'

* 300 dat? = 0 if you have data at 300K, 1 if you do not

* hphob name = the name of your hydrophobic region, ex = 'PVL'

* hphil name = the name of your hydrophilic region, ex = 'PEG'

* number of phob = the number of hydrophobic repeat units on one arm

* number of phil = the number of hydrophilic repeat units on one arm.
                 NOTE: for PEG, in analysis, we use the base of DME = 2*PEG
                 so here nu phil = 12, NOT 24!

* mon0 for orient autocor = This one is not great, but you must provide a list of
                           the monomers to consider for the autocorrelation function.
                          For a long armed A-[PVL16-DME12] star, we generally chose:
                          PVL1, PVL8, PVL16 and then PEG1 = DME1, PEG11 = DME5, PEG23 = DME12
                          Then we have to go and find where these columns are in the *scor file.
                          In the *scor file, at the bottom, the lines that have 0 in the first
                          column are what we need. The first 3 columns are about time, so 
                          the monomer directly attached to the core is column = 3 (zero-based)
                          For our example, this is PVL1. Then we count outward to get PVL8 = col[10]
                          and so on. The final result for this example is: [ 3, 10, 18, 19, 21, 30]


* array for comps of system = For the mass density files, I have simplified the analysis into 4
                            categories: core, hphob, hphil and water. Some systems will have 
                            more components (like a linker, or multiple units like a dendrimer)
                            so you just need to map them onto these four categories. The results
                            for the given category is the sum of the components. Sometimes, you 
                            may not have that component, then you simply leave the array empty.
                            Looking at the Gelcore = starg15 example: I have a core, then a 
                            core-PVL linker, then a hydrophobic region, then another linker,
                            then a PEG and finally water. 
                            Looking at the *his1 file, after rho = 
                            there are 6 columns corresponding to the different regions described
                            + a total one at the beginning.
                            Here, we start at 9, because the program splits the line by whitespace,
                            so our array is: [[coreCol, lin1Col], [PVLCol, lin2Col], [PEGCol], [watCol]]
                            and in terms of column numbers: [[ 9, 10], [11, 12], [ 13], [14]]

Putting all this together:

```
    8 : ([0, 0.5, 0], 
            ['Adamantane', 's', 1, 'PVL', 'PEG', 16, 12],
            [3, 10, 18, 19, 21, 30],
            [[9], [10], [11], [12]]), 
```


### other changes

If you add a new star that has a new name for the hydrophobic or hydrophilic 
region, you should also add it to the monomerColors dictionary. This 
is so the orientational autocorrelation functions will all be colored the 
same for comparison. Generally,

```
'HPHOB 1'  = colors[0]
'HPHOB 8'  = colors[1]
'HPHOB 16' = colors[4]

'HPHIL 1'  = colors[2]
'HPHIL 11' = colors[3]
'HPHIL 23' = colors[5] 
```


### starGrps and starGel

Finally, you need to add your star into an array for analysis!
This is at the top of starDics, and you can make whatever combination you want,
just add it to the end. You should also add in a corresponding list for
the starGel, to say whether it is a gelcore (1) or not (0).

```
ex: [ 8, 24, 25] ( [ 0, 0, 1] )

starGrps = [ [ 8, 12, 25, 15],              #0  
                    [ 12, 15, 25],          #1
                    [ 24, 26, 27, 30, 31],  #2
                    [ 6, 24, 26, 27],       #3
                    [ 8, 35],               #4
                    [ 24, 31, 32, 33, 34],  #5
                    [ 30, 31],              #6
                    [ 24, 31, 32, 34],      #7
                    [ 8, 12, 15] ,          #8 
                    [ 8, 12] ,              #9 
                    [ 12, 15],              #10 
                    [ 24, 26, 30, 31],      #11
                    [ 8, 24, 25] ]          #12

starGel = [ [ 0, 0, 0, 1],            #0  
                    [ 0, 1, 0],       #1
                    [ 0, 0, 0, 0, 0], #2
                    [ 0, 0, 0, 0],    #3
                    [ 0, 0],          #4
                    [ 0, 0, 0, 0, 0], #5
                    [ 0, 0],          #6
                    [ 0, 0, 0, 0],    #7
                    [ 0, 0, 1] ,      #8
                    [ 0, 0],          #9
                    [ 1, 1] ,         #10
                    [ 0, 0, 0, 0],    #11
                    [ 0, 0, 1]]       #12
```



