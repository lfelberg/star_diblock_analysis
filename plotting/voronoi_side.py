'''
Module for making voronoi plots
'''
import numpy as np
import importlib
import starDat
importlib.reload( starDat )
getData = starDat.manyDat

import starPlots
importlib.reload( starPlots )
vorPlot = starPlots.tempPlot

import util
importlib.reload( util )

def main(arg1, arg2):
    starList, starGel, term = arg1, arg2, 'dat'
    if 1 in starGel: term = 'gel'
    vorFile = util.allDir + 'voronoi_side.' + term
    outFile = util.outDir + 'vor'

    ###############
    # Getting data
    ###############
    data = getData(vorFile, starList)

    ###############
    # Plotting the files
    ###############
    titl = 'Normalized interfacial area \n'
    tempRan = [min(min(p) for p in data[0])-25,
                        max(max(p) for p in data[0])+25]
    temp = data[0]

    # Normalization constants from file
    sideNorm    = data[1]

    # contains data for various voronoi plots,
    # see plotname or titlList for details
    #y[i+1]=error of y[i]
    ys = [2, 4, 6, 8]
    ifAll = [ 0, 0, 0, 1]
    normLoc = [ 1, 1, 1, 1]

    # to find self-self, need to subtract from one
    ones = np.ones(data[1].shape)

    plotname = ['_side_phob', '_side_phil',
                '_side_wat', '_side_side']
    titlList = [' Alkane/Hphob', ' Alkane/Hphil',
                ' Alkane/Water', ' Alkane/Alkane']

    # Actual plot call here
    for i in range(len(ys)):
        ydat, err = [], []
        for j in range(len(data[ys[i]])):
            ydat.append(data[ys[i]][j]/data[normLoc[i]][j][0])
            err.append(data[ys[i]+1][j]/data[normLoc[i]][j][0])
        if ifAll[i]:
            ydat = ones - ydat

        vorPlot(starList, starGel,
                    'many',
                    temp, ydat, err,
                    hphob = False,
                    lege = util.legOpt,
                    title = titl + titlList[i] ,
                    outFile = outFile+plotname[i],
                    xran = tempRan)


