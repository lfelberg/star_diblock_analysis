'''
Module for making plots of interior water cluster size and counts
'''
import sys
import numpy as np
import importlib
import starDat 
importlib.reload( starDat )
getData = starDat.manyDat

import starPlots  
importlib.reload( starPlots )
diPlot = starPlots.tempPlot

import util  
importlib.reload( util )

def main(arg1, arg2):
    starList, starGel, term = arg1, arg2, 'dat'
    if 1 in starGel: term = 'gel'
    diFile = util.allDir + 'dihedral_bb.' + term
    outFile = util.outDir + 'dihedral_conforms'

    ###############
    # Getting data
    ###############
    data = getData(diFile, starList)

    ###############
    # Plotting the files at 350K
    ###############
    diPlot(starList, starGel,
                'bar',
                data[0],
                np.array(data[2]),  #.astype(np.float),
                yErr = [],
                hphob = False,
                lege = util.legOpt,
                title = 'Dihedral angle conformations, 350K' \
                            if util.tiOpt else '',
                outFile = outFile,
                xran = [-0.55, len(data[0][0])-0.25])

    countr = 0
    for i in starList:
        temp=[300,350,400,450];tdatX = [data[0][countr]]*4
        tdatCt = [data[1][countr],data[2][countr],
                  data[3][countr],data[4][countr]]

        diPlot(temp, [ 0, 0, 0, 0],
                'bar',
                tdatX,
                np.array(tdatCt).astype(np.float),
                yErr = [],
                hphob = True,
                yphob = [i],
                lege = util.legOpt,
                title = 'Dihedral angle conformations'  \
                            if util.tiOpt else '',
                outFile = outFile + '_st',
                xran = [-0.55, len(data[0][0])-0.25])

        #if pox_bool:
        #    side_data = getData(sideFile, starList, pox_bool)
        #    tdat_sideX = [side_data[0][countr]]*4;
        #    tdat_sideCt = [side_data[1][countr],side_data[2][countr],
        #                        side_data[3][countr],side_data[4][countr]]
        #    anglePlot(tdat_sideX[starSpecs[i][1][2]:],
        #            tdat_sideCt[starSpecs[i][1][2]:],
        #            temp[starSpecs[i][1][2]:],
        #            'Dihedral angle conformations, '+starSpecs[i][1][0],
        #            outFile+'dihedral_side'+str(i),
        #            'Dihedral conformation',tempFlag=1 )

        countr += 1;

if __name__=='__main__':
    main(sys.arg[1], sys.arg[2])
