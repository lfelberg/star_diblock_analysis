
## What groups of systems to plot
starGrps = [ [ 8, 12, 25, 15],              #0
                    [ 12, 15, 25],          #1
                    [ 24, 26, 27, 30, 31],  #2
                   #[ 24, 26, 30, 31],  #2
                    [ 26, 27, 30, 31],      #3
                    [ 8, 35],               #4
                    [ 24, 31, 33, 34],      #5
                    [ 30, 31],              #6
                    [ 24, 33, 34],          #7
                    [ 8, 12, 15] ,          #8
                    [ 8, 12] ,              #9
                    [ 12, 15],              #10
                    [ 24, 26, 30, 31],      #11
                    [ 24, 26, 31],          #12
                    [ 8, 12, 13, 25],       #13
                    [ 8, 35, 36, 37],       #14
                    [ 13 ],                 #15
                    [ 4, 6, 8, 9 ],         #16
                    [ 4, 8 ],               #17
                    [ 24, 27, 30 ],         #18
                    ]

## What groups of systems to plot
starGel = [ [ 0, 0, 0, 1],                 #0
                    [ 0, 1, 0],            #1
                    [ 0, 0, 0, 0, 0],      #2
                   #[ 0, 0, 0, 0],         #2
                    [ 0, 0, 0, 0],         #3
                    [ 0, 0],               #4
                    [ 0, 0, 0, 0],         #5
                    [ 0, 0],               #6
                    [ 0, 0, 0, ],          #7
                    [ 0, 0, 1] ,           #8
                    [ 0, 0],               #9
                    [ 1, 1] ,              #10
                    [ 0, 0, 0, 0] ,        #11
                    [ 0, 0, 0] ,           #12
                    [ 0, 0, 1, 0],         #13
                    [ 0, 0, 0, 0],         #14
                    [ 1 ],                 #15
                    [ 0, 0, 0, 0 ],        #16
                    [ 0, 0 ],              #17
                    [ 0, 0, 0] ,           #18
                    ]

"""
Below is an array for storing the file names
        of available plotting functions
        extras are not available for all stars
"""
starPlt = [ 'dihedral_plot',
                'g_of_r',
                'massden',
                'massden_err',
                'orient_autocorr',
                'persistence',
                'rgyr',
                'voronoi',
                'voronoi_side',
                'water_clust',
                'water_neigh',
                'water_restime',
                'water_penetration']

extras = [ 'hydrogen_bonds', 'sidechain_len']

"""
Below is a dictionary for storing the names and
necessary colors for a given star.
key is int and value is a tuple:
    ([color], [Star name, marker shape, 300 dat?,
        hphob name, phil name, #phob, #phil],
        [mono for orient autocor],
        [array of comps of system in his1 file:
            [core], [hphob], [hphil], [wat])
"""
starAll = {
    'H2O' : (  [0,0,0],
            ['Water', '^', 0, 'Water', 'Water', 0,0],
            [], []),
    0 : (  [0,0,0],
            ['Hphobe', '^', 0, 'PVL', 'PEG', 8,3],
            [3, 10, 11, 13],
            []),
    4 : ([0, 0, 1],
            ['S-PEG', '^', 0, 'PVL', 'PEG', 8,3],
            [3, 10, 11, 13],
            []),
    6 : ([1, 0, 0],
            ['S-POXA', 'o', 0, 'PVL', 'POXA', 8,6],
            [3, 10, 11, 15],
            []),
    8 : ([0, 0.0, 0],
   #8 : ([51./255., 160./255., 44./255.],
           #['Adamantane', 's', 1, 'PVL', 'PEG', 16,12],
            ['PVL', 's', 1, 'PVL', 'PEG', 16,12],
            [3, 10, 18, 19, 21, 30],
            []),
    9 : ([.93, .53, .18],
            ['L-PC1', 's', 1, 'PVL', 'PC1', 16,12],
            [3, 4, 5, 7, 9, 11],
            [[9],[10],[11, 12],[13]]),
    12 : ([0.6, 0.2, 0],
            ['Dendrimer', 'D', 1, 'PVL', 'PEG', 18, 12],
            [4, 11, 19, 21, 23, 30],
            [[9, 10, 11],[12, 13],[14],[15]]),
   #24 : ([77./255., 175./255., 74./255.],
    24 : ([87./255., 175./255., 104./255.],
           #['PMeOX', 'D', 1, 'PVL', 'PMeOX', 16, 24],
            ['PMeOX', 'D', 1, 'PVL', 'P*OX', 16, 24],
            [3, 10, 18, 19, 23, 41],
            [[9],[10],[11],[12]]),
    25 : ([72./255., 108./255., 186./255.],
            ['GelcorePEG', '^', 1, 'Linker', 'PEG', 1, 12],
            [4, 6, 15],
            [[9, 10],[],[11],[12]]),
    26 : ([55./255., 126./255., 184./255.],
            ['PEtOX', 's', 1, 'PVL', 'PEtOX', 16,24],
            [3, 10, 18, 19, 23, 41],
            []),
    27 : ([0.894, 0.102, 0.109],
            ['PTBuOX', 'v', 1, 'PVL', 'PTBuOX', 16,24],
            [3, 10, 18, 19, 23, 41],
            [[9],[10],[11],[12]]),
    30 : ([152./255., 78./255., 163./255.],
            ['PNBuOX', 'o', 1, 'PVL', 'PNBuOX', 16,24],
            [3, 10, 18, 19, 23, 41],
            []),
    31 : ([255./255., 128./255., 0./255.],
            ['PNOctOX', 'v', 1, 'PVL', 'PNOctOX', 16,24],
            [3, 10, 18, 19, 23, 41],
            []),
    32 : ([0.03, 0.41, .870],
            ['P(EG)OX', 'o', 1, 'PVL', 'P(EG)OX', 16,24],
            [3, 10, 18, 19, 23, 41],
            []),
   #33 : ([252./255., 141./255., 98./255.],
    33 : ([ 240./255., 228./255., 87./255.],
            ['PVarOX', 'o', 1, 'PVL', 'PVarOX', 16,24],
            [3, 10, 18, 19, 23, 41],
            []),
   #34 : ([141./255., 160./255., 203./255.],
    34 : ([213./255.,  94./255.,  0./255.],
            ['PMeOX-PEI', 'v', 1, 'PVL', 'PMeOX-PEI', 16,12],
            [3, 10, 18, 21, 23, 28],
            []),
   #35 : ([0.7, 0.87, 0.54],
    35 : ([230./255., 159./255.,  0./255.],
            ['PEtVL-PVL', 'v', 1, 'PEtVL-PVL', 'PEG', 16, 12],
            [3, 10, 18, 21, 28, 30],
            []),
   #36 : ( [0.12, 0.47, 0.71],
    36 : ([86./255., 180./255., 233./255.],
            ['PTButVL-PVL', 'v', 1, 'PTButVL-PVL', 'PEG', 16, 12],
            [3, 10, 18, 21, 28, 30],
            []),
   #37 : ( [0.65, 0.81, 0.89],
    37 : ([  204./255., 121./255., 167./255.],
            ['PNButVL-PVL', 'v', 1, 'PNButVL-PVL', 'PEG', 16, 12],
            [3, 10, 18, 21, 28, 30],
            []),
    300 : ( [0, 0, 0],
            ['300K', ''] ),
    350 : ( [0, 0, 1],
            ['350K', '']  ),
    400 : ( [0.0, 0.0, 0.0],
            ['400K', '']  ),
    450 : ( [1, 0, 0],
            ['450K', '']  )
}

gelAll = {
    13 : ([1, 0, 0],
            ['Gelcore', 'D', 1, 'PVL', 'PEG', 16, 12],
            [3, 10, 18, 21, 28, 30],
            [[9, 10],[11, 12],[13],[14]]),
    15 : ([1, 0, 0],
            ['Gelcore', 'D', 1, 'PVL', 'PEG', 16, 12],
            [3, 10, 18, 21, 28, 30],
            [[9, 10],[11, 12],[13],[14]]),
}

"""
Below is a list and a dictionary for matching
monomer names to colors
(for consistency/ comparison purposes)
Used for the orientation_autocorrelation function
"""
colors = [(0.859, 0.078, 0.234), (0.801, 0.586, 0.047),
            (0., 0.391, 0.), (0.289, 0.461, 1.),
            (0.289, 0., 0.508), (0.777, 0.441, 0.441),
            (0.777, 0.379, 0.078), (0., 0.9297, 0.9297)]

monomerColors = {
                'PVL 1' :  colors[0],
                 'PVL 8' :  colors[1],
                 'PVL 16' : colors[4],
                 'PEtVL 1' :  colors[0],
                 'PEtVL 8' :  colors[1],
                 'PEtVL 16' : colors[4],
                 'PTButVL 1' :  colors[0],
                 'PTButVL 8' :  colors[1],
                 'PTButVL 16' : colors[4],
                 'PNButVL 1' :  colors[0],
                 'PNButVL 8' :  colors[1],
                 'PNButVL 16' : colors[4],
                 'PEG 1' : colors[2],
                 'PEG 11' : colors[3],
                 'PEG 23' : colors[5],
                 'POXA 1' : colors[2],
                 'POXA 11' : colors[3],
                 'POXA 23' : colors[5],
                 'P*OX 1' : colors[2],
                 'P*OX 11' : colors[3],
                 'P*OX 23' : colors[5],
                 'PMeOX 1' : colors[2],
                 'PMeOX 11' : colors[3],
                 'PMeOX 23' : colors[5],
                 'PEtOX 1' : colors[2],
                 'PEtOX 11' : colors[3],
                 'PEtOX 23' : colors[5],
                 'PTBuOX 1' : colors[2],
                 'PTBuOX 11' : colors[3],
                 'PTBuOX 23' : colors[5],
                 'PNBuOX 1' : colors[2],
                 'PNBuOX 11' : colors[3],
                 'PNBuOX 23' : colors[5],
                 'PNOctOX 1' : colors[2],
                 'PNOctOX 11' : colors[3],
                 'PNOctOX 23' : colors[5],
                 'P(EG)OX 1' : colors[2],
                 'P(EG)OX 11' : colors[3],
                 'P(EG)OX 23' : colors[5],
                 'PVarOX 1' : colors[2],
                 'PVarOX 11' : colors[3],
                 'PVarOX 23' : colors[5],
                 'P(EG)OX 1' : colors[2],
                 'P(EG)OX 11' : colors[3],
                 'P(EG)OX 23' : colors[5],
                 'PC1 1' : colors[2],
                 'PC1 5' : colors[3],
                 'PC1 11' : colors[5],
}
