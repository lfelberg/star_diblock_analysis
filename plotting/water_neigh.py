'''
Module for plotting the water penetration 
versus polymer radius data
'''
import importlib
import starDics  
importlib.reload( starDics )
starSpecs=starDics.starAll

import starPlots  
importlib.reload( starPlots )
tempPlot = starPlots.tempPlot

import starDat 
importlib.reload( starDat )
getData = starDat.manyDat

import util  
importlib.reload( util )

def main(arg1, arg2):
    starList, starGel, term = arg1, arg2, 'dat'
    if 1 in starGel: term = 'gel'
    titL = 'Water cluster dist from core vs polymer radius'
    datFile = util.allDir + 'water_neigh.dat'
    outFile = util.outDir

    ###############
    # Getting data
    ###############                  
    data = getData(datFile, starList)

    ##############################
    #  plotting
    ##############################
    countr = 0
    for i in starList: 
        x, y, yErr = [], [], []
        starG = starGel[countr]
        for j in range(3): # 3 Lines to plot 
            x.append( data[0][countr] )
            y.append( data[j*2+2][countr] )
            yErr.append( data[j*2+3][countr] )
        tempPlot( ['H2O', i, 0], [0,starG, starG],
                    'one',
                    x, y, yErr,
                    hphob = True,
                    lege = util.legOpt,
                    title = titL if util.tiOpt else '',
                    outFile = outFile+'water_depth_radius_st' + str(i),
                    xran = [min(data[0][countr])-25,
                                    max(data[0][countr])+25])   
        countr += 1

if __name__=='__main__':
    main(sys.arg[1], sys.arg[2])
