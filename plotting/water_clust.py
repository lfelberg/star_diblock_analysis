'''
Module for making plots of interior water
cluster size and counts
'''
import numpy as np
import importlib
import starPlots
importlib.reload( starPlots )
waterPlot = starPlots.tempPlot

import starDat
importlib.reload( starDat )
getData = starDat.manyDat

import util
importlib.reload( util )

def main(arg1, arg2):
    starList, starGel, term = arg1, arg2, 'dat'
    if 1 in starGel: term = 'gel'
    datFile = util.allDir + 'water_clu.' + term
    outFile = util.outDir + 'water_int_'

    ###############
    # Getting data
    ###############
    data = getData(datFile, starList)

    ###############
    # Plotting the files at given temp
    ###############
    runType = [ 'count', 'size']
    if   util.temp == 350: datLoc = [2, 6] # for 350K
    elif util.temp == 400: datLoc = [3, 7] # for 400K
    xran = [[-0.5,6.5],[1-0.5,5+0.5]]


    for i in range(len(runType)):
        # Finding xmax and xmin
        nonZ = []
        for j in range(len(starList)):
             nonZ.append(np.where(np.array(data[datLoc[i]][j])!=0)[0].tolist())
        xma = max(max(nonZ))
        xmn = min(min(nonZ))
        waterPlot(starList, starGel,
                'bar',
                data[0],
                data[datLoc[i]],
                hphob = False,
                lege = util.legOpt,
                title = 'Interior water cluster '  \
                            + runType[i] +' ' + str(util.temp) + 'K',
                outFile = outFile+runType[i],
                xran = xran[i] if runType[i] == 'size' \
                        else [xmn-0.5,xma+0.5]  )

    ###############
    # Plotting each star at all temps
    ###############
    countr = 0
    for i in starList:
        temp=[300,350,400,450]
        tdatX = [data[0][countr]]*4
        datTemp = [[data[1][countr],data[2][countr],
                        data[3][countr],data[4][countr]],
                        [data[5][countr],data[6][countr],
                        data[7][countr],data[8][countr]]]
        countr += 1

        # Finding xmax and xmin
        nonZ  = np.where(np.array(datTemp[0][3]) != 0)
        xma = max(nonZ[0])
        xmn = min(nonZ[0])

        for j in range(len(runType)):
            waterPlot(temp, [0,0,0,0],
                'bar',
                tdatX,
                datTemp[j],
                hphob = True,
                yphob = [i],
                lege = util.legOpt,
                title = 'Interior water cluster '  \
                            + runType[j] + ' ',
                outFile = outFile+runType[j] + '_st',
                xran = xran[j] if runType[j] == 'size' \
                            else [xmn-0.5,xma + 0.5] )

if __name__=='__main__':
    main(sys.arg[1], sys.arg[2])
