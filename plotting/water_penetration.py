'''
Module for making plots of distance of
interior water to bulk and to core
'''
import importlib
import starDat
importlib.reload( starDat )
getData = starDat.manyDat

import starPlots
importlib.reload( starPlots )
watPlot = starPlots.tempPlot

import util
importlib.reload( util )

def main(arg1, arg2):
    starList, starGel, term = arg1, arg2, 'dat'
    if 1 in starGel: term = 'gel'
    watFile = util.allDir + 'water_depth.' + term
    outFile = util.outDir + 'water_depth_to_'

    data = getData(watFile, starList)

    ###############
    # Plotting the files at 350K
    ###############
    runType = [ 'bulk', 'core']
    datLoc = [11, 3]
    xran = [[0,25],[0,40]]
    for i in range(len(runType)):
            watPlot(starList, starGel,
                'many',
                data[0],
                data[datLoc[i]],
                yErr = data[datLoc[i]+1],
                hphob = False,
                lege = util.legOpt,
                title = 'Distance to ' + runType[i] + ', 350K' \
                            if util.tiOpt else '',
                outFile = outFile+runType[i] + '_st',
                xran = xran[i]  )


    ###############
    # Plotting each star at all temps
    ###############
    countr = 0
    for i in starList:
        temp=[300,350,400,450];tdatX = [data[0][countr]]*4
        datDist = [
                   [data[9][countr],data[11][countr],
                    data[13][countr],data[15][countr]],
                   [data[1][countr],data[3][countr],
                    data[5][countr],data[7][countr]],
                   ]
        errDist = [
                   [data[10][countr],data[12][countr],
                    data[14][countr],data[16][countr]],
                   [data[2][countr],data[4][countr],
                    data[6][countr],data[8][countr]],
                  ]
        countr += 1

        for j in range(len(runType)):
            watPlot(temp, [0,0,0,0],
                'many',
                tdatX,
                datDist[j],
                yErr = errDist[j],
                hphob = False,
                yphob = [i],
                lege = util.legOpt,
                title = 'Distance to ' + runType[j] \
                           if util.tiOpt else '' ,
                outFile = outFile+runType[j]+'_st' + str(i),
                xran = xran[j]  )

if __name__=='__main__':
    main(sys.arg[1], sys.arg[2])
