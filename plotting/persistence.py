'''
Module for obtaining and plotting
the persistence data
'''
import importlib
import starDics
importlib.reload( starDics )

import starDat
importlib.reload( starDat )
getData = starDat.manyDat

import starPlots
importlib.reload( starPlots )
perstPlot = starPlots.tempPlot

import util
importlib.reload( util )

def main(arg1, arg2):
    starList, starGel, term = arg1, arg2, 'dat'
    if 1 in starGel: term = 'gel'
    datFile = util.allDir + 'persistence.' + term
    outFile = util.outDir + 'persistence'
    if starList == [30, 31]:
        datFile = util.allDir + 'persistence_sidePox.dat'
        outFile += '_side'

    ###############
    # Getting data
    ###############
    data = getData(datFile, starList)

    ###############
    # Plotting persistence
    ###############
    runType = ['innermost',  'outermost']
    figNam = ['',  '_wrt_last']
    datLoc = [1,9]

    countr = 0
    for i in starList:
        temp=[300,350,400,450]
        starSpecs =  starDics.starAll if (starGel[countr] == 0) \
                                                      else starDics.gelAll

        tStart = starSpecs[i][1][2]
        glls = [0,0,0,0]
        name = starSpecs[i][1][0]
        for j in range(len(runType)):

            nam = starSpecs[i][1][4] if starList == [30, 31] \
                                                   else starSpecs[i][1][3+j]
            title = 'Intermonomer correlation, '
            title += 'from ' + runType[j] + ' '
            title += nam + ' \n' + name
            tdatX = [data[0][countr]]*4
            tdat = [[0]*len(data[0][countr]) \
                            for m in range(4)]
            tdatErr = [[0]*len(data[0][countr]) \
                            for m in range(4)]
            count = 0
            for k in range(datLoc[j],datLoc[j]+7,2):
                tdat[count] = data[k][countr]
                tdatErr[count] = data[k+1][countr]
                count += 1

            if runType[j] == 'outermost':
                tdatX = [[max(tdatX[tStart:][0]) - xx
                                for xx in x] for x in tdatX]

            perstPlot( temp[tStart:], glls[tStart:],
                    'many',
                    tdatX[tStart:],
                    tdat[tStart:],
                    [[xx/1.5 for xx in x] for x in tdatErr[tStart:]],
                    lege = util.legOpt,
                    title = title, # if util.tiOpt else '',
                    outFile = outFile+figNam[j]+'_st'+str(i),
                    xran = [0, max(tdatX[tStart:][0])+0.5])
        countr += 1

if __name__=='__main__':
    main(sys.arg[1], sys.arg[2])

