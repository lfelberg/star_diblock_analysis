'''
Module for obtaining and plotting
the Rg data as a function of simulation len
'''
import importlib
import starDics
importlib.reload( starDics )
starSpecs=starDics.starAll

import util
importlib.reload( util )

import matplotlib.pyplot as plt
import numpy as np

def getData(datFile, starNum):
    """Function to get data from sprp file"""
    lines = open(datFile).readlines()
    dta, ct = -1, 0
    for line in lines:
        tsv = line.split()
        if dta == -1:
            data = np.zeros((len(tsv),len(lines))) if 'sprp' in datFile else \
                       np.zeros(len(lines)) #for vor
            dta = 1     
        if 'sprp' in datFile:       
            for i in range(len(tsv)): # for rg
                data[i][ct] = float(tsv[i])
        else:
            data[ct] = float(tsv[8])
        ct+=1
    
    return data

def main(arg1, arg2):
    starList, starGel, term = arg1, arg2, 'dat'
    if 1 in starGel: term = 'gel'

    plt.rcParams.update({'font.size': 12})
    fig     = plt.figure(1, figsize = (3, 3))
    ax     = fig.add_subplot(1,1,1)
    countr = 0; lin = ['-',':','--']

    for i in starList:
        temp=[350,400,450]
        
        st = 'star'; tempct = 0
        if starGel[countr]: st += 'g'
        
        for j in temp:
            datFile = util.datDir+st+str(i)+'/'+str(j)+\
                            'K/stargaze_' + st + str(i)+ '_' + \
                            str(j)+ 'Kw.sprp' #for rg
            datFile = '/Users/lfelberg/Documents/POX/rg_v_time/stargaze_' \
                       + st + str(i)+ '_' + str(j)+ 'Kw.sprp' #for rg
            #datFile = util.outDir + 'star' + str(i) + \
            #                '_' + str(j) + 'vor.dat'
            data = getData(datFile, starList)
        
            if 'sprp' in datFile:
                tStart = data[1]/1000.;rg = data[-4]
            else:
                tStart = range(len(data)); rg = data
            
            pltSp =  starDics.starAll if (starGel[countr] == 0) \
                                                      else starDics.gelAll
            ax.plot( tStart, rg, 
                        color = pltSp[i][0], linestyle = lin[tempct],
                        label = pltSp[i][1][0] + ', ' +str(j))   
            tempct += 1
        countr += 1

    ax.legend( columnspacing = 0.4,                                                       
               fontsize =  7.0 ,
               handletextpad = 0.2,                                                       
               handlelength = 1.3,                                                        
               borderaxespad = -0.9,
               loc = 8 , 
               ncol = 2,
               bbox_to_anchor=(0.02, .05, 1., 1.102), 
               )
    ax.set_ylim([10,22]); ax.set_xlim([0,60])
    nm = ""
    for i in range(len(starList)): nm += "{0}_".format(starList[i])
    ax.set_xlabel('Time (ns)'); ax.set_ylabel('$R_G \, (\AA)$')
    titl = util.outDir+'rg_v_time_st'+nm[:-1]+'.png' if 'sprp' in datFile else \
                util.outDir+'vor_v_time_St'+nm[:-1]+'.png'
    plt.savefig(titl, bbox_inches='tight', dpi=300)
    plt.close()

if __name__=='__main__':
    main(sys.arg[1], sys.arg[2])

