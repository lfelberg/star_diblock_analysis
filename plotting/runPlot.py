import importlib
import util
importlib.reload( util )
starPlt = util.starPlt
starName = util.starName
orienLeg = util.orienLeg

import starDics
importlib.reload( starDics )
starAll  = starDics.starAll
starGrps = starDics.starGrps
starGel  = starDics.starGel

for i in util.pltS:
    grpCt = 0
    for j in util.ind:
        print ("Plotting", i, " for ", j)
        if i == 'dihedral_plot':
            import dihedral_plot
            dihedral_plot.main(starGrps[j], starGel[j])
        if i == 'hydrogen_bonds':
            import hydrogen_bonds
            hydrogen_bonds.main(starGrps[j], starGel[j])
        if i == 'massden':
            import massden
            massden.main(starGrps[j], starGel[j], starName[grpCt])
        if i == 'orient_autocorr':
            import orient_autocorr
            orient_autocorr.main(starGrps[j], starGel[j], orienLeg[grpCt], 
                                 starName[grpCt])
        if i == 'persistence':
            import persistence
            persistence.main(starGrps[j], starGel[j])
        if i == 'rgyr':
            import rgyr
            rgyr.main(starGrps[j], starGel[j])
        if i == 'rgyr_v_time':
            import rgyr_v_time
            rgyr_v_time.main(starGrps[j], starGel[j])
        if i == 'sidechain_len':
            import sidechain_len
            sidechain_len.main(starGrps[j], starGel[j])
        if i == 'surface_area_vol':
            import surface_area_vol
            surface_area_vol.main(starGrps[j], starGel[j])
        if i == 'voronoi':
            import voronoi
            voronoi.main(starGrps[j], starGel[j])
        if i == 'voronoi_side':
            import voronoi_side
            voronoi_side.main(starGrps[j], starGel[j])
        if i == 'water_clust':
            import water_clust
            water_clust.main(starGrps[j], starGel[j])
        if i == 'water_neigh':
            import water_neigh
            water_neigh.main(starGrps[j], starGel[j])
        if i == 'water_penetration':
            import water_penetration
            water_penetration.main(starGrps[j], starGel[j])
        grpCt += 1

###############################
## Things you may want to change
###############################
### Which group of stars to use
#ind = 5
#
##starGrps = [ [ 8, 12, 25, 15],         #0
#                    #[ 12, 15, 25],             #1
#                    #[ 24, 26, 27, 30, 31], #2
#                    #[ 6, 24, 26, 27],         #3
#                    #[ 8, 35],                     #4
#                    #[ 24, 31, 32, 33, 34], #5
#                    #[ 30, 31],                   #6
#                    #[ 24, 31, 32, 34],       #7
#                    #[ 8, 12, 15]                #8
#                    #[ 8, 12]                      #9
#
#
#pltS = ['water_neigh']
### Which plots to make:
##starPlt =  'dihedral_plot',
#                #'hydrogen_bonds'
#                #'massden',
#                #'orient_autocorr',
#                #'persistence',
#                #'rgyr',
#                #'sidechain_len'
#                #'voronoi',
#                #'water_clust',
#                #'water_neigh',
#                #'water_penetration'
#
### Whether or not to include a legend
#legOpt=False
### Whether or not to include a title
#tiOpt=False
#
### Location of star home dir
#homeDir = '/Users/lfelberg/star_diblock_copolymer/'
### Location of output
#outDir = '/Users/lfelberg/Desktop/'
#
#stplt = util.PlotStar(ind, pltS, [legOpt, tiOpt], [homeDir, outDir])
