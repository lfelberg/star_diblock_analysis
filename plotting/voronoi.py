'''
Module for making voronoi plots
'''
import numpy as np
import importlib
import starDat
importlib.reload( starDat )
getData = starDat.manyDat

import starPlots
importlib.reload( starPlots )
vorPlot = starPlots.tempPlot

import util
importlib.reload( util )

def main(arg1, arg2):
    starList, starGel, term = arg1, arg2, 'dat'
    if 1 in starGel: term = 'gel'
    vorFile = util.allDir + 'voronoi.' + term
    outFile = util.outDir + 'vor'

    ###############
    # Getting data
    ###############
    data = getData(vorFile, starList)

    ###############
    # Plotting the files
    ###############
    plotname = outFile+'_phob_phob_star'
    titl = 'Normalized interfacial area \n'
    tempRan = [min(min(p) for p in data[0])-25,
                        max(max(p) for p in data[0])+25]
    temp = data[0]

    # Normalization constants from file
    corNorm    = data[1]; phobNorm = data[2]
    linkNorm    = data[3]; philNorm    = data[4]

    # contains data for various voronoi plots,
    # see plotname or titlList for details
    #y[i+1]=error of y[i]
    ys = [21, 31, 17, 29, 19, 17]
    ifAll = [ 1, 1, 0, 0, 0, 0, 0]
    normLoc = [ 2, 4, 2, 4, 2, 4, 4]

    # to find self-self, need to subtract from one
    ones = np.ones(data[1].shape)

    plotname = ['_phob_phob', '_phil_phil',
                '_normphob_phil','_phil_wat',
                '_phob_wat','_normphil_phob', '_poly_wat']
    titlList = [' Hphob/Hphob', ' Hphil/Hphil',
                   ' Hphobic/Hphil WRT Hphobe',
                   ' Hphil/Water', ' Hphobic/Water',
                   'Hphobic/Hphil WRT Hphil',
                   ' Water/entire polymer']

    # Actual plot call here
    for i in range(len(ys)):
        ydat, err = [], []
        for j in range(len(data[ys[i]])):
            ydat.append(data[ys[i]][j]/data[normLoc[i]][j][0])
            err.append(data[ys[i]+1][j]/data[normLoc[i]][j][0])
        if ifAll[i]:
            ydat = ones - ydat

        vorPlot(starList, starGel,
                    'many',
                    temp, ydat, err,
                    hphob = False,
                    lege = util.legOpt,
                    title = titl + titlList[i] ,
                    outFile = outFile+plotname[i],
                    xran = tempRan)


    ##################################
    ## Hydrophilic with all other star material
    ##################################
    ys = [9, 17,]
    normLoc = 4

    plotname = ['_normphil_allphob']
    titlList = ['Hphil/All hydrophobic',]
    ydat = (data[ys[0]]+
                data[ys[1]])/data[normLoc]
    err   = (data[ys[0]+1]+
                data[ys[1]+1])/data[normLoc]

    ydat = (data[29])/data[4]
    err = data[30] / data[4]
    ydat = ones - ydat

    vorPlot(starList, starGel,
                'many',
                temp, ydat, err,
                hphob = False,
                lege = util.legOpt,
                title = titl + titlList[0] ,
                outFile = outFile+plotname[0]+'st',
                xran = tempRan)



    ############################
    ## CORE
    ###########################
    if 1 in util.starGel:
        ys = [5, 9, 11, 13, 5, 9]
        ifAll = [ 0, 0, 0, 1, 0, 0]
        normLoc = [ 1,1,1,1,2,4]

        # to find self-self, need to subtract from one
        ones = np.ones(data[1].shape)
        plotname = ['_normcore_phob_', '_core_phil_',
                    '_core_wat_', '_core_core_',
                    '_normphob_core_',
                    '_normphil_core_']
        titlList = [' Core/Hphob WRT Core', ' Core/Hphil',
                    ' Core/Water', ' Core/core' ,
                    'Hphob/Core WRT Hphob',
                    'Hphil/Core WRT Hphil']

        # Actual plot call here
        for i in range(len(ys)):
            ydat = data[ys[i]]/data[normLoc[i]]
            err   = data[ys[i]+1]/data[normLoc[i]]
            if ifAll[i]:
                ydat = ones - ydat

            vorPlot(starList, starGel,
                        'many',
                        temp, ydat, err,
                        hphob = False,
                        lege = util.legOpt,
                        title = titl + titlList[i] ,
                        outFile = outFile+plotname[i]+'st',
                        xran = tempRan)

if __name__=='__main__':
    main(sys.arg[1], sys.arg[2])
