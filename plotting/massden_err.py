'''
This module is for plotting
mass density functions given .his1 files
(output of stargaze program)
'''
import importlib
import starDat
importlib.reload( starDat )
getData = starDat.oneDat

import starPlots
importlib.reload( starPlots )
makePlot = starPlots.tempPlot

import util
importlib.reload( util )
starList = util.starGrp

##############################
# Getting data and plotting
##############################
ct = 0
mdNames = ['All', 'Core', 'Hphob', 'Hphil', 'Water']

for i in starList:
    st = 'star'
    if util.starGel[ct]:
        st += 'g'
    hisFile = util.datDir + st + str(i)
    hisFile += '/' + str(util.temp)+ 'K/stargaze_' + st
    hisFile += str(i)+ '_' +str(util.temp)+'Kw.his1'
    dirOuput = util.outDir + '/hist_all_' + st
    dirOuput +=  str(i)+'_' + str(util.temp) + 'K'

    titlPlt = 'Mass density, ' + util.starName[ct] + ' 350K'

    radius, massDen = getData(hisFile, i, util.starGel[ct])

    mdErr = [massDen[1], massDen[3], massDen[5], massDen[7]+massDen[9], massDen[11]]
    massDen = [massDen[0], massDen[2], massDen[4], massDen[6]+massDen[8], massDen[10]]

    makePlot(mdNames, util.starGel[ct],
                    'one',
                    radius,
                    massDen,
                    yErr = mdErr,
                    yphob = i,
                    lege = util.legOpt,
                    title = titlPlt if util.tiOpt else '',
                    outFile = dirOuput,
                    xran = [0, 40])
    ct += 1
