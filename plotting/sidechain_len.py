'''
Module for obtaining and 
    plotting the sidechain lengths
'''
import numpy as np
import importlib
import starPlots  
importlib.reload( starPlots )
lenPlot = starPlots.tempPlot

import starDat 
importlib.reload( starDat )
getData = starDat.manyDat

import util  
importlib.reload( util )

def main(arg1, arg2):
    starList, starGel, term = arg1, arg2, 'dat'
	titL = 'Sidechain length '
	datFile = util.allDir + 'sidelen.dat'
	outFile = util.outDir + 'sidechain_len'

	data = getData(datFile, starList) 
	 
	if datFile.endswith('sori'):                
		n, bins = np.histogram(data, 
						bins=np.arange(2.0,10.0, 0.2))
		n = n/float(sum(n))
		for a in n:
			print ("{0:5.4f}".format( a ))

	else:
		countr = 0
		for i in starList:
			temp=[300,350,400,450]
			tdatX = [data[0][countr]]*4
			tdatCt = [data[1][countr],data[1][countr],
							data[2][countr],data[3][countr]]
			countr += 1
			lenPlot(temp, [0, 0, 0, 0],
				'bar',
				tdatX, 
				tdatCt,
				hphob = True,
				yphob = [i], 
				lege = util.legOpt,
				title = 'Sidechain length'  if util.tiOpt else '', 
				outFile = outFile+ '_st', 
				xran = [min(tdatX[0])-0.1, max(tdatX[0]) + 0.01] )  

if __name__=='__main__':
    main(sys.arg[1], sys.arg[2])

