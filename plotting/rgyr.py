'''
Module for making Rg and anisotropy plots
'''
import importlib
import starPlots
importlib.reload( starPlots )
tempPlot = starPlots.tempPlot

import starDat
importlib.reload( starDat )
getData = starDat.manyDat

import util
importlib.reload( util )

def main(arg1, arg2):
    starList, starGel, term = arg1, arg2, 'dat'
    if 1 in starGel: term = 'gel'
    hphobOpt, yran = True, []
    runType = [ 'rgyr',  'anis']
    datLoc = [1,5]

    ##############################
    # Getting data and plotting
    ##############################
    for typ in runType:
        outFile = util.outDir  + typ
        datFile = util.allDir + typ +'_all.' + term
        data = getData(datFile, starList)

        yran = [0,30]
        if (typ == 'anis'):
            yran = [0.0, 0.15]
            if (term == 'gel'):
                yran = [0.0, 0.3]

        temp = [min(min(p) for p in data[0])-25,
                    max(max(p) for p in data[0])+25]
       
        tempPlot(starList, starGel,
                    'many',
                    data[0],
                    data[datLoc[0]], #- data[datLoc[1]],
                    data[datLoc[0]+1],
                    hphobOpt,
                   #False, 
                    data[datLoc[1]],
                    data[datLoc[1]+1],
                    lege = util.legOpt,
                    title = typ if util.tiOpt else '',
                    outFile = outFile,
                    xran = temp,
                    yran = yran)

if __name__=='__main__':
    main(sys.arg[1], sys.arg[2])
