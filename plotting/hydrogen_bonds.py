'''
Module for obtaining and plotting hydrogen bonds
'''
import importlib
import starPlots  
importlib.reload( starPlots )
bondPlot = starPlots.tempPlot

import starDat 
importlib.reload( starDat )
getData = starDat.manyDat

import util  
importlib.reload( util )

def main(arg1, arg2):
    starList, starGel, term = arg1, arg2, 'dat'
    if 1 in starGel: term = 'gel'
    titL = 'Hydrogen bonds versus temperature'
    datFile = util.allDir + 'hbonds.dat'
    outFile = util.outDir + 'hbonds_'
    #datFile = util.allDir + 'mix_vor.dat'
    #outFile = util.outDir + 'mix_vor_'
    starList = util.starGrp

    ###############
    # Getting data
    ###############
    data = getData(datFile, starList)

    bondPlot(starList, starGel,
                    'many',
                    data[0], data[1], data[2],
                    hphob = False,
                    lege = util.legOpt,
                    title = titL if util.tiOpt else '',
                    outFile = outFile+'st')

if __name__=='__main__':
    main(sys.arg[1], sys.arg[2])
