'''
Storing some important file names
'''
import starDics
import importlib
importlib.reload( starDics )
starAll  = starDics.starAll
starGrps = starDics.starGrps
starGel  = starDics.starGel
starPlt  = starDics.starPlt

#############################
 #Things you may want to change
#############################
# Which group of stars to use
ind = [ 
#       2 ,
#       7 ,
        14,
#       3,
      ]

#starGrps = [ [ 8, 12, 25, 15],        #0
                    #[ 12, 15, 25],    #1
                    #[ 24, 26, 27, 30, 31], #2
                    #[ 26, 27, 30, 31],     #3
                    #[ 8, 35],              #4
                    #[ 24, 31, 33, 34],     #5
                    #[ 30, 31],             #6
                    #[ 24, 33, 34],         #7
                    #[ 8, 12, 15]           #8
                    #[ 8, 12]               #9
                    #[ 12, 15],             10
                    #[ 24, 26, 30, 31]      #11
                    #[ 24, 26, 31]          #12
                    #[ 8, 12, 13, 25],      #13
                    #[ 8, 35, 36, 37]       #14
                    #[ 13 ]                 #15
                    #[ 4, 6, 8, 9],         #16
                    #[ 4, 8 ],              #17
                    #[ 24, 27, 30]          #18

## Which plots to make:
pltS = [
        'dihedral_plot',
#       'g_of_r',
#       'hydrogen_bonds',
#       'massden_err',
#       'massden',
#       'orient_autocorr',
#       'persistence',
#       'rgyr',
#       'rgyr_v_time',
#       'sidechain_len',
#       'surface_area_vol',
#       'voronoi',
#       'voronoi_side',
#       'water_clust',
#       'water_neigh',
#       'water_penetration',
       ]

## Whether or not to include a legend
legOpt=False
legOpt=True
## Whether or not to include a title
tiOpt=True
tiOpt=False

## Temperature for individual star plots (mass den)
temp = 400

##############################
## Location of star home dir
#homeDir = '/Users/lfelberg/Documents/star_diblock_copolymer/'
homeDir = '/Users/lfelberg/star_diblock_copolymer/'
## Location of script files
scriptLoc = homeDir + 'analysis/plotting/'
## Location of data files
datDir = homeDir + 'MATLAB/'
allDir = datDir + '/star_all/'
## Location of output
outDir = '/Users/lfelberg/Documents/pubs/star_alkane/tex/plots/'

#############
#############
## Different repeat units of each polymer
starName = [[] for i in ind]
orienLeg = [[[] for j in starDics.starGrps[i]] for i in ind]
phobloc = [ 1, 8, 16]
phibloc  = [ 1, 11, 23]
ct = 0
for i in ind:
    ict = 0
    for j in starDics.starGrps[i]:
        starAll = starDics.starAll if (starGel[i][ct]==0) else starDics.gelAll
        if i == 9: phibloc = [1, 5, 11]
 
        # Creating a list of star polymer names
        starName[ct] += [starAll[j][1][0]]
 
        nphob, nPts = 3, len(starAll[j][2])
        if nPts == 4:   nphob = 2
        elif nPts == 3: nphob = 0
        for k in range(nPts):
            if k < nphob:
                name = starAll[j][1][3].split('-')[0]
                name += ' ' + str(phobloc[k])
            else:
                name = starAll[j][1][4].split('-')[0]
                name += ' ' + str(phibloc[k-nphob])
            orienLeg[ct][ict] += [name]
        ict += 1
    ct += 1

#class PlotStar:
#    """A class for plotting star properties"""
#
#    starName = [ ]
#
#    def __init__(self, groupInd, plotList, options,
#                        dirs, temp = 350):
#        """ Constructor"""
#        self.ind = groupInd
#        self.starGrp = starGrps[self.ind]
#        self.starGl = starGel[self.ind]
#        self.pltLst = plotList
#        self.legOpt = options[0]
#        self.tiOpt = options[1]
#        self.homeDir = dirs[0]
#        self.scriptLoc = self.homeDir + 'MATLAB/scripts/'
#        self.datDir = self.homeDir + 'MATLAB/star_all/'
#        self.outDir = dirs[1]
#        self.temp = temp
#
#        self.makeName()
#
#    def makeName(self):
#        """ Function to make list of names"""
#        for i in self.starGrp:
#            self.starName += [starAll[i][1][0]]
#
#    def plots(self):
#        """Plots all plots on list"""
#        for pl in self.pltLst:
#            if i == 'all':
#                for name in starPlt:
#                    print name
#                    if ( name == 'sidechain') \
#                        and (self.starGrp != [30, 31]):
#                        continue
#                    elif ( name == 'hydrogen_bonds') \
#                        and (self.starGrp != [ 12, 15, 25]):
#                        continue
#                    else:
#                        execfile(self.scriptLoc+name+'.py')
#                        continue
#
#            else:
#                execfile(self.scriptLoc+i+'.py')
