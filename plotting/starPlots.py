'''
 File to store all plotting functions for each
 data analysis method
'''
import numpy as np
import itertools
import pylab
import matplotlib
import matplotlib.pyplot as plt
import importlib
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage,
                                  AnnotationBbox)

import starDics  
importlib.reload( starDics )

colors = starDics.colors
orienCol = starDics.monomerColors

def flip(items, ncol):
    ''' Flipping the order of a legend '''
    return itertools.chain(*[items[i::ncol]
                   for i in range(ncol)])


def barPlot(ax, leg, x, y, stars, gels,
                    tempFlag, title, fign, starNo):
    ''' Plotting a bar graph '''
    if ((starNo == []) and (stars[0] in starDics.starAll)):
        pltSp =  starDics.starAll
    elif ((starNo[0] == []) and (stars[0] not in starDics.starAll)):
        pltSp =  starDics.gelAll
    elif (starNo[0] in starDics.starAll):
        pltSp =  starDics.starAll
    else:
        pltSp = starDics.gelAll
    ranSt = pltSp[starNo[0]][1][2] if tempFlag ==1 else 0
    wid = x[0][1] - x [0][0] if isinstance(x[0][0], float) else 1
    wid = (0.95*wid)/(len(stars)-ranSt)
    start=1 if "count" in title else 0
    start=20 if "res" in title else start
    xDt, clrs = [], []

    for i in range(0,len(x)-ranSt):
        pltSp =  starDics.starAll if (gels[ranSt+i] == 0) \
                                         else starDics.gelAll
        xDat = x[ranSt+i] if isinstance(x[ranSt+i][0], float) \
                    else range(len(x[ranSt+i]))
        xDt.append(xDat)
        
        yDt = [float(i) for i in y[ranSt+i]]
        ax.bar([dat+i*wid-0.5-start for dat in xDat],
                yDt, label = pltSp[stars[ranSt+i]][1][0],
                color=tuple(pltSp[stars[ranSt+i]][0]),
                edgecolor=tuple(pltSp[stars[ranSt+i]][0]),
                width=wid)
        leg = leg+[pltSp[stars[ranSt+i]][1][0]]
        clrs = clrs + [pltSp[stars[ranSt+i]][0]]
        if tempFlag==0:
            fign += '%d_' % stars[i]
        elif i==(0):
            fign += '%d_' % starNo[0]

    maxlen=[len(p) for p in xDt]
    if (len(x[maxlen.index(max(maxlen))]) < 14):
        if start < 2:
            ax.set_xticks([dat-start for dat in
                            xDt[maxlen.index(max(maxlen))]])
        else:
            ax.set_xticks([dat for dat in
                            xDt[maxlen.index(max(maxlen))]])
    if not isinstance(x[0][0], float):
        ax.set_xticklabels(x[0], fontsize = 5.)

    return(leg, fign, clrs)


def manyPlot(ax, leg, x, y, yErr,
                    hphob, yphob, yphobErr,
                    stars, gels,
                    title, fign):
        '''Plotting a multi-data graph'''
        for i in range(len(stars)):
            star    = stars[i]
            pltSp =  starDics.starAll if (gels[i] == 0) \
                                                  else starDics.gelAll
            clr      = tuple(pltSp[star][0])
            mark  = pltSp[star][1][1]
            name = pltSp[star][1][0]
            if yErr == []:
                plt.plot(x[i], y[i], linewidth = 1.5,
                        color=clr, label = name)
            else:
                plt.errorbar(x[i], y[i], yErr[i],
                    color=clr,
                    marker=mark,
                    markersize=4,
                    mec=clr, label = name)
            leg += [name]
            if hphob: # If there is a hphob part to data
                plt.errorbar(x[i], yphob[i], yphobErr[i],
                    color= clr,
                    marker=mark,
                    markersize=4,
                    mec=clr, mfc='none', label = name+" Hphob" )
                leg += [name + ' Hphob']
            if star < 100:
                fign=fign+ '%d_' % star

        maxlen=[len(p) for p in x]
        if len(x[maxlen.index(max(maxlen))]) < 10:
            ax.set_xticks(x[maxlen.index(
                                            max(maxlen))])
        return(leg, fign)


def onePlot(ax, legen, x, y, yErr, star, gels, title, fign, outFile):
        '''Plotting a one star data graph'''
        leg = []
        pltSp =  starDics.starAll if (gels == 0) else starDics.gelAll
        
        if (outFile.find('radius')!=-1):# for water pen plots
            pltSp =  starDics.starAll if (gels[1] == 0) else starDics.gelAll
            name = pltSp[legen[1]][1][0]
            col  = tuple(pltSp[legen[1]][0])
            stmk = pltSp[legen[1]][1][1]
            radCol = [ [0,0,0], col, col ]
            mCol = [ [0,0,0], col, 'none' ]
            radMk = [ '^', stmk, stmk ]
            lege = ['Average cluster depth', name + ' total radius',
                        name + ' hphob radius']
        elif (outFile.find('his')!=-1):
            radCol = ['blue','green','red','cyan', 'magenta']
            mCol = radCol
            radMk = [ '.', '.', '.', '.', '.', '.' ]

        for i in range(len(y)):
            if (outFile.find('radius')==-1):# for water pen plots
                if legen[i] == 'Hphob':   leg += [pltSp[star][1][3]]
                elif legen[i] == 'Hphil': leg += [pltSp[star][1][4]]
                else:                     leg += [legen[i]]
            # for water penetration plots and massden
            if yErr != []:
                xval = x[i] if 'radius' in outFile else x
                plt.errorbar(xval, y[i], yErr[i],
                    color= radCol[i],
                    marker=radMk[i],
                    markersize=4,
                    mec=radCol[i], mfc=mCol[i],
                    label = leg[-1] )

                if 'radius' in outFile:
                    maxlen=[len(p) for p in x]

                    if len(x[maxlen.index(max(maxlen))]) < 10:
                         ax.set_xticks(x[maxlen.index(
                                                 max(maxlen))])
            else:
                if (outFile.find('orient')!=-1):
                    plt.plot(x, y[i], color = orienCol[legen[i]],
                            linewidth = 1.5,
                            label = legen[i])
                    if "_ln" in outFile: ax.set_xscale("log", nonposx='clip')
                elif y[i].any != 0.0: #If not blank
                    ax.plot(x, y[i], linewidth = 1.5,
                            label = legen[i])

        fign=fign+ '_'
        return(leg if (outFile.find('radius')==-1) else lege,
                            fign)


def legPlt( titl, fig, ax, leg, lgloc, ncl, bar = False, clrs = [] ):
    '''Putting up the legend'''
    handles, labels = ax.get_legend_handles_labels()
    ncl = 2
    if (lgloc == 9):
         bbox = (0., 0.99, 1., .102)
    elif (lgloc == 8):
        bbox = (0., 0.15, 1., .102)
        handles = flip(handles, ncl)
        labels = flip(labels, ncl)
    elif (lgloc == 2):
        bbox = ( 0.5, 0.85, 1, 0.102)
    else:
        bbox = (0.04 , 0.04, 0., 0.)

    if bar:
        figleg = plt.figure(figsize=(.05, .05))
        patches = [ matplotlib.patches.Patch(color=clr, label=lg)
            for lg, clr in zip(leg, clrs)]
        figleg.legend(patches, leg, loc='center', 
                      ncol = ncl,
                      columnspacing = 0.4,
                      fontsize =  10,
                      handletextpad = 0.2,
                      handlelength = 1.5,
                      borderaxespad = -0.9,)
    else:
        plt.rcParams.update({'font.size': 2})
        figleg  = plt.figure(2, figsize = ( .05,  .05)) 
        axleg = figleg.add_subplot(111)
        legend = plt.figlegend(handles, labels, loc = "center", 
                      ncol = ncl,
                      columnspacing = 0.4,
                      fontsize =  10,
                      handletextpad = 0.2,
                      handlelength = 1.5,
                      borderaxespad = -0.9,)
 
    figleg.savefig(titl+".png",bbox_inches='tight', dpi=300)
    plt.close(figleg)

def tempPlot(stars, gels,
    pltType,
    x, y,
    yErr = [],
    hphob = False,
    yphob = [], yphobErr = [],
    lege = True,
    title = '',
    outFile = None,
    xran = [325, 475], yran = []):
    ''' Function to plot anis, mass dens, persist
    radius of gyration, voronoi results '''

    siz = (3.0,3.0) if "_ln" not in outFile else (1.0,1.0)
    fig     = plt.figure(1, figsize = siz)
    leg    = []
    fign   = '_st' if (type(stars[0]) is not str) and (stars[0] < 100) else ''
    xlab  = 'Temperature (K)'
    ylab  = ''
    ax     = fig.add_subplot(1,1,1)
    lgloc  = 9
    ncl    = len(x)
    fs     = 14 # x/y label font
    ls     =  9 # tick label font 

    if pltType == 'bar':
        leg, fign, clrs = barPlot( ax, leg, x, y, stars, gels, hphob,
                                        title, fign, yphob )
    elif pltType == 'many':
         leg, fign = manyPlot( ax, leg, x, y, yErr,
                    hphob, yphob, yphobErr,
                    stars, gels, title, fign)
    elif pltType == 'one':
        groups = stars # stars here actually dif comp
        star = yphob # yphob here actually a star no
        leg, fign = onePlot( ax, groups, x, y, yErr, star,
                    gels, title, fign, outFile)                    
    else:   
        print ('Plot type not recognised!')
        return

    ax.set_title(title, y= 1.1 if lege else 1.0)

    if (outFile.find('rgyr')!=-1):
        yrang = [0, 30]
        ymax = (max(max(p) for p in y))
        if (ymax > 30 ):
            yrang = [ 0, np.ceil(ymax/10)*10]
        ylab = 'Radius of Gyration ($\AA$)'
    elif (outFile.find('anis')!=-1):
        yrang = [0, .15]
        ylab = 'Anisotropy'
    elif (outFile.find('vor')!=-1):
        ax.set_title(title, y =.83, fontsize = 12);
        yrang = [0.0, 1.0]
        ylab = 'Normalized Interfacial Area'
    elif (outFile.find('hbond')!=-1):
        yrang = [0.0, 0.5]
        ylab = 'Avg. hbonds per PEG monomer'
    elif (outFile.find('radius')!=-1):
        yrang = [0.0, 45]
        ylab = 'Distance from core ($\AA$)'
        lgloc = 2; ncl = 1
    elif (outFile.find('water_res')!=-1):
        yrang = [0.0, 0.05]
        words = title.split(); xlab = ''
        ylab = 'Fraction observed'
        for i in [0,2,3,4,5]:
            xlab += words[i] + ' '
    elif (outFile.find('water_int')!=-1):
        yrang = [0.0, 1.0] if "size" in outFile else [0.0, 0.7]
        words = title.split(); xlab = ''
        ylab = 'Fraction observed'
        for i in [0,2,3]:
            xlab += words[i] + ' '
        ax.set_title('', y= 1.1 if lege else 1.0)
    elif (outFile.find('dihedral')!=-1):
        yrang = [0.0, 0.75]
        ylab = 'Fraction observed'
        xlab = 'Dihedral conformation'
    elif (outFile.find('sidechain')!=-1):
        yrang = [0.0, 1.0]
        ylab = 'Fraction observed'
        xlab = 'Length of sidechain ($\AA$)'
    elif (outFile.find('pers')!=-1):
        yrang = [-0.5, 1.0]
        words = title.split(); xlab = 'Monomer from '
        title = ''
        fs = 10
        ylab = 'Correlation'
        for i in [3,4]:
            xlab += words[i] + ' '
        ax.set_title('', y= 1.1 if lege else 1.0)
    elif (outFile.find('sav')!=-1):
        ylab = 'Surface area ratio'
        lgloc = 8
    elif (outFile.find('his')!=-1):
        yrang = [0.0, 1.6]
        ylab = 'Mass Density ($g/cm^3$)'
        xlab = 'Distance from core ($\AA$)'
    elif (outFile.find('orient')!=-1):
        yrang = [0.0, 1.0]
        ylab = ''
        xlab = 'Time (ns)'
        ncl   = 6
        lgloc=  9
        ls = 8
        if "_ln" in outFile: fs = 8
    elif (outFile.find('g_r')!=-1):
        yrang = [0.0, 30.0]
        ylab = 'g(r)'
        xlab = 'r ($\AA$)'
    elif (outFile.find('depth_to')!=-1):
        yrang = [0.0, 0.1]
        ls = 9
        ylab = 'Local density (molecules/$\AA^3$)'
        xlab = 'Distance ($\AA$)'

    ax.set_xlim(xran); ax.set_ylim( yran if yran != [] else yrang)
    ax.set_ylabel(ylab, fontsize = fs);   ax.set_xlabel(xlab, fontsize = fs)
    ax.tick_params(axis='x',labelsize=ls);ax.tick_params(axis='y',labelsize=ls)
    ax.xaxis.labelpad = -1; ax.yaxis.labelpad = -1
   
    # Adding insert of water size bar to the water count figure
    if (outFile.find('water_int')!=-1):
        if "size" not in outFile:
            fn = outFile.replace("_count","_size")
            fn+= fign[:-1]+ '.png'
            im = plt.imread(fn, format='png')
            xl = ax.get_xlim(); yl = ax.get_ylim()

            newax = fig.add_axes([0.4, 0.4, 0.48, 0.48], anchor='C',)
            newax.imshow(im, extent=[0, 1,0., 1.]) #, zorder=19)
            newax.axis('off')

    if outFile != None:
        plt.savefig(outFile+fign[:-1]+ '.png', format='png',
                        bbox_inches = 'tight', dpi=300, ) #transparent = True)
        # Adding insert of water size bar to the water count figure
        if (outFile.find('orient')!=-1) and "_ln" not in outFile:
           fn = outFile+fign[:-1]+ '_ln.png'
           im = plt.imread(fn, format='png')
           xl = ax.get_xlim(); yl = ax.get_ylim()
 
           newax = fig.add_axes([0.115, 0.09, 0.45, 0.45], anchor='SW',)
           newax.imshow(im, extent=[0, 1,0., 1.]) #, zorder=19)
           newax.axis('off')
           plt.savefig(outFile+fign[:-1]+ '_insert.png', format='png',
                             bbox_inches = 'tight', dpi=300)

        if (lege):
            ln = outFile+fign[:-1]+"_lege"
            if pltType == 'bar':
                legPlt(ln,fig,ax,leg,lgloc,ncl,True,clrs)
            else:
                legPlt(ln,fig,ax,leg,lgloc,ncl)

        plt.close()
    else:
        plt.show()

