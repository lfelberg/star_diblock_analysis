'''
Module for making plots of distance of
interior water to bulk and to core
'''
import importlib
import starDat
importlib.reload( starDat )
getData = starDat.manyDat

import starPlots
importlib.reload( starPlots )
watPlot = starPlots.tempPlot

import util
importlib.reload( util )

starList = util.starGrp
starGel = util.starGel
term = 'gel' if 1 in util.starGel else 'dat'
watFile = util.allDir + 'g_r_poxO_poxN.' + term
outFile = util.outDir + 'g_r_poxO_poxN_'

data = getData(watFile, starList)

###############
# Plotting the files at 350K
###############
runType = [ 'g(r)']
datLoc = [2, 6]
xran = [[0,20]]
for i in range(len(runType)):
        watPlot(starList, starGel,
            'many',
            data[0],
            data[datLoc[i]],
            yErr = [],
            hphob = False,
            lege = util.legOpt,
            title = 'g(r), 350K' \
                        if util.tiOpt else '',
            outFile = outFile+runType[i] + '_st',
            xran = xran[i]  )


###############
# Plotting each star at all temps
###############
countr = 0
for i in starList:
    temp=[350,400,450];tdatX = [data[0][countr]]*3
    datDist = [[data[1][countr],data[2][countr],
                    data[3][countr]]]
    countr += 1

    for j in range(len(runType)):
        watPlot(temp, [0,0,0],
            'many',
            tdatX,
            datDist[j],
            hphob = False,
            yphob = [i],
            lege = util.legOpt,
            title = 'Distance to ' + runType[j] \
                       if util.tiOpt else '' ,
            outFile = outFile+runType[j]+'_st' + str(i),
            xran = xran[j]  )
