'''
This module is for creating orientational autocorrelation
function plots given a *.scor file
'''
import importlib
import starDat
importlib.reload( starDat )
getData = starDat.oneDat

import starPlots
importlib.reload( starPlots )
makePlot = starPlots.tempPlot

import util
importlib.reload( util )

####### For fitting to exponential
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def stretched_exp(x, b, c):
    '''Stretched exponential fit'''
    return np.exp(-pow(x/b, c))

colors = [(0.859, 0.078, 0.234), (0.801, 0.586, 0.047),
            (0., 0.391, 0.), (0.289, 0.461, 1.),
            (0.289, 0., 0.508), (0.777, 0.441, 0.441),
            (0.777, 0.379, 0.078), (0., 0.9297, 0.9297)]

##############################
# Getting data and plotting
##############################
def main(arg1, arg2, arg3, arg4):
    starList, starGel, orienLeg, starName, term = arg1,arg2,arg3,arg4,'dat'
    if 1 in starGel: term = 'gel'
    ct = 0

    for i in starList:
        st = 'star'
        if starGel[ct]:
            st += 'g'
        corFile = util.datDir + st + str(i)
        corFile += '/' + str(util.temp)+ 'K/stargaze_' + st
        corFile += str(i)+ '_' + str(util.temp)+ 'Kw.scor'
        dirOuput = util.outDir + '/orient_autocorr_' + st
        dirOuput +=  str(i)+'_' + str(util.temp)
        titlPlt = 'Time orientational autocorrelation function\n'
        titlPlt += starName[ct] + str(util.temp)+ 'K'

        radius, orienDat = getData(corFile, i, starGel[ct])
        radius = np.array(radius)

        fig = plt.figure(1, figsize = (3.0, 3.0))
        ax  = fig.add_subplot(1,1,1) 
        for j in range(len(orienDat)):
            lt_zero = orienDat[j] < 0
            orienDat[j][lt_zero] = 0.0
            popt, pcov = curve_fit(stretched_exp, radius[-200:-1],
                                   orienDat[j][-200:-1],
                                   bounds=(0, [1.e7, .75]))
            ax.plot(radius[-200:-1], orienDat[j][-200:-1], color = colors[j], 
                     label="Original Data "+str(j))
            ax.plot(radius[-200:-1],stretched_exp(radius[-200:-1], *popt), 'k-', 
                     label="Fitted Curve "+str(j))
        ax.set_ylim([0,1]); ax.tick_params(axis='x',labelsize=9)
        plt.savefig(util.outDir+"exp_fit_st"+str(i)+ '.png', format='png',
                            bbox_inches = 'tight', dpi=300)
        plt.close()

        makePlot(orienLeg[ct], starGel[ct],
                        'one',
                        radius/1000.,
                        orienDat,
                        yErr = [],
                        yphob = i,
                        lege = util.legOpt,
                        title = titlPlt if util.tiOpt else '',
                       #outFile = dirOuput+"_ln",
                        outFile = dirOuput,
                       #xran = [0, 2000])
                        xran = [0, 4]
                )
        ct += 1


if __name__=='__main__':
    main(sys.arg[1], sys.arg[2], sys.arg[3], sys.arg[4])
