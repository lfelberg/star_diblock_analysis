'''
Module for making plots of interior water
residence times
'''
import numpy as np
import importlib
import starPlots
importlib.reload( starPlots )
waterPlot = starPlots.tempPlot

import starDat
importlib.reload( starDat )
getData = starDat.manyDat

import util
importlib.reload( util )

starList = util.starGrp
starGel = util.starGel
term = 'gel' if 1 in util.starGel else 'dat'
datFile = util.allDir + 'water_residence.' + term
outFile = util.outDir + 'water_restime'

###############
# Getting data
###############
data = getData(datFile, starList)

###############
# Plotting the files at 350K
###############
xran = [0, 360]

np.where(np.array(data[2]) != 0)

waterPlot(starList, starGel,
          'bar',
          data[0],
          data[2],
          hphob = False,
          lege = util.legOpt,
          title = 'Interior water cluster residence time (ps)',
          outFile = outFile,
          xran = xran )

###############
# Plotting each star at all temps
###############
countr = 0
for i in starList:
  temp=[300,350,400,450]
  tdatX = [data[0][countr]]*4
  datTemp = [data[1][countr],data[2][countr],
                  data[3][countr],data[4][countr]]
  countr += 1

  waterPlot(temp, [0,0,0,0],
            'bar',
            tdatX,
            datTemp,
            hphob = True,
            yphob = [i],
            lege = util.legOpt,
            title = 'Interior water cluster residence time (ps) ',
            outFile = outFile + '_st',
            xran = xran )
