# star diblock copolymer analysis tools repo

This repo contains analysis tools for analysis of star polymer systems.
It contains the following directories:

1. bash: contains all shell scripts used for post processing
2. bin: contains executables for compiled fortran programs
3. build: contains requisite files for building star polymer systems
4. plotting: contains python files for plotting results
5. python: contains python scripts used for analysis
6. rst2data: contains restart2data exectutables from a variety of lammps versions


## Fortan files in this directory

Most of these are from Bill Swope, and are therefore pretty well commented. 
This gives a brief overview of what each file generally does, how to execute 
it and what it produces.

*center.f* - Program to read in a LAMMPS xyz file and apply a 
centering operation to each frame so that a selected
molecule or site will be situated in the the center of the cell.

    Command line: ./center star_temp.xyz

    Generated file: star_temp.c.xyz, the centered xyz file

*classify.f* - This program classifies sites in a star polymer system 
given the **.lmp file. See file for more details.

    Command line: ./classify classify.in

    Generated file: classification.classify, file with one line of classification
      for each atom in the system

*getvolumes.f* - program to read run.*.inp and run.*.out files and 
make a file of times and volumes, one for each xyz file.

    Command line: ./getvolumes (run in the directory that contains the run
     files, both inp and out, should start from 0 -> 1000)

    Generated file: a .vol file for each .xyz file. A getvolumes.out file
       that provides information about the timespan of each run.

*interiorcheck.f* - Setup inputs for voronoi program to provide further analysis
on interior water cluster environment (whether it is hydrophobic or philic)

    Command line: ./interiorcheck *.cluster

    Generated file: *.interior.vin (you will run voronoi.f with this file)

*interiortable.f* - Read a voronoi.class output file and to match it
with information used to make it so that surface area 
composition can be computed for each interior cluster. It will report the
hydrophilic and phobic water contact area for each instance and compute the 
average. You should change lines 52-60 to reflect different classification names
and different star compositions.

    Command line: ./interiortable *.cluster

    Generated file: *.interior.class

*interior_water_analysis_ADM_SE.f* - Analyze surface distances for interior waters.
Find the water's depth from polymer surface as well has distance from core.

    Command line: ./interior_water_analysis_ADM_SE *.wain

    Generated file: *.waout, contains, at the end, histograms of water depth 
      and distance from the surface

*mixture.f* - Program to read lmp files, each describing a molecule, 
and combine them to make a mixture, mxin file contains details about
the concentration of each component and their lmp filenames

    Command line: ./mixture *.mxin

    Generated file: *.lmp (name designated in *.mxin file)

*orientation.f* - This files the orienational autocorrelation functions

    Command line: ./orientation *.sori

    Generated file: *.scor

*peg_dihdral.f* - Look at the dihedral conformers of PEG backbone of each arm of a 
star polymer

    Command line: ./peg_dihedral *.rgin (also expects torin file, which is 
     identical to rgin)

    Generated file: *.torout, verbose program run, but also at the end
      a histogram of backbone triplet conformers.

*radiusofgyr.f* - Look at the Rg of some portion of each arm of a star polymer.

    Command line: ./radiusofgyr *.rgin

    Generated file: *.rgout, comments and details about the execution of the program,
                    *.rgseg, data on Rg of each section with a line for each timestep

*rgyr.f* - This file is used to compute the radius of gyration for total star and hydrophobic core. 
         It also computes the anisotropy.

    Command line: ./rgyr *.sprp > *.rgyr

    Generated file: *.rgyr

*starcheck.f* - Program to see if the star polymer is expanded too far within 
the simulation cell. 

    Command line: ./starcheck *.schkin

    Generated file: *.schkout, records information on the length of star 
     polymer for each timestep in xyz files indicated in the input file.

*stargaze.f* - This file runs the stargaze analysis on .xyz files.

    Command line: ./stargaze stargaze_STAR_TEMPKw

    Inputs:  a .sin file (which specifies the location of the .xyz files to be used), a qsub file

    Outputs: a .scor file, a .sori file, a .sprp file, a .sori file

*surface_analysis.f* - Analyze the surface-area-to-volume ratio
for star polymers core+hydrophobic region. Will have to be modified to reflect
the number of classes in each component (core, hydrophobic)

    Command line: ./surface_analysis *.vout

    Generated file: None, will print to stdout

*voronoi.f* - This file performs the voronoi analysis a star given the .xyz outputs of its trajectories

    Command line: qsub *.qsub

    Inputs: classification file for the individual star, a qsub file, .vin file (specifying the .xyz file 
    to be used and their location)

    Outputs: a .class file, a .cluster file, a .vout file, and .neighbors files for each time step

    * compiling *
    gcc -c timers.c
    gfortran -c voronoi.f
    gfortran -o bin/voronoi voronoi.o timers.o 
                  
*water_correlation.f* - Analyze residence times for interior waters, computes
a histogram of the lifetimes of interior waters, shows how the interior water
residence is correlated. If a water is inside at time 0, what is the prob that 
at time t it will still be inside.

    Command line: ./water_correlation *.watout

    Generated file: None, probably writes to stdout.


*water_time.f* - Analyze the correlation function of the interior
water and estimate a residence time 

    Command line: ./water_time water_correlation.out

    Generated file: Prints to stdout


