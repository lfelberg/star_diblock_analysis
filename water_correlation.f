c
c
c Using this program to analyze residence times for interior waters
c 5/24/2016 A.Carr
c
c The program computes the following:
c
c 1) a histogram of lifetimes for the amount of time water molecules spend as interior
c    molecules inside a star polymer.
c 2) a correlation function showing the probability that for a water molecule that is interior
c    at time 0 that it is interior at some time t later.
c
c
c Program reads input file given on the command line
c of type *.waout. For example,  
c
c water_corr_bill3.exe STAR08_350Kw_int.waout
c
c The *.waout file was produced by other programs and bring together
c output from voronoi analyses on each of the interior clusters, an
c from voronoi analyses that give a list of frontier sites.
c
c An option was added 9/12/2016 to allow one to omit certain water molecules
c from consideration.  This was to remove "trapped" water molecules that
c were observed at T=350K for some of the systems.   Molecules are referenced 
c by their molecule id.
c
c water_corr_bill3.exe STAR08_350Kw_int.waout omit 2 2874 20186 
c
c
c
c
c
      implicit real*8(a-h,o-z) 
      character*256 fname,line,xyzname,classfile,xyzcurrent
      character*20 classname(100)
      real*8 hist(100)
      integer iclass(200 000), imask(200 000), isites(500)
      integer istate(200 000,3), ichange(200 000,3) 
      integer pointer(10 000,2), intervals(100 000,2)
      logical ifexist
      dimension corr1(5000),corr2(5000)
      dimension iomit(100)

      natmx=200 000    !size of iclass,imask,istate,ichange arrays
      ninteriormx=500  !max number of interior molecules in a frame
                       !size of isites
      nptrmx=50000     !size of pointer array (max num interior waters)
      nintermx=100000  !size of interval array (max segments)
      
      deltime=40      !time interval between frames in picoseconds
                      !this value is set by the user, but the program checks that
                      !the time between samples read in is consistent with this
                      !for the gelcore simulations, deltime was 20 ps
                      !NOTE, for STAR8 I think this should be 40, not 20
      write(*,'(''Time between samples assumed to be '',
     x  i10,'' psec.  If this is not correct, program stops.'')')
     x  int(deltime)

      ncorr=1000   !size of correlation function arrays

       do i=1,natmx
       istate(i,1)=0
       istate(i,2)=0
       istate(i,3)=0
       ichange(i,1)=0
       ichange(i,2)=0
       ichange(i,3)=0
       enddo


       do i=1,nptrmx
       pointer(i,1)=0
       pointer(i,2)=0
       enddo
     
       do i=1,nintermx
       intervals(i,1)=0
       intervals(i,2)=0
       enddo

       time0=0         !time base for starting time
       time1=0         !time for frame of first analysis 
       time2=0         !time for second frame analysed
       timeprev=-1.d0  


c      dhist=20.d0     !histogram bin width (note this is a time interval in ps)
       dhist=deltime   !histogram bin width (note this is a time interval in ps)
                       !this sets the histogram width equal to the sample period
                       !dhist should be an integer multiple of the sample period
       nhismx=100
       do i=1,nhismx
       hist(i)=0.d0
       enddo

        call getarg(1,line)
c       write (*,'('' line is '',a)') line(1:100)
        is=index(line,' ')-1
        open(10,file=line(1:is))
        is=index(line,'STARG')+5   !I am not sure how this worked for STAR08_350Kw...
        read(line(is:is+1),'(i2.2)') numstar
        is=index(line,'Kw')-3
        read(line(is:is+2),'(i3)') itemp  
        write(*,'(''Input file from command line: '',a)') 
     x    line(1:index(line,' ')-1)
        write (*,'(''numstar is '', i2.2,'' temperature is '', i3)') 
     x  numstar,itemp

        ifomit=0  !set flag to omit nothing
        if(iargc().gt.1) then
          ifomit=1  !set flag to omit some water
          call getarg(3,line)
          read(line,*) nomit
          write(*,'(''Command line indicates to remove '',
     x      i3,'' water molecules from consideration: '')')
     x      nomit
          do i=1,nomit
            call getarg(3+i,line)
            read(line,*) iomit(i)
            write(*,'(''Omit water molecule '',i6)') iomit(i)
          enddo 
        endif

      

20    read(10,120,end=800) line    !top of loop for each new configuration
120   format(a256)
        if(index(line,'Unformatted').eq.0) go to 20
      is=index(line,'Time')
      is2=index(line(is:),'*')-1+(is-1)
c     write(*,'(''Unformatted string '',/,
c    x  ''*'',a,''*'')') line(1:is2)
c     write (*,'(''*'',a,''*'',a,''*'')')
c    x  line(is-15:is-2),line(is+4:is2)
      read(line(is-15:is-2),*)nsolatm,nwatatm
      if(nsolatm+nwatatm.gt.natmx) then
        write(*,'(''Error: not enough storage for site lists '')')
        write(*,'(''Current allocation for '',i10,
     x  '' but need '',i10)') natmx,nsolatm+nwatatm
        stop 'not enough storage for atomic site lists'
      endif
c
c     test this fix; we need the SECOND instance of the word Time
c     it occurs at least 4 characters beyond is (which is the first occurence of Time)
c     is=index(line(is+4:),'Time')+(is+3)
c     is2=index(line(is:),'*')-1+(is-1)
c     write(*,'(''Time string *'',a,''*'')') line(is+4:is2)
c     read(line(is+4:is2),*)time
c
c     a better way to extract the time given some of the berkeley results is
c     to find the terminal asterisk (*) and go backwards to get the last numerical
c     token before it:
      is=index(line,'Time')
      is2=index(line(is:),'*')-1+(is-1)  !points to charcter just before the last asterisk
      is3=0
      do i=is2,is,-1
        if(is3.eq.0.and.line(i:i).eq.' ') is3=i+1
      enddo
      read(line(is3:is2),*) time

c
c     write (*,'(''nsolatm'',i10,''nwatatm'',i10,''time'',f15.4,
c    x  '' time0 '',f15.4,'' time-time0 '',f15.4)')
c    x nsolatm,nwatatm,time,time0,time-time0
 
      if (time1.eq.0.d0) then
        time1=time
        time2=time+deltime
        time0=time-deltime
      endif
      if(timeprev.eq.-1.d0) then
        timeprev=time
      else
        if(time-timeprev.ne.deltime) then
          write(*,'(''Error: there was a change in time interval '',
     x      ''between successive samples.'',
     x      /,''Previous time '',f15.4,'' Current time '',f15.4,
     x      '' Expected time '',f15.4)')
     x      timeprev,time,timeprev+deltime
          stop 'change in time between successive samples'
        endif
      endif


30    read(10,130,end=800) line
130   format(a256)
      if(index(line,'Number of interior solvent sites').eq.0) go to 30
      is=index(line,'sites')+5
      read(line(is:),*) ninterior
      if(mod(ninterior,3).ne.0) then
        write (*,'(''Error, nsites not a multiple of 3'')')
        stop
      endif

      ninterior=ninterior/3   !change to number of interior water molecules
      write(*,'(''Ninteriormx:  '',i10,'' ninterior '',i10)')
     x  ninteriormx,ninterior
      if(ninterior.gt.ninteriormx) then
        write(*,'(''Error: not enough storage for '',
     x    ''interior water list '')')
        write(*,'(''Current allocation for '',i10,
     x  '' but need '',i10)') ninteriormx,ninterior
        stop 'not enough memory for interior water molecules'
      endif

40    read(10,'(a256)',end=800) line
      if(index(line,'Reading coordinates').eq.0) go to 40
      do i=1,ninterior
41      read(10,'(a256)') line
      if(index(line,'Polymer site').ne.0) go to 41  
      is=index(line,'index')+5
      read(line(is:is+6),*)ix
      imolx=(ix-nsolatm-1)/3+1
      isites(i)=imolx    !this loop saves a list of molecule indices that are interior
c      istate(imolx,1)=1
      is=index(line,'H')+1
      read(line(is:is+11),*) height
      is=index(line,'D')+1
      read(line(is:is+11),*) depth 
      is=index(line,'V')+1
      read(line(is:is+11),*) volume 
        write (*,'(''time'',f12.2,i3,i6,''height'',f15.4,''depth'',
     x    f15.4,''volume'',f15.4)')
     x    time-time0,i,imolx,height,depth,volume 
      enddo

        do i=1,nwatatm/3
        istate(i,2)=0   !clear the "current" status of each water
        enddo
        do i=1,ninterior
        j=isites(i)
        istate(j,2)=1   !set the current status as "interior" for the interior waters
        enddo
 
       do i=1,nwatatm/3
        if(istate(i,1).eq.0.and.istate(i,2).eq.0) then
c         Water out, stays out, do nothing
        elseif(istate(i,1).eq.1.and.istate(i,2).eq.1) then
c         Water was internal, stays internal, do nothing
        elseif(istate(i,1).eq.0.and.istate(i,2).eq.1) then
c         Water out, becomes interior, save the time it is first seen to be interior
          istate(i,3)=time-time0   
          ichange(i,1)=ichange(i,1)+1
        elseif(istate(i,1).eq.1.and.istate(i,2).eq.0) then
c         Water in, comes out, write out time data (atom index, time entered, time left, duration) 
          write(*,'('' i '', i6, '' time in '', i6,
     x      '' time out '', f15.4, '' duration '', f15.4)')
     x      i, istate(i,3), time-time0, time-time0-istate(i,3)
          life=time-time0-istate(i,3)   !duration it was interior
          ichange(i,2)=ichange(i,2)+life !increase the total residence time for this molecule
          ibin=life/dhist
          if(ibin.le.0) stop 'error in lifetime computation'
          ibin=min(ibin,100)
          hist(ibin)=hist(ibin)+1.d0
         endif
       enddo

       do i=1,nwatatm/3
       istate(i,1)=istate(i,2)  !copy current status to previous status for next pass
       enddo

      timeprev=time
      go to 20   !go to top of loop and read inforamtion about the next configuration






800   continue      !branch to here when we hit end of the *.waout file

c      process the last time block
       do i=1,nwatatm/3
        if(istate(i,1).eq.0) then
c         Water out, stays out, do nothing
        elseif(istate(i,1).eq.1) then
c         Water in, comes out, write out time data (atom index, time entered, time left, duration)
c           write(*,'('' i '', i6, '' time in '', i6,
c     x     '' time out '', f15.4, '' duration '', f15.4)')
c     x     i, istate(i,3), time-time0+(time2-time1),
c     x     time-time0+(time2-time1)-istate(i,3)
           life=time-time0+(time2-time1)-istate(i,3)
           ichange(i,2)=ichange(i,2)+life
           ibin=life/dhist
           if(ibin.le.0) stop 'error in lifetime computation'
           ibin=min(ibin,100)
           hist(ibin)=hist(ibin)+1.d0
         endif
       enddo


c     This is now the end of first pass




c     We have a histogram of lifetimes in hist
c     write out the histogram information
      n1=0
      do i=1,nhismx
      n1=n1+hist(i)
      enddo
      write(*,'(/,''Total entries in hist '',i5)') n1
      write(*,'(''(This should be num of segments)'')') 
      write(*,'(''Lifetime distribution '')') 
      do i=1,nhismx
      write(*,'(''time '', f7.2,'' hist '', f10.4, '' count '', f8.2)')
     x 0.5*dhist+(i-1)*dhist, hist(i)/n1, hist(i)
      enddo



      
c     write out some accounting information
      n=0
      ntime=0
      nchange=0
      do i=1,nwatatm/3
        if (istate(i,3).gt.0) then 
          n=n+1    !counter for water molecules that have ever been interior ones
          if(n.gt.nptrmx) then
            write(*,'(''Error:  pointer array not '',
     x        ''large enough'')')
            stop 'pointer array not large enough'
          endif
          ntime=ntime+ichange(i,2)   !total amount of time for all interior molecules
          nchange=nchange+ichange(i,1) !number of segments of time for interior molecules
          ichange(i,3)=n   !set up pointers for next pass through the data
          pointer(n,1)=i   !set up pointers for next pass through the data
c         write(*,'(''water mol index '',i7,
c    x    '' entries '',i4,
c    x    '' agg time '',i5)')
c    x    i,ichange(i,1),ichange(i,2)
        endif 
      enddo

      if(nchange.gt.nintermx) then
        write(*,'(''Error:  interval array not '',
     x    ''large enough'')')
        stop 'interval array not large enough'
      endif


      write(*,'(/,
     x          ''Number internal waters over all times '',i6,/,
     x          ''Total aggregate time for all waters   '',i6,/,
     x          ''Total segments of time                '',i6)') 
     x  n,ntime,nchange
      write(*,'(/,''avg lifetime '', f9.5)') dble(ntime)/nchange
      write(*,'(  ''nsamples, (number of frames ) '',f12.4)') 
     x  (time-time0)/deltime 
      write(*,'(  ''avg wat load '', f9.5)') 
     x  dble(ntime)/(time-time0)




c     At this point information has been processed from the first pass




c     develop pointers for the second pass through the data

c     after this loop, pointer(i,2) has the number of time segments
c     that molecule pointer(i,1) is interior to the polymer
      do i=1,n
      j=pointer(i,1)
      pointer(i,2)=ichange(j,1)
c     write(*,'(''pointer(i,1)'', i6, '' pointer(i,2)'',
c    x i6)') pointer(i,1), pointer(i,2)    
      enddo



c     after this loop, pointer(i,2) indicates the last entry for 
c     molecule pointer(i,1)
      do i=2,n
      pointer(i,2)=pointer(i-1,2)+pointer(i,2)     
c     write(*,'(''i'', i6, ''pointer(i,1)'', i6, '' pointer(i,2)'',
c    x i6)') i, pointer(i,1), pointer(i,2)    
      enddo

 
c     after this loop, pointer(i,2) points to one less than the first
c     site where the intervals storage will start for molecule pointer(i,1)
      nsave=pointer(n,2) 
      do i=n,2,-1
      pointer(i,2)=pointer(i-1,2)      
c      write(*,'(''i'', i6, ''pointer(i,1)'', i6, '' pointer(i,2)'',
c     x i6)') i, pointer(i,1), pointer(i,2)    
      enddo
      pointer(1,2)=0
      
c     do i=1,n
c     write(*,'(''i'', i6, ''pointer(i,1)'', i6, '' pointer(i,2)'',
c    x i6)') i, pointer(i,1), pointer(i,2)    
c     enddo






c
c--------------------------------------------------------------------------
c     now rewind the file and start all over again
c
      rewind(10)



      do i=1,natmx
        istate(i,1)=0
        istate(i,2)=0
        istate(i,3)=0
        ichange(i,1)=0
        ichange(i,2)=0
c       ichange(i,3)=0    !note we DON'T initialize this column
      enddo

      do i=1,nhismx
        hist(i)=0.d0
      enddo

220   read(10,'(a256)',end=1800) line
      if(index(line,'Unformatted').eq.0) go to 220
      is=index(line,'Time')
      is2=index(line(is:),'*')-1+(is-1)
c     write (*,'(''*'',a,''*'',a,''*'')')
c    x  line(is-15:is-2),line(is+4:is2)
      read(line(is-15:is-2),*)nsolatm,nwatatm


c     test this fix; we need the SECOND instance of the word Time
c     it occurs at least 4 characters beyond is (which is the first occurence of Time)
c     read(line(is+4:is2),*)time
      is=index(line(is+4:),'Time')+(is+3)
      is2=index(line(is:),'*')-1+(is-1)
      read(line(is+4:is2),*)time

c      write (*,'(''nsolatm'',i10,''nwatatm'',i10,''time'',f15.4)')
c     x nsolatm,nwatatm,time
 

230   read(10,'(a256)',end=1800) line
      if(index(line,'Number of interior solvent sites').eq.0) go to 230
      is=index(line,'sites')+5
      read(line(is:),*)ninterior
      if(mod(ninterior,3).ne.0) then
        write (*,'(''Error, nsites not a multiple of 3'')')
        stop
      endif


      ninterior=ninterior/3    !convert to number of interior water molecules

240   read(10,'(a256)',end=1800) line
      if(index(line,'Reading coordinates').eq.0) go to 240
      do i=1,ninterior
241     read(10,'(a256)') line
        if(index(line,'Polymer site').ne.0) go to 241  
        is=index(line,'index')+5
        read(line(is:is+6),*)ix    !read site index
        imolx=(ix-nsolatm-1)/3+1   !compute molecule index
        isites(i)=imolx            !save molecule index in isite array
c        istate(imolx,1)=1
        is=index(line,'H')+1
        read(line(is:is+11),*) height
        is=index(line,'D')+1
        read(line(is:is+11),*) depth 
        is=index(line,'V')+1
        read(line(is:is+11),*) volume 
c        write (*,'(''time'',f12.2,i3,i6,''height'',f15.4,''depth'',
c     x   f15.4,''volume'',f15.4)')
c     x   time-time0,i,imolx,height,depth,volume 
      enddo

      do i=1,nwatatm/3
        istate(i,2)=0
      enddo

      do i=1,ninterior
        j=isites(i)
        istate(j,2)=1 
      enddo
 
      do i=1,nwatatm/3
        if(istate(i,1).eq.0.and.istate(i,2).eq.0) then
c Water out, stays out, do nothing
        elseif(istate(i,1).eq.1.and.istate(i,2).eq.1) then
c Water was internal, stays internal, do nothing
        elseif(istate(i,1).eq.0.and.istate(i,2).eq.1) then
c Water out, becomes interior, save the time
          istate(i,3)=time-time0   
          ichange(i,1)=ichange(i,1)+1
        elseif(istate(i,1).eq.1.and.istate(i,2).eq.0) then
c Water in, comes out, write out time data (atom index, time entered, time left, duration) 
c           write(*,'('' i '', i6, '' time in '', i6,
c     x     '' time out '', f15.4, '' duration '', f15.4)')
c     x     i, istate(i,3), time-time0, time-time0-istate(i,3)
          life=time-time0-istate(i,3)
          ichange(i,2)=ichange(i,2)+life
          ibin=life/dhist
          ibin=max(1,ibin)
          ibin=min(ibin,100)
          hist(ibin)=hist(ibin)+1.d0

c         following is just a consistency check; if all is working ok
          m=ichange(i,3)
          if(pointer(m,1).ne.i) then
            write(*,'(''Error:  problem with pointer array.'')')
            stop
          endif
          iptr=pointer(m,2)+1
          pointer(m,2)=pointer(m,2)+1
          intervals(iptr,1)=istate(i,3)  !entry time
          intervals(iptr,2)=time-time0   !exit time
        endif
      enddo

      do i=1,nwatatm/3
        istate(i,1)=istate(i,2)
      enddo

      go to 220



1800  continue    !arrive here when you hit the end of the *.waout file
      close(10)


      do i=1,nwatatm/3
        if(istate(i,1).eq.0) then
c Water out, stays out, do nothing
        elseif(istate(i,1).eq.1) then
c Water in, comes out, write out time data (atom index, time entered, time left, duration)
c           write(*,'('' i '', i6, '' time in '', i6,
c     x     '' time out '', f15.4, '' duration '', f15.4)')
c     x     i, istate(i,3), time-time0+(time2-time1),
c     x     time-time0+(time2-time1)-istate(i,3)
          life=time-time0+(time2-time1)-istate(i,3)
          ichange(i,2)=ichange(i,2)+life
          ibin=life/dhist
          ibin=max(1,ibin)
          ibin=min(ibin,100)
          hist(ibin)=hist(ibin)+1.d0

          m=ichange(i,3)
          if(pointer(m,1).ne.i) then
            write(*,'(''Error'')')
            stop
          endif
          iptr=pointer(m,2)+1
          pointer(m,2)=pointer(m,2)+1
          intervals(iptr,1)=istate(i,3)
          intervals(iptr,2)=time-time0+(time2-time1)
        endif
      enddo


c
c     reset the pointers
c
c     after this loop, pointer(i,2) points to one less than the first
c     site where the intervals storage will start for molecule pointer(i,1)
      nsave=pointer(n,2) 
      do i=n,2,-1
        pointer(i,2)=pointer(i-1,2)      
c      write(*,'(''i'', i6, ''pointer(i,1)'', i6, '' pointer(i,2)'',
c     x i6)') i, pointer(i,1), pointer(i,2)    
      enddo
      pointer(1,2)=0
      

c     write(*,'(''After final pointer reset'')')

c     do i=1,n
c     write(*,'(''i'', i6, ''pointer(i,1)'', i6, '' pointer(i,2)'',
c    x i6)') i, pointer(i,1), pointer(i,2)    
c     enddo

      



c     write out some accounting information
      n=0
      ntime=0
      nchange=0
      do i=1,nwatatm/3
        if (istate(i,3).gt.0) then 
         n=n+1
         ntime=ntime+ichange(i,2)   !aggregate time
         nchange=nchange+ichange(i,1) !number of segments
c        ichange(i,3)=n
         if(ichange(i,3).ne.n) then
           write(*,'(''Error:  problem in column three '',
     x       ''of ichange'')')
           stop 'Error; problem in column three of ichange'
         endif
c        pointer(n,1)=i
         if(pointer(n,1).ne.i) then
           write(*,'(''Error:  problem in column two '',
     x       ''of pointer'')')
           stop 'Error; problem in column three of ichange'
         endif
     
         write(*,'(''molindex '', i7 , '' entries'', i4,
     x   '' agg time '', i5)')
     x   i,ichange(i,1),ichange(i,2)
        endif 
      enddo

      write(*,'(''n distinct internal waters'',i6, 
     x ''total agg time'', i6,''total entries'', i6)') n,ntime,nchange
      write(*,'(''avg lifetime'', f8.4)') dble(ntime)/nchange
      write(*,'(''nsamples'',f12.4)') (time-time0)/deltime 

c
c     do another check:  go through the pointer and interval arrays
c     sum up the number of segments, total time
c
      write(*,'(/,''Another consistency check'')')

      ntottim=0
      ntotseg=0
      do i=1,n
        im=pointer(i,1)
        ip=pointer(i,2)
        nseg=ichange(im,1)
        ntotseg=ntotseg+nseg
        do j=1,nseg
          ntottim=ntottim+
     x      (intervals(ip+j,2)-intervals(ip+j,1))
        enddo
        write(*,'(''Interior index '',i4,
     x    '' ; Orig mol index '',i6,
     x    '' ; Segment '',i2,
     x    '' ; Start/End '',2i8)')
     x    (i,im,nseg,
     x    intervals(ip+j,1),intervals(ip+j,2),
     x    j=1,nseg)         
      enddo
      write(*,'(/,''Consistency check: '',/
     x  '' Total segments '',i5,'' Total time '',i8)')
     x  ntotseg,ntottim



c     write out the histogram information again; should be identical
c     to before

c     n1=0
c     do i=1,nhismx
c     n1=n1+hist(i)
c     enddo
c     write(*,'(''n1 is '', i6)') n1

c     do i=1,nhismx
c     write(*,'(''dist '', f7.2,'' hist '', f10.4, '' count '', f8.2)')
c    x 0.5*dhist+(i-1)*dhist, hist(i)/n1, hist(i)
c     enddo



      do i=1,n
        molix=pointer(i,1)
c        write(*,'(''Internal molecule '',i5,'' molecule index '',i6)')
c     x    i,molix
        do j=1,ichange(molix,1)
          iptr=pointer(i,2)+j
c          write(*,'(''Interval '',i2,'' Start/end '',2i8,
c     x     '' Duration '',i6)') 
c     x     j,intervals(iptr,1),intervals(iptr,2),
c     x     intervals(iptr,2)-intervals(iptr,1)
        enddo
      enddo






c-----------------------------------------------------------------------
c     with the pointer and interval data structures we can compute
c     correlation functions
c
c     Consider an indicator function, x(i,j)
c     x(i,j)=1 if molecule i is interior to the polymer at time t(j)=j*dt
c           =0 if not
c     Index i goes over all water molecules = 1 to M=nwatatm/3
c     Index j goes over all times at which we have samples j = 1 to (time-time0)/deltime
c     (deltime = 20 picoseconds) corresponding to times j*deltime
c
c
c     The correlation function, C(k)=<x(i,j)*x(i,j+k)>/<x(i,j)**2>
c     is the conditional probability that a molecule that is interior at time t will
c     still be interior at time k*deltime later
c
c     note that we have time intervals (endpoints) during which each molecule was 
c     was interior to the polymer; the time index corresponding to these times
c     is given by j=time/deltime
c
      write(*,'(//,''Computing correlation functions'')')

      inquire(file='correlationfunctions.dat',exist=ifexist) 
      if(ifexist)then
        open(15,file='correlationfunctions.dat',status='OLD') 
300     read(15,*,end=310)
        go to 300
310     continue
      else
        open(15,file='correlationfunctions.dat',status='NEW')
      endif

      do i=1,ncorr
        corr1(i)=0.d0   !numerator (sum over double indicator function)
        corr2(i)=0.d0   !denominator (sum over single indicator function)
      enddo

c     the last time at which we have a sample is time-time0; this corresponds to
c     the following number of samples
      write(*,'(''First/last time at which we have data is '',2i10,
     x  '' Delta time '',i5)')
     x  int(time0+deltime),int(time),int(deltime)
      nsample=(time-time0)/deltime
      write(*,'(''Elapsed time '',i8,
     x  '' Number of samples is '',i6)') int(nsample*deltime),nsample

      do imol=1,n   !loop over molecules that were ever at any time interior ot the polymer
        molix=pointer(imol,1)
c       check if this molecule is to be omitted from analysis
        ifremove=0
        if(ifomit.eq.1) then
          do j=1,nomit
            if(iomit(j).eq.molix) ifremove=1
          enddo 
          if(ifremove.eq.1) then 
            write(*,'(''Found and removing '',
     x        ''molecule from CF'',i6)') molix
            write(*,'(''Molecule time segments '',i3,
     x        '' molecule agg time '',i6)') 
     x        ichange(molix,1),ichange(molix,2)
          endif
        endif
        if(ifremove.eq.0) then
c         write(*,'(''Internal molecule '',i5,'' molecule index '',i6,
c    x      '' segments '',i3)')
c    x      imol,molix,ichange(molix,1)
          do j=1,ichange(molix,1)   !loop over the number of segments of this molecule 
            iptr=pointer(imol,2)+j
            write(*,'(''Segment '',i2,'' Start/end '',
     x        2i8,'' Duration '',i6)') 
     x        j,intervals(iptr,1),intervals(iptr,2),
     x        intervals(iptr,2)-intervals(iptr,1)
c           loop over the range of times seen in this interval
            it1=intervals(iptr,1)/deltime
            it2=intervals(iptr,2)/deltime
            write(*,'(''Start time '',i10,i6,'' end time '',i10,i6)')
     x      intervals(iptr,1),it1,intervals(iptr,2),it2
c
c           note that at time it2 the molecule was OUT not in; so 
c           the last time it was in is one time step back from this one
c
c           increment appropriate parts of the corr2 array
            do k=it1,it2-1
c             the indicator function at time k contributes to corr2 at
c             all lag times from lag=0 to lag=Nsample-k
c              write(*,'(''Unit indicator function at time index '',i4,
c     x          '' contributes to sum with lag from zero to '',i5)')
c     x          k,nsample-k
              do lag=0,min(ncorr-1,nsample-k)
                corr2(lag+1)=corr2(lag+1)+1.d0
              enddo

c             consider the interactions between time k and the rest in this same block
              do l=k,it2-1
                lag=l-k
                if(lag.le.ncorr-1) then
                  corr1(lag+1)=corr1(lag+1)+1.d0
c                 write(*,'(''Unit indicator functions '',
c    x              ''at time indices '',2i4,
c    x              '' contribute to intrablocksum with lags '',2i5)')
c    x              k,l,l-k,lag
                endif
              enddo

c             consider the interaction between time k and those in other blocks
              do j2=j+1,ichange(molix,1)
                iptr2=pointer(imol,2)+j2
                it12=intervals(iptr2,1)/deltime
                it22=intervals(iptr2,2)/deltime
                do l=it12,it22-1
                  lag=l-k
                  if(lag.le.ncorr-1) then
                    corr1(lag+1)=corr1(lag+1)+1.d0
c                   write(*,'(''Unit indicator functions '',
c     x               ''at time indices '',2i4,
c     x               '' contribute to interblocksum with lags '',2i5)')
c     x               k,l,l-k,lag
                  endif
                enddo
              enddo

            enddo  !end of do loop over the times in this segment
          enddo  !end of do loop over the time segments this water has been interior
        endif   !end of ifblock for if this molecule is not in the "omit" list 
      enddo   !end of do loop over water molecules that have been interior

      write(*,'(//,''Number of times '',i5)') nsample
      write(*,'(''Number of water molecules '',i5)') nwatatm/3
      write(*,'(''Corr1 (numerator) at k=0:  '',i10)') int(corr1(1))
      write(*,'(''Corr2 (denominat) at k=0:  '',i10)') int(corr2(1))
      write(*,'(''Average loading '',f10.4)') corr2(1)/nsample



      write(*,'(/,''First normalization '',
     x  ''(just counts, no shift)'')')

      do k=0,min(200,nsample-1)
        write(*,'(''Corr2('',f6.3,'')= '',i10, '' Ave loading '',f10.4,
     x    '' Corr1='',i10,
     x    '' C1/C2='',f10.5)') 

     x    k*(time2-time1)/1000.d0,
     x    int(corr2(k+1)),corr2(k+1)/(nsample-k),
     x    int(corr1(k+1)),
     x    corr1(k+1)/corr2(k+1)
      enddo

c     write(15,'(i10,2i5,251f10.4)')250,numstar,itemp,
c    x (corr1(k+1)/corr2(k+1),k=0,250)
      write(15,'(i10,2i5)') 250,numstar,itemp
      write(15,'(f6.3,f10.4)')
     x (k*(time2-time1)/1000.d0,corr1(k+1)/corr2(k+1),k=0,250)

c     NOTE: the alternative normalization did not have any noticable affect
c     write(*,'(/,''Alternative normalization (full, shift)'')')

c     do k=0,min(200,nsample-1)
c       xumer=corr1(k+1)-(corr2(k+1)**2)/((nsample-k)*(nwatatm/3))
c       denom=corr2(k+1)-(corr2(k+1)**2)/((nsample-k)*(nwatatm/3))
c       write(*,'(''Corr2('',f6.3,'')= '',f10.4,'' Ave loading '',f10.4,
c    x    '' Corr1='',f10.4,
c    x    '' C1/C2='',f10.4)') 

c    x    k*(time2-time1)/1000.d0,denom,corr2(k+1)/(nsample-k),
c    x    xumer,xumer/denom
c     enddo

c      write(*,'(/,''Second alternative normalization'')')

c      do k=0,min(200,nsample-1)
c        write(*,'(''Denom <x>('',i3,'')= '',f10.6,
c     x            '' Numer <x_j*x_(j+k)>='',f10.6,
c     x    '' C1/C2='',f10.6)') 

c     x    k,nsample*corr2(k+1)/(nsample-k),
c     x      nsample*corr1(k+1)/(nsample-k),
c     x      corr1(k+1)/corr2(k+1)
c      enddo

       close(15)

      end



