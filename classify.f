c
c     this program classifies sites in a star polymer system
c     given the *.lmp file
c 
c     the goal is to generate an input file for the voronoi program
c     it is specified in the voronoi input file with the directive
c     "classificationfile scheme1.classify"
c
c     the idea is to provide input to classify subsets of sites so that
c     statistics can be kept on the volumes and interfacial areas 
c     for any type of classification scheme
c
c     Examples could be:
c
c     all polymer sites VS water sites
c     all hydrophobic sites VS hydrophilic sites VS water
c     all alkane VS ester VS alcohol VS water sites
c     each chain could be in a different class
c     each peg region could be in a separate class
c     the adamantane and the first n repeat units connected to along
c     each chain
c
c     in any of these cases, the voronoi program should compute the
c     volume and interfacial surface area between any class of sites
c     
c     this program reads an input file classify.in
c       first line of this file is the *.lmp file to read for topology information
c       (there must also be a *.pdb in the directory with the same name as the lmp)
c
c     voronoi expects to read *.classify
c     line 1:  nschemes    number of classification schemes
c     Then for each scheme,
c       number of classes in this scheme
c         class name and identifying number for each class
c     Then for each of the atoms, the class identifying number
c
c
c     
c     read in an LMP file that describes the molecule
c
c
c     this program was derived from the stargaze program
c     Aug 2011
c
c
c
      implicit real*8 (a-h,o-z)

      character*120 mname,line,string,xyzname

      dimension coord(3,7000000),charge(7000000),itype(7000000)
      dimension molid(7000000)
      integer resid(7000000)
      character*20 atname(7000000)
      dimension ijbond(3,700000),bndpar(2,5000)
      dimension ijkangle(4,7000000),angpar(2,5000)
      dimension ijkldih(5,92000),dihpar(4,5000)
      dimension vdw(4,100),fmass(100)
      character*100 nametypes(100)
    
      dimension box(3,2)
      dimension amass(120)
      dimension ibuf(4,2)

      character*10 resname(7000000)   !from the corresponding pdb file
      integer residue(5,700000)   !dimension to number of residues (approx Nwater)
      integer restype(5,100)      !information about each type of residue 
      dimension molecule(5,700000)!information about each molecule
      dimension moltype(5,100)    !information about each type of molecule
      integer strand(5,100)       !information about each strand
      integer strandseq(500)      !informaiton about the sequence of res types along strand
                                  !...this is for getting Rg from core to termini

      dimension ijres(5,700000)  !array to hold residue connectivity; constructed from bond list

      dimension buffer(4,100) !hold up to 100 vector properties of the star for output

      dimension iresclass(50)  !class id for each residue type
      character*20 classname(50)  !name of each class
      dimension idclass(50)  !id number for each class
      dimension iresnumclass(700000) !class number for each residue
      



      natmx=7000000 !size of site-based arrays
      nattpmx=100  !size of atom type arrays
      nbnmx=7000000 !size of bond list arrays
      nbtmx=5000   !size of bond type arrays
      nanmx=7000000 !size of angle list arrays
      nantmx=5000  !size of angle type arrays
      ndimx=920000   !size of dihedral list array
      nditmx=5000  !size of the dihedral type array
      nresmx=700000 !size of residue array


    
      open(11,file='classify.in')

      read(11,'(a)') mname   !first line is name of lmp file to use
      call readlmp(mname,
     x       nsites,itype,molid,charge,coord,
     x       atname,natmx,
     x       nbonds,nbtp,bndpar,ijbond,
     x       nbnmx,nbtmx,
     x       nangle,natp,angpar,ijkangle,
     x       nanmx,nantmx,
     x       ndih,ndtp,dihpar,ijkldih,
     x       ndimx,nditmx,
     x       natomtp,fmass,vdw,nametypes,
     x       nattpmx,
     x       box)

c

c     left justify the site names
      do j=1,nsites
        call leftjust(atname(j))
      enddo


      write(*,*)
      write(*,'(''   Number of atom types in this system    '',i8)')
     x  natomtp
c     write(*,'(''   Number of bond types in this system    '',i8)')
c    x  nbtp
c     write(*,'(''   Number of angle types in this system   '',i8)')
c    x  natp
c     write(*,'(''   Number of dihedral types this system   '',i8)')
c    x  ndtp


      write(*,*)
      write(*,'(''   Number of atoms in this system         '',i8)')
     x  nsites
c     write(*,'(''   Number of bonds in this system         '',i8)')
c    x  nbonds
c     write(*,'(''   Number of angles in this system        '',i8)')
c    x  nangle
c     write(*,'(''   Number of dihedrals in this system     '',i8)')
c    x  ndih



c     open the corresponding pdb file and get the residue names
      is=index(mname,'.lmp')
      open(15,file=mname(1:is-1)//'.pdb')
      do i=1,nsites
        read(15,'(a120)') line
        read(line,'(17x,a5)') resname(i)
        call leftjust(resname(i))
        resname(i)(4:)=' '
      enddo
      close(15)

c
c     need to know for each atom:
c     what molecule it is in (molid)
c     what residue it is in (resid, populated using last field of atname)
c
c     loop over the atomic sites 
c       develop a list of residues
c         residue(1,i) = first atomic site for residue i
c         residue(2,i) = last atomic site for residue i
c         residue(3,i) = type for residue i (points into restype)
c         residue(4,i) = strand for residue i
c         residue(5,i) = last (non-hydrogen) site for residue i (used for orientation dynamics)
c       populate resid - what residue each atom is in
c         resid(i) = residue index for atom i
c       develop list molecules
c         for each molecule, what is the first and last atomic site index
c         molecule(1,i) = first atomic site of molecule i
c         molecule(2,i) = last atomic site of molecule i

      nmol=0
      im=-1     !current molecule index
      ir=-1     !current residue index for molecule
      ifirst=0  !first and
      ilast=0   !last sites for this residue
      nres=0
      do i=1,nsites
        call parser(atname(i),string,lelfield,i1,i2,i3)
        if(im.ne.molid(i)) then
          if(i.ne.1) then
            residue(2,nres)=i-1
            molecule(2,nmol)=i-1
          endif
          nres=nres+1
          residue(1,nres)=i
          im=molid(i)
          if(i3.ne.-1) ir=i3
          nmol=nmol+1
          molecule(1,nmol)=i
        else
          if(i3.eq.-1) then
            if(resname(i)(1:3).ne.'WAT') then
              write(*,'('' Unknown residue field for site '',i7)')
     x          i
              
              write(*,'('' atname(i) = *'',a,
     x          ''* string = *'',a,''*'')')
     x          atname(i)(1:20),string(1:20)
              stop 'unknown residue fields'
            endif
          endif
          if(i3.ne.-1.and.i3.ne.ir) then
            residue(2,nres)=i-1
            nres=nres+1
            residue(1,nres)=i
            ir=i3
          endif
        endif
        resid(i)=nres
      enddo
      residue(2,nres)=nsites
      molecule(2,nmol)=nsites
  
      write(*,'('' Number of residues found = '',i6)') nres
c
c     for each residue, find last non-hydrogen site for orientational dynamics
c
      do i=1,nres
        ifsite=residue(1,i)
        ilsite=residue(2,i)
        isave=0 
        do j=ilsite,ifsite+1,-1   !start at end, find last nonhydrogen site
          if(isave.eq.0.and.atname(j)(1:1).ne.'H') isave=j
c         if(ilsite-ifsite+1.eq.3) then
c           write(*,'('' 3-site residue '',i5,'' type= '',
c    x      a,'' first/last sites= '',2i6,'' atname(j)='',a,
c    x      '' isave= '',i6)')
c    x      i,resname(ifsite),ifsite,ilsite,atname(j)(1:3),isave
c         endif
        enddo
        residue(5,i)=isave  !if isave=0, it means there are no nonhydrogen sites except first site
        if(isave.ne.0) then
          is=index(atname(ifsite),' ')-1
          is1=index(atname(ilsite),' ')-1
          write(*,'('' Residue vector for residue '',i6,'' of type '',a,
     x      '' between sites '',a,'' and '',a,'' isave='',i6)') 
     x      j,resname(ifsite),atname(ifsite)(1:is),
     x                        atname(ilsite)(1:is1),isave
        endif
      enddo


c
c     loop over the residue list; develop a list of unique residue types 
c     restype(5,100)      !information about each type of residue 
c     restype(1,i) !index of first (representative) atom in this type of residue
c     restype(2,i) !count this type of residue

      nrtype=0  !number of residue types found
      
      do i=1,nres
        ifirst=residue(1,i)
        ilast=residue(2,i)
c       write(*,'('' Residue '',i5,'' First site '',i7,2x,a,
c    x                             '' Last site  '',i7,2x,a,
c    x           '' Name '',a)')
c    x  i,ifirst,atname(ifirst)(1:12),ilast,atname(ilast)(1:12),
c    x    resname(ifirst)(1:5)

        ifound=0
        do j=1,nrtype
          if(ifound.eq.0.and.
     x      resname(restype(1,j))(1:4).eq.resname(ifirst)(1:4)) ifound=j
        enddo
        if(ifound.eq.0) then
          nrtype=nrtype+1
          restype(1,nrtype)=ifirst
          restype(2,nrtype)=0
          ifound=nrtype
        endif
        restype(2,ifound)=restype(2,ifound)+1
        residue(3,i)=ifound

      enddo

      write(*,'(/,'' Number of residue types found is '',i5)') nrtype
      nrtot=0
      do i=1,nrtype
        write(*,'('' Residue type '',i3,'' Resname '',a,
     x    '' count= '',i7)')  i,resname(restype(1,i))(1:4),restype(2,i)
        nrtot=nrtot+restype(2,i)
      enddo
      write(*,'('' Total number of residues found is '',i6)') nrtot 




c     write(*,'('' Classification based on residue type (1)'',
c    x  '' or index (2)?:'')')
c     read(*,*) ichoice
      read(11,*) ichoice
      if(ichoice.eq.1) then
        write(*,'('' You selected classification based on'',
     x    '' residue types.'')') 
      elseif(ichoice.eq.2) then
        write(*,'('' You selected classification based on'',
     x    '' residue indicies.'')') 
      else
        stop 'unrecognized value for ichoice'
      endif

      if(ichoice.eq.1) then

      open(12,file='classification.classify')
      write(*,'(/,''Classification scheme based on residue type '')')
      write(*,'(''Assign each residue type a classification index '')')
      write(*,'(''(Note that several residue types can have the'',
     x    '' same index.)'')')

      write(*,'(''Reading residue types and classes from '',
     x  ''classify.in'')')
      read(11,*) nrtype 
      write(*,'(''Number of residue types is '',i5)') nrtype


c     this is awkward - but most file input will be by residue index
c     (with ichoice=2)

      do i=1,nrtype
c       write(*,'(''Residue type '',i3,''  name '',a,
c    x    ''.  Enter a class index:'')') 
c    x    i,resname(restype(1,i))(1:4)
c       read(*,*) id
        read(11,*) id
        iresclass(i)=id
        write(*,'(''Residue type '',i3,''  name '',a,
     x    ''.  will have class index:'')') 
     x    i,resname(restype(1,i))(1:4),id
      enddo

      write(*,'(/,''What you entered:'')')
      irescls=0
      do i=1,nrtype
        write(*,'(''Residue type '',i3,''  name '',a,2x,
     x    '' has class index '',i2)') i,resname(restype(1,i))(1:4),
     x    iresclass(i)
        irescls=max(iresclass(i),irescls)
      enddo

      write(*,'(/,''For each distinct class index, provide a name'')')
      nclass=0  !number of classes associated with this scheme
      do i=1,irescls
        n=0
        do j=1,nrtype
          if(iresclass(j).eq.i) n=n+1
        enddo
        if(n.gt.0) then
          nclass=nclass+1
          idclass(nclass)=i
          write(*,'(''Enter name for class with index '',i2,
     x       '':'')') i
          read(*,'(a20)') classname(nclass)
        endif
      enddo

      write(*,'(/,''There are '',i2,'' distinct classes in this'',
     x    '' scheme.'')') nclass
      do i=1,nclass
        is=index(classname(i),' ')-1
        write(*,'(''Class '',i2,'':  class index '',i3,
     x    '' name '',a)') i,idclass(i),classname(i)(1:is)
        do j=1,nrtype
          if(iresclass(j).eq.idclass(i)) 
     x      write(*,'(''  This class associated with residues '',
     x      ''named '',a)') resname(restype(1,j))(1:4)
        enddo
      enddo

  
      write(12,'(i5,'' number of classification schemes '')') 1
      write(12,'(i5,'' number of classes in this scheme'')') nclass
      do i=1,nclass
        is=index(classname(i),' ')-1
        write(12,'(a,5x,i5)') classname(i)(1:is),idclass(i)
      enddo
      write(12,'(i10)') nsites
      do i=1,nsites
        j=resid(i)   !this atom is in residue number j
        k=residue(3,j) !this is the residue type of residue j
c       l=iresclass(k) !this is the class associated with residue type k
        do m=1,nclass
          if(iresclass(k).eq.idclass(m)) l=m
        enddo
        is=index(classname(l),' ')-1
        is1=index(atname(i),' ')-1
c       write(12,'('' Atom '',i7,'' name '',a,'' residue number '',i5,
c    x    '' class '',i3,'' classname '',a)') 
c    x    i,atname(i)(1:is1),j,idclass(l),classname(l)(1:is)
        write(12,'(i5,2x,a)') idclass(l),classname(l)(1:is)
      enddo

      close(12)

      elseif(ichoice.eq.2) then

        open(12,file='classification.classify')
        write(*,'(/,''Classification scheme based on residue index'')')
        write(*,'(''Assign each residue index range a '',
     x    ''classification index '')')
        write(*,'(''(Note that several residue index ranges could'',
     x      '' have the same classification index.)'')')

        write(*,'(''Reading residue ranges and classes from '',
     x    ''classify.in'')')
        read(11,*) nrange
        write(*,'(''Number of ranges is '',i5)') nrange

        do i=1,nres
          iresnumclass(i)=-1
        enddo

        nclass=0
        do i=1,nrange
          read(11,'(a120)') line 
          write(*,'(''Line from classify.in:  '',a80)')
     x      line(1:80)
          is1=index(line,' ')-1
          iffound=0
          do j=1,nclass
            is=index(classname(j),' ')-1
            if(iffound.eq.0.and.
     x         line(1:is1).eq.classname(j)(1:is)) 
     x         iffound=j
          enddo
          if(iffound.eq.0) then
            nclass=nclass+1
            classname(nclass)=line(1:is1)
            iffound=nclass
          endif
c         write(*,'(''current content in line: '',a80)')
c    x      line(1:80)
c         write(*,'('' len(line) '',i4)') len(line)
c         write(*,'('' is1 (last character position '',i3)')
c    x      is1
c         write(*,'('' line with numbers *'',a80,''*'')')
c    x      line(is1+1:is1+1+79)
          read(line(is1+1:),*) ifrst,ilast
          write(*,'(''Range number '',i6,
     x      '' : first/last residue indices: '',2i8,
     x      '' classname '',a20,'' class number '',i4)')
     x      i,ifrst,ilast,classname(iffound),iffound
          do j=ifrst,ilast
            if(iresnumclass(j).eq.-1) then
              iresnumclass(j)=iffound
            else
              write(*,'(''Error:  multiply assigned residue '',
     x          i7,'' in classes '',i3,'' and '',i3)') 
     x          j,iresnumclass(j),i
              stop 'multiply assigned residue'
            endif
          enddo
        enddo

        do i=1,nres
          if(iresnumclass(i).eq.-1) then
            write(*,'(''Error - residue '',i6,'' not assigned to'',
     x      '' a class '')') i

          endif
        enddo

        write(12,'(i5,'' number of classification schemes '')') 1
        write(12,'(i5,'' number of classes in this scheme'')') nclass
        do i=1,nclass
          is=index(classname(i),' ')-1
          write(12,'(a,5x,i5)') classname(i)(1:is),i
        enddo
        write(12,'(i10)') nsites

        do i=1,nsites
          j=resid(i)   !this atom is in residue number j
          write(*,'('' Site number '',i6,'' in residue '',i5)')
     x     i,j
c         k=residue(3,j) !this is the residue type of residue j
c         l=iresclass(k) !this is the class associated with residue type k
          l=iresnumclass(j)
          is=index(classname(l),' ')-1
          is1=index(atname(i),' ')-1
          write(*,'('' Atom '',i7,'' name '',a,'' residue number '',i5,
     x      '' class '',i3,'' classname '',a)') 
     x      i,atname(i)(1:is1),j,l,classname(l)(1:is)
          write(12,'(i6,2x,a)') l,classname(l)(1:is)
        enddo

        close(12)


      endif




c     loop over the molecule list; develop a list of unique molecule types 
c     moltype(5,100)      !information about each type of molecule
c     moltype(1,i) !index of first (representative) atom in this type of molecule
c     moltype(2,i) !index of last (representative) atom in this type of molecule
c     mollype(3,i) !count this type of molecule

c     for each molecule, what is the type of molecule (star, water, cargo)
c     molecule(3,i) = type of the molecule (points into moltype array)

      write(*,'(/,'' Number of molecules found is '',i6)') nmol

      nmtype=0  !number of molecule types found

      do i=1,nmol
        ifirst=molecule(1,i)
        ilast=molecule(2,i)
c       write(*,'('' Molecule '',i6,
c    x    '' first/last atom indices = '',2i7,
c    x    '' first/last residue indices = '',2i6,'' nres = '',i4)')
c    x    i,ifirst,ilast,
c    x    resid(ifirst),resid(ilast),
c    x    resid(ilast)-resid(ifirst)+1

        ifound=0
        do j=1,nmtype
          if(ifound.eq.0   .and.
c         identify a match if the number of atoms, first and last residue types are is same
     x      ilast-ifirst+1 .eq. moltype(2,j)-moltype(1,j)+1 .and.
     x      resname(ifirst)(1:4).eq.resname(moltype(1,j))(1:4) .and.
     x      resname(ilast )(1:4).eq.resname(moltype(2,j))(1:4)  )
     x      ifound=j
        enddo
        if(ifound.eq.0) then
          nmtype=nmtype+1
          moltype(1,nmtype)=ifirst
          moltype(2,nmtype)=ilast
          moltype(3,nmtype)=0    
          ifound=nmtype
        endif
        moltype(3,ifound)=moltype(3,ifound)+1
        molecule(3,i)=ifound  !save the type index of this molecule
      enddo

      write(*,'(/,'' Number of molecule types found is '',i5)') nmtype
      nmtot=0
      do i=1,nmtype
        write(*,'('' Molecule type '',i3,'' First/last atom '',2i7,
     x    '' count= '',i7)')  i,moltype(1,i),moltype(2,i),moltype(3,i)
        nmtot=nmtot+moltype(3,i)
      enddo
      write(*,'('' Total number of molecules found is '',i7)') nmtot 


c 
c
c     for each strand:  what is first and last residue


c     find the molecule type for strand identification
c     must have >3 atoms; must have >2 residues
c     expect this will be just one molecule
c     for now crash if more than one type and/or more than one molecule
      n=0
      do i=1,nmtype
        ifirst=moltype(1,i)
        ilast =moltype(2,i)
        irfirst=resid(ifirst)
        irlast =resid(ilast )
        if(ilast-ifirst+1.gt.3 .and. irlast-irfirst .gt. 2) then
          n=n+1
          istar=i
        endif
      enddo
      if(n.gt.1) then
        write(*,'('' Error - need new code to handle multiple'',
     x    '' types of polymers '')')
        stop 'new code needs to be written to handle multiple types'
      endif
      if(moltype(3,istar).gt.1) then
        write(*,'('' Error - need new code to handle multiple'',
     x    '' copies of polymer'')')
        stop 'new code needs to be written to handle multiple copies'
      endif

c     write(*,'(/,'' Strand analysis will be done on molecules'',
c    x  '' of type '',i3,'' with '',i5,'' atoms and '',i5,
c    x  '' residues.'')') istar,moltype(2,istar)-moltype(1,istar)+1,
c    x   resid(moltype(2,istar))-resid(moltype(1,istar))+1
c     write(*,'('' This is molecule '',i6,'' and spans atomic sites '',
c    x  i7,'' to '',i7,
c    x  '' and residues '',i7,'' to '',i7)') 
c    x  molid(moltype(1,istar)),moltype(1,istar),moltype(2,istar),
c    x  resid(moltype(1,istar)),resid(moltype(2,istar))


c     irfrst=resid(moltype(1,istar))
c     irlast=resid(moltype(2,istar))
c     write(*,'(/,'' Strand analysis on residues '',i7,'' to '',i7)')
c    x irfrst,irlast 



      


      end


c
c     subroutine to read in lmp parameter/coordinate file
c
      subroutine readlmp(fname,nsites,itype,molid,
     x                    charge,coord,atname,natmx,
     x                    nbonds,nbtp,bndpar,ijbond,nbnmx,nbtmx,
     x                    nangle,natp,angpar,ijkangle,nanmx,nantmx,
     x                    ndih,ndtp,dihpar,ijkldih,ndimx,nditmx,
     x                    natomtp,fmass,vdw,nametypes,nattpmx,
     x                    box)
      implicit real*8 (a-h,o-z)
      character*(*) fname
      character*120 line

c     natmx - max number of atoms (sites) (nsites.le.natmx) 
c     nattpmx - max number of atom types (natomtp.le.nattpmx)

c     nbonds - number of bonds
c     nbnmx - max number of bonds (nbonds.le.nbnmx)
c     nbtp - number of bond types
c     nbtmx - max number of bond types (nbtp.le.nbtmx)

c     nangle - number of angles
c     nanmx - max number of angles (nangle.le.nanmx)
c     natp - number of angle types
c     nantmx - max number of angle types (natp.le.nantmx)

c     ndih - number of dihedral angles
c     ndimx - max number of dihedral angles (ndih.le.ndimx)
c     ndtp - number of dihedral angle types
c     nditmx - max number of dihedral angle types (ndtp.le.nditmx)

c     natomtp - number of atom types
c     nattpmx - max number of atom types (natomtp.le.nattpmx)

      dimension coord(3,natmx),charge(natmx),itype(natmx),molid(natmx)
      character*(*) atname(natmx)
      dimension box(3,2) 

      dimension ijbond(3,nbnmx),bndpar(2,nbtmx)
      dimension ijkangle(4,nanmx),angpar(2,nantmx)
      dimension ijkldih(5,ndimx),dihpar(4,nditmx)

      dimension vdw(4,nattpmx),fmass(nattpmx)
      character*(*) nametypes(nattpmx)

      is=index(fname,' ')-1
      write(*,'(/,'' Reading parameters from file *'',a,''*'')')
     x     fname(1:is)
      open(10,file=fname)  

c     read number of atoms and their coordinates
c     read nsites,itype,charge,coords,atname

10    continue
      read(10,'(a120)',end=21) line
      if(index(line,'atoms').eq.0) go to 10
      read(line,*) nsites
      write(*,'('' natoms = '',i8)') nsites
      if(nsites.gt.natmx) then
        write(*,'('' Increase size of site array; '',/,
     x            '' nsites = '',i10,'' natmx = '',i10)')
     x            nsites,natmx
        stop 'Increase natmx'
      endif
21    continue


      write(*,'('' Reading coordinates and charges for '',i7,
     x  '' sites '')') nsites
20    continue
      read(10,'(a120)',end=22) line
      if(index(line,'Atoms').eq.0) go to 20
      read(10,'(a120)') line
      totchg=0.d0
      do i=1,nsites
        read(10,'(a120)') line
        read(line,*) ix,im,it1,ch1,x1,y1,z1
c       read(line,'(i10,i5,i5)') ix,im,it1 
c       read(line(21:),*) ch1,x1,y1,z1
        if(ix.gt.nsites.or.ix.le.0) then
          write(*,'('' Error: site index out of range i = '',i10,
     x      '' ix = '',i10)') i,ix
          stop 'error reading coordinates'
        endif 
        molid(ix)=im
        itype(ix)=it1
        charge(ix)=ch1
        coord(1,ix)=x1
        coord(2,ix)=y1
        coord(3,ix)=z1
        if(mod(i,5000).eq.0) then
          write(*,'('' Reading coordinates for '',i6,'' of '',i6)') 
     x      i,nsites
        endif
c       parse the line for a name string in the comment field
        iloc=index(line,'#')
        call leftjust(line(iloc+1:))
        ilocnm=index(line(iloc+1:),'_')


c
c       find token with an underscore and assume it is the atname field
c
        if(iloc.ne.0.and.ilocnm.ne.0) then
          ilocnm=ilocnm+iloc
c         # is at line(iloc:iloc);  underscore is at line(ilocnm:ilocnm)
c         search backwards from underscore location to a blank or the # to find beginning
          iptr=0
          do kk=ilocnm,iloc+1,-1
            if(iptr.eq.0.and.line(kk:kk).eq.' ') iptr=kk+1
          enddo 
          if(iptr.eq.0) iptr=iloc+1
          ilocnm=index(line(iptr:),' ')+iptr-1 -1  
          atname(ix)=line(iptr:ilocnm)
        else
c         special fixup code for water (should not be executed for proper names)
c         subroutine packer(elfield,i1,i2,i3,string)
          if(line(iloc+1:iloc+1).eq.'O'.or.
     x       line(iloc+1:iloc+1).eq.'H') then
            if(line(iloc+1:iloc+2).eq.'O ') then
              call packer('O',ix,1,0,atname(ix))   
            elseif(line(iloc+1:iloc+2).eq.'H1') then
              call packer('H',ix,1,0,atname(ix))   
            elseif(line(iloc+1:iloc+2).eq.'H2') then
              call packer('H',ix,1,0,atname(ix))   
            endif
          else
            write(atname(ix),'(''X'',i5.5)') ix
          endif
        endif
        totchg=totchg+charge(ix)
c       write(*,'('' IN ATOM LOOP: ix,im,it1,atname: '',3i5,
c    x    '' *'',a,''*'')')
c    x    ix,im,it1,atname(ix)
      enddo

      write(*,'('' Total charge '',f15.5)') totchg
22    continue
      
c     do i=1,nsites
c       write(*,'('' Site '',i6,'' *'',a,''* Type '',i5,3f10.5)')
c    x    i,atname(i)(1:20),itype(i)
c    x    ,(coord(k,i),k=1,3)
c     enddo

c
c     read box sizes
c
      rewind(10)
      ifxset=0
      ifyset=0
      ifzset=0
23    continue
      read(10,'(a120)') line
      isx=index(line,'xlo') 
      isy=index(line,'ylo') 
      isz=index(line,'zlo') 
      if(isx+isy+isz.eq.0) go to 23
      if(isx.ne.0) then 
        read(line,*) xlo,xhi
        ifxset=1
      elseif(isy.ne.0) then
        read(line,*) ylo,yhi
        ifyset=1
      elseif(isz.ne.0) then
        read(line,*) zlo,zhi
        ifzset=1
      endif
      if(ifxset*ifyset*ifzset.eq.0) go to 23
      write(*,'('' Box dimensions (X): '',2f15.8,/,
     x          '' Box dimensions (Y): '',2f15.8,/,
     x          '' Box dimensions (Z): '',2f15.8)')
     x  xlo,xhi,ylo,yhi,zlo,zhi
      box(1,1)=xlo
      box(1,2)=xhi
      box(2,1)=ylo
      box(2,2)=yhi
      box(3,1)=zlo
      box(3,2)=zhi


c     read bonds
c     read nbonds,nbtp,bndpar(2,*),ijbond(3,*)

      rewind(10)
30    continue
      read(10,'(a120)',end=40) line
      if(index(line,' bonds ').eq.0) go to 30
      read(line,*) nbonds
      if(nbonds.gt.nbnmx) then
        write(*,'('' Increase size of bond array; '',/,
     x            '' nbonds = '',i10,'' nbnmx = '',i10)')
     x            nbonds,nbnmx
        stop 'Increase nbnmx'
      endif
40    continue
      read(10,'(a120)',end=45) line
      if(index(line,' bond types ').eq.0) go to 40
      read(line,*) nbtp
c     write(*,'('' Nbond types = '',i6)') nbtp
      if(nbtp.gt.nbtmx) then
        write(*,'('' Increase size of bond type array; '',/,
     x            '' nbtp   = '',i10,'' nbtmx = '',i10)')
     x            nbtp,nbtmx
        stop 'Increase nbtmx'
      endif
45    continue
      read(10,'(a120)',end=50) line
      if(index(line,'Bond Coeffs').eq.0) go to 45
      read(10,'(a120)') line
      do i=1,nbtp  
        read(10,'(a120)') line
        read(line,*) ix,bndpar(1,i),bndpar(2,i)
c       write(*,'('' Bond type '',i3,'' Parm '',2f10.5)')
c    x     i,bndpar(1,i),bndpar(2,i)
      enddo
50    continue
      read(10,'(a120)',end=55) line
      if(index(line,'Bonds').eq.0) go to 50

      write(*,'('' Reading list of '',i7,
     x  '' bonds '')') nbonds
      read(10,'(a120)') line
      do i=1,nbonds
        read(10,'(a120)') line
        read(line,*) ix,ijbond(3,i),ijbond(1,i),ijbond(2,i)
        if(mod(i,10000).eq.0) then
          write(*,'('' Reading bonds '',i6,'' of '',i6)') 
     x      i,nbonds
        endif
c       write(*,'('' Bond '',i6,'' Type '',i4,'' Sites '',2i6)')
c    x    i,ijbond(3,i),ijbond(1,i),ijbond(2,i)
      enddo


c     read angles
c     nangle,natp,angpar(2,*),ijkangle(4,*)

      rewind(10)
55    continue
      read(10,'(a120)',end=60) line
      if(index(line,' angles').eq.0) go to 55
      read(line,*) nangle
      if(nangle.gt.nanmx) then
        write(*,'('' Increase size of angle array; '',/,
     x            '' nangle = '',i10,'' nanmx = '',i10)')
     x            nangle,nanmx
        stop 'Increase nanmx'
      endif
60    continue
      read(10,'(a120)',end=65) line
      if(index(line,' angle types ').eq.0) go to 60
      read(line,*) natp
      if(natp.gt.nantmx) then
        write(*,'('' Increase size of angle type array; '',/,
     x            '' natp   = '',i10,'' nantmx = '',i10)')
     x            natp,nantmx
        stop 'Increase nantmx'
      endif
65    continue
      read(10,'(a120)',end=70) line
      if(index(line,'Angle Coeffs').eq.0) go to 65
      read(10,'(a120)') line
      do i=1,natp  
        read(10,'(a120)') line
        read(line,*) ix,angpar(1,i),angpar(2,i)
      enddo
70    continue
      read(10,'(a120)',end=71) line
      if(index(line,'Angles').eq.0) go to 70
      write(*,'('' Reading list of '',i7,
     x  '' angles '')') nangle
      read(10,'(a120)') line
      do i=1,nangle
        read(10,'(a120)') line
        read(line,*) ix,ijkangle(4,i),(ijkangle(k,i),k=1,3)
      enddo
71    continue



c     read dihedrals
c     read ndih,ndtp,dihpar(4,*),ijkldih(5,*)

      rewind(10)
75    continue
      read(10,'(a120)',end=80) line
      if(index(line,' dihedrals').eq.0) go to 75
      read(line,*) ndih  
      if(ndih.gt.ndimx) then
        write(*,'('' Increase size of dihedral array; '',/,
     x            '' ndih   = '',i10,'' ndimx = '',i10)')
     x            ndih,ndimx
        stop 'Increase ndimx'
      endif
80    continue
      read(10,'(a120)',end=85) line
      if(index(line,' dihedral types ').eq.0) go to 80
      read(line,*) ndtp
c     write(*,'('' Dihedrals: '',i6,''  Types: '',i6)')
c    x   ndih,ndtp
      if(ndtp.gt.nditmx) then
        write(*,'('' Increase size of dihedral type array; '',/,
     x            '' ndtp   = '',i10,'' nditmx = '',i10)')
     x            ndtp,nditmx
        stop 'Increase nditmx'
      endif
85    continue
      read(10,'(a120)',end=90) line
      if(index(line,'Dihedral Coeffs').eq.0) go to 85
      read(10,'(a120)') line
      do i=1,ndtp  
        read(10,'(a120)') line
c       write(*,'('' Line: *'',a,''*'')') line(1:50)
        read(line,*) ix,dihpar(1,i),nd,dd,dihpar(4,i)
c       write(*,'('' '',i5,f10.4,i5,i5,f10.4)') 
c    x    is,dihpar(1,i),nd,dd,dihpar(4,i)
        dihpar(2,i)=nd         
        dihpar(3,i)=dd         
      enddo
c     write(*,'('' Types read in '')')
90    continue
      read(10,'(a120)',end=91) line
      if(index(line,'Dihedrals').eq.0) go to 90
      write(*,'('' Reading list of '',i7,
     x  '' dihedrals '')') ndih  
      read(10,'(a120)') line
      do i=1,ndih  
        read(10,'(a120)') line
        read(line,*) ix,ijkldih(5,i),(ijkldih(k,i),k=1,4)
      enddo
c     write(*,'('' Dihedrals read in '')')
91    continue


c     read vdw
c     read natomtp,vdw(4,*)

      rewind(10)
100   continue
      read(10,'(a120)',end=105) line
      if(index(line,' atom types ').eq.0) go to 100
      read(line,*) natomtp
      write(*,'('' Reading list of '',i7,
     x  '' vdw parameters '')') natomtp
      if(natomtp.gt.nattpmx) then
        write(*,'('' Increase size of vdw array; '',/,
     x            '' natomtp   = '',i10,'' nattpmx = '',i10)')
     x            natomtp,nattpmx
        stop 'Increase nattpmx'
      endif
      ifound=0
105   continue
      read(10,'(a120)',end=107) line
      if(index(line,'Pair Coeffs').eq.0) go to 105
      ifound=1
      read(10,'(a120)') line
      do i=1,natomtp  
        read(10,'(a120)') line
        read(line,*) ix,vdw(2,i),vdw(1,i),vdw(4,i),vdw(3,i)
        write(*,'('' Atom type '',i3,'' vdw = '',2f10.5)')
     x     i,vdw(1,i),vdw(2,i)
      enddo
107   continue
      if(ifound.eq.0) then
        write(*,'('' Error:  this lmp file is missing '',
     x  '' the Pair Coeffs block needed for generating mixtures '')')
        stop 'missing Pair Coeffs block in lmp file'
      endif

c
c     read masses 
c     note that we are assuming that extra information is on these
c     lines in files made by ffassign with type names
c
      rewind(10)
26    continue
      read(10,'(a120)') line
      if(index(line,'Masses').eq.0) go to 26
      write(*,'('' Reading list of '',i7,
     x  '' masses  '')') natomtp
      read(10,'(a120)') line
      do i=1,natomtp  
        read(10,'(a120)') line
        read(line,*) ix,fmass(i)
c       write(*,'('' Atom type '',i3,'' mass = '',f10.5)')
c    x     i,fmass(i)           

c     extract the name string: character*100 nametypes(100)
        nametypes(i)=' '
        is=index(line,'#')
        ks=1
        ie=1
        if(is.ne.0) then
c         in line(is+1:) look for LAST token         
c         this will be a nonblank string surrounded by blanks
          ie=0
          do js=len(line),is+1,-1
            if(line(js:js).ne.' ') then
              ie=js
              go to 210
            endif
          enddo
210       continue
          if(ie.ne.0) then
            ks=0
            do js=ie,is+1,-1
              if(line(js:js).eq.' ') then
                ks=js+1 
                go to 215
              endif
            enddo
          endif
215       continue
c         at this point the last token is in line(ks:ie) 
          nametypes(i)=line(ks:ie)
        endif
        write(*,'('' For atom type '',i3,'' Name *'',a,''*'')')
     x    i,nametypes(i)(1:ie-ks+1)
      enddo
c


200   continue

      close(10)


      end


c
c     left justify a string
c
      subroutine leftjust(string)
      character*(*) string

      is=0
      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 10
        endif
      enddo

10    continue
      if(is.eq.0) return  !all blank string

      do i=1,len(string)-is+1
        string(i:i)=string(is+i-1:is+i-1)
      enddo
c     blank out the last is-1 characters
      do i=len(string)-is+2,len(string)
        string(i:i)=' '
      enddo

      end


c
c     subroutine lstchr returns the index of the last nonblank character in a string
c
      subroutine lstchr(string,ilst)
      character*(*) string 

      ilst=0  !if string is all blank, return zero
      do i=len(string),1,-1
        if(string(i:i).ne.' ') then
          ilst=i
          go to 10
        endif
      enddo
10    continue

      end



c
c     subroutine to parse a string into an element field and up to 3 integers
c     unused fields will be returned with values of -1
c
      subroutine parser(string,elfield,lelfield,i1,i2,i3)
      character*(*) string,elfield
      character*120 line
      character*10 numbers
      dimension is(3)
      data numbers/'1234567890'/

      do i=1,3
        is(i)=-1
      enddo
      line=string
      call leftjust(line)
      ne=2 !assume two character element name
      ifnum=index(numbers,line(2:2))
      if(line(2:2).eq.' '.or.line(2:2).eq.'_'.or.ifnum.ne.0) ne=1
      lelfield=ne
      elfield=line(1:ne)

      do i=1,3
        if(line(ne+1:ne+1).eq.'_') then
c         assume there is a number field after the underscore
c         look for next space or underscore
          l=index(line(ne+2:),'_')
          if(l.gt.0) then
            read(line(ne+2:ne+l),*) is(i)
            ne=ne+l
          else
            read(line(ne+2:),*) is(i)
            go to 10
          endif
        endif
      enddo

10    continue

      i1=is(1)
      i2=is(2)
      i3=is(3)

      end

c
c     function to get atomic number given a string
c     holding the element name
c


      integer function igetatnum(string)
      character*(*) string
      character*26 upper,lower
      character*10 numbers
      character*80 elements
      character*2 ename
      
      upper='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
      lower='abcdefghijklmnopqrstuvwxyz'
      numbers='0123456789'
      elements(1:20)= 'H HELIBEB C N O F NE' 
      elements(21:40)='NAMGALSIP S CLARK CA'
      elements(41:60)='SCTIV CRMNFECONICUZN'
      elements(61:80)='GAGEASSEBRKRRBSRY ZR'

      igetatnum=o

      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 100
        endif
      enddo
100   continue
      ie=len(string)
      do i=is+1,len(string)
        ifnum=index(numbers,string(i:i))
        if(string(i:i).eq.'_'.or.string(i:i).eq.' '.or.
     x    ifnum.ne.0) then
          ie=i-1
          go to 200
        endif
      enddo
200   continue

      ename(1:2)=string(is:ie)  !pads with blanks
c     write(*,'('' ENAME =*'',a2,''*'')') ename

c     element string is in string(is:ie)
      do i=1,2  
        ix=index(lower,ename(i:i))
        if(ix.ne.0) ename(i:i)=upper(ix:ix)
      enddo
c     write(*,'('' ENAME =*'',a2,''*'')') ename

      ix=index(elements,ename(1:2))
      if(ix.eq.0) then
        write(*,'('' Element not found: '',a2)') ename
        stop 'ERROR:  missing element in igetatnum'
      endif
    
      igetatnum=1+(ix-1)/2

      end
c
c     subroutine to populate the atomic masses array
c
      subroutine amassinit(amass,n)
      implicit real*8 (a-h,o-z)
      dimension amass(n)

c     atomic masses as needed
      do i=1,120
        amass(i)=0.d0
      enddo

      amass(1)=1.00794d0   !agrees w/Mulliken
      amass(2)=4.002602d0
      amass(3)=6.941d0
      amass(4)=9.012182d0
      amass(5)=10.811d0

      amass(6)=12.0107d0
      amass(6)=12.0110d0    !agrees w/Mulliken
      amass(7)=14.0067d0
      amass(8)=15.9994d0    !agrees w/Mulliken
      amass(9)=18.9984032d0
      amass(10)=20.1797d0

      amass(11)=22.98976928d0
      amass(12)=24.305d0
      amass(13)=26.9815386d0
      amass(14)=28.0855d0
      amass(15)=30.973762d0

      amass(16)=32.065d0
      amass(17)=35.453d0
      amass(18)=39.948d0
      amass(19)=39.0983d0
      amass(20)=40.078d0

      amass(21)=44.955912d0
      amass(22)=47.867d0
      amass(23)=50.9415d0
      amass(24)=51.9961d0
      amass(25)=54.938045d0

      amass(26)=55.845d0
      amass(27)=58.933195d0
      amass(28)=58.6934d0
      amass(29)=63.546d0
      amass(30)=65.38d0

      amass(31)=69.723d0
      amass(32)=72.64d0
      amass(33)=74.9216d0
      amass(34)=78.96d0
      amass(35)=79.904d0
      amass(36)=83.798d0

      end 



C
C     MACHINE INDEPENDENT (ALMOST) RANDOM NUMBER GENERATOR
C     SLOPPILY CONVERTED TO DOUBLE PRECISION.
C     LOW 32 BITS OF FLOATING POINT RANDOM NUMBER ARE ZERO.
C     I.E., THEY ARE ONLY SINGLE PRECISION RANDOM ASSIGNED TO DOUBLES.
C
      DOUBLE PRECISION FUNCTION RAN(IX)
      INTEGER*4 IX,I,I1,I2,I3,I4,I5,I6,I7,I8,I9
      DATA I2/16807/,I4/32768/,I5/65536/,I3/2147483647/
      I6=IX/I5
      I7=(IX-I6*I5)*I2
      I8=I7/I5
      I9=I6*I2+I8
      I1=I9/I4
      IX=(((I7-I8*I5)-I3)+(I9-I1*I4)*I5)+I1
      IF(IX.LT.0) IX=IX+I3
      RAN=(4.656613E-10)*FLOAT(IX)
      RETURN
      END

c
c     subroutine to pack up to four fields into a string
c
      subroutine packer(elfield,i1,i2,i3,string)
      character*(*) elfield,string
      character*120 line
      character*10 num
      dimension is(3)
      line=elfield
      call leftjust(line)
c     assume this field is either one or two characters, look for a space or an underscore
      ne=2 !assume 2 character element name
      if(line(2:2).eq.' '.or.line(2:2).eq.'_') ne=1 !set it to 1 character in these cases
      is(1)=i1
      is(2)=i2
      is(3)=i3
      
      do i=1,3
        if(is(i).ge.0) then
          write(num,'(i10)') is(i)
          ip=0
          do j=1,len(num)
            if(ip.eq.0.and.num(j:j).ne.' ') ip=j 
          enddo
          line(ne+1:ne+len(num)-ip+2)='_'//num(ip:len(num))   !move len(num)-j+1 characters
          ne=ne+len(num)-ip+2
        endif
      enddo

      string=line

      end
