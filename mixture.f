c
c     program to read lmp files, each describing a molecule,
c     and combine them to make a mixture
c 
c     plan to support ways of placing the molecules,
c     but start with placement on a cubic lattice
c     with the number of sites the next larger cube from
c     the number of molecules to be added.  
c
c     alternate molecules types to end up with the designed
c     number and proportions.
c
c     set the volume of the box to result in desired density.
c
c     NOT DONE YET: would prob be a good idea to shift box boundary from 0 to L
c     and adjust coordinates accordingly.  (Not sure why they are not always 0:L)
c     
c     change program 11/2012 to support ifsuper=3 mode
c
c     a) rescaling of volume
c      
c        Expect volume changes to be small and hope first that scaling atomic 
c        positions will work.  If not, try scaling COG molecule positions.  If
c        that doesn't work, should scale positions of clusters, but not sure
c        what to do if clusters span the box. (atoms and molecules don't have that
c        problem); need to give a scale factor or an incremental volume to determine
c        the new volume.
c
c     b) insertion of molecules of type 2 into the system described as type 1
c        
c        This mode will place some number of molecule at random positions.
c        A bump check is performed and placement is rejected if overlap
c        Could provide provision to check for bonds through a ring
c        If there is no place, we need to be smarter about the location of insertion.
c        Jed has implemented an approach where voids are identified and
c        placement attempts are limited to these locations.  
c        The cell volume is mapped onto a 3D array with cells of approx 1 Angstrom
c        and cells are labelled as occupied or not in a Boolean array.
c        If placement is not possible even with volume expansion, we will have to
c        expand greatly, insert and then compress to the desired volume.
c        Could attempt a set of molecule reorientations at each site to try to
c        find a better fit given a location for the COG.
c       
c     
c
c
c
c     this program began as a modification of 
c     merge.f (designed to solvate a large polymeric molecule
c     in a large water box).   
c
c
c     changes that have to be made in other programs now
c     1) expect ffassign.f to place residue names in comment field of Masses block
c     2) expect mixture.f or the code that solvates molecules to put atom names in the
c        Atoms block, EVEN FOR WATER:  format is C_2333_3_143  or O_2342_1_143
c        where 143 is a residue number (each water molecule is a residue)
c        not sure about the hydrogens on the water, i.e., if the site to which bonded
c        is to be indicated
c
c
c
      implicit real*8 (a-h,o-z)
      character*120 mname(10),line,string,xyzname,fname,olmpname
      dimension num(10),wtmol(10)
      dimension nsites(10),nbonds(10),nbtp(10),nangle(10),natp(10)
      dimension ndih(10),ndtp(10),natomtp(10)

      dimension coord(3,250000,2),charge(250000,2)
      dimension itype(250000,2),molid(250000,2)
      character*20 atname(250000,2)
      dimension ijbond(3,300000,2),bndpar(2,5000,2)
      dimension ijkangle(4,300000,2),angpar(2,5000,2)
      dimension ijkldih(5,700000,2),dihpar(4,24000,2)
      dimension vdw(4,100,2),fmass(100,2)
      character*100 nametypes(100,2)
c     character*20 resname(250000,2)  !"residue" names from corresponding pdb files
    
      dimension box(3,2,2)
      dimension lseq(100000) !dimension must be for the total number of molecules in the system
      dimension amass(120)
      dimension ibuf(4,2)
      dimension temp1(3,100),temp2(3,100)  !temp array for original and rotated coordinates

c     following supports superposition mode 2 (insert molecules of a type already seen)
      dimension maptp(100) !dimension to at least number of atom types in the second molecule
      dimension mapbn(5000)!dimension to at least number of bond types in the second molecule
      dimension mapan(5000)!dimension to at least number of angletypes in the second molecule
      dimension mapdi(5000)!dimension to at least number of dihedtypes in the second molecule


      external ran

      natmx=250000 !size of site-based arrays
      nattpmx=100  !size of atom type arrays
      nbnmx=300000  !size of bond list arrays
      nbtmx=5000   !size of bond type arrays
      nanmx=300000  !size of angle list arrays
      nantmx=5000  !size of angle type arrays
      ndimx=700000  !size of dihedral list array
      nditmx=5000  !size of the dihedral type array


    
      call getarg(1,line)
      if(index(line,'.mxin').eq.0) then
        write(*,'(''Error:  expecting input file of type *.mxin '',
     x    ''to be mentioned on command line'')')
        stop 'Expecting file of type mxin mentioned on command line'
      endif
      write(*,'(/,''Mixture generation program '')')
      call leftjust(line)
      is=index(line,' ')-1
      write(*,'(/,''Input file '',a)') line(1:is)
      open(20,file=line(1:is))



      read(20,'(a120)') fname   !this is the output file name
      call leftjust(fname)
      is=index(fname,' ')-1
      write(*,'(''Output LMP file:  '',a)') fname(1:is)

      read(20,'(a120)') line
      read(line,*) ifolmp   ! ifolmp=1 means take name of a reference lmp file
      if(ifolmp.eq.1) then
        call leftjust(line)
        is=index(line,' ')+1
        call leftjust(line(is:))
        olmpname=line(is:)
        is=index(olmpname,' ')-1
        olmpname(is+1:)=' '
        write(*,'(''Reference LMP:  '',a,
     x    '' (for atom type names)'')') olmpname(1:is)
      endif


      read(20,*) ntypes
c     if(ntypes.ne.2) then
c       BUT I HOPE ONE TYPE STILL WORKS
c       write(*,'(''Error:  only implemented for two system types'')')
c       stop 'only have implementation for two types'
c     endif
      do i=1,ntypes
        read(20,'(a120)') line
        call leftjust(line)
        read(line,*) num(i)
        is=index(line,' ')+1
        call leftjust(line(is:))
        is1=index(line(is:),' ')+is-2
        if(num(i).eq.1) then
        write(*,'(''Component '',i2,'':  '',i8,
     x    '' copy from file   '',a)')
     x    i,num(i),line(is:is1)
        else
        write(*,'(''Component '',i2,'':  '',i8,
     x    '' copies from file '',a)')
     x    i,num(i),line(is:is1)
        endif
        mname(i)=line(is:is1)
      enddo


      read(20,'(a120)') line
      read(line,*) ifsuper   !superposition mode
      write(*,'(/,''Superposition mode '',i2)') ifsuper
      if(ifsuper.eq.0) then
        write(*,'(''In this mode, molecules from each file '',
     x    ''are placed on a grid.'')')
        call leftjust(line)
        is=index(line,' ')+1
        read(line(is:),*) rho  !read target density for superposition mode 0
        write(*,'(''Target density (rho) '',f10.4)') rho
      elseif(ifsuper.eq.1) then
        write(*,'(''In this mode, molecules from the second file '',
     x    ''are overlayed on a system established by the first.'')')
      elseif(ifsuper.eq.2) then
        write(*,'(''In this mode, molecules from the second file '',
     x    ''are inserted at random sites in a system '',
     x    ''established by the first.'')')
        call leftjust(line)
        is=index(line,' ')+1
        call leftjust(line(is:))
        read(line(is:),*) vmol2  !read volume assumed for molecules of type 2      
        is=index(line(is:),' ')+is
        call leftjust(line(is:))
c       note:  to prevent growth in the number of bond, angle and torsion types
c       as each iteration of reactant is added, need to map new bonds, angles and
c       torsions onto the ones of the old set
        read(line(is:),*) ntp2,(maptp(i),i=1,ntp2) !number of types and type mapping     
        read(20,*) nbntp2,(mapbn(i),i=1,nbntp2) !number of bond types and type mapping     
        read(20,*) nantp2,(mapan(i),i=1,nantp2) !number of angle types and type mapping     
        read(20,*) nditp2,(mapdi(i),i=1,nditp2) !number of dihed types and type mapping     
      endif






      close(20)




c     eventually read this in, but for now just set
c     order of molecules determines order in lmp file
c     names of lmp files and number of each
c     ntypes=2

c     mname(1)='plal32-1.lmp'
c     mname(2)='plad32.lmp'
c     mname(1)='plal32-1.lmp'
c     mname(2)='plad32-1.lmp'
c     mname(1)='acetone.lmp'
c     mname(1)='gelcore_ua.lmp'
c     mname(2)='roh32_ua.lmp'

c     ntypes=1
c     mname(1)='tetra.lmp'
c     mname(1)='staradam16pdvldme.lmp'
c     mname(2)='ibuprofen.lmp'

c     num(1)=512  !number of molecules of type 1
c     num(1)=1000 !number of molecules of type 1
c     num(2)=250  !number of molecules of type 2

c     num(1)=1  !number of molecules of type 1
c     num(2)=1  !number of molecules of type 2

c     rho=0.84197d0  !target mass density
c     rho=0.01d0     !target mass density

c     rho=1.009d0   !target mass density, tetraglyme
c     Hans diamide_2 system of 560 molecules
c     mname(1)='diamide_2.lmp'
c     num(1)=560  !number of molecules of type 1
c     rho=1.00000d0  !target mass density

c     mname(1)='merge.lmp'
c     mname(2)='benzen.lmp'

c     num(1)=1    !number of molecules of type 1
c     num(2)=12   !number of molecules of type 2

c     3:1 N=10000
c     num(1)=7500 !number of molecules of type 1 (lactone)
c     num(2)=2500 !number of molecules of type 2 (roh)
c     4:1 N=10000
c     num(1)=8000 !number of molecules of type 1 (lactone)
c     num(2)=2000 !number of molecules of type 2 (roh)
c     5:1 N=12000
c     num(1)=7500 !number of molecules of type 1 (lactone)
c     num(2)=2500 !number of molecules of type 2 (roh)
c     10:1 N=11000
c     num(1)=10000 !number of molecules of type 1 (lactone)
c     num(2)=1000 !number of molecules of type 2 (roh)
c     1:7 N=16000
c     num(1)=2000  !number of molecules of type 1 (lactone)
c     num(2)=14000 !number of molecules of type 2 (roh)
c     1:7 N=8000 !small 1:7 system
c     num(1)=1000  !number of molecules of type 1 (lactone)
c     num(2)=7000 !number of molecules of type 2 (roh)
c     1:2 N=15000
c     num(1)=5000  !number of molecules of type 1 (lactone)
c     num(2)=10000 !number of molecules of type 2 (roh)

c     num(1)=1000 !number of molecules of type 1
c     rho=1.00d0  !target mass density


      ifreplace=0 !use coordinates in the lmp file
c     ifreplace=1 !use coordinates in an xyz file
      xyzname='merge_test.50.xyz'
      xyzname='last_staradam16pdvldme_setup.1.xyz'



c     ifsuper=0  !superposition mode:  if this = 0 place molecules on lattice (normal operation) 
c     ifsuper=1  !superposition mode:  if this = 1 overlay molecules of type 2 into system type 1    
c     if(ifsuper.eq.1) then
c
c       In this superposition mode, some number of molecules of type 2 are placed 
c       into a system determined by type 1.
c
c     endif


c     if(ifsuper.eq.2) then
c
c       establish placement information for the molecules of type 2
c       could be random positions and orientations; 
c       could be random positions in a spherical shell, etc.
c
c       new mode in which molecules are randomly placed 
c       into voids.  another position is tried if bump check fails
c       note that in this superposition mode, we assume that the 
c       second molecule type exists already in the first system, and so
c       in this case, don't reproduce atom types, but map onto existing ones
c       need to identify the atom types that correspond to the new molecule
c
c     endif




      do i=1,ntypes
        call readlmp(mname(i),
     x         nsites(i),itype(1,i),molid(1,i),charge(1,i),coord(1,1,i),
     x         atname(1,i),natmx,
     x         nbonds(i),nbtp(i),bndpar(1,1,i),ijbond(1,1,i),
     x         nbnmx,nbtmx,
     x         nangle(i),natp(i),angpar(1,1,i),ijkangle(1,1,i),
     x         nanmx,nantmx,
     x         ndih(i),ndtp(i),dihpar(1,1,i),ijkldih(1,1,i),
     x         ndimx,nditmx,
     x         natomtp(i),fmass(1,i),vdw(1,1,i),nametypes(1,i),
     x         nattpmx,
     x         box(1,1,i))
      enddo

c
c     following code replaces the nametypes information with that from a reference
c     lmp file
c     check that the number of types and masses is compatible between this reference
c     lmp file and the ones in system 1;  if not, stop
c
      if(ifolmp.eq.1) then
        open(20,file=olmpname)
10      read(20,'(a120)',end=15) line
        if(index(line,'atom types').eq.0) go to 10
        go to 20
15      write(*,'(''End of file reached in reference LMP file '',
     x    ''without finding atom types'')')
        stop 'Error - no atom types line in reference LMP file'
20      read(line,*) nolmptp
        if(nolmptp.ne.natomtp(1)) then
          write(*,'(''Error: different numbers of atom types in '',
     x      ''first system and reference LMP file '')')
          stop 'Error - different numbers of types in LMP files'
        endif
        rewind(20)
25      read(20,'(a120)',end=15) line
        if(index(line,'Masses').eq.0) go to 25
        read(20,'(a120)') !blank
        do i=1,nolmptp  
          read(20,'(a120)') line
          read(line,*) ix,folmpmass  !atom type and mass
          if(folmpmass.ne.fmass(i,1)) then
            write(*,'(''Error - masses do not agree between '',
     x        ''reference LMP file and that of first system '')')
            stop 'Masses do not agree between LMP files'
          endif
c         extract the nametypes string (last token and after #)
          is=index(line,'#')
          if(is.eq.0) then
            write(*,'(''Error - no nametypes string in reference '',
     x        ''LMP file'')')
            stop 'No nametypes string in reference LMP file'
          endif
          do js=len(line),is+1,-1
            if(line(js:js).ne.' ') then
              ie=js  !ie is index of last nonblank character
              go to 30 
            endif
          enddo
30        continue
          do js=ie,is+1,-1
            if(line(js:js).eq.' ') then
              ks=js+1 
              go to 32 
            endif
          enddo
32        continue
          is=index(nametypes(i,1),' ')-1
          write(*,'(''Original name type '',i2,'':'',a,
     x      ''; new name '',a)')
     x      i,nametypes(i,1)(1:is),line(ks:ie)
          nametypes(i,1)=line(ks:ie) !last token is in line(ks:ie) 
        enddo
        close(20)

c       another patch needed when using lmp files produced by restart2data
c       is to get a valid set of atom names; for simple cases construct
c       this from the nametypes data just read in
c       structure of nametypes just read in is molname_element_num1_num2_num3
c       extract element_num1_num2,num3 for atom names
        iat=0
        imol=1
        do i=1,nsites(1)
          if(molid(i,1).ne.imol) then
            imol=molid(i,1)
            iat=0
          endif
          iat=iat+1
          it=itype(i,1)
          line=nametypes(it,1)
          call leftjust(line)
          is=index(line,'_')+1
          is1=index(line(is:),'_')+is-2
          atname(i,1)=' '
          if(iat.le.9) write(atname(i,1),'(a,i1)')  
     x      line(is:is1)//'_',iat
          if(iat.gt.9) write(atname(i,1),'(a,i2)')  
     x      line(is:is1)//'_',iat
          is=index(atname(i,1),' ')-1
          write(*,'(''Site '',i7,'' molid '',i6,'' name*'',a,''*'')') 
     x      i,imol,atname(i,1)(1:is)
        enddo





      endif







c
c     following code to replace the coordinates from the lmp files with 
c     those from an xyz file.  (note that lmp files often contain 
c     coordinates "as built" whereas the xyz files can represent some
c     degree of equilibration, optimization or thermalization
c     note that the xyz file must be compatible with the lmp file
c
      if(ifreplace.eq.1) call getcoord(xyzname,nsites(1),itype(1,1),
     x   coord(1,1,1),natmx)



c     left justify the site names
      do i=1,ntypes
        do j=1,nsites(i)
          call leftjust(atname(j,i))
        enddo
      enddo


      is=index(fname,' ')-1
      write(*,'(//,'' Writing LAMMPS file:  '',a)')
     x  fname(1:is)
      open(15,file=fname(1:is))


      nattypes=0
      nbntypes=0
      nantypes=0
      nditypes=0

      write(*,*)
      do i=1,ntypes
        write(*,'('' Molecular type information for type '',i2)') i
        write(*,'(''   Number of atom types in this molecule  '',i8)')
     x    natomtp(i)
        write(*,'(''   Number of bond types in this molecule  '',i8)')
     x    nbtp(i)
        write(*,'(''   Number of angle types in this molecule '',i8)')
     x    natp(i)
        write(*,'(''   Number of dihedral types this molecule '',i8)')
     x    ndtp(i)
        nattypes=nattypes+natomtp(i)
        nbntypes=nbntypes+nbtp(i)
        nantypes=nantypes+natp(i)
        nditypes=nditypes+ndtp(i)
      enddo
      write(*,*)
      write(*,'('' Total number of atom types (all component) '',i6)')
     x  nattypes 
      write(*,'('' Total number of bond types (all component) '',i6)')
     x  nbntypes 
      write(*,'('' Total number of angle types (all component)'',i6)')
     x  nantypes 
      write(*,'('' Total number of dihedral type (all compont)'',i6)')
     x  nditypes 



      nsitet=0
      nbondt=0
      nanglet=0
      ndiht=0
      nmol=0

      write(*,*)
      do i=1,ntypes
        write(*,'('' Molecular component '',i2)') i
        write(*,'(''   Number of atoms in this molecule type  '',i8)')
     x    nsites(i)
        write(*,'(''   Number of bonds in this molecule type  '',i8)')
     x    nbonds(i)
        write(*,'(''   Number of angles in this molecule type '',i8)')
     x    nangle(i)
        write(*,'(''   Number of dihedrals in this molecule   '',i8)')
     x    ndih(i)
        wtmol(i)=0.d0
        do j=1,nsites(i)
c         write(*,'('' site '',i3,'' type '',i3,'' name '',a10,
c    x      '' mass '',f10.4)')
c    x      j,itype(j,i),atname(j,i)(1:10),fmass(itype(j,i),i)
          wtmol(i)=wtmol(i)+fmass(itype(j,i),i)
        enddo
        write(*,'(''   Molecular wt of this molecule type'',f13.4)')
     x    wtmol(i)
        write(*,'(''   Number of molecules of this component  '',i8)')
     x    num(i)
        write(*,'(''   Number of atoms for this component     '',i8)')
     x    num(i)*nsites(i)
        write(*,'(''   Number of bonds for this component     '',i8)')
     x    num(i)*nbonds(i)
        write(*,'(''   Number of angles for this component    '',i8)')
     x    num(i)*nangle(i)
        write(*,'(''   Number of dihedrals for this component '',i8)')
     x    num(i)*ndih(i)   
        nmol=nmol+num(i)
        nsitet=nsitet+nsites(i)*num(i)
        nbondt=nbondt+nbonds(i)*num(i)
        nanglet=nanglet+nangle(i)*num(i)
        ndiht=ndiht+ndih(i)*num(i)
      enddo

      write(*,*)
      write(*,'('' Total number of molecules (all component)  '',i6)')
     x  nmol   
      write(*,'('' Total number of atoms (all component)      '',i6)')
     x  nsitet   
      write(*,'('' Total number of bonds (all component)      '',i6)')
     x  nbondt   
      write(*,'('' Total number of angles (all component)     '',i6)')
     x  nanglet  
      write(*,'('' Total number of dihedrals (all component)  '',i6)')
     x  ndiht  



      if(ifsuper.eq.2) then

        if(ntp2.ne.natomtp(2)) then
          write(*,'(''Error:  number of types in the mapping array '',
     x      ''for molecule of type 2 is '',i3,/,
     x      ''and this does not match the number of types read in, '',
     x      i3)') ntp2,natomtp(2)
        endif
        if(nbntp2.ne.nbtp(2)) then
          write(*,'(''Error:  number of bond types in the mapping'',
     x      '' array for molecule of type 2 is '',i3,/,
     x      ''and this does not match the number of types read in, '',
     x      i3)') nbntp2,nbtp(2)
        endif
        if(nantp2.ne.natp(2)) then
          write(*,'(''Error:  number of angle types in the mapping'',
     x      '' array for molecule of type 2 is '',i3,/,
     x      ''and this does not match the number of types read in, '',
     x      i3)') nantp2,natp(2)
        endif
        if(nditp2.ne.ndtp(2)) then
          write(*,'(''Error:  number of types in the mapping array '',
     x      ''for molecule of type 2 is '',i3,/,
     x      ''and this does not match the number of types read in, '',
     x      i3)') nditp2,ndtp(2)
        endif

        write(*,'(/,''In superposition mode 2 the atom types'',
     x    '' for the second molecule type are already included.'')')
        do i=1,natomtp(2)
          write(*,'(''Second atom type '',i3,'' maps to type '',i3,
     x      '' in the first system.'')') i,maptp(i)
        enddo
        write(*,*)
        do i=1,nbtp(2)
          write(*,'(''Second bond type '',i3,'' maps to type '',i3,
     x      '' in the first system.'')') i,mapbn(i)
        enddo
        write(*,*)
        do i=1,natp(2)
          write(*,'(''Second angl type '',i3,'' maps to type '',i3,
     x      '' in the first system.'')') i,mapan(i)
        enddo
        write(*,*)
        do i=1,ndtp(2)
          write(*,'(''Second dihe type '',i3,'' maps to type '',i3,
     x      '' in the first system.'')') i,mapdi(i)
        enddo

        write(*,'(/,''Volume assumed for molecules of type 2 is '',
     x    f10.4,'' Angstrom**3'')') vmol2
        write(*,'(''This implies a cell volume increase for the'',
     x    '' system of '',f15.4,'' Angstroms**3'')') 
     x    num(2)*vmol2
      endif



      write(15,'('' LMP file generated by mixture program '')') !header
      write(15,*)
      write(15,'(5x,i10,'' atoms '')') 
     x  nsitet
      write(15,'(5x,i10,'' bonds '')') 
     x  nbondt
      write(15,'(5x,i10,'' angles '')') 
     x  nanglet
      write(15,'(5x,i10,'' dihedrals '')')  
     x  ndiht






      write(15,*)
      if(ifsuper.ne.2) then
        write(15,'(5x,i10,'' atom types '')') nattypes
        write(15,'(5x,i10,'' bond types '')') nbntypes   
        write(15,'(5x,i10,'' angle types '')') nantypes   
        write(15,'(5x,i10,'' dihedral types '')') nditypes   
      else
c       this mode assumes these types have already been defined in the first system
        write(15,'(5x,i10,'' atom types '')') natomtp(1)
        write(15,'(5x,i10,'' bond types '')') nbtp(1)   
        write(15,'(5x,i10,'' angle types '')') natp(1)
        write(15,'(5x,i10,'' dihedral types '')') ndtp(1)
      endif

c
c     determine cell dimensions for target density (g/cm**3)
c


      do i=1,ntypes
        write(*,*)
        write(*,'('' Molecular component '',i2)') i
        write(*,'('' Bounding cell at xmin = '',f10.5,'' xmax = '',
     x    f10.5)')  box(1,1,i),box(1,2,i)
        write(*,'('' Bounding cell at ymin = '',f10.5,'' ymax = '',
     x    f10.5)')  box(2,1,i),box(2,2,i)
        write(*,'('' Bounding cell at zmin = '',f10.5,'' zmax = '',
     x    f10.5)')  box(3,1,i),box(3,2,i)
        write(*,'('' Density (g/cm**3) = '',f10.5)') 
     x    (10.d0/6.02214179d0)*wtmol(i)/
     x    ( (box(1,2,i)-box(1,1,i))
     x   *  (box(2,2,i)-box(2,1,i))
     x   *  (box(3,2,i)-box(3,1,i)) )
      enddo

      wt=0.d0
      do i=1,ntypes
        wt=wt+num(i)*wtmol(i)
      enddo


c     in superposition mode 2, increase the dimensions of the first system
      if(ifsuper.eq.2) then
        write(*,'(/,''In superposition mode 2, volume and density'',
     x    '' derived:'')')
        volume0=((box(1,2,1)-box(1,1,1))*
     x           (box(2,2,1)-box(2,1,1))*
     x           (box(3,2,1)-box(3,1,1)) )
        write(*,'(''Original volume (system 1):        '',f15.4)')
     x    volume0
        write(*,'(''Added volume from molecule type 2: '',f15.4)')
     x    num(2)*vmol2
        volume=num(2)*vmol2+volume0
        write(*,'(''New total volume:                  '',f15.4)')
     x    volume
        write(*,'(''Volume expansion factor:           '',f15.6)')
     x    volume/volume0
        scale=(volume/volume0)**(1.d0/3.d0)
        write(*,'(''Edge length expansion factor:      '',f15.6)')
     x    scale                         
        rho=wt*(1.d0/volume)*(10.d0/6.02214179d0)   !new density in gm/cm**3
      endif


      volume=wt*(1.d0/rho)*(10.d0/6.02214179d0)   !volume in (Angstrom)**3


c     in superposition mode 1, just use the dimensions of the first system
      if(ifsuper.eq.1) then
        volume = 
     x    (box(1,2,1)-box(1,1,1)) 
     x  * (box(2,2,1)-box(2,1,1)) 
     x  * (box(3,2,1)-box(3,1,1)) 
        rho=wt*(1.d0/volume)*(10.d0/6.02214179d0)   !volume in (Angstrom)**3
      endif


      write(*,*)
      write(*,'(''Cell dimensions to be used in new system '')')
      if(ifsuper.eq.1) write(*,'('' Superposition mode 1 in effect. '',
     x  '' Using cell dimensions of first system.'')')
      if(ifsuper.eq.2) write(*,'('' Superposition mode 2 in effect. '',
     x  '' Using cell dimensions of first system with increase for'',
     x  '' new material of second system.'')')
      edge=volume**(1.d0/3.d0)           !edge length in Angstrom
      write(*,'(/,''Cubic simulation cell edge length '',f20.8,
     x  '' Angstrom'')') edge
      write(*,'(''Cubic simulation cell volume      '',f20.8,
     x  '' Angstrom**3'')') volume
      write(*,'(''Simulation cell mass density      '',f20.8,
     x  '' gm/cm**3'')') rho    


c     cell edge 
      write(15,*)
      write(15,'(5x,2f15.8,'' xlo xhi '')') box(1,1,1),box(1,1,1)+edge
      write(15,'(5x,2f15.8,'' ylo yhi '')') box(2,1,1),box(2,1,1)+edge
      write(15,'(5x,2f15.8,'' zlo zhi '')') box(3,1,1),box(3,1,1)+edge


      write(15,*)
      write(15,'('' Masses  '')')
      write(15,*)
      ioffset=0
      iupper=ntypes
      if(ifsuper.eq.2) iupper=1 !for superposition mode 2, only need first system 
      do i=1,iupper 
        do j=1,natomtp(i)
c         find first example of this atom type in component molecule i
          l=0
          do k=1,nsites(i)
            if(l.eq.0.and.itype(k,i).eq.j) l=k 
          enddo
          if(l.eq.0) then
            write(*,'('' Warning:  no example of type '',i5,
     x        '' found in molecule '',i5)') j,i
          endif
c         is=index(atname(l,i),' ')-1
c??       it=itype(j,i)
c??       call lstchr(nametypes(it,i),is)
          call lstchr(nametypes( j,i),is)
          write(*,'(''Check:  nametypes('',i5,'','',i5,
     x      '')='',a)') j,i,nametypes(j,i)(1:is)
          write(15,'(i10,f15.8,
     x    ''    # Num '',4x,'' Ex: '',i4,'' '',a)') 
c    x    j+ioffset,fmass(j,i),l,atname(l,i)(1:is)
     x    j+ioffset,fmass(j,i),l,nametypes( j,i)(1:is)  
        enddo
        ioffset=ioffset+natomtp(i)
      enddo



      write(15,*)
      write(15,'('' Pair Coeffs'')')
      write(15,*)
      ioffset=0
      iupper=ntypes
      if(ifsuper.eq.2) iupper=1 !for superposition mode 2, only need first system 
      do i=1,iupper 
        do j=1,natomtp(i)
c         find first example of this atom type in component molecule i
          l=0
          do k=1,nsites(i)
            if(l.eq.0.and.itype(k,i).eq.j) l=k  
          enddo
          if(l.eq.0) then
            write(*,'('' Warning:  no example of type '',i5,
     x        '' found in molecule '',i5)') j,i
          endif
          is=index(atname(l,i),' ')-1
          write(15,'(i10,4f15.8,''   # '',a)') 
     x    j+ioffset,
     x    vdw(2,j,i),vdw(1,j,i), !epsilon,sigma
     x    vdw(2,j,i),vdw(1,j,i), !epsilon,sigma
     x    atname(l,i)(1:is)
        enddo
        ioffset=ioffset+natomtp(i)
      enddo




      write(15,*)
      write(15,'('' Bond Coeffs'')')
      write(15,*)
      ioffset=0
      iupper=ntypes
      if(ifsuper.eq.2) iupper=1 !for superposition mode 2, only need first system 
      do i=1,iupper
        do j=1,nbtp(i)
c         find first example of this bond type in component molecule i
          l=0
          do k=1,nbonds(i)
            if(l.eq.0.and.ijbond(3,k,i).eq.j) l=k  
          enddo
          if(l.eq.0) then
            write(*,'('' Warning:  no example of bond type '',i5,
     x        '' found in molecule '',i5)') j,i
          endif
          iat=ijbond(1,l,i)
          jat=ijbond(2,l,i)
          isi=index(atname(iat,i),' ')-1
          isj=index(atname(jat,i),' ')-1
          write(15,'(i10,2f15.8,
     x      ''    # '',4x,'' Ex: '',i4,'' '',a,'' - '',a)') 
     x      j+ioffset,
     x      bndpar(1,j,i),bndpar(2,j,i),l,
     x      atname(iat,i)(1:isi),atname(jat,i)(1:isj)
        enddo
        ioffset=ioffset+nbtp(i)
      enddo




      write(15,*)
      write(15,'('' Angle Coeffs'')')
      write(15,*)
      ioffset=0
      iupper=ntypes
      if(ifsuper.eq.2) iupper=1 !for superposition mode 2, only need first system 
      do i=1,iupper
        do j=1,natp(i)
c         find first example of this angle type in component molecule i
          l=0
          do k=1,nangle(i)
            if(l.eq.0.and.ijkangle(4,k,i).eq.j) l=k  
          enddo
          iat=ijkangle(1,l,i)
          jat=ijkangle(2,l,i)
          kat=ijkangle(3,l,i)
          isi=index(atname(iat,i),' ')-1
          isj=index(atname(jat,i),' ')-1
          isk=index(atname(kat,i),' ')-1
          write(15,'(i10,2f15.8,
     x      ''    # '',4x,'' Ex: '',i4,'' '',a,'' - '',a,'' - '',a)')
     x      j+ioffset,angpar(1,j,i),angpar(2,j,i),
     x      l,
     x      atname(iat,i)(1:isi),atname(jat,i)(1:isj),
     x      atname(kat,i)(1:isk)
        enddo
        ioffset=ioffset+natp(i)
      enddo



      write(15,*)
      write(15,'('' Dihedral Coeffs'')')
      write(15,*)
      ioffset=0
      iupper=ntypes
      if(ifsuper.eq.2) iupper=1 !for superposition mode 2, only need first system 
      do i=1,iupper
        do j=1,ndtp(i)
c         find first example of this dihedral type in component molecule i
          l=0
          do k=1,ndih(i)
            if(l.eq.0.and.ijkldih(5,k,i).eq.j) l=k  
          enddo
          iat=ijkldih(1,l,i)
          jat=ijkldih(2,l,i)
          kat=ijkldih(3,l,i)
          lat=ijkldih(4,l,i)
          isi=index(atname(iat,i),' ')-1
          isj=index(atname(jat,i),' ')-1
          isk=index(atname(kat,i),' ')-1
          isl=index(atname(lat,i),' ')-1
          write(15,'(i10,f10.5,i5,i5,f10.5,
     x      ''    # Num '',4x,'' Ex:  '',i4,
     x      '' '',a,'' - '',a,'' - '',a,'' - '',a)')
     x      j+ioffset,dihpar(1,j,i),int(dihpar(2,j,i)),
     x        int(dihpar(3,j,i)),dihpar(4,j,i),l,
     x      atname(iat,i)(1:isi),atname(jat,i)(1:isj),
     x      atname(kat,i)(1:isk),atname(lat,i)(1:isl)
        enddo
        ioffset=ioffset+ndtp(i)
      enddo







c
c     molecule placement within cell
c
      lmax=0
      do i=1,500
        if(lmax.eq.0.and.i**3.ge.nmol) lmax=i
      enddo

      if(ifsuper.eq.1.or.ifsuper.eq.2) lmax=1
c
c     set up and randomize a list of molecules
c
      l=0
      do i=1,ntypes
        do j=1,num(i)
          l=l+1
          lseq(l)=i
        enddo
      enddo
c     pick a random pair and swap them; do this a bunch of times
      iseed=7943
      do i=1,2*nmol
        i1=min(int(1+nmol*ran(iseed)),nmol)
        i2=min(int(1+nmol*ran(iseed)),nmol)
        isave=lseq(i1)
        lseq(i1)=lseq(i2)
        lseq(i2)=isave
      enddo      
c     write(*,'('' i='',i3,'' moltype='',i3)') (i,lseq(i),i=1,nmol)

c
c     loop over the molecule types placing all molecules of type 1 first, etc
c
      write(15,*)
      write(15,'('' Atoms'')')
      write(15,*)

      if(ifsuper.eq.0) then
c       normal operation - for placing sets of molecules to make a mixture
        ioffset=0
        natom=0
        imoffset=0
        do i=1,ntypes
          do j=1,nmol
            if(lseq(j).eq.i) then
c             the value of j determines a lattice site (ix,iy,iz) on an lmax*lmax*lmax grid
              iz=1+((j-1)/(lmax**2))
              iy=1+((j-1)-(iz-1)*lmax**2)/lmax
              ix=    j   -(iz-1)*lmax**2 - (iy-1)*lmax
c             place molecule of type i at location given by ix,iy,iz in cell
              trx=(ix-1)*(edge/lmax)-coord(1,1,i)  !shift first atom of this molecule
              try=(iy-1)*(edge/lmax)-coord(2,1,i)  !...to the lattice site
              trz=(iz-1)*(edge/lmax)-coord(3,1,i)
c       record is atomid, moleculeid, atomtype, charge, x,y,z, comment like "# C_1836_2_187"
              lstmol=0
              do k=1,nsites(i)
                lstmol=max(molid(k,i),lstmol)
                natom=natom+1
                isi=index(atname(k,i),' ')-1
                write(15,'(i10,i10,i5,f15.10,3f15.8,''    # '',a)')
     x             natom,molid(k,i)+imoffset,itype(k,i)+ioffset,
     x             charge(k,i),
     x             coord(1,k,i)+trx,coord(2,k,i)+try,coord(3,k,i)+trz,
     x             atname(k,i)(1:isi)
              enddo
              imoffset=imoffset+lstmol
            endif
          enddo
          ioffset=ioffset+natomtp(i)
        enddo  !end of loop over types

      elseif(ifsuper.eq.1) then
c       super position operation - for superposing molecules of type 2 
c       into a preexisting system of type 1 (designed to insert a cargo molecule near star)
        lstmol=0
        do k=1,nsites(1)   !place all sites in the first system
          natom=k      
          lstmol=max(lstmol,molid(k,1))
          isi=index(atname(k,1),' ')-1
c         record is atomid, moleculeid, atomtype, charge, x,y,z, comment like "# C_1836_2_187"
          write(15,'(i10,i10,i5,f10.5,3f15.8,''    # '',a)')
     x       natom,molid(k,1),itype(k,1),charge(k,1),
     x       coord(1,k,1),coord(2,k,1),coord(3,k,1),
     x       atname(k,1)(1:isi)
        enddo
c       now place the second component according to placement mode
c         placement mode possibilities:  uniform in a spherical shell about a 
c         given site (e.g., some atom id in system 1), 
c         or within a shell of radius rshell, and thickness of rdelta
        pi=4.d0*datan(1.d0)
        rshell=5.0d0   !inner radius of shell; these should be input parameters
        rdelta=0.5d0   !shell thickness; these should be input parameters
        itag=1         !carbon in adamantane in a star polymer
c       select random phi (0 to 2*pi); random theta (0 to pi); uniform on sphere surface
        iseed=94587
        ioffset=natomtp(1)
        imoffset=lstmol
        lstmol=0
        do j=1,num(2)  
          ranphi=2.d0*pi*ran(iseed)
          ranthe=dacos(1.d0-2.d0*ran(iseed))
          ranr= rshell**3   !compute random radial distance in the shell
     x      +  ( (rshell+rdelta)**3 - rshell**3 ) * ran(iseed)
          ranr=ranr**(1.d0/3.d0)
c         if(j.eq.1) then !north pole
c           ranthe=0.d0
c           ranphi=0.d0
c         elseif(j.eq.2) then !south pole
c           ranthe=pi
c           ranphi=0.d0
c         elseif(j.gt.2) then
c           if(j-2.le.(num(2)-2)/2) ranthe=(1.d0/3.d0)*pi
c           if(j-2.gt.(num(2)-2)/2) ranthe=(2.d0/3.d0)*pi
c           ranphi=(j-2)*(2.d0*pi)/( (num(2)-2)/2 )
c         endif
          write(*,'('' Placing cargo molecule '',i2,'' at R='',f10.5,
     x      '' Theta='',f10.5,'' Phi='',f10.5)')
     x      j,ranr,(180.d0/pi)*ranthe,(180.d0/pi)*ranphi
          ranx=ranr*dsin(ranthe)*dcos(ranphi)
          rany=ranr*dsin(ranthe)*dsin(ranphi)
          ranz=ranr*dcos(ranthe)
          trx=coord(1,itag,1) + ranx - coord(1,1,2)
          try=coord(2,itag,1) + rany - coord(2,1,2)
          trz=coord(3,itag,1) + ranz - coord(3,1,2)
          do k=1,nsites(2)
            natom=natom+1
            lstmol=max(lstmol,molid(k,2))
            isi=index(atname(k,2),' ')-1
c           record is atomid, moleculeid, atomtype, charge, x,y,z, comment like "# C_1836_2_187"
            write(15,'(i10,i10,i5,f10.5,3f15.8,''    # '',a)')
     x       natom,molid(k,2)+imoffset,itype(k,2)+ioffset,charge(k,2),
     x       coord(1,k,2)+trx,coord(2,k,2)+try,coord(3,k,2)+trz,
     x       atname(k,2)(1:isi)
c           write(* ,'(i10,i10,i5,f10.5,3f15.8,''    # '',a)')
c    x       natom,molid(k,2)+imoffset,itype(k,2)+ioffset,charge(k,2),
c    x       coord(1,k,2)+trx,coord(2,k,2)+try,coord(3,k,2)+trz,
c    x       atname(k,2)(1:isi)
          enddo !end of loop over atoms of type 2 molecule
          imoffset=imoffset+lstmol
        enddo  !end of loop over molecules of type 2
        
      elseif(ifsuper.eq.2) then
c       super position operation - for superposing molecules of type 2 
c       into a preexisting system of type 1 (designed to insert into "voids")
c       first place each molecules of system 1; must scale location of cog to new box size
        edgex=box(1,2,1)-box(1,1,1)   !original system 1 box dimensions
        edgey=box(2,2,1)-box(2,1,1)   !original system 1 box dimensions
        edgez=box(3,2,1)-box(3,1,1)   !original system 1 box dimensions
        ifmol=molid(1,1)         !first molid in system 1 
        ilmol=molid(nsites(1),1) !last molid in system 1
        ifatm=1
        do imol=ifmol,ilmol
          do i=ifatm,nsites(1)
            if(molid(i,1).ne.imol) then
              ilatm=i-1
              go to 37
            endif
          enddo
          ilatm=nsites(1)
37        continue     !this molecule (imol) goes from atom ifat to ilat     
c         write(*,'(''Molecule '',i5,'' sites '',i5,'' to '',i5,
c    x      '' number of sites '',i5)')
c    x      imol,ifatm,ilatm,ilatm-ifatm+1
c         establish the cog using a consistent image of all the sites of this molecule
c         this imaging uses the original cell dimensions  
          cogx=0.d0             
          cogy=0.d0             
          cogz=0.d0             
          do i=ifatm,ilatm
            dx=coord(1,i,1)-coord(1,ifatm,1) !image relative to first atom of molecule
            dy=coord(2,i,1)-coord(2,ifatm,1) !image relative to first atom of molecule
            dz=coord(3,i,1)-coord(3,ifatm,1) !image relative to first atom of molecule
            if(dx.le.-0.5d0*edgex) dx=dx+edgex
            if(dx.gt. 0.5d0*edgex) dx=dx-edgex
            if(dy.le.-0.5d0*edgey) dy=dy+edgey
            if(dy.gt. 0.5d0*edgey) dy=dy-edgey
            if(dz.le.-0.5d0*edgez) dz=dz+edgez
            if(dz.gt. 0.5d0*edgez) dz=dz-edgez
            cogx=cogx+dx/(ilatm-ifatm+1)
            cogy=cogy+dy/(ilatm-ifatm+1)
            cogz=cogz+dz/(ilatm-ifatm+1)
          enddo
          cogx=(scale-1.d0)*(cogx+coord(1,ifatm,1)) !this is the shift to apply  
          cogy=(scale-1.d0)*(cogy+coord(2,ifatm,1)) !this is the shift to apply  
          cogz=(scale-1.d0)*(cogz+coord(3,ifatm,1)) !this is the shift to apply  
          do i=ifatm,ilatm
            dx=coord(1,i,1)-coord(1,ifatm,1) !image relative to first atom of molecule
            dy=coord(2,i,1)-coord(2,ifatm,1) !image relative to first atom of molecule
            dz=coord(3,i,1)-coord(3,ifatm,1) !image relative to first atom of molecule
            if(dx.le.-0.5d0*edgex) dx=dx+edgex
            if(dx.gt. 0.5d0*edgex) dx=dx-edgex
            if(dy.le.-0.5d0*edgey) dy=dy+edgey
            if(dy.gt. 0.5d0*edgey) dy=dy-edgey
            if(dz.le.-0.5d0*edgez) dz=dz+edgez
            if(dz.gt. 0.5d0*edgez) dz=dz-edgez
c
c           following is the bug....I change coord(ifatm) on the first pass 
c           through the loop, but assume it is the old value on subsequent
c           passes.....need to fix this; this bug had little effect when the
c           number of added molecules was small (<100), but is more noticeable
c           for larger numbers (~1000) or if the volume change per molecule is
c           greater
c
            coord(1,i,1)=dx+coord(1,ifatm,1)+cogx  !apply the shit
            coord(2,i,1)=dy+coord(2,ifatm,1)+cogy  !apply the shit
            coord(3,i,1)=dz+coord(3,ifatm,1)+cogz  !apply the shit
            if(coord(1,i,1).gt.box(1,1,1)+scale*edgex) 
     x         coord(1,i,1)=coord(1,i,1)-scale*edgex
            if(coord(1,i,1).lt.box(1,1,1)) 
     x         coord(1,i,1)=coord(1,i,1)+scale*edgex
            if(coord(2,i,1).gt.box(2,1,1)+scale*edgey) 
     x         coord(2,i,1)=coord(2,i,1)-scale*edgey
            if(coord(2,i,1).lt.box(2,1,1)) 
     x         coord(2,i,1)=coord(2,i,1)+scale*edgey
            if(coord(3,i,1).gt.box(3,1,1)+scale*edgez) 
     x         coord(3,i,1)=coord(3,i,1)-scale*edgez
            if(coord(3,i,1).lt.box(3,1,1)) 
     x         coord(3,i,1)=coord(3,i,1)+scale*edgez
c         record is atomid, moleculeid, atomtype, charge, x,y,z, comment like "# C_1836_2_187"
            isi=index(atname(i,1),' ')-1
            write(15,'(i10,i10,i5,f10.5,3f15.8,''    # '',a)')
     x         i,molid(i,1),itype(i,1),charge(i,1),
     x         coord(1,i,1),coord(2,i,1),coord(3,i,1),
     x         atname(i,1)(1:isi)
          enddo
          ifatm=ilatm+1
        enddo
c       now place the second component according to placement mode
c       these are assumed to be num(2) single molecules each with nsites(2) sites
c       last molecule added was ilmol, last atom added was ilatm
c       compute site coordinates relative to the COG
        do i=1,nsites(2) 
          temp1(1,i)=coord(1,i,2)
          temp1(2,i)=coord(2,i,2)
          temp1(3,i)=coord(3,i,2)
        enddo
        cogx=0.d0             
        cogy=0.d0             
        cogz=0.d0             
        do i=1,nsites(2) 
          cogx=cogx+temp1(1,i)  !assume all sites are in a common image
          cogy=cogy+temp1(2,i)
          cogz=cogz+temp1(3,i)
        enddo
        do i=1,nsites(2) 
          temp1(1,i)=temp1(1,i)-cogx/nsites(2)
          temp1(2,i)=temp1(2,i)-cogy/nsites(2)
          temp1(3,i)=temp1(3,i)-cogz/nsites(2)
        enddo

c       write(*,'(''Molecule 2:  Site '',i3,'' R='',3f10.5)')
c    x     (i,temp1(1,i),temp1(2,i),temp1(3,i),i=1,nsites(2))
c       do i=1,nsites(2)
c         do j=1,nsites(2)
c           r2=(temp1(1,i)-temp1(1,j))**2
c    x        +(temp1(2,i)-temp1(2,j))**2
c    x        +(temp1(3,i)-temp1(3,j))**2
c           write(*,'('' site '',i3,'' to site '',i3,'' dist '',f10.4)')
c    x        i,j,dsqrt(r2)
c         enddo
c       enddo


        iseed=94587
        do imol=1,num(2)
c         this is molecule ilmol+imol in the grand list       

c         find placement site; pick randomly, or randomly from a list of "available" sites
          nlocate=0
40        trx=box(1,1,1)+(scale*edgex)*ran(iseed)
          try=box(2,1,1)+(scale*edgey)*ran(iseed)
          trz=box(3,1,1)+(scale*edgez)*ran(iseed)
          nlocate=nlocate+1

c         find orientation - note that this is not a full rotation, but probably ok
c         random rotation - we might drive this from a list of 20 orientations
c         rotate and translate
          norient=0
45        ranphi=2.d0*pi*ran(iseed)
          ranthe=dacos(1.d0-2.d0*ran(iseed))
c         ranphi=0.d0
c         ranthe=0.d0
          norient=norient+1
          sp=dsin(ranphi)  !full rotation would require a chi, too
          cp=dcos(ranphi)
          st=dsin(ranthe)
          ct=dcos(ranthe)
          do i=1,nsites(2)
            temp2(1,i)=trx + cp*   temp1(1,i) - sp*   temp1(2,i)
            temp2(2,i)=try + ct*sp*temp1(1,i) + ct*cp*temp1(2,i)
     x                      -st*   temp1(3,i)
            temp2(3,i)=trz + st*sp*temp1(1,i) + st*cp*temp1(2,i) 
     x                     + ct*   temp1(3,i)
          enddo

c         bump check against old material
          ibump=0
          do i=1,nsites(1) 
            do j=1,nsites(2)
              dx=coord(1,i,1)-temp2(1,j)
              if(dx.gt.+0.5d0*(scale*edgex)) dx=dx-(scale*edgex)
              if(dx.le.-0.5d0*(scale*edgex)) dx=dx+(scale*edgex)
              dy=coord(2,i,1)-temp2(2,j)
              if(dy.gt.+0.5d0*(scale*edgey)) dy=dy-(scale*edgey)
              if(dy.le.-0.5d0*(scale*edgey)) dy=dy+(scale*edgey)
              dz=coord(3,i,1)-temp2(3,j)
              if(dz.gt.+0.5d0*(scale*edgez)) dz=dz-(scale*edgez)
              if(dz.le.-0.5d0*(scale*edgez)) dz=dz+(scale*edgez)
              r2=dx**2+dy**2+dz**2
              if(r2.lt.8.d0) then
c               write(*,'(''Molecule '',i5,''   L'',i3,'' O'',i3,
c    x            '' bump check fails from site '',
c    x            i6,'' of molecule '',i5,'' first type'',
c    x            '' distance '',f10.4)')  
c    x            imol,nlocate,norient,i,molid(i,1),dsqrt(r2)
                ibump=i
                go to 50
              endif
            enddo
          enddo

c         bump check against new material
          do i=1,(imol-1)*nsites(2) 
            do j=1,nsites(2)
              dx=coord(1,i,2)-temp2(1,j)
              if(dx.gt.+0.5d0*(scale*edgex)) dx=dx-(scale*edgex)
              if(dx.le.-0.5d0*(scale*edgex)) dx=dx+(scale*edgex)
              dy=coord(2,i,2)-temp2(2,j)
              if(dy.gt.+0.5d0*(scale*edgey)) dy=dy-(scale*edgey)
              if(dy.le.-0.5d0*(scale*edgey)) dy=dy+(scale*edgey)
              dz=coord(3,i,2)-temp2(3,j)
              if(dz.gt.+0.5d0*(scale*edgez)) dz=dz-(scale*edgez)
              if(dz.le.-0.5d0*(scale*edgez)) dz=dz+(scale*edgez)
              r2=dx**2+dy**2+dz**2
              if(r2.lt.8.d0) then
                ibump=i  !failed 
c               write(*,'(''Molecule '',i5,''   L'',i3,'' O'',i3,
c    x            '' bump check fails from site '',
c    x            i6,'' of molecule '',i5,'' second type'',  
c    x            '' distance '',f10.4)')  
c    x            imol,nlocate,norient,i-nsites(2),i/nsites(2)-1,
c    x            dsqrt(r2)
                ibump=i
                go to 50
              endif
            enddo
          enddo

50        continue
c         ibump=0
          if(ibump.eq.0) then
            do j=1,nsites(2)
              coord(1,(imol-1)*nsites(2)+j,2)=temp2(1,j)
              coord(2,(imol-1)*nsites(2)+j,2)=temp2(2,j)
              coord(3,(imol-1)*nsites(2)+j,2)=temp2(3,j)
            enddo
            write(*,'(''Molecule '',i5,'' placement successful after '',
     x        i3,'' positions '',i4,'' orientations '')')
     x      imol,nlocate,norient+10*(nlocate-1)
c           write(*,'(''Molecule 2:  Site '',i3,'' R='',3f10.5)')
c    x       (i,temp2(1,i),temp2(2,i),temp2(3,i),i=1,nsites(2))

c         do ix=1,nsites(2)
c           do jx=1,nsites(2)
c             r2=(temp2(1,ix)-temp2(1,jx))**2
c    x          +(temp2(2,ix)-temp2(2,jx))**2
c    x          +(temp2(3,ix)-temp2(3,jx))**2
c           write(*,'('' site '',i3,'' to site '',i3,'' dist '',f10.4)')
c    x          ix,jx,dsqrt(r2)
c           enddo
c         enddo

          else
c           ok?  if not, find new orientation; if no more orientation, find new place
            if(norient.lt.10) go to 45
            if(nlocate.lt.50) go to 40
c           attempts exceeded?  stop
            stop 'no place found'
          endif

        enddo   !end of loop over molecules of the second type (imol)

c       record is atomid, moleculeid, atomtype, charge, x,y,z, comment like "# C_1836_2_187"
        do i=1,num(2)
          do j=1,nsites(2)
            isi=index(atname(j,2),' ')-1
            write(15,'(i10,i10,i5,f10.5,3f15.8,''    # '',a)')
     x         num(1)*nsites(1)+(i-1)*nsites(2)+j,
     x         molid(nsites(1),1)+i,
     x         maptp(itype(j,2)),charge(j,2),
     x         coord(1,(i-1)*nsites(2)+j,2),
     x         coord(2,(i-1)*nsites(2)+j,2),
     x         coord(3,(i-1)*nsites(2)+j,2),
     x         atname(j,2)(1:isi)
          enddo
        enddo


      endif !end of if-block for normal or superpositioning modes









      write(15,*)
      write(15,'('' Velocities'')')
      write(15,*)
      iatom=0
      do i=1,ntypes
        do j=1,num(i) 
          do k=1,nsites(i)
            iatom=iatom+1
            write(15,'(i10,5x,3f10.5)') iatom,0.,0.,0.
          enddo
        enddo
      enddo




      write(15,*)
      write(15,'('' Bonds'')')
      write(15,*)
      ibond=0
      ioffset=0
      joffset=0
      do i=1,ntypes
        do j=1,num(i)
          do k=1,nbonds(i)
            ibond=ibond+1
            if(ifsuper.ne.2) then
              write(15,'(i10,3i10)') ibond,
     x        ijbond(3,k,i)+joffset,  !bond type with bond offset
     x        ijbond(1,k,i)+ioffset,  !atom index with atom offset
     x        ijbond(2,k,i)+ioffset   !atom index with atom offset
            elseif(i.eq.1) then
              write(15,'(i10,3i10)') ibond,
     x        ijbond(3,k,1)+joffset,  !bond type with bond offset
     x        ijbond(1,k,1)+ioffset,  !atom index with atom offset
     x        ijbond(2,k,1)+ioffset   !atom index with atom offset
            elseif(i.eq.2) then
              write(15,'(i10,3i10)') ibond,
     x        mapbn(ijbond(3,k,2)),   !bond type with bond offset
     x        ijbond(1,k,2)+ioffset,  !atom index with atom offset
     x        ijbond(2,k,2)+ioffset   !atom index with atom offset
            endif
          enddo
          ioffset=ioffset+nsites(i)
        enddo
        joffset=joffset+nbtp(i)
      enddo




      write(15,*)
      write(15,'('' Angles'')')
      write(15,*)
      iangle=0
      ioffset=0
      joffset=0
      do i=1,ntypes
        do j=1,num(i)
          do k=1,nangle(i)
            iangle=iangle+1
            if(ifsuper.ne.2) then
              write(15,'(i10,4i10)') iangle,
     x          ijkangle(4,k,i)+joffset,ijkangle(1,k,i)+ioffset,
     x          ijkangle(2,k,i)+ioffset,ijkangle(3,k,i)+ioffset
            elseif(i.eq.1) then
              write(15,'(i10,4i10)') iangle,
     x          ijkangle(4,k,i)+joffset,ijkangle(1,k,i)+ioffset,
     x          ijkangle(2,k,i)+ioffset,ijkangle(3,k,i)+ioffset
            elseif(i.eq.2) then
              write(15,'(i10,4i10)') iangle,
     x          mapan(ijkangle(4,k,i)),ijkangle(1,k,i)+ioffset,
     x          ijkangle(2,k,i)+ioffset,ijkangle(3,k,i)+ioffset
            endif
          enddo
          ioffset=ioffset+nsites(i)
        enddo
        joffset=joffset+natp(i)
      enddo



      write(15,*)
      write(15,'('' Dihedrals'')')
      write(15,*)
      idih=0
      ioffset=0
      joffset=0
      do i=1,ntypes
        do j=1,num(i)
          do k=1,ndih(i)
            idih=idih+1
            if(ifsuper.ne.2) then
              write(15,'(i10,5i10)') idih,
     x          ijkldih(5,k,i)+joffset,
     x          ijkldih(1,k,i)+ioffset,ijkldih(2,k,i)+ioffset,
     x          ijkldih(3,k,i)+ioffset,ijkldih(4,k,i)+ioffset
            elseif(i.eq.1) then
              write(15,'(i10,5i10)') idih,
     x          ijkldih(5,k,i)+joffset,
     x          ijkldih(1,k,i)+ioffset,ijkldih(2,k,i)+ioffset,
     x          ijkldih(3,k,i)+ioffset,ijkldih(4,k,i)+ioffset
            elseif(i.eq.2) then
              write(15,'(i10,5i10)') idih,
     x          mapdi(ijkldih(5,k,i)),
     x          ijkldih(1,k,i)+ioffset,ijkldih(2,k,i)+ioffset,
     x          ijkldih(3,k,i)+ioffset,ijkldih(4,k,i)+ioffset
            endif
          enddo
          ioffset=ioffset+nsites(i)
        enddo
        joffset=joffset+ndtp(i)
      enddo


      write(15,*)   ! need a blank line after the last dihedral (don't ask why)
      close(15)





c
c     implement geometric combining rules information for lammps
c
 
      is=index(fname,'.lmp')-1
      open(16,file=fname(1:is)//'.paircoeff')
      write(*,'('' Geometric combining rules for all type pairs '')')

      ioffset=0
      do it=1,ntypes
        joffset=ioffset
        do jt=it,ntypes  !lammps appears to want upper triangle of type-type matrix
          do i=1,natomtp(it)
            vd2i=vdw(2,i,it) !epsilon
            vd1i=vdw(1,i,it) !sigma  
            jstart=1
            if(it.eq.jt) jstart=i
            do j=jstart,natomtp(jt)
              vd2j=vdw(2,j,jt) !epsilon
              vd1j=vdw(1,j,jt) !sigma  
              write(16,'(''pair_coeff     '',2i5,2f15.8)')
     x          i+ioffset,j+joffset,
     x          dsqrt(vd2i*vd2j),
     x          dsqrt(vd1i*vd1j)
            enddo
          enddo
          joffset=joffset+natomtp(jt)
        enddo
        ioffset=ioffset+natomtp(it)
      enddo

      close(16)









c
c     write a Jed-style simplified pdb file for vmd to use     
c     fields are "ATOM"
c                atom number, i7
c                atom name, a3, e.g., H_2
c                residue (3 char)
c                mol num
c                xyz
c                charge
c
c
c     note:  residue names are no in the lmp files, but ffassign has
c     put this information in the pdb files
c     get residue names for each site from a corresponding pdb file
c
c     do i=1,ntypes
c       is1=index(mname(i),'.lmp')-1
c       open(15,file=nname(i)(1:is1)//'.pdb')
c       do k=1,nsites(i)
c         read(15,'(a120)') line
c         read(line,'(17x,a3)') resname(k,i)
c       enddo
c       close(15)
c     enddo
  
c

c
      is=index(fname,'.lmp')-1
      open(15,file=fname(1:is)//'.pdb')

      if(ifsuper.eq.0) then
c       normal operation - for placing sets of molecules to make a mixture
        ioffset=0
        imoffset=0  !note that following code builds the coordinates 
        natom=0     !...exactly as was done for the lmp file generated above
        do i=1,ntypes
          do j=1,nmol
            if(lseq(j).eq.i) then
c           place molecule j in a site determined by its index j
              iz=1+((j-1)/(lmax**2))
              iy=1+((j-1)-(iz-1)*lmax**2)/lmax
              ix=    j   -(iz-1)*lmax**2 - (iy-1)*lmax
              trx=(ix-1)*(edge/lmax)-coord(1,1,i)  !shift first atom of this molecule
              try=(iy-1)*(edge/lmax)-coord(2,1,i)  !...to the lattice site
              trz=(iz-1)*(edge/lmax)-coord(3,1,i)
              lstmol=0
              do k=1,nsites(i)
                lstmol=max(molid(k,i),lstmol)
                natom=natom+1
                it=itype(k,i)
                is1=index(nametypes(it,i),'_')              !residue in substring 1:is1-1
                is2=index(nametypes(it,i)(is1+1:),'_')+is1  !element in substring is1+1:is2-1
                write(15,'(''ATOM'',i7,2x,a3,1x,a3,i6,4x,3f8.3,
     x            1x,f5.2,1x,f5.2)')
     x            natom,                          !atom index
     x            nametypes(it,i)(is1+1:is2-1),   !element
     x            nametypes(it,i)(1:is1-1),       !residue
     x            molid(k,i)+imoffset,            !molcule index
     x            coord(1,k,i)+trx,  
     x            coord(2,k,i)+try,  
     x            coord(3,k,i)+trz,  
     x            charge(k,i),0.0d0
              enddo
              imoffset=imoffset+lstmol
            endif
          enddo
          ioffset=ioffset+natomtp(i)
        enddo

      elseif(ifsuper.eq.1) then
c       super position operation - for superposing molecules of type 2 into a preexisting system of type 1
        lstmol=0
        do k=1,nsites(1)
          natom=k      
          lstmol=max(lstmol,molid(k,1))
          it=itype(k,1)
          is1=index(nametypes(it,1),'_')              !residue in substring 1:is1-1
          is2=index(nametypes(it,1)(is1+1:),'_')+is1  !element in substring is1+1:is2-1
          write(15,'(''ATOM'',i7,2x,a3,1x,a3,i6,4x,3f8.3,
     x      1x,f5.2,1x,f5.2)')
     x      natom,                          !atom index
     x      nametypes(it,1)(is1+1:is2-1),   !element
     x      nametypes(it,1)(1:is1-1),       !residue
     x      molid(k,1),                     !molcule index
     x      coord(1,k,1),  
     x      coord(2,k,1),  
     x      coord(3,k,1),  
     x      charge(k,1),0.0d0
        enddo



c
c       now place the second component according to placement mode
c
c       placement mode:  uniform in a spherical shell about a given site (atom id in system 1), 
c       with a shell radius of rshell, and a shell thickness of rdelta all defined above
        iseed=94587
        imoffset=lstmol
        lstmol=0
        natom=nsites(1) 
        do j=1,num(2)  
          pi=4.d0*datan(1.d0)
          ranphi=2.d0*pi*ran(iseed)
          ranthe=dacos(1.d0-2.d0*ran(iseed))
c         select radius random in the shell
          ranr=rshell  !just for nnow us the inner shell radius
          ranr= rshell**3   
     x       +  ( (rshell+rdelta)**3 - rshell**3 ) * ran(iseed)
          ranr=ranr**(1.d0/3.d0)

c         if(j.eq.1) then !north pole
c           ranthe=0.d0
c           ranphi=0.d0
c         elseif(j.eq.2) then !south pole
c           ranthe=pi
c           ranphi=0.d0
c         elseif(j.gt.2) then
c           if(j-2.le.(num(2)-2)/2) ranthe=(1.d0/3.d0)*pi
c           if(j-2.gt.(num(2)-2)/2) ranthe=(2.d0/3.d0)*pi
c           ranphi=(j-2)*(2.d0*pi)/( (num(2)-2)/2 )
c         endif

          ranx=ranr*dsin(ranthe)*dcos(ranphi)
          rany=ranr*dsin(ranthe)*dsin(ranphi)
          ranz=ranr*dcos(ranthe)
          trx=coord(1,itag,1) + ranx - coord(1,1,2)
          try=coord(2,itag,1) + rany - coord(2,1,2)
          trz=coord(3,itag,1) + ranz - coord(3,1,2)
          do k=1,nsites(2)
            lstmol=max(lstmol,molid(k,2))
            natom=natom+1
            it=itype(k,2)
            is1=index(nametypes(it,2),'_')              !residue in substring 1:is1-1
            is2=index(nametypes(it,2)(is1+1:),'_')+is1  !element in substring is1+1:is2-1
            write(15,'(''ATOM'',i7,2x,a3,1x,a3,i6,4x,3f8.3,
     x        1x,f5.2,1x,f5.2)')
     x        natom,                          !atom index
     x        nametypes(it,2)(is1+1:is2-1),   !element
     x        nametypes(it,2)(1:is1-1),       !residue
     x        molid(k,1)+imoffset,            !molcule index
     x        coord(1,k,2)+trx,  
     x        coord(2,k,2)+try,  
     x        coord(3,k,2)+trz,  
     x        charge(k,1),0.0d0
          enddo !end of loop over atoms of type 2 molecule
          imoffset=imoffset+lstmol
        enddo  !end of loop over molecules of type 2



      elseif(ifsuper.eq.2) then
c       super position operation - for superposing molecules of type 2 into a preexisting system of type 1
        lstmol=0
        do k=1,nsites(1)
          natom=k      
          lstmol=max(lstmol,molid(k,1))
          it=itype(k,1)
          is1=index(nametypes(it,1),'_')              !residue in substring 1:is1-1
          is2=index(nametypes(it,1)(is1+1:),'_')+is1  !element in substring is1+1:is2-1
          write(15,'(''ATOM'',i7,2x,a3,1x,a3,i6,4x,3f8.3,
     x      1x,f5.2,1x,f5.2)')
     x      natom,                          !atom index
     x      nametypes(it,1)(is1+1:is2-1),   !element
     x      nametypes(it,1)(1:is1-1),       !residue
     x      molid(k,1),                     !molcule index
     x      coord(1,k,1),  
     x      coord(2,k,1),  
     x      coord(3,k,1),  
     x      charge(k,1),0.0d0
        enddo

c       record is atomid, moleculeid, atomtype, charge, x,y,z, comment like "# C_1836_2_187"
        do i=1,num(2)
          do j=1,nsites(2)
            natom=num(1)*nsites(1)+(i-1)*nsites(2)+j
            it=maptp(itype(j,2))
            is1=index(nametypes(it,1),'_')              !residue in substring 1:is1-1
            is2=index(nametypes(it,1)(is1+1:),'_')+is1  !element in substring is1+1:is2-1
            write(15,'(''ATOM'',i7,2x,a3,1x,a3,i6,4x,3f8.3,
     x        1x,f5.2,1x,f5.2)')
     x        natom,                          !atom index
     x        nametypes(it,1)(is1+1:is2-1),   !element
     x        nametypes(it,1)(1:is1-1),       !residue
     x        molid(nsites(1),1)+i,           !molcule index
     x        coord(1,(i-1)*nsites(2)+j,2),
     x        coord(2,(i-1)*nsites(2)+j,2),
     x        coord(3,(i-1)*nsites(2)+j,2),
     x        charge(j,2),0.d0
          enddo
        enddo





        
      endif !end of if-block for normal or superpositioning modes



      write(15,'(i10,3i10)') nbondt
      ioffset=0
      do i=1,ntypes
        do j=1,num(i)
          do k=1,nbonds(i)
            if(ifsuper.ne.2) then
              write(15,'(''CONECT'',2i7)') 
     x          ijbond(1,k,i)+ioffset,ijbond(2,k,i)+ioffset
            elseif(i.eq.1) then
              write(15,'(''CONECT'',2i7)') 
     x          ijbond(1,k,i)+ioffset,ijbond(2,k,i)+ioffset
            elseif(i.eq.2) then
              write(15,'(''CONECT'',2i7)') 
     x          mapbn(ijbond(1,k,i)),ijbond(2,k,i)+ioffset
            endif
          enddo
          ioffset=ioffset+nsites(i)
        enddo
      enddo


      close(15)





c     dimension ijres(5,1000)  !array to hold residue connectivity; construct from bond list
c
c
c     analyze the bond list data to find chains
c     these are used to provide information for the psf file for star polymers (mostly)
c
c     maxres=0  !find the range for the residue index; check if ijres is big enough
c     minres=1000
c     do i=1,nsites
c       parse the atom names to find the residue index for each atom
c       call parser(atname(i),string,lelfield,i1,i2,i3)
c       maxres=max(maxres,i3)
c       minres=min(minres,i3)
c     enddo
c     write(*,'('' Range of residue indices is '',i3,'' to '',i3)')
c    x  minres,maxres
c     if(maxres-minres+1.gt.1000) then
c       write(*,'('' Increase size of ijres to at least '',i5)')
c    x    maxres-minres+1
c       stop 'increase size of ijres'
c     endif

c     do i=1,1000
c       do j=1,5
c         ijres(j,i)=0
c       enddo
c     enddo
c     scan through the bond list and find bonds that connect different residues
c     do i=1,nbonds
c       iat=ijbond(1,i)
c       call parser(atname(iat),string,lelfield,i1,i2,i3)
c       jat=ijbond(2,i)
c       call parser(atname(jat),string,lelfield,i1,i2,j3)
c       if(i3.ne.j3) then
c         two residues are connected; see if this connection has already been seen
c         look for j3 in the list of i3
c         ifound=0
c         do j=1,ijres(1,i3-minres+1)
c           if(ijres(j+1,i3-minres+1).eq.j3) ifound=1
c         enddo
c         if(ifound.eq.0) then
c           inext=ijres(1,i3-minres+1)+1
c           ijres(1,i3-minres+1)=inext                      
c           if(inext+1.le.5) ijres(inext+1,i3-minres+1)=j3           
c         endif
c         look for i3 in the list of j3
c         ifound=0
c         do j=1,ijres(1,j3-minres+1)
c           if(ijres(j+1,j3-minres+1).eq.i3) ifound=1
c         enddo
c         if(ifound.eq.0) then
c           inext=ijres(1,j3-minres+1)+1
c           ijres(1,j3-minres+1)=inext                      
c           if(inext+1.le.5) ijres(inext+1,j3-minres+1)=i3           
c         endif
c       endif
c     enddo

c     do i=minres,maxres
c       write(*,'('' Residue '',i3,'' connected to '',10i5)')
c    x    i,
c    x  (ijres(k+1,i-minres+1),k=1,min(4,(ijres(1,i-minres+1))))
c     enddo

c     most residues will be connected to two others in the same chain
c     residues with only one connection are chain termini and
c     residues with more than two are nodes
c     chains will go from a terminus to a node
c
c     store a chain id in the last column of ijres
c     do i=minres,maxres
c       ijres(5,i-minres+1)=-1   !initialize to "unassigned" chain id
c     enddo

c     nchain=0
c     find next nonassigned terminus
c90   continue
c     ifnew=0
c     do i=minres,maxres
c       if(ijres(5,i-minres+1).eq.-1.and.ijres(1,i-minres+1).eq.1) then
c         ifnew=1
c         nchain=nchain+1
c         ijres(5,i-minres+1)=nchain  !assign terminus the next chain id
c         now assign this chain id from this residue to the next nodes
c         icurrent=i   !icurrent is the last residue assigned a chain id
c         inext=ijres(2,icurrent-minres+1)  !inext is a candidate; if not a node
c00       continue
c         if(ijres(1,inext-minres+1).eq.2) then
c           normal chain extension condition
c           ijres(5,inext-minres+1)=nchain
c           inew=ijres(2,inext-minres+1)               
c           if(inew.eq.icurrent) inew=ijres(3,inext-minres+1)       
c           icurrent=inext
c           inext=inew
c           go to 400
c         elseif(ijres(1,inext-minres+1).gt.2) then
c           node condition, terminating the chain
c           go to 390
c         elseif(ijres(1,inext-minres+1).lt.2) then
c           error condition for star polymers; ok for linear strands whiere could be 1
c           but should never be zero
c           write(*,'('' Error condition; new code needed to'',
c    x        '' address non-star topologies'',
c    x        '' (dendrimers or linear chains).'')')
c           write(*,'('' Error condition; unexpected termination of'',
c    x        '' chain.'')')
c           stop 'Error condition in chain identification'
c         endif
c       endif
c     enddo
c     if(ifnew.ne.0) go to 390

c     write(*,'('' Number of chains found = '',i3)') nchain
c     do i=minres,maxres
c       if(ijres(1,i-minres+1).gt.2) then
c         write(*,'('' Node:  residue number '',i3)') i
c         ijres(5,i-minres+1)=0  !assign chain id of zero to nodes
c       endif
c     enddo

c     do i=1,nchain
c       nlong=0
c       do j=minres,maxres
c         if(ijres(5,j-minres+1).eq.i) then
c           nlong=nlong+1
c           write(*,'('' Chain '',i3,'' residue '',i3,'' resid '',i3)')
c    x        i,nlong,j
c         endif
c       enddo
c       write(*,'('' Length of chain '',i3,'' is '',i3,'' residues.'')')
c    x    i,nlong
c     enddo



c
c     build a psf file for vmd to use
c
      is=index(fname,'.lmp')-1
      open(15,file=fname(1:is)//'.psf')
      call amassinit(amass,120)

      write(15,'(''PSF '')')
      write(15,*)
      write(15,*)
      write(15,'(i8,'' !NTITLE'')') 3
c     note could write the date, names of files used, summary of structure
      write(15,'('' REMARKS '','' Generated by mixture   '')') 
      write(15,'('' REMARKS '','' Generated by mixture   '')') 
      write(15,'('' REMARKS '','' Generated by mixture   '')') 
      write(15,*)

      write(15,'(i8,'' !NATOM'')') nsitet
c     fields are atom ID
c                segment name   (might eventually make this into a strand index)
c                residue ID
c                residue name
c                atom name
c                atom type (charmm format uses integer; xplor uses name)
c                charge
c                mass
      iatom=0
      iroffset=0  !offset for types
      do i=1,ntypes
        isoffset=0  !offset for molecules within a type
        do j=1,num(i)
          iresnum=0  !counts residues from 0 within a copy of one copy of a system
          do k=1,nsites(i)
            iatom=iatom+1
            call parser(atname(k,i),string,lele,i1,i2,i3)
            if(i3.gt.-1) then
              iresnum=i3
            elseif(i3.le.-1) then
c             messy fixup for cases where water molecules were added without 
c             following a newly established naming convention; this 
c             will trap when the string has O or H AND i3=-1 
c             for these cases we increment the residue number from previous
              if(string(1:1).eq.'O') iresnum=iresnum+1
            endif
            iresindex=iresnum+1
            if(i.eq.1.and.j.eq.1) iresindex=iresindex-1
c           istrnd=ijres(5,i3-minres+1)
            iat=igetatnum(string(1:lele))
            it=itype(k,i)
            is1=index(nametypes(it,i),'_')  !residue name in substring 1:is1-1
       
            if(iat.gt.0) then
c             write(15,'(i8,1x,a1  
c                    atom and segment 
c    x             ,i5,4x,a3     
c                    residue
c    x             ,2x,a3,2x,a3    
c                    atom
c    x             ,f12.6,f14.4,i20 
c                    charge and mass
c    x              )')     
c           following format file from archive.ambermd.org
c           (I8,1X,A4,1X, I4,1X,A4,1X, A4,1X,I4,1X, 2G14.6,I8)
              write(15,'(i8,1x,a4,1x, 
c                    atom and segment 
     x              i4,1x,a4,1x,    
c                    residue
     x              a4,1x,i4,1x, 
c                    atom
     x              2f14.6,i8 
c                    charge and mass
     x              )')     
     x          iatom,char(ichar('A')+i-1),  !molecule types get chain id A, B, C, ....
     x          iresindex+isoffset+iroffset,nametypes(it,i)(1:is1-1),
c    x          string(1:lele),string(1:lele),    ! atom name/type xplor format uses string
     x          string(1:lele),12,                ! atom name/type charmm format uses number
     x          charge(k,i),amass(iat),0 
            else
c             write(15,'(i8,1x,a1  
c                    atom and segment 
c    x             ,i5,4x,a3     
c                    residue
c    x             ,2x,a3,2x,a3    
c                    atom
c    x             ,f12.6,f14.4,i20 
c                    charge and mass
c    x              )')     
              write(15,'(i8,1x,a4,1x,  
c                    atom and segment 
     x              i4,1x,a4,1x,     
c                    residue
     x              a4,1x,i4,1x,    
c                    atom
     x              2f14.6,i8 
c                    charge and mass
     x              )')     
     x          iatom,char(ichar('A')+i-1),  !molecule types get chain id A, B, C, ....
     x          iresindex+isoffset+iroffset,nametypes(it,i)(1:is1-1),
c    x          string(1:lele),string(1:lele),    ! atom name/type xplor format uses string
     x          string(1:lele),12,                ! atom name/type charmm format uses number
     x          charge(k,i),-100.,0 

            endif
          enddo !end of loop over sites of molecules of type i
          isoffset=isoffset+iresindex
        enddo   !end of loop over molecules of type i
        iroffset=iroffset+isoffset
      enddo     !end of loop over types


c     all solvent molecules have the same chain identifier (name=W id=nchain+1)
c     do i=1,nsolvnt1   !loop over remaining solvent sites
c       molid=1+i
c       imol=closest(1,i+nwremv)
c       do j=1,isolmol
c         jat=isolmol*(imol-1)+j
c         it=itype2(jat)
c         is1=index(nametypes2(it),'_')  !residue in substring 1:is1-1
c         is2=index(nametypes2(it)(is1+1:),'_')  
c         if(is2.eq.0) is2=index(nametypes2(it)(is1+1:),' ')
c         is2=is2+is1                    !element in substring is1+1:is2-1
c         iat=igetatnum(nametypes2(it)(is1+1:is2-1))
c         write(15,'(i8,1x,a1  
c                  atom and segment 
c    x           ,i5,4x,a3     
c                  residue id and name
c    x           ,2x,a3,2x,a3    
c                  atom
c    x           ,f12.6,f14.4,i20 
c                  charge and mass
c    x            )')     
c    x      nsites+isolmol*(i-1)+j,'W',
c    x      i+maxres,nametypes2(it)(1:is1-1),
c    x      nametypes2(it)(is1+1:is2-1),
c    x      nametypes2(it)(is1+1:is2-1),
c    x      charge2(jat),amass(iat),0 
c         call lstchr(atname2(jat),i1)
c         write(15,'(i10,i10,i5,f10.5,3f15.8,
c    x     ''    # '',a)')
c    x     nsites+isolmol*(i-1)+j,molid,natomtp+itype2(jat),
c    x     charge2(jat),(coord2(k,jat),k=1,3),atname2(jat)(1:i1)
c       enddo
c     enddo


c
c     list the bonds
c
      write(15,*)
      write(15,'(i8,'' !NBONDS'')') nbondt
      nbuf=0  !number in the buffer
      ioffset=0
      do i=1,ntypes
        do j=1,num(i)
          do k=1,nbonds(i)
            nbuf=nbuf+1
            ibuf(nbuf,1)=ijbond(1,k,i)+ioffset
            ibuf(nbuf,2)=ijbond(2,k,i)+ioffset
            if(nbuf.eq.4) then
              write(15,'(8i8)') (ibuf(l,1),ibuf(l,2),l=1,4)
              nbuf=0
            endif
          enddo
          ioffset=ioffset+nsites(i)
        enddo
      enddo

      if(nbuf.gt.0) write(15,'(8i8)') (ibuf(l,1),ibuf(l,2),l=1,nbuf)

c
c     four pairs per line 8i8
c     nsets=(nbonds+nsolvnt1*(nbonds2/nsolvnt0))/4
c     nleft=(nbonds+nsolvnt1*(nbonds2/nsolvnt0))-4*nsets
c     do iset=1,nsets    
c       write(15,'('' iset = '',i6)') iset
c       i1=4*(iset-1)+1  !first bond of this set of four
c       i2=4*(iset-1)+2  !second bond of this set of four
c       i3=4*(iset-1)+3  !third bond of this set of four
c       i4=4*(iset-1)+4  !fourth bond of this set of four

c       ia1=ijbond(1,i1)  !in the polymer
c       ia2=ijbond(2,i1)  !in the polymer
c       ib1=ijbond(1,i2)  !in the polymer
c       ib2=ijbond(2,i2)  !in the polymer
c       ic1=ijbond(1,i3)  !in the polymer
c       ic2=ijbond(2,i3)  !in the polymer
c       id1=ijbond(1,i4)  !in the polymer
c       id2=ijbond(2,i4)  !in the polymer

c       find out what solvent molecule this pertains to
c       if(i1.gt.nbonds) then
c         ibmol=1+(i1-nbonds-1)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c         ibnd=(i1-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c         imol=closest(1,ibmol+nwremv) !which molecule in the big list
c         ia1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c         ia2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c         write(15,'('' i1:  ibmol,ibnd,imol '',3i10)') ibmol,ibnd,imol
c       endif

c       if(i2.gt.nbonds) then
c         ibmol=1+(i2-nbonds-1)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c         ibnd=(i2-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c         imol=closest(1,ibmol+nwremv) !which molecule in the big list
c         ib1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c         ib2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c         write(15,'('' i2:  ibmol,ibnd,imol '',3i10)') ibmol,ibnd,imol
c       endif

c       if(i3.gt.nbonds) then
c         ibmol=1+(i3-nbonds-1)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c         ibnd=(i3-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c         imol=closest(1,ibmol+nwremv) !which molecule in the big list
c         ic1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c         ic2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c         write(15,'('' i3:  ibmol,ibnd,imol '',3i10)') ibmol,ibnd,imol
c       endif

c       if(i4.gt.nbonds) then
c         ibmol=1+(i4-nbonds-1)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c         ibnd=(i4-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c         imol=closest(1,ibmol+nwremv) !which molecule in the big list
c         id1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c         id2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c         write(15,'('' i4:  ibmol,ibnd,imol '',3i10)') ibmol,ibnd,imol
c       endif
c       write(15,'(8i8)') ia1,ia2, ib1,ib2, 
c    x                    ic1,ic2, id1,id2  
c     enddo   



c     if(nleft.gt.0) then
c       i1=4*nsets+1  !first bond of this set of four
c       i2=4*nsets+2  !second bond of this set of four
c       i3=4*nsets+3  !third bond of this set of four
c       ia1=ijbond(1,i1)  !in the polymer
c       ia2=ijbond(2,i1)  !in the polymer
c       ib1=ijbond(1,i2)  !in the polymer
c       ib2=ijbond(2,i2)  !in the polymer
c       ic1=ijbond(1,i3)  !in the polymer
c       ic2=ijbond(2,i3)  !in the polymer

c       if(i1.gt.nbonds) then
c         ibmol=1+(i1-nbonds)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c         ibnd=(i1-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c         imol=closest(1,ibmol+nwremv) !which molecule in the big list
c         ia1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c         ia2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c       endif

c       if(i2.gt.nbonds) then
c         ibmol=1+(i2-nbonds)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c         ibnd=(i2-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c         imol=closest(1,ibmol+nwremv) !which molecule in the big list
c         ib1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c         ib2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c       endif

c       if(i3.gt.nbonds) then
c         ibmol=1+(i3-nbonds)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c         ibnd=(i3-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c         imol=closest(1,ibmol+nwremv) !which molecule in the big list
c         ic1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c         ic2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c       endif

c       if(nleft.eq.1) then
c         write(15,'(8i8)') ia1,ia2
c       elseif(nleft.eq.2) then
c         write(15,'(8i8)') ia1,ia2,ib1,ib2
c       elseif(nleft.eq.3) then
c         write(15,'(8i8)') ia1,ia2,ib1,ib2,ic1,ic2
c       endif
c     endif
c
c

      write(15,*)

      close(15)   !close the psf file



c     call bengy(nsites,coord,nbonds,ijbond,nbtp,bndpar)
c     call aengy(nsites,coord,nangle,ijkangle,natp,angpar)
c     call dengy(nsites,coord,ndih,ijkldih,ndtp,dihpar)
c     call vdwcou(nsites,itype,coord,charge,vdw,atname,
c    x                  natmx,nattpmx,
c    x                  nbonds,ijbond,nbnmx,
c    x                  nangle,ijkangle,nanmx)




c
c     write out a table to implement geometric combining rules
c
c     do i=1,natomtp-1
c       do j=i+1,natomtp
c         write(*,'(''pair_coeff     '',2i5,2f15.8)')
c    x    i,j,dsqrt(vdw(2,i)*vdw(2,j)),
c    x        dsqrt(vdw(1,i)*vdw(1,j))
c       enddo
c     enddo


      end

c
c     subroutine to compute electrostatic and vdw intramolecular energy
c
      subroutine vdwcou(nsites,itype,coord,charge,vdw,atname,
     x                  natmx,nattpmx,
     x                  nbonds,ijbond,nbnmx,
     x                  nangle,ijkangle,nanmx)
      implicit real*8 (a-h,o-z)
      dimension coord(3,natmx),charge(natmx),itype(natmx)
      character*(*) atname(nattpmx)
      dimension vdw(4,nattpmx)
      dimension ijbond(3,nbnmx)
      dimension ijkangle(4,nanmx)

      dimension neigh(5000,5)
      dimension ipair14(2,8000)

c
c     electrostatics and vdw
c
      ecou=0.d0
      evdw=0.d0
      do i=1,nsites-1
        it=itype(i)
        do j=i+1,nsites
        jt=itype(j)
        dist=dsqrt( (coord(1,i)-coord(1,j))**2
     x             +(coord(2,i)-coord(2,j))**2
     x             +(coord(3,i)-coord(3,j))**2 )
        ecou=ecou + charge(i)*charge(j)/dist
        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/(dist**12) - cv/(dist**6)
        evdw=evdw+eij                  
c       write(*,'('' i,j,r = '',2i5,f10.4,'' sig eps = '',
c    x    2f10.4,'' Eij = '',f10.3)')
c    x    i,j,dist,sig,eps,eij
        enddo
      enddo


c
c     build neighbor list
c
      do i=1,5000
        do j=1,5
          neigh(i,j)=0
        enddo
      enddo




      e12=0.d0
      e12vdw=0.d0
      do i=1,nbonds
        ia=ijbond(1,i)
        ib=ijbond(2,i)
        na=neigh(ia,1)+1
        neigh(ia,1)=na
        neigh(ia,na+1)=ib
        nb=neigh(ib,1)+1
        neigh(ib,1)=nb
        neigh(ib,nb+1)=ia

        dist=dsqrt( (coord(1,ia)-coord(1,ib))**2
     x             +(coord(2,ia)-coord(2,ib))**2
     x             +(coord(3,ia)-coord(3,ib))**2 )
        e12=e12 + charge(ia)*charge(ib)/dist
        it=itype(ia)
        jt=itype(ib)
        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
        if(eij.gt.1000.d0) then
          write(*,'('' Large VDW term, i,j = '',2i6,
     x      '' e_ij = '',f25.4)') ia,ib,eij
  
        endif
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/(dist**12) - cv/(dist**6)
        e12vdw=e12vdw+eij
      enddo


c     note that every 1-3 interaction is in an angle term and it is there only once
      e13=0.d0
      e13vdw=0.d0
      do i=1,nangle
        ia=ijkangle(1,i)
        ja=ijkangle(2,i)
        ka=ijkangle(3,i)
        dist=dsqrt( (coord(1,ia)-coord(1,ka))**2
     x             +(coord(2,ia)-coord(2,ka))**2
     x             +(coord(3,ia)-coord(3,ka))**2 )
        e13=e13 + charge(ia)*charge(ka)/dist
        it=itype(ia)
        jt=itype(ka)
        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/(dist**12) - cv/(dist**6)
        e13vdw=e13vdw+eij                  
      enddo



c     every 1-4 interaction is in a dihedral term, but multiple dihedrals have the same 1-4
c     some dihedrals are improper and the 1-4 with these are really 1-2 and 1-3
c     so to get the 1-4 list check each torsion, 

      e14=0.d0
      e14vdw=0.d0
      n14=0
      do i=1,nsites
        ni=neigh(i,1) !neighbors of i
        do ineigh=1,ni
          j=neigh(i,ineigh+1) 
          nj=neigh(j,1) !neighbors of j
          do jneigh=1,nj
            k=neigh(j,jneigh+1)
            if(k.ne.i) then   !...that are not i
              nk=neigh(k,1) !neighbors of k
              do kneigh=1,nk
                l=neigh(k,kneigh+1)
                if(l.ne.j) then
                  if(l.gt.i) then
                    n14=n14+1
                    ipair14(1,n14)=i
                    ipair14(2,n14)=l
c                   write(*,'('' 1-4 interaction: '',3i6,
c    x              a,1x,a,1x,a,1x,a)')
c    x              n14,i,l,atname(i),atname(j),atname(k),atname(l)
        dist=dsqrt( (coord(1,i)-coord(1,l))**2
     x             +(coord(2,i)-coord(2,l))**2
     x             +(coord(3,i)-coord(3,l))**2 )
        e14=e14 + charge(i)*charge(l)/dist
        it=itype(i)
        jt=itype(l)
        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/dist**12 - cv/dist**6
        e14vdw=e14vdw+eij                  
                  endif
                endif
              enddo
            endif
          enddo  
        enddo
      enddo


      e14x=0.d0
      e14xdup=0.d0

      e14xvdw=0.d0
      e14xvdwdup=0.d0

      do i=1,n14
        ia=ipair14(1,i)
        ja=ipair14(2,i)
        it=itype(ia)
        jt=itype(ja)
        dist=dsqrt( (coord(1,ia)-coord(1,ja))**2
     x             +(coord(2,ia)-coord(2,ja))**2
     x             +(coord(3,ia)-coord(3,ja))**2 )

        e14x=e14x + charge(ia)*charge(ja)/dist

        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/dist**12 - cv/dist**6

        e14xvdw=e14xvdw+eij                  

        do j=i+1,n14
          ib=ipair14(1,j)
          jb=ipair14(2,j)
          if(ib.eq.ia.and.jb.eq.ja) then
            write(*,'('' duplicate 1-4:  '',a,1x,a)')
     x        atname(ib),atname(jb)
            e14xvdwdup=e14xvdwdup+eij
            e14xdup=e14xdup+
     x              charge(ia)*charge(ja)/dist
          endif
        enddo
      enddo


      fact=627.509d0*0.529177d0  !332.d0
      write(*,'('' Factor = '',f15.6)') fact

      write(*,'(//,'' Electrostatic interactions '',/)') 

      write(*,'(/,'' Ecou from all 1-4 terms     : '',f10.5)')
     x   fact*e14x
      write(*,'('' Ecou from duplicated terms: '',f10.5)')
     x   fact*e14xdup
      write(*,'('' Ecou without duplicates   : '',f10.5)')
     x   fact*e14x-e14xdup


      write(*,'('' Ecou= '',f15.6)') fact*ecou
      write(*,'('' E12 = '',f15.6)') fact*e12
      write(*,'('' E13 = '',f15.6)') fact*e13 
      write(*,'('' E14 = '',f15.6)') fact*e14 
c     write(*,'('' E14x= '',f15.6)') fact*e14x 


      write(*,'(/,'' Ecoul-E12-E13-E14     = '',f15.6)') 
     x    fact*(ecou-e13-e12-e14)
      write(*,'(  '' E14                   = '',f15.6)') 
     x    fact*(e14)
      write(*,'(  '' Ecoul-E12-E13-0.5*E14 = '',f15.6)') 
     x    fact*(ecou-e13-e12-e14+0.5d0*e14)




      write(*,'(//,'' Van der Waals interactions '',/)') 

      write(*,'(/,'' Evdw from all 1-4 terms     : '',f10.5)')
     x   e14xvdw
      write(*,'('' Evdw from duplicated terms: '',f10.5)')
     x   e14xvdwdup
      write(*,'('' Evdw without duplicates   : '',f10.5)')
     x   e14xvdw-e14xvdwdup


      write(*,'(/,'' Evdw= '',f25.4)') evdw
      write(*,'('' E12v= '',f25.4)') e12vdw
      write(*,'('' E13v= '',f25.4)') e13vdw
      write(*,'('' E14v= '',f25.4)') e14vdw-e14xvdwdup

      write(*,'(/,'' Evdw-E12v-E13v-E14v     = '',f25.4)') 
     x    (evdw-e13vdw-e12vdw-(e14vdw-e14xvdwdup))
      write(*,'(  '' E14                     = '',f25.4)') 
     x    (e14vdw-e14xvdwdup)
      write(*,'(  '' Evdw-E12v-E13v-0.5*E14v = '',f25.4)') 
     x    (evdw-e13vdw-e12vdw-(e14vdw-e14xvdwdup)
     x    +0.5d0*(e14vdw-e14xvdwdup))




      end
c
c     subroutine to read a set of coordinates from an xyz file
c     expect this to have a set of coords, we are usually interested
c     in either the first or the last
c
      subroutine getcoord(fname,nsites,itype,coord,natmx)
      implicit real*8 (a-h,o-z)
      character*120 line
      character*(*) fname
      dimension coord(3,natmx),itype(natmx)

      write(*,'('' Reading coordinates from file *'',a,''*'')')
     x     fname(1:40)

      open(10,file=fname)  
c
c     file format is 
c     line 1: number
c     line 2: Atoms  
c     line 3,... for each atom a type and three coords
c     this is repeated for as many frames
c
c     first count how many frames save line number of last
c
      nframe=0
10    continue
      read(10,'(a120)',end=50) line
      nframe=nframe+1
      read(line,*) nat
      if(nat.ne.nsites) then
        write(*,'('' Error:  xyz and lmp not matched '',/,
     x    '' Files have different number of sites:  N(xyz) = '',
     x    i7,'' vs N(lmp) = '',i7)') nat,nsites
        stop 'xyz file not matched with cooresponding lmp file'
      endif
      read(10,'(a120)') line   !line has "Atoms"
      do i=1,nat
        read(10,'(a120)') line
        read(line,*) it
        if(it.ne.itype(i)) then
          write(*,'('' Type mismatch between xyz and lmp files '')')
          write(*,'('' For atom number '',i6,
     x      '' Type(xyz) = '',i4,'' Type(lmp) = '',i4)') i,it,itype(i)
          stop 'xyz file and lmp file have different atom types'
        endif
      enddo
      go to 10


50    continue
      iframe=nframe  !this takes the last frame
      write(*,'('' Selecting frame '',i5,'' from a total'',
     x          '' of '',i5)') iframe,nframe

      rewind(10)
c     position file to right place
      do i=1,(2+nsites)*(iframe-1)
        read(10,'(a120)') line
      enddo
c     now read into coord
      read(10,'(a120)') line
      read(10,'(a120)') line
      do i=1,nat
        read(10,'(a120)') line
        read(line,*) it,(coord(k,i),k=1,3)
      enddo
c

      close(10)



      end

c
c     subroutine to read in lmp parameter/coordinate file
c
      subroutine readlmp(fname,nsites,itype,molid,
     x                    charge,coord,atname,natmx,
     x                    nbonds,nbtp,bndpar,ijbond,nbnmx,nbtmx,
     x                    nangle,natp,angpar,ijkangle,nanmx,nantmx,
     x                    ndih,ndtp,dihpar,ijkldih,ndimx,nditmx,
     x                    natomtp,fmass,vdw,nametypes,nattpmx,
     x                    box)
      implicit real*8 (a-h,o-z)
      character*(*) fname
      character*120 line

c     natmx - max number of atoms (sites) (nsites.le.natmx) 
c     nattpmx - max number of atom types (natomtp.le.nattpmx)

c     nbonds - number of bonds
c     nbnmx - max number of bonds (nbonds.le.nbnmx)
c     nbtp - number of bond types
c     nbtmx - max number of bond types (nbtp.le.nbtmx)

c     nangle - number of angles
c     nanmx - max number of angles (nangle.le.nanmx)
c     natp - number of angle types
c     nantmx - max number of angle types (natp.le.nantmx)

c     ndih - number of dihedral angles
c     ndimx - max number of dihedral angles (ndih.le.ndimx)
c     ndtp - number of dihedral angle types
c     nditmx - max number of dihedral angle types (ndtp.le.nditmx)

c     natomtp - number of atom types
c     nattpmx - max number of atom types (natomtp.le.nattpmx)

      dimension coord(3,natmx),charge(natmx),itype(natmx),molid(natmx)
      character*(*) atname(natmx)
      dimension box(3,2) 

      dimension ijbond(3,nbnmx),bndpar(2,nbtmx)
      dimension ijkangle(4,nanmx),angpar(2,nantmx)
      dimension ijkldih(5,ndimx),dihpar(4,nditmx)

      dimension vdw(4,nattpmx),fmass(nattpmx)
      character*(*) nametypes(nattpmx)

      is=index(fname,' ')-1
      write(*,'(/,'' Reading parameters from file *'',a,''*'')')
     x     fname(1:is)
      open(10,file=fname)  

c     read number of atoms and their coordinates
c     read nsites,itype,charge,coords,atname

10    continue
      read(10,'(a120)',end=21) line
      if(index(line,'atoms').eq.0) go to 10
      read(line,*) nsites
      write(*,'('' natoms = '',i8)') nsites
      if(nsites.gt.natmx) then
        write(*,'('' Increase size of site array; '',/,
     x            '' nsites = '',i10,'' natmx = '',i10)')
     x            nsites,natmx
        stop 'Increase natmx'
      endif
21    continue


      write(*,'('' Reading coordinates and charges for '',i7,
     x  '' sites '')') nsites
20    continue
      read(10,'(a120)',end=22) line
      if(index(line,'Atoms').eq.0) go to 20
      read(10,'(a120)') line
      totchg=0.d0
      do i=1,nsites
        read(10,'(a120)') line
        read(line,*) ix,im,it1,ch1,x1,y1,z1
c       read(line,'(i10,i5,i5)') ix,im,it1 
c       read(line(21:),*) ch1,x1,y1,z1
        if(ix.gt.nsites.or.ix.le.0) then
          write(*,'('' Error: site index out of range i = '',i10,
     x      '' ix = '',i10)') i,ix
          stop 'error reading coordinates'
        endif 
        molid(ix)=im
        itype(ix)=it1
        charge(ix)=ch1
        coord(1,ix)=x1
        coord(2,ix)=y1
        coord(3,ix)=z1
        if(mod(i,5000).eq.0) then
          write(*,'('' Reading coordinates for '',i6,'' of '',i6)') 
     x      i,nsites
        endif
        iloc=index(line,'#')
        if(iloc.ne.0) then
          read(line(iloc+1:),'(a20)') atname(ix)
        else
          write(atname(ix),'(''X'',i5.5)') ix
        endif
        totchg=totchg+charge(ix)
c       write(*,'('' IN ATOM LOOP: ix,im,it1,atname: '',3i5,
c    x    '' *'',a,''*'')')
c    x    ix,im,it1,atname(ix)
      enddo

      write(*,'('' Total charge '',f15.5)') totchg
22    continue
      
c     do i=1,nsites
c       write(*,'('' Site '',i6,'' *'',a,''* Type '',i5,3f10.5)')
c    x    i,atname(i)(1:20),itype(i)
c    x    ,(coord(k,i),k=1,3)
c     enddo

c
c     read box sizes
c
      rewind(10)
      ifxset=0
      ifyset=0
      ifzset=0
23    continue
      read(10,'(a120)') line
      isx=index(line,'xlo') 
      isy=index(line,'ylo') 
      isz=index(line,'zlo') 
      if(isx+isy+isz.eq.0) go to 23
      if(isx.ne.0) then 
        read(line,*) xlo,xhi
        ifxset=1
      elseif(isy.ne.0) then
        read(line,*) ylo,yhi
        ifyset=1
      elseif(isz.ne.0) then
        read(line,*) zlo,zhi
        ifzset=1
      endif
      if(ifxset*ifyset*ifzset.eq.0) go to 23
      write(*,'('' Box dimensions (X): '',2f15.8,/,
     x          '' Box dimensions (Y): '',2f15.8,/,
     x          '' Box dimensions (Z): '',2f15.8)')
     x  xlo,xhi,ylo,yhi,zlo,zhi
      box(1,1)=xlo
      box(1,2)=xhi
      box(2,1)=ylo
      box(2,2)=yhi
      box(3,1)=zlo
      box(3,2)=zhi


c     read bonds
c     read nbonds,nbtp,bndpar(2,*),ijbond(3,*)

      rewind(10)
30    continue
      read(10,'(a120)',end=40) line
      if(index(line,' bonds ').eq.0) go to 30
      read(line,*) nbonds
      if(nbonds.gt.nbnmx) then
        write(*,'('' Increase size of bond array; '',/,
     x            '' nbonds = '',i10,'' nbnmx = '',i10)')
     x            nbonds,nbnmx
        stop 'Increase nbnmx'
      endif
40    continue
      read(10,'(a120)',end=45) line
      if(index(line,' bond types ').eq.0) go to 40
      read(line,*) nbtp
c     write(*,'('' Nbond types = '',i6)') nbtp
      if(nbtp.gt.nbtmx) then
        write(*,'('' Increase size of bond type array; '',/,
     x            '' nbtp   = '',i10,'' nbtmx = '',i10)')
     x            nbtp,nbtmx
        stop 'Increase nbtmx'
      endif
45    continue
      read(10,'(a120)',end=50) line
      if(index(line,'Bond Coeffs').eq.0) go to 45
      read(10,'(a120)') line
      do i=1,nbtp  
        read(10,'(a120)') line
        read(line,*) ix,bndpar(1,i),bndpar(2,i)
c       write(*,'('' Bond type '',i3,'' Parm '',2f10.5)')
c    x     i,bndpar(1,i),bndpar(2,i)
      enddo
50    continue
      read(10,'(a120)',end=55) line
      if(index(line,'Bonds').eq.0) go to 50

      write(*,'('' Reading list of '',i7,
     x  '' bonds '')') nbonds
      read(10,'(a120)') line
      do i=1,nbonds
        read(10,'(a120)') line
        read(line,*) ix,ijbond(3,i),ijbond(1,i),ijbond(2,i)
        if(mod(i,10000).eq.0) then
          write(*,'('' Reading bonds '',i6,'' of '',i6)') 
     x      i,nbonds
        endif
c       write(*,'('' Bond '',i6,'' Type '',i4,'' Sites '',2i6)')
c    x    i,ijbond(3,i),ijbond(1,i),ijbond(2,i)
      enddo


c     read angles
c     nangle,natp,angpar(2,*),ijkangle(4,*)

      rewind(10)
55    continue
      read(10,'(a120)',end=60) line
      if(index(line,' angles').eq.0) go to 55
      read(line,*) nangle
      if(nangle.gt.nanmx) then
        write(*,'('' Increase size of angle array; '',/,
     x            '' nangle = '',i10,'' nanmx = '',i10)')
     x            nangle,nanmx
        stop 'Increase nanmx'
      endif
60    continue
      read(10,'(a120)',end=65) line
      if(index(line,' angle types ').eq.0) go to 60
      read(line,*) natp
      if(natp.gt.nantmx) then
        write(*,'('' Increase size of angle type array; '',/,
     x            '' natp   = '',i10,'' nantmx = '',i10)')
     x            natp,nantmx
        stop 'Increase nantmx'
      endif
65    continue
      read(10,'(a120)',end=70) line
      if(index(line,'Angle Coeffs').eq.0) go to 65
      read(10,'(a120)') line
      do i=1,natp  
        read(10,'(a120)') line
        read(line,*) ix,angpar(1,i),angpar(2,i)
      enddo
70    continue
      read(10,'(a120)',end=71) line
      if(index(line,'Angles').eq.0) go to 70
      write(*,'('' Reading list of '',i7,
     x  '' angles '')') nangle
      read(10,'(a120)') line
      do i=1,nangle
        read(10,'(a120)') line
        read(line,*) ix,ijkangle(4,i),(ijkangle(k,i),k=1,3)
      enddo
71    continue



c     read dihedrals
c     read ndih,ndtp,dihpar(4,*),ijkldih(5,*)

      rewind(10)
75    continue
      read(10,'(a120)',end=80) line
      if(index(line,' dihedrals').eq.0) go to 75
      read(line,*) ndih  
c     if(ndih.lt.1) go to 91
      if(ndih.gt.ndimx) then
        write(*,'('' Increase size of dihedral array; '',/,
     x            '' ndih   = '',i10,'' ndimx = '',i10)')
     x            ndih,ndimx
        stop 'Increase ndimx'
      endif
80    continue
      read(10,'(a120)',end=85) line
      if(index(line,' dihedral types ').eq.0) go to 80
      read(line,*) ndtp
c     write(*,'('' Dihedrals: '',i6,''  Types: '',i6)')
c    x   ndih,ndtp
      if(ndtp.gt.nditmx) then
        write(*,'('' Increase size of dihedral type array; '',/,
     x            '' ndtp   = '',i10,'' nditmx = '',i10)')
     x            ndtp,nditmx
        stop 'Increase nditmx'
      endif
85    continue
      read(10,'(a120)',end=90) line
      if(index(line,'Dihedral Coeffs').eq.0) go to 85
      read(10,'(a120)') line
      do i=1,ndtp  
        read(10,'(a120)') line
c       write(*,'('' Line: *'',a,''*'')') line(1:50)
        read(line,*) ix,dihpar(1,i),nd,dd,dihpar(4,i)
c       write(*,'('' '',i5,f10.4,i5,i5,f10.4)') 
c    x    is,dihpar(1,i),nd,dd,dihpar(4,i)
        dihpar(2,i)=nd         
        dihpar(3,i)=dd         
      enddo
c     write(*,'('' Types read in '')')
90    continue
      read(10,'(a120)',end=91) line
      if(index(line,'Dihedrals').eq.0) go to 90
      write(*,'('' Reading list of '',i7,
     x  '' dihedrals '')') ndih  
      read(10,'(a120)') line
      do i=1,ndih  
        read(10,'(a120)') line
        read(line,*) ix,ijkldih(5,i),(ijkldih(k,i),k=1,4)
      enddo
c     write(*,'('' Dihedrals read in '')')
91    continue


c     read vdw
c     read natomtp,vdw(4,*)

      rewind(10)
100   continue
      read(10,'(a120)',end=105) line
      if(index(line,' atom types ').eq.0) go to 100
      read(line,*) natomtp
      write(*,'('' Reading list of '',i7,
     x  '' vdw parameters '')') natomtp
      if(natomtp.gt.nattpmx) then
        write(*,'('' Increase size of vdw array; '',/,
     x            '' natomtp   = '',i10,'' nattpmx = '',i10)')
     x            natomtp,nattpmx
        stop 'Increase nattpmx'
      endif
      ifound=0
105   continue
      read(10,'(a120)',end=107) line
      if(index(line,'Pair Coeffs').eq.0) go to 105
      ifound=1
      read(10,'(a120)') line
      do i=1,natomtp  
        read(10,'(a120)') line
        read(line,*) ix,vdw(2,i),vdw(1,i),vdw(4,i),vdw(3,i)
c       write(*,'('' Atom type '',i3,'' vdw = '',2f10.5)')
c    x     i,vdw(1,i),vdw(2,i)
      enddo
107   continue
      if(ifound.eq.0) then
        write(*,'('' Error:  this lmp file is missing '',
     x  '' the Pair Coeffs block needed for generating mixtures '')')
        write(*,'('' SETTING THESE TO ZEROS'')')
        do i=1,natomtp
          vdw(1,i)=0.d0
          vdw(2,i)=0.d0
          vdw(3,i)=0.d0
          vdw(4,i)=0.d0
        enddo
c       stop 'missing Pair Coeffs block in lmp file'
      endif

c
c     read masses 
c     note that we are assuming that extra information is on these
c     lines in files made by ffassign with type names
c
      rewind(10)
26    continue
      read(10,'(a120)') line
      if(index(line,'Masses').eq.0) go to 26
      write(*,'('' Reading list of '',i7,
     x  '' masses  '')') natomtp
      read(10,'(a120)') line
      do i=1,natomtp  
        read(10,'(a120)') line
        read(line,*) ix,fmass(i)
c       write(*,'('' Atom type '',i3,'' mass = '',f10.5)')
c    x     i,fmass(i)           

c     extract the name string: character*100 nametypes(100)
        nametypes(i)=' '
        is=index(line,'#')
        ks=1
        ie=1
        if(is.ne.0) then
c         in line(is+1:) look for LAST token         
c         this will be a nonblank string surrounded by blanks
          ie=0
          do js=len(line),is+1,-1
            if(line(js:js).ne.' ') then
              ie=js
              go to 210
            endif
          enddo
210       continue
          if(ie.ne.0) then
            ks=0
            do js=ie,is+1,-1
              if(line(js:js).eq.' ') then
                ks=js+1 
                go to 215
              endif
            enddo
          endif
215       continue
c         at this point the last token is in line(ks:ie) 
          nametypes(i)=line(ks:ie)
        endif
        write(*,'('' For atom type '',i3,'' Name *'',a,''*'')')
     x    i,nametypes(i)(1:ie-ks+1)
      enddo
c


200   continue

      close(10)


      end


c
c     subroutine to compute bond energy
c
      subroutine bengy(nsites,coord,nbonds,ijbond,nbtp,bndpar)
      implicit real*8 (a-h,o-z)
      dimension coord(3,nsites),ijbond(3,nbonds),bndpar(2,nbtp)

      ebond=0.d0
      do i=1,nbonds
        ia=ijbond(1,i)
        ja=ijbond(2,i)
        dist=dsqrt( (coord(1,ia)-coord(1,ja))**2
     x             +(coord(2,ia)-coord(2,ja))**2
     x             +(coord(3,ia)-coord(3,ja))**2 )
        it=ijbond(3,i)
        eij=bndpar(1,it) * (dist - bndpar(2,it))**2
        ebond=ebond+eij
        write(*,'('' Site '',i6,'' R= '',3f10.5,'' ; Site '',i5,
     x    '' R= '',3f10.5)')  ia,(coord(k,ia),k=1,3),
     x                        ja,(coord(k,ja),k=1,3)
        write(*,'('' Bond '',i5,'' Sites '',2i6,'' dist '',f10.5,
     x    '' Eij '',f10.5)')
     x    i,ia,ja,dist,eij
      enddo
      write(*,'('' E(bond) = '',f15.6)') ebond



      end

c
c     subroutine to compute angle energy
c
      subroutine aengy(nsites,coord,nangle,ijkangle,natp,angpar)
      implicit real*8 (a-h,o-z)
      dimension coord(3,nsites),ijkangle(4,nangle),angpar(2,natp)
      pi=4.d0*datan(1.d0)


      eang=0.d0
      do i=1,nangle
        ia=ijkangle(1,i)
        ja=ijkangle(2,i)
        ka=ijkangle(3,i)
        it=ijkangle(4,i)
        call angint(coord(1,ia),coord(1,ja),coord(1,ka),thet)
        eijk=angpar(1,it) * (thet - (pi/180.d0)*angpar(2,it))**2
c       write(*,'('' Angle index = '',i5,'' type = '',i5,
c    x   '' parms = '',2f10.3)')
c    x   i,it,angpar(1,it),angpar(2,it)
c       write(*,'('' Angle = '',f10.3,'' Engy = '',f10.3)')
c    x    thet*(180.d0/pi),eij
        eang =eang +eijk
      enddo
      write(*,'('' E(angl) = '',f15.6)') eang 

      end


c
c     subroutine to compute dihedral energy
c
      subroutine dengy(nsites,coord,ndih,ijkldih,ndtp,dihpar)
      implicit real*8 (a-h,o-z)
      dimension coord(3,nsites),ijkldih(5,ndih),dihpar(4,ndtp)
      dimension dum1(12),dum2(78)
      pi=4.d0*datan(1.d0)
      edih=0.d0
      do i=1,ndih  
        ia=ijkldih(1,i)
        ja=ijkldih(2,i)
        ka=ijkldih(3,i)
        la=ijkldih(4,i)
        it=ijkldih(5,i)
        call dihint(1,coord(1,ia),coord(1,ja),
     x                coord(1,ka),coord(1,la),
     x              phi,dum1,dum2)
        eijkl=dihpar(1,it) * ( 1.d0 + 
     x      cos(dihpar(2,it)*phi - (pi/180.d0)*dihpar(3,it)))
        edih =edih +eijkl
c       write(*,'('' Term '',i6,'' phi = '',f10.5,
c    x    '' Parms = '',3f10.5,'' E(ijkl) = '',f12.5)')
c    x   i,phi,dihpar(1,it),dihpar(2,it),dihpar(3,it),eijkl
      enddo
      write(*,'('' E(dihe) = '',f15.6)') edih 

      end



c Change History:
c $Log: util.f,v $
c Revision 1.8  1994/08/03  19:49:54  swope
c Changed behavior of xyz2int so that zmatricies are not printed out tha
c undefined torsions.  When angles are close to 180 they are changed to
c when close to 0 they are changed to 0.0001.    (Swope Aug 3, 1994)
c
c Revision 1.6  1994/07/30  06:33:09  swope
c Several changes by Swope to internal-cartesian and cartesian-internal
c conversion routines:
c angint-switch to low angle expression on new threshold
c dihint-call tlkemsg when atoms too close
c xyz2int-change messages during certain errors
c i2x-changes in ways linear systems treated: two strikes policy
c     for generating cartesians with undefined torsions
c xyz2dih-added new return code to flag 1-2-3 linear vs 2-3-4 linear
c
c Revision 1.5  1994/07/07  16:34:02  gclie
c This version includes xyz2int and int2xyz from Swope.  It should repla
c
c*arc i2x
      subroutine i2x(nat,lnk,rab,ang,dih,istart,iend,xyz)
c
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Copyright (c) 1994 International Business Machines Corporation
C  All rights reserved
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Description      Given the link vector and internal coordinates
c                     of a list of particles, this subroutine computes
c                     the cartesian coordinates for a subset of the
c                     particles, those with indicies from istart to
c                     iend.  The units of the cartesian coordinates
c                     will be the same as those of the input internals.
c                     The units of the angles are assumed to be radians.
c                   If the particle indicies between istart and iend
c                     fall in the range 1,2 or 3, particle 1 is placed
c                     at the origin, particle 2 is placed on the x-axis,
c                     and particle 3 is placed in the x-y plane.
c
C
C  Input
C
C     nat           Number of atoms in list
C
C     lnk(3,nat)    link vector for describing internals
C                     lnk(1,i) gives label for distance from i
C                     lnk(2,i) gives label for angle i-lnk(1,i)-lnk(2,i)
C                     lnk(3,i) gives label for torsion
C                     i-lnk(1,i)-lnk(2,i)-lnk(3,i)
C                     Note that first six entries are ignored.
C
C     rab(nat)      Vector of distances.
C
C     ang(nat)      Vector of angles.  Range returned is 0.le.ang.le.pi.
C
C     dih(nat)      Vector of torsions. Range returned is
C                   -pi .lt. phi .le. pi, and follows IUPAC
C                   sign and range convention.
c
c     istart        The index of the first atom whose cartesian
c                   coordinates are to be computed.  istart must be
c                   an integer with ( 1 .le. istart .le. nat ).
c
c     iend          The index of the last atom whose cartesian
c                   coordinates are to be computed.  iend must be
c                   an integer with ( 1 .le. iend .le. nat ).
C
C
C  Output
C
C     xyz(3,i)      cartesian coordinates of atoms i=istart to iend
C
C
C  Linkage          List of subroutines called by this one
C
C  Programmer       William Swope (some from Carnevali and Nesbet)
C
C  Date             June 1994
C
C  Update log       Record of all updates including names of programmers
C                     descriptions of changes made, and dates of change.
C                   07-29-94 Modified by Swope to die (tlkemsg) if
C                            there are TWO invalid torsion angles.
C                            Invalid torsion angles are those that
C                            result in ill-defined cartesian coordinates
C                            This change still allows linear molecules
C                            and molecules with two long linear segments
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
c
      Implicit Real*8 (a, b, d-h, o, p, r-y)
      Implicit Integer (i-n)
c     Violation of ARCHEM conventions, but there are no logicals here
c     Implicit Logical (z)
      Implicit Real*8 (z)
      Implicit Character (c)
c     Implicit Undefined (q)
 
      character cproduct*48
      dimension cproduct(3)
 
c     Input and output arrays
      real*8 xyz(3,nat),rab(nat),ang(nat),dih(nat)
      integer lnk(3,nat)
 
c     Local arrays
c     Following arrays give molecule coordinate system relative to lab
      real*8 xhat(3),yhat(3),zhat(3),zprev(3)
      real*8 r12(3),r23(3),r43(3)
 
 
C     THE SIGN CONVENTION FOR PHI IS AS FOLLOWS:
C     NUMBER THE ASSOCIATED ATOMS 1, 2, 3, 4.  ATOMS 1 AND 4 ARE
C     "TERMINAL" ATOMS AND THE DIHEDRAL ANGLE IS ABOUT THE BOND
C     BETWEEN ATOMS 2 AND 3.  IMAGINE HOLDING THE PLANE FORMED BY
C     ATOMS 1, 2, AND 3 FIXED AND ROTATING THE PLANE FORMED BY
C     ATOMS 2, 3, AND 4 ABOUT A VECTOR THAT POINTS FROM ATOM 2
C     TO ATOM 3.  THE POSITIVE SENSE CORRESPONDS TO THE RIGHT-HAND
C     RULE FOR ROTATION ABOUT THE VECTOR FROM ATOM 2 TO ATOM 3.
c     This agrees with the IUPAC convention.
C
C     FURTHERMORE, PHI=0 CORRESPONDS TO A CONDITION WHERE ALL FOUR
C     ATOMS ARE ON A PLANE AND ATOMS 1 AND 4 ARE "CIS" TO THE BOND
C     BETWEEN ATOMS 2 AND 3.  LIKEWISE, PHI=180 DEGREES CORRESPONDS
C     A CONDITION WHERE ALL FOUR ATOMS ARE ON A PLANE AND ATOMS 1 AND
C     4 ARE "TRANS" TO THE BOND BETWEEN ATOMS 2 AND 3.
 
c     comment out the following call to check the integrity of the
c     link vector.  this call is appropriate in int2xyz, where the
c     full set of cartesians is computed, but should probably not
c     be made over and over again if the cartesians are computed
c     one at a time or in small increments.
c
c     check validity of link vector
c     call chklnk(nat,lnk,iret)
c     if(iret.ge.2) then
c       print *, ' Error: link matrix is not usable for determination',
c    >           ' of cartesian coordinates.'
c       print *, ' Complete link vector: '
c       write(*,'(3i10)') ((lnk(i,j),i=1,3),j=1,nat)
c       call tlkemsg(' Invalid link matrix.','int2xyz')
c     endif
 
 
c     lnk(1,i),i=1,nat specifies atom indicies for "bond" distances
c     lnk(2,i),i=1,nat specifies atom indicies for "bond" angles
c     lnk(3,i),i=1,nat specifies atom indicies for dihedral angles
 
      pi=4.d0*datan(1.d0)
 
      if(istart.le.0   .or.
     >   istart.gt.nat .or.
     >   iend.le.0     .or.
     >   iend.gt.nat   .or.
     >   istart.gt.iend ) then
      write(*,'('' Error in subroutine i2x: invalid arguments for'',/,
     >          '' atom indicies: istart='',i5,'' iend='',i5,/,
     >          '' Values should be from 1 to nat='',i5)')
     >          istart,iend,nat
C     CALL TLKEMSG(' INVALID ATOM INDICES PASSED TO X2I.','X2I')
      endif
 
      zprev(1)=0.d0
      zprev(2)=0.d0
      zprev(3)=1.d0
 
c     failure count on placing atoms when torsion undefined
      nstrike=0
 
 
      do 10 i=istart,iend
 
      if(i.eq.1) then
c     if first atom
        xyz(1,1)=0.d0
        xyz(2,1)=0.d0
        xyz(3,1)=0.d0
      elseif(i.eq.2) then
c     if second atom
        xyz(1,2)=rab(2)
        xyz(2,2)=0.d0
        xyz(3,2)=0.d0
      elseif(i.eq.3) then
c     if third atom (note that normally 0.le.ang.le.pi)
        if(lnk(1,3).eq.2) then
          xyz(1,3)=rab(2) -rab(3)*cos(ang(3))
        else
          xyz(1,3)=       +rab(3)*cos(ang(3))
        endif
        xyz(2,3)=         +rab(3)*sin(ang(3))
        xyz(3,3)=0.d0
        if(abs(ang(3)).gt.1.d-3 .and.
     >     abs(abs(ang(3))-pi).gt.1.d-3) nstrike=1
      endif
 
c     for fourth or higher atom
      if(i.le.3) go to 10
 
      l2=lnk(1,i)
      l3=lnk(2,i)
      l4=lnk(3,i)
      r23(1)=xyz(1,l2)-xyz(1,l3)
      r23(2)=xyz(2,l2)-xyz(2,l3)
      r23(3)=xyz(3,l2)-xyz(3,l3)
 
c     local x axis is directed from atom l3 to atom l2
      d=r23(1)**2+r23(2)**2+r23(3)**2
      if(d.le.1.d-10) then
        print *, ' Error: Atoms too close to establish unique',
     >           ' directions for cartesian placement from z-matrix.'
        print *, ' Atom indicies: ',l2,l3
C       CALL TLKEMSG(' ATOMS TOO CLOSE.','I2X')
      endif
      d=1.d0/sqrt(d)
      xhat(1)=r23(1)*d
      xhat(2)=r23(2)*d
      xhat(3)=r23(3)*d
 
c     local z axis is directed along r23 cross r43, if this is non-zero
      r43(1)=xyz(1,l4)-xyz(1,l3)
      r43(2)=xyz(2,l4)-xyz(2,l3)
      r43(3)=xyz(3,l4)-xyz(3,l3)
      zhat(1)=r23(2)*r43(3)-r23(3)*r43(2)
      zhat(2)=r23(3)*r43(1)-r23(1)*r43(3)
      zhat(3)=r23(1)*r43(2)-r23(2)*r43(1)
      d=zhat(1)**2+zhat(2)**2+zhat(3)**2
c
      if(d.le.1.d-8) then
        if(abs(ang(i)).gt.1.d-3 .and.
     >     abs(abs(ang(i))-pi).gt.1.d-3) then
c         Problem: angle 4-3-2 is (nearly) linear AND ang(i) is other
c         than zero or pi.  This means placement of particle 1 is
c         undefined.  The first time this happens, we will allow it,
c         and place particle 1 in the (local) x-y plane.  This
c         corresponds to setting zhat to the lab z axis.
          if(nstrike.ne.0) then
          print *
          print *, ' A sequence of bond angles near 180 (or 0) degrees'
          print *, ' has resulted in an undefined torsion angle and'
          print *, ' this torsion angle must be defined to determine'
          print *, ' unique Cartesian coordinates for the molecule.'
          print *
          print *, ' Offending link vector:'
          print *, ' lnk(1,',i,') = ',l2
          print *, ' lnk(2,',i,') = ',l3
          print *, ' lnk(3,',i,') = ',l4
          print *
C         CALL TLKEMSG('CANNOT GENERATE CARTESIAN COORDINATES.','I2X')
          endif
          nstrike=1
        endif
        zhat(1)=zprev(1)
        zhat(2)=zprev(2)
        zhat(3)=zprev(3)
      else
        d=1.d0/sqrt(d)
        zhat(1)=zhat(1)*d
        zhat(2)=zhat(2)*d
        zhat(3)=zhat(3)*d
        zprev(1)=zhat(1)
        zprev(2)=zhat(2)
        zprev(3)=zhat(3)
      endif
 
c     local y axis is zhat cross xhat
      yhat(1)=zhat(2)*xhat(3)-zhat(3)*xhat(2)
      yhat(2)=zhat(3)*xhat(1)-zhat(1)*xhat(3)
      yhat(3)=zhat(1)*xhat(2)-zhat(2)*xhat(1)
      d=1.d0/sqrt(yhat(1)**2+yhat(2)**2+yhat(3)**2)
      yhat(1)=yhat(1)*d
      yhat(2)=yhat(2)*d
      yhat(3)=yhat(3)*d
 
      if(abs(ang(i)).gt.1.d-3 .and.
     >   abs(abs(ang(i))-pi).gt.1.d-3) nstrike=1
 
c     in molecule frame (i.e., relative to xhat,yhat,zhat coord system)
c     new center is placed at ...
      x=-rab(i)*cos(ang(i))
      rho=rab(i)*sin(ang(i))
      y=rho*cos(dih(i))
      z=rho*sin(dih(i))
 
c     in lab frame ...
      xyz(1,i) = xyz(1,l2) + x*xhat(1)+y*yhat(1)+z*zhat(1)
      xyz(2,i) = xyz(2,l2) + x*xhat(2)+y*yhat(2)+z*zhat(2)
      xyz(3,i) = xyz(3,l2) + x*xhat(3)+y*yhat(3)+z*zhat(3)
 
10    continue
 
      end


c
c     angle calculation stolen from dihint
c
      subroutine angint(xyz1,xyz2,xyz3,ang)
      Implicit Real*8 (a-h,o-z)
c     Input and output arrays
      real*8 xyz1(3),xyz2(3),xyz3(3)
      real*8 a(3),b(3)

c     Compute a=r1-r2, b=r3-r2
      do 10 k=1,3
      a(k)=xyz1(k)-xyz2(k)
10    b(k)=xyz3(k)-xyz2(k)
 
c     Compute a . b 
      dot=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
 
c     Compute lengths
      b2=b(1)**2+b(2)**2+b(3)**2
      a2=a(1)**2+a(2)**2+a(3)**2
      angcos=dot/dsqrt(a2*b2)
      ang=dacos(angcos)

      end





c Change History:
c $Log$
c*arc dihint
      subroutine dihint(iop,xyz1,xyz2,xyz3,xyz4,
     >       phi,dphidx,d2phidx2)
c
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Copyright (c) 1994 International Business Machines Corporation
C  All rights reserved
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Description      Given the coordinates of four particles, this
C                     subroutine computes the torsion (dihedral)
C                     angle between them, the Jacobean (dphi/dx(i))
C                     and d2phi/dx(i)dx(j).
C
C  Input
C
C     iop           Control
C                   iop = 1 means just compute angle
C                   iop = 2 means compute angle and Jacobean
C                   iop = 3 means compute angle, Jacobean and second
C                           derivative
C
C     xyz1(3)       cartesian coordinates of first atom
C     xyz2(3)       cartesian coordinates of second atom
C     xyz3(3)       cartesian coordinates of third atom
C     xyz4(3)       cartesian coordinates of fourth atom
C
C
C  Output
C
C     phi           Angle between atoms 1-2-3-4.  Range returned is
C                   -pi .lt. phi .le. pi, following IUPAC
C                   sign and range convention.
C
C     dphidx(12)    Jacobean: dphi/dx(1), dphi/dy(1), ...
C
C     d2phid2(45)   Second derivative, d2phi/dx(i)dx(2), a symmetric
C                   matrix, stored lower triangle by row
C
C  Linkage          List of subroutines called by this one
C
C  Programmer       William Swope (some from Carnevali)
C
C  Date             June 1994
C
C  Update log       Record of all updates including names of programmers,
C                     descriptions of changes made, and dates of change.
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
c
      Implicit Real*8 (a, b, d-h, o, p, r-y)
      Implicit Integer (i-n)
c     Implicit Logical (z)
c     Violation of ARCHEM convention; but no logical data here
      Implicit Real*8 (z)
c     Implicit Character (c)
c     Violation of ARCHEM convention; but no character data here
      Implicit Real*8 (c)
c     Implicit Undefined (q)
 
      character cproduct*48
      dimension cproduct(3)
 
c     Input and output arrays
      real*8 xyz1(3),xyz2(3),xyz3(3),xyz4(3)
      real*8 dphidx(12),d2phidx2(78)
 
 
C     THE SIGN CONVENTION FOR PHI IS AS FOLLOWS:
C     NUMBER THE ASSOCIATED ATOMS 1, 2, 3, 4.  ATOMS 1 AND 4 ARE
C     "TERMINAL" ATOMS AND THE DIHEDRAL ANGLE IS ABOUT THE BOND
C     BETWEEN ATOMS 2 AND 3.  IMAGINE HOLDING THE PLANE FORMED BY
C     ATOMS 1, 2, AND 3 FIXED AND ROTATING THE PLANE FORMED BY
C     ATOMS 2, 3, AND 4 ABOUT A VECTOR THAT POINTS FROM ATOM 2
C     TO ATOM 3.  THE POSITIVE SENSE CORRESPONDS TO THE RIGHT-HAND
C     RULE FOR ROTATION ABOUT THE VECTOR FROM ATOM 2 TO ATOM 3.
c     This agrees with the IUPAC convention.
C
C     FURTHERMORE, PHI=0 CORRESPONDS TO A CONDITION WHERE ALL FOUR
C     ATOMS ARE ON A PLANE AND ATOMS 1 AND 4 ARE "CIS" TO THE BOND
C     BETWEEN ATOMS 2 AND 3.  LIKEWISE, PHI=180 DEGREES CORRESPONDS
C     A CONDITION WHERE ALL FOUR ATOMS ARE ON A PLANE AND ATOMS 1 AND
C     4 ARE "TRANS" TO THE BOND BETWEEN ATOMS 2 AND 3.
 
 
c     Local and scratch arrays
 
      dimension a(3),b(3),c(3),u(3),v(3),acrossc(3)
 
c     First derivatives of u**2, v**2 with respect to a, b, and c
      dimension du2da(3),du2db(3),dv2db(3),dv2dc(3)
 
c     First derivatives of D=u**2*v**2 and F=D**(-1/2) with
c     respect to a, b, and c
      dimension ddda(3),dddb(3),dddc(3)
      dimension dfda(3),dfdb(3),dfdc(3)
 
c     First derivatives of uv with respect to a, b, and c
      dimension duvda(3),duvdb(3),duvdc(3)
 
c     Derivatives of S with respect to a, b, and c
      dimension dsda(3),dsdb(3),dsdc(3)
 
c     Derivatives of cosphi and sinphi with respect to a, b, and c
      dimension dcosphida(3),dcosphidb(3),dcosphidc(3)
      dimension dsinphida(3),dsinphidb(3),dsinphidc(3)
 
c     Derivatives of phi with respect to a, b, and c
      dimension dphida(3),dphidb(3),dphidc(3)
 
c     Second derivatives
      dimension du2dada(3,3),du2dbdb(3,3),du2dadb(3,3)
      dimension dv2dbdb(3,3),dv2dbdc(3,3),dv2dcdc(3,3)
      dimension dddada(3,3),dddbdb(3,3),dddcdc(3,3)
      dimension dddadb(3,3),dddadc(3,3),dddbdc(3,3)
      dimension dfdada(3,3),dfdbdb(3,3),dfdcdc(3,3)
      dimension dfdadb(3,3),dfdadc(3,3),dfdbdc(3,3)
      dimension              duvdbdb(3,3)
      dimension dsdadb(3,3),dsdadc(3,3),dsdbdc(3,3)
      dimension              dsdbdb(3,3)
      dimension duvdadb(3,3),duvdadc(3,3),duvdbdc(3,3)
      dimension dcosphidada(3,3),dcosphidbdb(3,3),dcosphidcdc(3,3)
      dimension dcosphidadb(3,3),dcosphidadc(3,3),dcosphidbdc(3,3)
      dimension dsinphidada(3,3),dsinphidbdb(3,3),dsinphidcdc(3,3)
      dimension dsinphidadb(3,3),dsinphidadc(3,3),dsinphidbdc(3,3)
      dimension dphidada(3,3),dphidbdb(3,3),dphidcdc(3,3)
      dimension dphidadb(3,3),dphidadc(3,3),dphidbdc(3,3)
 
      dimension atilde(3,3),btilde(3,3),ctilde(3,3)
 
c     identity tensor here to simplify some loops
      real*8 delta(3,3)/1.d0,3*0.d0,1.d0,3*0.d0,1.d0/
 
      data cproduct /'$RCSfile$',
     $               '$Revision$$Date$',
     $               '$IBMCSA: PRODUCT VERSION NUMBER $'/
 
c     Statement function gives index of (i,j) in lower triang by row
      lowtri(i,j) = j + i*(i-1)/2
 
      if(iop.ne.1 .and. iop.ne.2 .and. iop.ne.3) then
        print *, ' Incorrect input to dihint subroutine: iop = ',iop
c       call arcdie()
      endif
 
      ierror=0
 
      if(iop.ge.1) then
c     compute angle
 
c     Compute a=r2-r1, b=r3-r2, c=r4-r3
      do 10 k=1,3
      a(k)=xyz2(k)-xyz1(k)
      b(k)=xyz3(k)-xyz2(k)
10    c(k)=xyz4(k)-xyz3(k)
 
c     Compute u = a x b , v = b x c, vectors normal to 123, 234 planes
      u(1)=a(2)*b(3)-a(3)*b(2)
      u(2)=a(3)*b(1)-a(1)*b(3)
      u(3)=a(1)*b(2)-a(2)*b(1)
      v(1)=b(2)*c(3)-b(3)*c(2)
      v(2)=b(3)*c(1)-b(1)*c(3)
      v(3)=b(1)*c(2)-b(2)*c(1)
 
c     Compute lengths and scalar products
      b2=b(1)**2+b(2)**2+b(3)**2
      u2=u(1)**2+u(2)**2+u(3)**2
      v2=v(1)**2+v(2)**2+v(3)**2
      if( b2.eq.0.d0 )then
c       this should be a hard error; print out message and die
c       if this happens there is something seriously wrong
        print *, ' Error:  Can not compute torsion angle when central',
     >           ' bond has zero length.'
c       call arcdie('dihint')
      endif
      b1=sqrt(b2)
      u1=sqrt(u2)
      v1=sqrt(v2)
      d=u2*v2
      d1=sqrt(d)
      uv=u(1)*v(1)+u(2)*v(2)+u(3)*v(3)
      av=a(1)*v(1)+a(2)*v(2)+a(3)*v(3)
      s=b1*av
      if( u2.eq.0.d0 .or. v2.eq.0.d0 )then
        print *, ' Error:  Three collinear atoms make torsion angle',
     >           ' undefined.'
        print *, ' Zero returned for angle, gradient and hessian.'
        ierror=1
        f=0.d0
        d=1.d0
        d1=1.d0
c       Compute cosphi and sinphi
        cosphi=1.d0
        sinphi=0.d0
      else
        f=1.d0/d1
c       Compute cosphi and sinphi
        cosphi=uv*f
        sinphi=s*f
      endif
 
c     Compute phi itself, produce range of -pi.lt.phi.le.pi
      phi=atan2(sinphi,cosphi)
 
      endif
 
      if(iop.ge.2) then
c     compute dphidx
 
c     Compute derivatives of u**2 and v**2 with respect to a, b, and c.
      a2=a(1)**2+a(2)**2+a(3)**2
      c2=c(1)**2+c(2)**2+c(3)**2
      ab=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
      bc=b(1)*c(1)+b(2)*c(2)+b(3)*c(3)
      ac=a(1)*c(1)+a(2)*c(2)+a(3)*c(3)
      do 20 k=1,3
      du2da(k)=2.d0*(b2*a(k)-ab*b(k))
      du2db(k)=2.d0*(a2*b(k)-ab*a(k))
      dv2db(k)=2.d0*(c2*b(k)-bc*c(k))
20    dv2dc(k)=2.d0*(b2*c(k)-bc*b(k))
 
c     Compute derivatives of D=u**2*v**2 with respect to a, b, and c.
      do 35 k=1,3
      ddda(k)=v2*du2da(k)
      dddb(k)=v2*du2db(k)+u2*dv2db(k)
35    dddc(k)=u2*dv2dc(k)
 
c     Compute derivatives of F=D**(-1/2) with respect to a, b, and c.
      do 40 k=1,3
      dfda(k)=-0.5d0*ddda(k)/(d*d1)
      dfdb(k)=-0.5d0*dddb(k)/(d*d1)
40    dfdc(k)=-0.5d0*dddc(k)/(d*d1)
 
 
 
      if(abs(sinphi).gt.abs(cosphi))then
c     if(.false.)then
c*************************************************
c     CASE A - use expressions derived from cosphi
c     Compute derivatives of u*v=C with respect to a, b, and c.
      do 50 k=1,3
      duvda(k)=b(k)*bc-c(k)*b2
      duvdb(k)=a(k)*bc+c(k)*ab-2.d0*b(k)*ac
50    duvdc(k)=b(k)*ab-a(k)*b2
c     Compute derivatives of cosphi=(u*v)*F=C*F with respect to a, b, and c.
      do 60 k=1,3
      dcosphida(k)=duvda(k)*f+uv*dfda(k)
      dcosphidb(k)=duvdb(k)*f+uv*dfdb(k)
60    dcosphidc(k)=duvdc(k)*f+uv*dfdc(k)
c     Compute derivatives of phi with respect to a, b, and c.
      do 70 k=1,3
      dphida(k)=-dcosphida(k)/sinphi
      dphidb(k)=-dcosphidb(k)/sinphi
70    dphidc(k)=-dcosphidc(k)/sinphi
c*************************************************
      else
c*************************************************
c     CASE B - use expressions derived from sinphi
      acrossc(1)=a(2)*c(3)-a(3)*c(2)
      acrossc(2)=a(3)*c(1)-a(1)*c(3)
      acrossc(3)=a(1)*c(2)-a(2)*c(1)
      s=b1*av
c     Compute derivatives of S=b(a*v) with respect to a, b, and c.
      do 85 k=1,3
      dsda(k)=b1*v(k)
      dsdb(k)=b(k)*av/b1-b1*acrossc(k)
85    dsdc(k)=b1*u(k)
c     Compute derivatives of sinphi with respect to a, b, and c.
      do 90 k=1,3
      dsinphida(k)=dsda(k)*f+s*dfda(k)
      dsinphidb(k)=dsdb(k)*f+s*dfdb(k)
90    dsinphidc(k)=dsdc(k)*f+s*dfdc(k)
c     Compute derivatives of phi with respect to a, b, and c.
      do 95 k=1,3
      dphida(k)=dsinphida(k)/cosphi
      dphidb(k)=dsinphidb(k)/cosphi
95    dphidc(k)=dsinphidc(k)/cosphi
c*************************************************
 
      endif
 
c     Add contribution to dphi/dx array
      do 80 k=1,3
      dphidx(k  )=          -dphida(k)
      dphidx(3+k)= dphida(k)-dphidb(k)
      dphidx(6+k)= dphidb(k)-dphidc(k)
80    dphidx(9+k)= dphidc(k)
 
c     This code is for three atoms collinear; torsion undefined
      if(ierror.eq.1) then
        do 81 k=1,12
81      dphidx(k)=0.d0
      endif
 
      endif
 
      if(iop.ge.3) then
 
c     Compute second derivatives of u**2 and v**2 with respect to a, b, and c.
      do 110 i=1,3
      do 110 j=1,3
      du2dada(i,j)=2.d0*(b2*delta(i,j)-b(i)*b(j))
      du2dbdb(i,j)=2.d0*(a2*delta(i,j)-a(i)*a(j))
      du2dadb(i,j)=2.d0*(2.d0*a(i)*b(j)-b(i)*a(j)-ab*delta(i,j))
      dv2dbdb(i,j)=2.d0*(c2*delta(i,j)-c(i)*c(j))
      dv2dcdc(i,j)=2.d0*(b2*delta(i,j)-b(i)*b(j))
      dv2dbdc(i,j)=2.d0*(2.d0*b(i)*c(j)-c(i)*b(j)-bc*delta(i,j))
110   continue
 
c     Compute second derivatives of D=u**2*v**2 with respect to a, b, and c.
      do 120 i=1,3
      do 120 j=1,3
      dddada(i,j)=
     >            v2*du2dada(i,j)
      dddbdb(i,j)=dv2db(i)*du2db(j)+du2db(i)*dv2db(j)
     >           +v2*du2dbdb(i,j)  +u2*dv2dbdb(i,j)
      dddcdc(i,j)=
     >                             +u2*dv2dcdc(i,j)
      dddadb(i,j)=                 +du2da(i)*dv2db(j)
     >           +v2*du2dadb(i,j)
      dddadc(i,j)=                 +du2da(i)*dv2dc(j)
     >
      dddbdc(i,j)=                 +du2db(i)*dv2dc(j)
     >                             +u2*dv2dbdc(i,j)
120   continue
 
c     Compute second derivatives of F=D**(-1/2) with respect to a, b, and c.
      f3=f/d
      do 130 i=1,3
      do 130 j=1,3
      dfdada(i,j)=.5d0*f3*(1.5d0*ddda(i)*ddda(j)/d-dddada(i,j))
      dfdbdb(i,j)=.5d0*f3*(1.5d0*dddb(i)*dddb(j)/d-dddbdb(i,j))
      dfdcdc(i,j)=.5d0*f3*(1.5d0*dddc(i)*dddc(j)/d-dddcdc(i,j))
      dfdadb(i,j)=.5d0*f3*(1.5d0*ddda(i)*dddb(j)/d-dddadb(i,j))
      dfdadc(i,j)=.5d0*f3*(1.5d0*ddda(i)*dddc(j)/d-dddadc(i,j))
      dfdbdc(i,j)=.5d0*f3*(1.5d0*dddb(i)*dddc(j)/d-dddbdc(i,j))
130   continue
 
      if(abs(sinphi).gt.abs(cosphi))then
c     if(.false.)then
c*************************************************
c     CASE A - use expressions derived from cosphi
c     Compute second derivatives of u*v=C with respect to a, b, and c.
      do 140 i=1,3
      do 140 j=1,3
      duvdbdb(i,j)=a(i)*c(j)+c(i)*a(j)-2.d0*ac*delta(i,j)
      duvdadb(i,j)=bc*delta(i,j)+b(i)*c(j)-2.d0*c(i)*b(j)
      duvdadc(i,j)=b(i)*b(j)-b2*delta(i,j)
      duvdbdc(i,j)=ab*delta(i,j)+a(i)*b(j)-2.d0*b(i)*a(j)
140   continue
c     Compute second derivatives of cosphi=(u*v)*F=C*F with respect to a, b, and
c.
      do 150 i=1,3
      do 150 j=1,3
      dcosphidada(i,j)=
     >                +dfda(i)*duvda(j)+duvda(i)*dfda(j)+uv*dfdada(i,j)
      dcosphidbdb(i,j)=
     >  duvdbdb(i,j)*f+dfdb(i)*duvdb(j)+duvdb(i)*dfdb(j)+uv*dfdbdb(i,j)
      dcosphidcdc(i,j)=
     >                +dfdc(i)*duvdc(j)+duvdc(i)*dfdc(j)+uv*dfdcdc(i,j)
      dcosphidadb(i,j)=
     >  duvdadb(i,j)*f+dfda(i)*duvdb(j)+duvda(i)*dfdb(j)+uv*dfdadb(i,j)
      dcosphidadc(i,j)=
     >  duvdadc(i,j)*f+dfda(i)*duvdc(j)+duvda(i)*dfdc(j)+uv*dfdadc(i,j)
      dcosphidbdc(i,j)=
     >  duvdbdc(i,j)*f+dfdb(i)*duvdc(j)+duvdb(i)*dfdc(j)+uv*dfdbdc(i,j)
150   continue
c     Compute second derivatives of phi with respect to a, b, and c.
      z1=-cosphi/(sinphi**3)
      z2=-1.d0/sinphi
      do 160 i=1,3
      do 160 j=1,3
      dphidada(i,j)=z1*dcosphida(i)*dcosphida(j)+z2*dcosphidada(i,j)
      dphidbdb(i,j)=z1*dcosphidb(i)*dcosphidb(j)+z2*dcosphidbdb(i,j)
      dphidcdc(i,j)=z1*dcosphidc(i)*dcosphidc(j)+z2*dcosphidcdc(i,j)
      dphidadb(i,j)=z1*dcosphida(i)*dcosphidb(j)+z2*dcosphidadb(i,j)
c     note dcosphidadc is non zero but dphidadc is zero - don't compute
      dphidadc(i,j)=z1*dcosphida(i)*dcosphidc(j)+z2*dcosphidadc(i,j)
      dphidbdc(i,j)=z1*dcosphidb(i)*dcosphidc(j)+z2*dcosphidbdc(i,j)
160   continue
      else
c*************************************************
c     CASE B - use expressions derived from sinphi
c     Compute the product of the antisymmetric tensor times a, b, and c
      call antsym(a,atilde)
      call antsym(b,btilde)
      call antsym(c,ctilde)
c     Compute second derivatives of S=b(a*v) with respect to a, b, and c.
      b3=b1*b2
      do 210 i=1,3
      do 210 j=1,3
      dsdbdb(i,j)=(delta(i,j)/b1-b(i)*b(j)/b3)*av
     >            -(acrossc(i)*b(j)+acrossc(j)*b(i))/b1
      dsdadb(i,j)=v(i)*b(j)/b1+b1*ctilde(i,j)
      dsdadc(i,j)=-b1*btilde(i,j)
      dsdbdc(i,j)=b(i)*u(j)/b1+b1*atilde(i,j)
210   continue
c     Compute second derivatives of sinphi with respect to a, b, and c.
      do 220 i=1,3
      do 220 j=1,3
      dsinphidada(i,j)=
     >                +dfda(i)*dsda(j)+dsda(i)*dfda(j)+s*dfdada(i,j)
      dsinphidbdb(i,j)=
     >  dsdbdb(i,j)*f+dfdb(i)*dsdb(j)+dsdb(i)*dfdb(j)+s*dfdbdb(i,j)
      dsinphidcdc(i,j)=
     >                +dfdc(i)*dsdc(j)+dsdc(i)*dfdc(j)+s*dfdcdc(i,j)
      dsinphidadb(i,j)=
     >  dsdadb(i,j)*f+dfda(i)*dsdb(j)+dsda(i)*dfdb(j)+s*dfdadb(i,j)
      dsinphidadc(i,j)=
     >  dsdadc(i,j)*f+dfda(i)*dsdc(j)+dsda(i)*dfdc(j)+s*dfdadc(i,j)
      dsinphidbdc(i,j)=
     >  dsdbdc(i,j)*f+dfdb(i)*dsdc(j)+dsdb(i)*dfdc(j)+s*dfdbdc(i,j)
220   continue
c     Compute second derivatives of phi with respect to a, b, and c.
      z1=sinphi/cosphi**3
      z2=1.d0/cosphi
      do 230 i=1,3
      do 230 j=1,3
      dphidada(i,j)=z1*dsinphida(i)*dsinphida(j)+z2*dsinphidada(i,j)
      dphidbdb(i,j)=z1*dsinphidb(i)*dsinphidb(j)+z2*dsinphidbdb(i,j)
      dphidcdc(i,j)=z1*dsinphidc(i)*dsinphidc(j)+z2*dsinphidcdc(i,j)
      dphidadb(i,j)=z1*dsinphida(i)*dsinphidb(j)+z2*dsinphidadb(i,j)
c     note dsinphidadc is non zero but dphidadc is zero - don't compute
      dphidadc(i,j)=z1*dsinphida(i)*dsinphidc(j)+z2*dsinphidadc(i,j)
      dphidbdc(i,j)=z1*dsinphidb(i)*dsinphidc(j)+z2*dsinphidbdc(i,j)
230   continue
      end if
 
c     Add contributions to second derivative matrix
c     Derivative with respect to r1,r1:
      do 21 i=1,3
      do 21 j=1,i
      index=lowtri(i,j)
      d2phidx2(index)=dphidada(i,j)
21    continue
c     Derivative with respect to r2,r2:
      do 22 i=1,3
      do 22 j=1,i
      index=lowtri(i+3,j+3)
      d2phidx2(index)=dphidada(i,j)+dphidbdb(i,j)
     >               -dphidadb(i,j)-dphidadb(j,i)
22    continue
c     Derivative with respect to r3,r3:
      do 23 i=1,3
      do 23 j=1,i
      index=lowtri(i+6,j+6)
      d2phidx2(index)=dphidbdb(i,j)+dphidcdc(i,j)
     >               -dphidbdc(i,j)-dphidbdc(j,i)
23    continue
c     Derivative with respect to r4,r4:
      do 24 i=1,3
      do 24 j=1,i
      index=lowtri(i+9,j+9)
      d2phidx2(index)=dphidcdc(i,j)
24    continue
c     Derivative with respect to r1,r2:
      do 25 i=1,3
      do 25 j=1,3
      index=lowtri(j+3,i)
      d2phidx2(index)=-dphidada(i,j)+dphidadb(i,j)
25    continue
c     Derivative with respect to r1,r3:
      do 26 i=1,3
      do 26 j=1,3
      index=lowtri(j+6,i)
      d2phidx2(index)=-dphidadb(i,j)+dphidadc(i,j)
26    continue
c     Derivative with respect to r1,r4:
      do 27 i=1,3
      do 27 j=1,3
      index=lowtri(j+9,i)
      d2phidx2(index)=-dphidadc(i,j)
c
c     note: dphidadc is always zero - this array and its computation
c     should be removed later
c
c      write(*,887) i,j,index,dphidadc(i,j)
c887   format(' r1,r4 block: i,j=',2i2,' index=',i4,' dphidadc=',f10.4)
27    continue
c     Derivative with respect to r2,r3:
      do 28 i=1,3
      do 28 j=1,3
      index=lowtri(j+6,i+3)
      d2phidx2(index)=
     >               +dphidadb(i,j)-dphidadc(i,j)
     >               -dphidbdb(i,j)+dphidbdc(i,j)
28    continue
c     Derivative with respect to r2,r4:
      do 29 i=1,3
      do 29 j=1,3
      index=lowtri(j+9,i+3)
      d2phidx2(index)=dphidadc(i,j)-dphidbdc(i,j)
29    continue
c     Derivative with respect to r3,r4:
      do 30 i=1,3
      do 30 j=1,3
      index=lowtri(j+9,i+6)
      d2phidx2(index)=dphidbdc(i,j)-dphidcdc(i,j)
30    continue
 
c     code for three atoms colinear
      if(ierror.eq.1) then
        do 45 i=1,78
45      d2phidx2(i)=0.d0
      endif
 
      endif
 
 
      end
c
c     Subroutine to compute the product of the antysimmetric
c     tensor times a vector. The result is an antisymmetric tensor.
      subroutine antsym(x,xtilde)
      implicit real*8 (a-h,o-z)
      dimension x(3),xtilde(3,3)
      xtilde(1,1)=0.d0
      xtilde(2,2)=0.d0
      xtilde(3,3)=0.d0
      xtilde(1,2)=x(3)
      xtilde(2,3)=x(1)
      xtilde(3,1)=x(2)
      xtilde(2,1)=-x(3)
      xtilde(3,2)=-x(1)
      xtilde(1,3)=-x(2)
      return
      end








c
c     subroutine to write out solvent information files for lammps simulation
c
      subroutine wrtlmps1(nwat,coord,xlow,xhigh)
      implicit real*8 (a-h,o-z)
      dimension coord(3,*)
      dimension fmass(2),vdw(2,2),bondpar(2,2),anglepar(2,2),charge(2)


      oxmass=15.9994d0
      hymass=1.008d0
      fmass(1)=oxmass
      fmass(2)=hymass
      vdw(1,1)=0.16275d0  !epsilon o-o
      vdw(2,1)=3.16435d0  !sigma   o-o
      vdw(1,2)=0.0d0      !epsilon h-h
      vdw(2,2)=0.0d0      !sigma   h-h
      bondpar(1,1)=250.d0 !spring constant (doesn't matter)
      bondpar(2,1)=0.9572d0 !OH bond length (constrained by shake)
      anglepar(1,1)=250.d0 !spring constant (doesn't matter)
      anglepar(2,1)=104.52d0 !HOH angle (constrained)
      charge(1)=-1.0484d0  !O charge TI4P-Ew
      charge(2)= 0.5242d0  !H charge TI4P-Ew
      
c     TIP4P-Ew
      nsites=3*nwat 
      nbonds=2*nwat
      nangles=nwat
      ndihed=0
      nadded=0


      write(15,'('' Header line '')')
      write(15,*)
      write(15,'(5x,i10,'' atoms '')') nsites
      write(15,'(5x,i10,'' bonds '')') nbonds
      write(15,'(5x,i10,'' angles '')') nangles
      write(15,'(5x,i10,'' dihedrals '')') ndihed + nadded

c     TIP4P-Ew
      nattp=2
      nbntp=1
      nantp=1
      nditp=0

      write(15,*)
      write(15,'(5x,i10,'' atom types '')') nattp
      write(15,'(5x,i10,'' bond types '')') nbntp
      write(15,'(5x,i10,'' angle types '')') nantp
      write(15,'(5x,i10,'' dihedral types '')') nditp


c     cell edge is xlow to xhigh
      write(15,*)
      write(15,'(5x,2f15.8,'' xlo xhi '')') xlow,xhigh
      write(15,'(5x,2f15.8,'' ylo yhi '')') xlow,xhigh
      write(15,'(5x,2f15.8,'' zlo zhi '')') xlow,xhigh

c     TIP4P-Ew
      write(15,*)
      write(15,'('' Masses  '')')
      write(15,*)
      write(15,'(i10,f15.8,''    # O - TIP4P-Ew'')') 1,fmass(1) 
      write(15,'(i10,f15.8,''    # H - TIP4P-Ew'')') 2,fmass(2) 


c     TIP4P-Ew
      write(15,*)
      write(15,'('' Pair Coeffs'')')
      write(15,*)
      write(15,'(i10,4f15.8,''   # O - TIP4P-Ew'')') 1,
     x    vdw(1,1),vdw(2,1), !epsilon,sigma
     x    vdw(1,1),vdw(2,1)  !epsilon,sigma
      write(15,'(i10,4f15.8,''   # H - TIP4P-Ew'')') 2,
     x    vdw(1,2),vdw(2,2), !epsilon,sigma
     x    vdw(1,2),vdw(2,2)  !epsilon,sigma


      write(15,*)
      write(15,'('' Bond Coeffs'')')
      write(15,*)
      write(15,'(i10,2f15.8,
     x  ''    # OH bond TIP4P-Ew'')') 1,
     x  bondpar(1,1),bondpar(2,1)


      write(15,*)
      write(15,'('' Angle Coeffs'')')
      write(15,*)
      write(15,'(i10,2f15.8,
     x  ''    # HOH angle TIP4P-Ew'')') 1,
     x  anglepar(1,1),anglepar(2,1)


c     write(15,*)
c     write(15,'('' Dihedral Coeffs'')')
c     write(15,*)


      write(15,*)
      write(15,'('' Atoms '')')
      write(15,*)
      do i=1,nwat
c     fix up the coordinates by removing the fictional sites
c     this assumes the sites are ordered O,H,H,Q
        iat=4*(i-1)
        write(15,'(i10,i10,i5,f15.10,3f15.8,''    # O '')') 
     x    3*(i-1)+1,i,1,charge(1),(coord(k,iat+1),k=1,3)
        write(15,'(i10,i10,i5,f15.10,3f15.8,''    # H1'')') 
     x    3*(i-1)+2,i,2,charge(2),(coord(k,iat+2),k=1,3)
        write(15,'(i10,i10,i5,f15.10,3f15.8,''    # H2'')') 
     x    3*(i-1)+3,i,2,charge(2),(coord(k,iat+3),k=1,3)
      enddo


      write(15,*)
      write(15,'('' Velocities '')')
      write(15,*)
      do i=1,nsites
        write(15,'(i10,5x,3f10.5)') i,0.,0.,0.
      enddo


      write(15,*)
      write(15,'('' Bonds  '')')
      write(15,*)
      do i=1,nbonds,2
        iwat=1+(i-1)/2
        ij1=3*(iwat-1)+1
        write(15,'(i10,3i10)') i,  1,ij1,ij1+1
        write(15,'(i10,3i10)') i+1,1,ij1,ij1+2
      enddo


      write(15,*)
      write(15,'('' Angles '')')
      write(15,*)
      do i=1,nangles
        iwat=i
        iat=3*(iwat-1)+2
        jat=3*(iwat-1)+1
        kat=3*(iwat-1)+3
        write(15,'(i10,4i10)') i,1,iat,jat,kat
      enddo


c     write(15,*)
c     write(15,'('' Dihedrals '')')
c     write(15,*)
c     do i=1,ndihed +nadded
c       write(15,'(i10,5i10)') i,ijkldih(6,i),
c    x    ijkldih(1,i),ijkldih(2,i),ijkldih(3,i),ijkldih(4,i)
c     enddo

      write(15,*)

      close(15)


      end


c
c     left justify a string
c
      subroutine leftjust(string)
      character*(*) string

      is=0
      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 10
        endif
      enddo

10    continue
      if(is.eq.0) return  !all blank string

      do i=1,len(string)-is+1
        string(i:i)=string(is+i-1:is+i-1)
      enddo
c     blank out the last is-1 characters
      do i=len(string)-is+2,len(string)
        string(i:i)=' '
      enddo

      end


c
c     subroutine lstchr returns the index of the last nonblank character in a string
c
      subroutine lstchr(string,ilst)
      character*(*) string 

      ilst=0  !if string is all blank, return zero
      do i=len(string),1,-1
        if(string(i:i).ne.' ') then
          ilst=i
          go to 10
        endif
      enddo
10    continue

      end



c
c     subroutine to parse a string into an element field and up to 3 integers
c     unused fields will be returned with values of -1
c
      subroutine parser(string,elfield,lelfield,i1,i2,i3)
      character*(*) string,elfield
      character*120 line
      character*10 numbers
      dimension is(3)
      data numbers/'1234567890'/

      do i=1,3
        is(i)=-1
      enddo
      line=string
      call leftjust(line)
      ne=2 !assume two character element name
      ifnum=index(numbers,line(2:2))
      if(line(2:2).eq.' '.or.line(2:2).eq.'_'.or.ifnum.ne.0) ne=1
      lelfield=ne
      elfield=line(1:ne)

      do i=1,3
        if(line(ne+1:ne+1).eq.'_') then
c         assume there is a number field after the underscore
c         look for next space or underscore
          l=index(line(ne+2:),'_')
          if(l.gt.0) then
            read(line(ne+2:ne+l),*) is(i)
            ne=ne+l
          else
            read(line(ne+2:),*) is(i)
            go to 10
          endif
        endif
      enddo

10    continue

      i1=is(1)
      i2=is(2)
      i3=is(3)

      end

c
c     function to get atomic number given a string
c     holding the element name
c


      integer function igetatnum(string)
      character*(*) string
      character*26 upper,lower
      character*10 numbers
      character*80 elements
      character*2 ename
      
      upper='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
      lower='abcdefghijklmnopqrstuvwxyz'
      numbers='0123456789'
      elements(1:20)= 'H HELIBEB C N O F NE' 
      elements(21:40)='NAMGALSIP S CLARK CA'
      elements(41:60)='SCTIV CRMNFECONICUZN'
      elements(61:80)='GAGEASSEBRKRRBSRY ZR'

      igetatnum=o

      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 100
        endif
      enddo
100   continue
      ie=len(string)
      do i=is+1,len(string)
        ifnum=index(numbers,string(i:i))
        if(string(i:i).eq.'_'.or.string(i:i).eq.' '.or.
     x    ifnum.ne.0) then
          ie=i-1
          go to 200
        endif
      enddo
200   continue

      ename(1:2)=string(is:ie)  !pads with blanks
c     write(*,'('' ENAME =*'',a2,''*'')') ename

c     element string is in string(is:ie)
      do i=1,2  
        ix=index(lower,ename(i:i))
        if(ix.ne.0) ename(i:i)=upper(ix:ix)
      enddo
c     write(*,'('' ENAME =*'',a2,''*'')') ename

      ix=index(elements,ename(1:2))
      if(ix.eq.0) then
c       write(*,'('' WARNING:  Element not found: '',a2)') ename
c       stop 'ERROR:  missing element in igetatnum'
        igetatnum=0
      else
        igetatnum=1+(ix-1)/2
      endif
    

      end
c
c     subroutine to populate the atomic masses array
c
      subroutine amassinit(amass,n)
      implicit real*8 (a-h,o-z)
      dimension amass(n)

c     atomic masses as needed
      do i=1,120
        amass(i)=0.d0
      enddo

      amass(1)=1.00794d0   !agrees w/Mulliken
      amass(2)=4.002602d0
      amass(3)=6.941d0
      amass(4)=9.012182d0
      amass(5)=10.811d0

      amass(6)=12.0107d0
      amass(6)=12.0110d0    !agrees w/Mulliken
      amass(7)=14.0067d0
      amass(8)=15.9994d0    !agrees w/Mulliken
      amass(9)=18.9984032d0
      amass(10)=20.1797d0

      amass(11)=22.98976928d0
      amass(12)=24.305d0
      amass(13)=26.9815386d0
      amass(14)=28.0855d0
      amass(15)=30.973762d0

      amass(16)=32.065d0
      amass(17)=35.453d0
      amass(18)=39.948d0
      amass(19)=39.0983d0
      amass(20)=40.078d0

      amass(21)=44.955912d0
      amass(22)=47.867d0
      amass(23)=50.9415d0
      amass(24)=51.9961d0
      amass(25)=54.938045d0

      amass(26)=55.845d0
      amass(27)=58.933195d0
      amass(28)=58.6934d0
      amass(29)=63.546d0
      amass(30)=65.38d0

      amass(31)=69.723d0
      amass(32)=72.64d0
      amass(33)=74.9216d0
      amass(34)=78.96d0
      amass(35)=79.904d0
      amass(36)=83.798d0

      end 



C
C     MACHINE INDEPENDENT (ALMOST) RANDOM NUMBER GENERATOR
C     SLOPPILY CONVERTED TO DOUBLE PRECISION.
C     LOW 32 BITS OF FLOATING POINT RANDOM NUMBER ARE ZERO.
C     I.E., THEY ARE ONLY SINGLE PRECISION RANDOM ASSIGNED TO DOUBLES.
C
      DOUBLE PRECISION FUNCTION RAN(IX)
      INTEGER*4 IX,I,I1,I2,I3,I4,I5,I6,I7,I8,I9
      DATA I2/16807/,I4/32768/,I5/65536/,I3/2147483647/
      I6=IX/I5
      I7=(IX-I6*I5)*I2
      I8=I7/I5
      I9=I6*I2+I8
      I1=I9/I4
      IX=(((I7-I8*I5)-I3)+(I9-I1*I4)*I5)+I1
      IF(IX.LT.0) IX=IX+I3
      RAN=(4.656613E-10)*FLOAT(IX)
      RETURN
      END

