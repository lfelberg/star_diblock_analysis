c
c     program to evaluate energy given a lmp input file
c
      implicit real*8 (a-h,o-z)
      character*120 line,elfield,fname1,fname2,fnamexyz
      dimension coord(3,150000),molecule(150000),iresidue(150000)
      dimension charge(150000),itype(150000)
      character*80 atname(150000)
      dimension ijbond(3,200000),bndpar(2,5000)
      dimension ijkangle(4,50000),angpar(2,5000)
      dimension ijkldih(5,8000),dihpar(4,5000)
      dimension vdw(4,100)
      dimension mpointer(4,200000),irpointer(2,200000)
      dimension ioption(10),buffer(2,1000)

      dimension neigh(5000,5)
      dimension ipair14(2,8000)

      natmx=150000  !max number of atomic sites (nsites)
      nbnmx=200000  !max number of bonds 
      nbtmx=5000    !max number of bond types 
      nangmx=50000  !max number of angles
      nangtmx=5000  !max number of angle types 
      ndihmx=8000   !max number of dihedrals
      ndihtmx=5000  !max number of dihedral types 
      nmolmx=200000 !max number of molecules


      fname1='/jg0/locals/STAR23/350Kt/star23tol.lmp'
      fnamexyz='/jg0/locals/STAR23/350Kt/STAR23_350Kt.40.xyz'
      ifxyz=1  !set to one to replace lmp coordinates with ones from the *.xyz file

      do i=1,10
        ioption(i)=0
      enddo
      ioption(1)=1   !bond flag
      ioption(2)=0   !angle flag
      ioption(3)=0   !dihedral flag
      ioption(4)=0   !flag

      open(10,file=fname1)


c     read number of atoms and their coordinates
10    continue
      read(10,'(a120)',end=200) line
      if(index(line,'atoms').eq.0) go to 10
      read(line,*) nsites
      write(*,'(''Number of atoms (nsites) = '',i8)') nsites
      if(nsites.gt.natmx) then
        write(*,'(''Not enough memory for atomic sites '',
     x    ''increase natmx.  Need enough storage for '',i6)') 
     x    nsites
        stop 'Need more storage for sites (natmx)'
      endif

20    continue
      read(10,'(a120)',end=200) line
      if(index(line,'Atoms').eq.0) go to 20
      read(10,'(a120)') line
      totchg=0.d0
      write(*,'(''Reading coordinates'')')
      do i=1,nsites
        read(10,'(a120)') line
        read(line,*) ix,im,itype(ix),charge(ix),
     x    (coord(k,ix),k=1,3)
        molecule(ix)=im
c       write(*,'('' Reading coordinates: '',i5)') ix
        iloc=index(line,'#')
        atname(ix)=' '
        if(iloc.ne.0) then
          is2=len(line)
          call leftjust(line(iloc+1:is2))
          call lstchr(line(iloc+1:is2),is1)
          read(line(iloc+1:iloc+is1),'(a)') atname(ix)(1:is1)
        else
          write(atname(ix)(1:6),'(''X'',i5.5)') ix
        endif
c       call leftjust(atname(ix))
        call parser(atname(ix),elfield,lelfield,i1,i2,i3)
        iresidue(ix)=i3
        totchg=totchg+charge(ix)
      enddo
      write(*,'(''Total charge '',f10.5)') totchg



      if(ifxyz.eq.1) then
        call getcoord(fnamexyz,nsites,itype,coord)
        is=index(fnamexyz,'.xyz')
        open(20,file=fnamexyz(1:is)//'vol')
188     continue  
        read(20,'(i20,f20.10)',end=189) istep,volume
        go to 188
189     continue  
        close(20)
        edge=volume**(1.d0/3.d0)
        write(*,'(''Final volume from *.vol file: '',
     x    f20.10,'' edge: '',f20.10)') volume,edge
      endif






      if(ioption(1).eq.1) then
c     read bond
      write(*,'(''Reading bonds'')')
      rewind(10)
30    continue
      read(10,'(a120)',end=200) line
      if(index(line,' bonds ').eq.0) go to 30
      read(line,*) nbonds
      if(nbonds.gt.nbnmx) then
        write(*,'(''Not enough memory for bond list '',
     x    ''increase nbnmx.  Need enough storage for '',i6)') 
     x    nbonds
        stop 'Need more storage for bonds'
      endif
40    continue
      read(10,'(a120)',end=200) line
      if(index(line,' bond types ').eq.0) go to 40
      read(line,*) nbtp
      if(nbtp.gt.nbtmx) then
        write(*,'(''Not enough memory for bond types list '',
     x    ''increase nbtmx.  Need enough storage for '',i6)') 
     x    nbtp
        stop 'Need more storage for bonds'
      endif



45    continue
      read(10,'(a120)',end=200) line
      if(index(line,'Bond Coeffs').eq.0) go to 45
      read(10,'(a120)') line
      do i=1,nbtp  
        read(10,'(a120)') line
        read(line,*) ix,bndpar(1,i),bndpar(2,i)
      enddo
50    continue
      read(10,'(a120)',end=200) line
      if(index(line,'Bonds').eq.0) go to 50
      read(10,'(a120)') line
      do i=1,nbonds
        read(10,'(a120)') line
        read(line,*) ix,ijbond(3,i),ijbond(1,i),ijbond(2,i)
      enddo

      call bengy(nsites,coord,nbonds,ijbond,nbtp,bndpar,edge)
      endif   !end of ifblock for bonds




      if(ioption(2).eq.1) then
c     read angles
      write(*,'(''Reading angles'')')
      rewind(10)
55    continue
      read(10,'(a120)',end=200) line
      if(index(line,' angles').eq.0) go to 55
      read(line,*) nangle
      if(nangle.gt.nangmx) then
        write(*,'(''Not enough memory for angle list '',
     x    ''increase nangmx.  Need enough storage for '',i6)') 
     x    nangle
        stop 'Need more storage for angles'
      endif
60    continue
      read(10,'(a120)',end=200) line
      if(index(line,' angle types ').eq.0) go to 60
      read(line,*) natp
      if(natp.gt.nangtmx) then
        write(*,'(''Not enough memory for angle type list '',
     x    ''increase nangtmx.  Need enough storage for '',i6)') 
     x    natp
        stop 'Need more storage for bonds'
      endif
65    continue
      read(10,'(a120)',end=200) line
      if(index(line,'Angle Coeffs').eq.0) go to 65
      read(10,'(a120)') line
      do i=1,natp  
        read(10,'(a120)') line
        read(line,*) ix,angpar(1,i),angpar(2,i)
      enddo
70    continue
      read(10,'(a120)',end=200) line
      if(index(line,'Angles').eq.0) go to 70
      read(10,'(a120)') line
      do i=1,nangle
        read(10,'(a120)') line
        read(line,*) ix,ijkangle(4,i),(ijkangle(k,i),k=1,3)
      enddo

      call aengy(nsites,coord,nangle,ijkangle,natp,angpar)
      endif   !end of ifblock for angles




      if(ioption(3).eq.1) then
c     read dihedrals
      write(*,'(''Reading dihedrals'')')
      rewind(10)
75    continue
      read(10,'(a120)',end=200) line
      if(index(line,' dihedrals').eq.0) go to 75
      read(line,*) ndih  
      if(ndih.gt.ndihmx) then
        write(*,'(''Not enough memory for dihedral list '',
     x    ''increase ndihmx.  Need enough storage for '',i6)') 
     x    ndih
        stop 'Need more storage for dihedral angles'
      endif
80    continue
      read(10,'(a120)',end=200) line
      if(index(line,' dihedral types ').eq.0) go to 80
      read(line,*) ndtp
c     write(*,'('' Dihedrals: '',i6,''  Types: '',i6)')
c    x   ndih,ndtp
      if(ndtp.gt.ndihtmx) then
        write(*,'(''Not enough memory for dihedral type list '',
     x    ''increase ndihtmx.  Need enough storage for '',i6)') 
     x    ndtp
        stop 'Need more storage for dihedral type list'
      endif
85    continue
      read(10,'(a120)',end=200) line
      if(index(line,'Dihedral Coeffs').eq.0) go to 85
      read(10,'(a120)') line
      do i=1,ndtp  
        read(10,'(a120)') line
c       write(*,'('' Line: *'',a,''*'')') line(1:50)
        read(line,*) ix,dihpar(1,i),nd,dd,dihpar(4,i)
c       write(*,'('' '',i5,f10.4,i5,i5,f10.4)') 
c    x    is,dihpar(1,i),nd,dd,dihpar(4,i)
        dihpar(2,i)=nd         
        dihpar(3,i)=dd         
      enddo
c     write(*,'('' Types read in '')')
90    continue
      read(10,'(a120)',end=200) line
      if(index(line,'Dihedrals').eq.0) go to 90
      read(10,'(a120)') line
      do i=1,ndih  
        read(10,'(a120)') line
        read(line,*) ix,ijkldih(5,i),(ijkldih(k,i),k=1,4)
      enddo
c     write(*,'('' Dihedrals read in '')')


      call dengy(nsites,coord,ndih,ijkldih,ndtp,dihpar)
      endif !end of ifblock for dihedrals




c     read vdw
      rewind(10)
100   continue
      read(10,'(a120)',end=200) line
      if(index(line,' atom types ').eq.0) go to 100
      read(line,*) natomtp
105   continue
      read(10,'(a120)',end=200) line
      if(index(line,'Pair Coeffs').eq.0) go to 105
      read(10,'(a120)') line
      do i=1,natomtp  
        read(10,'(a120)') line
        read(line,*) ix,vdw(2,i),vdw(1,i),vdw(4,i),vdw(3,i)
        write(*,'('' Atom type '',i3,'' vdw = '',2f10.5)')
     x     i,vdw(1,i),vdw(2,i)
      enddo

c
c     write out a table to implement geometric combining rules
c
c     do i=1,natomtp-1
c       do j=i+1,natomtp
c         write(*,'(''pair_coeff     '',2i5,2f15.8)')
c    x    i,j,dsqrt(vdw(2,i)*vdw(2,j)),
c    x        dsqrt(vdw(1,i)*vdw(1,j))
c       enddo
c     enddo


c
c     find out how many molecules and the index of first and last site of each
c
      nmol=molecule(nsites)
      if(nmol.gt.nmolmx) then
        write(*,'(''Not enough memory for molecule pointer list '',
     x    ''increase nmolmx.  Need enough storage for '',i6)') 
     x    nmol 
        stop 'Need more storage for molecule pointer list'
      endif
      write(*,'(''Number of molecules '',i6)') nmol
      imol=1
      mpointer(1,imol)=1
      do i=1,nsites
        if(molecule(i).ne.imol) then
          mpointer(2,imol)=i-1
          imol=molecule(i)  !new current molecule index
          mpointer(1,imol)=i
        endif
      enddo
      mpointer(2,nmol)=nsites

      do imol=1,20
        call lstchr(atname(mpointer(1,imol)),is1)
        call lstchr(atname(mpointer(2,imol)),is2)
        write(*,'(''Molecule '',i5,
     x    '' first site index is '',i5,'' name '',a,
     x    '' last site index is '',i5,'' name '',a)')
     x    imol,
     x    mpointer(1,imol),atname(mpointer(1,imol))(1:is1),
     x    mpointer(2,imol),atname(mpointer(2,imol))(1:is2)
c       call parser(atname(mpointer(1,imol)),elfield,lelfield,i1,i2,i3)
c       write(*,'('' atname = '',a,'' Element *'',a,
c    x    ''* i1,i2,i3 '',3i6)') atname(mpointer(1,imol))(1:is1),
c    x    elfield(1:lelfield),i1,i2,i3
      enddo



c
c     find out how many repeat units and the index of first and last site of each
c     each molecule has a set of residues
c
c     irpointer(1,*) = index of first atom of this residue
c              (2,*) = index of last atom of this residue
c                      residue index from the atname string from iresidue
c                      molecule it is in from molecule
c     mpointer(1,*) = index of first atom of this molecule
c             (2,*) = index of last atom of this molecule
c             (3,*) = index of the first residue of this molecule
c             (4,*) = index of the last residue of this molecule
      nres=0
      irpointer(1,1)=1
      do imol=1,nmol
        imfrst=mpointer(1,imol)
        imlast=mpointer(2,imol)
        if(nres.gt.0) irpointer(2,nres)=mpointer(2,imol-1)
        nres=nres+1
        irpointer(1,nres)=imfrst
        icur=iresidue(imfrst)
        mpointer(3,imol)=nres
        do iat=imfrst+1,imlast
          if(iresidue(iat).ne.icur) then
            irpointer(2,nres)=iat-1
            nres=nres+1
            irpointer(1,nres)=iat
            icur=iresidue(iat)
          endif
        enddo
        mpointer(4,imol)=nres
      enddo
      irpointer(2,nres)=mpointer(2,nmol)

      do imol=1,nmol
        irfrst=mpointer(3,imol)
        irlast=mpointer(4,imol)
        do ires=irfrst,irlast
          iafrst=irpointer(1,ires)
          ialast=irpointer(2,ires)
          if(imol.le.30.or.imol.gt.(nmol-20)) then
          call lstchr(atname(iafrst),is1)
          call lstchr(atname(ialast),is2)
          write(*,'('' Molecule '',i6,'' Residue '',i6,
     x      '' First atom '',i6,2x,a,
     x      '' Last atom '',i6,2x,a)')
     x    imol,ires,iafrst,atname(iafrst)(1:is1),
     x              ialast,atname(ialast)(1:is2)
          endif
        enddo
      enddo






c     the box dimensions change for each frame (if npt),
c     determine box dimensions from the coordinate data
      xmx=coord(1,1)
      xmn=coord(1,1)
      ymx=coord(2,1)
      ymn=coord(2,1)
      zmx=coord(3,1)
      zmn=coord(3,1)
      do i=1,nsites
        xmx=max(xmx,coord(1,i))
        xmn=min(xmn,coord(1,i))
        ymx=max(ymx,coord(2,i))
        ymn=min(ymn,coord(2,i))
        zmx=max(zmx,coord(3,i))
        zmn=min(zmn,coord(3,i))
      enddo
c     assume cubic
      xmx=max(xmx,ymx,zmx)
      xmn=min(xmn,ymn,zmn)
      if(dabs(xmx+xmn).lt.0.1d0*xmx) then
c     case 1 has coordinates from -0.5*edge to +0.5*edge
        xmx=max(xmx,-xmn)
        xmn=-xmx
      elseif(dabs(xmn).lt.0.1d0*xmx) then
c     case 2 has coordinates from zero to edge length
        xmn=0.d0
      endif
      edgex=xmx-xmn
      edgey=xmx-xmn
      edgez=xmx-xmn

      write(*,'(''Box dimensions, volume used for this frame: '',
     x  3f10.3,5x,f15.3)') 
     x  edgex,edgey,edgez,edgex*edgey*edgez


c     place all atoms of each molecule in the same image cell
c     use first site of each molecule as the tag site
      do imol=1,nmol
        ifrst=mpointer(1,imol)
        ilast=mpointer(2,imol)
        do i=ifrst+1,ilast
          dx=coord(1,i)-coord(1,ifrst)
          dy=coord(2,i)-coord(2,ifrst)
          dz=coord(3,i)-coord(3,ifrst)
          sx=0.d0
          sy=0.d0
          sz=0.d0
          if(dx.ge. 0.5d0*edgex) sx=-edgex
          if(dx.lt.-0.5d0*edgex) sx=+edgex
          if(dy.ge. 0.5d0*edgey) sy=-edgey
          if(dy.lt.-0.5d0*edgey) sy=+edgey
          if(dz.ge. 0.5d0*edgez) sz=-edgez
          if(dz.lt.-0.5d0*edgez) sz=+edgez
          coord(1,i)=coord(1,i)+sx
          coord(2,i)=coord(2,i)+sy
          coord(3,i)=coord(3,i)+sz
        enddo
c       if(ilast-ifrst.eq.2) then
c         do i=1,(ilast-ifrst)
c           do j=i+1,(ilast-ifrst+1)
c             dx=coord(1,ifrst-1+i)-coord(1,ifrst-1+j)
c             dy=coord(2,ifrst-1+i)-coord(2,ifrst-1+j)
c             dz=coord(3,ifrst-1+i)-coord(3,ifrst-1+j)
c             dist=dsqrt(dx**2+dy**2+dz**2)
c             write(*,'(''Mol '',i6,2x,a10,2x,a10,2x,f10.4)')
c    x         imol,atname(ifrst-1+i)(1:10),atname(ifrst-1+j)(1:10),dist
c           enddo
c         enddo
c       endif
      enddo


c
c     compute intermolecular interactions
c
      fact=627.509d0*0.529177d0  !332.d0
      ectot=0.d0
      evtot=0.d0
      imol=1
c     do imol=1,nmol-1
        imfrst=mpointer(1,imol)
        imlast=mpointer(2,imol)
        do jmol=imol+1,nmol
          jmfrst=mpointer(1,jmol)
          jmlast=mpointer(2,jmol)
c         find nearest image of jmol to imol
          dx=coord(1,jmfrst)-coord(1,imfrst)
          dy=coord(2,jmfrst)-coord(2,imfrst)
          dz=coord(3,jmfrst)-coord(3,imfrst)
          sx=0.d0
          sy=0.d0
          sz=0.d0
          if(dx.ge. 0.5d0*edgex) sx=-edgex
          if(dx.lt.-0.5d0*edgex) sx=+edgex
          if(dy.ge. 0.5d0*edgey) sy=-edgey
          if(dy.lt.-0.5d0*edgey) sy=+edgey
          if(dz.ge. 0.5d0*edgez) sz=-edgez
          if(dz.lt.-0.5d0*edgez) sz=+edgez
          ecou=0.d0
          evdw=0.d0
          do iat=imfrst,imlast
            it=itype(iat)
            do jat=jmfrst,jmlast
              dx=coord(1,jat)-coord(1,iat)+sx
              dy=coord(2,jat)-coord(2,iat)+sy
              dz=coord(3,jat)-coord(3,iat)+sz
              dist=dsqrt(dx**2+dy**2+dz**2)
              jt=itype(jat)
              ecij=charge(iat)*charge(jat)/dist
              ecou=ecou+ecij
              sig=dsqrt(vdw(1,it)*vdw(1,jt))
              eps=dsqrt(vdw(2,it)*vdw(2,jt))
              x=(sig/dist)**6
              eij=4.d0*eps*x*(x-1.d0)
              evdw=evdw+eij                  
            enddo
          enddo
c         if(dabs(fact*ecou+evdw).gt.10.d0) 
            write(*,'(''Molecules '',i6,2x,i6,'' E(coul) = '',f10.5,
     x      '' E(lj) = '',f10.5)') imol,jmol,fact*ecou,evdw
          ecour=0.d0
          evdwr=0.d0
          irfrst=mpointer(3,imol)
          irlast=mpointer(4,imol)
          jrfrst=mpointer(3,jmol)
          jrlast=mpointer(4,jmol)
          ecoumm=0.d0
          evdwmm=0.d0
          do ires=irfrst,irlast
            do jres=jrfrst,jrlast
              ecouirjr=0.d0
              evdwirjr=0.d0
              do iat=irpointer(1,ires),irpointer(2,ires)
                it=itype(iat)
                do jat=irpointer(1,jres),irpointer(2,jres)
                  dx=coord(1,jat)-coord(1,iat)+sx
                  dy=coord(2,jat)-coord(2,iat)+sy
                  dz=coord(3,jat)-coord(3,iat)+sz
                  dist=dsqrt(dx**2+dy**2+dz**2)
                  jt=itype(jat)
                  ecij=charge(iat)*charge(jat)/dist
                  ecouirjr=ecouirjr+ecij
                  sig=dsqrt(vdw(1,it)*vdw(1,jt))
                  eps=dsqrt(vdw(2,it)*vdw(2,jt))
                  x=(sig/dist)**6
                  eij=4.d0*eps*x*(x-1.d0)
                  evdwirjr=evdwirjr+eij                  
                enddo
              enddo
              write(*,'(''Interactions '',
     x        ''('',i6,''/'',i6,''/'',i6,''-'',i6,'') - ('',
     x              i6,''/'',i6,''/'',i6,''-'',i6,'' ) '',
     x        ''Ecoul='',f10.5,'' Elj='',f10.5)')
     x        ires,imol,irpointer(1,ires),irpointer(2,ires),
     x        jres,jmol,irpointer(1,jres),irpointer(2,jres),
     x        fact*ecouirjr,evdwirjr
              buffer(1,jres-jrfrst+1)=fact*ecouirjr
              buffer(2,jres-jrfrst+1)=     evdwirjr
              ecoumm=ecoumm+ecouirjr
              evdwmm=evdwmm+evdwirjr
            enddo
            write(21,'(i5,100f8.4)') ires,
     x        (0.d0,j=1,irlast-ires),
     x        (buffer(1,j),j=1,jrlast-jrfrst+1),
     x        (0.d0,j=1,ires-irfrst)
            write(22,'(i5,100f8.4)') ires,
     x        (0.d0,j=1,irlast-ires),
     x        (buffer(2,j),j=1,jrlast-jrfrst+1),
     x        (0.d0,j=1,ires-irfrst)
            write(23,'(i5,100f8.4)') ires,
     x        (0.d0,j=1,irlast-ires),
     x        (buffer(1,j)+buffer(2,j),j=1,jrlast-jrfrst+1),
     x        (0.d0,j=1,ires-irfrst)
          enddo
          write(*,'(''From res-res Ecou='',f10.5,'' Evdw='',f10.5)')
     x      fact*ecoumm,evdwmm




          ectot=ectot+ecou
          evtot=evtot+evdw
        enddo
c     enddo   !end of imol (outer) loop
      
      write(*,'(''Total electrostatic energy '',f20.6)') fact*ectot
      write(*,'(''Total Lennard-Jones energy '',f20.6)') evtot



      stop 10




c
c     electrostatics and vdw
c
      ecou=0.d0
      evdw=0.d0
      do i=1,nsites-1
        it=itype(i)
        do j=i+1,nsites
        jt=itype(j)
        dist=dsqrt( (coord(1,i)-coord(1,j))**2
     x             +(coord(2,i)-coord(2,j))**2
     x             +(coord(3,i)-coord(3,j))**2 )
        ecou=ecou + charge(i)*charge(j)/dist
        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/(dist**12) - cv/(dist**6)
        evdw=evdw+eij                  
c       write(*,'('' i,j,r = '',2i5,f10.4,'' sig eps = '',
c    x    2f10.4,'' Eij = '',f10.3)')
c    x    i,j,dist,sig,eps,eij
        enddo
      enddo

c
c     build neighbor list
c
      do i=1,5000
        do j=1,5
          neigh(i,j)=0
        enddo
      enddo

      e12=0.d0
      e12vdw=0.d0
      do i=1,nbonds
        ia=ijbond(1,i)
        ib=ijbond(2,i)
        na=neigh(ia,1)+1
        neigh(ia,1)=na
        neigh(ia,na+1)=ib
        nb=neigh(ib,1)+1
        neigh(ib,1)=nb
        neigh(ib,nb+1)=ia

        dist=dsqrt( (coord(1,ia)-coord(1,ib))**2
     x             +(coord(2,ia)-coord(2,ib))**2
     x             +(coord(3,ia)-coord(3,ib))**2 )
        e12=e12 + charge(ia)*charge(ib)/dist
        it=itype(ia)
        jt=itype(ib)
        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/(dist**12) - cv/(dist**6)
        e12vdw=e12vdw+eij

      enddo

c     note that every 1-3 interaction is in an angle term and it is there only once
      e13=0.d0
      e13vdw=0.d0
      do i=1,nangle
        ia=ijkangle(1,i)
        ja=ijkangle(2,i)
        ka=ijkangle(3,i)
        dist=dsqrt( (coord(1,ia)-coord(1,ka))**2
     x             +(coord(2,ia)-coord(2,ka))**2
     x             +(coord(3,ia)-coord(3,ka))**2 )
        e13=e13 + charge(ia)*charge(ka)/dist
        it=itype(ia)
        jt=itype(ka)
        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/(dist**12) - cv/(dist**6)
        e13vdw=e13vdw+eij                  
      enddo

c     every 1-4 interaction is in a dihedral term, but multiple dihedrals have the same 1-4
c     some dihedrals are improper and the 1-4 with these are really 1-2 and 1-3
c     so to get the 1-4 list check each torsion, 

      e14=0.d0
      e14vdw=0.d0
      n14=0
      do i=1,nsites
        ni=neigh(i,1) !neighbors of i
        do ineigh=1,ni
          j=neigh(i,ineigh+1) 
          nj=neigh(j,1) !neighbors of j
          do jneigh=1,nj
            k=neigh(j,jneigh+1)
            if(k.ne.i) then   !...that are not i
              nk=neigh(k,1) !neighbors of k
              do kneigh=1,nk
                l=neigh(k,kneigh+1)
                if(l.ne.j) then
                  if(l.gt.i) then
                    n14=n14+1
                    ipair14(1,n14)=i
                    ipair14(2,n14)=l
c                   write(*,'('' 1-4 interaction: '',3i6,
c    x              a,1x,a,1x,a,1x,a)')
c    x              n14,i,l,atname(i),atname(j),atname(k),atname(l)
        dist=dsqrt( (coord(1,i)-coord(1,l))**2
     x             +(coord(2,i)-coord(2,l))**2
     x             +(coord(3,i)-coord(3,l))**2 )
        e14=e14 + charge(i)*charge(l)/dist
        it=itype(i)
        jt=itype(l)
        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/dist**12 - cv/dist**6
        e14vdw=e14vdw+eij                  
                  endif
                endif
              enddo
            endif
          enddo  
        enddo
      enddo

      e14x=0.d0
      e14xdup=0.d0

      e14xvdw=0.d0
      e14xvdwdup=0.d0

      do i=1,n14
        ia=ipair14(1,i)
        ja=ipair14(2,i)
        it=itype(ia)
        jt=itype(ja)
        dist=dsqrt( (coord(1,ia)-coord(1,ja))**2
     x             +(coord(2,ia)-coord(2,ja))**2
     x             +(coord(3,ia)-coord(3,ja))**2 )

        e14x=e14x + charge(ia)*charge(ja)/dist

        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/dist**12 - cv/dist**6

        e14xvdw=e14xvdw+eij                  

        do j=i+1,n14
          ib=ipair14(1,j)
          jb=ipair14(2,j)
          if(ib.eq.ia.and.jb.eq.ja) then
            write(*,'('' duplicate 1-4:  '',a,1x,a)')
     x        atname(ib),atname(jb)
            e14xvdwdup=e14xvdwdup+eij
            e14xdup=e14xdup+
     x              charge(ia)*charge(ja)/dist
          endif
        enddo
      enddo

      write(*,'('' Factor = '',f15.6)') fact

      write(*,'(//,'' Electrostatic interactions '',/)') 

      write(*,'(/,'' Ecou from all 1-4 terms     : '',f10.5)')
     x   fact*e14x
      write(*,'('' Ecou from duplicated terms: '',f10.5)')
     x   fact*e14xdup
      write(*,'('' Ecou without duplicates   : '',f10.5)')
     x   fact*e14x-e14xdup


      write(*,'('' Ecou= '',f15.6)') fact*ecou
      write(*,'('' E12 = '',f15.6)') fact*e12
      write(*,'('' E13 = '',f15.6)') fact*e13 
      write(*,'('' E14 = '',f15.6)') fact*e14 
c     write(*,'('' E14x= '',f15.6)') fact*e14x 


      write(*,'(/,'' Ecoul-E12-E13-E14     = '',f15.6)') 
     x    fact*(ecou-e13-e12-e14)
      write(*,'(  '' E14                   = '',f15.6)') 
     x    fact*(e14)
      write(*,'(  '' Ecoul-E12-E13-0.5*E14 = '',f15.6)') 
     x    fact*(ecou-e13-e12-e14+0.5d0*e14)




      write(*,'(//,'' Van der Waals interactions '',/)') 

      write(*,'(/,'' Evdw from all 1-4 terms     : '',f10.5)')
     x   e14xvdw
      write(*,'('' Evdw from duplicated terms: '',f10.5)')
     x   e14xvdwdup
      write(*,'('' Evdw without duplicates   : '',f10.5)')
     x   e14xvdw-e14xvdwdup



      write(*,'(/,'' Evdw= '',f25.4)') evdw
      write(*,'('' E12v= '',f25.4)') e12vdw
      write(*,'('' E13v= '',f25.4)') e13vdw
      write(*,'('' E14v= '',f25.4)') e14vdw-e14xvdwdup

      write(*,'(/,'' Evdw-E12v-E13v-E14v     = '',f25.4)') 
     x    (evdw-e13vdw-e12vdw-(e14vdw-e14xvdwdup))
      write(*,'(  '' E14                     = '',f25.4)') 
     x    (e14vdw-e14xvdwdup)
      write(*,'(  '' Evdw-E12v-E13v-0.5*E14v = '',f25.4)') 
     x    (evdw-e13vdw-e12vdw-(e14vdw-e14xvdwdup)
     x    +0.5d0*(e14vdw-e14xvdwdup))




200   continue





      end

c
c     subroutine to compute bond energy
c
      subroutine bengy(nsites,coord,nbonds,ijbond,nbtp,bndpar,edge)
      implicit real*8 (a-h,o-z)
      dimension coord(3,nsites),ijbond(3,nbonds),bndpar(2,nbtp)

      ebond=0.d0
      do i=1,nbonds
        ia=ijbond(1,i)
        ja=ijbond(2,i)
        dx=         (coord(1,ia)-coord(1,ja))**2
        dy=        +(coord(2,ia)-coord(2,ja))**2
        dz=        +(coord(3,ia)-coord(3,ja))**2
        if(dx.gt.0.5d0*edge) then
          dx=dx-edge
        elseif(dx.lt.-0.5d0*edge) then
          dx=dx+edge
        endif
        if(dy.gt.0.5d0*edge) then
          dy=dy-edge
        elseif(dy.lt.-0.5d0*edge) then
          dy=dy+edge
        endif
        if(dz.gt.0.5d0*edge) then
          dz=dz-edge
        elseif(dz.lt.-0.5d0*edge) then
          dz=dz+edge
        endif
        dist=dsqrt( dx**2 + dy**2 + dz**2  )      
        it=ijbond(3,i)
        eij=bndpar(1,it) * (dist - bndpar(2,it))**2
        if(eij.gt.10.d0) write(*,'(''Large bond energy between sites '',
     x    2i8,'' dist = '',f12.4,'' energy = '',f12.4)')
     x    ia,ja,dist,eij
        ebond=ebond+eij
      enddo
      write(*,'(''E(bond) = '',f15.6)') ebond



      end

c
c     subroutine to compute angle energy
c
      subroutine aengy(nsites,coord,nangle,ijkangle,natp,angpar)
      implicit real*8 (a-h,o-z)
      dimension coord(3,nsites),ijkangle(4,nangle),angpar(2,natp)
      pi=4.d0*datan(1.d0)


      eang=0.d0
      do i=1,nangle
        ia=ijkangle(1,i)
        ja=ijkangle(2,i)
        ka=ijkangle(3,i)
        it=ijkangle(4,i)
        call angint(coord(1,ia),coord(1,ja),coord(1,ka),thet)
        eijk=angpar(1,it) * (thet - (pi/180.d0)*angpar(2,it))**2
c       write(*,'('' Angle index = '',i5,'' type = '',i5,
c    x   '' parms = '',2f10.3)')
c    x   i,it,angpar(1,it),angpar(2,it)
c       write(*,'('' Angle = '',f10.3,'' Engy = '',f10.3)')
c    x    thet*(180.d0/pi),eij
        eang =eang +eijk
      enddo
      write(*,'(''E(angl) = '',f15.6)') eang 

      end


c
c     subroutine to compute dihedral energy
c
      subroutine dengy(nsites,coord,ndih,ijkldih,ndtp,dihpar)
      implicit real*8 (a-h,o-z)
      dimension coord(3,nsites),ijkldih(5,ndih),dihpar(4,ndtp)
      pi=4.d0*datan(1.d0)
      edih=0.d0
      do i=1,ndih  
        ia=ijkldih(1,i)
        ja=ijkldih(2,i)
        ka=ijkldih(3,i)
        la=ijkldih(4,i)
        it=ijkldih(5,i)
        call dihint(1,coord(1,ia),coord(1,ja),
     x                coord(1,ka),coord(1,la),
     x              phi,dum1,dum2)
        eijkl=dihpar(1,it) * ( 1.d0 + 
     x      cos(dihpar(2,it)*phi - (pi/180.d0)*dihpar(3,it)))
        edih =edih +eijkl
c       write(*,'('' Term '',i6,'' phi = '',f10.5,
c    x    '' Parms = '',3f10.5,'' E(ijkl) = '',f12.5)')
c    x   i,phi,dihpar(1,it),dihpar(2,it),dihpar(3,it),eijkl
      enddo
      write(*,'(''E(dihe) = '',f15.6)') edih 

      end



c Change History:
c $Log: util.f,v $
c Revision 1.8  1994/08/03  19:49:54  swope
c Changed behavior of xyz2int so that zmatricies are not printed out tha
c undefined torsions.  When angles are close to 180 they are changed to
c when close to 0 they are changed to 0.0001.    (Swope Aug 3, 1994)
c
c Revision 1.6  1994/07/30  06:33:09  swope
c Several changes by Swope to internal-cartesian and cartesian-internal
c conversion routines:
c angint-switch to low angle expression on new threshold
c dihint-call tlkemsg when atoms too close
c xyz2int-change messages during certain errors
c i2x-changes in ways linear systems treated: two strikes policy
c     for generating cartesians with undefined torsions
c xyz2dih-added new return code to flag 1-2-3 linear vs 2-3-4 linear
c
c Revision 1.5  1994/07/07  16:34:02  gclie
c This version includes xyz2int and int2xyz from Swope.  It should repla
c
c*arc i2x
      subroutine i2x(nat,lnk,rab,ang,dih,istart,iend,xyz)
c
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Copyright (c) 1994 International Business Machines Corporation
C  All rights reserved
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Description      Given the link vector and internal coordinates
c                     of a list of particles, this subroutine computes
c                     the cartesian coordinates for a subset of the
c                     particles, those with indicies from istart to
c                     iend.  The units of the cartesian coordinates
c                     will be the same as those of the input internals.
c                     The units of the angles are assumed to be radians.
c                   If the particle indicies between istart and iend
c                     fall in the range 1,2 or 3, particle 1 is placed
c                     at the origin, particle 2 is placed on the x-axis,
c                     and particle 3 is placed in the x-y plane.
c
C
C  Input
C
C     nat           Number of atoms in list
C
C     lnk(3,nat)    link vector for describing internals
C                     lnk(1,i) gives label for distance from i
C                     lnk(2,i) gives label for angle i-lnk(1,i)-lnk(2,i)
C                     lnk(3,i) gives label for torsion
C                     i-lnk(1,i)-lnk(2,i)-lnk(3,i)
C                     Note that first six entries are ignored.
C
C     rab(nat)      Vector of distances.
C
C     ang(nat)      Vector of angles.  Range returned is 0.le.ang.le.pi.
C
C     dih(nat)      Vector of torsions. Range returned is
C                   -pi .lt. phi .le. pi, and follows IUPAC
C                   sign and range convention.
c
c     istart        The index of the first atom whose cartesian
c                   coordinates are to be computed.  istart must be
c                   an integer with ( 1 .le. istart .le. nat ).
c
c     iend          The index of the last atom whose cartesian
c                   coordinates are to be computed.  iend must be
c                   an integer with ( 1 .le. iend .le. nat ).
C
C
C  Output
C
C     xyz(3,i)      cartesian coordinates of atoms i=istart to iend
C
C
C  Linkage          List of subroutines called by this one
C
C  Programmer       William Swope (some from Carnevali and Nesbet)
C
C  Date             June 1994
C
C  Update log       Record of all updates including names of programmers
C                     descriptions of changes made, and dates of change.
C                   07-29-94 Modified by Swope to die (tlkemsg) if
C                            there are TWO invalid torsion angles.
C                            Invalid torsion angles are those that
C                            result in ill-defined cartesian coordinates
C                            This change still allows linear molecules
C                            and molecules with two long linear segments
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
c
      Implicit Real*8 (a, b, d-h, o, p, r-y)
      Implicit Integer (i-n)
c     Violation of ARCHEM conventions, but there are no logicals here
c     Implicit Logical (z)
      Implicit Real*8 (z)
      Implicit Character (c)
c     Implicit Undefined (q)
 
      character cproduct*48
      dimension cproduct(3)
 
c     Input and output arrays
      real*8 xyz(3,nat),rab(nat),ang(nat),dih(nat)
      integer lnk(3,nat)
 
c     Local arrays
c     Following arrays give molecule coordinate system relative to lab
      real*8 xhat(3),yhat(3),zhat(3),zprev(3)
      real*8 r12(3),r23(3),r43(3)
 
 
C     THE SIGN CONVENTION FOR PHI IS AS FOLLOWS:
C     NUMBER THE ASSOCIATED ATOMS 1, 2, 3, 4.  ATOMS 1 AND 4 ARE
C     "TERMINAL" ATOMS AND THE DIHEDRAL ANGLE IS ABOUT THE BOND
C     BETWEEN ATOMS 2 AND 3.  IMAGINE HOLDING THE PLANE FORMED BY
C     ATOMS 1, 2, AND 3 FIXED AND ROTATING THE PLANE FORMED BY
C     ATOMS 2, 3, AND 4 ABOUT A VECTOR THAT POINTS FROM ATOM 2
C     TO ATOM 3.  THE POSITIVE SENSE CORRESPONDS TO THE RIGHT-HAND
C     RULE FOR ROTATION ABOUT THE VECTOR FROM ATOM 2 TO ATOM 3.
c     This agrees with the IUPAC convention.
C
C     FURTHERMORE, PHI=0 CORRESPONDS TO A CONDITION WHERE ALL FOUR
C     ATOMS ARE ON A PLANE AND ATOMS 1 AND 4 ARE "CIS" TO THE BOND
C     BETWEEN ATOMS 2 AND 3.  LIKEWISE, PHI=180 DEGREES CORRESPONDS
C     A CONDITION WHERE ALL FOUR ATOMS ARE ON A PLANE AND ATOMS 1 AND
C     4 ARE "TRANS" TO THE BOND BETWEEN ATOMS 2 AND 3.
 
c     comment out the following call to check the integrity of the
c     link vector.  this call is appropriate in int2xyz, where the
c     full set of cartesians is computed, but should probably not
c     be made over and over again if the cartesians are computed
c     one at a time or in small increments.
c
c     check validity of link vector
c     call chklnk(nat,lnk,iret)
c     if(iret.ge.2) then
c       print *, ' Error: link matrix is not usable for determination',
c    >           ' of cartesian coordinates.'
c       print *, ' Complete link vector: '
c       write(*,'(3i10)') ((lnk(i,j),i=1,3),j=1,nat)
c       call tlkemsg(' Invalid link matrix.','int2xyz')
c     endif
 
 
c     lnk(1,i),i=1,nat specifies atom indicies for "bond" distances
c     lnk(2,i),i=1,nat specifies atom indicies for "bond" angles
c     lnk(3,i),i=1,nat specifies atom indicies for dihedral angles
 
      pi=4.d0*datan(1.d0)
 
      if(istart.le.0   .or.
     >   istart.gt.nat .or.
     >   iend.le.0     .or.
     >   iend.gt.nat   .or.
     >   istart.gt.iend ) then
      write(*,'('' Error in subroutine i2x: invalid arguments for'',/,
     >          '' atom indicies: istart='',i5,'' iend='',i5,/,
     >          '' Values should be from 1 to nat='',i5)')
     >          istart,iend,nat
C     CALL TLKEMSG(' INVALID ATOM INDICES PASSED TO X2I.','X2I')
      endif
 
      zprev(1)=0.d0
      zprev(2)=0.d0
      zprev(3)=1.d0
 
c     failure count on placing atoms when torsion undefined
      nstrike=0
 
 
      do 10 i=istart,iend
 
      if(i.eq.1) then
c     if first atom
        xyz(1,1)=0.d0
        xyz(2,1)=0.d0
        xyz(3,1)=0.d0
      elseif(i.eq.2) then
c     if second atom
        xyz(1,2)=rab(2)
        xyz(2,2)=0.d0
        xyz(3,2)=0.d0
      elseif(i.eq.3) then
c     if third atom (note that normally 0.le.ang.le.pi)
        if(lnk(1,3).eq.2) then
          xyz(1,3)=rab(2) -rab(3)*cos(ang(3))
        else
          xyz(1,3)=       +rab(3)*cos(ang(3))
        endif
        xyz(2,3)=         +rab(3)*sin(ang(3))
        xyz(3,3)=0.d0
        if(abs(ang(3)).gt.1.d-3 .and.
     >     abs(abs(ang(3))-pi).gt.1.d-3) nstrike=1
      endif
 
c     for fourth or higher atom
      if(i.le.3) go to 10
 
      l2=lnk(1,i)
      l3=lnk(2,i)
      l4=lnk(3,i)
      r23(1)=xyz(1,l2)-xyz(1,l3)
      r23(2)=xyz(2,l2)-xyz(2,l3)
      r23(3)=xyz(3,l2)-xyz(3,l3)
 
c     local x axis is directed from atom l3 to atom l2
      d=r23(1)**2+r23(2)**2+r23(3)**2
      if(d.le.1.d-10) then
        print *, ' Error: Atoms too close to establish unique',
     >           ' directions for cartesian placement from z-matrix.'
        print *, ' Atom indicies: ',l2,l3
C       CALL TLKEMSG(' ATOMS TOO CLOSE.','I2X')
      endif
      d=1.d0/sqrt(d)
      xhat(1)=r23(1)*d
      xhat(2)=r23(2)*d
      xhat(3)=r23(3)*d
 
c     local z axis is directed along r23 cross r43, if this is non-zero
      r43(1)=xyz(1,l4)-xyz(1,l3)
      r43(2)=xyz(2,l4)-xyz(2,l3)
      r43(3)=xyz(3,l4)-xyz(3,l3)
      zhat(1)=r23(2)*r43(3)-r23(3)*r43(2)
      zhat(2)=r23(3)*r43(1)-r23(1)*r43(3)
      zhat(3)=r23(1)*r43(2)-r23(2)*r43(1)
      d=zhat(1)**2+zhat(2)**2+zhat(3)**2
c
      if(d.le.1.d-8) then
        if(abs(ang(i)).gt.1.d-3 .and.
     >     abs(abs(ang(i))-pi).gt.1.d-3) then
c         Problem: angle 4-3-2 is (nearly) linear AND ang(i) is other
c         than zero or pi.  This means placement of particle 1 is
c         undefined.  The first time this happens, we will allow it,
c         and place particle 1 in the (local) x-y plane.  This
c         corresponds to setting zhat to the lab z axis.
          if(nstrike.ne.0) then
          print *
          print *, ' A sequence of bond angles near 180 (or 0) degrees'
          print *, ' has resulted in an undefined torsion angle and'
          print *, ' this torsion angle must be defined to determine'
          print *, ' unique Cartesian coordinates for the molecule.'
          print *
          print *, ' Offending link vector:'
          print *, ' lnk(1,',i,') = ',l2
          print *, ' lnk(2,',i,') = ',l3
          print *, ' lnk(3,',i,') = ',l4
          print *
C         CALL TLKEMSG('CANNOT GENERATE CARTESIAN COORDINATES.','I2X')
          endif
          nstrike=1
        endif
        zhat(1)=zprev(1)
        zhat(2)=zprev(2)
        zhat(3)=zprev(3)
      else
        d=1.d0/sqrt(d)
        zhat(1)=zhat(1)*d
        zhat(2)=zhat(2)*d
        zhat(3)=zhat(3)*d
        zprev(1)=zhat(1)
        zprev(2)=zhat(2)
        zprev(3)=zhat(3)
      endif
 
c     local y axis is zhat cross xhat
      yhat(1)=zhat(2)*xhat(3)-zhat(3)*xhat(2)
      yhat(2)=zhat(3)*xhat(1)-zhat(1)*xhat(3)
      yhat(3)=zhat(1)*xhat(2)-zhat(2)*xhat(1)
      d=1.d0/sqrt(yhat(1)**2+yhat(2)**2+yhat(3)**2)
      yhat(1)=yhat(1)*d
      yhat(2)=yhat(2)*d
      yhat(3)=yhat(3)*d
 
      if(abs(ang(i)).gt.1.d-3 .and.
     >   abs(abs(ang(i))-pi).gt.1.d-3) nstrike=1
 
c     in molecule frame (i.e., relative to xhat,yhat,zhat coord system)
c     new center is placed at ...
      x=-rab(i)*cos(ang(i))
      rho=rab(i)*sin(ang(i))
      y=rho*cos(dih(i))
      z=rho*sin(dih(i))
 
c     in lab frame ...
      xyz(1,i) = xyz(1,l2) + x*xhat(1)+y*yhat(1)+z*zhat(1)
      xyz(2,i) = xyz(2,l2) + x*xhat(2)+y*yhat(2)+z*zhat(2)
      xyz(3,i) = xyz(3,l2) + x*xhat(3)+y*yhat(3)+z*zhat(3)
 
10    continue
 
      end


c
c     angle calculation stolen from dihint
c
      subroutine angint(xyz1,xyz2,xyz3,ang)
      Implicit Real*8 (a-h,o-z)
c     Input and output arrays
      real*8 xyz1(3),xyz2(3),xyz3(3)
      real*8 a(3),b(3)

c     Compute a=r1-r2, b=r3-r2
      do 10 k=1,3
      a(k)=xyz1(k)-xyz2(k)
10    b(k)=xyz3(k)-xyz2(k)
 
c     Compute a . b 
      dot=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
 
c     Compute lengths
      b2=b(1)**2+b(2)**2+b(3)**2
      a2=a(1)**2+a(2)**2+a(3)**2
      angcos=dot/dsqrt(a2*b2)
      ang=dacos(angcos)

      end





c Change History:
c $Log$
c*arc dihint
      subroutine dihint(iop,xyz1,xyz2,xyz3,xyz4,
     >       phi,dphidx,d2phidx2)
c
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Copyright (c) 1994 International Business Machines Corporation
C  All rights reserved
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Description      Given the coordinates of four particles, this
C                     subroutine computes the torsion (dihedral)
C                     angle between them, the Jacobean (dphi/dx(i))
C                     and d2phi/dx(i)dx(j).
C
C  Input
C
C     iop           Control
C                   iop = 1 means just compute angle
C                   iop = 2 means compute angle and Jacobean
C                   iop = 3 means compute angle, Jacobean and second
C                           derivative
C
C     xyz1(3)       cartesian coordinates of first atom
C     xyz2(3)       cartesian coordinates of second atom
C     xyz3(3)       cartesian coordinates of third atom
C     xyz4(3)       cartesian coordinates of fourth atom
C
C
C  Output
C
C     phi           Angle between atoms 1-2-3-4.  Range returned is
C                   -pi .lt. phi .le. pi, following IUPAC
C                   sign and range convention.
C
C     dphidx(12)    Jacobean: dphi/dx(1), dphi/dy(1), ...
C
C     d2phid2(45)   Second derivative, d2phi/dx(i)dx(2), a symmetric
C                   matrix, stored lower triangle by row
C
C  Linkage          List of subroutines called by this one
C
C  Programmer       William Swope (some from Carnevali)
C
C  Date             June 1994
C
C  Update log       Record of all updates including names of programmers,
C                     descriptions of changes made, and dates of change.
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
c
      Implicit Real*8 (a, b, d-h, o, p, r-y)
      Implicit Integer (i-n)
c     Implicit Logical (z)
c     Violation of ARCHEM convention; but no logical data here
      Implicit Real*8 (z)
c     Implicit Character (c)
c     Violation of ARCHEM convention; but no character data here
      Implicit Real*8 (c)
c     Implicit Undefined (q)
 
      character cproduct*48
      dimension cproduct(3)
 
c     Input and output arrays
      real*8 xyz1(3),xyz2(3),xyz3(3),xyz4(3)
      real*8 dphidx(12),d2phidx2(78)
 
 
C     THE SIGN CONVENTION FOR PHI IS AS FOLLOWS:
C     NUMBER THE ASSOCIATED ATOMS 1, 2, 3, 4.  ATOMS 1 AND 4 ARE
C     "TERMINAL" ATOMS AND THE DIHEDRAL ANGLE IS ABOUT THE BOND
C     BETWEEN ATOMS 2 AND 3.  IMAGINE HOLDING THE PLANE FORMED BY
C     ATOMS 1, 2, AND 3 FIXED AND ROTATING THE PLANE FORMED BY
C     ATOMS 2, 3, AND 4 ABOUT A VECTOR THAT POINTS FROM ATOM 2
C     TO ATOM 3.  THE POSITIVE SENSE CORRESPONDS TO THE RIGHT-HAND
C     RULE FOR ROTATION ABOUT THE VECTOR FROM ATOM 2 TO ATOM 3.
c     This agrees with the IUPAC convention.
C
C     FURTHERMORE, PHI=0 CORRESPONDS TO A CONDITION WHERE ALL FOUR
C     ATOMS ARE ON A PLANE AND ATOMS 1 AND 4 ARE "CIS" TO THE BOND
C     BETWEEN ATOMS 2 AND 3.  LIKEWISE, PHI=180 DEGREES CORRESPONDS
C     A CONDITION WHERE ALL FOUR ATOMS ARE ON A PLANE AND ATOMS 1 AND
C     4 ARE "TRANS" TO THE BOND BETWEEN ATOMS 2 AND 3.
 
 
c     Local and scratch arrays
 
      dimension a(3),b(3),c(3),u(3),v(3),acrossc(3)
 
c     First derivatives of u**2, v**2 with respect to a, b, and c
      dimension du2da(3),du2db(3),dv2db(3),dv2dc(3)
 
c     First derivatives of D=u**2*v**2 and F=D**(-1/2) with
c     respect to a, b, and c
      dimension ddda(3),dddb(3),dddc(3)
      dimension dfda(3),dfdb(3),dfdc(3)
 
c     First derivatives of uv with respect to a, b, and c
      dimension duvda(3),duvdb(3),duvdc(3)
 
c     Derivatives of S with respect to a, b, and c
      dimension dsda(3),dsdb(3),dsdc(3)
 
c     Derivatives of cosphi and sinphi with respect to a, b, and c
      dimension dcosphida(3),dcosphidb(3),dcosphidc(3)
      dimension dsinphida(3),dsinphidb(3),dsinphidc(3)
 
c     Derivatives of phi with respect to a, b, and c
      dimension dphida(3),dphidb(3),dphidc(3)
 
c     Second derivatives
      dimension du2dada(3,3),du2dbdb(3,3),du2dadb(3,3)
      dimension dv2dbdb(3,3),dv2dbdc(3,3),dv2dcdc(3,3)
      dimension dddada(3,3),dddbdb(3,3),dddcdc(3,3)
      dimension dddadb(3,3),dddadc(3,3),dddbdc(3,3)
      dimension dfdada(3,3),dfdbdb(3,3),dfdcdc(3,3)
      dimension dfdadb(3,3),dfdadc(3,3),dfdbdc(3,3)
      dimension              duvdbdb(3,3)
      dimension dsdadb(3,3),dsdadc(3,3),dsdbdc(3,3)
      dimension              dsdbdb(3,3)
      dimension duvdadb(3,3),duvdadc(3,3),duvdbdc(3,3)
      dimension dcosphidada(3,3),dcosphidbdb(3,3),dcosphidcdc(3,3)
      dimension dcosphidadb(3,3),dcosphidadc(3,3),dcosphidbdc(3,3)
      dimension dsinphidada(3,3),dsinphidbdb(3,3),dsinphidcdc(3,3)
      dimension dsinphidadb(3,3),dsinphidadc(3,3),dsinphidbdc(3,3)
      dimension dphidada(3,3),dphidbdb(3,3),dphidcdc(3,3)
      dimension dphidadb(3,3),dphidadc(3,3),dphidbdc(3,3)
 
      dimension atilde(3,3),btilde(3,3),ctilde(3,3)
 
c     identity tensor here to simplify some loops
      real*8 delta(3,3)/1.d0,3*0.d0,1.d0,3*0.d0,1.d0/
 
      data cproduct /'$RCSfile$',
     $               '$Revision$$Date$',
     $               '$IBMCSA: PRODUCT VERSION NUMBER $'/
 
c     Statement function gives index of (i,j) in lower triang by row
      lowtri(i,j) = j + i*(i-1)/2
 
      if(iop.ne.1 .and. iop.ne.2 .and. iop.ne.3) then
        print *, ' Incorrect input to dihint subroutine: iop = ',iop
c       call arcdie()
      endif
 
      ierror=0
 
      if(iop.ge.1) then
c     compute angle
 
c     Compute a=r2-r1, b=r3-r2, c=r4-r3
      do 10 k=1,3
      a(k)=xyz2(k)-xyz1(k)
      b(k)=xyz3(k)-xyz2(k)
10    c(k)=xyz4(k)-xyz3(k)
 
c     Compute u = a x b , v = b x c, vectors normal to 123, 234 planes
      u(1)=a(2)*b(3)-a(3)*b(2)
      u(2)=a(3)*b(1)-a(1)*b(3)
      u(3)=a(1)*b(2)-a(2)*b(1)
      v(1)=b(2)*c(3)-b(3)*c(2)
      v(2)=b(3)*c(1)-b(1)*c(3)
      v(3)=b(1)*c(2)-b(2)*c(1)
 
c     Compute lengths and scalar products
      b2=b(1)**2+b(2)**2+b(3)**2
      u2=u(1)**2+u(2)**2+u(3)**2
      v2=v(1)**2+v(2)**2+v(3)**2
      if( b2.eq.0.d0 )then
c       this should be a hard error; print out message and die
c       if this happens there is something seriously wrong
        print *, ' Error:  Can not compute torsion angle when central',
     >           ' bond has zero length.'
c       call arcdie('dihint')
      endif
      b1=sqrt(b2)
      u1=sqrt(u2)
      v1=sqrt(v2)
      d=u2*v2
      d1=sqrt(d)
      uv=u(1)*v(1)+u(2)*v(2)+u(3)*v(3)
      av=a(1)*v(1)+a(2)*v(2)+a(3)*v(3)
      s=b1*av
      if( u2.eq.0.d0 .or. v2.eq.0.d0 )then
        print *, ' Error:  Three collinear atoms make torsion angle',
     >           ' undefined.'
        print *, ' Zero returned for angle, gradient and hessian.'
        ierror=1
        f=0.d0
        d=1.d0
        d1=1.d0
c       Compute cosphi and sinphi
        cosphi=1.d0
        sinphi=0.d0
      else
        f=1.d0/d1
c       Compute cosphi and sinphi
        cosphi=uv*f
        sinphi=s*f
      endif
 
c     Compute phi itself, produce range of -pi.lt.phi.le.pi
      phi=atan2(sinphi,cosphi)
 
      endif
 
      if(iop.ge.2) then
c     compute dphidx
 
c     Compute derivatives of u**2 and v**2 with respect to a, b, and c.
      a2=a(1)**2+a(2)**2+a(3)**2
      c2=c(1)**2+c(2)**2+c(3)**2
      ab=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
      bc=b(1)*c(1)+b(2)*c(2)+b(3)*c(3)
      ac=a(1)*c(1)+a(2)*c(2)+a(3)*c(3)
      do 20 k=1,3
      du2da(k)=2.d0*(b2*a(k)-ab*b(k))
      du2db(k)=2.d0*(a2*b(k)-ab*a(k))
      dv2db(k)=2.d0*(c2*b(k)-bc*c(k))
20    dv2dc(k)=2.d0*(b2*c(k)-bc*b(k))
 
c     Compute derivatives of D=u**2*v**2 with respect to a, b, and c.
      do 35 k=1,3
      ddda(k)=v2*du2da(k)
      dddb(k)=v2*du2db(k)+u2*dv2db(k)
35    dddc(k)=u2*dv2dc(k)
 
c     Compute derivatives of F=D**(-1/2) with respect to a, b, and c.
      do 40 k=1,3
      dfda(k)=-0.5d0*ddda(k)/(d*d1)
      dfdb(k)=-0.5d0*dddb(k)/(d*d1)
40    dfdc(k)=-0.5d0*dddc(k)/(d*d1)
 
 
 
      if(abs(sinphi).gt.abs(cosphi))then
c     if(.false.)then
c*************************************************
c     CASE A - use expressions derived from cosphi
c     Compute derivatives of u*v=C with respect to a, b, and c.
      do 50 k=1,3
      duvda(k)=b(k)*bc-c(k)*b2
      duvdb(k)=a(k)*bc+c(k)*ab-2.d0*b(k)*ac
50    duvdc(k)=b(k)*ab-a(k)*b2
c     Compute derivatives of cosphi=(u*v)*F=C*F with respect to a, b, and c.
      do 60 k=1,3
      dcosphida(k)=duvda(k)*f+uv*dfda(k)
      dcosphidb(k)=duvdb(k)*f+uv*dfdb(k)
60    dcosphidc(k)=duvdc(k)*f+uv*dfdc(k)
c     Compute derivatives of phi with respect to a, b, and c.
      do 70 k=1,3
      dphida(k)=-dcosphida(k)/sinphi
      dphidb(k)=-dcosphidb(k)/sinphi
70    dphidc(k)=-dcosphidc(k)/sinphi
c*************************************************
      else
c*************************************************
c     CASE B - use expressions derived from sinphi
      acrossc(1)=a(2)*c(3)-a(3)*c(2)
      acrossc(2)=a(3)*c(1)-a(1)*c(3)
      acrossc(3)=a(1)*c(2)-a(2)*c(1)
      s=b1*av
c     Compute derivatives of S=b(a*v) with respect to a, b, and c.
      do 85 k=1,3
      dsda(k)=b1*v(k)
      dsdb(k)=b(k)*av/b1-b1*acrossc(k)
85    dsdc(k)=b1*u(k)
c     Compute derivatives of sinphi with respect to a, b, and c.
      do 90 k=1,3
      dsinphida(k)=dsda(k)*f+s*dfda(k)
      dsinphidb(k)=dsdb(k)*f+s*dfdb(k)
90    dsinphidc(k)=dsdc(k)*f+s*dfdc(k)
c     Compute derivatives of phi with respect to a, b, and c.
      do 95 k=1,3
      dphida(k)=dsinphida(k)/cosphi
      dphidb(k)=dsinphidb(k)/cosphi
95    dphidc(k)=dsinphidc(k)/cosphi
c*************************************************
 
      endif
 
c     Add contribution to dphi/dx array
      do 80 k=1,3
      dphidx(k  )=          -dphida(k)
      dphidx(3+k)= dphida(k)-dphidb(k)
      dphidx(6+k)= dphidb(k)-dphidc(k)
80    dphidx(9+k)= dphidc(k)
 
c     This code is for three atoms collinear; torsion undefined
      if(ierror.eq.1) then
        do 81 k=1,12
81      dphidx(k)=0.d0
      endif
 
      endif
 
      if(iop.ge.3) then
 
c     Compute second derivatives of u**2 and v**2 with respect to a, b, and c.
      do 110 i=1,3
      do 110 j=1,3
      du2dada(i,j)=2.d0*(b2*delta(i,j)-b(i)*b(j))
      du2dbdb(i,j)=2.d0*(a2*delta(i,j)-a(i)*a(j))
      du2dadb(i,j)=2.d0*(2.d0*a(i)*b(j)-b(i)*a(j)-ab*delta(i,j))
      dv2dbdb(i,j)=2.d0*(c2*delta(i,j)-c(i)*c(j))
      dv2dcdc(i,j)=2.d0*(b2*delta(i,j)-b(i)*b(j))
      dv2dbdc(i,j)=2.d0*(2.d0*b(i)*c(j)-c(i)*b(j)-bc*delta(i,j))
110   continue
 
c     Compute second derivatives of D=u**2*v**2 with respect to a, b, and c.
      do 120 i=1,3
      do 120 j=1,3
      dddada(i,j)=
     >            v2*du2dada(i,j)
      dddbdb(i,j)=dv2db(i)*du2db(j)+du2db(i)*dv2db(j)
     >           +v2*du2dbdb(i,j)  +u2*dv2dbdb(i,j)
      dddcdc(i,j)=
     >                             +u2*dv2dcdc(i,j)
      dddadb(i,j)=                 +du2da(i)*dv2db(j)
     >           +v2*du2dadb(i,j)
      dddadc(i,j)=                 +du2da(i)*dv2dc(j)
     >
      dddbdc(i,j)=                 +du2db(i)*dv2dc(j)
     >                             +u2*dv2dbdc(i,j)
120   continue
 
c     Compute second derivatives of F=D**(-1/2) with respect to a, b, and c.
      f3=f/d
      do 130 i=1,3
      do 130 j=1,3
      dfdada(i,j)=.5d0*f3*(1.5d0*ddda(i)*ddda(j)/d-dddada(i,j))
      dfdbdb(i,j)=.5d0*f3*(1.5d0*dddb(i)*dddb(j)/d-dddbdb(i,j))
      dfdcdc(i,j)=.5d0*f3*(1.5d0*dddc(i)*dddc(j)/d-dddcdc(i,j))
      dfdadb(i,j)=.5d0*f3*(1.5d0*ddda(i)*dddb(j)/d-dddadb(i,j))
      dfdadc(i,j)=.5d0*f3*(1.5d0*ddda(i)*dddc(j)/d-dddadc(i,j))
      dfdbdc(i,j)=.5d0*f3*(1.5d0*dddb(i)*dddc(j)/d-dddbdc(i,j))
130   continue
 
      if(abs(sinphi).gt.abs(cosphi))then
c     if(.false.)then
c*************************************************
c     CASE A - use expressions derived from cosphi
c     Compute second derivatives of u*v=C with respect to a, b, and c.
      do 140 i=1,3
      do 140 j=1,3
      duvdbdb(i,j)=a(i)*c(j)+c(i)*a(j)-2.d0*ac*delta(i,j)
      duvdadb(i,j)=bc*delta(i,j)+b(i)*c(j)-2.d0*c(i)*b(j)
      duvdadc(i,j)=b(i)*b(j)-b2*delta(i,j)
      duvdbdc(i,j)=ab*delta(i,j)+a(i)*b(j)-2.d0*b(i)*a(j)
140   continue
c     Compute second derivatives of cosphi=(u*v)*F=C*F with respect to a, b, and
c.
      do 150 i=1,3
      do 150 j=1,3
      dcosphidada(i,j)=
     >                +dfda(i)*duvda(j)+duvda(i)*dfda(j)+uv*dfdada(i,j)
      dcosphidbdb(i,j)=
     >  duvdbdb(i,j)*f+dfdb(i)*duvdb(j)+duvdb(i)*dfdb(j)+uv*dfdbdb(i,j)
      dcosphidcdc(i,j)=
     >                +dfdc(i)*duvdc(j)+duvdc(i)*dfdc(j)+uv*dfdcdc(i,j)
      dcosphidadb(i,j)=
     >  duvdadb(i,j)*f+dfda(i)*duvdb(j)+duvda(i)*dfdb(j)+uv*dfdadb(i,j)
      dcosphidadc(i,j)=
     >  duvdadc(i,j)*f+dfda(i)*duvdc(j)+duvda(i)*dfdc(j)+uv*dfdadc(i,j)
      dcosphidbdc(i,j)=
     >  duvdbdc(i,j)*f+dfdb(i)*duvdc(j)+duvdb(i)*dfdc(j)+uv*dfdbdc(i,j)
150   continue
c     Compute second derivatives of phi with respect to a, b, and c.
      z1=-cosphi/(sinphi**3)
      z2=-1.d0/sinphi
      do 160 i=1,3
      do 160 j=1,3
      dphidada(i,j)=z1*dcosphida(i)*dcosphida(j)+z2*dcosphidada(i,j)
      dphidbdb(i,j)=z1*dcosphidb(i)*dcosphidb(j)+z2*dcosphidbdb(i,j)
      dphidcdc(i,j)=z1*dcosphidc(i)*dcosphidc(j)+z2*dcosphidcdc(i,j)
      dphidadb(i,j)=z1*dcosphida(i)*dcosphidb(j)+z2*dcosphidadb(i,j)
c     note dcosphidadc is non zero but dphidadc is zero - don't compute
      dphidadc(i,j)=z1*dcosphida(i)*dcosphidc(j)+z2*dcosphidadc(i,j)
      dphidbdc(i,j)=z1*dcosphidb(i)*dcosphidc(j)+z2*dcosphidbdc(i,j)
160   continue
      else
c*************************************************
c     CASE B - use expressions derived from sinphi
c     Compute the product of the antisymmetric tensor times a, b, and c
      call antsym(a,atilde)
      call antsym(b,btilde)
      call antsym(c,ctilde)
c     Compute second derivatives of S=b(a*v) with respect to a, b, and c.
      b3=b1*b2
      do 210 i=1,3
      do 210 j=1,3
      dsdbdb(i,j)=(delta(i,j)/b1-b(i)*b(j)/b3)*av
     >            -(acrossc(i)*b(j)+acrossc(j)*b(i))/b1
      dsdadb(i,j)=v(i)*b(j)/b1+b1*ctilde(i,j)
      dsdadc(i,j)=-b1*btilde(i,j)
      dsdbdc(i,j)=b(i)*u(j)/b1+b1*atilde(i,j)
210   continue
c     Compute second derivatives of sinphi with respect to a, b, and c.
      do 220 i=1,3
      do 220 j=1,3
      dsinphidada(i,j)=
     >                +dfda(i)*dsda(j)+dsda(i)*dfda(j)+s*dfdada(i,j)
      dsinphidbdb(i,j)=
     >  dsdbdb(i,j)*f+dfdb(i)*dsdb(j)+dsdb(i)*dfdb(j)+s*dfdbdb(i,j)
      dsinphidcdc(i,j)=
     >                +dfdc(i)*dsdc(j)+dsdc(i)*dfdc(j)+s*dfdcdc(i,j)
      dsinphidadb(i,j)=
     >  dsdadb(i,j)*f+dfda(i)*dsdb(j)+dsda(i)*dfdb(j)+s*dfdadb(i,j)
      dsinphidadc(i,j)=
     >  dsdadc(i,j)*f+dfda(i)*dsdc(j)+dsda(i)*dfdc(j)+s*dfdadc(i,j)
      dsinphidbdc(i,j)=
     >  dsdbdc(i,j)*f+dfdb(i)*dsdc(j)+dsdb(i)*dfdc(j)+s*dfdbdc(i,j)
220   continue
c     Compute second derivatives of phi with respect to a, b, and c.
      z1=sinphi/cosphi**3
      z2=1.d0/cosphi
      do 230 i=1,3
      do 230 j=1,3
      dphidada(i,j)=z1*dsinphida(i)*dsinphida(j)+z2*dsinphidada(i,j)
      dphidbdb(i,j)=z1*dsinphidb(i)*dsinphidb(j)+z2*dsinphidbdb(i,j)
      dphidcdc(i,j)=z1*dsinphidc(i)*dsinphidc(j)+z2*dsinphidcdc(i,j)
      dphidadb(i,j)=z1*dsinphida(i)*dsinphidb(j)+z2*dsinphidadb(i,j)
c     note dsinphidadc is non zero but dphidadc is zero - don't compute
      dphidadc(i,j)=z1*dsinphida(i)*dsinphidc(j)+z2*dsinphidadc(i,j)
      dphidbdc(i,j)=z1*dsinphidb(i)*dsinphidc(j)+z2*dsinphidbdc(i,j)
230   continue
      end if
 
c     Add contributions to second derivative matrix
c     Derivative with respect to r1,r1:
      do 21 i=1,3
      do 21 j=1,i
      index=lowtri(i,j)
      d2phidx2(index)=dphidada(i,j)
21    continue
c     Derivative with respect to r2,r2:
      do 22 i=1,3
      do 22 j=1,i
      index=lowtri(i+3,j+3)
      d2phidx2(index)=dphidada(i,j)+dphidbdb(i,j)
     >               -dphidadb(i,j)-dphidadb(j,i)
22    continue
c     Derivative with respect to r3,r3:
      do 23 i=1,3
      do 23 j=1,i
      index=lowtri(i+6,j+6)
      d2phidx2(index)=dphidbdb(i,j)+dphidcdc(i,j)
     >               -dphidbdc(i,j)-dphidbdc(j,i)
23    continue
c     Derivative with respect to r4,r4:
      do 24 i=1,3
      do 24 j=1,i
      index=lowtri(i+9,j+9)
      d2phidx2(index)=dphidcdc(i,j)
24    continue
c     Derivative with respect to r1,r2:
      do 25 i=1,3
      do 25 j=1,3
      index=lowtri(j+3,i)
      d2phidx2(index)=-dphidada(i,j)+dphidadb(i,j)
25    continue
c     Derivative with respect to r1,r3:
      do 26 i=1,3
      do 26 j=1,3
      index=lowtri(j+6,i)
      d2phidx2(index)=-dphidadb(i,j)+dphidadc(i,j)
26    continue
c     Derivative with respect to r1,r4:
      do 27 i=1,3
      do 27 j=1,3
      index=lowtri(j+9,i)
      d2phidx2(index)=-dphidadc(i,j)
c
c     note: dphidadc is always zero - this array and its computation
c     should be removed later
c
c      write(*,887) i,j,index,dphidadc(i,j)
c887   format(' r1,r4 block: i,j=',2i2,' index=',i4,' dphidadc=',f10.4)
27    continue
c     Derivative with respect to r2,r3:
      do 28 i=1,3
      do 28 j=1,3
      index=lowtri(j+6,i+3)
      d2phidx2(index)=
     >               +dphidadb(i,j)-dphidadc(i,j)
     >               -dphidbdb(i,j)+dphidbdc(i,j)
28    continue
c     Derivative with respect to r2,r4:
      do 29 i=1,3
      do 29 j=1,3
      index=lowtri(j+9,i+3)
      d2phidx2(index)=dphidadc(i,j)-dphidbdc(i,j)
29    continue
c     Derivative with respect to r3,r4:
      do 30 i=1,3
      do 30 j=1,3
      index=lowtri(j+9,i+6)
      d2phidx2(index)=dphidbdc(i,j)-dphidcdc(i,j)
30    continue
 
c     code for three atoms colinear
      if(ierror.eq.1) then
        do 45 i=1,78
45      d2phidx2(i)=0.d0
      endif
 
      endif
 
 
      end
c
c     Subroutine to compute the product of the antysimmetric
c     tensor times a vector. The result is an antisymmetric tensor.
      subroutine antsym(x,xtilde)
      implicit real*8 (a-h,o-z)
      dimension x(3),xtilde(3,3)
      xtilde(1,1)=0.d0
      xtilde(2,2)=0.d0
      xtilde(3,3)=0.d0
      xtilde(1,2)=x(3)
      xtilde(2,3)=x(1)
      xtilde(3,1)=x(2)
      xtilde(2,1)=-x(3)
      xtilde(3,2)=-x(1)
      xtilde(1,3)=-x(2)
      return
      end

c
c     subroutine to parse a string into an element field and up to 3 integers
c     unused fields will be returned with values of -1
c
      subroutine parser(string,elfield,lelfield,i1,i2,i3)
      character*(*) string,elfield
      character*120 line
      character*10 numbers
      dimension is(3)
      data numbers/'1234567890'/

      do i=1,3
        is(i)=-1
      enddo
      line=string
      call leftjust(line)
      ne=2 !assume two character element name
      ifnum=index(numbers,line(2:2))
      if(line(2:2).eq.' '.or.line(2:2).eq.'_'.or.ifnum.ne.0) ne=1
      lelfield=ne
      elfield=line(1:ne)

      do i=1,3
        if(line(ne+1:ne+1).eq.'_') then
c         assume there is a number field after the underscore
c         look for next space or underscore
          l=index(line(ne+2:),'_')
          if(l.gt.0) then
            read(line(ne+2:ne+l),*) is(i)
            ne=ne+l
          else
            read(line(ne+2:),*) is(i)
            go to 10
          endif
        endif
      enddo

10    continue

      i1=is(1)
      i2=is(2)
      i3=is(3)

      end


c
c     left justify a string
c
      subroutine leftjust(string)
      character*(*) string

      is=0
      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 10
        endif
      enddo

10    continue
      if(is.eq.0) return  !all blank string

      do i=1,len(string)-is+1
        string(i:i)=string(is+i-1:is+i-1)
      enddo
c     blank out the last is-1 characters
      do i=len(string)-is+2,len(string)
        string(i:i)=' '
      enddo

      end


c
c     subroutine lstchr returns the index of the last nonblank character in a string
c
      subroutine lstchr(string,ilst)
      character*(*) string 

      ilst=0  !if string is all blank, return zero
      do i=len(string),1,-1
        if(string(i:i).ne.' ') then
          ilst=i
          go to 10
        endif
      enddo
10    continue

      end


c
c     subroutine to read a set of coordinates from an xyz file
c     expect this to have a set of coords, we are usually interested
c     in either the first or the last
c
      subroutine getcoord(fname,nsites,itype,coord)
      implicit real*8 (a-h,o-z)
      character*120 line
      character*(*) fname
      dimension coord(3,nsites),itype(nsites)

      write(*,'('' Reading coordinates from file *'',a,''*'')')
     x     fname(1:40)

      open(11,file=fname)  
c
c     file format is 
c     line 1: number
c     line 2: Atoms  
c     line 3,... for each atom a type and three coords
c     this is repeated for as many frames
c
c     first count how many frames save line number of last
c
      nframe=0
10    continue
      read(11,'(a120)',end=50) line
      nframe=nframe+1
      read(line,*) nat
      if(nat.ne.nsites) then
        write(*,'('' Error:  xyz and lmp not matched '',/,
     x    '' Files have different number of sites:  N(xyz) = '',
     x    i6,''; N(lmp) = '',i6)') nat,nsites
        stop 'xyz file not matched with cooresponding lmp file'
      endif
      read(11,'(a120)') line   !line has "Atoms"
      do i=1,nat
        read(11,'(a120)') line
        read(line,*) it
        if(it.ne.itype(i)) then
          write(*,'('' Type mismatch between xyz and lmp files '')')
          write(*,'('' Atom number '',i6,
     x      '' Type(xyz) = '',i4,'' Type(lmp) = '',i4)') i,it,itype(i)
          stop 'xyz file and lmp file have different atoms types'
        endif
      enddo
      go to 10


50    continue
      iframe=nframe
      write(*,'('' Selecting frame '',i5,'' from a total'',
     x          '' of '',i5)') iframe,nframe

      rewind(11)
c     position file to right place
      do i=1,(2+nsites)*(iframe-1)
        read(11,'(a120)') line
      enddo
c     now read into coord
      read(11,'(a120)') line
      read(11,'(a120)') line
      do i=1,nat
        read(11,'(a120)') line
        read(line,*) it,(coord(k,i),k=1,3)
      enddo
c

      close(11)

      write(*,'('' Frame '',i5,'' has been read'')')
     x  iframe



      end

