c
c    program to read a bondlist file and spit out the new bonds
c    version 8/23/2012
c    add code to identify degree of saturation:  (new bonds)-(lactones)
c
c    Nov 2012 code added to support the possibility that not all molecules of a given
c    type are stored together.
c
c    Dec 2012 implement a special gelcore-project mode
c    -find most recent *.bonds file in a series given a name template
c    -find out the number of molecules in the last block given info in *.bonds
c    -decide if molecules need to be added based on a target
c    -generate the required *.mxin and *.fxlmp input files
c
c    Feb 2023 implement ability to process packed *.bonds files produced from applying
c    bondpack.f to *.bonds to produce *.bonds.pack
c
c
      implicit real*8 (a-h,o-z)
      character*256 line,argument
      character*256 fnbond,fnclus
      dimension ibond(72500,3)    !dimension to number of new bonds in system
      dimension ibndseq(72500,5)  !dimension to number of new bonds in system
                                  !column 5 indicates the bond is of type 1 (to lactone monomer)
                                  !column 2 (to a different cluster); 3 (same cluster)
      dimension nlist(50000,6)    !dimension to number of molecules
      dimension molecule(500000,2)!dimension to number of sites - mol index and mol type for each site
      dimension ladder(2000,2)    !dimension to count doubly bonded lactide pairs      
      dimension mstate(50000,5)   !dimension to number of molecules
                                  !gives active, activated, inactive status
      dimension mhistory(50000)   !dimension to number of molecules
                                  !indicate if a molecule has been included in a large cluster > nclssz
      dimension mcluster(50000)   !indicate for each molecule the cluster number it was in after 
                                  !previous call to cluster (0 means monomer)

      dimension molblock(50,2)    !for up to 50 blocks of molecule coordinates, the type and number in the block
                                  !this is to support additional blocks of coordinates from addition of material
                                  !note that this make molecule counts read in redundant info

      dimension mcomp(200,200)    !array to collect statistics on the composition of clusters
                                  !column i describes i-mers

      character*256 fngcbond,fngcsave !for gelcore project mode
      logical there               !for gelcore project mode

 
      nbmax=72500 !dimension of ibond and ibndseq
      nmolmx=50000  !leading dimension of nlist; max number of molecules
      natmmx=500000 !leading dimension of molecule; max number of sites     
      ibseqmx=72500 
      nladmx=2000
      nclssz=100  !default size for writing information about clusters to file fnclus
      mcompmx=200  !size of mcomp array
      ifclsopn=0  !flag for whether the large cluster file has been opened
      ifcallcls=0 !0=cluster subroutine never called; new bonds are all to monomers (type 1)
      ifgcproj=0  !flag for whether this is part of a gelcore project cycle


      call getarg(1,argument)
      is=index(argument,' ')-1
c     write (*,'(''argument is *'',a,''*'')') argument(1:is)  
      if(argument(is-4:is).ne.'.gbin') then
        write(*,'(''Error:  program is called with *.gbin '',
     x ''filename on command line'')')
        stop 'error: want *.gbin filename on command line'
      endif


      open(9,file=argument(1:is))
      open(11,file=argument(1:is-5)//'.gbout')
      write(11,'(''Bond analysis '')')
      write(11,'(''Input file:         '',a)') 
     x  argument(1:is)
      write(11,'(''Output file:        '',a)') 
     x  argument(1:is-5)//'.gbout'
      write(11,'(''Large cluster file: '',a)') 
     x  argument(1:is-5)//'.gblrg'
      fnclus=argument(1:is-5)//'.gblrg'

      read(9,'(a256)') line
      read(line,*) ntypes
      if (ntypes.ne.2) stop

      call leftjust(line)    !this part reads a cluster size threshhold for printing 
      is=index(line,' ')     !out extra info about larger clusters into a file *.gblrg
      call leftjust(line(is+1:))  !default is established above
      if(line(is+1:is+1).ne.' ') read(line(is+1:),*) nclssz

      write(11,'(''Number of types of molecules '',i5)') ntypes

      read(9,*) nattp1, nrestp1, masstp1
c     write(11,'(''Number of molecules of type 1          '', i5,
c    x  '' (ntype1)'')') ntype1
      write(11,'(''Number of sites in molecules of type 1 '', i5,
     x  '' (nattp1)'')') nattp1
      write(11,'(''Number of residues in molecules type 1 '', i5,
     x  '' (nrestp1)'')') nrestp1
      write(11,'(''Mass of molecules of type 1            '', i5,
     x  '' (masstp1)'')') masstp1

      read(9,*) nattp2, nrestp2, masstp2
c     write(11,'(''Number of molecules of type 2          '', i5,
c    x  '' (ntype2)'')') ntype2
      write(11,'(''Number of sites in molecules of type 2 '', i5,
     x  '' (nattp2)'')') nattp2
      write(11,'(''Number of residues in molecules type 2 '', i5,
     x  '' (nrestp2)'')') nrestp2
      write(11,'(''Mass of molecules of type 2            '', i5,
     x  '' (masstp2)'')') masstp2
      write(11,'(''Information about clusters larger than '',i4, 
     x  '' will be written the large cluster file.'')') nclssz




      ifirst=1 !signal for the first pass through the loop over *.bonds files
      ibseq=0
5     continue
      read(9,'(a256)',end=500) line
      call leftjust(line)    
      is=index(line,' ')-1
      if(is.eq.0) go to 500

c     situations that are possible:
c     1) line in *.gbin file is a proper *.bonds file
c     2) line in *.gbin file is a *.bonds.pack file (processed with bondpack.f)
c     3) line in *.gbin file has an asterisk - build the *.bonds file from the *.gbin file name
c     4) line in *.gbin file has an asterisk and a "pack" string (*.bonds.pack) - build the file name
c
c     the bond file name mentioned in this line of input will trigger the
c     gelcore project mode by having an asterisk in the name
c     when this is the case, we will construct the bond file name from the
c     *.gbin file on the command line
c     note that the *.bonds file listed in *.gbin will be ignored (except 
c     for noting the asterisk) the name will be constructed consistent with 
c     the name of the *.gbin file
      ifpack=0
      if(index(line(1:is),'*').ne.0) then
        ifgcproj=1
        if(index(line,'pack').ne.0) ifpack=1   !look anywhere on the line for the pack string
c       find the current *.bonds file
        is=index(argument,' ')-1
        idot=index(argument,'.gbin')
        fngcbond=argument(1:idot)//'bonds'
        if(ifpack.eq.1) fngcbond=argument(1:idot)//'bonds.pack'

c       iast=index(line(1:is),'*') !asterisk location within line(1:is)
c       do i=1,999 
c         if(i.le.9) then 
c           write(fngcbond,'(a,i1,a)') line(1:iast-1),i,line(iast+1:is)
c           idigit=1
c         elseif(i.le.99) then
c           write(fngcbond,'(a,i2,a)') line(1:iast-1),i,line(iast+1:is)
c           idigit=2
c         else
c           write(fngcbond,'(a,i3,a)') line(1:iast-1),i,line(iast+1:is)
c           idigit=3
c         endif
c         inquire(file=fngcbond,exist=there)
c         if(there) then
c           num=i
c           isave=idigit
c           fngcsave=fngcbond
c         else
c           go to 20
c         endif
c       enddo
c0      continue
c       fngcbond=fngcsave
       
        is=index(fngcbond,' ')-1 
        write(11,'(/,''Program getbond in gelcore project mode '',/,
     x    ''Bond file name template (in gbin):  '',a,/,
     x    ''Highest indexed existing bond file: '',a)')
     x    line(1:is),fngcbond(1:is)
c       extract num from the field between dots just before the *.gbin
        idot0=0
        do i=idot-1,1,-1
          if(idot0.eq.0.and.argument(i:i).eq.'.') idot0=i
        enddo
        read(argument(idot0+1:idot-1),*) num
        write(11,'(''Index extracted from *.gbin file name '',i5)') num
        line(1:)=fngcbond
        is=index(line,' ')-1
      endif
c


      write(11,'(''Bond file: '',a)') line(1:is)
      ifpack=0
      if(index(line,'pack').ne.0) ifpack=1   !look for pack string anywhere on the line



      open(10,file=line(1:is))
      fnbond=line
      ifsub=0
      if(line(is+1:is+15).ne.'               ') ifsub=1
      if(ifsub.eq.1) then
        read(line(is+1:),*) istart,iend
        write(11,'(''Processing subset of file from time '',i10,
     x  '' to time '',i10)')
     x  istart, iend
      endif 

c     in gelcore project mode find the last time associated with this bond file
      if(ifgcproj.eq.1) then
        mxatix=0
        ngcpack=0   !counter for number of bonds in a packed file
24      read(10,'(a120)',end=26) line
        if(index(line,'TIMESTEP').ne.0) read(10,*) istart 
        if(index(line,'NUMBER').ne.0) read(10,*) ngcbond
        if(index(line,'ITEM: BONDS').ne.0.and.ifpack.eq.1) then
          ngcpack=ngcpack+ngcbond
          do i=1,ngcbond
            read(10,*) i1,i2,i3,i4
            mxatix=max(mxatix,i3,i4)
          enddo
        endif
        go to 24

26      continue
        if(ifpack.eq.1) then
          ngcbond=ngcpack  !total number of bonds at the end
        elseif(ifpack.eq.0) then !just read the last block of bonds
          backspace(10)
          mxatix=0
          do i=1,ngcbond
            backspace(10)
            read(10,*) i1,i2,i3,i4
            backspace(10)
c           write(11,'(''Backing through final bond list: '',
c    x        5i8)')
c    x        ngcbond-i+1,i1,i2,i3,i4
            mxatix=max(mxatix,i3,i4)
          enddo
        endif

        rewind(10)
        ifsub=1
        iend=istart
        write(11,'(''In gelcore project mode, use last bond'',
     x    '' set from time '',i10)') istart
        write(11,'(''In gelcore mode, last bond set size: '',i8)')
     x     ngcbond
        write(11,'(''In gelcore mode, maximum atom index: '',i8)')
     x     mxatix 
 
      endif




c     new required input line
c     note that each new *.bonds file can have a different number of molecules,
c     but assume we always add molecules only to the last block, keeping
c     all data structures in tact
      if(ifirst.eq.1) then
        read(9,*) nblocks,(molblock(i,1),molblock(i,2),i=1,nblocks)
        write(11,'(''Number of blocks of contiguous molecule types '',
     x    i5)') nblocks

        if(ifgcproj.eq.1) then
c         there must be three blocks, the third block must have the
c         type of the first, the number of molecules in the third type
c         is computed from the largest atom index in the *.bonds file
          if(nblocks.ne.3.or.molblock(1,1).ne.molblock(3,1).or.
     x      molblock(3,1).ne.1) then
            write(11,'(''Error:  in gelcore project mode '',
     x        ''must have exactly 3 blocks'',/,''and types of '',
     x        ''first and third blocks must match '',
     x        ''nblocks = '',i3,''; type of first = '',i2,
     x        ''; type of second = '',i2)')
     x        nblocks,molblock(1,1),molblock(3,1)
            stop 'error in gelcore project mode' 
          endif
          molblock(3,2)=(mxatix-(molblock(1,2)*nattp1 
     x                         + molblock(2,2)*nattp2))
          if(mod(molblock(3,2),nattp1).ne.0) then
            write(11,'(''Error:  number of remaining sites '',
     x        ''in third block is '',i6,/,''and this is '',
     x        ''not a multiple of '',i6,'', the number of '',
     x        /,''sites in molecules of the first type.'')')
     x        molblock(3,2),nattp1
            stop 'Error in number of remaining sites'
          endif
          molblock(3,2)=molblock(3,2)/nattp1
          write(11,'(''In gelcore mode, number of molecules in third'',
     x      '' block is '',i5)') molblock(3,2)
        endif



        ntype1=0
        ntype2=0
        do i=1,nblocks
          if(molblock(i,1).eq.1) ntype1=ntype1+molblock(i,2)
          if(molblock(i,1).eq.2) ntype2=ntype2+molblock(i,2)
          write(11,'(''Block '',i2,'':  '',i6,'' molecules of type '',
     x    i2)')  i,molblock(i,2),molblock(i,1)
        enddo
      else
        read(9,'(a120)') line 
        read(line,*) nb
        if(nb.ne.nblocks) then
          write(11,'(''Error: can not change the number'',
     x      '' of blocks, only the size of the last block.'')')
          stop 'Illegal change in number of blocks of molecule types'
        endif
        call leftjust(line)
        is=index(line,' ')+1
        call leftjust(line(is:))
        do i=1,nb-1
          read(line(is:),*) i1,i2
          if(i1.ne.molblock(i,1).or.i2.ne.molblock(i,2)) then
            write(11,'(''Error: can not change the order or size'',
     x        '' of blocks,'',
     x        '' except for the size of the last block.'')')
            stop 
     x    'Illegal change in size or type of blocks of molecule types'
          endif
          is=index(line(is:),' ')+is
          call leftjust(line(is:))
          is=index(line(is:),' ')+is
          call leftjust(line(is:))
        enddo
        read(line(is:),*) i1,i2
        if(i1.ne.molblock(nb,1)) then
          write(11,'(''Error: can not change the type of the last'',
     x      '' block.'')')
          stop 'Illegal change in type of last block of molecule types'
        endif
        if(i2.lt.molblock(nb,2)) then
          write(11,'(''Error: can not decrease size of the last'',
     x      '' block,'')')
          stop 'Illegal change in size of last block of molecule types'
        elseif(i2.gt.molblock(nb,2)) then
          write(11,'(''Increasing number of molecules in last block '',
     x      ''by '',i4,'' from '',i6,'' to '',i6)') 
     x    i2-molblock(nb,2),molblock(nb,2),i2
          if(molblock(nb,1).eq.1) ntype1=ntype1+(i2-molblock(nb,2))
          if(molblock(nb,1).eq.2) ntype2=ntype2+(i2-molblock(nb,2))
          molblock(nb,2)=i2
        endif
      endif

      ifirst=0   !flag to signify first pass through the loop over *.bonds files

      if(ntype1+ntype2.gt.nmolmx) then
        write (11,'(''too many molecules, check nmolmx'')')
        stop 'too many molecules'
      endif 

      if(ntype1*nattp1 + ntype2*nattp2 .gt.natmmx) then
        write (11,'(''Error:  too many sites, check natmmx'')')
        stop 'too many sites, check natmmx'
      endif 

c
c     build data structure holding the molecule index and molecule type
c     for each site
c
      iaptr=0
      imptr=0
      do i=1,nblocks
        do j=1,molblock(i,2)
          imptr=imptr+1
          if(molblock(i,1).eq.1) k2=nattp1
          if(molblock(i,1).eq.2) k2=nattp2
          do k=1,k2     
            iaptr=iaptr+1
            molecule(iaptr,1)=imptr
            molecule(iaptr,2)=molblock(i,2)
          enddo
        enddo
      enddo



c     it is useful to know the initial time and the time between samples
c     in principle this can be different for each of the *.bonds files
      timeini=-1.d0
      deltime=+0.d0


10    continue
      read(10,'(a256)',end=100) line
      is=index(line,'TIMESTEP')
      if(is.eq.0) go to 10
      read(10,*) time

      if(deltime.eq.0.d0.and.timeini.ne.-1.d0) then
        deltime=time-timeini
      endif
      if(timeini.eq.-1.d0) then
        timeini=time
      endif

      read(10,'(a256)',end=100) line
      read(10,*) nbonds
      write(11,'(''Time '',f20.1,
     x  '' Number of bonds '',i10)') time,nbonds

      if(ifsub.eq.1) then
        itime=time
        if(itime.lt.istart.and.ifpack.eq.0) then
          write(11,'(''Skipping this block; time less than range'')')
          go to 10
        elseif(itime.gt.iend) then     
          write(11,'(''Skipping rest of time series in this file'')')
          close(10)
          go to 5
        endif
      endif
      

      read(10,'(a256)',end=100) line   !reads the string "BONDS"
      icount=0
      do ibnd=1,nbonds
        read(10,*) i1,i2,i3,i4
        if(i2.eq.13.or.i2.eq.14) then
c          write(*,'(f20.10,5x,3i10)') time,i1,i3,i4
          icount=icount+1
          if(icount.gt.nbmax) then
            write(11,'(''Error:  Not enough storage; '',
     x        ''increase size of ibond/ibndseq.'')')
            stop 'Increase size of arrays'
          endif
          ibond(icount,1)=i3
          ibond(icount,2)=i4
          ibond(icount,3)=i2  !type = 13 or 14
        endif
      enddo



      itimelst=time   !this will hold the time of the last set of bonds examined
                      !even if none are new (and, hence, not the same as the
                      !time at which the last bond was found)

      do i=1,icount
        if(ibond(i,1).gt.ibond(i,2)) then
          isave=ibond(i,1)
          ibond(i,1)=ibond(i,2)
          ibond(i,2)=isave
        endif
c       has this one been seen before?
        ifseen=0
        do j=1,ibseq
          if(ibond(i,1).eq.ibndseq(j,1).and.
     x       ibond(i,2).eq.ibndseq(j,2)) then
             ifseen=1
             go to 125
         endif
        enddo
125     continue
        if(ifseen.eq.0) then
          if (ibseq+1.gt.ibseqmx) then
           write(*,'(''increase ibndseq ibseqmx equals '', i6)') ibseqmx
          endif
          ibseq=ibseq+1
          ibndseq(ibseq,1)=ibond(i,1)
          ibndseq(ibseq,2)=ibond(i,2)
          ibndseq(ibseq,3)=int(time)
          ibndseq(ibseq,4)=ibond(i,3)   !type 
          if(ifcallcls.eq.0) then
            ibndseq(ibseq,5)=1  !bond type 0 (unknown), 1 (to lactone), 2 (to dift cluster), 3 (to same cluster)
          else
c           imol=molnum(ibndseq(ibseq,1),ntype1,nattp1,ntype2,nattp2)
c           jmol=molnum(ibndseq(ibseq,2),ntype1,nattp1,ntype2,nattp2)
            imol=molecule(ibndseq(ibseq,1),1)   !use the new data structure instead
            jmol=molecule(ibndseq(ibseq,2),1)   !use the new data structure instead
            if(mcluster(imol).eq.0.and.mcluster(jmol).eq.0)  then
c             this is the case of the first link of an arm an a solvent lactone
              ibndseq(ibseq,5)=1  !bond type 0 (unknown), 1 (to lactone), 2 (to dift cluster), 3 (to same cluster)
            elseif(mcluster(imol).eq.0) then
c             here assume imol is not in a cluster, but jmol is
c             if(imol.le.ntype1) then
              if(molecule(ibndseq(ibseq,1),2).eq.1) then   !assumes type 1 is lactones
                ibndseq(ibseq,5)=1 !cluster with jmol gets a lactone from solvent
              else
                ibndseq(ibseq,5)=2 !cluster with jmol gets a new arm from solvent (count as crosslink)
              endif
              mcluster(imol)=mcluster(jmol) !not sure this does any good 
            elseif(mcluster(jmol).eq.0) then
c             here assume jmol is not in a cluster, but imol is
c             if(jmol.le.ntype1) then
              if(molecule(ibndseq(ibseq,2),2).eq.1) then   !assumes type 1 is lactones
                ibndseq(ibseq,5)=1 !cluster with imol gets a lactone from solvent
              else
                ibndseq(ibseq,5)=2 !cluster with imol gets a new arm from solvent (count as crosslink)
              endif
              mcluster(jmol)=mcluster(imol) !not sure this does any good
            else
c             here if both were in clusters
              if(mcluster(imol).eq.mcluster(jmol)) then
c               here if we see an internal crosslinking bond
                ibndseq(ibseq,5)=3 !intra cluster bond                                                  
              else
c               here if we see a bond that joins two clusters (a true crosslinking)
                ibndseq(ibseq,5)=2 !inter cluster bond                                                  
              endif
            endif

c              this patch makes it so that the number of bonds to lactones is never bigger than the number of
c              lactones in all clusters; without this, there are situations when a solvent lactone can bond to
c              the same or even differnet clusters in the same time (differnt ends of it) and then it looks like
c              there are more solvent lactones than there should be
c prob wrong   mhistory(imol)=1
c              mhistory(jmol)=1

          endif
        endif
      enddo


      if(itime.lt.istart.and.ifpack.eq.1) go to 10

c     do i=1,icount-1
c       do j=i+1,icount
c         if(ibond(i,1).gt.ibond(j,1)) then
c           isave=ibond(i,1)
c           ibond(i,1)=ibond(j,1)
c           ibond(j,1)=isave
c           isave=ibond(i,2)
c           ibond(i,2)=ibond(j,2)
c           ibond(j,2)=isave
c           note: should flip the type info, too
c         endif
c       enddo
c     enddo
c     do i=1,icount
c       write(*,'(''New bond '',i10,2i5)') i,ibond(i,1),ibond(i,2)
c     enddo

      nlad=0

c     make a molecule-based neighbor list
      do i=1,nbmax
        nlist(i,1)=0
      enddo
      
      do i=1,nmolmx
        do j=1,4
          mstate(i,j)=0
        enddo
      enddo


      do i=1,ibseq
c       imol=molnum(ibndseq(i,1),ntype1,nattp1,ntype2,nattp2)
c       jmol=molnum(ibndseq(i,2),ntype1,nattp1,ntype2,nattp2)
        imol=molecule(ibndseq(i,1),1)
        jmol=molecule(ibndseq(i,2),1)

        if(nlist(imol,1)+1.gt.4) then
          write(11,'('' Warning:  molecule '',i5,'' seems to '',
     x      ''have too many bonds:  '',i2)')
     x      imol,nlist(imol,1)+1
        endif

        if(nlist(jmol,1)+1.gt.4) then
          write(11,'('' Warning:  molecule '',i5,'' seems to '',
     x      ''have too many bonds:  '',i2)')
     x      jmol,nlist(jmol,1)+1
        endif

c       see if these molecules are already bonded
c       alert if this is the case and dont add to list
        ifound=0
        do k=1,nlist(imol,1)
          if(nlist(imol,k+1).eq.jmol) ifound=1
        enddo
        if(ifound.eq.1) then
          write(11,'('' Warning:  two molecules doubly connected:  '',
     x      2i6)') imol,jmol
         if(nlad+1.gt.nladmx) then
           stop 'increase ladder array'
         endif
         nlad=nlad+1
          if(imol.lt.jmol) then
           ladder(nlad,1)=imol
           ladder(nlad,2)=jmol
          else
           ladder(nlad,1)=jmol
           ladder(nlad,2)=imol
          endif
        else
          j=nlist(imol,1)+1
          nlist(imol,1)=j
          nlist(imol,j+1)=jmol
          j=nlist(jmol,1)+1
          nlist(jmol,1)=j
          nlist(jmol,j+1)=imol
        endif


c       mstate holds, for each molecule, the number of new bonds it has
c       this can be 0, 1 or 2
        if (mstate(imol,1).eq.0) then
c         no bonds so far; this is the first
          mstate(imol,1)=ibndseq(i,1) 
          mstate(imol,2)=1
        elseif (mstate(imol,1).eq.ibndseq(i,1)) then
c         at least one bond so far; it is to this site so increase the counter
          mstate(imol,2)=2
        elseif(mstate(imol,3).eq.0) then
c         the bond is to the other site
          mstate(imol,3)=ibndseq(i,1)
          mstate(imol,4)=1                  
        elseif(mstate(imol,3).eq.ibndseq(i,1)) then
c         the bond is to the second site and there is already one there
          mstate(imol,4)=2                  
        else
          stop 'logic error'
        endif


        if (mstate(jmol,1).eq.0) then
c         no bonds so far; this is the first
          mstate(jmol,1)=ibndseq(i,2) 
          mstate(jmol,2)=1
        elseif (mstate(jmol,1).eq.ibndseq(i,2)) then
c         at least one bond so far; it is to this site so increase the counter
          mstate(jmol,2)=2
        elseif(mstate(jmol,3).eq.0) then
c         the bond is to the other site
          mstate(jmol,3)=ibndseq(i,2)
          mstate(jmol,4)=1                  
        elseif(mstate(jmol,3).eq.ibndseq(i,2)) then
c         the bond is to the second site and there is already one there
          mstate(jmol,4)=2                  
        else
          stop 'logic error'
        endif




      enddo

c       do i=1,ntype1+ntype2
c        write(11,'('' Molecule '',i5,'' numnei '',i2,''  '',
c     x    5i5)')
c     x    i,nlist(i,1),(nlist(i,j+1),j=1,nlist(i,1))
c      enddo

c      do k=1,ntype1+ntype2
c        if(mstate(k,1).ne.0) then
c          write(*,'('' time '',i10,'' nmol '',i6,'' mstate: '',4i6)')
c     x      int(time),k,(mstate(k,l),l=1,4)
c        endif
c      enddo





      ifgbcls=0
      iprint=0
      iounit=11
      nmol=ntype1+ntype2
      call cluster(nlist,nmol,nmolmx,iprint,ifgbcls,
     x   ibndseq,ibseqmx,ibseq,iounit,argument,
     x   ntype1, nattp1, nrestp1, masstp1,ladder,nlad,nladmx,
     x   ntype2, nattp2, nrestp2, masstp2,fnbond,itimelst,
     x   mstate, mcluster,
     x   ifclsopn,fnclus,mhistory,nclssz,
     x   nblocks,molblock,molecule,natmmx,mcomp,mcompmx,
     x   ifgcproj,fngcbond)
      ifcallcls=1




      go to 10






100   continue
      close(10)
      go to 5
500   continue


c      write(*,'('' ibseq is '', i10)') ibseq 


      do i=1,ibseq
c        write(*,'('' ntype1'', i5,'' nattp1'', i5,'' ntype2'',
c     x  i5,'' nattp2'',i5)')
c     x  ntype1, nattp1, ntype2, nattp2 
c       imol=molnum(ibndseq(i,1),ntype1,nattp1,ntype2,nattp2)
c       jmol=molnum(ibndseq(i,2),ntype1,nattp1,ntype2,nattp2)
        imol=molecule(ibndseq(i,1),1)
        jmol=molecule(ibndseq(i,2),1)
c       residue information might be useful for viewing with VMD,
c       but is not important here
c       if it is important, this logic needs to be improved
c       easiest to add a column to molecule array to include the
c       residue number that each site belongs with
c       if(imol.le.ntype1) then
c         ires=imol
c       else
c         ires=ntype1+nrestp2*((imol-ntype1)-1)+1
c       endif
c       if(jmol.le.ntype1) then
c         jres=jmol
c       else
c         jres=ntype1+nrestp2*((jmol-ntype1)-1)+1
c       endif
c       write(11,'(''Bond '',i5,'' Time '',i10,5x,
c    x    i6,'' ('',i6,''/'',i6,'' ) '',
c    x    i6,'' ('',i6,''/'',i6,'' ) '',
c    x   '' Type '',i3,''(13/14)  Crosslink '',i1,
c    x   '' (1/2/3) '')') 
c    x  i,ibndseq(i,3),ibndseq(i,1),imol,ires,
c    x                 ibndseq(i,2),jmol,jres,
c    x   ibndseq(i,4),ibndseq(i,5)
        write(11,'(''Bond '',i5,'' Time '',i10,5x,
     x    i6,'' ('',i6,'' ) '',
     x    i6,'' ('',i6,'' ) '',
     x   '' Type '',i3,''(13/14)  Crosslink '',i1,
     x   '' (1/2/3) '')') 
     x  i,ibndseq(i,3),ibndseq(i,1),imol,
     x                 ibndseq(i,2),jmol,
     x   ibndseq(i,4),ibndseq(i,5)
      enddo



c
c     write the bonds.vs.time.dat file
c     since so many codes assume the first data point is the base time
c     and the second time is used with the first to get a time increment
c     we should write out data for these two times even if there are
c     no bonds on the first or on the first and second times
c
      tzero=ibndseq(1,3) !this is the time at which the first new bond was seen
      tnow=tzero 
      num13=0
      num14=0
      open(21,file='bonds.vs.time.dat')
      if(tzero.gt.timeini)
     x  write(21,'(f20.10 , 3i10)') timeini,num13,num14,num13+num14 
      if(tzero.gt.timeini+deltime)
     x  write(21,'(f20.10 , 3i10)') timeini+deltime,num13,num14,num13+num14 
      do i=1,ibseq
       if(ibndseq(i,3).ne.tnow) then
         write(21,'(f20.10 , 3i10)') tnow,num13,num14,num13+num14 
         tnow=ibndseq(i,3)
       endif
       if(ibndseq(i,4).eq.13) num13=num13+1
       if(ibndseq(i,4).eq.14) num14=num14+1
      enddo
      write(21,'(f20.10 , 3i10)') tnow,num13,num14,num13+num14 
      close(21)





c     make a neighbor list
      do i=1,nbmax
        nlist(i,1)=0
      enddo
      do i=1,ibseq
c       imol=molnum(ibndseq(i,1),ntype1,nattp1,ntype2,nattp2)
c       jmol=molnum(ibndseq(i,2),ntype1,nattp1,ntype2,nattp2)
        imol=molecule(ibndseq(i,1),1)
        jmol=molecule(ibndseq(i,2),1)

        if(nlist(imol,1)+1.gt.4) then
          write(11,'('' Warning:  molecule '',i5,'' seems to '',
     x      ''have too many bonds:  '',i2)') 
     x      imol,nlist(imol,1)+1
        endif 

        if(nlist(jmol,1)+1.gt.4) then
          write(11,'('' Warning:  molecule '',i5,'' seems to '',
     x      ''have too many bonds:  '',i2)') 
     x      jmol,nlist(jmol,1)+1
        endif 

c       see if these molecules are already bonded
c       alert if this is the case and dont add to list
        ifound=0
        do k=1,nlist(imol,1)
          if(nlist(imol,k+1).eq.jmol) ifound=1
        enddo
        if(ifound.eq.1) then
          write(11,'('' Warning:  two molecules doubly connected:  '',
     x      2i6)') imol,jmol
        else
          j=nlist(imol,1)+1
          nlist(imol,1)=j
          nlist(imol,j+1)=jmol
          j=nlist(jmol,1)+1
          nlist(jmol,1)=j
          nlist(jmol,j+1)=imol
        endif
      enddo
c     do i=1,ntype1+ntype2
c        write(*,'('' Molecule '',i5,'' numnei '',i2,''  '',
c     x    5i5)')
c     x    i,nlist(i,1),(nlist(i,j+1),j=1,nlist(i,1))
c     enddo


      
      ifgbcls=1
      nmol=ntype1+ntype2
      call cluster(nlist,nmol,nmolmx,iprint,ifgbcls,
     x   ibndseq,ibseqmx,ibseq,iounit,argument,
     x   ntype1, nattp1, nrestp1, masstp1,ladder,nlad,nladmx,
     x   ntype2, nattp2, nrestp2, masstp2,fnbond,itimelst,
     x   mstate, mcluster,
     x   ifclsopn,fnclus,mhistory,nclssz,
     x   nblocks,molblock,molecule,natmmx,mcomp,mcompmx,
     x   ifgcproj,fngcbond)


      if(ifclsopn.eq.1) close(22)



      end



      function molnum(i,ntype1,nattp1,ntype2,nattp2)
      write(*,'(''MOLNUM SHOULD NOT BE CALLED ANYMORE'')')
      stop 'molnum should not be called'
      n1=ntype1*nattp1
      n2=ntype2*nattp2
      ntot=n1+n2
      if(i.gt.ntot)then
      write(*,'(''i '', i5,'' ntype1'', i5,'' nattp1'', i5,
     x '' ntype2 '', i5,'' nattp2'', i5)') i,ntype1,nattp1,ntype2,nattp2
        stop 'i is gt ntot'
      endif
      if(i.le.n1) then
        molnum=1+(i-1)/nattp1
      else
        molnum=ntype1 + 1+(i-n1-1)/nattp2
      endif
c      write(*,'(''molnum '', i5)') molnum
c      write(*,'(''i '', i5,'' ntype1'', i5,'' nattp1'', i5,
c     x '' ntype2 '', i5,'' nattp2'', i5)') i,ntype1,nattp1,ntype2,nattp2
      return 
      end

c
c     routine to perform clustering given a neighbor list
c
      subroutine cluster(neigh,nmol,nmolmx,iprint,ifgbcls,
     x   ibndseq,ibseqmx,ibseq,iounit,argument,
     x   ntype1, nattp1, nrestp1, masstp1,ladder,nlad,nladmx,
     x   ntype2, nattp2, nrestp2, masstp2,fnbond,itimelst,
     x   mstate, mcluster,
     x   ifclsopn,fnclus,mhistory,nclssz,
     x   nblocks,molblock,molecule,natmmx,mcomp,mcompmx,
     x   ifgcproj,fngcbond)

      dimension neigh(nmolmx,6),mstate(nmolmx,5)
      dimension ibndseq(ibseqmx,5)
      dimension ladder(nladmx,2)
      character*(*) argument,fnbond,fnclus
      dimension mhistory(nmolmx)
      dimension mcluster(nmolmx)
      dimension molecule(natmmx,2),molblock(50,2)

      dimension mcomp(mcompmx,mcompmx)   !array to hold composition information for each cluster size


C
C     FOR SEPARATING SITES INTO CLUSERS
C
C     ICLUS GIVES FOR UP TO NCLMX CLUSTERS,
C     ICLUS(1,I) IS NUMBER OF ATOMS IN THE ITH CLUSTER
C     ICLUS(2,I) IS A POINTER TO FIRST ATOM OF CLUSTER I IN CLSLST
C     CLSIND IS THE CLUSTER INDEX FOR EACH ATOM
cC     LCLIND IS WHETHER THE NEIGHBORS OF A GIVEN ATOM ARE INCLUDED
cC     IN THE CLUSTER

      parameter (nmx=100000)  ! max number of molecules
      dimension ICLUS(6,nmx)  ! dimension to number of clusters (<nmx)
                              ! extra row (3) added to count number of new bonds
                              ! extra rows (4,5,6) to count bonds of types 1, 2, 3 (should sum to value in row 3)
      integer CLSIND(nmx),CLSLST(nmx)  ! dimension to number of molecules, nmx
cc      LOGICAL LCLIND(nmx)     ! dimension to number of molecules

      character*(*) fngcbond  !to support gelcore project mode
      character*120 line

      integer icheck(nmx)
      dimension isort(nmx,2)
      dimension hist(3,100)
      nhist=100

c
c     check neighbor list
c
      do i=1,nmol
        nn=neigh(i,1)
        if(nn.lt.0.or.nn.gt.4) then
          write(iounit,'(''Error:  number of neighbors incorrect '')')
          write(iounit,'(''Error:  molecule number '',i6)') i
          write(iounit,'(''Error:  number of neighbors '',i6)') nn
        endif
        do j=1,nn
          k=neigh(i,j+1)
          if(k.le.0.or.k.gt.nmol) then
            write(iounit,'(''Error:  failure in neighbor list check'')')
            write(iounit,'(''Error:  molecule number '',i6)') i
            write(iounit,'(''Error:  number of neigh '',i6)') neigh(i,1)
            write(iounit,'(''Error:  neighbor list '',10i6)') 
     x        (neigh(i,l+1),l=1,neigh(i,1))
          endif
        enddo
c       check for duplicates in molecule i list
        Do j=1,nn-1
          do k=j+1,nn
            if(neigh(i,j+1).eq.neigh(i,k+1)) then
              write(iounit,'(''Error:  duplicate in NL '')')
              write(iounit,'(''Error:  duplicate for '',i6)') i
              write(iounit,'(''Error:  list '',10i6)') 
     x          (neigh(i,l+1),l=1,neigh(i,1)) 
            endif
          enddo
        enddo
      enddo
c
c     check that if i in J's list that j is in i's list
c
      do i=1,nmol
        nn=neigh(i,1)
        do j=1,nn
          jmol=neigh(i,j+1)
          nnj=neigh(jmol,1)
          ifound=0
          do k=1,nnj
            if(i.eq.neigh(jmol,k+1)) ifound=1
          enddo
          if(ifound.eq.0) then
            write(iounit,'(''Error:  nlist check '')')
            write(iounit,'(''Error:  i,j lists inconsistent '')')
            write(iounit,'(''Error:  i,j = '',2i6)') i,jmol
            write(iounit,'(''Error:  list for '',i6)') i
            write(iounit,'(''Error:  '',10i6)') (neigh(i,kk+1),kk=1,nn)
            write(iounit,'(''Error:  list for '',i6)') jmol
            write(iounit,'(''Error:  '',10i6)') 
     x        (neigh(jmol,kk+1),kk=1,nnj)
          endif
        enddo

      enddo




c
c     end of checks
c



      if(nmol.gt.nmx) then
        write(iounit,'(''Error:  increase storage in '',
     x    ''cluster routine'')')
        stop 'Increase storage in cluster routine'
      endif

      nclmx=nmx   ! maximum number of clusters (size of iclus)

      iCLCNT=0  !count number of clusters found so far, current cluster index
      if(nmol.eq.0) go to 940

      DO I=1,nmol   
        CLSIND(I)=-1       ! initialize to unassigned
c       LCLIND(I)=.FALSE.  ! neighbors not assigned, either
      enddo


      INEXT=0   !next molecule for consideration

912   continue

C     FIND NEXT UNASSIGNED molecule
      DO 915 I=INEXT+1,nmol
      ISAVE=I
      IF(CLSIND(I).EQ.-1) GO TO 916
915   CONTINUE

      GO TO 940  !if all molecules assigned

916   INEXT=ISAVE   ! next molecule for consideration

      IF(ICLCNT+1.GT.NCLMX) then
        write(iounit,'(''Error:  increase storage in cluster '',
     x    ''routine'')') 
        write(iounit,'(''Error:  need to hold more than nclmx '',
     x    ''clusters'')') 
        stop 'Increase storage in cluster routine; too many clusters'
      endif        

      ICLCNT=ICLCNT+1
      NCLUST=1    ! number of sites found in this cluster
      CLSIND(INEXT)=ICLCNT
c      LCLIND(INEXT)=.TRUE.   !means that neighbors of atom inext have been included
      IFRST=1
      ILAST=0
c     first site of cluster iclcnt is inext; add its neighbors to clslst
      n=neigh(inext,1)          
      DO J=1,n
        k=neigh(inext,j+1)   
        ifdup=0
        do l=1,j-1
          if(k.eq.neigh(inext,l+1)) ifdup=1
        enddo
        if(ifdup.eq.0) then
          NCLUST=NCLUST+1
          CLSIND(k)=ICLCNT
          ILAST=ILAST+1
          CLSLST(ILAST)=k
        endif
      enddo
c      write(*,'(''Cluster '',i5,'' seed molecule '',i5)')
c     x   iclcnt, inext
c      write(*,'(''Seed neighbors: clslst('',i5,'')'',i5)') 
c     x   (kk,clslst(kk),kk=1,ilast)


C     SCAN LIST FOR ATOMS IDENTIFIED IN CLUSTER ICLCNT, WHOSE NEIGHBORS
C     ARE not yet assigned to this cluster

921   CONTINUE
      IF((ILAST-IFRST+1).EQ.0) GO TO 930
      I=CLSLST(IFRST)
c      LCLIND(I)=.TRUE.
      n=neigh(i,1)          
c      write(*,'(''Considering neighbors of clslst('',i5,'') '',
c     x  i5,'':'',5i6)') ifrst,i,(neigh(i,kk+1),kk=1,neigh(i,1))
      DO 927 J=1,N
        k=neigh(i,j+1) 
c        write(*,'(''Neighbor '',i3,'' index '',i5)') j,k
        IF(CLSIND(k).EQ.-1) THEN
          NCLUST=NCLUST+1
          cLSIND(k)=ICLCNT
C ADD TO CLSLST IF NOT ALREADY THERE
          DO l=IFRST,ILAST
            IF(k.EQ.CLSLST(l)) GO TO 926
          enddo
          IF(ILAST+1.GT.NMX) THEN
            write(iounit,'(''Shifting clslst; ifrst,ilast '',2i5)')
     x        ifrst,ilast
            DO L=1,(ILAST-IFRST+1)
              CLSLST(L)=CLSLST(IFRST+L-1)
            enddo
            ILAST=ILAST-IFRST+1
            IFRST=1
          endif
c          write(*,'(''Adding clslst('',i5,'')='',i5)')
c     x      ilast+1,k
          ILAST=ILAST+1
          CLSLST(ILAST)=k
926       continue           
        ELSE
C         IF(CLSIND(k).NE.-1) THEN
          IF(CLSIND(k).NE.ICLCNT) then
            WRITE(iounit,'(''ERROR - Molecule IDENTIFIED'',
     X      '' AS BELONGING TO TWO DIFFERENT CLUSTERS '')')
            write(iounit,'(''ERROR - molecule index '',i5)')
     x        k
            write(iounit,'(''ERROR - cluster indices'',2i5)')
     x        iclcnt,clsind(k)
            stop 'error in cluster routine'
          endif
        endif
927   CONTINUE


929   CONTINUE
      IFRST=IFRST+1
      GO TO 921

930   CONTINUE

      ICLUS(1,ICLCNT)=NCLUST !save size of cluster iclcnt
c      write(*,'(''Final size of cluster '',i5,'' is '',i5)')
c     x    iclcnt,nclust
c      kk=0
c      do i=1,nmol
c        kk=kk+1
c        if(clsind(i).eq.iclcnt) then
c          write(*,'(''Member '',i5,
c     x    '' is '',i5)')  kk,i  
c        endif
c      enddo

C     LOOK FOR NEXT UNASSIGNED CENTER, FROM INEXT+1 TO NATOMS
      GO TO 912





940   CONTINUE  !here when done partitioning centers into clusters
    
C     PUT INDICIES SEQUENTIALLY INTO CLSLST ARRAY

      LCLPTR=1
      DO 943 I=1,ICLCNT
      ICLUS(2,I)=LCLPTR
943   LCLPTR=LCLPTR+ICLUS(1,I)

      DO 944 I=1,nmol   
        IF(CLSIND(I).GT.0) THEN
          CLSLST(ICLUS( 2,CLSIND(I)))=I
          ICLUS(2,CLSIND(I)) = ICLUS(2,CLSIND(I)) + 1
        ENDIF
944   CONTINUE

      LCLPTR=1
      DO 945 I=1,ICLCNT
      ICLUS(2,I)=LCLPTR
945   LCLPTR=LCLPTR+ICLUS(1,I)
 

C     DOUBLE CHECK THAT THEY WERE ALL COUNTED
      do i=1,nmol
        IF(CLSIND(I).EQ.-1) WRITE(iounit,'(''WARNING:  UNASSIGNED'',
     x    '' CENTER  I = '',I6)') I
      enddo


c
c     find out how many new bonds are in each cluster
c
      do i=1,iclcnt
        iclus(3,i)=0 !initialize counter for new bonds in cluster i
        iclus(4,i)=0 !initialize counter for bonds of type 1 in cluster i (to lactone)
        iclus(5,i)=0 !initialize counter for bonds of type 2 in cluster i (inter cluster bond)
        iclus(6,i)=0 !initialize counter for bonds of type 3 in cluster i (intra cluster bond)
      enddo

      do i=1,ibseq
c       imol=molnum(ibndseq(i,1),ntype1,nattp1,ntype2,nattp2)
c       jmol=molnum(ibndseq(i,2),ntype1,nattp1,ntype2,nattp2)
        imol=molecule(ibndseq(i,1),1)
        jmol=molecule(ibndseq(i,2),1)
c       get cluster index for molecules imol and jmol (they should be the same)
        imolc=clsind(imol)
        jmolc=clsind(jmol)
        if(imolc.ne.jmolc) then
          write(iounit,'(''Error:  molecules bonded should '',
     x       ''be in same cluster but are not.'')')
          stop 'error: bonded moleclues not in same cluster'
        endif
        iclus(3,imolc)=iclus(3,imolc)+1
c       write(iounit,'('' Bond '',i5,'' Atom site '',i6,'' ('',2i5,
c    x                              '') Atom site '',i6,'' ('',2i5,
c    x   '') '',
c    x   '' running count '',i5)')
c    x   i,ibndseq(i,1),imol,clsind(imol),
c    x     ibndseq(i,2),jmol,clsind(jmol),iclus(3,imolc)
        ibt=ibndseq(i,5)  ! 0 unknown; 1 lactone; 2 difft; 3 same
        if(ibt.eq.1.or.ibt.eq.2.or.ibt.eq.3) then
          iclus(3+ibt,imolc)=iclus(3+ibt,imolc)+1
        else
          write(iounit,'(''Error: unrecognized bond type '',i5)') ibt   
          stop 'unrecognized bond type'
        endif
      enddo


      write(iounit,'(''Total number of bonds in ibndseq: '',i6)') ibseq
      ibtot=0
      iblac=0
      ibtra=0
      ibter=0
      do i=1,iclcnt
        ibtot=ibtot+iclus(3,i)
        iblac=iblac+iclus(4,i)
        ibter=ibter+iclus(5,i)
        ibtra=ibtra+iclus(6,i)
        if(iclus(4,i)+iclus(5,i)+iclus(6,i).ne.iclus(3,i)) then
          write(*,'(''Number of bonds for cluster '',i5,
     x      '' inconsistent: iblac '',i5,'' ibter '',i5,
     x      '' ibtra '',i5,'' ibtot '',i5)')
     x      i,iclus(4,i),iclus(5,i),iclus(6,i),iclus(3,i)
        endif
      enddo
      write(iounit,'(''Total number of bonds in all clusters: '',
     x  i6)') ibseq
      write(iounit,'(''Total bonds formed to solvent lactone: '',
     x  i6)') iblac
      write(iounit,'(''Total cross linking (inter)          : '',
     x  i6)') ibter
      write(iounit,'(''Total cross linking (extra)          : '',
     x  ''not counted here'')') 
   
c
c
c







C     WRITE OUT CLUSTER INFORMATION
      WRITE(iounit,'(/,''Number of distinguishable'',
     X                 '' clusters is '',I10)') iclcnt
      ntot=0
      n1=0   !number of clusters with one member
      n1a=0  !number of clusters with one member of type a
      n1b=0  !number of clusters with one member of type b
      ilargest=0
      do i=1,iclcnt
        if(ilargest.lt.iclus(1,i)) then
          ilargest=iclus(1,i)
          ilargind=i
        endif
        ntot=ntot+iclus(1,i) 
        if(iclus(1,i).eq.1) then
          n1=n1+1
          imol=clslst(iclus(2,i))  !molecule number
          mptr=0
          itp=0
          do k=1,nblocks
            mptr=mptr+molblock(k,2)
            if(itp.eq.0.and.imol.le.mptr) itp=molblock(k,1)
          enddo
          if(itp.eq.1) then
            n1a=n1a+1
          elseif(itp.eq.2) then
            n1b=n1b+1
          endif
        endif
c       if(iclus(1,i).gt.1) then
c        if(iprint.gt.5)  write(iounit,'(''  Cluster number '',i6,
c     x   '' size = '',i6 )') i,iclus(1,i)
          l=iclus(2,i)
          if(iprint.gt.10) write(iounit,'(10i6)')
     x    (clslst(l-1+k),k=1,iclus(1,i))
c         do k=1,iclus(1,i)
c           imol=clslst(l-1+k)
c           if(imol.gt.1000) then
c             compute residue number for molecules
c             imol=1000 + (imol-1000-1)*4 + 1
c             clslst(l-1+k)=imol
c           endif
c         enddo
c         write(*,'(10i6)') (clslst(l-1+k),k=1,iclus(1,i))
c       endif
      enddo

      write(iounit,'(/,''Largest cluster is '',i4,'' of size '',i6)')
     x  ilargind,ilargest
      write(iounit,'(''Number of clusters with one member '',i6)') 
     x  n1
      write(iounit,'(''Number of free molecules (type 1) '',i6)')
     x  n1a
      write(iounit,'(''Number of free molecules (type 2) '',i6)')
     x  n1b
      write(iounit,'(''Total number of molecules in clusters '',
     x  i6,'' (should be '',i6,'')'')') 
     x  ntot,nmol





      if(ifgcproj.eq.1) then
c       in gelcore project mode, determine if new lactones need to be added
c       information about this should be encoded in the *gbin file
c       this would be a target size, a threshhold to trigger the addition,
c       or even a lag time (if the number free has not changed in n steps,
c       add anyway.)
        if(n1a.gt.500) then
c         this is the mode where we attempt to keep about 1000 free lactones
c         number to add is target-n1a where target is 1000
          n1add=1000-n1a
        else
c         this is the mode where we attempt to keep about 100 free lactones
c         number to add is target-n1a where target is 100
          n1add=100-n1a
        endif
c
c       alternative logic to keep target number of free lactones near some muliple of 1000
c       if n1a is an exact multiple of 1000, assume we are at the start and don't add
c       if not, find the next larger even multiple of 1000 and assume this is the target
c       and add if we are 100 or more less than this
c       this logic will not work if more than 1000 lactones are absorbed
c       n1a at this point is the number of free lactone molecules
        if(n1a.le.100) then
          n1target=100
          n1add=n1target-n1a
        elseif(mod(n1a,1000).eq.0) then
          n1target=n1a
          n1add=0
        else
          n1target=1000*(n1a/1000+1)
          n1add=n1target-n1a
        endif 

        write(11,'(''In gelcore project mode, target size assumed '',
     x    ''to be '',i6)') n1target
c
        if(n1add.ge.95) then
c         generate the mxin file; read from template generic.gbin and replace * by number
c         the presence of this file triggers molecule addition and equilibration
c         (note that the fxlmp file can be generic)
          write(11,'(''In gelcore project mode, rules trigger '',
     x      ''addition of '',i5,'' molecules.'')') n1add
          open(22,file='generic.mxin')

          is=index(fngcbond,'.bond')
          write(11,'(''In gelcore project mode, creating file '',
     x      a)') fngcbond(1:is)//'mxin'
          open(23,file=fngcbond(1:is)//'mxin')
427       continue
          read(22,'(a120)',end=429) line
          iast=index(line,'*')
          is=lstchr(line)
          if(iast.ne.0) then 
            write(23,'(a,i6,a)') line(1:iast-1),
     x      n1add,line(iast+1:is)
          else
            write(23,'(a)') line(1:is)
          endif
          go to 427
429       continue

          close(23)

          close(22)

        endif

      endif



c
c     checking
c
      do i=1,nmol
        icheck(i)=0
      enddo

      do ic=1,iclcnt
        l=iclus(2,ic)
c        write(*,'(''Cluster '',i6,'' Size '',i3,  
c     x     '' Pointer: '',2i6)') ic,iclus(1,ic),iclus(2,ic),
c     x     iclus(2,ic)+iclus(1,ic)-1
        do j=1,iclus(1,ic)
          imember=clslst(l+j-1)
          if(imember.le.0.or.imember.gt.nmol) then
            write(iounit,'(''Error:  imember out of range '',
     x        i6)') imember
          endif
          if(icheck(imember).eq.0) then
            icheck(imember)=ic
          else
            write(iounit,'(''Error: molecule '',i6,'' in more than '',
     x      ''one cluster.'')')  imember
            write(iounit,'(''Error: molecule '',i6,'' in '',
     x      ''clusters '')')  icheck(imember),ic
          endif
        enddo
      enddo
      do i=1,imol
        if(icheck(i).eq.0) then
          write(iounit,'(''Error:  unassigned molecule '',
     x      i6)') i
        endif
      enddo

      


c      code for sorting clusters from largest to smallest
   
      do i=1,iclcnt
        isort(i,1)=i
        isort(i,2)=iclus(1,i) !size
      enddo   
      do i=1,iclcnt-1
        do j=i+1,iclcnt
          if (isort(i,2).lt.isort(j,2)) then
            idum=isort(i,1)
            isort(i,1)=isort(j,1)
            isort(j,1)=idum
            idum=isort(i,2)
            isort(i,2)=isort(j,2)
            isort(j,2)=idum
          endif 
        enddo
      enddo 



c  code to populate histograms with cluster sizes

      do i=1,nhist
      hist(1,i)=0
      hist(2,i)=0
      hist(3,i)=0
      enddo

      do i=1,iclcnt
        ibin=min(nhist,max(1,iclus(1,i)))
        hist(1,ibin)=hist(1,ibin)+1
        ibin = min(nhist,(1+(max(1,iclus(1,i))-1)/10))
        hist(2,ibin)=hist(2,ibin)+1
        ibin = min(nhist,(1+(max(1,iclus(1,i))-1)/100))
        hist(3,ibin)=hist(3,ibin)+1
      enddo




      write(iounit,'(//,'' Histogram '')')
      n1=0
      n2=0
      n3=0
      do i=1,nhist
        write(iounit,'( i3, '' count = '', i6, '' range '',
     x  i5,''-'',i5 ,'' count = '',
     x  i6, '' range '', i5,''-'',i5 ,'' count = '', i6)')
     x  i,int(hist(1,i)),1+10*(i-1),10*i,int(hist(2,i)),1+100*(i-1),
     x  100*i,int(hist(3,i))    
        n1=n1+hist(1,i)
        n2=n2+hist(2,i)
        n3=n3+hist(3,i)
      enddo
       
c      write(iounit,'(''total number of clusters = '', 3i6)') n1, n2, n3  


cccccccccccccccccccc



      do i=1,mcompmx
        do j=1,mcompmx
          mcomp(i,j)=0
        enddo
      enddo

c
c     histogram for each i-mer what its composition is
c

      do i=1,iclcnt
        n1a=0
        n1b=0
        isz=iclus(1,i) !cluster size
        do j=1,isz
          imol=clslst(iclus(2,i)-1+j)  !molecule number
          itp=0
          mptr=0
          do k=1,nblocks
            mptr=mptr+molblock(k,2)
            if(itp.eq.0.and.imol.le.mptr) itp=molblock(k,1)
          enddo
          if(itp.eq.1) then
            n1a=n1a+1   !lactone counter
          elseif(itp.eq.2) then
            n1b=n1b+1   !arm counter
          else 
            write(iounit,'(''Error:  There is not supposed to '',
     x        ''be more than 2 types of molecules.'')')
            stop 'there is not supposed to be more than 2 types'
          endif
        enddo
        if(isz.le.mcompmx) then
c         bump counter in mcomp; cluster size gives column; number of arms+1 gives row
          if(n1b+1.le.mcompmx) mcomp(n1b+1,isz)=mcomp(n1b+1,isz)+1
        endif
      enddo


      write(iounit,'(//,''Cluster composition '',/)')
      do i=1,mcompmx
        n1=0
        do j=1,i+1  !i-mers can have from 0 to i arms
          n1=n1+mcomp(j,i)
        enddo
        if(n1.gt.0) then
          write(iounit,'(''Composition of the '',i3,''-mers:'')') i
          do j=1,i+1
            if(mcomp(j,i).gt.0) write(iounit,
     x        '(5x,i5,'' Arms; '',i5,'' Lactones: '',i6)')
     x        j-1,i-(j-1),mcomp(j,i)
          enddo
          write(iounit,'(''Total              '',15x,i5)')
     x      n1
        endif
      enddo

ccccccccccccccc







      if (ifgbcls.eq.1) then
        is=index(argument,'.gbin')-1
c        write(*,'(''argument is *'',a,''*'')') argument(1:is)
        open(13,file=argument(1:is)//'.gbcls') 
        write(13,*) 2
c       write(13,*) ntype1, nattp1, nrestp1, masstp1
c       write(13,*) ntype2, nattp2, nrestp2, masstp2
        write(13,*) nattp1, nrestp1, masstp1
        write(13,*) nattp2, nrestp2, masstp2

        write(13,'(a,5x,i10)') fnbond(1:lstchr(fnbond)),itimelst
c       note that this characterizes the clusters with the most recent
c       *.bonds ; blocking structure depends on which *.bonds
        write(13,*) nblocks,(molblock(i,1),molblock(i,2),i=1,nblocks)
      endif

      do i=1,ntype1+ntype2
        mstate(i,5)=0
      enddo



      nlactot=0
      nnewtot=0
      nblctot=0  !count total bonds to solvent lactones
      nbertot=0  !count total bonds to inter cluster
      nbratot=0  !count total bonds to intra cluster
      nextra=0

      do i=1,iclcnt
        if (isort(i,2).gt.1) then    !this limits attention to clusters > 1
          m=isort(i,1)
          n=iclus(2,m)
          newbond=iclus(3,m)
          ic1=0   !lactone counter (type 1)
          ic2=0   !arm counter (type 2)
          ilad=0  !ladder counter
          do k=1,iclus(1,m)
            imol=clslst(n-1+k)
            itp=0
            mptr=0
            do m1=1,nblocks
              mptr=mptr+molblock(m1,2)
              if(itp.eq.0.and.imol.le.mptr) itp=molblock(m1,1)
            enddo
c           if(imol.le.ntype1) then 
            if(itp.eq.1) then 
             ic1=ic1+1  !lactone counter
            else
             ic2=ic2+1  !arm counter
            endif
            do l=1,nlad
              if(clslst(n-1+k).eq.ladder(l,1).or.
     x         clslst(n-1+k).eq.ladder(l,2)) then
               ilad=ilad+1
              endif  
            enddo
          enddo 
          nnewtot=nnewtot+newbond  !running total number of new bonds
          nlactot=nlactot+ic1      !running total number of lactones in clusters
          nblctot=nblctot+iclus(4,m)
          nbertot=nbertot+iclus(5,m)
          nbratot=nbratot+iclus(6,m)
c         Note that extra crosslinking bonds can occur that might not be
c         counted; these show up when the number of crosslinks computed
c         as total bonds in the cluster minus the number of lactones
c         is greater than the sum of crosslink bonds nber+nbra
          iextra=(newbond-ic1) - (iclus(5,m)+iclus(6,m))
          nextra=nextra + iextra                                        
c
c          write(iounit,'(''  Cluster number '',i6,'' ('',i6,'') '',
c     x      ''  size = '',i6 ,'' nlac = '', i6 ,'' narm ='', i6,  
c     x      '' mass = '', i6, '' ladders = '', i6,
c     x      '' new bonds = '',i6)') 
c     x      i,m,iclus(1,m),
c     x      ic1,ic2,ic1*masstp1+ic2*masstp2,ilad/2,newbond
c            write(iounit,'(10i6)') (clslst(n-1+k),k=1,iclus(1,m))
          do k=1,iclus(1,m)
            mstate(clslst(n-1+k),5)=i 
          enddo

          
          if (ifgbcls.eq.1) then
            write(13    ,'(''  Cluster number '',i6,'' ('',i6,'') '',
     x        ''  size = '',i6 ,'' nlac = '', i6 ,'' narm ='', i6,  
     x        '' mass = '', i6, '' ladders = '', i6,
     x        '' new bonds = '',i6,'' lac bonds = '',i6,
     x        '' ter bonds = '',i6,'' tra bonds = '',
     x        i6,'' extra bonds = '',i6)') 
     x        i,m,iclus(1,m),
     x        ic1,ic2,ic1*masstp1+ic2*masstp2,ilad/2,newbond,
     x        iclus(4,m),iclus(5,m),iclus(6,m),iextra
            write(13,'(10i6)') (clslst(n-1+k),k=1,iclus(1,m))
          endif

        endif 
      enddo 
        
      write(iounit,'('' Time '',i10,'' New bonds '',i10,
     x  '' Lactones in all clusters '',i10,
     x  '' Bonds to solvent lactones '',i10,
     x  '' Cross linking inter '',i10,'' Cross linking intra '',i10,
     x  '' Cross linking extra '',i10)')
     x  itimelst,nnewtot,nlactot,nblctot,nbertot,nbratot,nextra

c     do i=1,ntype1
c       if(mstate(i,2).eq.1) write(iounit,'(3i10,'' activated '')') 
c    x    mstate(i,1),mstate(i,5),itimelst
c       if(mstate(i,4).eq.1) write(iounit,'(3i10,'' activated '' )') 
c    x    mstate(i,3),mstate(i,5),itimelst
c     enddo

      do i=1,ntype1+ntype2
        mptr=0
        itp=0
        do k=1,nblocks
          mptr=mptr+molblock(k,2)
          if(itp.eq.0.and.i.le.mptr) itp=molblock(k,1)
        enddo
        if(itp.eq.1) then
          if(mstate(i,2).eq.1) write(iounit,'(3i10,'' activated '')') 
     x      mstate(i,1),mstate(i,5),itimelst
          if(mstate(i,4).eq.1) write(iounit,'(3i10,'' activated '' )') 
     x      mstate(i,3),mstate(i,5),itimelst
        endif
      enddo





c
c     write out the new bonds
c
      if (ifgbcls.eq.1) then
        write(13,'(/,'' New bonds '',i10)') ibseq
        do i=1,ibseq
          write(13,'(4i10)') (ibndseq(i,k),k=1,4)
        enddo

c       nactive=0
c       do i=1,ntype1
c         if(mstate(i,2).eq.1) nactive=nactive+1
c         if(mstate(i,4).eq.1) nactive=nactive+1
c       enddo

c       nbarm=0
c       do i=ntype1+1,ntype1+ntype2
c         if(mstate(i,2).eq.1) nbarm=nbarm+1
c       enddo

        nactive=0
        nbarm=0
        do i=1,ntype1+ntype2
          mptr=0
          itp=0
          do k=1,nblocks
            mptr=mptr+molblock(k,2)
            if(itp.eq.0.and.i.le.mptr) itp=molblock(k,1)
          enddo
          if(itp.eq.1) then  !assumes type 1 is for the lactones
            if(mstate(i,2).eq.1) nactive=nactive+1
            if(mstate(i,4).eq.1) nactive=nactive+1
          elseif(itp.eq.2) then  !assumes type 2 is for the arms
            if(mstate(i,2).eq.1) nbarm=nbarm+1
          endif
        enddo





        write(13,'(/,'' Activated sites '',i10)') nactive 
        write(iounit,'(/,'' Activated sites '',i10)') nactive 
        write(iounit,'(/,'' Narm bonds      '',i10)') nbarm   
        write(iounit,'(/,'' Total arms      '',i10)') ntype2  


c       do i=1,ntype1
c         if(mstate(i,2).eq.1) write(13,'(3i10,'' activated '' )') 
c    x     mstate(i,1),mstate(i,5),itimelst
c         if(mstate(i,4).eq.1) write(13,'(3i10,'' activated '' )') 
c    x     mstate(i,3),mstate(i,5),itimelst
c       enddo

        do i=1,ntype1+ntype2
          mptr=0
          itp=0
          do k=1,nblocks
            mptr=mptr+molblock(k,2)
            if(itp.eq.0.and.i.le.mptr) itp=molblock(k,1)
          enddo
          if(itp.eq.1) then
            if(mstate(i,2).eq.1) write(13,'(3i10,'' activated '' )') 
     x       mstate(i,1),mstate(i,5),itimelst
            if(mstate(i,4).eq.1) write(13,'(3i10,'' activated '' )') 
     x       mstate(i,3),mstate(i,5),itimelst
          endif
        enddo


        close(13)
      endif




c
c     write out information about the larger clusters (>nclussz)
c
      do i=1,iclcnt
        if (isort(i,2).gt.nclssz) then
          if(ifclsopn.eq.0) then
            open(22,file=fnclus)
            ifclsopn=1
            do j=1,nmol
              mhistory(j)=0   !clear the flag for already seen molecules
            enddo
          endif
          m=isort(i,1)
          n=iclus(2,m)   !pointer into clslst of first molecule in cluster m

c         check to see if any of the moleucles in this cluster have already been seen before in lg clusters
c         meanwhile, count the arms, lactones, bonds, ladders
c         write out the time it was seen, the number of molecules, nlac, narm, nbonds
c         nlac:narm ratio num crosslinking bonds fraction crosslinking bonds

          ic1=0
          ic2=0
          ilad=0
          iffound=0
          do k=1,iclus(1,m)
            if(mhistory(clslst(n-1+k)).ne.0) iffound=1
            if(clslst(n-1+k).le.ntype1) then 
             ic1=ic1+1  !lactone counter
            else
             ic2=ic2+1  !arm counter
            endif
            do l=1,nlad
              if(clslst(n-1+k).eq.ladder(l,1).or.
     x           clslst(n-1+k).eq.ladder(l,2)) then
               ilad=ilad+1
              endif  
            enddo
          enddo 
          do k=1,iclus(1,m)
            mhistory(clslst(n-1+k))=1   !save that this molecule has been seen already     
          enddo

          if(iffound.ne.1) then
c           write out information about this cluster
            newbond=iclus(3,m)
            iextra=(iclus(3,m)-ic1)-(iclus(5,m)+iclus(6,m))
            write(22,'(''Time observed '',i10,
     x        ''  cluster index '',i6,'' ('',i6,'') '',
     x        '' size = '',i6 ,'' nlac = '', i6 ,'' narm ='', i6,  
     x        '' mass = '',i6, '' ladders = '',i6,
     x        '' new bonds = '',i6,'' lac bonds = '',i6,
     x        '' ter bonds = '',i6,'' tra bonds = '',i6,
     x        '' ext bonds = '',i6)') 
     x        itimelst,
     x        i,m,iclus(1,m),
     x        ic1,ic2,ic1*masstp1+ic2*masstp2,ilad/2,newbond,
     x        iclus(4,m),iclus(5,m),iclus(6,m),iextra
            write(22,'(10i6)') (clslst(n-1+k),k=1,iclus(1,m))
          endif
        endif  !end of ifblock to see if cluster is big enough to write into file fnclus
      enddo  !end of loop over clusters



c
c     save for each molecule the cluster index it is in; save 0 if this is a monomer
c
      do i=1,nmol
        mcluster(i)=0  !initialize to monomers
      enddo

      do i=1,iclcnt
        if (isort(i,2).gt.1) then
          m=isort(i,1)
          n=iclus(2,m)   !pointer into clslst of first molecule in cluster m
          do k=1,iclus(1,m)
            mcluster(clslst(n-1+k))=i   !save (i or m) cluster number for this molecule             
          enddo
        endif
      enddo  !end of loop over clusters





      return
      end




c
c     integer function to return the location of the last nonblank character from a string
c
      function lstchr(string)
      character*(*) string
      iloc=0
      do i=len(string),1,-1
        if(iloc.eq.0.and.string(i:i).ne.' ') iloc=i
      enddo
      lstchr=iloc
      end


c
c     subroutine to left justify a string
c
      subroutine leftjust(string)
      character*(*) string
      l=len(string)
c     find first non blank
      ifrst=0
      do i=1,l
        if(ifrst.eq.0.and.string(i:i).ne.' ') then
          ifrst=i
        endif
      enddo
c     if ifrst=0 the entire string is blank
      if(ifrst.eq.0) return
      do i=ifrst,l
        string(i-ifrst+1:i-ifrst+1)=string(i:i)
      enddo
      string(l-ifrst+2:l)=' '
      return
      end
