c
c     program to read an lmp file (describing molecular topology)
c                 and an xyz file (giving coordinates)
c                 and a list of internal coordinates (torsion types) to monitor
c
c     each snapshot of the xyz file is analyzed and for each molecule in the
c     file with torsions of the target type the angle is measured
c     this produces an output file with time stamp, molecule id, torsion angle value list
c     
c 
      implicit real*8 (a-h,o-z)
      character*120 mname,xyzname,tlistname,argstring,line,string
      logical ifexist

      dimension coord(3,100000),charge(100000),itype(100000)
      dimension imol(100000),molptr(5,10000)   !dimension to total num of molecule in system
      character*20 atname(100000)
      dimension ijbond(3,90000),bndpar(2,5000)
      dimension ijkangle(4,175000),angpar(2,5000)
      dimension ijkldih(5,300000),dihpar(4,5000)
      dimension vdw(4,100),fmass(100)
      character*100 nametypes(100)
    
      dimension box(3,2)
      dimension lseq(10000) !dimension must be for the total number of molecules in the system
      dimension amass(120)
      dimension buffer(1000)

      dimension irange(100,8),itlist(1000)  !hold information from the torsion list input file
      dimension idlist(2,1000) !holds information about the intermolecular distances to compute
      dimension itmonitor(10000)  !unrolled list of torsions to measure ordered by molecule
      dimension coordmi(3,4)



      natmx=100000 !size of site-based arrays
      nattpmx=100  !size of atom type arrays
      nbnmx=90000  !size of bond list arrays
      nbtmx=5000   !size of bond type arrays
      nanmx=175000  !size of angle list arrays
      nantmx=5000  !size of angle type arrays
      ndimx=300000   !size of dihedral list array
      nditmx=5000  !size of the dihedral type array
      itmonmx=10000 !size of itmonitor
      nmolmx=10000  !size of molptr and lseq (max number of molecules)

      iprint=10 !detailed print
      iprint=1 !minimal print

      call getarg(1,argstring)
      call leftjust(argstring)
      is=index(argstring,' ')-1
c     look for input file
      inquire(file=argstring(1:is)//'.gin',exist=ifexist)
      if(.not.ifexist) then
        write(*,'('' Missing input file.  Looking for '',a)')
     x    argstring(1:is)//'.gin'
        stop 'missing input file'
      endif
      open(15,file=argstring(1:is)//'.gin')
      open(6,file=argstring(1:is)//'.gout')
      write(6,'('' Program to compute internal coordinates from '',
     x   ''LAMMPS *.xyz file.'')')
      write(6,'('' Input file:  '',a)') argstring(1:is)//'.gin'
      write(6,'('' Output file: '',a)') argstring(1:is)//'.gout'
      write(6,'('' Data file:   '',a)') argstring(1:is)//'.gdat'
      write(6,'('' Coord file:  '',a)') argstring(1:is)//'.xyz'
      read(15,'(a120)') line
      mname=line
      call ucase(line)
      is1=index(line,'LMP FILE:')+9
      call leftjust(mname(is1:))
      is2=index(mname(is1:),' ')+is1-1
      inquire(file=mname(is1:is2),exist=ifexist)
      if(.not.ifexist) then
        write(*,'('' Missing LMP file.  Looking for '',a)')
     x    mname(is1:is2)
        stop 'missing LMP file'
      endif
      line=mname
      mname=line(is1:is2)
      write(6,'('' LMP file:    '',a)') mname(1:is2-is1+1)

      inquire(file=argstring(1:is)//'.xyz',exist=ifexist)
      if(.not.ifexist) then
        write(*,'('' Missing coordinate file.  Looking for '',a)')
     x    argstring(1:is)//'.xyz'
        stop 'missing xyz file'
      endif
      xyzname=argstring(1:is)//'.xyz'



    
c     lmp file (eventually read this in, but for now just set)
c     mname='plal8_512.lmp'
c     xyz file (eventually read this in, but for now just set)
c     xyzname='pla8B_512_startup6.xyz'
c     file with torsions to monitor                        
c     tlistname='plal8_512.gtor'
c     needs a range of molecule ids (e.g., 2-2000) and a set of torsion types for each
c     Molecules 2 2000 Torsiontypes 528 892 1274

c     write(*,'(//,'' Analysis of xyz file:   '',a)') xyzname
c     write(*,'(   '' Topological info from:  '',a)') mname
c     write(*,'(   '' Torsion to monitor from:'',a)') tlistname



c     assume structure of input file is a set of molecule index ranges
c     and for each range there can be, optionally, a list of torsion,
c     a list of interatomic distances, a keyword "rgyr" or a keyword
c     "end2end"

c irange(beg,end,number of internals,first internal)
      nrange=0
      ntlist=0  !number of torsions
      ndlist=0  !number of distances
c     open(15,file=tlistname)
10    continue
      read(15,'(a120)',end=50) line
      call ucase(line)
      is=index(line,'MOLECULE RANGE:')
      if(is.eq.0) then
        write(*,'('' Error:  expecting string MOLECULE RANGE:'')')
        write(*,'('' Instead found: '',a)') line(1:30)
        stop 'input error reading range of molecules'
      endif
      nrange=nrange+1
      is=is+15
      call leftjust(line(is:))
      is1=index(line(is:),' ')+is-2
      read(line(is:is1),*) irange(nrange,1)
      call leftjust(line(is1+2:))
      it=index(line(is1+2:),' ')+is1
      read(line(is1+2:it),*) irange(nrange,2)
      if(irange(nrange,1).le.0.or.irange(nrange,2).le.0
     x  .or.irange(nrange,2).lt.irange(nrange,1)) then
        write(*,'('' Error: molecule indices must be positive'',
     x    '' and second index .ge. first index '')')
        stop 'molecule ranges invalid'
      endif
      irange(nrange,3)=ntlist+1
      irange(nrange,4)=ntlist
      irange(nrange,5)=ndlist+1
      irange(nrange,6)=ndlist
      irange(nrange,7)=0  !set rgyr flag to off
      irange(nrange,8)=0  !set endtoend flag to off

15    continue
c     for this range read in lines with keywords:  torsions distance rgyr endtoend
c     this set terminates when end of file or new molecule range identifier
      read(15,'(a120)',end=50) line
      call ucase(line)
      is=index(line,'MOLECULE RANGE:')
      if(is.ne.0) then
        backspace 15
        go to 10
      endif
      itoken=0
      if(index(line,'TORSION').ne.0) then
        is=index(line,'TORSION')
        it=index(line(is:),' ')+is-1
16      call leftjust(line(it+1:))
        if(line(it+1:it+1).eq.' ') go to 17
        itoken=itoken+1
        read(line(it+1:),*) ival
        itlist(ntlist+itoken)=ival 
        it=index(line(it+1:),' ')+it
        go to 16
17      continue
        irange(nrange,4)=ntlist+itoken
        ntlist=ntlist+itoken
      elseif(index(line,'DISTANCE').ne.0) then
        is=index(line,'DISTANCE')
        it=index(line(is:),' ')+is-1
26      call leftjust(line(it+1:))
        if(line(it+1:it+1).eq.' ') go to 27
        itoken=itoken+1
        read(line(it+1:),*) ival
        idlist(1,ndlist+itoken)=ival 
        it=index(line(it+1:),' ')+it
        call leftjust(line(it+1:))
        read(line(it+1:),*) ival
        idlist(2,ndlist+itoken)=ival 
        it=index(line(it+1:),' ')+it
        go to 26
27      continue
        irange(nrange,6)=ndlist+itoken
        ndlist=ndlist+itoken
      elseif(index(line,'RGYR').ne.0) then
        irange(nrange,7)=1   !flag to compute rgyr for this molecule
      elseif(index(line,'ENDTOEND').ne.0) then
        irange(nrange,8)=1   !flag to find end to end distance for this molecule
      else
        write(*,'('' Error:  unidentified keyword in string '',a)')
     x    line(1:30)
      endif

      go to 15
    


50    continue
      close(15)


      write(*,'(/,'' Number of ranges read in is '',i3)') nrange
      do i=1,nrange
        write(*,'(/,'' Range '',i3,'' Molecules '',i6,'' to '',i6,
     x    '' (torsions: '',2i6,
     x    '' ) (distances '',2i6,'')'')') 
     x    i,irange(i,1),irange(i,2),irange(i,3),irange(i,4),
     x      irange(i,5),irange(i,6)
        is=irange(i,3)
        it=irange(i,4)
        if(it-is+1.gt.0) then
        write(*,'('' Torsion index  '',i3,'' torsion type '',i6)')
     x    (j-is+1,itlist(j),j=is,it)
        endif
        is=irange(i,5)
        it=irange(i,6)
        if(it-is+1.gt.0) then
        write(*,'('' Distance index '',i3,'' site pairs   '',2i6)')
     x    (j-is+1,idlist(1,j),idlist(2,j),j=is,it)
        endif
        if(irange(i,7).eq.1)
     x     write(*,'('' Rgyr flag set for range '')')
        if(irange(i,8).eq.1)
     x     write(*,'('' End-to-end distance flag set for range '')')
      enddo



      call readlmp(mname,
     x       nsites,itype,imol,charge,coord,
     x       atname,natmx,
     x       nbonds,nbtp,bndpar,ijbond,
     x       nbnmx,nbtmx,
     x       nangle,natp,angpar,ijkangle,
     x       nanmx,nantmx,
     x       ndih,ndtp,dihpar,ijkldih,
     x       ndimx,nditmx,
     x       natomtp,fmass,vdw,nametypes,
     x       nattpmx,
     x       box)




      write(*,*)
      write(*,'('' Type information '',i2)') 
      write(*,'(''   Number of atom types   '',i8)')
     x  natomtp
      write(*,'(''   Number of bond types   '',i8)')
     x  nbtp
      write(*,'(''   Number of angle types  '',i8)')
     x  natp
      write(*,'(''   Number of dihedral types'',i7)')
     x  ndtp

      write(*,*)
      write(*,'(''   Number of atoms        '',i8)')
     x  nsites
      write(*,'(''   Number of bonds        '',i8)')
     x  nbonds
      write(*,'(''   Number of angles       '',i8)')
     x  nangle
      write(*,'(''   Number of dihedrals    '',i8)')
     x  ndih



c
c     look for the first and last atom index for each molecule
c     assume sites are contiguous for each molecule
c
      molcnt=0

      do i=1,nsites
        if(imol(i).ne.molcnt) then
          if(molcnt.ne.0) molptr(2,molcnt)=i-1
c         just found the start of the next molecule
          if(molcnt+1.gt.nmolmx) then
            write(*,'('' Error:  need to increase size of molptr'')')
            stop 'not enough space in molptr'
          endif
          molcnt=molcnt+1
          if(molcnt.ne.imol(i)) then
            write(*,'('' Possible problem from missing molecule'')')
            stop 'missing molecules'
          endif
          molptr(1,molcnt)=i !record index of first atom of this molecule
        endif
      enddo
      molptr(2,molcnt)=nsites  !record index of last atom for last molecle

      write(*,'(/,'' Number of molecules found is '',i6)') molcnt
c    
c     find out what range each molecule is in
c     flag as an error if a molecule is in more than one range
c
      do i=1,molcnt
        ifound=0
        do j=1,nrange
          if(irange(j,1).le.i.and.irange(j,2).ge.i) then
            if(ifound.ne.0) then
              write(*,'('' Error: current code does not handle '',
     x          ''overlapping ranges '')')
              write(*,'('' Molecule '',i6,'' is in ranges '',i3,
     x          '' and '',i3)') i,ifound,j
              stop 'cannot have overlapping ranges' 
            endif
            ifound=j
          endif
        enddo
        molptr(3,i)=ifound  !record range index for this molecle
        molptr(4,i)=0       !initialize pointers into itmonitor
        molptr(5,i)=0

      enddo


      do i=1,molcnt
        write(*,'('' Molecule '',i6,'' (Sites '',i10,'' to '',i10,
     x    '' Num '',i4,'') Range '',i3)')
     x  i,molptr(1,i),molptr(2,i),molptr(2,i)-molptr(1,i)+1,molptr(3,i)
      enddo

c
c



c
c     produce itmonitor, the unrolled list of torsions to monitor ordered by molecule
c     molptr(4,im) and molptr(5,im) point into this list for each molecule im
c     note that if molptr(5,im) .lt. molptr(4,im) there are no torsions to monitor
c     for this molecule (I am hoping when this happens molptr(5,im)=molptr(4,im)-1
c     so that the number of torsions will be molptr(5,im)-molptr(4,im)+1 and this will be 0
c     
c
      idptr=0
      do ir=1,nrange
        ims=irange(ir,1)
        imt=irange(ir,2)
        is= irange(ir,3)
        it= irange(ir,4)
        write(*,'('' Processing range '',i3,'': '',i4,
     x    '' torsion types for '',i6,
     x    '' molecules '')') ir,it-is+1,imt-ims+1
        do im=ims,imt
          molptr(4,im)=idptr+1 !points to next place in the itmonitor array for this molecule
c         find all torsions for this molecule (im) of each type 
c         go through the torsion list, looking for instances of this torsion type for this molecule
          ic=0
          do i=is,it
c           write(*,'('' Looking for torsion types '',i5,
c    x        '' for molecule '',i6)') itlist(i),im
            do id=1,ndih
              if(ijkldih(5,id).eq.itlist(i)) then
                if(imol(ijkldih(1,id)).eq.im) then
c                 write(*,'('' Found torsion of type '',i3,
c    x              '' for molecule '',i5,'' atoms '',4i5)')
c    x              itlist(i),im,(ijkldih(k,id),k=1,4)
                  ic=ic+1
                  idptr=idptr+1
                  if(idptr.gt.itmonmx) then
                    write(*,'('' Increase size if itmonitor itmonmx= '',
     x              i8)') itmonmx
                    stop 'itmonitor too small'
                  endif
                  itmonitor(idptr)=id
                endif
              endif
            enddo
          enddo
          molptr(5,im)=idptr
          if(im.eq.ims) then
            write(*,'('' Will compute '',i3,
     x        '' torsions for each molecule of this type '')') 
     x        ic
          endif
          if(ic.ne.(molptr(5,im)-molptr(4,im)+1)) then 
            stop 'error getting molptr45'
          endif
        enddo !end of loop over molecules in this range
      enddo  !end of loop over ranges













c
c     following code to replace the coordinates from the lmp files with 
c     those from an xyz file.  (note that lmp files often contain 
c     coordinates "as built" whereas the xyz files can represent some
c     degree of equilibration, optimization or thermalization
c
      icntrl=0
      call getcoord(xyzname,nsites,itype,coord,icntrl)
      nframe=icntrl

c     nframe=10



c     left justify the site names
      do j=1,nsites
        call leftjust(atname(j))
      enddo


c
c     buffer the results for each molecule
      
   
      is=index(argstring,' ')-1
      open(11,file=argstring(1:is)//'.gdat')

      do iframe=1,nframe

        icntrl=iframe
        write(*,'('' Retrieving frame '',i3)') iframe
        call getcoord(xyzname,nsites,itype,coord,icntrl)
c       the box dimensions change for each frame (if npt),
c       but for now just take what was in the original lmp file
c       alternative is to look at coordinates to see what 
c       are the box boundaries
c       edgex=dabs(box(1,2)-box(1,1))
c       edgey=dabs(box(2,2)-box(2,1))
c       edgez=dabs(box(3,2)-box(3,1))
c       determine box dimensions from the coordinate data
        xmx=coord(1,1)
        xmn=coord(1,1)
        ymx=coord(2,1)
        ymn=coord(2,1)
        zmx=coord(3,1)
        zmn=coord(3,1)
        do i=1,nsites
          xmx=max(xmx,coord(1,i))
          xmn=min(xmn,coord(1,i))
          ymx=max(ymx,coord(2,i))
          ymn=min(ymn,coord(2,i))
          zmx=max(zmx,coord(3,i))
          zmn=min(zmn,coord(3,i))
        enddo
c       assume cubic
        write(*,'(''Box x-dimension (xyz file):'',3f15.4)')
     x    xmn,xmx,xmx-xmn
        write(*,'(''Box y-dimension (xyz file):'',3f15.4)')
     x    ymn,ymx,ymx-ymn
        write(*,'(''Box z-dimension (xyz file):'',3f15.4)')
     x    zmn,zmx,zmx-zmn
        edgemx=max(xmx-xmn,ymx-ymn,zmx-zmn)
c       shift box to invoke convention that coordinates go from 0 to edgemx
        do i=1,nsites
          coord(1,i)=coord(1,i)-xmn
          coord(2,i)=coord(2,i)-ymn
          coord(3,i)=coord(3,i)-zmn
        enddo

        edgex=edgemx 
        edgey=edgemx 
        edgez=edgemx 

        write(*,'('' Box dimensions, volume used for this frame: '',
     x    3f10.3,5x,f15.3)') 
     x    edgex,edgey,edgez,edgex*edgey*edgez


cwcs    imolecule=0  !initial molecule index
cwcs    do i=1,idptr

cwcs    change looping from being over unrolled torsion list to
cwcs    being over molecules

        do im=1,molcnt
          nbuf=0
          ir=molptr(3,im)  !range index for this molecule (0 if not in one)
          if(ir.gt.0) then
            ifrst=molptr(1,im)
            ilast=molptr(2,im)
c           first, get consistent image for each site in the molecule
            do j=ifrst+1,ilast
              dx=coord(1,j)-coord(1,ifrst)
              dy=coord(2,j)-coord(2,ifrst)
              dz=coord(3,j)-coord(3,ifrst)
              if(dx.gt.+0.5d0*edgex) dx=dx-edgex
              if(dx.lt.-0.5d0*edgex) dx=dx+edgex
              if(dy.gt.+0.5d0*edgey) dy=dy-edgey
              if(dy.lt.-0.5d0*edgey) dy=dy+edgey
              if(dz.gt.+0.5d0*edgez) dz=dz-edgez
              if(dz.lt.-0.5d0*edgez) dz=dz+edgez
              coord(1,j)=dx+coord(1,ifrst)
              coord(2,j)=dy+coord(2,ifrst)
              coord(3,j)=dz+coord(3,ifrst)
            enddo

c
c           process torsions for this molecule
c
            it1=molptr(4,im)  !first torsion in itmonitor
            it2=molptr(5,im)  !last torsion in itmonitor
            do i=it1,it2
              it=itmonitor(i)
              iat=ijkldih(1,it)
              jat=ijkldih(2,it)
              kat=ijkldih(3,it)
              lat=ijkldih(4,it)
c
c         image according the first site
c
c             do k=1,3
c               coordmi(k,1)=0.d0
c             enddo

c             coordmi(1,2)=coord(1,jat)-coord(1,iat)
c             if(coordmi(1,2).gt.0.5d0*edgex) 
c    x           coordmi(1,2)=coordmi(1,2)-edgex
c             if(coordmi(1,2).le.-0.5d0*edgex) 
c    x           coordmi(1,2)=coordmi(1,2)+edgex
c             coordmi(2,2)=coord(2,jat)-coord(2,iat)
c             if(coordmi(2,2).gt.0.5d0*edgey) 
c    x           coordmi(2,2)=coordmi(2,2)-edgey
c             if(coordmi(2,2).le.-0.5d0*edgey) 
c    x           coordmi(2,2)=coordmi(2,2)+edgey
c             coordmi(3,2)=coord(3,jat)-coord(3,iat)
c             if(coordmi(3,2).gt.0.5d0*edgez) 
c    x           coordmi(3,2)=coordmi(3,2)-edgez
c             if(coordmi(3,2).le.-0.5d0*edgez) 
c    x           coordmi(3,2)=coordmi(3,2)+edgez
c         
c             coordmi(1,3)=coord(1,kat)-coord(1,iat)
c             if(coordmi(1,3).gt.0.5d0*edgex) 
c    x           coordmi(1,3)=coordmi(1,3)-edgex
c             if(coordmi(1,3).le.-0.5d0*edgex) 
c    x           coordmi(1,3)=coordmi(1,3)+edgex
c             coordmi(2,3)=coord(2,kat)-coord(2,iat)
c             if(coordmi(2,3).gt.0.5d0*edgey) 
c    x           coordmi(2,3)=coordmi(2,3)-edgey
c             if(coordmi(2,3).le.-0.5d0*edgey) 
c    x           coordmi(2,3)=coordmi(2,3)+edgey
c             coordmi(3,3)=coord(3,kat)-coord(3,iat)
c             if(coordmi(3,3).gt.0.5d0*edgez) 
c    x           coordmi(3,3)=coordmi(3,3)-edgez
c             if(coordmi(3,3).le.-0.5d0*edgez) 
c    x           coordmi(3,3)=coordmi(3,3)+edgez

c             coordmi(1,4)=coord(1,lat)-coord(1,iat)
c             if(coordmi(1,4).gt.0.5d0*edgex) 
c    x           coordmi(1,4)=coordmi(1,4)-edgex
c             if(coordmi(1,4).le.-0.5d0*edgex) 
c    x           coordmi(1,4)=coordmi(1,4)+edgex
c             coordmi(2,4)=coord(2,lat)-coord(2,iat)
c             if(coordmi(2,4).gt.0.5d0*edgey) 
c    x           coordmi(2,4)=coordmi(2,4)-edgey
c             if(coordmi(2,4).le.-0.5d0*edgey) 
c    x           coordmi(2,4)=coordmi(2,4)+edgey
c             coordmi(3,4)=coord(3,lat)-coord(3,iat)
c             if(coordmi(3,4).gt.0.5d0*edgez) 
c    x           coordmi(3,4)=coordmi(3,4)-edgez
c             if(coordmi(3,4).le.-0.5d0*edgez) 
c    x           coordmi(3,4)=coordmi(3,4)+edgez

c             call dihint(1,coordmi(1,1),coordmi(1,2),
c    x                      coordmi(1,3),coordmi(1,4),
c    x                  phi,dum1,dum2)

              call dihint(1,coord(1,iat),coord(1,jat),
     x                      coord(1,kat),coord(1,lat),
     x                  phi,dum1,dum2)


              phi=phi*(180.d0/(4.d0*datan(1.d0)))
              nbuf=nbuf+1
              buffer(nbuf)=phi

              if(iframe.eq.1.or.iprint.eq.10) then
                is=index(atname(iat),' ')-1
                js=index(atname(jat),' ')-1
                ks=index(atname(kat),' ')-1
                ls=index(atname(lat),' ')-1
                write(*,'('' Frame '',i5,'' Molecule '',i6,'' Phi '',
     x          f10.4,'' Sites '',
     x          4i6,4x,a,'' - '',a,'' - '',a,'' - '',a)')
     x          iframe,im,phi,iat,jat,kat,lat,
     x          atname(iat)(1:is),
     x          atname(jat)(1:js),
     x          atname(kat)(1:ks),
     x          atname(lat)(1:ls)
              endif
            enddo   !end of do loop over the list of torsions for this molecule




c
c           compute intersite distances for this molecule
c
            it1=irange(ir,5)  !first intersite distance 
            it2=irange(ir,6)  !last intersite distance
            do i=it1,it2
              j=idlist(1,i)+molptr(1,im)-1
              k=idlist(2,i)+molptr(1,im)-1
              dist=dsqrt( (coord(1,j)-coord(1,k))**2
     x                   +(coord(2,j)-coord(2,k))**2
     x                   +(coord(3,j)-coord(3,k))**2  )
              nbuf=nbuf+1
              buffer(nbuf)=dist
              if(iframe.eq.1.or.iprint.eq.10) then
                js=index(atname(j),' ')-1
                ks=index(atname(k),' ')-1
                write(*,'('' Frame '',i5,'' Molecule '',i6,'' Dist'',
     x            f10.4,'' Sites '',4i6,4x,a,'' - '',a)')
     x            iframe,im,dist,j,(j-molptr(1,im)+1),
     x            k,(k-molptr(1,im)+1),atname(j)(1:js),atname(k)(1:ks) 
              endif
            enddo

c
c           compute Rg for molecule
c

            if(irange(ir,7).eq.1) then   !compute radius of gyration (Rg) for molecule
              comx=0.d0
              comy=0.d0
              comz=0.d0
              totmas=0.d0
              do j=ifrst,ilast
                jt=itype(j)
                fm=fmass(jt)
                fm=1.d0
                comx=comx+fm*coord(1,j)
                comy=comy+fm*coord(2,j)
                comz=comz+fm*coord(3,j)
                totmas=totmas+fm
              enddo
              comx=comx/totmas
              comy=comy/totmas
              comz=comz/totmas
              rgx=0.d0
              rgy=0.d0
              rgz=0.d0
              do j=ifrst,ilast
                rgx=rgx+(coord(1,j)-comx)**2
                rgy=rgy+(coord(2,j)-comy)**2
                rgz=rgz+(coord(3,j)-comz)**2
              enddo
              rg=dsqrt((rgx+rgy+rgz)/(ilast-ifrst+1))
              nbuf=nbuf+1
              buffer(nbuf)=rg     
              if(iframe.eq.1.or.iprint.eq.10) 
     x          write(*,'('' Frame '',i5,'' Molecule '',i6,'' Rg  '',
     x            f10.4)') iframe,im,rg
            endif  !end of block to compute Rg


c
c           compute end-to-end distance for molecule (largest distance between non-H sites)
c
            if(irange(ir,8).eq.1) then   !compute end-to-end distance
c
c             find largest end to end distance         
c
              distmax=0.d0
              do j=ifrst,ilast-1
                jt=itype(j)
                if(fmass(jt).gt.2.d0) then
                  do k=j+1,ilast
                    kt=itype(k)
                    if(fmass(kt).gt.2.d0) then
                      dist=dsqrt( (coord(1,j)-coord(1,k))**2
     x                           +(coord(2,j)-coord(2,k))**2
     x                           +(coord(3,j)-coord(3,k))**2  )
                      if(dist.gt.distmax)  then
                        distmax=dist
                        jmx=j
                        kmx=k
                      endif
                    endif
                  enddo
                endif
              enddo
              nbuf=nbuf+1
              buffer(nbuf)=distmax
              if(iframe.eq.1.or.iprint.eq.10) then
                js=index(atname(jmx),' ')-1
                ks=index(atname(kmx),' ')-1
                write(*,'('' Frame '',i5,'' Molecule '',i6,'' E2E '',
     x          f10.4,'' Sites '',4i6,4x,a,'' - '',a)')  
     x          iframe,im,distmax,
     x          jmx,(jmx-molptr(1,im)+1),
     x          kmx,(kmx-molptr(1,im)+1),
     x          atname(jmx)(1:js),atname(kmx)(1:ks) 
              endif
            endif  !end of block to compute end-to-end distance


c           flush the final buffer (buffer has torsions, end-to-end, Rg, intersite distances)
            if(nbuf.gt.0) then
              write(11,'(i5,i10,100f10.4)') 
     x          iframe,im,(buffer(j),j=1,nbuf)
            endif

          endif  !end of if-block for whether this molecule is in a range

        enddo  !end of loop over molecules

      enddo   !end of loop over frames

      close(11)




      end

c
c     subroutine to compute electrostatic and vdw intramolecular energy
c
      subroutine vdwcou(nsites,itype,coord,charge,vdw,atname,
     x                  natmx,nattpmx,
     x                  nbonds,ijbond,nbnmx,
     x                  nangle,ijkangle,nanmx)
      implicit real*8 (a-h,o-z)
      dimension coord(3,natmx),charge(natmx),itype(natmx)
      character*(*) atname(nattpmx)
      dimension vdw(4,nattpmx)
      dimension ijbond(3,nbnmx)
      dimension ijkangle(4,nanmx)

      dimension neigh(5000,5)
      dimension ipair14(2,8000)

c
c     electrostatics and vdw
c
      ecou=0.d0
      evdw=0.d0
      do i=1,nsites-1
        it=itype(i)
        do j=i+1,nsites
        jt=itype(j)
        dist=dsqrt( (coord(1,i)-coord(1,j))**2
     x             +(coord(2,i)-coord(2,j))**2
     x             +(coord(3,i)-coord(3,j))**2 )
        ecou=ecou + charge(i)*charge(j)/dist
        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/(dist**12) - cv/(dist**6)
        evdw=evdw+eij                  
c       write(*,'('' i,j,r = '',2i5,f10.4,'' sig eps = '',
c    x    2f10.4,'' Eij = '',f10.3)')
c    x    i,j,dist,sig,eps,eij
        enddo
      enddo


c
c     build neighbor list
c
      do i=1,5000
        do j=1,5
          neigh(i,j)=0
        enddo
      enddo




      e12=0.d0
      e12vdw=0.d0
      do i=1,nbonds
        ia=ijbond(1,i)
        ib=ijbond(2,i)
        na=neigh(ia,1)+1
        neigh(ia,1)=na
        neigh(ia,na+1)=ib
        nb=neigh(ib,1)+1
        neigh(ib,1)=nb
        neigh(ib,nb+1)=ia

        dist=dsqrt( (coord(1,ia)-coord(1,ib))**2
     x             +(coord(2,ia)-coord(2,ib))**2
     x             +(coord(3,ia)-coord(3,ib))**2 )
        e12=e12 + charge(ia)*charge(ib)/dist
        it=itype(ia)
        jt=itype(ib)
        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
        if(eij.gt.1000.d0) then
          write(*,'('' Large VDW term, i,j = '',2i6,
     x      '' e_ij = '',f25.4)') ia,ib,eij
  
        endif
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/(dist**12) - cv/(dist**6)
        e12vdw=e12vdw+eij
      enddo


c     note that every 1-3 interaction is in an angle term and it is there only once
      e13=0.d0
      e13vdw=0.d0
      do i=1,nangle
        ia=ijkangle(1,i)
        ja=ijkangle(2,i)
        ka=ijkangle(3,i)
        dist=dsqrt( (coord(1,ia)-coord(1,ka))**2
     x             +(coord(2,ia)-coord(2,ka))**2
     x             +(coord(3,ia)-coord(3,ka))**2 )
        e13=e13 + charge(ia)*charge(ka)/dist
        it=itype(ia)
        jt=itype(ka)
        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/(dist**12) - cv/(dist**6)
        e13vdw=e13vdw+eij                  
      enddo



c     every 1-4 interaction is in a dihedral term, but multiple dihedrals have the same 1-4
c     some dihedrals are improper and the 1-4 with these are really 1-2 and 1-3
c     so to get the 1-4 list check each torsion, 

      e14=0.d0
      e14vdw=0.d0
      n14=0
      do i=1,nsites
        ni=neigh(i,1) !neighbors of i
        do ineigh=1,ni
          j=neigh(i,ineigh+1) 
          nj=neigh(j,1) !neighbors of j
          do jneigh=1,nj
            k=neigh(j,jneigh+1)
            if(k.ne.i) then   !...that are not i
              nk=neigh(k,1) !neighbors of k
              do kneigh=1,nk
                l=neigh(k,kneigh+1)
                if(l.ne.j) then
                  if(l.gt.i) then
                    n14=n14+1
                    ipair14(1,n14)=i
                    ipair14(2,n14)=l
c                   write(*,'('' 1-4 interaction: '',3i6,
c    x              a,1x,a,1x,a,1x,a)')
c    x              n14,i,l,atname(i),atname(j),atname(k),atname(l)
        dist=dsqrt( (coord(1,i)-coord(1,l))**2
     x             +(coord(2,i)-coord(2,l))**2
     x             +(coord(3,i)-coord(3,l))**2 )
        e14=e14 + charge(i)*charge(l)/dist
        it=itype(i)
        jt=itype(l)
        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/dist**12 - cv/dist**6
        e14vdw=e14vdw+eij                  
                  endif
                endif
              enddo
            endif
          enddo  
        enddo
      enddo


      e14x=0.d0
      e14xdup=0.d0

      e14xvdw=0.d0
      e14xvdwdup=0.d0

      do i=1,n14
        ia=ipair14(1,i)
        ja=ipair14(2,i)
        it=itype(ia)
        jt=itype(ja)
        dist=dsqrt( (coord(1,ia)-coord(1,ja))**2
     x             +(coord(2,ia)-coord(2,ja))**2
     x             +(coord(3,ia)-coord(3,ja))**2 )

        e14x=e14x + charge(ia)*charge(ja)/dist

        sig=0.5d0*(vdw(1,it)+vdw(1,jt))
        sig=dsqrt(vdw(1,it)*vdw(1,jt))
        eps=dsqrt(vdw(2,it)*vdw(2,jt))
        x=(sig/dist)**6
        eij=4.d0*eps*x*(x-1.d0)
c       av=4.d0*eps*(vdw(1,it)*vdw(1,jt))**6
c       cv=4.d0*eps*(vdw(1,it)*vdw(1,jt))**3
c       eij=av/dist**12 - cv/dist**6

        e14xvdw=e14xvdw+eij                  

        do j=i+1,n14
          ib=ipair14(1,j)
          jb=ipair14(2,j)
          if(ib.eq.ia.and.jb.eq.ja) then
            write(*,'('' duplicate 1-4:  '',a,1x,a)')
     x        atname(ib),atname(jb)
            e14xvdwdup=e14xvdwdup+eij
            e14xdup=e14xdup+
     x              charge(ia)*charge(ja)/dist
          endif
        enddo
      enddo


      fact=627.509d0*0.529177d0  !332.d0
      write(*,'('' Factor = '',f15.6)') fact

      write(*,'(//,'' Electrostatic interactions '',/)') 

      write(*,'(/,'' Ecou from all 1-4 terms     : '',f10.5)')
     x   fact*e14x
      write(*,'('' Ecou from duplicated terms: '',f10.5)')
     x   fact*e14xdup
      write(*,'('' Ecou without duplicates   : '',f10.5)')
     x   fact*e14x-e14xdup


      write(*,'('' Ecou= '',f15.6)') fact*ecou
      write(*,'('' E12 = '',f15.6)') fact*e12
      write(*,'('' E13 = '',f15.6)') fact*e13 
      write(*,'('' E14 = '',f15.6)') fact*e14 
c     write(*,'('' E14x= '',f15.6)') fact*e14x 


      write(*,'(/,'' Ecoul-E12-E13-E14     = '',f15.6)') 
     x    fact*(ecou-e13-e12-e14)
      write(*,'(  '' E14                   = '',f15.6)') 
     x    fact*e14
      write(*,'(  '' Ecoul-E12-E13-0.5*E14 = '',f15.6)') 
     x    fact*(ecou-e13-e12-e14+0.5d0*e14)




      write(*,'(//,'' Van der Waals interactions '',/)') 

      write(*,'(/,'' Evdw from all 1-4 terms     : '',f10.5)')
     x   e14xvdw
      write(*,'('' Evdw from duplicated terms: '',f10.5)')
     x   e14xvdwdup
      write(*,'('' Evdw without duplicates   : '',f10.5)')
     x   e14xvdw-e14xvdwdup


      write(*,'(/,'' Evdw= '',f25.4)') evdw
      write(*,'('' E12v= '',f25.4)') e12vdw
      write(*,'('' E13v= '',f25.4)') e13vdw
      write(*,'('' E14v= '',f25.4)') e14vdw-e14xvdwdup

      write(*,'(/,'' Evdw-E12v-E13v-E14v     = '',f25.4)') 
     x    (evdw-e13vdw-e12vdw-(e14vdw-e14xvdwdup))
      write(*,'(  '' E14                     = '',f25.4)') 
     x    (e14vdw-e14xvdwdup)
      write(*,'(  '' Evdw-E12v-E13v-0.5*E14v = '',f25.4)') 
     x    (evdw-e13vdw-e12vdw-(e14vdw-e14xvdwdup)
     x    +0.5d0*(e14vdw-e14xvdwdup))




      end
c
c     subroutine to read a set of coordinates from an xyz file
c     expect this to have a set of coords, we are usually interested
c     in either the first or the last
c
c     icntrl = 0 means to count the number of coordinate sets, 
c                read in the first, return the count as icntrl
c     icntrl .ne. 0 means to retrieve coordinate set icntrl
c
      subroutine getcoord(fname,nsites,itype,coord,icntrl)
      implicit real*8 (a-h,o-z)
      character*120 line
      character*(*) fname
      dimension coord(3,nsites),itype(nsites)

      is=index(fname,' ')-1
      write(*,'(/,'' Reading coordinates from xyz file *'',a,''*'')')
     x     fname(1:is)

      open(10,file=fname(1:is))  
c
c     file format: 
c     line 1: number
c     line 2: Atoms  
c     line 3,... for each atom a type and three coords
c     this is repeated for as many frames
c
c
c     first count how many frames save line number of last
      if(icntrl.eq.0) then
c
        nframe=0
10      continue
        read(10,'(a120)',end=50) line
          nframe=nframe+1
        read(line,*) nat
        if(nat.ne.nsites) then
          write(*,'('' Error:  xyz and lmp not matched '',/,
     x    '' Files have different number of sites:  N(xyz) = '',
     x    i6,''; N(lmp) = '',i6)') nat,nsites
          stop 'xyz file not matched with cooresponding lmp file'
        endif
        read(10,'(a120)') line   !line has "Atoms"
        if(mod(nframe,10).eq.0) write(*,'('' Reading frame '',i3)')
     x    nframe
        do i=1,nat
          read(10,'(a120)') line
          if(nframe.eq.1) then
          read(line,*) it
          if(it.ne.itype(i)) then
            write(*,'('' Type mismatch between xyz and lmp files '')')
            write(*,'('' Atom number '',i6,
     x        '' Type(xyz) = '',i4,'' Type(lmp) = '',i4)') i,it,itype(i)
            stop 'xyz file and lmp file have different atoms types'
          endif
          endif
        enddo
        go to 10

50      continue
    
      elseif(icntrl.gt.0) then
     
        iframe=icntrl
        write(*,'('' Retrieving frame '',i5)')
     x            iframe

        rewind(10)
c       position file to right place
        do i=1,(2+nsites)*(iframe-1)
          read(10,'(a120)') line
        enddo
c       now read into coord
        read(10,'(a120)') line
        read(10,'(a120)') line
        do i=1,nsites
          read(10,'(a120)') line
          read(line,*) it,(coord(k,i),k=1,3)
        enddo
c

      endif

      close(10)

      if(icntrl.eq.0) then
        icntrl=nframe
        write(*,'('' This xyz file contains '',i5,'' frames'')')
     x            nframe
      endif


      end

c
c     subroutine to read in lmp parameter/coordinate file
c
      subroutine readlmp(fname,nsites,itype,imol,charge,coord,atname,
     x                    natmx,
     x                    nbonds,nbtp,bndpar,ijbond,nbnmx,nbtmx,
     x                    nangle,natp,angpar,ijkangle,nanmx,nantmx,
     x                    ndih,ndtp,dihpar,ijkldih,ndimx,nditmx,
     x                    natomtp,fmass,vdw,nametypes,nattpmx,
     x                    box)
      implicit real*8 (a-h,o-z)
      character*(*) fname
      character*120 line

c     natmx - max number of atoms (sites) (nsites.le.natmx) 
c     nattpmx - max number of atom types (natomtp.le.nattpmx)

c     nbonds - number of bonds
c     nbnmx - max number of bonds (nbonds.le.nbnmx)
c     nbtp - number of bond types
c     nbtmx - max number of bond types (nbtp.le.nbtmx)

c     nangle - number of angles
c     nanmx - max number of angles (nangle.le.nanmx)
c     natp - number of angle types
c     nantmx - max number of angle types (natp.le.nantmx)

c     ndih - number of dihedral angles
c     ndimx - max number of dihedral angles (ndih.le.ndimx)
c     ndtp - number of dihedral angle types
c     nditmx - max number of dihedral angle types (ndtp.le.nditmx)

c     natomtp - number of atom types
c     nattpmx - max number of atom types (natomtp.le.nattpmx)

      dimension coord(3,natmx),charge(natmx),itype(natmx),imol(natmx)
      character*(*) atname(natmx)
      dimension box(3,2) 

      dimension ijbond(3,nbnmx),bndpar(2,nbtmx)
      dimension ijkangle(4,nanmx),angpar(2,nantmx)
      dimension ijkldih(5,ndimx),dihpar(4,nditmx)

      dimension vdw(4,nattpmx),fmass(nattpmx)
      character*(*) nametypes(nattpmx)

      is=index(fname,' ')-1
      write(*,'(/,'' Reading parameters from file *'',a,''*'')')
     x     fname(1:is)
      open(10,file=fname)  

c     read number of atoms and their coordinates
c     read nsites,itype,charge,coords,atname

10    continue
      read(10,'(a120)',end=21) line
      if(index(line,'atoms').eq.0) go to 10
      read(line,*) nsites
      write(*,'('' natoms = '',i8)') nsites
      if(nsites.gt.natmx) then
        write(*,'('' Increase size of site array; '',/,
     x            '' nsites = '',i10,'' natmx = '',i10)')
     x            nsites,natmx
        stop 'Increase natmx'
      endif
21    continue


      write(*,'('' Reading coordinates and charges for '',i7,
     x  '' sites '')') nsites
20    continue
      read(10,'(a120)',end=22) line
      if(index(line,'Atoms').eq.0) go to 20
      read(10,'(a120)') line
      totchg=0.d0
      do i=1,nsites
        read(10,'(a120)') line
        read(line,*) ix,im,it1,ch1,x1,y1,z1
c       read(line,'(i10,i5,i5)') ix,im,it1 
c       read(line(21:),*) ch1,x1,y1,z1
        if(ix.gt.nsites.or.ix.le.0) then
          write(*,'('' Error: site index out of range i = '',i10,
     x      '' ix = '',i10)') i,ix
          stop 'error reading coordinates'
        endif 
        itype(ix)=it1
        imol(ix)=im
        charge(ix)=ch1
        coord(1,ix)=x1
        coord(2,ix)=y1
        coord(3,ix)=z1
        if(mod(i,5000).eq.0) then
          write(*,'('' Reading coordinates for '',i6,'' of '',i6)') 
     x      i,nsites
        endif
        iloc=index(line,'#')
        if(iloc.ne.0) then
          read(line(iloc+1:),'(a20)') atname(ix)
        else
          write(atname(ix),'(''X'',i5.5)') ix
        endif
        totchg=totchg+charge(ix)
c       write(*,'('' IN ATOM LOOP: ix,im,it1,atname: '',3i5,
c    x    '' *'',a,''*'')')
c    x    ix,im,it1,atname(ix)
      enddo

      write(*,'('' Total charge '',f15.5)') totchg
22    continue
      
c     do i=1,nsites
c       write(*,'('' Site '',i6,'' *'',a,''* Type '',i5,3f10.5)')
c    x    i,atname(i)(1:20),itype(i)
c    x    ,(coord(k,i),k=1,3)
c     enddo

c
c     read box sizes
c
      rewind(10)
      ifxset=0
      ifyset=0
      ifzset=0
23    continue
      read(10,'(a120)') line
      isx=index(line,'xlo') 
      isy=index(line,'ylo') 
      isz=index(line,'zlo') 
      if(isx+isy+isz.eq.0) go to 23
      if(isx.ne.0) then 
        read(line,*) xlo,xhi
        ifxset=1
      elseif(isy.ne.0) then
        read(line,*) ylo,yhi
        ifyset=1
      elseif(isz.ne.0) then
        read(line,*) zlo,zhi
        ifzset=1
      endif
      if(ifxset*ifyset*ifzset.eq.0) go to 23
c     write(*,'('' Box dimensions (X): '',2f15.8,/,
c    x          '' Box dimensions (Y): '',2f15.8,/,
c    x          '' Box dimensions (Z): '',2f15.8)')
c    x  xlo,xhi,ylo,yhi,zlo,zhi
      box(1,1)=xlo
      box(1,2)=xhi
      box(2,1)=ylo
      box(2,2)=yhi
      box(3,1)=zlo
      box(3,2)=zhi


c     read bonds
c     read nbonds,nbtp,bndpar(2,*),ijbond(3,*)

      rewind(10)
30    continue
      read(10,'(a120)',end=40) line
      if(index(line,' bonds ').eq.0) go to 30
      read(line,*) nbonds
      if(nbonds.gt.nbnmx) then
        write(*,'('' Increase size of bond array; '',/,
     x            '' nbonds = '',i10,'' nbnmx = '',i10)')
     x            nbonds,nbnmx
        stop 'Increase nbnmx'
      endif
40    continue
      read(10,'(a120)',end=45) line
      if(index(line,' bond types ').eq.0) go to 40
      read(line,*) nbtp
c     write(*,'('' Nbond types = '',i6)') nbtp
      if(nbtp.gt.nbtmx) then
        write(*,'('' Increase size of bond type array; '',/,
     x            '' nbtp   = '',i10,'' nbtmx = '',i10)')
     x            nbtp,nbtmx
        stop 'Increase nbtmx'
      endif
45    continue
      read(10,'(a120)',end=50) line
      if(index(line,'Bond Coeffs').eq.0) go to 45
      read(10,'(a120)') line
      do i=1,nbtp  
        read(10,'(a120)') line
        read(line,*) ix,bndpar(1,i),bndpar(2,i)
c       write(*,'('' Bond type '',i3,'' Parm '',2f10.5)')
c    x     i,bndpar(1,i),bndpar(2,i)
      enddo
50    continue
      read(10,'(a120)',end=55) line
      if(index(line,'Bonds').eq.0) go to 50

      write(*,'('' Reading list of '',i7,
     x  '' bonds '')') nbonds
      read(10,'(a120)') line
      do i=1,nbonds
        read(10,'(a120)') line
        read(line,*) ix,ijbond(3,i),ijbond(1,i),ijbond(2,i)
        if(mod(i,10000).eq.0) then
          write(*,'('' Reading bonds '',i6,'' of '',i6)') 
     x      i,nbonds
        endif
c       write(*,'('' Bond '',i6,'' Type '',i4,'' Sites '',2i6)')
c    x    i,ijbond(3,i),ijbond(1,i),ijbond(2,i)
      enddo


c     read angles
c     nangle,natp,angpar(2,*),ijkangle(4,*)

      rewind(10)
55    continue
      read(10,'(a120)',end=60) line
      if(index(line,' angles').eq.0) go to 55
      read(line,*) nangle
      if(nangle.gt.nanmx) then
        write(*,'('' Increase size of angle array; '',/,
     x            '' nangle = '',i10,'' nanmx = '',i10)')
     x            nangle,nanmx
        stop 'Increase nanmx'
      endif
60    continue
      read(10,'(a120)',end=65) line
      if(index(line,' angle types ').eq.0) go to 60
      read(line,*) natp
      if(natp.gt.nantmx) then
        write(*,'('' Increase size of angle type array; '',/,
     x            '' natp   = '',i10,'' nantmx = '',i10)')
     x            natp,nantmx
        stop 'Increase nantmx'
      endif
65    continue
      read(10,'(a120)',end=70) line
      if(index(line,'Angle Coeffs').eq.0) go to 65
      read(10,'(a120)') line
      do i=1,natp  
        read(10,'(a120)') line
        read(line,*) ix,angpar(1,i),angpar(2,i)
      enddo
70    continue
      read(10,'(a120)',end=71) line
      if(index(line,'Angles').eq.0) go to 70
      write(*,'('' Reading list of '',i7,
     x  '' angles '')') nangle
      read(10,'(a120)') line
      do i=1,nangle
        read(10,'(a120)') line
        read(line,*) ix,ijkangle(4,i),(ijkangle(k,i),k=1,3)
        if(mod(i,10000).eq.0) then
          write(*,'('' Reading angles'',i6,'' of '',i6)') 
     x      i,nangle
        endif
      enddo
71    continue



c     read dihedrals
c     read ndih,ndtp,dihpar(4,*),ijkldih(5,*)

      rewind(10)
75    continue
      read(10,'(a120)',end=80) line
      if(index(line,' dihedrals').eq.0) go to 75
      read(line,*) ndih  
      if(ndih.gt.ndimx) then
        write(*,'('' Increase size of dihedral array; '',/,
     x            '' ndih   = '',i10,'' ndimx = '',i10)')
     x            ndih,ndimx
        stop 'Increase ndimx'
      endif
80    continue
      read(10,'(a120)',end=85) line
      if(index(line,' dihedral types ').eq.0) go to 80
      read(line,*) ndtp
c     write(*,'('' Dihedrals: '',i6,''  Types: '',i6)')
c    x   ndih,ndtp
      if(ndtp.gt.nditmx) then
        write(*,'('' Increase size of dihedral type array; '',/,
     x            '' ndtp   = '',i10,'' nditmx = '',i10)')
     x            ndtp,nditmx
        stop 'Increase nditmx'
      endif
85    continue
      read(10,'(a120)',end=90) line
      if(index(line,'Dihedral Coeffs').eq.0) go to 85
      read(10,'(a120)') line
      do i=1,ndtp  
        read(10,'(a120)') line
c       write(*,'('' Line: *'',a,''*'')') line(1:50)
        read(line,*) ix,dihpar(1,i),nd,dd,dihpar(4,i)
c       write(*,'('' '',i5,f10.4,i5,i5,f10.4)') 
c    x    is,dihpar(1,i),nd,dd,dihpar(4,i)
        dihpar(2,i)=nd         
        dihpar(3,i)=dd         
      enddo
c     write(*,'('' Types read in '')')
90    continue
      read(10,'(a120)',end=91) line
      if(index(line,'Dihedrals').eq.0) go to 90
      write(*,'('' Reading list of '',i7,
     x  '' dihedrals '')') ndih  
      read(10,'(a120)') line
      do i=1,ndih  
        read(10,'(a120)') line
        read(line,*) ix,ijkldih(5,i),(ijkldih(k,i),k=1,4)
        if(mod(i,20000).eq.0) then
          write(*,'('' Reading dihedral '',i6,'' of '',i6)') 
     x      i,ndih   
        endif
      enddo
c     write(*,'('' Dihedrals read in '')')
91    continue


c     read vdw
c     read natomtp,vdw(4,*)

      rewind(10)
100   continue
      read(10,'(a120)',end=105) line
      if(index(line,' atom types ').eq.0) go to 100
      read(line,*) natomtp
      write(*,'('' Reading list of '',i7,
     x  '' vdw parameters '')') natomtp
      if(natomtp.gt.nattpmx) then
        write(*,'('' Increase size of vdw array; '',/,
     x            '' natomtp   = '',i10,'' nattpmx = '',i10)')
     x            natomtp,nattpmx
        stop 'Increase nattpmx'
      endif
105   continue
      read(10,'(a120)',end=107) line
      if(index(line,'Pair Coeffs').eq.0) go to 105
      read(10,'(a120)') line
      do i=1,natomtp  
        read(10,'(a120)') line
        read(line,*) ix,vdw(2,i),vdw(1,i),vdw(4,i),vdw(3,i)
c       write(*,'('' Atom type '',i3,'' vdw = '',2f10.5)')
c    x     i,vdw(1,i),vdw(2,i)
      enddo
107   continue

c
c     read masses 
c     note that we are assuming that extra information is on these
c     lines in files made by ffassign with type names
c
      rewind(10)
26    continue
      read(10,'(a120)') line
      if(index(line,'Masses').eq.0) go to 26
      write(*,'('' Reading list of '',i7,
     x  '' masses  '')') natomtp
      read(10,'(a120)') line
      do i=1,natomtp  
        read(10,'(a120)') line
        read(line,*) ix,fmass(i)
c       write(*,'('' Atom type '',i3,'' mass = '',f10.5)')
c    x     i,fmass(i)           

c     extract the name string: character*100 nametypes(100)
        nametypes(i)=' '
        is=index(line,'#')
        ks=1
        ie=1
        if(is.ne.0) then
c         in line(is+1:) look for LAST token         
c         this will be a nonblank string surrounded by blanks
          ie=0
          do js=len(line),is+1,-1
            if(line(js:js).ne.' ') then
              ie=js
              go to 210
            endif
          enddo
210       continue
          if(ie.ne.0) then
            ks=0
            do js=ie,is+1,-1
              if(line(js:js).eq.' ') then
                ks=js+1 
                go to 215
              endif
            enddo
          endif
215       continue
c         at this point the last token is in line(ks:ie) 
          nametypes(i)=line(ks:ie)
        endif
        write(*,'('' For atom type '',i3,'' Name *'',a,''*'')')
     x    i,nametypes(i)(1:ie-ks+1)
      enddo
c


200   continue

      close(10)


      end


c
c     subroutine to compute bond energy
c
      subroutine bengy(nsites,coord,nbonds,ijbond,nbtp,bndpar)
      implicit real*8 (a-h,o-z)
      dimension coord(3,nsites),ijbond(3,nbonds),bndpar(2,nbtp)

      ebond=0.d0
      do i=1,nbonds
        ia=ijbond(1,i)
        ja=ijbond(2,i)
        dist=dsqrt( (coord(1,ia)-coord(1,ja))**2
     x             +(coord(2,ia)-coord(2,ja))**2
     x             +(coord(3,ia)-coord(3,ja))**2 )
        it=ijbond(3,i)
        eij=bndpar(1,it) * (dist - bndpar(2,it))**2
        ebond=ebond+eij
        write(*,'('' Site '',i6,'' R= '',3f10.5,'' ; Site '',i5,
     x    '' R= '',3f10.5)')  ia,(coord(k,ia),k=1,3),
     x                        ja,(coord(k,ja),k=1,3)
        write(*,'('' Bond '',i5,'' Sites '',2i6,'' dist '',f10.5,
     x    '' Eij '',f10.5)')
     x    i,ia,ja,dist,eij
      enddo
      write(*,'('' E(bond) = '',f15.6)') ebond



      end

c
c     subroutine to compute angle energy
c
      subroutine aengy(nsites,coord,nangle,ijkangle,natp,angpar)
      implicit real*8 (a-h,o-z)
      dimension coord(3,nsites),ijkangle(4,nangle),angpar(2,natp)
      pi=4.d0*datan(1.d0)


      eang=0.d0
      do i=1,nangle
        ia=ijkangle(1,i)
        ja=ijkangle(2,i)
        ka=ijkangle(3,i)
        it=ijkangle(4,i)
        call angint(coord(1,ia),coord(1,ja),coord(1,ka),thet)
        eijk=angpar(1,it) * (thet - (pi/180.d0)*angpar(2,it))**2
c       write(*,'('' Angle index = '',i5,'' type = '',i5,
c    x   '' parms = '',2f10.3)')
c    x   i,it,angpar(1,it),angpar(2,it)
c       write(*,'('' Angle = '',f10.3,'' Engy = '',f10.3)')
c    x    thet*(180.d0/pi),eij
        eang =eang +eijk
      enddo
      write(*,'('' E(angl) = '',f15.6)') eang 

      end


c
c     subroutine to compute dihedral energy
c
      subroutine dengy(nsites,coord,ndih,ijkldih,ndtp,dihpar)
      implicit real*8 (a-h,o-z)
      dimension coord(3,nsites),ijkldih(5,ndih),dihpar(4,ndtp)
      pi=4.d0*datan(1.d0)
      edih=0.d0
      do i=1,ndih  
        ia=ijkldih(1,i)
        ja=ijkldih(2,i)
        ka=ijkldih(3,i)
        la=ijkldih(4,i)
        it=ijkldih(5,i)
        call dihint(1,coord(1,ia),coord(1,ja),
     x                coord(1,ka),coord(1,la),
     x              phi,dum1,dum2)
        eijkl=dihpar(1,it) * ( 1.d0 + 
     x      cos(dihpar(2,it)*phi - (pi/180.d0)*dihpar(3,it)))
        edih =edih +eijkl
c       write(*,'('' Term '',i6,'' phi = '',f10.5,
c    x    '' Parms = '',3f10.5,'' E(ijkl) = '',f12.5)')
c    x   i,phi,dihpar(1,it),dihpar(2,it),dihpar(3,it),eijkl
      enddo
      write(*,'('' E(dihe) = '',f15.6)') edih 

      end



c Change History:
c $Log: util.f,v $
c Revision 1.8  1994/08/03  19:49:54  swope
c Changed behavior of xyz2int so that zmatricies are not printed out tha
c undefined torsions.  When angles are close to 180 they are changed to
c when close to 0 they are changed to 0.0001.    (Swope Aug 3, 1994)
c
c Revision 1.6  1994/07/30  06:33:09  swope
c Several changes by Swope to internal-cartesian and cartesian-internal
c conversion routines:
c angint-switch to low angle expression on new threshold
c dihint-call tlkemsg when atoms too close
c xyz2int-change messages during certain errors
c i2x-changes in ways linear systems treated: two strikes policy
c     for generating cartesians with undefined torsions
c xyz2dih-added new return code to flag 1-2-3 linear vs 2-3-4 linear
c
c Revision 1.5  1994/07/07  16:34:02  gclie
c This version includes xyz2int and int2xyz from Swope.  It should repla
c
c*arc i2x
      subroutine i2x(nat,lnk,rab,ang,dih,istart,iend,xyz)
c
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Copyright (c) 1994 International Business Machines Corporation
C  All rights reserved
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Description      Given the link vector and internal coordinates
c                     of a list of particles, this subroutine computes
c                     the cartesian coordinates for a subset of the
c                     particles, those with indicies from istart to
c                     iend.  The units of the cartesian coordinates
c                     will be the same as those of the input internals.
c                     The units of the angles are assumed to be radians.
c                   If the particle indicies between istart and iend
c                     fall in the range 1,2 or 3, particle 1 is placed
c                     at the origin, particle 2 is placed on the x-axis,
c                     and particle 3 is placed in the x-y plane.
c
C
C  Input
C
C     nat           Number of atoms in list
C
C     lnk(3,nat)    link vector for describing internals
C                     lnk(1,i) gives label for distance from i
C                     lnk(2,i) gives label for angle i-lnk(1,i)-lnk(2,i)
C                     lnk(3,i) gives label for torsion
C                     i-lnk(1,i)-lnk(2,i)-lnk(3,i)
C                     Note that first six entries are ignored.
C
C     rab(nat)      Vector of distances.
C
C     ang(nat)      Vector of angles.  Range returned is 0.le.ang.le.pi.
C
C     dih(nat)      Vector of torsions. Range returned is
C                   -pi .lt. phi .le. pi, and follows IUPAC
C                   sign and range convention.
c
c     istart        The index of the first atom whose cartesian
c                   coordinates are to be computed.  istart must be
c                   an integer with ( 1 .le. istart .le. nat ).
c
c     iend          The index of the last atom whose cartesian
c                   coordinates are to be computed.  iend must be
c                   an integer with ( 1 .le. iend .le. nat ).
C
C
C  Output
C
C     xyz(3,i)      cartesian coordinates of atoms i=istart to iend
C
C
C  Linkage          List of subroutines called by this one
C
C  Programmer       William Swope (some from Carnevali and Nesbet)
C
C  Date             June 1994
C
C  Update log       Record of all updates including names of programmers
C                     descriptions of changes made, and dates of change.
C                   07-29-94 Modified by Swope to die (tlkemsg) if
C                            there are TWO invalid torsion angles.
C                            Invalid torsion angles are those that
C                            result in ill-defined cartesian coordinates
C                            This change still allows linear molecules
C                            and molecules with two long linear segments
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
c
      Implicit Real*8 (a, b, d-h, o, p, r-y)
      Implicit Integer (i-n)
c     Violation of ARCHEM conventions, but there are no logicals here
c     Implicit Logical (z)
      Implicit Real*8 (z)
      Implicit Character (c)
c     Implicit Undefined (q)
 
      character cproduct*48
      dimension cproduct(3)
 
c     Input and output arrays
      real*8 xyz(3,nat),rab(nat),ang(nat),dih(nat)
      integer lnk(3,nat)
 
c     Local arrays
c     Following arrays give molecule coordinate system relative to lab
      real*8 xhat(3),yhat(3),zhat(3),zprev(3)
      real*8 r12(3),r23(3),r43(3)
 
 
C     THE SIGN CONVENTION FOR PHI IS AS FOLLOWS:
C     NUMBER THE ASSOCIATED ATOMS 1, 2, 3, 4.  ATOMS 1 AND 4 ARE
C     "TERMINAL" ATOMS AND THE DIHEDRAL ANGLE IS ABOUT THE BOND
C     BETWEEN ATOMS 2 AND 3.  IMAGINE HOLDING THE PLANE FORMED BY
C     ATOMS 1, 2, AND 3 FIXED AND ROTATING THE PLANE FORMED BY
C     ATOMS 2, 3, AND 4 ABOUT A VECTOR THAT POINTS FROM ATOM 2
C     TO ATOM 3.  THE POSITIVE SENSE CORRESPONDS TO THE RIGHT-HAND
C     RULE FOR ROTATION ABOUT THE VECTOR FROM ATOM 2 TO ATOM 3.
c     This agrees with the IUPAC convention.
C
C     FURTHERMORE, PHI=0 CORRESPONDS TO A CONDITION WHERE ALL FOUR
C     ATOMS ARE ON A PLANE AND ATOMS 1 AND 4 ARE "CIS" TO THE BOND
C     BETWEEN ATOMS 2 AND 3.  LIKEWISE, PHI=180 DEGREES CORRESPONDS
C     A CONDITION WHERE ALL FOUR ATOMS ARE ON A PLANE AND ATOMS 1 AND
C     4 ARE "TRANS" TO THE BOND BETWEEN ATOMS 2 AND 3.
 
c     comment out the following call to check the integrity of the
c     link vector.  this call is appropriate in int2xyz, where the
c     full set of cartesians is computed, but should probably not
c     be made over and over again if the cartesians are computed
c     one at a time or in small increments.
c
c     check validity of link vector
c     call chklnk(nat,lnk,iret)
c     if(iret.ge.2) then
c       print *, ' Error: link matrix is not usable for determination',
c    >           ' of cartesian coordinates.'
c       print *, ' Complete link vector: '
c       write(*,'(3i10)') ((lnk(i,j),i=1,3),j=1,nat)
c       call tlkemsg(' Invalid link matrix.','int2xyz')
c     endif
 
 
c     lnk(1,i),i=1,nat specifies atom indicies for "bond" distances
c     lnk(2,i),i=1,nat specifies atom indicies for "bond" angles
c     lnk(3,i),i=1,nat specifies atom indicies for dihedral angles
 
      pi=4.d0*datan(1.d0)
 
      if(istart.le.0   .or.
     >   istart.gt.nat .or.
     >   iend.le.0     .or.
     >   iend.gt.nat   .or.
     >   istart.gt.iend ) then
      write(*,'('' Error in subroutine i2x: invalid arguments for'',/,
     >          '' atom indicies: istart='',i5,'' iend='',i5,/,
     >          '' Values should be from 1 to nat='',i5)')
     >          istart,iend,nat
C     CALL TLKEMSG(' INVALID ATOM INDICES PASSED TO X2I.','X2I')
      endif
 
      zprev(1)=0.d0
      zprev(2)=0.d0
      zprev(3)=1.d0
 
c     failure count on placing atoms when torsion undefined
      nstrike=0
 
 
      do 10 i=istart,iend
 
      if(i.eq.1) then
c     if first atom
        xyz(1,1)=0.d0
        xyz(2,1)=0.d0
        xyz(3,1)=0.d0
      elseif(i.eq.2) then
c     if second atom
        xyz(1,2)=rab(2)
        xyz(2,2)=0.d0
        xyz(3,2)=0.d0
      elseif(i.eq.3) then
c     if third atom (note that normally 0.le.ang.le.pi)
        if(lnk(1,3).eq.2) then
          xyz(1,3)=rab(2) -rab(3)*cos(ang(3))
        else
          xyz(1,3)=       +rab(3)*cos(ang(3))
        endif
        xyz(2,3)=         +rab(3)*sin(ang(3))
        xyz(3,3)=0.d0
        if(abs(ang(3)).gt.1.d-3 .and.
     >     abs(abs(ang(3))-pi).gt.1.d-3) nstrike=1
      endif
 
c     for fourth or higher atom
      if(i.le.3) go to 10
 
      l2=lnk(1,i)
      l3=lnk(2,i)
      l4=lnk(3,i)
      r23(1)=xyz(1,l2)-xyz(1,l3)
      r23(2)=xyz(2,l2)-xyz(2,l3)
      r23(3)=xyz(3,l2)-xyz(3,l3)
 
c     local x axis is directed from atom l3 to atom l2
      d=r23(1)**2+r23(2)**2+r23(3)**2
      if(d.le.1.d-10) then
        print *, ' Error: Atoms too close to establish unique',
     >           ' directions for cartesian placement from z-matrix.'
        print *, ' Atom indicies: ',l2,l3
C       CALL TLKEMSG(' ATOMS TOO CLOSE.','I2X')
      endif
      d=1.d0/sqrt(d)
      xhat(1)=r23(1)*d
      xhat(2)=r23(2)*d
      xhat(3)=r23(3)*d
 
c     local z axis is directed along r23 cross r43, if this is non-zero
      r43(1)=xyz(1,l4)-xyz(1,l3)
      r43(2)=xyz(2,l4)-xyz(2,l3)
      r43(3)=xyz(3,l4)-xyz(3,l3)
      zhat(1)=r23(2)*r43(3)-r23(3)*r43(2)
      zhat(2)=r23(3)*r43(1)-r23(1)*r43(3)
      zhat(3)=r23(1)*r43(2)-r23(2)*r43(1)
      d=zhat(1)**2+zhat(2)**2+zhat(3)**2
c
      if(d.le.1.d-8) then
        if(abs(ang(i)).gt.1.d-3 .and.
     >     abs(abs(ang(i))-pi).gt.1.d-3) then
c         Problem: angle 4-3-2 is (nearly) linear AND ang(i) is other
c         than zero or pi.  This means placement of particle 1 is
c         undefined.  The first time this happens, we will allow it,
c         and place particle 1 in the (local) x-y plane.  This
c         corresponds to setting zhat to the lab z axis.
          if(nstrike.ne.0) then
          print *
          print *, ' A sequence of bond angles near 180 (or 0) degrees'
          print *, ' has resulted in an undefined torsion angle and'
          print *, ' this torsion angle must be defined to determine'
          print *, ' unique Cartesian coordinates for the molecule.'
          print *
          print *, ' Offending link vector:'
          print *, ' lnk(1,',i,') = ',l2
          print *, ' lnk(2,',i,') = ',l3
          print *, ' lnk(3,',i,') = ',l4
          print *
C         CALL TLKEMSG('CANNOT GENERATE CARTESIAN COORDINATES.','I2X')
          endif
          nstrike=1
        endif
        zhat(1)=zprev(1)
        zhat(2)=zprev(2)
        zhat(3)=zprev(3)
      else
        d=1.d0/sqrt(d)
        zhat(1)=zhat(1)*d
        zhat(2)=zhat(2)*d
        zhat(3)=zhat(3)*d
        zprev(1)=zhat(1)
        zprev(2)=zhat(2)
        zprev(3)=zhat(3)
      endif
 
c     local y axis is zhat cross xhat
      yhat(1)=zhat(2)*xhat(3)-zhat(3)*xhat(2)
      yhat(2)=zhat(3)*xhat(1)-zhat(1)*xhat(3)
      yhat(3)=zhat(1)*xhat(2)-zhat(2)*xhat(1)
      d=1.d0/sqrt(yhat(1)**2+yhat(2)**2+yhat(3)**2)
      yhat(1)=yhat(1)*d
      yhat(2)=yhat(2)*d
      yhat(3)=yhat(3)*d
 
      if(abs(ang(i)).gt.1.d-3 .and.
     >   abs(abs(ang(i))-pi).gt.1.d-3) nstrike=1
 
c     in molecule frame (i.e., relative to xhat,yhat,zhat coord system)
c     new center is placed at ...
      x=-rab(i)*cos(ang(i))
      rho=rab(i)*sin(ang(i))
      y=rho*cos(dih(i))
      z=rho*sin(dih(i))
 
c     in lab frame ...
      xyz(1,i) = xyz(1,l2) + x*xhat(1)+y*yhat(1)+z*zhat(1)
      xyz(2,i) = xyz(2,l2) + x*xhat(2)+y*yhat(2)+z*zhat(2)
      xyz(3,i) = xyz(3,l2) + x*xhat(3)+y*yhat(3)+z*zhat(3)
 
10    continue
 
      end


c
c     angle calculation stolen from dihint
c
      subroutine angint(xyz1,xyz2,xyz3,ang)
      Implicit Real*8 (a-h,o-z)
c     Input and output arrays
      real*8 xyz1(3),xyz2(3),xyz3(3)
      real*8 a(3),b(3)

c     Compute a=r1-r2, b=r3-r2
      do 10 k=1,3
      a(k)=xyz1(k)-xyz2(k)
10    b(k)=xyz3(k)-xyz2(k)
 
c     Compute a . b 
      dot=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
 
c     Compute lengths
      b2=b(1)**2+b(2)**2+b(3)**2
      a2=a(1)**2+a(2)**2+a(3)**2
      angcos=dot/dsqrt(a2*b2)
      ang=dacos(angcos)

      end





c Change History:
c $Log$
c*arc dihint
      subroutine dihint(iop,xyz1,xyz2,xyz3,xyz4,
     >       phi,dphidx,d2phidx2)
c
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Copyright (c) 1994 International Business Machines Corporation
C  All rights reserved
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Description      Given the coordinates of four particles, this
C                     subroutine computes the torsion (dihedral)
C                     angle between them, the Jacobean (dphi/dx(i))
C                     and d2phi/dx(i)dx(j).
C
C  Input
C
C     iop           Control
C                   iop = 1 means just compute angle
C                   iop = 2 means compute angle and Jacobean
C                   iop = 3 means compute angle, Jacobean and second
C                           derivative
C
C     xyz1(3)       cartesian coordinates of first atom
C     xyz2(3)       cartesian coordinates of second atom
C     xyz3(3)       cartesian coordinates of third atom
C     xyz4(3)       cartesian coordinates of fourth atom
C
C
C  Output
C
C     phi           Angle between atoms 1-2-3-4.  Range returned is
C                   -pi .lt. phi .le. pi, following IUPAC
C                   sign and range convention.
C
C     dphidx(12)    Jacobean: dphi/dx(1), dphi/dy(1), ...
C
C     d2phid2(45)   Second derivative, d2phi/dx(i)dx(2), a symmetric
C                   matrix, stored lower triangle by row
C
C  Linkage          List of subroutines called by this one
C
C  Programmer       William Swope (some from Carnevali)
C
C  Date             June 1994
C
C  Update log       Record of all updates including names of programmers,
C                     descriptions of changes made, and dates of change.
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
c
      Implicit Real*8 (a, b, d-h, o, p, r-y)
      Implicit Integer (i-n)
c     Implicit Logical (z)
c     Violation of ARCHEM convention; but no logical data here
      Implicit Real*8 (z)
c     Implicit Character (c)
c     Violation of ARCHEM convention; but no character data here
      Implicit Real*8 (c)
c     Implicit Undefined (q)
 
      character cproduct*48
      dimension cproduct(3)
 
c     Input and output arrays
      real*8 xyz1(3),xyz2(3),xyz3(3),xyz4(3)
      real*8 dphidx(12),d2phidx2(78)
 
 
C     THE SIGN CONVENTION FOR PHI IS AS FOLLOWS:
C     NUMBER THE ASSOCIATED ATOMS 1, 2, 3, 4.  ATOMS 1 AND 4 ARE
C     "TERMINAL" ATOMS AND THE DIHEDRAL ANGLE IS ABOUT THE BOND
C     BETWEEN ATOMS 2 AND 3.  IMAGINE HOLDING THE PLANE FORMED BY
C     ATOMS 1, 2, AND 3 FIXED AND ROTATING THE PLANE FORMED BY
C     ATOMS 2, 3, AND 4 ABOUT A VECTOR THAT POINTS FROM ATOM 2
C     TO ATOM 3.  THE POSITIVE SENSE CORRESPONDS TO THE RIGHT-HAND
C     RULE FOR ROTATION ABOUT THE VECTOR FROM ATOM 2 TO ATOM 3.
c     This agrees with the IUPAC convention.
C
C     FURTHERMORE, PHI=0 CORRESPONDS TO A CONDITION WHERE ALL FOUR
C     ATOMS ARE ON A PLANE AND ATOMS 1 AND 4 ARE "CIS" TO THE BOND
C     BETWEEN ATOMS 2 AND 3.  LIKEWISE, PHI=180 DEGREES CORRESPONDS
C     A CONDITION WHERE ALL FOUR ATOMS ARE ON A PLANE AND ATOMS 1 AND
C     4 ARE "TRANS" TO THE BOND BETWEEN ATOMS 2 AND 3.
 
 
c     Local and scratch arrays
 
      dimension a(3),b(3),c(3),u(3),v(3),acrossc(3)
 
c     First derivatives of u**2, v**2 with respect to a, b, and c
      dimension du2da(3),du2db(3),dv2db(3),dv2dc(3)
 
c     First derivatives of D=u**2*v**2 and F=D**(-1/2) with
c     respect to a, b, and c
      dimension ddda(3),dddb(3),dddc(3)
      dimension dfda(3),dfdb(3),dfdc(3)
 
c     First derivatives of uv with respect to a, b, and c
      dimension duvda(3),duvdb(3),duvdc(3)
 
c     Derivatives of S with respect to a, b, and c
      dimension dsda(3),dsdb(3),dsdc(3)
 
c     Derivatives of cosphi and sinphi with respect to a, b, and c
      dimension dcosphida(3),dcosphidb(3),dcosphidc(3)
      dimension dsinphida(3),dsinphidb(3),dsinphidc(3)
 
c     Derivatives of phi with respect to a, b, and c
      dimension dphida(3),dphidb(3),dphidc(3)
 
c     Second derivatives
      dimension du2dada(3,3),du2dbdb(3,3),du2dadb(3,3)
      dimension dv2dbdb(3,3),dv2dbdc(3,3),dv2dcdc(3,3)
      dimension dddada(3,3),dddbdb(3,3),dddcdc(3,3)
      dimension dddadb(3,3),dddadc(3,3),dddbdc(3,3)
      dimension dfdada(3,3),dfdbdb(3,3),dfdcdc(3,3)
      dimension dfdadb(3,3),dfdadc(3,3),dfdbdc(3,3)
      dimension              duvdbdb(3,3)
      dimension dsdadb(3,3),dsdadc(3,3),dsdbdc(3,3)
      dimension              dsdbdb(3,3)
      dimension duvdadb(3,3),duvdadc(3,3),duvdbdc(3,3)
      dimension dcosphidada(3,3),dcosphidbdb(3,3),dcosphidcdc(3,3)
      dimension dcosphidadb(3,3),dcosphidadc(3,3),dcosphidbdc(3,3)
      dimension dsinphidada(3,3),dsinphidbdb(3,3),dsinphidcdc(3,3)
      dimension dsinphidadb(3,3),dsinphidadc(3,3),dsinphidbdc(3,3)
      dimension dphidada(3,3),dphidbdb(3,3),dphidcdc(3,3)
      dimension dphidadb(3,3),dphidadc(3,3),dphidbdc(3,3)
 
      dimension atilde(3,3),btilde(3,3),ctilde(3,3)
 
c     identity tensor here to simplify some loops
      real*8 delta(3,3)/1.d0,3*0.d0,1.d0,3*0.d0,1.d0/
 
      data cproduct /'$RCSfile$',
     $               '$Revision$$Date$',
     $               '$IBMCSA: PRODUCT VERSION NUMBER $'/
 
c     Statement function gives index of (i,j) in lower triang by row
      lowtri(i,j) = j + i*(i-1)/2
 
      if(iop.ne.1 .and. iop.ne.2 .and. iop.ne.3) then
        print *, ' Incorrect input to dihint subroutine: iop = ',iop
c       call arcdie()
      endif
 
      ierror=0
 
      if(iop.ge.1) then
c     compute angle
 
c     Compute a=r2-r1, b=r3-r2, c=r4-r3
      do 10 k=1,3
      a(k)=xyz2(k)-xyz1(k)
      b(k)=xyz3(k)-xyz2(k)
10    c(k)=xyz4(k)-xyz3(k)
 
c     Compute u = a x b , v = b x c, vectors normal to 123, 234 planes
      u(1)=a(2)*b(3)-a(3)*b(2)
      u(2)=a(3)*b(1)-a(1)*b(3)
      u(3)=a(1)*b(2)-a(2)*b(1)
      v(1)=b(2)*c(3)-b(3)*c(2)
      v(2)=b(3)*c(1)-b(1)*c(3)
      v(3)=b(1)*c(2)-b(2)*c(1)
 
c     Compute lengths and scalar products
      b2=b(1)**2+b(2)**2+b(3)**2
      u2=u(1)**2+u(2)**2+u(3)**2
      v2=v(1)**2+v(2)**2+v(3)**2
      if( b2.eq.0.d0 )then
c       this should be a hard error; print out message and die
c       if this happens there is something seriously wrong
        print *, ' Error:  Can not compute torsion angle when central',
     >           ' bond has zero length.'
c       call arcdie('dihint')
      endif
      b1=sqrt(b2)
      u1=sqrt(u2)
      v1=sqrt(v2)
      d=u2*v2
      d1=sqrt(d)
      uv=u(1)*v(1)+u(2)*v(2)+u(3)*v(3)
      av=a(1)*v(1)+a(2)*v(2)+a(3)*v(3)
      s=b1*av
      if( u2.eq.0.d0 .or. v2.eq.0.d0 )then
        print *, ' Error:  Three collinear atoms make torsion angle',
     >           ' undefined.'
        print *, ' Zero returned for angle, gradient and hessian.'
        ierror=1
        f=0.d0
        d=1.d0
        d1=1.d0
c       Compute cosphi and sinphi
        cosphi=1.d0
        sinphi=0.d0
      else
        f=1.d0/d1
c       Compute cosphi and sinphi
        cosphi=uv*f
        sinphi=s*f
      endif
 
c     Compute phi itself, produce range of -pi.lt.phi.le.pi
      phi=atan2(sinphi,cosphi)
 
      endif
 
      if(iop.ge.2) then
c     compute dphidx
 
c     Compute derivatives of u**2 and v**2 with respect to a, b, and c.
      a2=a(1)**2+a(2)**2+a(3)**2
      c2=c(1)**2+c(2)**2+c(3)**2
      ab=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
      bc=b(1)*c(1)+b(2)*c(2)+b(3)*c(3)
      ac=a(1)*c(1)+a(2)*c(2)+a(3)*c(3)
      do 20 k=1,3
      du2da(k)=2.d0*(b2*a(k)-ab*b(k))
      du2db(k)=2.d0*(a2*b(k)-ab*a(k))
      dv2db(k)=2.d0*(c2*b(k)-bc*c(k))
20    dv2dc(k)=2.d0*(b2*c(k)-bc*b(k))
 
c     Compute derivatives of D=u**2*v**2 with respect to a, b, and c.
      do 35 k=1,3
      ddda(k)=v2*du2da(k)
      dddb(k)=v2*du2db(k)+u2*dv2db(k)
35    dddc(k)=u2*dv2dc(k)
 
c     Compute derivatives of F=D**(-1/2) with respect to a, b, and c.
      do 40 k=1,3
      dfda(k)=-0.5d0*ddda(k)/(d*d1)
      dfdb(k)=-0.5d0*dddb(k)/(d*d1)
40    dfdc(k)=-0.5d0*dddc(k)/(d*d1)
 
 
 
      if(abs(sinphi).gt.abs(cosphi))then
c     if(.false.)then
c*************************************************
c     CASE A - use expressions derived from cosphi
c     Compute derivatives of u*v=C with respect to a, b, and c.
      do 50 k=1,3
      duvda(k)=b(k)*bc-c(k)*b2
      duvdb(k)=a(k)*bc+c(k)*ab-2.d0*b(k)*ac
50    duvdc(k)=b(k)*ab-a(k)*b2
c     Compute derivatives of cosphi=(u*v)*F=C*F with respect to a, b, and c.
      do 60 k=1,3
      dcosphida(k)=duvda(k)*f+uv*dfda(k)
      dcosphidb(k)=duvdb(k)*f+uv*dfdb(k)
60    dcosphidc(k)=duvdc(k)*f+uv*dfdc(k)
c     Compute derivatives of phi with respect to a, b, and c.
      do 70 k=1,3
      dphida(k)=-dcosphida(k)/sinphi
      dphidb(k)=-dcosphidb(k)/sinphi
70    dphidc(k)=-dcosphidc(k)/sinphi
c*************************************************
      else
c*************************************************
c     CASE B - use expressions derived from sinphi
      acrossc(1)=a(2)*c(3)-a(3)*c(2)
      acrossc(2)=a(3)*c(1)-a(1)*c(3)
      acrossc(3)=a(1)*c(2)-a(2)*c(1)
      s=b1*av
c     Compute derivatives of S=b(a*v) with respect to a, b, and c.
      do 85 k=1,3
      dsda(k)=b1*v(k)
      dsdb(k)=b(k)*av/b1-b1*acrossc(k)
85    dsdc(k)=b1*u(k)
c     Compute derivatives of sinphi with respect to a, b, and c.
      do 90 k=1,3
      dsinphida(k)=dsda(k)*f+s*dfda(k)
      dsinphidb(k)=dsdb(k)*f+s*dfdb(k)
90    dsinphidc(k)=dsdc(k)*f+s*dfdc(k)
c     Compute derivatives of phi with respect to a, b, and c.
      do 95 k=1,3
      dphida(k)=dsinphida(k)/cosphi
      dphidb(k)=dsinphidb(k)/cosphi
95    dphidc(k)=dsinphidc(k)/cosphi
c*************************************************
 
      endif
 
c     Add contribution to dphi/dx array
      do 80 k=1,3
      dphidx(k  )=          -dphida(k)
      dphidx(3+k)= dphida(k)-dphidb(k)
      dphidx(6+k)= dphidb(k)-dphidc(k)
80    dphidx(9+k)= dphidc(k)
 
c     This code is for three atoms collinear; torsion undefined
      if(ierror.eq.1) then
        do 81 k=1,12
81      dphidx(k)=0.d0
      endif
 
      endif
 
      if(iop.ge.3) then
 
c     Compute second derivatives of u**2 and v**2 with respect to a, b, and c.
      do 110 i=1,3
      do 110 j=1,3
      du2dada(i,j)=2.d0*(b2*delta(i,j)-b(i)*b(j))
      du2dbdb(i,j)=2.d0*(a2*delta(i,j)-a(i)*a(j))
      du2dadb(i,j)=2.d0*(2.d0*a(i)*b(j)-b(i)*a(j)-ab*delta(i,j))
      dv2dbdb(i,j)=2.d0*(c2*delta(i,j)-c(i)*c(j))
      dv2dcdc(i,j)=2.d0*(b2*delta(i,j)-b(i)*b(j))
      dv2dbdc(i,j)=2.d0*(2.d0*b(i)*c(j)-c(i)*b(j)-bc*delta(i,j))
110   continue
 
c     Compute second derivatives of D=u**2*v**2 with respect to a, b, and c.
      do 120 i=1,3
      do 120 j=1,3
      dddada(i,j)=
     >            v2*du2dada(i,j)
      dddbdb(i,j)=dv2db(i)*du2db(j)+du2db(i)*dv2db(j)
     >           +v2*du2dbdb(i,j)  +u2*dv2dbdb(i,j)
      dddcdc(i,j)=
     >                             +u2*dv2dcdc(i,j)
      dddadb(i,j)=                 +du2da(i)*dv2db(j)
     >           +v2*du2dadb(i,j)
      dddadc(i,j)=                 +du2da(i)*dv2dc(j)
     >
      dddbdc(i,j)=                 +du2db(i)*dv2dc(j)
     >                             +u2*dv2dbdc(i,j)
120   continue
 
c     Compute second derivatives of F=D**(-1/2) with respect to a, b, and c.
      f3=f/d
      do 130 i=1,3
      do 130 j=1,3
      dfdada(i,j)=.5d0*f3*(1.5d0*ddda(i)*ddda(j)/d-dddada(i,j))
      dfdbdb(i,j)=.5d0*f3*(1.5d0*dddb(i)*dddb(j)/d-dddbdb(i,j))
      dfdcdc(i,j)=.5d0*f3*(1.5d0*dddc(i)*dddc(j)/d-dddcdc(i,j))
      dfdadb(i,j)=.5d0*f3*(1.5d0*ddda(i)*dddb(j)/d-dddadb(i,j))
      dfdadc(i,j)=.5d0*f3*(1.5d0*ddda(i)*dddc(j)/d-dddadc(i,j))
      dfdbdc(i,j)=.5d0*f3*(1.5d0*dddb(i)*dddc(j)/d-dddbdc(i,j))
130   continue
 
      if(abs(sinphi).gt.abs(cosphi))then
c     if(.false.)then
c*************************************************
c     CASE A - use expressions derived from cosphi
c     Compute second derivatives of u*v=C with respect to a, b, and c.
      do 140 i=1,3
      do 140 j=1,3
      duvdbdb(i,j)=a(i)*c(j)+c(i)*a(j)-2.d0*ac*delta(i,j)
      duvdadb(i,j)=bc*delta(i,j)+b(i)*c(j)-2.d0*c(i)*b(j)
      duvdadc(i,j)=b(i)*b(j)-b2*delta(i,j)
      duvdbdc(i,j)=ab*delta(i,j)+a(i)*b(j)-2.d0*b(i)*a(j)
140   continue
c     Compute second derivatives of cosphi=(u*v)*F=C*F with respect to a, b, and
c.
      do 150 i=1,3
      do 150 j=1,3
      dcosphidada(i,j)=
     >                +dfda(i)*duvda(j)+duvda(i)*dfda(j)+uv*dfdada(i,j)
      dcosphidbdb(i,j)=
     >  duvdbdb(i,j)*f+dfdb(i)*duvdb(j)+duvdb(i)*dfdb(j)+uv*dfdbdb(i,j)
      dcosphidcdc(i,j)=
     >                +dfdc(i)*duvdc(j)+duvdc(i)*dfdc(j)+uv*dfdcdc(i,j)
      dcosphidadb(i,j)=
     >  duvdadb(i,j)*f+dfda(i)*duvdb(j)+duvda(i)*dfdb(j)+uv*dfdadb(i,j)
      dcosphidadc(i,j)=
     >  duvdadc(i,j)*f+dfda(i)*duvdc(j)+duvda(i)*dfdc(j)+uv*dfdadc(i,j)
      dcosphidbdc(i,j)=
     >  duvdbdc(i,j)*f+dfdb(i)*duvdc(j)+duvdb(i)*dfdc(j)+uv*dfdbdc(i,j)
150   continue
c     Compute second derivatives of phi with respect to a, b, and c.
      z1=-cosphi/(sinphi**3)
      z2=-1.d0/sinphi
      do 160 i=1,3
      do 160 j=1,3
      dphidada(i,j)=z1*dcosphida(i)*dcosphida(j)+z2*dcosphidada(i,j)
      dphidbdb(i,j)=z1*dcosphidb(i)*dcosphidb(j)+z2*dcosphidbdb(i,j)
      dphidcdc(i,j)=z1*dcosphidc(i)*dcosphidc(j)+z2*dcosphidcdc(i,j)
      dphidadb(i,j)=z1*dcosphida(i)*dcosphidb(j)+z2*dcosphidadb(i,j)
c     note dcosphidadc is non zero but dphidadc is zero - don't compute
      dphidadc(i,j)=z1*dcosphida(i)*dcosphidc(j)+z2*dcosphidadc(i,j)
      dphidbdc(i,j)=z1*dcosphidb(i)*dcosphidc(j)+z2*dcosphidbdc(i,j)
160   continue
      else
c*************************************************
c     CASE B - use expressions derived from sinphi
c     Compute the product of the antisymmetric tensor times a, b, and c
      call antsym(a,atilde)
      call antsym(b,btilde)
      call antsym(c,ctilde)
c     Compute second derivatives of S=b(a*v) with respect to a, b, and c.
      b3=b1*b2
      do 210 i=1,3
      do 210 j=1,3
      dsdbdb(i,j)=(delta(i,j)/b1-b(i)*b(j)/b3)*av
     >            -(acrossc(i)*b(j)+acrossc(j)*b(i))/b1
      dsdadb(i,j)=v(i)*b(j)/b1+b1*ctilde(i,j)
      dsdadc(i,j)=-b1*btilde(i,j)
      dsdbdc(i,j)=b(i)*u(j)/b1+b1*atilde(i,j)
210   continue
c     Compute second derivatives of sinphi with respect to a, b, and c.
      do 220 i=1,3
      do 220 j=1,3
      dsinphidada(i,j)=
     >                +dfda(i)*dsda(j)+dsda(i)*dfda(j)+s*dfdada(i,j)
      dsinphidbdb(i,j)=
     >  dsdbdb(i,j)*f+dfdb(i)*dsdb(j)+dsdb(i)*dfdb(j)+s*dfdbdb(i,j)
      dsinphidcdc(i,j)=
     >                +dfdc(i)*dsdc(j)+dsdc(i)*dfdc(j)+s*dfdcdc(i,j)
      dsinphidadb(i,j)=
     >  dsdadb(i,j)*f+dfda(i)*dsdb(j)+dsda(i)*dfdb(j)+s*dfdadb(i,j)
      dsinphidadc(i,j)=
     >  dsdadc(i,j)*f+dfda(i)*dsdc(j)+dsda(i)*dfdc(j)+s*dfdadc(i,j)
      dsinphidbdc(i,j)=
     >  dsdbdc(i,j)*f+dfdb(i)*dsdc(j)+dsdb(i)*dfdc(j)+s*dfdbdc(i,j)
220   continue
c     Compute second derivatives of phi with respect to a, b, and c.
      z1=sinphi/cosphi**3
      z2=1.d0/cosphi
      do 230 i=1,3
      do 230 j=1,3
      dphidada(i,j)=z1*dsinphida(i)*dsinphida(j)+z2*dsinphidada(i,j)
      dphidbdb(i,j)=z1*dsinphidb(i)*dsinphidb(j)+z2*dsinphidbdb(i,j)
      dphidcdc(i,j)=z1*dsinphidc(i)*dsinphidc(j)+z2*dsinphidcdc(i,j)
      dphidadb(i,j)=z1*dsinphida(i)*dsinphidb(j)+z2*dsinphidadb(i,j)
c     note dsinphidadc is non zero but dphidadc is zero - don't compute
      dphidadc(i,j)=z1*dsinphida(i)*dsinphidc(j)+z2*dsinphidadc(i,j)
      dphidbdc(i,j)=z1*dsinphidb(i)*dsinphidc(j)+z2*dsinphidbdc(i,j)
230   continue
      end if
 
c     Add contributions to second derivative matrix
c     Derivative with respect to r1,r1:
      do 21 i=1,3
      do 21 j=1,i
      index=lowtri(i,j)
      d2phidx2(index)=dphidada(i,j)
21    continue
c     Derivative with respect to r2,r2:
      do 22 i=1,3
      do 22 j=1,i
      index=lowtri(i+3,j+3)
      d2phidx2(index)=dphidada(i,j)+dphidbdb(i,j)
     >               -dphidadb(i,j)-dphidadb(j,i)
22    continue
c     Derivative with respect to r3,r3:
      do 23 i=1,3
      do 23 j=1,i
      index=lowtri(i+6,j+6)
      d2phidx2(index)=dphidbdb(i,j)+dphidcdc(i,j)
     >               -dphidbdc(i,j)-dphidbdc(j,i)
23    continue
c     Derivative with respect to r4,r4:
      do 24 i=1,3
      do 24 j=1,i
      index=lowtri(i+9,j+9)
      d2phidx2(index)=dphidcdc(i,j)
24    continue
c     Derivative with respect to r1,r2:
      do 25 i=1,3
      do 25 j=1,3
      index=lowtri(j+3,i)
      d2phidx2(index)=-dphidada(i,j)+dphidadb(i,j)
25    continue
c     Derivative with respect to r1,r3:
      do 26 i=1,3
      do 26 j=1,3
      index=lowtri(j+6,i)
      d2phidx2(index)=-dphidadb(i,j)+dphidadc(i,j)
26    continue
c     Derivative with respect to r1,r4:
      do 27 i=1,3
      do 27 j=1,3
      index=lowtri(j+9,i)
      d2phidx2(index)=-dphidadc(i,j)
c
c     note: dphidadc is always zero - this array and its computation
c     should be removed later
c
c      write(*,887) i,j,index,dphidadc(i,j)
c887   format(' r1,r4 block: i,j=',2i2,' index=',i4,' dphidadc=',f10.4)
27    continue
c     Derivative with respect to r2,r3:
      do 28 i=1,3
      do 28 j=1,3
      index=lowtri(j+6,i+3)
      d2phidx2(index)=
     >               +dphidadb(i,j)-dphidadc(i,j)
     >               -dphidbdb(i,j)+dphidbdc(i,j)
28    continue
c     Derivative with respect to r2,r4:
      do 29 i=1,3
      do 29 j=1,3
      index=lowtri(j+9,i+3)
      d2phidx2(index)=dphidadc(i,j)-dphidbdc(i,j)
29    continue
c     Derivative with respect to r3,r4:
      do 30 i=1,3
      do 30 j=1,3
      index=lowtri(j+9,i+6)
      d2phidx2(index)=dphidbdc(i,j)-dphidcdc(i,j)
30    continue
 
c     code for three atoms colinear
      if(ierror.eq.1) then
        do 45 i=1,78
45      d2phidx2(i)=0.d0
      endif
 
      endif
 
 
      end
c
c     Subroutine to compute the product of the antysimmetric
c     tensor times a vector. The result is an antisymmetric tensor.
      subroutine antsym(x,xtilde)
      implicit real*8 (a-h,o-z)
      dimension x(3),xtilde(3,3)
      xtilde(1,1)=0.d0
      xtilde(2,2)=0.d0
      xtilde(3,3)=0.d0
      xtilde(1,2)=x(3)
      xtilde(2,3)=x(1)
      xtilde(3,1)=x(2)
      xtilde(2,1)=-x(3)
      xtilde(3,2)=-x(1)
      xtilde(1,3)=-x(2)
      return
      end








c
c     subroutine to write out solvent information files for lammps simulation
c
      subroutine wrtlmps1(nwat,coord,xlow,xhigh)
      implicit real*8 (a-h,o-z)
      dimension coord(3,*)
      dimension fmass(2),vdw(2,2),bondpar(2,2),anglepar(2,2),charge(2)


      oxmass=15.9994d0
      hymass=1.008d0
      fmass(1)=oxmass
      fmass(2)=hymass
      vdw(1,1)=0.16275d0  !epsilon o-o
      vdw(2,1)=3.16435d0  !sigma   o-o
      vdw(1,2)=0.0d0      !epsilon h-h
      vdw(2,2)=0.0d0      !sigma   h-h
      bondpar(1,1)=250.d0 !spring constant (doesn't matter)
      bondpar(2,1)=0.9572d0 !OH bond length (constrained by shake)
      anglepar(1,1)=250.d0 !spring constant (doesn't matter)
      anglepar(2,1)=104.52d0 !HOH angle (constrained)
      charge(1)=-1.0484d0  !O charge TI4P-Ew
      charge(2)= 0.5242d0  !H charge TI4P-Ew
      
c     TIP4P-Ew
      nsites=3*nwat 
      nbonds=2*nwat
      nangles=nwat
      ndihed=0
      nadded=0


      write(15,'('' Header line '')')
      write(15,*)
      write(15,'(5x,i10,'' atoms '')') nsites
      write(15,'(5x,i10,'' bonds '')') nbonds
      write(15,'(5x,i10,'' angles '')') nangles
      write(15,'(5x,i10,'' dihedrals '')') ndihed + nadded

c     TIP4P-Ew
      nattp=2
      nbntp=1
      nantp=1
      nditp=0

      write(15,*)
      write(15,'(5x,i10,'' atom types '')') nattp
      write(15,'(5x,i10,'' bond types '')') nbntp
      write(15,'(5x,i10,'' angle types '')') nantp
      write(15,'(5x,i10,'' dihedral types '')') nditp


c     cell edge is xlow to xhigh
      write(15,*)
      write(15,'(5x,2f15.8,'' xlo xhi '')') xlow,xhigh
      write(15,'(5x,2f15.8,'' ylo yhi '')') xlow,xhigh
      write(15,'(5x,2f15.8,'' zlo zhi '')') xlow,xhigh

c     TIP4P-Ew
      write(15,*)
      write(15,'('' Masses  '')')
      write(15,*)
      write(15,'(i10,f15.8,''    # O - TIP4P-Ew'')') 1,fmass(1) 
      write(15,'(i10,f15.8,''    # H - TIP4P-Ew'')') 2,fmass(2) 


c     TIP4P-Ew
      write(15,*)
      write(15,'('' Pair Coeffs'')')
      write(15,*)
      write(15,'(i10,4f15.8,''   # O - TIP4P-Ew'')') 1,
     x    vdw(1,1),vdw(2,1), !epsilon,sigma
     x    vdw(1,1),vdw(2,1)  !epsilon,sigma
      write(15,'(i10,4f15.8,''   # H - TIP4P-Ew'')') 2,
     x    vdw(1,2),vdw(2,2), !epsilon,sigma
     x    vdw(1,2),vdw(2,2)  !epsilon,sigma


      write(15,*)
      write(15,'('' Bond Coeffs'')')
      write(15,*)
      write(15,'(i10,2f15.8,
     x  ''    # OH bond TIP4P-Ew'')') 1,
     x  bondpar(1,1),bondpar(2,1)


      write(15,*)
      write(15,'('' Angle Coeffs'')')
      write(15,*)
      write(15,'(i10,2f15.8,
     x  ''    # HOH angle TIP4P-Ew'')') 1,
     x  anglepar(1,1),anglepar(2,1)


c     write(15,*)
c     write(15,'('' Dihedral Coeffs'')')
c     write(15,*)


      write(15,*)
      write(15,'('' Atoms '')')
      write(15,*)
      do i=1,nwat
c     fix up the coordinates by removing the fictional sites
c     this assumes the sites are ordered O,H,H,Q
        iat=4*(i-1)
        write(15,'(i10,i10,i5,f10.5,3f15.8,''    # O '')') 
     x    3*(i-1)+1,i,1,charge(1),(coord(k,iat+1),k=1,3)
        write(15,'(i10,i10,i5,f10.5,3f15.8,''    # H1'')') 
     x    3*(i-1)+2,i,2,charge(2),(coord(k,iat+2),k=1,3)
        write(15,'(i10,i10,i5,f10.5,3f15.8,''    # H2'')') 
     x    3*(i-1)+3,i,2,charge(2),(coord(k,iat+3),k=1,3)
      enddo


      write(15,*)
      write(15,'('' Velocities '')')
      write(15,*)
      do i=1,nsites
        write(15,'(i10,5x,3f10.5)') i,0.,0.,0.
      enddo


      write(15,*)
      write(15,'('' Bonds  '')')
      write(15,*)
      do i=1,nbonds,2
        iwat=1+(i-1)/2
        ij1=3*(iwat-1)+1
        write(15,'(i10,3i10)') i,  1,ij1,ij1+1
        write(15,'(i10,3i10)') i+1,1,ij1,ij1+2
      enddo


      write(15,*)
      write(15,'('' Angles '')')
      write(15,*)
      do i=1,nangles
        iwat=i
        iat=3*(iwat-1)+2
        jat=3*(iwat-1)+1
        kat=3*(iwat-1)+3
        write(15,'(i10,4i10)') i,1,iat,jat,kat
      enddo


c     write(15,*)
c     write(15,'('' Dihedrals '')')
c     write(15,*)
c     do i=1,ndihed +nadded
c       write(15,'(i10,5i10)') i,ijkldih(6,i),
c    x    ijkldih(1,i),ijkldih(2,i),ijkldih(3,i),ijkldih(4,i)
c     enddo

      write(15,*)

      close(15)


      end


c
c     left justify a string
c
      subroutine leftjust(string)
      character*(*) string

      is=0
      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 10
        endif
      enddo

10    continue
      if(is.eq.0) return  !all blank string

      do i=1,len(string)-is+1
        string(i:i)=string(is+i-1:is+i-1)
      enddo
c     blank out the last is-1 characters
      do i=len(string)-is+2,len(string)
        string(i:i)=' '
      enddo

      end


c
c     subroutine lstchr returns the index of the last nonblank character in a string
c
      subroutine lstchr(string,ilst)
      character*(*) string 

      ilst=0  !if string is all blank, return zero
      do i=len(string),1,-1
        if(string(i:i).ne.' ') then
          ilst=i
          go to 10
        endif
      enddo
10    continue

      end



c
c     subroutine to parse a string into an element field and up to 3 integers
c     unused fields will be returned with values of -1
c
      subroutine parser(string,elfield,lelfield,i1,i2,i3)
      character*(*) string,elfield
      character*120 line
      dimension is(3)
      do i=1,3
        is(i)=-1
      enddo
      line=string
      call leftjust(line)
      ne=2 !assume two character element name
      if(line(2:2).eq.' '.or.line(2:2).eq.'_') ne=1
      lelfield=ne
      elfield=line(1:ne)

      do i=1,3
        if(line(ne+1:ne+1).eq.'_') then
c         assume there is a number field after the underscore
c         look for next space or underscore
          l=index(line(ne+2:),'_')
          if(l.gt.0) then
            read(line(ne+2:ne+l),*) is(i)
            ne=ne+l
          else
            read(line(ne+2:),*) is(i)
            go to 10
          endif
        endif
      enddo

10    continue

      i1=is(1)
      i2=is(2)
      i3=is(3)

      end

c
c     subroutine to upper case a string
c
      subroutine ucase(string)
      character*26 upper,lower
      character*(*) string

      upper='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
      lower='abcdefghijklmnopqrstuvwxyz'
      do i=1,len(string)
        is=index(lower,string(i:i))
        if(is.ne.0) string(i:i)=upper(is:is)
      enddo

      end

c
c     function to get atomic number given a string
c     holding the element name
c


      integer function igetatnum(string)
      character*(*) string
      character*26 upper,lower
      character*80 elements
      character*2 ename
      
      upper='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
      lower='abcdefghijklmnopqrstuvwxyz'
      elements(1:20)= 'H HELIBEB C N O F NE' 
      elements(21:40)='NAMGALSIP S CLARK CA'
      elements(41:60)='SCTIV CRMNFECONICUZN'
      elements(61:80)='GAGEASSEBRKRRBSRY ZR'

      igetatnum=o

      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 100
        endif
      enddo
100   continue
      ie=len(string)
      do i=is+1,len(string)
        if(string(i:i).eq.'_'.or.string(i:i).eq.' ') then
          ie=i-1
          go to 200
        endif
      enddo
200   continue

      ename(1:2)=string(is:ie)  !pads with blanks
c     write(*,'('' ENAME =*'',a2,''*'')') ename

c     element string is in string(is:ie)
      do i=1,2  
        ix=index(lower,ename(i:i))
        if(ix.ne.0) ename(i:i)=upper(ix:ix)
      enddo
c     write(*,'('' ENAME =*'',a2,''*'')') ename

      ix=index(elements,ename(1:2))
      if(ix.eq.0) then
        write(*,'('' Element not found: '',a2)') ename
        stop 'ERROR:  missing element in igetatnum'
      endif
    
      igetatnum=1+(ix-1)/2

      end
c
c     subroutine to populate the atomic masses array
c
      subroutine amassinit(amass,n)
      implicit real*8 (a-h,o-z)
      dimension amass(n)

c     atomic masses as needed
      do i=1,120
        amass(i)=0.d0
      enddo

      amass(1)=1.00794d0   !agrees w/Mulliken
      amass(2)=4.002602d0
      amass(3)=6.941d0
      amass(4)=9.012182d0
      amass(5)=10.811d0

      amass(6)=12.0107d0
      amass(6)=12.0110d0    !agrees w/Mulliken
      amass(7)=14.0067d0
      amass(8)=15.9994d0    !agrees w/Mulliken
      amass(9)=18.9984032d0
      amass(10)=20.1797d0

      amass(11)=22.98976928d0
      amass(12)=24.305d0
      amass(13)=26.9815386d0
      amass(14)=28.0855d0
      amass(15)=30.973762d0

      amass(16)=32.065d0
      amass(17)=35.453d0
      amass(18)=39.948d0
      amass(19)=39.0983d0
      amass(20)=40.078d0

      amass(21)=44.955912d0
      amass(22)=47.867d0
      amass(23)=50.9415d0
      amass(24)=51.9961d0
      amass(25)=54.938045d0

      amass(26)=55.845d0
      amass(27)=58.933195d0
      amass(28)=58.6934d0
      amass(29)=63.546d0
      amass(30)=65.38d0

      amass(31)=69.723d0
      amass(32)=72.64d0
      amass(33)=74.9216d0
      amass(34)=78.96d0
      amass(35)=79.904d0
      amass(36)=83.798d0

      end 



C
C     MACHINE INDEPENDENT (ALMOST) RANDOM NUMBER GENERATOR
C     SLOPPILY CONVERTED TO DOUBLE PRECISION.
C     LOW 32 BITS OF FLOATING POINT RANDOM NUMBER ARE ZERO.
C     I.E., THEY ARE ONLY SINGLE PRECISION RANDOM ASSIGNED TO DOUBLES.
C
      DOUBLE PRECISION FUNCTION RAN(IX)
      INTEGER*4 IX,I,I1,I2,I3,I4,I5,I6,I7,I8,I9
      DATA I2/16807/,I4/32768/,I5/65536/,I3/2147483647/
      I6=IX/I5
      I7=(IX-I6*I5)*I2
      I8=I7/I5
      I9=I6*I2+I8
      I1=I9/I4
      IX=(((I7-I8*I5)-I3)+(I9-I1*I4)*I5)+I1
      IF(IX.LT.0) IX=IX+I3
      RAN=(4.656613E-10)*FLOAT(IX)
      RETURN
      END

