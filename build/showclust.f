c
c     This is a program to read the x.gbcls file output by getbond.f and map the clusters 
c     back to a psf file so that they can be visualized in VMD. 
c     Numbering convention follows psf.
c
c     The program reads an input file  *.scin  
c     This holds:
c       the psf file name:  *.psf
c       the gbcls file name:  *.gbcls
c

      implicit real*8 (a-h,o-z)
      character*256 line,fnpsf,fngbcls,fnbonds,argument
      character*4 name(400000)   !dimension to natmx
      dimension ilist(10000)
      dimension ibndseq(400000,4)   !dimension to ibseqmx
      dimension ibnd(2,400000),iactive(400000,2)   !dimension ibnd to ibmax; dimension iactive to natmx
      dimension iclsize(20000)  !for each cluster, this gives its size

      dimension molblock(50,2)  !for up to 50 blocks of molecules, the type and number in the block



      ibseqmx=100000
      natmx=400000
      ibmax=400000
      iclmax=20000

      call getarg(1,argument)
      is=index(argument,' ')-1
c     write (*,'(''argument is *'',a,''*'')') argument(1:is)  
      if(argument(is-4:is).ne.'.scin') then
        write(*,'(''Error:  program is called with *.scin '',
     x ''filename on command line'')')
        stop 'error: want *.scin filename on command line'
      endif

      open(9,file=argument(1:is))
      read(9,'(a)') fnpsf
      read(9,'(a)') fngbcls
      close(9) 
      
      open(9,file=fngbcls)  
      read(9,*) ntypes
      if (ntypes.ne.2) stop 'ntypes ne 2'

      write(*,'(''Number of types of molecules '',i5)') ntypes
c     read(9,*) ntype1, nattp1, nrestp1, masstp1
      read(9,*) nattp1, nrestp1, masstp1
c     write(*,'(''Number of molecules of type 1          '', i5,
c    x  '' (ntype1)'')') ntype1
      write(*,'(''Number of sites in molecules of type 1 '', i5,
     x  '' (nattp1)'')') nattp1
      write(*,'(''Number of residues in molecules type 1 '', i5,
     x  '' (nrestp1)'')') nrestp1
      write(*,'(''Mass of molecules of type 1            '', i5,
     x  '' (masstp1)'')') masstp1

c     read(9,*) ntype2, nattp2, nrestp2, masstp2
      read(9,*) nattp2, nrestp2, masstp2
c     write(*,'(''Number of molecules of type 2          '', i5,
c    x  '' (ntype2)'')') ntype2
      write(*,'(''Number of sites in molecules of type 2 '', i5,
     x  '' (nattp2)'')') nattp2
      write(*,'(''Number of residues in molecules type 2 '', i5,
     x  '' (nrestp2)'')') nrestp2
      write(*,'(''Mass of molecules of type 2            '', i5,
     x  '' (masstp2)'')') masstp2

      read(9,'(a)') fnbonds
      isave=0
      do i=lstchr(fnbonds),1,-1
        if(isave.eq.0.and.fnbonds(i:i).eq.' ') then
          isave=i  
        endif
      enddo

      read(fnbonds(isave:lstchr(fnbonds)),*) lsttime
      write(*,'(''lsttime is '', i10)') lsttime

      read(9,*) nblocks,(molblock(i,1),molblock(i,2),i=1,nblocks)
      write(*,'(''Number of blocks of contiguous molecule types '',
     x  i5)') nblocks

      ntype1=0
      ntype2=0
      do i=1,nblocks
        if(molblock(i,1).eq.1)  ntype1=ntype1+molblock(i,2)
        if(molblock(i,1).eq.2)  ntype2=ntype2+molblock(i,2)
        write(*,'(''Block '',i2,'':  '',i6,'' molecules of type '',
     x    i2)') i,molblock(i,2),molblock(i,1)
      enddo

      write(*,'(''Number of molecules of type 1          '', i5,
     x  '' (ntype1)'')') ntype1
      write(*,'(''Number of molecules of type 2          '', i5,
     x  '' (ntype2)'')') ntype2


      nat=ntype1*nattp1+ntype2*nattp2
      if(nat.gt.natmx) then
        write (11,'(''too many atoms, check natmx'')')
        stop 'too many atoms'
      endif 


c process the getbond cluster information from *.gbcls

      do i=1,nat
        name(i)=' '
      enddo
      do i=1,iclmax
        iclsize(i)=0
      enddo

      ifbnd=0  !flag to indicate if bond list is included in the gbcls file
200   continue
      read(9,'(a120)',end=300) line
      if(line(1:10).eq.'          ') then
        read(9,'(a120)',end=300) line
        if(index(line,'New bonds').ne.0) then
          is=index(line,'New bonds')+9
          read(line(is:),*) ibseq
          if(ibseq.gt.ibseqmx) stop 'not enough storage in ibndseq'
          ifbnd=1
          write(*,'('' Reading bond information from'',
     x      '' gbcls file.  New bonds '',i8)') ibseq
          do i=1,ibseq
            read(9,*) (ibndseq(i,k),k=1,4)
          enddo

        else 
c         here to read list of active sites
          is=index(line,'Activated sites ')+15
          read(line(is:),*) nactive
          if(nactive.gt.natmx) stop 'not enough storage in active list'
          write(*,'('' Reading active site information from'',
     x      '' gbcls file.  Active sites '',i8)') nactive
          do i=1,nactive
            read(9,*) (iactive(i,k),k=1,2)  !atom number, cluster number
          enddo

        endif


      else
        is=index(line,'size =')+6
        read(line(is:),*) isize     
        is=index(line,'number')+6
        read(line(is:),*) iclus    
        read(9,'(10i6)') (ilist(i),i=1,isize)
        write(*,'(''cluster '',i4,'' size '',i6)') iclus, isize
c       write(*,'(''molecular members: '',10i8)') 
c    x    (ilist(i),i=1,isize)
 
        if(iclus.gt.iclmax) then
          write(*,'(''Too many clusters, increase iclmax '')')
          stop 'Too many clusters, increase iclmax'
        endif
        iclsize(iclus)=isize

c       if (isize.gt.5) then    
          do j=1,isize
            m=ilist(j)  !molecule index
            call atnum(m,nattp1,nattp2,molblock,nblocks,m1,m2,mtype)
c           write(*,'(''Cluster '',i4,'' Molecule '',i6,'' Type '',i1,
c    x        '' first/last atoms '',2i6)')
c    x        iclus,m,mtype,m1,m2 
c           if (m.le.ntype1) then
c             m1=1+(m-1)*nattp1 !index of first atom of this molecule
c             m2=m1+nattp1-1    !index of last atom of this molecule
c           else
c             m1=1+(m-1-ntype1)*nattp2+ntype1*nattp1
c             m2=m1+nattp2-1 
c           endif
            do k=m1,m2
c             label by index (largest to smallest)
              if (mtype.eq.1) write(name(k)(1:4),'(''L'',i3.3)') iclus
              if (mtype.eq.2) write(name(k)(1:4),'(''A'',i3.3)') iclus
c             label by size 
              if (mtype.eq.1) write(name(k)(1:4),'(''L'',i3.3)') isize
              if (mtype.eq.2) write(name(k)(1:4),'(''A'',i3.3)') isize
            enddo
          enddo
c       endif  !end of if(isize) block
      endif                            
       
      do i=1,nactive
        iat=iactive(i,1)  !atom number
c       tag the atom with either the cluster number or the cluster size
        write(name(iat)(1:4),'(''X'',i3.3)') iactive(i,2) !cluster number
        isize=iclsize(iactive(i,2))
        write(name(iat)(1:4),'(''X'',i3.3)') isize        !cluster size  
      enddo




      go to 200
300   continue
      close(9)

      



c
c     read the psf file
c


      open(10,file=fnpsf)
      is=index(fnpsf,'.psf')
      istart=0
      do i=is,1,-1
       if (istart.eq.0.and.fnpsf(i:i).eq.'/') then
        istart=i+1
       endif
      enddo
      if(istart.eq.0) istart=1
      write(*,'(''*'',a,''*'')') 
     x fnpsf(istart:is-1)//'.'//fnbonds(isave+1:lstchr(fnbonds))//'.psf'
c     new psf file with new names applied
      open(11,file=
     x fnpsf(istart:is-1)//'.'
     x //fnbonds(isave+1:lstchr(fnbonds))//'.psf')
10    continue
      read(10,'(a120)') line
      if(index(line,'!NATOM').ne.0) go to 50 
      write(11,'(a120)') line
      go to 10
50    continue
      read(line,*) natom
      write(11,'(a120)') line
      do i=1,natom
        read(10,'(a120)') line
c       name(i)=line(20:23)
        im=molnum(i,nattp1,nattp2,molblock,nblocks)
        call atnum(im,nattp1,nattp2,molblock,nblocks,m1,m2,mtype)
c       if(ires.le.ntype1-1) then
c         if (name(i).eq.'    ') name(i)='GELC'
c          name(i)='GELC'
c       else
c         if (name(i).eq.'    ') name(i)='ARM0'
c          name(i)='ARM0'
c       endif
        if(mtype.eq.1.and.name(i).eq.'    ') name(i)='GELC'
        if(mtype.eq.2.and.name(i).eq.'    ') name(i)='ARM0'
        write(line(15:18),'(i4)') im-1
        is=lstchr(line)
        write(11,'(a)') line(1:19)//name(i)//line(24:is) 
      enddo 
      

c
c      write out the new bonds
c
500    continue
       read(10,'(a120)',end=600) line
        if (index(line,'!NBONDS').ne.0) then
         read(line,*) nbond
        if (nbond+ibseq.gt.ibmax) stop 'ibnd not big enough'
         read(10,'(8i8)') ((ibnd(i,j),i=1,2),j=1,nbond)
         write(11,'(i8,'' !NBONDS '')') nbond+ibseq
       do j=1,ibseq
        ibnd(1,j+nbond)= ibndseq(j,1)
        ibnd(2,j+nbond)= ibndseq(j,2)
       enddo  
         write(11,'(8i8)') ((ibnd(i,j),i=1,2),j=1,nbond+ibseq)
       go to 500
        endif
       write(11,'(a120)') line

      go to 500
600    continue   

      
      close(10)
      close(11)
      end 
        
c
c     given an atom number find the molecule index
c
c     function molnum(i,ntype1,nattp1,ntype2,nattp2)
      function molnum(i,nattp1,nattp2,molblock,nblocks)
      dimension molblock(50,2)

      nat=0
      nmo=0
      do j=1,nblocks
        if(molblock(j,1).eq.1) then
c         block j is of type 1
          if(i.le.nat+nattp1*molblock(j,2)) then
            molnum=nmo+1+(i-1-nat)/nattp1
            return     
          endif
          nat=nat+nattp1*molblock(j,2)
          nmo=nmo+molblock(j,2)
        elseif(molblock(j,1).eq.2) then
c         block j is of type 2
          if(i.le.nat+nattp2*molblock(j,2)) then
            molnum=nmo+1+(i-1-nat)/nattp2
            return     
          endif
          nat=nat+nattp2*molblock(j,2)
          nmo=nmo+molblock(j,2)
        endif 
      enddo

      write(*,'(''Error in function molnum: '')')
      write(*,'(''i= '',i10)') i
      return
      end



c
c     given a molecule number find the first and last atom indices
c
      subroutine atnum(i,nattp1,nattp2,molblock,nblocks,m1,m2,mtype)
      dimension molblock(50,2)

      nat=0
      nmo=0
      do j=1,nblocks
        if(molblock(j,1).eq.1) then
          if(i.le.nmo+molblock(j,2)) then
            m1=nat+(i-nmo-1)*nattp1+1
            m2=m1+nattp1-1                           
            mtype=1
            return
          endif
          nat=nat+nattp1*molblock(j,2)
        elseif(molblock(j,1).eq.2) then
          if(i.le.nmo+molblock(j,2)) then
            m1=nat+(i-nmo-1)*nattp2+1
            m2=m1+nattp2-1                           
            mtype=2
            return
          endif
          nat=nat+nattp2*molblock(j,2)
        endif
        nmo=nmo+molblock(j,2)
      enddo


      write(*,'(''Error in function atnum: '')')
      write(*,'(''i= '',i10)') i
      return
      end




c
c     integer function to return the location of the last nonblank character from a string
c
      function lstchr(string)
      character*(*) string
      iloc=0
      do i=len(string),1,-1
        if(iloc.eq.0.and.string(i:i).ne.' ') iloc=i
      enddo
      lstchr=iloc
      end
