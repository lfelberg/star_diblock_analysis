c
c     program to read two lmp files and merge them into
c     a single one, effectively solvating the first
c     also, remove solvent molecules that are too close
c
c     this program derived from the merge.f program
c
c     changes:  need to include repeat unit info in the Masses block
c     this allows compatibility with the mixture program that
c     allows insertion of molecules into a system
c
c
  
      implicit real*8 (a-h,o-z)
      character*120 fname1,fname2,fname3,fname4,line,string
      logical ifexist

      dimension coord(3,7 500 000),charge(7 500 000),itype(7 500 000)
      character*20 atname(7 500 000)
      dimension ijbond(3,7 500 000),bndpar(2,5000)
      dimension ijkangle(4,2 500 000),angpar(2,5000)
      dimension ijkldih(5,7 500 000),dihpar(4,5000)
      dimension vdw(4,100),fmass(100)
    

      dimension coord2(3,7 500 000),charge2(7 500 000),itype2(7 500 000)
      character*20 atname2(7 500 000)
      dimension ijbond2(3,7 500 000),bndpar2(2,5000)
      dimension ijkangle2(4,2 500 000),angpar2(2,5000)
      dimension ijkldih2(5,7 500 000),dihpar2(4,5000)
      dimension vdw2(4,100),fmass2(100)

      dimension closest(2,7 500 000)
      dimension iorder(7 500 000)   !redirection arrays
      dimension imolecule(7 500 000) !molecule type for each solvent site

      character*100 nametypes(100)  !a name for each type from the *.lmp file
      character*100 nametypes2(100)  !a name for each type from the *.lmp file

      dimension ijres(5,10000)  !array to hold residue connectivity; construct from bond list
                               !needed for charmm psf file
      dimension amass(120)
      dimension ipsfbond(2,4)

      dimension molecule(10,7)  !information about each molecule type in the solvent
                                !molecule(*,1) = number sites in this molecule type
                                !molecule(*,2) = number molecules in this molecule type
                                !molecule(*,3) = first site index of molecules of this type
                                !molecule(*,4) = first molecule index of molecules of this type
                                !molecule(*,5) = number molecules removed of this type         
                                !molecule(*,7) = (integer) mass of molecules of this type      


      natmx=7 500 000 !size of site-based arrays
      nbnmx=7 500 000  !size of bond list arrays
      nbtmx=5000   !size of bond type arrays
      nanmx=2 500 000  !size of angle list arrays
      nantmx=5000  !size of angle type arrays
      ndimx=7 500 000   !size of dihedral list array
      nditmx=5000  !size of the dihedral type array
      nattpmx=100

c     open the input file (*.solin) extract name for output file *.sout
      call getarg(1,string)
      if(index(string,'.solin').eq.0) then
        write(*,'(''Error: need to have file *.solin '',
     x    ''on command line'')')
        stop 'Error: need to name *.solin on command line'
      endif
      call leftjust(string)
      inquire(file=string,exist=ifexist)
      if(.not.ifexist) then
        write(*,'(''Error: input file does not exist" '',a)')
     x    string(1:index(string,' ')-1)
      endif



    
c     fname1='staradam16pladme.lmp'     !STAR2
c     fname1='staradam16pedme.lmp'      !STAR3
c     fname1='staradam16pdvldme.lmp'    !STAR4
c     fname1='starpent12mpa16pladme.lmp'!MPA  
      fname1='staradam16pdvlpoxa.lmp'   !STAR5
c     fname1='staribup.lmp'             !STAR7
c     fname1='staradam16pdvlpc1.lmp'    !STAR8
c     fname1='staradam16pcdme.lmp'        !STAR10
c     fname1='staradam16pcpc1.lmp'        !STAR11
c     fname1='starpent12mpa16pdvldme.lmp' !STAR12
c     fname1='starpent12mpa16pcdme.lmp'   !STAR13

      fname2='../analysis/tip4p_ew_46K.lmp'
      fname2='tip4p_ew_216K.lmp'

c     fname3='STAR2_500K.4.xyz'   !STAR2
c     fname3='star3_setup03.xyz'  !STAR3
c     fname3='star4_setup03.xyz'  !STAR4
c     fname3='mpa_setup02.xyz'    !STAR5 (mpa)
c     fname3='config_oplsaa_80.xyz'  !STAR5 (mpa - expanded)
c     fname3='config_oplsaa_150.xyz' !STAR5 (mpa - compact)
c     fname3='config_plaff2_90.xyz'  !STAR5 (mpa - expanded)
c     fname3='config_plaff2_160.xyz' !STAR5 (mpa - compact)
c     fname3='config90.xyz'       !STAR5 (mpa - compact)
c     fname3='last_staribup2.xyz' !STAR4
c     fname3='last_20_setup.3.c.xyz'       !STAR7
c     fname3='staradam16pdvldme_setup.3.xyz.frame7' !STAR8
c     fname3='setup.2.30.xyz' !STAR9
c     fname3='setup.2.36.xyz' !STAR10
c     fname3='setup.2.35.xyz' !STAR11
c     fname3='setup.4.31.xyz' !STAR12
c     fname3='setup.2.26.xyz' !STAR13

      ifreplace=1   ! 1 means to use solute coordinates from xyz file (fname3)
      iframe=1
      ifreplace=0   ! 0 means to use solute coordinates from lmp file (fname1)

c     fname4='star2wat2.lmp'      !STAR2
c     fname4='star3wat2.lmp'      !STAR3
c     fname4='star4wat2.lmp'      !STAR4
c     fname4='star5wat_oplsaa_80.lmp'      !MPA  
c     fname4='star5wat_oplsaa_150.lmp'     !MPA  
c     fname4='star5wat_plaff2_90.lmp'      !MPA  
c     fname4='star5wat_plaff2_160.lmp'     !MPA  
c     fname4='star6wat.lmp'     !PDVL-POXA
c     fname4='star7_20_wat.lmp'      !STAR7
c     fname4='star11wat.lmp'       !STAR11
c     fname4='star12wat.lmp'       !STAR12
c     fname4='star13wat.lmp'       !STAR13
      fname4='star6armcheck.lmp'   !PDVL-POXA

      istopearly=1 !just read in solute info and stop
      istopearly=2 !read in solute and solvent info and stop
      istopearly=3 !don't stop; finish                         

      open(6,file=string(1:index(string,'.solin'))//'solout')
      write(6,'(/,''Program to solvate a molecule'',
     x  '' in a solvent box '')')
      write(6,'(''Input file name : '',a)') 
     x  string(1:index(string,' ')-1)
      

      ifreplace=0   !take solute coords from lmp file  (fname1)
      open(11,file=string(1:index(string,' ')-1))
      fname1=' '
      fname2=' '
      fname3=' '
      ixyzset=-1
      read(11,*)  iop1,iop2,iop3
                   !op1: 1 = take solute coords from lmp file   (on line 2)
                   !op1: 2 = take solute coords from xyz file   (on line 3)
                   !op2: 1 = just read and report on solute
                   !op2: 2 = just read and report on solute and solvent
                   !op2: 3 = read solute and solvent and produce output lmp file
                   !op3: 1 = take solvent coords from lmp file   (not implemented)
                   !op3: 2 = take solvent coords from xyz file   (not implemented)
      read(11,'(a120)') fname1   !solute *.lmp file name  (line 2)
      read(11,'(a120)') fname3   !solute *.xyz file name for alternate solute coordinates  
      if(iop1.eq.1) then 
        write(6,'(''Control option 1:  '',i2,
     x    '' (solute coordinates from lmp file)'')') iop1 
        write(6,'(''Solute lmp file:   '',a)') 
     x    fname1(1:index(fname1,' ')-1)
        fname3=' '
      elseif(iop1.eq.2) then
        ifreplace=1  !take solute coords from xyz file (fname3)
        write(6,'(''Control option 1:  '',i2,
     x    '' (solute coordinates from xyz file)'')') iop1 
        call leftjust(fname3)
        read(fname3(index(fname3,' '):),*) iframe 
        write(6,'(''Solute lmp file:   '',a)') 
     x    fname1(1:index(fname1,' ')-1)
        write(6,'(''xyz file name:     '',a)') 
     x    fname3(1:index(fname3,' ')-1)
        write(6,'(''xyz coord index:   '',i5)') iframe 
      endif

      read(11,'(a120)') fname2   !solvent file name  (line 4)
      read(11,*) ntype  !number of molecule types in the solvent
      write(6,'(''Number of molecule types in the solvent is '',
     x  i5)') ntype
      do i=1,ntype
        read(11,*) molecule(i,1),molecule(i,2)  !read number of sites/molecule, no. molecules
        write(6,'(''For molecule type '',i2,'' number of sites is '',
     x    i4,'' and there are '',i6,'' molecules of this type.'')')
     x    i,molecule(i,1),molecule(i,2)
      enddo
c     generate data structure giving the molecule type of each site in the solvent
      jat=0
      do i=1,ntype
        do j=1,molecule(i,1)*molecule(i,2)
          jat=jat+1
          imolecule(jat)=i
        enddo
      enddo
      molecule(1,3)=1    
      molecule(1,4)=1
      do i=2,ntype  
        molecule(i,3)=molecule(i-1,3)
     x    +molecule(i-1,1)*molecule(i-1,2)   !site index of first site of this molecule type
        molecule(i,4)=molecule(i-1,4)
     x    +molecule(i-1,2)        !molecule index of first molecule of this type
      enddo



      read(11,'(a120)') fname4   !output lmp file name (line 7)
      if(iop2.eq.1) then
        write(6,'(''Control option 2:  '',i2,
     x    '' (just read and report on solute)'')') iop2 
        istopearly=1
        fname2=' ' 
        fname4=' '
      elseif(iop2.eq.2) then
        istopearly=2
        write(6,'(''Control option 2:  '',i2,
     x    '' (just read and report on solute and solvent)'')') iop2 
        write(6,'(''Solvent lmp file:  '',a)') 
     x    fname2(1:index(fname2,' ')-1)
        fname4=' '
      elseif(iop2.eq.3) then
        istopearly=3
        write(6,'(''Control option 2:  '',i2,
     x    '' (read and report on solute and solvent,'',
     x    '' then produce new lmp file)'')') iop2 
        write(6,'(''Solvent lmp file:  '',a)') 
     x    fname2(1:index(fname2,' ')-1)
        write(6,'(''Produce output lmp file:   '',a)')  
     x    fname4(1:index(fname4,' '))
      endif
      close(11)


c     solute parameters
      call readlmp(fname1,nsites,itype,charge,coord,atname,natmx,
     x                    nbonds,nbtp,bndpar,ijbond,nbnmx,nbtmx,
     x                    nangle,natp,angpar,ijkangle,nanmx,nantmx,
     x                    ndih,ndtp,dihpar,ijkldih,ndimx,nditmx,
     x                    natomtp,vdw,nattpmx,
     x                    xlo,xhi,ylo,yhi,zlo,zhi,fmass,
     x                    nametypes)

c
c     get coordinates from an xyz file to replace those in the lmp file
c     (note that lmp files often contain coordinates "as built"
c
      if(ifreplace.eq.1) then
        call getcoord(fname3(1:index(fname3,' ')),nsites,
     x    itype,coord,iframe)
      endif

c
c     find range and size of solute molecule, its center of geometry
c
      xcent=0.d0
      ycent=0.d0
      zcent=0.d0
      xsmin=coord(1,1)
      xsmax=coord(1,1)
      ysmin=coord(2,1)
      ysmax=coord(2,1)
      zsmin=coord(3,1)
      zsmax=coord(3,1)
      do i=1,nsites
        xsmin=min(xsmin,coord(1,i))
        xsmax=max(xsmax,coord(1,i))
        ysmin=min(ysmin,coord(2,i))
        ysmax=max(ysmax,coord(2,i))
        zsmin=min(zsmin,coord(3,i))
        zsmax=max(zsmax,coord(3,i))
        xcent=xcent+coord(1,i)
        ycent=ycent+coord(2,i)
        zcent=zcent+coord(3,i)
      enddo
      xcent=xcent/nsites
      ycent=ycent/nsites
      zcent=zcent/nsites
  
c
      write(*,'(/,'' Solute characteristics '')')

      if1=index(fname1,' ')-1
      if3=index(fname3,' ')-1
      write(*,'('' Parameters file for solute '',a)') fname1(1:if1)
      if(ifreplace.eq.0) then
        write(*,'('' Coordinate file for solute '',a)') fname1(1:if1)
      else
        write(*,'('' Coordinate file for solute '',a)') fname3(1:if3)
      endif
      write(*,'('' Solute box x-range:        '',2f15.8)') xlo,xhi
      write(*,'('' Solute box y-range:        '',2f15.8)') ylo,yhi
      write(*,'('' Solute box z-range:        '',2f15.8)') zlo,zhi

      write(*,'('' Solute molecule x-range:  '',2f15.8,
     x  '' range '',f10.5,/,
     x          '' Solute molecule y-range:  '',2f15.8,
     x  '' range '',f10.5,/,
     x          '' Solute molecule z-range:  '',2f15.8,
     x  '' range '',f10.5)') 
     x  xsmin,xsmax,(xsmax-xsmin),
     x  ysmin,ysmax,(ysmax-ysmin),
     x  zsmin,zsmax,(zsmax-zsmin)  
      write(*,'('' Solute center of geometry: ('',
     x  f10.4,'','',f10.4,'','',f10.4,'')'')')
     x  xcent,ycent,zcent

      totmas=0.d0
      do i=1,nsites
        totmas=totmas+fmass(itype(i))
c       write(*,'('' Solute site '',i5,'' mass = '',f10.5)')
c    x     i,fmass(itype(i))
      enddo
      write(*,'('' Total MW of solute molecule '',f10.4)') totmas

c     generalize to solvents other than water     
      nwremv=totmas/18.d0
      write(*,'('' Number of water molecules of this weight = '',i5)')
     x  nwremv

      if(istopearly.eq.1) stop 'Stop after reading solute information'


c     solvent parameters
      call readlmp(fname2,nsites2,itype2,charge2,coord2,atname2,natmx,
     x                    nbonds2,nbtp2,bndpar2,ijbond2,nbnmx,nbtmx,
     x                    nangle2,natp2,angpar2,ijkangle2,nanmx,nantmx,
     x                    ndih2,ndtp2,dihpar2,ijkldih2,ndimx,nditmx,
     x                    natomtp2,vdw2,nattpmx,
     x                    xlo2,xhi2,ylo2,yhi2,zlo2,zhi2,fmass2,
     x                    nametypes2)


c     get coordinates from an xyz file to replace those in the lmp file
c     (note that lmp files often contain coordinates "as built"
c NOTE need a new file name for these; also the frame number and a volume
c     if(iop3.eq.2) then
c       call getcoord(fname3(1:index(fname3,' ')),nsites2,
c    x    itype2,coord2,iframesolv)
c     endif


      write(*,'(/,'' Solvent characteristics '')')
      if2=index(fname2,' ')-1
      write(*,'('' Parameters/coords for solvent'',a)') fname2(1:if2)
      write(*,'('' Solvent box x-range: (LMP) '',2f15.8)') xlo2,xhi2
      write(*,'('' Solvent box y-range: (LMP) '',2f15.8)') ylo2,yhi2
      write(*,'('' Solvent box z-range: (LMP) '',2f15.8)') zlo2,zhi2

c
c     adjust the solvent so that the box range goes from zero to L
c
      xhi2=xhi2-xlo2
      yhi2=yhi2-ylo2
      zhi2=zhi2-zlo2
      xlo2=0.d0
      ylo2=0.d0
      zlo2=0.d0
c     translate all particles to fit in this box
      do i=1,nsites2
        x=coord2(1,i)
        y=coord2(2,i)
        z=coord2(3,i)
        if(x.gt.0.d0) then
          dx=xhi2*int(x/xhi2)
        else
          dx=xhi2*(-1+int(x/xhi2))
        endif
        coord2(1,i)=x-dx
        if(y.gt.0.d0) then
          dy=yhi2*int(y/yhi2)
        else
          dy=yhi2*(-1+int(y/yhi2))
        endif
        coord2(2,i)=y-dy
        if(z.gt.0.d0) then
          dz=zhi2*int(z/zhi2)
        else
          dx=zhi2*(-1+int(z/zhi2))
        endif
        coord2(3,i)=z-dz
      enddo

c
c     shift sites so that they are the nearest image to the 
c     first site of each molecule
c
      do it=1,ntype   !loop over number of types in the solvent
        do imol=1,molecule(it,2)  !loop over number of molecules of this type
          iat=molecule(it,3)+(imol-1)*molecule(it,1)  !first site this molecule
          x=coord2(1,iat)
          y=coord2(2,iat)
          z=coord2(3,iat)
          do jat=iat+1,iat+molecule(it,1)-1
            dx=coord2(1,jat)-x
            dy=coord2(2,jat)-y
            dz=coord2(3,jat)-z
            if(dx.lt.-0.5d0*xhi2) then
              coord2(1,jat)=coord2(1,jat)+xhi2
            elseif(dx.gt. 0.5d0*xhi2) then
              coord2(1,jat)=coord2(1,jat)-xhi2
            endif
            if(dy.lt.-0.5d0*yhi2) then
              coord2(2,jat)=coord2(2,jat)+yhi2
            elseif(dy.gt. 0.5d0*yhi2) then
              coord2(2,jat)=coord2(2,jat)-yhi2
            endif
            if(dz.lt.-0.5d0*zhi2) then
              coord2(3,jat)=coord2(3,jat)+zhi2
            elseif(dz.gt. 0.5d0*zhi2) then
              coord2(3,jat)=coord2(3,jat)-zhi2
            endif
          enddo
        enddo
      enddo




      if(istopearly.eq.2) stop 'Stop after reading solvent information'
c
c     assume translation of center of molecule to center of solvent box and compute
c     closest distances of each water to a solute site
c
      write(*,'('' Computing nearest solute site to'',
     x   '' each solvent site'')')
      xlen=xhi2-xlo2
      ylen=yhi2-ylo2
      zlen=zhi2-zlo2
      xshft=0.5d0*(xhi2+xlo2) - xcent
      yshft=0.5d0*(yhi2+ylo2) - ycent
      zshft=0.5d0*(zhi2+zlo2) - zcent
c     (xshft,yshft,zshft) points from center of solute to center of solvent box
      do i=1,nsites2
        iclose=0
        dclose=xlen
        do j=1,nsites
          dx=coord2(1,i)-(coord(1,j)+xshft)
          dy=coord2(2,i)-(coord(2,j)+yshft)
          dz=coord2(3,i)-(coord(3,j)+zshft)
          if(dx.gt. 0.5d0*xlen) dx=dx-xlen
          if(dx.lt.-0.5d0*xlen) dx=dx+xlen
          if(dy.gt. 0.5d0*ylen) dy=dy-ylen
          if(dy.lt.-0.5d0*ylen) dy=dy+ylen
          if(dz.gt. 0.5d0*zlen) dz=dz-zlen
          if(dz.lt.-0.5d0*zlen) dz=dz+zlen
          dist=dsqrt(dx**2+dy**2+dz**2)
          if(dist.lt.dclose) then
            dclose=dist
            iclose=j
          endif
        enddo
c       closest solute site to solvent i is solute site j at distance dclose
        closest(1,i)=i
        closest(2,i)=dclose
c       if(i.le.200) write(*,'('' Closest solute site to solvent '',
c    x    ''site number '',i6,'' is solute site '',i6,'' at '',f10.4)') 
c    x    i,iclose,dclose
      enddo


c  here here


      write(*,'('' Computing distance to nearest solute site '',
     x   ''for each solvent molecule.'')')
c     reduce the list so it lists the closest solute site 
c     to any site of the solvent molecule
      jat=0
      jmol=0
      do i=1,ntype
        do imol=1,molecule(i,2)  !loop over the number of molecules of this type
          jmol=jmol+1
          jat=jat+1
          dclose=closest(2,jat)
          do iatm=2,molecule(i,1)  !loop over the number of sites in this molecule type
            jat=jat+1
            dclose=min(dclose,closest(2,jat))
          enddo
          closest(1,jmol)=jmol
          closest(2,jmol)=dclose  !save distance of closest solvent site
c         write(*,'(''Closest solute site to solvent molecule '',
c    x      i6,'' is at distance '',f10.3)') jmol,dclose
        enddo
      enddo


c     isolmol=3 !number of sites in a solvent molecule
c     do i=1,nsites2/isolmol
c       dclose=closest(2,1+isolmol*(i-1))
c       do j=2,isolmol
c         dclose=min(dclose,closest(2,j+isolmol*(i-1))) 
c       enddo
c       closest(1,i)=i
c       closest(2,i)=dclose
c       if(i.lt.200) write(*,'('' Closest solute to solvent molecule '',
c    x    i6,'' is site at '',f10.4)') i,dclose
c     enddo





c
c     compute mass of each molecule, sum up mass of closest ones 
c     until the mass of the solute (totmas) is reached
c
      nmoltot=0   !total number of solvent molecules of all types
      do i=1,ntype
        molecule(i,5)=0  !initialize counter for number of molecules removed
        nmoltot=nmoltot+molecule(i,2)
        xmass=0.d0
        write(*,'(''For type '',i1,'' num mol = '',i6,
     x    '' nsites = '',i4)')
     z    i,molecule(i,2),molecule(i,1)
        do iat=1,molecule(i,1)
          it=itype2(molecule(i,3)+iat-1)
          write(*,'(''For site '',i2,'' type = '',i2,
     x      '' mass = '',f10.3)')
     z      iat,it,fmass2(it)
          xmass=xmass+fmass2(it)
        enddo
        molecule(i,7)=xmass+0.5d0
        write(*,'(''Mass of solvent molecules of type '',i2,'' is '',
     x    f10.2,'' saved as integer '',i8,''.'')') i,xmass,
     x    molecule(i,7)
      enddo



c
c     bubble sort to get an ordered list
c     NOTE that we don't need to do a full bubble sort and that doing so
c     takes way too much time and has become unacceptable for large 
c     solvent cell.  We only need to find the closest N molecules
c     where the sum of their mass has hit the mass of the solute molecule.
c
      iremovemode=1  !remove solvent until the mass equals that of the solute
c     iremovemode=2  !remove solvent until the closest one is at least threshdist
      threshdist=1.4d0  ! solvent must be at least this far away
c
c
      write(*,'('' Sorting solvent molecule list by distance from'',
     x   '' solute molecule.'')')
      write(*,'(''Number of solvent molecules is '',i8)') jmol

      solvmass=0.d0  !for accumulating the mass of the close solvents
      do i=1,jmol-1
        do j=i+1,jmol                 
          if(closest(2,j).le.closest(2,i)) then
            xc=closest(2,i)
            closest(2,i)=closest(2,j)
            closest(2,j)=xc
            ic=closest(1,i)
            closest(1,i)=closest(1,j)
            closest(1,j)=ic
          endif
        enddo
c       find molecule type for this molecule?
        imol=closest(1,i)
        mtp=0
        do j=1,ntype
          if(imol.ge.molecule(j,4).and.
     x       imol.le.molecule(j,4)+molecule(j,2)-1)
     x      mtp=j
        enddo
c       write(*,'(i4,'':  next closest solvent molecule to '',
c    x    ''the solute molecule '',
c    x    '' is '',i6,'' at distance of '',f10.5,
c    x    '' of type '',i2)')
c    x    i,imol,closest(2,i),mtp
        solvmass=solvmass+molecule(mtp,7)
        if(iremovemode.eq.1) then
          if(solvmass.ge.totmas) then
            nwremv=i
            go to 210
          endif
        else 
          if(closest(2,i).gt.threshdist) then
            nwremv=i
            go to 210
          endif 
        endif
      enddo


c     loop over closest until mass of solute is reached
c     msolvent=0
c     do j=1,jmol
c       imol=closest(1,j)
c       jt=0
c       do i=1,ntype
c         if(imol.ge.molecule(i,4).and.
c    x       imol.le.molecule(i,4)+molecule(i,2)-1)
c    x      jt=i
c       enddo
c       msolvent=msolvent+molecule(jt,7)
c       if(msolvent.ge.totmas) then
c         nwremv=j
c         go to 210
c       endif
c     enddo


210   continue  



c
      if(iremovemode.eq.1) then
        write(*,'(/,''Solvent removal mode is 1; '',
     x    ''Solvent removed equal to mass of solute.'')')
      else
        write(*,'(''Solvent removal mode is 2; '',
     x    ''Solvent removed if closer than '',f10.4,''.'')')
     x    threshdist
      endif
      write(*,'(''Number of solvent molecules to'',
     x    '' be removed is '',i5,/)') nwremv

      do i=nwremv-5,nwremv
        write(*,'('' Solvent molecule '',i6,
     x    '' at d = '', f10.4,'' (removed)'')')
     x    int(closest(1,i)),closest(2,i)
      enddo

c     the following distances are not properly sorted with the new code
c     do i=nwremv+1,nwremv+5
c       write(*,'('' Solvent molecule '',i6,
c    x    '' at d = '', f10.4,'' (kept)'')')
c    x    int(closest(1,i)),closest(2,i)
c     enddo




c
c     there are better ways to do this, but for now do it the dumb way
c     assume we are going to discard the closest nwremv molecules and keep the rest
c     we want to retain the same sequence ordering we had before, just renumber
c     so we keep the first nwremv entries of closest array as they are
c     but reorder the remainder based on the molecule index in closest(1,*)
c     [Better way to do this is to just pull up the closest nwremv molecules out
c     of the list in that many passes through the data instead of two bubble sorts
c     each of which is order n**2.]
c
c     write(*,'('' Removing '',i6,
c    x  '' solvent molecules out of '',i6)') nwremv,nmoltot
c     write(*,'('' Sorting remaining '',i6,
c    x  '' solvent molecule list by index'')') nmoltot-nwremv
c     do i=1+nwremv,nmoltot-1
c       do j=i+1,nmoltot
c         if(closest(1,j).le.closest(1,i)) then
c           xc=closest(2,i)
c           closest(2,i)=closest(2,j)
c           closest(2,j)=xc
c           ic=closest(1,i)
c           closest(1,i)=closest(1,j)
c           closest(1,j)=ic
c         endif
c       enddo
c     enddo
c
c     construct reorder array - given old site index, we need the new one
c     this needed to reassign data in bond, angle, dihedral list
c

      do i=1,nsites2
        iorder(i)=1  !initialize (1 means this site will be kept)
      enddo


      do i=1,nwremv
        imol=closest(1,i)  !molecule index to be removed
        jt=0
        do j=1,ntype 
          if(imol.ge.molecule(j,4).and.
     x       imol.le.molecule(j,4)+molecule(j,2)-1)
     x      jt=j
        enddo
c       write(*,'(''Removed molecule number '',i5,
c    x    '' is molecule number '',i6,'' of type '',i1)')
c    x    i,imol,jt
        molecule(jt,5)=molecule(jt,5)+1
        do j=1,molecule(jt,1)
          isite=molecule(jt,3)-1+
     x       molecule(jt,1)*(imol-molecule(jt,4))+j
c         write(*,'(''Clearing iorder ('',i6,'')'')')
c    x       isite
          iorder(isite)=0
        enddo
      enddo
c
c     at this stage, iorder has for each solvent site 1/0 for if site is present
c     now make this into a redirection array, giving the new location for that
c     index after solvent is removed
c
      jnext=0
      do i=1,ntype
        do j=1,molecule(i,2)  !loop over the molecules of type i
          ifirst=molecule(i,3)+(j-1)*molecule(i,1)
          do k=1,molecule(i,1) !loop over the sites of molecule j
            isite=ifirst-1+k
            if(iorder(isite).ne.0) then
              jnext=jnext+1
              iorder(isite)=jnext
            endif
          enddo
        enddo
      enddo




c     do i=1,nmoltot-nwremv !loop over remaining MOLECULES
c       imol=closest(1,i+nwremv) !take the next molecule
c       jt=0
c       do i=1,ntype
c         if(imol.ge.molecule(i,4).and.
c    x       imol.le.molecule(i,4)+molecule(i,2)-1)
c    x      jt=i
c       enddo
c       do j=1,molecule(jt,1)
c         jat=molecule(jt,3)-1+j+(imol-molecule(jt,3))*molecule(jt,1)
c         iorder(isolmol*(imol-1)+j)=isolmol*(i-1)+j
c       enddo
c     enddo
      
c     do i=1,300
c       write(*,'('' Reorder array iorder('',i6,'') = '',i6)') 
c    x    i,iorder(i)
c     enddo
c





c

c
c     option 1 to remove all molecules within some distance of any solute molecule
c     option 2 to remove molecules until the mass density is what it would be if
c     all the material were water; for this compute the number of water molecules
c     to equal the mass of the solute
c

      write(*,'(/,'' Writing merged LAMMPS file '')')
      open(15,file=fname4)


      nsolvnt0=0   !solvent molecules at start
      nsites0=0    !solvent sites at start
      nsolvnt1=0   !solvent molecules left after removal
      nsites1=0    !solvent sites left after removal
      do i=1,ntype
        write(*,'(''Solvent molecule type '',i2,
     x    '' began with '',i6,'' molecules '',
     x    ''('',i7,'' sites) '',
     x    ''removed '',i5,'' molecules '',
     x    ''('',i7,'' sites) '',
     x    ''leaving '',i6,'' molecules '',
     x    ''('',i7,'' sites).'')')
     x    i,molecule(i,2),molecule(i,2)*molecule(i,1),
     x    molecule(i,5),molecule(i,5)*molecule(i,1),
     x    (molecule(i,2)-molecule(i,5)),
     x    (molecule(i,2)-molecule(i,5))*molecule(i,1)

          nsolvnt0=nsolvnt0+molecule(i,2)
          nsolvnt1=nsolvnt1+(molecule(i,2)-molecule(i,5))
          nsites0=nsites0+molecule(i,1)*molecule(i,2)
          nsites1=nsites1+
     x      molecule(i,1)*(molecule(i,2)-molecule(i,5))
      enddo
      write(*,'(/,''Totals:       '',2x,
     x  '' began with '',i6,'' molecules '',
     x  ''('',i7,'' sites) '',
     x  ''removed '',i5,'' molecules '',
     x  ''('',i7,'' sites) '',
     x  ''leaving '',i6,'' molecules '',
     x  ''('',i7,'' sites).'')')
     x  nsolvnt0,nsites0,
     x  (nsolvnt0-nsolvnt1),(nsites0-nsites1),
     x  nsolvnt1,nsites1

c     nsolvnt0=nsites2/isolmol
c     nsolvnt1=nsolvnt0 - nwremv
      write(*,'(/,'' Orig number of solvent molecules :'',i7,
     x  '' (= '',i7,'' sites)'')') 
     x  nsolvnt0,nsites0 
      write(*,'('' Solvent molecules removed        :'',i7,
     x  '' (= '',i7,'' sites)'')') 
     x  nwremv,nsites0-nsites1
      write(*,'('' New number of solvent molecules  :'',i7, 
     x  '' (= '',i7,'' sites)'')') 
     x  nsolvnt1,nsites1              
c     write(*,'('' New number of solvent sites      :'',i7)') 
c    x  isolmol*nsolvnt1
      write(*,'('' Solute sites                     :'',i7)') 
     x  nsites
      write(*,'('' Total number of sites            :'',i7)') 
c    x  nsites+isolmol*nsolvnt1
     x  nsites+nsites1           



      icb1=0   !solvent bonds kept
      icb2=0   !solvent bonds removed
      do i=1,nbonds2
        iat=iorder(ijbond2(1,i))
        jat=iorder(ijbond2(2,i))
        if(iat.eq.0.or.jat.eq.0) then
          icb2=icb2+1
        else
          icb1=icb1+1
        endif
      enddo

      write(*,'(/,'' Solute bonds                     :'',i7)') 
     x  nbonds
      write(*,'('' Remaining solvent bonds          :'',i7)') 
     x  icb1
      write(*,'('' Total bonds                      :'',i7)') 
     x  nbonds+icb1




      ica1=0   !solvent angles kept
      ica2=0   !solvent angles removed
      do i=1,nangle2
        iat=iorder(ijkangle2(1,i))
        jat=iorder(ijkangle2(2,i))
        kat=iorder(ijkangle2(3,i))
        if(iat.eq.0.or.jat.eq.0.or.kat.eq.0) then
          ica2=ica2+1   
        else
          ica1=ica1+1   
        endif
      enddo

      write(*,'(/,'' Solute angles                    :'',i7)') 
     x  nangle
      write(*,'('' Remaining solvent angles         :'',i7)') 
     x  ica1                         
      write(*,'('' Total angles                     :'',i7)') 
     x  nangle+ica1




      icd1=0  !solvent dihedrals kept
      icd2=0  !solvent dihedrals removed
      do i=1,ndih2
        iat=iorder(ijkldih2(1,i))
        jat=iorder(ijkldih2(2,i))
        kat=iorder(ijkldih2(3,i))
        lat=iorder(ijkldih2(4,i))
        if(iat.eq.0.or.jat.eq.0.or.kat.eq.0.or.lat.eq.0) then
          icd2=icd2+1           
        else
          icd1=icd1+1           
        endif
      enddo

      write(*,'(/,'' Solute dihedrals                 :'',i7)') 
     x  ndih  
      write(*,'('' Remaining solvent dihedrals      :'',i7)') 
     x  icd1                          
      write(*,'('' Total dihedrals                  :'',i7)') 
     x  ndih+icd1                          





      write(15,'('' LMP file made by solvate program '')')
      write(15,*)
      write(15,'(5x,i10,'' atoms '')') 
     x  nsites+nsites1            
      write(15,'(5x,i10,'' bonds '')') 
     x  nbonds+icb1                          
      write(15,'(5x,i10,'' angles '')') 
     x  nangle+ica1
      write(15,'(5x,i10,'' dihedrals '')')  
     x  ndih+icd1
 
      write(*,'('' Solute atom types                :'',i7)') 
     x  natomtp
      write(*,'('' Remaining solvent atom types     :'',i7)') 
     x  natomtp2                    
      write(*,'('' Total atom types                 :'',i7)') 
     x  natomtp+natomtp2                  
      write(*,'(/,'' Solute bond types                :'',i7)') 
     x  nbtp      
      write(*,'('' Remaining solvent bond types     :'',i7)') 
     x  nbtp2                       
      write(*,'('' Total bond types                 :'',i7)') 
     x  nbtp+nbtp2                         
      write(*,'(/,'' Solute angle types               :'',i7)') 
     x  natp      
      write(*,'('' Remaining solvent angle types    :'',i7)') 
     x  natp2                       
      write(*,'('' Total angle types                :'',i7)') 
     x  natp+natp2                         
      write(*,'(/,'' Solute dihedral types            :'',i7)') 
     x  ndtp      
      write(*,'('' Remaining solvent dihedral types :'',i7)') 
     x  ndtp2                       
      write(*,'('' Total dihedral types             :'',i7)') 
     x  ndtp+ndtp2                         

      write(15,*)
      write(15,'(5x,i10,'' atom types '')') natomtp+natomtp2
      write(15,'(5x,i10,'' bond types '')') nbtp+nbtp2
      write(15,'(5x,i10,'' angle types '')') natp+natp2
      write(15,'(5x,i10,'' dihedral types '')') ndtp+ndtp2


c     cell edge 
      write(15,*)
      write(15,'(5x,2f15.8,'' xlo xhi '')') xlo2,xhi2
      write(15,'(5x,2f15.8,'' ylo yhi '')') xlo2,xhi2
      write(15,'(5x,2f15.8,'' zlo zhi '')') xlo2,xhi2

c     left justify the site names
      do i=1,nsites  
        call leftjust(atname(i))
      enddo
      do i=1,nsites2  
        call leftjust(atname2(i))
      enddo




!names of sites in the Masses block (corresponds to map(1,x) in ffassign)
!string structure is, e.g., PLA_C_1 and PLA_H_C_1_2 (says hydrogen #2 on carbon C_1)
      write(15,*)
      write(15,'('' Masses  '')')
      write(15,*)
c     convention to place solute molecule(s) first then the solvent
      do i=1,natomtp
        do j=1,nsites
          if(itype(j).eq.i) then 
            iexamp=j
            go to 75
          endif
        enddo
75      continue
c       ism=index(atname(iexamp),' ')-1
        ism=index(nametypes(i),' ')-1
        write(15,'(i10,f15.8,
     x    ''    # Num '',4x,'' Ex: '',i4,'' '',a)') 
c    x    i,fmass(i),iexamp,atname(iexamp)(1:ism)
     x    i,fmass(i),iexamp,nametypes(i)(1:ism)
      enddo

      do i=1,natomtp2
        do j=1,nsites2
          if(itype2(j).eq.i) then 
            iexamp=j
            go to 76
          endif
        enddo
76      continue
c       ism=index(atname2(iexamp),' ')-1
        ism=index(nametypes2(i),' ')-1
        write(15,'(i10,f15.8,
     x    ''    # Num '',4x,'' Ex: '',i4,'' '',a)') 
c    x    i+natomtp,fmass2(i),iexamp,atname2(iexamp)(1:ism)
     x    i+natomtp,fmass2(i),iexamp,nametypes2(i)(1:ism)
      enddo


      write(15,*)
      write(15,'('' Pair Coeffs'')')
      write(15,*)
      do i=1,natomtp
        do j=1,nsites
          if(itype(j).eq.i) then 
            iexamp=j
            go to 77
          endif
        enddo
77      continue
        ism=index(atname(iexamp),' ')-1
        write(15,'(i10,4f15.8,''   # '',a)') 
     x    i,
     x    vdw(2,i),vdw(1,i), !epsilon,sigma
     x    vdw(2,i),vdw(1,i), !epsilon,sigma
     x    atname(iexamp)(1:ism)
      enddo
      do i=1,natomtp2
        do j=1,nsites2
          if(itype2(j).eq.i) then 
            iexamp=j
            go to 78
          endif
        enddo
78      continue
        ism=index(atname2(iexamp),' ')-1
        write(15,'(i10,4f15.8,''   # '',a)') 
     x    natomtp+i,
     x    vdw2(2,i),vdw2(1,i), !epsilon,sigma
     x    vdw2(2,i),vdw2(1,i), !epsilon,sigma
     x    atname2(iexamp)(1:ism)
      enddo

c
c     implement geometric combining rules information for lammps
c
      is1=index(fname4,'.lmp')-1
      open(16,file=fname4(1:is1)//'.paircoeff')
      write(*,'('' Geometric combining rules for all type pairs '')')
      do i=1,natomtp+natomtp2
        if(i.le.natomtp) then
          vd2i=vdw(2,i)  !epsilon
          vd1i=vdw(1,i)  !sigma  
        else
          vd2i=vdw2(2,i-natomtp)  !epsilon
          vd1i=vdw2(1,i-natomtp)  !sigma  
        endif
        do j=i,natomtp+natomtp2
          if(j.le.natomtp) then
            vd2j=vdw(2,j)  !epsilon
            vd1j=vdw(1,j)  !sigma  
          else
            vd2j=vdw2(2,j-natomtp)  !epsilon
            vd1j=vdw2(1,j-natomtp)  !sigma  
          endif
          write(16,'(''pair_coeff     '',2i5,2f15.8)')
     x      i,j,dsqrt(vd2i*vd2j),
     x          dsqrt(vd1i*vd1j)
        enddo
      enddo
      close(16)


      write(15,*)
      write(15,'('' Bond Coeffs'')')
      write(15,*)
      do i=1,nbtp
        do j=1,nbonds
          if(ijbond(3,j).eq.i) then
            iexamp=j 
            go to 82
          endif
        enddo
82      continue
        iat=ijbond(1,iexamp)
        jat=ijbond(2,iexamp)
        isi=index(atname(iat),' ')-1
        isj=index(atname(jat),' ')-1
        write(15,'(i10,2f15.8,
     x    ''    # '',4x,'' Ex: '',i4,'' '',a,'' - '',a)') i,
     x    bndpar(1,i),bndpar(2,i),iexamp,
     x    atname(iat)(1:isi),atname(jat)(1:isj)
      enddo
      do i=1,nbtp2
        do j=1,nbonds2
          if(ijbond2(3,j).eq.i) then
            iexamp=j 
            go to 83
          endif
        enddo
83      continue
        iat=ijbond2(1,iexamp)
        jat=ijbond2(2,iexamp)
        isi=index(atname2(iat),' ')-1
        isj=index(atname2(jat),' ')-1
        write(15,'(i10,2f15.8,
     x    ''    # '',4x,'' Ex: '',i4,'' '',a,'' - '',a)') 
     x    i+nbtp,
     x    bndpar2(1,i),bndpar2(2,i),iexamp,
     x    atname2(iat)(1:isi),atname2(jat)(1:isj)
      enddo



      write(15,*)
      write(15,'('' Angle Coeffs'')')
      write(15,*)
      do i=1,natp
        do j=1,nangle
          if(ijkangle(4,j).eq.i) then
            iexamp=j 
            go to 86
          endif
        enddo
86      continue
        iat=ijkangle(1,iexamp)
        jat=ijkangle(2,iexamp)
        kat=ijkangle(3,iexamp)
        isi=index(atname(iat),' ')-1
        isj=index(atname(jat),' ')-1
        isk=index(atname(kat),' ')-1
        write(15,'(i10,2f15.8,
     x    ''    # '',4x,'' Ex: '',i4,'' '',a,'' - '',a,'' - '',a)')
     x    i,angpar(1,i),angpar(2,i),
     x    iexamp,
     x    atname(iat)(1:isi),atname(jat)(1:isj),atname(kat)(1:isk)
      enddo
      do i=1,natp2
        do j=1,nangle2
          if(ijkangle2(4,j).eq.i) then
            iexamp=j 
            go to 89
          endif
        enddo
89      continue
        iat=ijkangle2(1,iexamp)
        jat=ijkangle2(2,iexamp)
        kat=ijkangle2(3,iexamp)
        isi=index(atname2(iat),' ')-1
        isj=index(atname2(jat),' ')-1
        isk=index(atname2(kat),' ')-1
        write(15,'(i10,2f15.8,
     x    ''    # '',4x,'' Ex: '',i6,'' '',a,'' - '',a,'' - '',a)')
     x    i+natp,angpar2(1,i),angpar2(2,i),
     x    iexamp,
     x    atname2(iat)(1:isi),
     x    atname2(jat)(1:isj),
     x    atname2(kat)(1:isk)
      enddo



      write(15,*)
      write(15,'('' Dihedral Coeffs'')')
      write(15,*)
      do i=1,ndtp
        do j=1,ndih  
          if(ijkldih(5,j).eq.i) then
            iexamp=j 
            go to 91
          endif
        enddo
91      continue
        iat=ijkldih(1,iexamp)
        jat=ijkldih(2,iexamp)
        kat=ijkldih(3,iexamp)
        lat=ijkldih(4,iexamp)
        isi=index(atname(iat),' ')-1
        isj=index(atname(jat),' ')-1
        isk=index(atname(kat),' ')-1
        isl=index(atname(lat),' ')-1
        write(15,'(i10,f10.5,i5,i5,f10.5,
     x    ''    # Num '',4x,'' Ex:  '',i6,
     x    '' '',a,'' - '',a,'' - '',a,'' - '',a)')
     x    i,dihpar(1,i),int(dihpar(2,i)),
     x      int(dihpar(3,i)),dihpar(4,i),iexamp,
     x    atname(iat)(1:isi),atname(jat)(1:isj),
     x    atname(kat)(1:isk),atname(lat)(1:isl)
      enddo
      do i=1,ndtp2
        do j=1,ndih2 
          if(ijkldih2(5,j).eq.i) then
            iexamp=j 
            go to 93
          endif
        enddo
93      continue
        iat=ijkldih2(1,iexamp)
        jat=ijkldih2(2,iexamp)
        kat=ijkldih2(3,iexamp)
        lat=ijkldih2(4,iexamp)
        isi=index(atname2(iat),' ')-1
        isj=index(atname2(jat),' ')-1
        isk=index(atname2(kat),' ')-1
        isl=index(atname2(lat),' ')-1
        write(15,'(i10,f10.5,i5,i5,f10.5,
     x    ''    # Num '',4x,'' Ex:  '',i6,
     x    '' '',a,'' - '',a,'' - '',a,'' - '',a)')
     x    i+ndtp,dihpar2(1,i),int(dihpar2(2,i)),
     x      int(dihpar2(3,i)),dihpar2(4,i),iexamp,
     x    atname2(iat)(1:isi),atname2(jat)(1:isj),
     x    atname2(kat)(1:isk),atname2(lat)(1:isl)
      enddo



c     build a quick and dirty pdb file to LUN 16
c     pdb fields are "ATOM"
c                    atom number, i7
c                    atom name, a3, e.g., H_2
c                    residue (3 char)
c                    mol num
c                    xyz
c                    charge

      is1=index(fname4,'.lmp')-1
      open(16,file=fname4(1:is1)//'.pdb')




c     build a quick psf file to LUN 17
      is1=index(fname4,'.lmp')-1
      open(17,file=fname4(1:is1)//'.psf')
      call amassinit(amass,120)

      write(17,'(''PSF '')')
      write(17,*)
      write(17,*)
      write(17,'(i8,'' !NTITLE'')') 3
c     note could write the date, names of files used, summary of structure
      write(17,'('' REMARKS '','' Generated by solvate   '')') 
      write(17,'('' REMARKS '','' Generated by solvate   '')') 
      write(17,'('' REMARKS '','' Generated by solvate   '')') 
      write(17,*)
      write(17,'(i8,'' !NATOM'')') nsites+nsites1
      maxres=0   !used in psf file
      do i=1,nsites
c       parse the atom names to find the residue index for each atom
        call parser(atname(i),string,lelfield,i1,i2,i3)
        maxres=max(maxres,i3)
      enddo
c     psf file specifies a chain id used within vmd; this information is in
c     the *.arm file that goes with the solute molecule
      is=index(fname1,'.lmp')
      open(18,file=fname1(1:is)//'arm')
      nres=0
300   continue      
c        3    PE     17   22    1    (i10, 5x, a10, then 3 integers)
      read(18,'(a120)',end=310)  line
      nres=nres+1
      read(line(26:),*) (ijres(k,nres),k=1,3) !grab first/last site and armid for residue
      go to 300
310   continue
      close(18)
      if(nres.ne.maxres+1) then   !note residue index in name field starts at 0
        write(*,'(''Error:  mismatch in residue count'',
     x    '' in *.arm file'')')
        write(*,'(''Residue count in *.arm file is '',i6)') nres
        write(*,'(''Residue count in *.lmp file is '',i6)') maxres+1
        stop 'mismatch in solute residue count'
      endif






      write(15,*)
      write(15,'('' Atoms'')')
      write(15,*)
      molid=1
      do i=1,nsites
        call lstchr(atname(i),i1)
        write(15,'(i10,i10,i5,f10.5,3f15.8,
     x     ''    # '',a)')
     x     i,molid,itype(i),charge(i),
     x     coord(1,i)+xshft,
     x     coord(2,i)+yshft,
     x     coord(3,i)+zshft,
     x     atname(i)(1:i1)

c       build a quick and dirty pdb file to LUN 16
        it=itype(i)
        is1=index(nametypes(it),'_')  !residue in substring 1:is1-1
        is2=index(nametypes(it)(is1+1:),'_')+is1  !element in substring is1+1:is2-1
        write(16,'(''ATOM'',i7,2x,a3,1x,a3,i6,4x,3f8.3,
     x    1x,f5.2,1x,f5.2)')
c       write(16,'(''ATOM'',i8,2x,a3,1x,a3,i7,2x,3f8.3,
c    x    1x,f5.2,1x,f5.2)')
     x    i,nametypes(it)(is1+1:is2-1),nametypes(it)(1:is1-1),
     x     1,  !make solute molecule number 1
     x     coord(1,i)+xshft,
     x     coord(2,i)+yshft,
     x     coord(3,i)+zshft,
     x     charge(i),0.0d0

c       build a quick psf file to LUN 17
        call parser(atname(i),string,lele,i1,i2,i3)
c       istrnd=ijres(5,i3-minres+1) !get strand index from *.arm file (with input solute *.lmp)
        istrnd=ijres(3,i3+1)
c       write(*,'(''Solute site '',i6,'' residue '',i5,'' arm '',i3)')
c    x    i,i3+1,istrnd
        iat=igetatnum(string(1:lele))
        write(17,'(i8,1x,a4,1x,  i4,1x,a4,1x, 
     x            a4,1x,i4,1x,   f14.6,f14.6,i8 )')     
     x    i,char(ichar('A')+istrnd),   !atom index (i8); segment (a4)
     x    i3,nametypes(it)(1:is1-1),   !residue index (i4); resname (a4)
     x    string(1:lele),12,           !atom name (a4); atom type (i4)        
     x    charge(i),amass(iat),0       !charge (f14.6); mass (f14.6)

      enddo

c     do i=1,nsolvnt1   !loop over remaining solvent sites
c       molid=1+i
c       imol=closest(1,i+nwremv)
c       do j=1,isolmol
c         jat=isolmol*(imol-1)+j
c         call lstchr(atname2(jat),i1)
c         write(15,'(i10,i10,i5,f10.5,3f15.8,
c    x     ''    # '',a)')
c    x     nsites+isolmol*(i-1)+j,molid,natomtp+itype2(jat),
c    x     charge2(jat),(coord2(k,jat),k=1,3),atname2(jat)(1:i1)
c       enddo
c     enddo

      jat=0
      jmol=0
      do i=1,ntype
        do j=1,molecule(i,2)
          do k=1,molecule(i,1)
            jat=jat+1
            if(iorder(jat).ne.0) then
              if(k.eq.1) jmol=jmol+1
              call lstchr(atname2(jat),i1)
              write(15,'(i10,i10,i5,f10.5,3f15.8,
     x          ''    # '',a)')
     x          nsites+iorder(jat)    ,jmol+1,natomtp+itype2(jat),
     x          charge2(jat),
     x          (coord2(l,jat),l=1,3),
     x          atname2(jat)(1:i1)

c             build a quick and dirty pdb file to LUN 16
              it=itype2(jat)
              is1=index(nametypes2(it),'_')  !residue in substring 1:is1-1
              is2=index(nametypes2(it)(is1+1:),'_')
              if(is2.eq.0) is2=index(nametypes2(it)(is1+1:),' ')
              is2=is2+is1  !element in substring is1+1:is2-1
c             write(16,'(''ATOM'',i7,2x,
c    x          a3,1x,a3,i6,4x,3f8.3,
c    x          1x,f5.2,1x,f5.2)')
              write(16,'(''ATOM'',i8,2x,
     x          a3,1x,a3,i7,2x,3f8.3,
     x          1x,f5.2,1x,f5.2)')
     x          nsites+iorder(jat),
     x          nametypes2(it)(is1+1:is2-1),
     x          nametypes2(it)(1:is1-1),
     x          1+jmol,     !molecule number
     x          (coord2(l,jat),l=1,3),
     x          charge2(jat),0.0d0

c             build a quick psf file to LUN 17
              iat=igetatnum(nametypes2(it)(is1+1:is2-1))
              write(17,'(i8,1x,a4,1x,  i4,1x,a4,1x, 
     x                  a4,1x,i4,1x,   f14.6,f14.6,i8 )')     
     x          nsites+iorder(jat),'W',
     x          jmol+maxres,nametypes2(it)(1:is1-1),
     x          nametypes2(it)(is1+1:is2-1),12,
     x          charge2(jat),amass(iat),0 


            endif
          enddo
        enddo
      enddo











c
c     check the distance from each solvent to all solute sites and
c     print a warning if any are less than thresh = 1.5
c
      thresh=1.5d0
c     do i=1,nsolvnt1   !loop over remaining solvent sites
c       molid=1+i
c       imol=closest(1,i+nwremv)
c       do j=1,isolmol  !loop over solvent sites
c         jat=isolmol*(imol-1)+j
c         do k=1,nsites !loop over solute sites
c           dx=coord2(1,jat)-(coord(1,k)+xshft)
c           dy=coord2(2,jat)-(coord(2,k)+yshft)
c           dz=coord2(3,jat)-(coord(3,k)+zshft)
c           if(dx.gt. 0.5d0*xlen) dx=dx-xlen
c           if(dx.lt.-0.5d0*xlen) dx=dx+xlen
c           if(dy.gt. 0.5d0*ylen) dy=dy-ylen
c           if(dy.lt.-0.5d0*ylen) dy=dy+ylen
c           if(dz.gt. 0.5d0*zlen) dz=dz-zlen
c           if(dz.lt.-0.5d0*zlen) dz=dz+zlen
c           dist=dsqrt(dx**2+dy**2+dz**2)
c           if(dist.lt.thresh) then
c             write(*,'(''WARNING:  Close approach'',
c    x        '' Solvent molecule '',i6,'' Solute site '',i6,
c    x        '' Distance '',f10.5)')
c    x          i,k,dist
c           endif
c         enddo
c       enddo
c     enddo


      thresh=1.5d0
      molid=0
      do i=1,ntype
        do j=1,molecule(i,2)
          kfirst=molecule(i,3) + (j-1)*molecule(i,1)
c         write(*,'(''Close approach check for type '',i2,
c    x      '' molecule '',i5,'' first index '',i6,
c    x      '' iorder '',i6)')
c    x      i,j,kfirst,iorder(kfirst)
          if(iorder(kfirst).ne.0) then
            molid=molid+1
            do k=1,molecule(i,1)
              kat=kfirst+k-1
              do l=1,nsites
                dx=coord2(1,kat)-(coord(1,l)+xshft)
                dy=coord2(2,kat)-(coord(2,l)+yshft)
                dz=coord2(3,kat)-(coord(3,l)+zshft)
                if(dx.gt. 0.5d0*xlen) dx=dx-xlen
                if(dx.lt.-0.5d0*xlen) dx=dx+xlen
                if(dy.gt. 0.5d0*ylen) dy=dy-ylen
                if(dy.lt.-0.5d0*ylen) dy=dy+ylen
                if(dz.gt. 0.5d0*zlen) dz=dz-zlen
                if(dz.lt.-0.5d0*zlen) dz=dz+zlen
                dist=dsqrt(dx**2+dy**2+dz**2)
                if(dist.lt.thresh) then
                  write(*,'(''WARNING:  Close approach'',
     x            '' Molecule type '',i2,
     x            '' Solvent molecule '',i6,'' Solute site '',i6,
     x            '' Distance '',f10.5)')
     x            i,j,l,dist
                endif
              enddo
            enddo
          endif
        enddo
      enddo




      write(15,*)
      write(15,'('' Velocities'')')
      write(15,*)
      do i=1,nsites+nsites1
        write(15,'(i10,5x,3f10.5)') i,0.,0.,0.
      enddo







      write(15,*)
      write(15,'('' Bonds'')')
      write(15,*)

c     build a quick psf file to LUN 17
      write(17,*)
      write(17,'(i8,'' !NBONDS'')') 
     x  nbonds+nsolvnt1*(nbonds2/nsolvnt0)
      npsfbond=0

      do i=1,nbonds
        write(15,'(i10,3i10)') i,ijbond(3,i),ijbond(1,i),ijbond(2,i)

c       build a quick and dirty pdb file to LUN 16
        write(16,'(''CONECT'',2i5)') ijbond(1,i),ijbond(2,i)

c       build a quick psf file to LUN 17
        if(npsfbond.eq.4) then
          write(17,'(8i8)') ((ipsfbond(k,l),k=1,2),l=1,4) 
          npsfbond=0
        endif
        npsfbond=npsfbond+1
        ipsfbond(1,npsfbond)=ijbond(1,i)
        ipsfbond(2,npsfbond)=ijbond(2,i)

      enddo

      icount=0
      do i=1,nbonds2
        iat=iorder(ijbond2(1,i))
        jat=iorder(ijbond2(2,i))
        if(iat.eq.0.or.jat.eq.0) then
c         write(15,'('' Removing bond '',i6,'' iat,jat='',2i6)')
c    x      i,iat,jat 
        else
          icount=icount+1
          write(15,'(i10,3i10)') nbonds+icount,
     x      nbtp+ijbond2(3,i),nsites+iat,nsites+jat                   

c         build a quick and dirty pdb file to LUN 16
          write(16,'(''CONECT'',2i5)') nsites+iat,nsites+jat

c         build a quick psf file to LUN 17
          if(npsfbond.eq.4) then
            write(17,'(8i8)') ((ipsfbond(k,l),k=1,2),l=1,4) 
            npsfbond=0
          endif
          npsfbond=npsfbond+1
          ipsfbond(1,npsfbond)=nsites+iat
          ipsfbond(2,npsfbond)=nsites+jat

        endif

      enddo

c     close the quick and dirty pdb file to LUN 16
      close(16)


c     build a quick psf file to LUN 17
      if(npsfbond.gt.0) write(17,'(8i8)') 
     x  ((ipsfbond(k,l),k=1,2),l=1,npsfbond) 
      close(17)




      write(15,*)
      write(15,'('' Angles'')')
      write(15,*)
      do i=1,nangle
        write(15,'(i10,4i10)') i,ijkangle(4,i),ijkangle(1,i),
     x                           ijkangle(2,i),ijkangle(3,i)
      enddo
      icount=0
      do i=1,nangle2
        iat=iorder(ijkangle2(1,i))
        jat=iorder(ijkangle2(2,i))
        kat=iorder(ijkangle2(3,i))
        if(iat.eq.0.or.jat.eq.0.or.kat.eq.0) then
c         write(15,'('' Removing angl '',i6,'' iat,jat,kat='',3i6)')
c    x      i,iat,jat,kat
        else
          icount=icount+1
          write(15,'(i10,4i10)') nangle+icount,
     x      natp+ijkangle2(4,i),nsites+iat,nsites+jat,nsites+kat                
        endif
      enddo


      write(15,*)
      write(15,'('' Dihedrals'')')
      write(15,*)
      do i=1,ndih   
        write(15,'(i10,5i10)') i,ijkldih(5,i),ijkldih(1,i),
     x    ijkldih(2,i),ijkldih(3,i),ijkldih(4,i)
      enddo
      icount=0
      do i=1,ndih2
        iat=iorder(ijkldih2(1,i))
        jat=iorder(ijkldih2(2,i))
        kat=iorder(ijkldih2(3,i))
        lat=iorder(ijkldih2(4,i))
        if(iat.eq.0.or.jat.eq.0.or.kat.eq.0.or.lat.eq.0) then
c         write(15,'('' Removing dih  '',i6,'' i,j,k,l='',4i6)')
c    x      i,iat,jat,kat,lat
        else
          icount=icount+1
          write(15,'(i10,5i10)') ndih+icount,
     x      ndtp+ijkldih2(5,i),
     x      nsites+iat,nsites+jat,nsites+kat,nsites+lat     
        endif
      enddo


      close(15)




c
c     write a Jed-style pdb file for vmd to use     
c     pdb fields are "ATOM"
c                    atom number, i7
c                    atom name, a3, e.g., H_2
c                    residue (3 char)
c                    mol num
c                    xyz
c                    charge
c
 
c      is1=index(fname4,'.lmp')-1
c      open(15,file=fname4(1:is1)//'.pdb')
c
c      molid=1
c      do i=1,nsites
cc       call lstchr(atname(i),i1)
cc         write(15,'(i10,i10,i5,f10.5,3f15.8,
cc    x     ''    # '',a)')
cc    x     i,molid,itype(i),charge(i),
cc    x     coord(1,i)+xshft,
cc    x     coord(2,i)+yshft,
cc    x     coord(3,i)+zshft,
cc    x     atname(i)(1:i1)
cc       find first field; assume residue name
c        it=itype(i)
c        is1=index(nametypes(it),'_')  !residue in substring 1:is1-1
c        is2=index(nametypes(it)(is1+1:),'_')+is1  !element in substring is1+1:is2-1
c        write(15,'(''ATOM'',i7,2x,a3,1x,a3,i6,4x,3f8.3,
c     x    1x,f5.2,1x,f5.2)')
c     x    i,nametypes(it)(is1+1:is2-1),nametypes(it)(1:is1-1),
c     x     1,  !make solute molecule number 1
c     x     coord(1,i)+xshft,
c     x     coord(2,i)+yshft,
c     x     coord(3,i)+zshft,
c     x     charge(i),0.0d0
c      enddo
c
c      do i=1,nsolvnt1   !loop over remaining solvent sites
c        imol=closest(1,i+nwremv)
c        do j=1,isolmol
c          jat=isolmol*(imol-1)+j
c          it=itype2(jat)
c          is1=index(nametypes2(it),'_')  !residue in substring 1:is1-1
c          is2=index(nametypes2(it)(is1+1:),'_')
c          if(is2.eq.0) is2=index(nametypes2(it)(is1+1:),' ')
c          is2=is2+is1  !element in substring is1+1:is2-1
cc         write(*,'('' Water molecle i,imol= '',2i6,'' atom j,jat='',
cc    x      2i6,'' type='',i3,'' name:*'',a,''* atname:*'',a,''*'')')
cc    x      i,imol,j,jat,it,nametypes2(it)(1:is2-1),atname(jat)
c          write(15,'(''ATOM'',i7,2x,
c     x      a3,1x,a3,i6,4x,3f8.3,
c     x      1x,f5.2,1x,f5.2)')
c     x      nsites+isolmol*(i-1)+j,
c     x      nametypes2(it)(is1+1:is2-1),nametypes2(it)(1:is1-1),
c     x      1+i,  
c     x      coord2(1,jat),
c     x      coord2(2,jat),
c     x      coord2(3,jat),
c     x      charge2(jat),0.0d0
cc         call lstchr(atname2(jat),i1)
cc         write(15,'(i10,i10,i5,f10.5,3f15.8,
cc    x      ''    # '',a)')
cc    x      nsites+isolmol*(i-1)+j,molid,natomtp+itype2(jat),
cc    x      charge2(jat),(coord2(k,jat),k=1,3),atname2(jat)(1:i1)
c        enddo
c      enddo
c
c
c
c      do i=1,nbonds
c        write(15,'(''CONECT'',2i5)') ijbond(1,i),ijbond(2,i)
c      enddo
c      icount=0
c      do i=1,nbonds2
c        iat=iorder(ijbond2(1,i))
c        jat=iorder(ijbond2(2,i))
c        if(iat.eq.0.or.jat.eq.0) then
cc         write(15,'('' Removing bond '',i6,'' iat,jat='',2i6)')
cc    x      i,iat,jat 
c        else
c          icount=icount+1
cc         write(15,'(i10,3i10)') nbonds+icount,
cc    x      nbtp+ijbond2(3,i),nsites+iat,nsites+jat                   
c        write(15,'(''CONECT'',2i5)') nsites+iat,nsites+jat
c        endif
c      enddo
c
c      close(15)





c
c     build a psf file for vmd to use
c     analyze the bond list data to find chains
c
c      maxres=0  !find the range for the residue index; check if ijres is big enough
c      minres=1000
c      do i=1,nsites
cc       parse the atom names to find the residue index for each atom
c        call parser(atname(i),string,lelfield,i1,i2,i3)
c        maxres=max(maxres,i3)
c        minres=min(minres,i3)
c      enddo
c      write(*,'('' Range of residue indices is '',i3,'' to '',i3)')
c     x  minres,maxres
c      if(maxres-minres+1.gt.1000) then
c        write(*,'('' Increase size of ijres to at least '',i5)')
c     x    maxres-minres+1
c        stop 'increase size of ijres'
c      endif
c      do i=1,1000
c        do j=1,5
c          ijres(j,i)=0
c        enddo
c      enddo
cc     scan through the bond list and find bonds that connect different residues
c      do i=1,nbonds
c        iat=ijbond(1,i)
c        call parser(atname(iat),string,lelfield,i1,i2,i3)
c        jat=ijbond(2,i)
c        call parser(atname(jat),string,lelfield,i1,i2,j3)
c        if(i3.ne.j3) then
cc         two residues are connected; see if this connection has already been seen
cc         look for j3 in the list of i3
c          ifound=0
c          do j=1,ijres(1,i3-minres+1)
c            if(ijres(j+1,i3-minres+1).eq.j3) ifound=1
c          enddo
c          if(ifound.eq.0) then
c            inext=ijres(1,i3-minres+1)+1
c            ijres(1,i3-minres+1)=inext                      
c            if(inext+1.le.5) ijres(inext+1,i3-minres+1)=j3           
c          endif
cc         look for i3 in the list of j3
c          ifound=0
c          do j=1,ijres(1,j3-minres+1)
c            if(ijres(j+1,j3-minres+1).eq.i3) ifound=1
c          enddo
c          if(ifound.eq.0) then
c            inext=ijres(1,j3-minres+1)+1
c            ijres(1,j3-minres+1)=inext                      
c            if(inext+1.le.5) ijres(inext+1,j3-minres+1)=i3           
c          endif
c        endif
c      enddo

c      do i=minres,maxres
c        nconn=min(4,(ijres(1,i-minres+1)))
c        if(nconn.ne.2) then
c          write(*,'('' Residue '',i3,'' connected to '',10i5)')
c     x      i,
c     x    (ijres(k+1,i-minres+1),k=1,min(4,(ijres(1,i-minres+1))))
c        endif
c      enddo
c
cc     most residues will be connected to two others in the same chain
cc     residues with only one connection are chain termini and
c     residues with more than two are nodes
c     chains will go from a terminus to a node
c
c     store a chain id in the last column of ijres
c      do i=minres,maxres
c        ijres(5,i-minres+1)=-1   !initialize to "unassigned" chain id
c      enddo

c      nchain=0
cc     find next nonassigned terminus
c390   continue
c      ifnew=0
c      do i=minres,maxres
c        if(ijres(5,i-minres+1).eq.-1.and.ijres(1,i-minres+1).eq.1) then
c          ifnew=1
c          nchain=nchain+1
c          ijres(5,i-minres+1)=nchain  !assign terminus the next chain id
cc         now assign this chain id from this residue to the next nodes
c          icurrent=i   !icurrent is the last residue assigned a chain id
c          inext=ijres(2,icurrent-minres+1)  !inext is a candidate; if not a node
c400       continue
c          if(ijres(1,inext-minres+1).eq.2) then
cc           normal chain extension condition
c            ijres(5,inext-minres+1)=nchain
c            inew=ijres(2,inext-minres+1)               
c            if(inew.eq.icurrent) inew=ijres(3,inext-minres+1)       
c            icurrent=inext
c            inext=inew
c            go to 400
c          elseif(ijres(1,inext-minres+1).gt.2) then
cc           node condition, terminating the chain
c            go to 390
c          elseif(ijres(1,inext-minres+1).lt.2) then
cc           error condition for star polymers; ok for linear strands whiere could be 1
cc           but should never be zero
c            write(*,'('' Error condition; new code needed to'',
c     x        '' address non-star topologies'',
c     x        '' (dendrimers or linear chains).'')')
c            write(*,'('' Error condition; unexpected termination of'',
c     x        '' chain.'')')
c            stop 'Error condition in chain identification'
c          endif
c        endif
c      enddo
c      if(ifnew.ne.0) go to 390
c
c      write(*,'('' Number of chains found = '',i3)') nchain
cc      do i=minres,maxres
c        if(ijres(1,i-minres+1).gt.2) then
c          write(*,'('' Node:  residue number '',i3)') i
c          ijres(5,i-minres+1)=0  !assign chain id of zero to nodes
c        endif
cc      enddo

c      do i=1,nchain
c        nlong=0
c        do j=minres,maxres
c          if(ijres(5,j-minres+1).eq.i) then
c            nlong=nlong+1
c            write(*,'('' Chain '',i3,'' residue '',i3,'' resid '',i3)')
c     x        i,nlong,j
c          endif
c        enddo
c        write(*,'('' Length of chain '',i3,'' is '',i3,'' residues.'')')
c     x    i,nlong
c      enddo
c
c
c     at this point the strand ID is in ijres(5,*) for residue *



c      is1=index(fname4,'.lmp')-1
c      open(15,file=fname4(1:is1)//'.psf')
c      call amassinit(amass,120)

c      write(15,'(''PSF '')')
c      write(15,*)
c      write(15,*)
c      write(15,'(i8,'' !NTITLE'')') 3
cc     note could write the date, names of files used, summary of structure
c      write(15,'('' REMARKS '','' Generated by merge     '')') 
c      write(15,'('' REMARKS '','' Generated by merge     '')') 
c      write(15,'('' REMARKS '','' Generated by merge     '')') 
c      write(15,*)
c
c      write(15,'(i8,'' !NATOM'')') nsites+nsolvnt1*isolmol
cc     fields are atom ID
cc                segment name   (might eventually make this into a strand index)
cc                residue ID
cc                residue name
cc                atom name
cc                atom type (charmm format uses integer; xplor uses name)
cc                charge
cc                mass
c
c      do i=1,nsites
c        call parser(atname(i),string,lele,i1,i2,i3)
c        istrnd=ijres(5,i3-minres+1)
c        it=itype(i)
c        is1=index(nametypes(it),'_')  !residue in substring 1:is1-1
c        is2=index(nametypes(it)(is1+1:),'_')+is1  !element in substring is1+1:is2-1
c        iat=igetatnum(string(1:lele))
cc       write(15,'(i8,1x,a1  
cc                  atom and segment 
cc    x           ,i5,4x,a3     
cc                  residue
cc    x           ,2x,a3,2x,a3    
cc                  atom
cc    x           ,f12.6,f14.4,i20 
cc                  charge and mass
cc    x            )')     
cc    x    i,char(ichar('A')+istrnd),
cc    x    i3,nametypes(it)(1:is1-1),
cc    x    string(1:lele),
cc    x    string(1:lele),
cc    x    charge(i),amass(iat),0 
cc       following format file from archive.ambermd.org
c        write(15,'(i8,1x,a4,1x,  
cc                  atom and segment 
c     x            i4,1x,a4,1x, 
cc                  residue
c     x            a4,1x,i4,1x,    
cc                  atom
c     x            f14.6,f14.6,i8
cc                  charge and mass
c     x            )')     
c     x    i,char(ichar('A')+istrnd),
c     x    i3,nametypes(it)(1:is1-1),
c     x    string(1:lele),12,              
c     x    charge(i),amass(iat),0 
c      enddo
c
cc     all solvent molecules have the same chain identifier (name=W id=nchain+1)
c      do i=1,nsolvnt1   !loop over remaining solvent sites
c        molid=1+i
c        imol=closest(1,i+nwremv)
c        do j=1,isolmol
c          jat=isolmol*(imol-1)+j
c          it=itype2(jat)
c          is1=index(nametypes2(it),'_')  !residue in substring 1:is1-1
c          is2=index(nametypes2(it)(is1+1:),'_')  
c          if(is2.eq.0) is2=index(nametypes2(it)(is1+1:),' ')
c          is2=is2+is1                    !element in substring is1+1:is2-1
c          iat=igetatnum(nametypes2(it)(is1+1:is2-1))
cc         write(15,'(i8,1x,a1  
cc                  atom and segment 
cc    x           ,i5,4x,a3     
cc                  residue id and name
cc    x           ,2x,a3,2x,a3    
cc                  atom
cc    x           ,f12.6,f14.4,i20 
cc                  charge and mass
cc    x            )')     
cc    x      nsites+isolmol*(i-1)+j,'W',
cc    x      i+maxres,nametypes2(it)(1:is1-1),
cc    x      nametypes2(it)(is1+1:is2-1),
cc    x      nametypes2(it)(is1+1:is2-1),
cc    x      charge2(jat),amass(iat),0 
c          write(15,'(i8,1x,a4,1x, 
cc                  atom and segment 
c     x            i4,1x,a4,1x,     
cc                  residue id and name
c     x            a4,1x,i4,1x,    
cc                  atom
c     x            f14.6,f14.6,i8  
cc                  charge and mass
c     x            )')     
c     x      nsites+isolmol*(i-1)+j,'W',
c     x      i+maxres,nametypes2(it)(1:is1-1),
c     x      nametypes2(it)(is1+1:is2-1),12,
c     x      charge2(jat),amass(iat),0 
cc         call lstchr(atname2(jat),i1)
cc         write(15,'(i10,i10,i5,f10.5,3f15.8,
cc    x     ''    # '',a)')
cc    x     nsites+isolmol*(i-1)+j,molid,natomtp+itype2(jat),
cc    x     charge2(jat),(coord2(k,jat),k=1,3),atname2(jat)(1:i1)
c        enddo
c      enddo
c
c
c
c
c     list the bonds

c      write(15,*)
c      write(15,'(i8,'' !NBONDS'')') 
c     x  nbonds+nsolvnt1*(nbonds2/nsolvnt0)
cc     write(15,'('' Nbonds = '',i6)') nbonds
cc     write(15,'('' Nsolvnt0= '',i6)') nsolvnt0
cc     write(15,'('' Nsolvnt1= '',i6)') nsolvnt1
cc     write(15,'('' Nbwats  = '',i6)') nsolvnt1*(nbonds2/nsolvnt0)
c 
cc     four pairs per line 8i8
c      nsets=(nbonds+nsolvnt1*(nbonds2/nsolvnt0))/4
c      nleft=(nbonds+nsolvnt1*(nbonds2/nsolvnt0))-4*nsets
c      do iset=1,nsets    
cc       write(15,'('' iset = '',i6)') iset
c        i1=4*(iset-1)+1  !first bond of this set of four
c        i2=4*(iset-1)+2  !second bond of this set of four
c        i3=4*(iset-1)+3  !third bond of this set of four
c        i4=4*(iset-1)+4  !fourth bond of this set of four
c
c        ia1=ijbond(1,i1)  !in the polymer
c        ia2=ijbond(2,i1)  !in the polymer
c        ib1=ijbond(1,i2)  !in the polymer
c        ib2=ijbond(2,i2)  !in the polymer
c        ic1=ijbond(1,i3)  !in the polymer
c        ic2=ijbond(2,i3)  !in the polymer
c        id1=ijbond(1,i4)  !in the polymer
c        id2=ijbond(2,i4)  !in the polymer
c
cc       find out what solvent molecule this pertains to
c        if(i1.gt.nbonds) then
c          ibmol=1+(i1-nbonds-1)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c          ibnd=(i1-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c          imol=closest(1,ibmol+nwremv) !which molecule in the big list
c          ia1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c          ia2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
cc         write(15,'('' i1:  ibmol,ibnd,imol '',3i10)') ibmol,ibnd,imol
c        endif
c
c        if(i2.gt.nbonds) then
c          ibmol=1+(i2-nbonds-1)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c          ibnd=(i2-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c          imol=closest(1,ibmol+nwremv) !which molecule in the big list
c          ib1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c          ib2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
cc         write(15,'('' i2:  ibmol,ibnd,imol '',3i10)') ibmol,ibnd,imol
c        endif
c
c        if(i3.gt.nbonds) then
c          ibmol=1+(i3-nbonds-1)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c          ibnd=(i3-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c          imol=closest(1,ibmol+nwremv) !which molecule in the big list
c          ic1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c          ic2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
cc         write(15,'('' i3:  ibmol,ibnd,imol '',3i10)') ibmol,ibnd,imol
c        endif
c
c        if(i4.gt.nbonds) then
c          ibmol=1+(i4-nbonds-1)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c          ibnd=(i4-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c          imol=closest(1,ibmol+nwremv) !which molecule in the big list
c          id1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c          id2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
cc         write(15,'('' i4:  ibmol,ibnd,imol '',3i10)') ibmol,ibnd,imol
c        endif
c        write(15,'(8i8)') ia1,ia2, ib1,ib2, 
c     x                    ic1,ic2, id1,id2  
c      enddo   
c
c

c      if(nleft.gt.0) then
c        i1=4*nsets+1  !first bond of this set of four
c        i2=4*nsets+2  !second bond of this set of four
c        i3=4*nsets+3  !third bond of this set of four
c        ia1=ijbond(1,i1)  !in the polymer
c        ia2=ijbond(2,i1)  !in the polymer
c        ib1=ijbond(1,i2)  !in the polymer
c        ib2=ijbond(2,i2)  !in the polymer
c        ic1=ijbond(1,i3)  !in the polymer
c        ic2=ijbond(2,i3)  !in the polymer
c
c        if(i1.gt.nbonds) then
c          ibmol=1+(i1-nbonds)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c          ibnd=(i1-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c          imol=closest(1,ibmol+nwremv) !which molecule in the big list
c          ia1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c          ia2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c        endif
c
c        if(i2.gt.nbonds) then
c          ibmol=1+(i2-nbonds)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c          ibnd=(i2-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c          imol=closest(1,ibmol+nwremv) !which molecule in the big list
c          ib1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c          ib2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c        endif
c
c        if(i3.gt.nbonds) then
c          ibmol=1+(i3-nbonds)/(nbonds2/nsolvnt0)   !indicates which solvent molecule
c          ibnd=(i3-nbonds) - (ibmol-1)*(nbonds2/nsolvnt0) !indicates which bond 
c          imol=closest(1,ibmol+nwremv) !which molecule in the big list
c          ic1=nsites+iorder(ijbond2(1,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c          ic2=nsites+iorder(ijbond2(2,ibnd+(nbonds2/nsolvnt0)*(imol-1)))
c        endif
c
c        if(nleft.eq.1) then
c          write(15,'(8i8)') ia1,ia2
c        elseif(nleft.eq.2) then
c          write(15,'(8i8)') ia1,ia2,ib1,ib2
c        elseif(nleft.eq.3) then
c          write(15,'(8i8)') ia1,ia2,ib1,ib2,ic1,ic2
c        endif
c      endif
cc
cc
c
c      write(15,*)
c
c      close(15)



      end

c
c     subroutine to read a set of coordinates from an xyz file
c     expect this to have a set of coords, we are usually interested
c     in either the first or the last
c
      subroutine getcoord(fname,nsites,itype,coord,iframe)
      implicit real*8 (a-h,o-z)
      character*120 line
      character*(*) fname
      dimension coord(3,nsites),itype(nsites)

      write(*,'('' Reading coordinates from file *'',a,''*'')')
     x     fname(1:40)

      open(10,file=fname)  
c
c     file format is 
c     line 1: number
c     line 2: Atoms  
c     line 3,... for each atom a type and three coords
c     this is repeated for as many frames
c
c     first count how many frames save line number of last
c
      nframe=0
10    continue
      read(10,'(a120)',end=50) line
      nframe=nframe+1
      read(line,*) nat
      if(nat.ne.nsites) then
        write(*,'('' Error:  xyz and lmp not matched '',/,
     x    '' Files have different number of sites:  N(xyz) = '',
     x    i6,''; N(lmp) = '',i6)') nat,nsites
        stop 'xyz file not matched with cooresponding lmp file'
      endif
      read(10,'(a120)') line   !line has "Atoms"
      do i=1,nat
        read(10,'(a120)') line
        read(line,*) it
        if(it.ne.itype(i)) then
          write(*,'('' Type mismatch between xyz and lmp files '')')
          write(*,'('' Atom number '',i6,
     x      '' Type(xyz) = '',i4,'' Type(lmp) = '',i4)') i,it,itype(i)
          stop 'xyz file and lmp file have different atoms types'
        endif
      enddo
      go to 10


50    continue
c     iframe is which frame to get
c     iframe=nframe
      write(*,'('' Selecting frame '',i5,'' from a total'',
     x          '' of '',i5)') iframe,nframe

      rewind(10)
c     position file to right place
      do i=1,(2+nsites)*(iframe-1)
        read(10,'(a120)') line
      enddo
c     now read into coord
      read(10,'(a120)') line
      read(10,'(a120)') line
      do i=1,nat
        read(10,'(a120)') line
        read(line,*) it,(coord(k,i),k=1,3)
      enddo
c

      close(10)



      end

c
c     subroutine to read in lmp parameter/coordinate file
c
      subroutine readlmp(fname,nsites,itype,charge,coord,atname,natmx,
     x                    nbonds,nbtp,bndpar,ijbond,nbnmx,nbtmx,
     x                    nangle,natp,angpar,ijkangle,nanmx,nantmx,
     x                    ndih,ndtp,dihpar,ijkldih,ndimx,nditmx,
     x                    natomtp,vdw,nattpmx,
     x                    xlo,xhi,ylo,yhi,zlo,zhi,fmass,
     x                    nametypes)
      implicit real*8 (a-h,o-z)
      character*120 line
      character*(*) fname
      dimension coord(3,natmx),charge(natmx),itype(natmx)
      character*20 atname(natmx)
      dimension ijbond(3,nbnmx),bndpar(2,nbtmx)
      dimension ijkangle(4,nanmx),angpar(2,nantmx)
      dimension ijkldih(5,ndimx),dihpar(4,nditmx)
      dimension vdw(4,nattpmx),fmass(nattpmx)
      character*(*) nametypes(nattpmx)

      write(*,'(/,'' Reading parameters from file *'',a,''*'')')
     x     fname(1:40)

      open(10,file=fname)  

c     read number of atoms and their coordinates
c     read nsites,itype,charge,coords,atname

10    continue
      read(10,'(a120)',end=21) line
      if(index(line,'atoms').eq.0) go to 10
      read(line,*) nsites
      write(*,'('' natoms = '',i8)') nsites
      if(nsites.gt.natmx) then
        write(*,'('' Increase size of site array; '',/,
     x            '' nsites = '',i10,'' natmx = '',i10)')
     x            nsites,natmx
        stop 'Increase natmx'
      endif
21    continue

      write(*,'('' Reading coordinates and charges for '',i7,
     x  '' sites '')') nsites
20    continue
      read(10,'(a120)',end=22) line
      if(index(line,'Atoms').eq.0) go to 20
      read(10,'(a120)') line
      totchg=0.d0
      do i=1,nsites
        read(10,'(a120)') line
        read(line,*) ix,im,it1,ch1,x1,y1,z1
c       read(line,'(i10,i5,i5)') ix,im,it1 
c       read(line(21:),*) ch1,x1,y1,z1
        if(ix.gt.nsites.or.ix.le.0) then
          write(*,'('' Error: site index out of range i = '',i10,
     x      '' ix = '',i10)') i,ix
          stop 'error reading coordinates'
        endif 
        itype(ix)=it1
        charge(ix)=ch1
        coord(1,ix)=x1
        coord(2,ix)=y1
        coord(3,ix)=z1
        if(mod(i,5000).eq.0) then
          write(*,'('' Reading coordinates for '',i6,'' of '',i6)') 
     x      i,nsites
        endif
        iloc=index(line,'#')
        if(iloc.ne.0) then
          read(line(iloc+1:),'(a20)') atname(ix)
        else
          write(atname(ix),'(''X'',i5.5)') ix
        endif
        totchg=totchg+charge(ix)
c       write(*,'('' IN ATOM LOOP: ix,im,it1,atname: '',3i5,
c    x    '' *'',a,''*'')')
c    x    ix,im,it1,atname(ix)
      enddo

      write(*,'('' Total charge '',f15.5)') totchg
22    continue
      
c     do i=1,nsites
c       write(*,'('' Site '',i6,'' *'',a,''* Type '',i5,3f10.5)')
c    x    i,atname(i)(1:20),itype(i)
c    x    ,(coord(k,i),k=1,3)
c     enddo

c
c     read box sizes
c
      rewind(10)
      ifxset=0
      ifyset=0
      ifzset=0
23    continue
      read(10,'(a120)') line
      isx=index(line,'xlo') 
      isy=index(line,'ylo') 
      isz=index(line,'zlo') 
      if(isx+isy+isz.eq.0) go to 23
      if(isx.ne.0) then 
        read(line,*) xlo,xhi
        ifxset=1
      elseif(isy.ne.0) then
        read(line,*) ylo,yhi
        ifyset=1
      elseif(isz.ne.0) then
        read(line,*) zlo,zhi
        ifzset=1
      endif
      if(ifxset*ifyset*ifzset.eq.0) go to 23
      write(*,'('' Box dimensions (X): '',2f15.8,/,
     x          '' Box dimensions (Y): '',2f15.8,/,
     x          '' Box dimensions (Z): '',2f15.8)')
     x  xlo,xhi,ylo,yhi,zlo,zhi


c     read bonds
c     read nbonds,nbtp,bndpar(2,*),ijbond(3,*)

      rewind(10)
30    continue
      read(10,'(a120)',end=40) line
      if(index(line,' bonds ').eq.0) go to 30
      read(line,*) nbonds
      if(nbonds.gt.nbnmx) then
        write(*,'('' Increase size of bond array; '',/,
     x            '' nbonds = '',i10,'' nbnmx = '',i10)')
     x            nbonds,nbnmx
        stop 'Increase nbnmx'
      endif
40    continue
      read(10,'(a120)',end=45) line
      if(index(line,' bond types ').eq.0) go to 40
      read(line,*) nbtp
c     write(*,'('' Nbond types = '',i6)') nbtp
      if(nbtp.gt.nbtmx) then
        write(*,'('' Increase size of bond type array; '',/,
     x            '' nbtp   = '',i10,'' nbtmx = '',i10)')
     x            nbtp,nbtmx
        stop 'Increase nbtmx'
      endif
45    continue
      read(10,'(a120)',end=50) line
      if(index(line,'Bond Coeffs').eq.0) go to 45
      read(10,'(a120)') line
      do i=1,nbtp  
        read(10,'(a120)') line
        read(line,*) ix,bndpar(1,i),bndpar(2,i)
c       write(*,'('' Bond type '',i3,'' Parm '',2f10.5)')
c    x     i,bndpar(1,i),bndpar(2,i)
      enddo
50    continue
      read(10,'(a120)',end=55) line
      if(index(line,'Bonds').eq.0) go to 50

      write(*,'('' Reading list of '',i7,
     x  '' bonds '')') nbonds
      read(10,'(a120)') line
      do i=1,nbonds
        read(10,'(a120)') line
        read(line,*) ix,ijbond(3,i),ijbond(1,i),ijbond(2,i)
        if(mod(i,10000).eq.0) then
          write(*,'('' Reading bonds '',i6,'' of '',i6)') 
     x      i,nbonds
        endif
c       write(*,'('' Bond '',i6,'' Type '',i4,'' Sites '',2i6)')
c    x    i,ijbond(3,i),ijbond(1,i),ijbond(2,i)
      enddo


c     read angles
c     nangle,natp,angpar(2,*),ijkangle(4,*)

      rewind(10)
55    continue
      read(10,'(a120)',end=60) line
      if(index(line,' angles').eq.0) go to 55
      read(line,*) nangle
      if(nangle.gt.nanmx) then
        write(*,'('' Increase size of angle array; '',/,
     x            '' nangle = '',i10,'' nanmx = '',i10)')
     x            nangle,nanmx
        stop 'Increase nanmx'
      endif
60    continue
      read(10,'(a120)',end=65) line
      if(index(line,' angle types ').eq.0) go to 60
      read(line,*) natp
      if(natp.gt.nantmx) then
        write(*,'('' Increase size of angle type array; '',/,
     x            '' natp   = '',i10,'' nantmx = '',i10)')
     x            natp,nantmx
        stop 'Increase nantmx'
      endif
65    continue
      read(10,'(a120)',end=70) line
      if(index(line,'Angle Coeffs').eq.0) go to 65
      read(10,'(a120)') line
      do i=1,natp  
        read(10,'(a120)') line
        read(line,*) ix,angpar(1,i),angpar(2,i)
      enddo
70    continue
      read(10,'(a120)',end=71) line
      if(index(line,'Angles').eq.0) go to 70
      write(*,'('' Reading list of '',i7,
     x  '' angles '')') nangle
      read(10,'(a120)') line
      do i=1,nangle
        read(10,'(a120)') line
        read(line,*) ix,ijkangle(4,i),(ijkangle(k,i),k=1,3)
      enddo
71    continue



c     read dihedrals
c     read ndih,ndtp,dihpar(4,*),ijkldih(5,*)

      rewind(10)
75    continue
      read(10,'(a120)',end=80) line
      if(index(line,' dihedrals').eq.0) go to 75
      read(line,*) ndih  
      if(ndih.gt.ndimx) then
        write(*,'('' Increase size of dihedral array; '',/,
     x            '' ndih   = '',i10,'' ndimx = '',i10)')
     x            ndih,ndimx
        stop 'Increase ndimx'
      endif
80    continue
      read(10,'(a120)',end=85) line
      if(index(line,' dihedral types ').eq.0) go to 80
      read(line,*) ndtp
c     write(*,'('' Dihedrals: '',i6,''  Types: '',i6)')
c    x   ndih,ndtp
      if(ndtp.gt.nditmx) then
        write(*,'('' Increase size of dihedral type array; '',/,
     x            '' ndtp   = '',i10,'' nditmx = '',i10)')
     x            ndtp,nditmx
        stop 'Increase nditmx'
      endif
85    continue
      read(10,'(a120)',end=90) line
      if(index(line,'Dihedral Coeffs').eq.0) go to 85
      read(10,'(a120)') line
      do i=1,ndtp  
        read(10,'(a120)') line
c       write(*,'('' Line: *'',a,''*'')') line(1:50)
        read(line,*) ix,dihpar(1,i),nd,dd,dihpar(4,i)
c       write(*,'('' '',i5,f10.4,i5,i5,f10.4)') 
c    x    is,dihpar(1,i),nd,dd,dihpar(4,i)
        dihpar(2,i)=nd         
        dihpar(3,i)=dd         
      enddo
c     write(*,'('' Types read in '')')
90    continue
      read(10,'(a120)',end=91) line
      if(index(line,'Dihedrals').eq.0) go to 90
      write(*,'('' Reading list of '',i7,
     x  '' dihedrals '')') ndih  
      read(10,'(a120)') line
      do i=1,ndih  
        read(10,'(a120)') line
        read(line,*) ix,ijkldih(5,i),(ijkldih(k,i),k=1,4)
      enddo
c     write(*,'('' Dihedrals read in '')')
91    continue


c     read vdw
c     read natomtp,vdw(4,*)

      rewind(10)
100   continue
      read(10,'(a120)',end=105) line
      if(index(line,' atom types ').eq.0) go to 100
      read(line,*) natomtp
      write(*,'('' Reading list of '',i7,
     x  '' vdw parameters '')') natomtp
      if(natomtp.gt.nattpmx) then
        write(*,'('' Increase size of vdw array; '',/,
     x            '' natomtp   = '',i10,'' nattpmx = '',i10)')
     x            natomtp,nattpmx
        stop 'Increase nattpmx'
      endif
105   continue
      read(10,'(a120)',end=107) line
      if(index(line,'Pair Coeffs').eq.0) go to 105
      read(10,'(a120)') line
      do i=1,natomtp  
        read(10,'(a120)') line
        read(line,*) ix,vdw(2,i),vdw(1,i),vdw(4,i),vdw(3,i)
c       write(*,'('' Atom type '',i3,'' vdw = '',2f10.5)')
c    x     i,vdw(1,i),vdw(2,i)
      enddo
107   continue

c
c     read masses 
c     note that we are assuming that extra information is on these
c     lines in files made by ffassign with type names
c
      rewind(10)
26    continue
      read(10,'(a120)') line
      if(index(line,'Masses').eq.0) go to 26
      write(*,'('' Reading list of '',i7,
     x  '' masses  '')') natomtp
      read(10,'(a120)') line
      do i=1,natomtp  
        read(10,'(a120)') line
        read(line,*) ix,fmass(i)
c       write(*,'('' Atom type '',i3,'' mass = '',f10.5)')
c    x     i,fmass(i)           

c     extract the name string: character*100 nametypes(100)
        nametypes(i)=' '
        is=index(line,'#')
        ks=1
        ie=1
        if(is.ne.0) then
c         in line(is+1:) look for LAST token         
c         this will be a nonblank string surrounded by blanks
          ie=0
          do js=len(line),is+1,-1
            if(line(js:js).ne.' ') then
              ie=js
              go to 210
            endif
          enddo
210       continue
          if(ie.ne.0) then
            ks=0
            do js=ie,is+1,-1
              if(line(js:js).eq.' ') then
                ks=js+1 
                go to 215
              endif
            enddo
          endif
215       continue
c         at this point the last token is in line(ks:ie) 
          nametypes(i)=line(ks:ie)
        endif
        write(*,'('' For atom type '',i3,'' Name *'',a,''*'')')
     x    i,nametypes(i)(1:ie-ks+1)
      enddo
c


200   continue

      close(10)


      end


c
c     subroutine to compute bond energy
c
      subroutine bengy(nsites,coord,nbonds,ijbond,nbtp,bndpar)
      implicit real*8 (a-h,o-z)
      dimension coord(3,nsites),ijbond(3,nbonds),bndpar(2,nbtp)

      ebond=0.d0
      do i=1,nbonds
        ia=ijbond(1,i)
        ja=ijbond(2,i)
        dist=dsqrt( (coord(1,ia)-coord(1,ja))**2
     x             +(coord(2,ia)-coord(2,ja))**2
     x             +(coord(3,ia)-coord(3,ja))**2 )
        it=ijbond(3,i)
        eij=bndpar(1,it) * (dist - bndpar(2,it))**2
        ebond=ebond+eij
        write(*,'('' Site '',i6,'' R= '',3f10.5,'' ; Site '',i5,
     x    '' R= '',3f10.5)')  ia,(coord(k,ia),k=1,3),
     x                        ja,(coord(k,ja),k=1,3)
        write(*,'('' Bond '',i5,'' Sites '',2i6,'' dist '',f10.5,
     x    '' Eij '',f10.5)')
     x    i,ia,ja,dist,eij
      enddo
      write(*,'('' E(bond) = '',f15.6)') ebond



      end

c
c     subroutine to compute angle energy
c
      subroutine aengy(nsites,coord,nangle,ijkangle,natp,angpar)
      implicit real*8 (a-h,o-z)
      dimension coord(3,nsites),ijkangle(4,nangle),angpar(2,natp)
      pi=4.d0*datan(1.d0)


      eang=0.d0
      do i=1,nangle
        ia=ijkangle(1,i)
        ja=ijkangle(2,i)
        ka=ijkangle(3,i)
        it=ijkangle(4,i)
        call angint(coord(1,ia),coord(1,ja),coord(1,ka),thet)
        eijk=angpar(1,it) * (thet - (pi/180.d0)*angpar(2,it))**2
c       write(*,'('' Angle index = '',i5,'' type = '',i5,
c    x   '' parms = '',2f10.3)')
c    x   i,it,angpar(1,it),angpar(2,it)
c       write(*,'('' Angle = '',f10.3,'' Engy = '',f10.3)')
c    x    thet*(180.d0/pi),eij
        eang =eang +eijk
      enddo
      write(*,'('' E(angl) = '',f15.6)') eang 

      end


c
c     subroutine to compute dihedral energy
c
      subroutine dengy(nsites,coord,ndih,ijkldih,ndtp,dihpar)
      implicit real*8 (a-h,o-z)
      dimension coord(3,nsites),ijkldih(5,ndih),dihpar(4,ndtp)
      pi=4.d0*datan(1.d0)
      edih=0.d0
      do i=1,ndih  
        ia=ijkldih(1,i)
        ja=ijkldih(2,i)
        ka=ijkldih(3,i)
        la=ijkldih(4,i)
        it=ijkldih(5,i)
        call dihint(1,coord(1,ia),coord(1,ja),
     x                coord(1,ka),coord(1,la),
     x              phi,dum1,dum2)
        eijkl=dihpar(1,it) * ( 1.d0 + 
     x      cos(dihpar(2,it)*phi - (pi/180.d0)*dihpar(3,it)))
        edih =edih +eijkl
c       write(*,'('' Term '',i6,'' phi = '',f10.5,
c    x    '' Parms = '',3f10.5,'' E(ijkl) = '',f12.5)')
c    x   i,phi,dihpar(1,it),dihpar(2,it),dihpar(3,it),eijkl
      enddo
      write(*,'('' E(dihe) = '',f15.6)') edih 

      end



c Change History:
c $Log: util.f,v $
c Revision 1.8  1994/08/03  19:49:54  swope
c Changed behavior of xyz2int so that zmatricies are not printed out tha
c undefined torsions.  When angles are close to 180 they are changed to
c when close to 0 they are changed to 0.0001.    (Swope Aug 3, 1994)
c
c Revision 1.6  1994/07/30  06:33:09  swope
c Several changes by Swope to internal-cartesian and cartesian-internal
c conversion routines:
c angint-switch to low angle expression on new threshold
c dihint-call tlkemsg when atoms too close
c xyz2int-change messages during certain errors
c i2x-changes in ways linear systems treated: two strikes policy
c     for generating cartesians with undefined torsions
c xyz2dih-added new return code to flag 1-2-3 linear vs 2-3-4 linear
c
c Revision 1.5  1994/07/07  16:34:02  gclie
c This version includes xyz2int and int2xyz from Swope.  It should repla
c
c*arc i2x
      subroutine i2x(nat,lnk,rab,ang,dih,istart,iend,xyz)
c
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Copyright (c) 1994 International Business Machines Corporation
C  All rights reserved
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Description      Given the link vector and internal coordinates
c                     of a list of particles, this subroutine computes
c                     the cartesian coordinates for a subset of the
c                     particles, those with indicies from istart to
c                     iend.  The units of the cartesian coordinates
c                     will be the same as those of the input internals.
c                     The units of the angles are assumed to be radians.
c                   If the particle indicies between istart and iend
c                     fall in the range 1,2 or 3, particle 1 is placed
c                     at the origin, particle 2 is placed on the x-axis,
c                     and particle 3 is placed in the x-y plane.
c
C
C  Input
C
C     nat           Number of atoms in list
C
C     lnk(3,nat)    link vector for describing internals
C                     lnk(1,i) gives label for distance from i
C                     lnk(2,i) gives label for angle i-lnk(1,i)-lnk(2,i)
C                     lnk(3,i) gives label for torsion
C                     i-lnk(1,i)-lnk(2,i)-lnk(3,i)
C                     Note that first six entries are ignored.
C
C     rab(nat)      Vector of distances.
C
C     ang(nat)      Vector of angles.  Range returned is 0.le.ang.le.pi.
C
C     dih(nat)      Vector of torsions. Range returned is
C                   -pi .lt. phi .le. pi, and follows IUPAC
C                   sign and range convention.
c
c     istart        The index of the first atom whose cartesian
c                   coordinates are to be computed.  istart must be
c                   an integer with ( 1 .le. istart .le. nat ).
c
c     iend          The index of the last atom whose cartesian
c                   coordinates are to be computed.  iend must be
c                   an integer with ( 1 .le. iend .le. nat ).
C
C
C  Output
C
C     xyz(3,i)      cartesian coordinates of atoms i=istart to iend
C
C
C  Linkage          List of subroutines called by this one
C
C  Programmer       William Swope (some from Carnevali and Nesbet)
C
C  Date             June 1994
C
C  Update log       Record of all updates including names of programmers
C                     descriptions of changes made, and dates of change.
C                   07-29-94 Modified by Swope to die (tlkemsg) if
C                            there are TWO invalid torsion angles.
C                            Invalid torsion angles are those that
C                            result in ill-defined cartesian coordinates
C                            This change still allows linear molecules
C                            and molecules with two long linear segments
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
c
      Implicit Real*8 (a, b, d-h, o, p, r-y)
      Implicit Integer (i-n)
c     Violation of ARCHEM conventions, but there are no logicals here
c     Implicit Logical (z)
      Implicit Real*8 (z)
      Implicit Character (c)
c     Implicit Undefined (q)
 
      character cproduct*48
      dimension cproduct(3)
 
c     Input and output arrays
      real*8 xyz(3,nat),rab(nat),ang(nat),dih(nat)
      integer lnk(3,nat)
 
c     Local arrays
c     Following arrays give molecule coordinate system relative to lab
      real*8 xhat(3),yhat(3),zhat(3),zprev(3)
      real*8 r12(3),r23(3),r43(3)
 
 
C     THE SIGN CONVENTION FOR PHI IS AS FOLLOWS:
C     NUMBER THE ASSOCIATED ATOMS 1, 2, 3, 4.  ATOMS 1 AND 4 ARE
C     "TERMINAL" ATOMS AND THE DIHEDRAL ANGLE IS ABOUT THE BOND
C     BETWEEN ATOMS 2 AND 3.  IMAGINE HOLDING THE PLANE FORMED BY
C     ATOMS 1, 2, AND 3 FIXED AND ROTATING THE PLANE FORMED BY
C     ATOMS 2, 3, AND 4 ABOUT A VECTOR THAT POINTS FROM ATOM 2
C     TO ATOM 3.  THE POSITIVE SENSE CORRESPONDS TO THE RIGHT-HAND
C     RULE FOR ROTATION ABOUT THE VECTOR FROM ATOM 2 TO ATOM 3.
c     This agrees with the IUPAC convention.
C
C     FURTHERMORE, PHI=0 CORRESPONDS TO A CONDITION WHERE ALL FOUR
C     ATOMS ARE ON A PLANE AND ATOMS 1 AND 4 ARE "CIS" TO THE BOND
C     BETWEEN ATOMS 2 AND 3.  LIKEWISE, PHI=180 DEGREES CORRESPONDS
C     A CONDITION WHERE ALL FOUR ATOMS ARE ON A PLANE AND ATOMS 1 AND
C     4 ARE "TRANS" TO THE BOND BETWEEN ATOMS 2 AND 3.
 
c     comment out the following call to check the integrity of the
c     link vector.  this call is appropriate in int2xyz, where the
c     full set of cartesians is computed, but should probably not
c     be made over and over again if the cartesians are computed
c     one at a time or in small increments.
c
c     check validity of link vector
c     call chklnk(nat,lnk,iret)
c     if(iret.ge.2) then
c       print *, ' Error: link matrix is not usable for determination',
c    >           ' of cartesian coordinates.'
c       print *, ' Complete link vector: '
c       write(*,'(3i10)') ((lnk(i,j),i=1,3),j=1,nat)
c       call tlkemsg(' Invalid link matrix.','int2xyz')
c     endif
 
 
c     lnk(1,i),i=1,nat specifies atom indicies for "bond" distances
c     lnk(2,i),i=1,nat specifies atom indicies for "bond" angles
c     lnk(3,i),i=1,nat specifies atom indicies for dihedral angles
 
      pi=4.d0*datan(1.d0)
 
      if(istart.le.0   .or.
     >   istart.gt.nat .or.
     >   iend.le.0     .or.
     >   iend.gt.nat   .or.
     >   istart.gt.iend ) then
      write(*,'('' Error in subroutine i2x: invalid arguments for'',/,
     >          '' atom indicies: istart='',i5,'' iend='',i5,/,
     >          '' Values should be from 1 to nat='',i5)')
     >          istart,iend,nat
C     CALL TLKEMSG(' INVALID ATOM INDICES PASSED TO X2I.','X2I')
      endif
 
      zprev(1)=0.d0
      zprev(2)=0.d0
      zprev(3)=1.d0
 
c     failure count on placing atoms when torsion undefined
      nstrike=0
 
 
      do 10 i=istart,iend
 
      if(i.eq.1) then
c     if first atom
        xyz(1,1)=0.d0
        xyz(2,1)=0.d0
        xyz(3,1)=0.d0
      elseif(i.eq.2) then
c     if second atom
        xyz(1,2)=rab(2)
        xyz(2,2)=0.d0
        xyz(3,2)=0.d0
      elseif(i.eq.3) then
c     if third atom (note that normally 0.le.ang.le.pi)
        if(lnk(1,3).eq.2) then
          xyz(1,3)=rab(2) -rab(3)*cos(ang(3))
        else
          xyz(1,3)=       +rab(3)*cos(ang(3))
        endif
        xyz(2,3)=         +rab(3)*sin(ang(3))
        xyz(3,3)=0.d0
        if(abs(ang(3)).gt.1.d-3 .and.
     >     abs(abs(ang(3))-pi).gt.1.d-3) nstrike=1
      endif
 
c     for fourth or higher atom
      if(i.le.3) go to 10
 
      l2=lnk(1,i)
      l3=lnk(2,i)
      l4=lnk(3,i)
      r23(1)=xyz(1,l2)-xyz(1,l3)
      r23(2)=xyz(2,l2)-xyz(2,l3)
      r23(3)=xyz(3,l2)-xyz(3,l3)
 
c     local x axis is directed from atom l3 to atom l2
      d=r23(1)**2+r23(2)**2+r23(3)**2
      if(d.le.1.d-10) then
        print *, ' Error: Atoms too close to establish unique',
     >           ' directions for cartesian placement from z-matrix.'
        print *, ' Atom indicies: ',l2,l3
C       CALL TLKEMSG(' ATOMS TOO CLOSE.','I2X')
      endif
      d=1.d0/sqrt(d)
      xhat(1)=r23(1)*d
      xhat(2)=r23(2)*d
      xhat(3)=r23(3)*d
 
c     local z axis is directed along r23 cross r43, if this is non-zero
      r43(1)=xyz(1,l4)-xyz(1,l3)
      r43(2)=xyz(2,l4)-xyz(2,l3)
      r43(3)=xyz(3,l4)-xyz(3,l3)
      zhat(1)=r23(2)*r43(3)-r23(3)*r43(2)
      zhat(2)=r23(3)*r43(1)-r23(1)*r43(3)
      zhat(3)=r23(1)*r43(2)-r23(2)*r43(1)
      d=zhat(1)**2+zhat(2)**2+zhat(3)**2
c
      if(d.le.1.d-8) then
        if(abs(ang(i)).gt.1.d-3 .and.
     >     abs(abs(ang(i))-pi).gt.1.d-3) then
c         Problem: angle 4-3-2 is (nearly) linear AND ang(i) is other
c         than zero or pi.  This means placement of particle 1 is
c         undefined.  The first time this happens, we will allow it,
c         and place particle 1 in the (local) x-y plane.  This
c         corresponds to setting zhat to the lab z axis.
          if(nstrike.ne.0) then
          print *
          print *, ' A sequence of bond angles near 180 (or 0) degrees'
          print *, ' has resulted in an undefined torsion angle and'
          print *, ' this torsion angle must be defined to determine'
          print *, ' unique Cartesian coordinates for the molecule.'
          print *
          print *, ' Offending link vector:'
          print *, ' lnk(1,',i,') = ',l2
          print *, ' lnk(2,',i,') = ',l3
          print *, ' lnk(3,',i,') = ',l4
          print *
C         CALL TLKEMSG('CANNOT GENERATE CARTESIAN COORDINATES.','I2X')
          endif
          nstrike=1
        endif
        zhat(1)=zprev(1)
        zhat(2)=zprev(2)
        zhat(3)=zprev(3)
      else
        d=1.d0/sqrt(d)
        zhat(1)=zhat(1)*d
        zhat(2)=zhat(2)*d
        zhat(3)=zhat(3)*d
        zprev(1)=zhat(1)
        zprev(2)=zhat(2)
        zprev(3)=zhat(3)
      endif
 
c     local y axis is zhat cross xhat
      yhat(1)=zhat(2)*xhat(3)-zhat(3)*xhat(2)
      yhat(2)=zhat(3)*xhat(1)-zhat(1)*xhat(3)
      yhat(3)=zhat(1)*xhat(2)-zhat(2)*xhat(1)
      d=1.d0/sqrt(yhat(1)**2+yhat(2)**2+yhat(3)**2)
      yhat(1)=yhat(1)*d
      yhat(2)=yhat(2)*d
      yhat(3)=yhat(3)*d
 
      if(abs(ang(i)).gt.1.d-3 .and.
     >   abs(abs(ang(i))-pi).gt.1.d-3) nstrike=1
 
c     in molecule frame (i.e., relative to xhat,yhat,zhat coord system)
c     new center is placed at ...
      x=-rab(i)*cos(ang(i))
      rho=rab(i)*sin(ang(i))
      y=rho*cos(dih(i))
      z=rho*sin(dih(i))
 
c     in lab frame ...
      xyz(1,i) = xyz(1,l2) + x*xhat(1)+y*yhat(1)+z*zhat(1)
      xyz(2,i) = xyz(2,l2) + x*xhat(2)+y*yhat(2)+z*zhat(2)
      xyz(3,i) = xyz(3,l2) + x*xhat(3)+y*yhat(3)+z*zhat(3)
 
10    continue
 
      end


c
c     angle calculation stolen from dihint
c
      subroutine angint(xyz1,xyz2,xyz3,ang)
      Implicit Real*8 (a-h,o-z)
c     Input and output arrays
      real*8 xyz1(3),xyz2(3),xyz3(3)
      real*8 a(3),b(3)

c     Compute a=r1-r2, b=r3-r2
      do 10 k=1,3
      a(k)=xyz1(k)-xyz2(k)
10    b(k)=xyz3(k)-xyz2(k)
 
c     Compute a . b 
      dot=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
 
c     Compute lengths
      b2=b(1)**2+b(2)**2+b(3)**2
      a2=a(1)**2+a(2)**2+a(3)**2
      angcos=dot/dsqrt(a2*b2)
      ang=dacos(angcos)

      end





c Change History:
c $Log$
c*arc dihint
      subroutine dihint(iop,xyz1,xyz2,xyz3,xyz4,
     >       phi,dphidx,d2phidx2)
c
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Copyright (c) 1994 International Business Machines Corporation
C  All rights reserved
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Description      Given the coordinates of four particles, this
C                     subroutine computes the torsion (dihedral)
C                     angle between them, the Jacobean (dphi/dx(i))
C                     and d2phi/dx(i)dx(j).
C
C  Input
C
C     iop           Control
C                   iop = 1 means just compute angle
C                   iop = 2 means compute angle and Jacobean
C                   iop = 3 means compute angle, Jacobean and second
C                           derivative
C
C     xyz1(3)       cartesian coordinates of first atom
C     xyz2(3)       cartesian coordinates of second atom
C     xyz3(3)       cartesian coordinates of third atom
C     xyz4(3)       cartesian coordinates of fourth atom
C
C
C  Output
C
C     phi           Angle between atoms 1-2-3-4.  Range returned is
C                   -pi .lt. phi .le. pi, following IUPAC
C                   sign and range convention.
C
C     dphidx(12)    Jacobean: dphi/dx(1), dphi/dy(1), ...
C
C     d2phid2(45)   Second derivative, d2phi/dx(i)dx(2), a symmetric
C                   matrix, stored lower triangle by row
C
C  Linkage          List of subroutines called by this one
C
C  Programmer       William Swope (some from Carnevali)
C
C  Date             June 1994
C
C  Update log       Record of all updates including names of programmers,
C                     descriptions of changes made, and dates of change.
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
c
      Implicit Real*8 (a, b, d-h, o, p, r-y)
      Implicit Integer (i-n)
c     Implicit Logical (z)
c     Violation of ARCHEM convention; but no logical data here
      Implicit Real*8 (z)
c     Implicit Character (c)
c     Violation of ARCHEM convention; but no character data here
      Implicit Real*8 (c)
c     Implicit Undefined (q)
 
      character cproduct*48
      dimension cproduct(3)
 
c     Input and output arrays
      real*8 xyz1(3),xyz2(3),xyz3(3),xyz4(3)
      real*8 dphidx(12),d2phidx2(78)
 
 
C     THE SIGN CONVENTION FOR PHI IS AS FOLLOWS:
C     NUMBER THE ASSOCIATED ATOMS 1, 2, 3, 4.  ATOMS 1 AND 4 ARE
C     "TERMINAL" ATOMS AND THE DIHEDRAL ANGLE IS ABOUT THE BOND
C     BETWEEN ATOMS 2 AND 3.  IMAGINE HOLDING THE PLANE FORMED BY
C     ATOMS 1, 2, AND 3 FIXED AND ROTATING THE PLANE FORMED BY
C     ATOMS 2, 3, AND 4 ABOUT A VECTOR THAT POINTS FROM ATOM 2
C     TO ATOM 3.  THE POSITIVE SENSE CORRESPONDS TO THE RIGHT-HAND
C     RULE FOR ROTATION ABOUT THE VECTOR FROM ATOM 2 TO ATOM 3.
c     This agrees with the IUPAC convention.
C
C     FURTHERMORE, PHI=0 CORRESPONDS TO A CONDITION WHERE ALL FOUR
C     ATOMS ARE ON A PLANE AND ATOMS 1 AND 4 ARE "CIS" TO THE BOND
C     BETWEEN ATOMS 2 AND 3.  LIKEWISE, PHI=180 DEGREES CORRESPONDS
C     A CONDITION WHERE ALL FOUR ATOMS ARE ON A PLANE AND ATOMS 1 AND
C     4 ARE "TRANS" TO THE BOND BETWEEN ATOMS 2 AND 3.
 
 
c     Local and scratch arrays
 
      dimension a(3),b(3),c(3),u(3),v(3),acrossc(3)
 
c     First derivatives of u**2, v**2 with respect to a, b, and c
      dimension du2da(3),du2db(3),dv2db(3),dv2dc(3)
 
c     First derivatives of D=u**2*v**2 and F=D**(-1/2) with
c     respect to a, b, and c
      dimension ddda(3),dddb(3),dddc(3)
      dimension dfda(3),dfdb(3),dfdc(3)
 
c     First derivatives of uv with respect to a, b, and c
      dimension duvda(3),duvdb(3),duvdc(3)
 
c     Derivatives of S with respect to a, b, and c
      dimension dsda(3),dsdb(3),dsdc(3)
 
c     Derivatives of cosphi and sinphi with respect to a, b, and c
      dimension dcosphida(3),dcosphidb(3),dcosphidc(3)
      dimension dsinphida(3),dsinphidb(3),dsinphidc(3)
 
c     Derivatives of phi with respect to a, b, and c
      dimension dphida(3),dphidb(3),dphidc(3)
 
c     Second derivatives
      dimension du2dada(3,3),du2dbdb(3,3),du2dadb(3,3)
      dimension dv2dbdb(3,3),dv2dbdc(3,3),dv2dcdc(3,3)
      dimension dddada(3,3),dddbdb(3,3),dddcdc(3,3)
      dimension dddadb(3,3),dddadc(3,3),dddbdc(3,3)
      dimension dfdada(3,3),dfdbdb(3,3),dfdcdc(3,3)
      dimension dfdadb(3,3),dfdadc(3,3),dfdbdc(3,3)
      dimension              duvdbdb(3,3)
      dimension dsdadb(3,3),dsdadc(3,3),dsdbdc(3,3)
      dimension              dsdbdb(3,3)
      dimension duvdadb(3,3),duvdadc(3,3),duvdbdc(3,3)
      dimension dcosphidada(3,3),dcosphidbdb(3,3),dcosphidcdc(3,3)
      dimension dcosphidadb(3,3),dcosphidadc(3,3),dcosphidbdc(3,3)
      dimension dsinphidada(3,3),dsinphidbdb(3,3),dsinphidcdc(3,3)
      dimension dsinphidadb(3,3),dsinphidadc(3,3),dsinphidbdc(3,3)
      dimension dphidada(3,3),dphidbdb(3,3),dphidcdc(3,3)
      dimension dphidadb(3,3),dphidadc(3,3),dphidbdc(3,3)
 
      dimension atilde(3,3),btilde(3,3),ctilde(3,3)
 
c     identity tensor here to simplify some loops
      real*8 delta(3,3)/1.d0,3*0.d0,1.d0,3*0.d0,1.d0/
 
      data cproduct /'$RCSfile$',
     $               '$Revision$$Date$',
     $               '$IBMCSA: PRODUCT VERSION NUMBER $'/
 
c     Statement function gives index of (i,j) in lower triang by row
      lowtri(i,j) = j + i*(i-1)/2
 
      if(iop.ne.1 .and. iop.ne.2 .and. iop.ne.3) then
        print *, ' Incorrect input to dihint subroutine: iop = ',iop
c       call arcdie()
      endif
 
      ierror=0
 
      if(iop.ge.1) then
c     compute angle
 
c     Compute a=r2-r1, b=r3-r2, c=r4-r3
      do 10 k=1,3
      a(k)=xyz2(k)-xyz1(k)
      b(k)=xyz3(k)-xyz2(k)
10    c(k)=xyz4(k)-xyz3(k)
 
c     Compute u = a x b , v = b x c, vectors normal to 123, 234 planes
      u(1)=a(2)*b(3)-a(3)*b(2)
      u(2)=a(3)*b(1)-a(1)*b(3)
      u(3)=a(1)*b(2)-a(2)*b(1)
      v(1)=b(2)*c(3)-b(3)*c(2)
      v(2)=b(3)*c(1)-b(1)*c(3)
      v(3)=b(1)*c(2)-b(2)*c(1)
 
c     Compute lengths and scalar products
      b2=b(1)**2+b(2)**2+b(3)**2
      u2=u(1)**2+u(2)**2+u(3)**2
      v2=v(1)**2+v(2)**2+v(3)**2
      if( b2.eq.0.d0 )then
c       this should be a hard error; print out message and die
c       if this happens there is something seriously wrong
        print *, ' Error:  Can not compute torsion angle when central',
     >           ' bond has zero length.'
c       call arcdie('dihint')
      endif
      b1=sqrt(b2)
      u1=sqrt(u2)
      v1=sqrt(v2)
      d=u2*v2
      d1=sqrt(d)
      uv=u(1)*v(1)+u(2)*v(2)+u(3)*v(3)
      av=a(1)*v(1)+a(2)*v(2)+a(3)*v(3)
      s=b1*av
      if( u2.eq.0.d0 .or. v2.eq.0.d0 )then
        print *, ' Error:  Three collinear atoms make torsion angle',
     >           ' undefined.'
        print *, ' Zero returned for angle, gradient and hessian.'
        ierror=1
        f=0.d0
        d=1.d0
        d1=1.d0
c       Compute cosphi and sinphi
        cosphi=1.d0
        sinphi=0.d0
      else
        f=1.d0/d1
c       Compute cosphi and sinphi
        cosphi=uv*f
        sinphi=s*f
      endif
 
c     Compute phi itself, produce range of -pi.lt.phi.le.pi
      phi=atan2(sinphi,cosphi)
 
      endif
 
      if(iop.ge.2) then
c     compute dphidx
 
c     Compute derivatives of u**2 and v**2 with respect to a, b, and c.
      a2=a(1)**2+a(2)**2+a(3)**2
      c2=c(1)**2+c(2)**2+c(3)**2
      ab=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
      bc=b(1)*c(1)+b(2)*c(2)+b(3)*c(3)
      ac=a(1)*c(1)+a(2)*c(2)+a(3)*c(3)
      do 20 k=1,3
      du2da(k)=2.d0*(b2*a(k)-ab*b(k))
      du2db(k)=2.d0*(a2*b(k)-ab*a(k))
      dv2db(k)=2.d0*(c2*b(k)-bc*c(k))
20    dv2dc(k)=2.d0*(b2*c(k)-bc*b(k))
 
c     Compute derivatives of D=u**2*v**2 with respect to a, b, and c.
      do 35 k=1,3
      ddda(k)=v2*du2da(k)
      dddb(k)=v2*du2db(k)+u2*dv2db(k)
35    dddc(k)=u2*dv2dc(k)
 
c     Compute derivatives of F=D**(-1/2) with respect to a, b, and c.
      do 40 k=1,3
      dfda(k)=-0.5d0*ddda(k)/(d*d1)
      dfdb(k)=-0.5d0*dddb(k)/(d*d1)
40    dfdc(k)=-0.5d0*dddc(k)/(d*d1)
 
 
 
      if(abs(sinphi).gt.abs(cosphi))then
c     if(.false.)then
c*************************************************
c     CASE A - use expressions derived from cosphi
c     Compute derivatives of u*v=C with respect to a, b, and c.
      do 50 k=1,3
      duvda(k)=b(k)*bc-c(k)*b2
      duvdb(k)=a(k)*bc+c(k)*ab-2.d0*b(k)*ac
50    duvdc(k)=b(k)*ab-a(k)*b2
c     Compute derivatives of cosphi=(u*v)*F=C*F with respect to a, b, and c.
      do 60 k=1,3
      dcosphida(k)=duvda(k)*f+uv*dfda(k)
      dcosphidb(k)=duvdb(k)*f+uv*dfdb(k)
60    dcosphidc(k)=duvdc(k)*f+uv*dfdc(k)
c     Compute derivatives of phi with respect to a, b, and c.
      do 70 k=1,3
      dphida(k)=-dcosphida(k)/sinphi
      dphidb(k)=-dcosphidb(k)/sinphi
70    dphidc(k)=-dcosphidc(k)/sinphi
c*************************************************
      else
c*************************************************
c     CASE B - use expressions derived from sinphi
      acrossc(1)=a(2)*c(3)-a(3)*c(2)
      acrossc(2)=a(3)*c(1)-a(1)*c(3)
      acrossc(3)=a(1)*c(2)-a(2)*c(1)
      s=b1*av
c     Compute derivatives of S=b(a*v) with respect to a, b, and c.
      do 85 k=1,3
      dsda(k)=b1*v(k)
      dsdb(k)=b(k)*av/b1-b1*acrossc(k)
85    dsdc(k)=b1*u(k)
c     Compute derivatives of sinphi with respect to a, b, and c.
      do 90 k=1,3
      dsinphida(k)=dsda(k)*f+s*dfda(k)
      dsinphidb(k)=dsdb(k)*f+s*dfdb(k)
90    dsinphidc(k)=dsdc(k)*f+s*dfdc(k)
c     Compute derivatives of phi with respect to a, b, and c.
      do 95 k=1,3
      dphida(k)=dsinphida(k)/cosphi
      dphidb(k)=dsinphidb(k)/cosphi
95    dphidc(k)=dsinphidc(k)/cosphi
c*************************************************
 
      endif
 
c     Add contribution to dphi/dx array
      do 80 k=1,3
      dphidx(k  )=          -dphida(k)
      dphidx(3+k)= dphida(k)-dphidb(k)
      dphidx(6+k)= dphidb(k)-dphidc(k)
80    dphidx(9+k)= dphidc(k)
 
c     This code is for three atoms collinear; torsion undefined
      if(ierror.eq.1) then
        do 81 k=1,12
81      dphidx(k)=0.d0
      endif
 
      endif
 
      if(iop.ge.3) then
 
c     Compute second derivatives of u**2 and v**2 with respect to a, b, and c.
      do 110 i=1,3
      do 110 j=1,3
      du2dada(i,j)=2.d0*(b2*delta(i,j)-b(i)*b(j))
      du2dbdb(i,j)=2.d0*(a2*delta(i,j)-a(i)*a(j))
      du2dadb(i,j)=2.d0*(2.d0*a(i)*b(j)-b(i)*a(j)-ab*delta(i,j))
      dv2dbdb(i,j)=2.d0*(c2*delta(i,j)-c(i)*c(j))
      dv2dcdc(i,j)=2.d0*(b2*delta(i,j)-b(i)*b(j))
      dv2dbdc(i,j)=2.d0*(2.d0*b(i)*c(j)-c(i)*b(j)-bc*delta(i,j))
110   continue
 
c     Compute second derivatives of D=u**2*v**2 with respect to a, b, and c.
      do 120 i=1,3
      do 120 j=1,3
      dddada(i,j)=
     >            v2*du2dada(i,j)
      dddbdb(i,j)=dv2db(i)*du2db(j)+du2db(i)*dv2db(j)
     >           +v2*du2dbdb(i,j)  +u2*dv2dbdb(i,j)
      dddcdc(i,j)=
     >                             +u2*dv2dcdc(i,j)
      dddadb(i,j)=                 +du2da(i)*dv2db(j)
     >           +v2*du2dadb(i,j)
      dddadc(i,j)=                 +du2da(i)*dv2dc(j)
     >
      dddbdc(i,j)=                 +du2db(i)*dv2dc(j)
     >                             +u2*dv2dbdc(i,j)
120   continue
 
c     Compute second derivatives of F=D**(-1/2) with respect to a, b, and c.
      f3=f/d
      do 130 i=1,3
      do 130 j=1,3
      dfdada(i,j)=.5d0*f3*(1.5d0*ddda(i)*ddda(j)/d-dddada(i,j))
      dfdbdb(i,j)=.5d0*f3*(1.5d0*dddb(i)*dddb(j)/d-dddbdb(i,j))
      dfdcdc(i,j)=.5d0*f3*(1.5d0*dddc(i)*dddc(j)/d-dddcdc(i,j))
      dfdadb(i,j)=.5d0*f3*(1.5d0*ddda(i)*dddb(j)/d-dddadb(i,j))
      dfdadc(i,j)=.5d0*f3*(1.5d0*ddda(i)*dddc(j)/d-dddadc(i,j))
      dfdbdc(i,j)=.5d0*f3*(1.5d0*dddb(i)*dddc(j)/d-dddbdc(i,j))
130   continue
 
      if(abs(sinphi).gt.abs(cosphi))then
c     if(.false.)then
c*************************************************
c     CASE A - use expressions derived from cosphi
c     Compute second derivatives of u*v=C with respect to a, b, and c.
      do 140 i=1,3
      do 140 j=1,3
      duvdbdb(i,j)=a(i)*c(j)+c(i)*a(j)-2.d0*ac*delta(i,j)
      duvdadb(i,j)=bc*delta(i,j)+b(i)*c(j)-2.d0*c(i)*b(j)
      duvdadc(i,j)=b(i)*b(j)-b2*delta(i,j)
      duvdbdc(i,j)=ab*delta(i,j)+a(i)*b(j)-2.d0*b(i)*a(j)
140   continue
c     Compute second derivatives of cosphi=(u*v)*F=C*F with respect to a, b, and
c.
      do 150 i=1,3
      do 150 j=1,3
      dcosphidada(i,j)=
     >                +dfda(i)*duvda(j)+duvda(i)*dfda(j)+uv*dfdada(i,j)
      dcosphidbdb(i,j)=
     >  duvdbdb(i,j)*f+dfdb(i)*duvdb(j)+duvdb(i)*dfdb(j)+uv*dfdbdb(i,j)
      dcosphidcdc(i,j)=
     >                +dfdc(i)*duvdc(j)+duvdc(i)*dfdc(j)+uv*dfdcdc(i,j)
      dcosphidadb(i,j)=
     >  duvdadb(i,j)*f+dfda(i)*duvdb(j)+duvda(i)*dfdb(j)+uv*dfdadb(i,j)
      dcosphidadc(i,j)=
     >  duvdadc(i,j)*f+dfda(i)*duvdc(j)+duvda(i)*dfdc(j)+uv*dfdadc(i,j)
      dcosphidbdc(i,j)=
     >  duvdbdc(i,j)*f+dfdb(i)*duvdc(j)+duvdb(i)*dfdc(j)+uv*dfdbdc(i,j)
150   continue
c     Compute second derivatives of phi with respect to a, b, and c.
      z1=-cosphi/(sinphi**3)
      z2=-1.d0/sinphi
      do 160 i=1,3
      do 160 j=1,3
      dphidada(i,j)=z1*dcosphida(i)*dcosphida(j)+z2*dcosphidada(i,j)
      dphidbdb(i,j)=z1*dcosphidb(i)*dcosphidb(j)+z2*dcosphidbdb(i,j)
      dphidcdc(i,j)=z1*dcosphidc(i)*dcosphidc(j)+z2*dcosphidcdc(i,j)
      dphidadb(i,j)=z1*dcosphida(i)*dcosphidb(j)+z2*dcosphidadb(i,j)
c     note dcosphidadc is non zero but dphidadc is zero - don't compute
      dphidadc(i,j)=z1*dcosphida(i)*dcosphidc(j)+z2*dcosphidadc(i,j)
      dphidbdc(i,j)=z1*dcosphidb(i)*dcosphidc(j)+z2*dcosphidbdc(i,j)
160   continue
      else
c*************************************************
c     CASE B - use expressions derived from sinphi
c     Compute the product of the antisymmetric tensor times a, b, and c
      call antsym(a,atilde)
      call antsym(b,btilde)
      call antsym(c,ctilde)
c     Compute second derivatives of S=b(a*v) with respect to a, b, and c.
      b3=b1*b2
      do 210 i=1,3
      do 210 j=1,3
      dsdbdb(i,j)=(delta(i,j)/b1-b(i)*b(j)/b3)*av
     >            -(acrossc(i)*b(j)+acrossc(j)*b(i))/b1
      dsdadb(i,j)=v(i)*b(j)/b1+b1*ctilde(i,j)
      dsdadc(i,j)=-b1*btilde(i,j)
      dsdbdc(i,j)=b(i)*u(j)/b1+b1*atilde(i,j)
210   continue
c     Compute second derivatives of sinphi with respect to a, b, and c.
      do 220 i=1,3
      do 220 j=1,3
      dsinphidada(i,j)=
     >                +dfda(i)*dsda(j)+dsda(i)*dfda(j)+s*dfdada(i,j)
      dsinphidbdb(i,j)=
     >  dsdbdb(i,j)*f+dfdb(i)*dsdb(j)+dsdb(i)*dfdb(j)+s*dfdbdb(i,j)
      dsinphidcdc(i,j)=
     >                +dfdc(i)*dsdc(j)+dsdc(i)*dfdc(j)+s*dfdcdc(i,j)
      dsinphidadb(i,j)=
     >  dsdadb(i,j)*f+dfda(i)*dsdb(j)+dsda(i)*dfdb(j)+s*dfdadb(i,j)
      dsinphidadc(i,j)=
     >  dsdadc(i,j)*f+dfda(i)*dsdc(j)+dsda(i)*dfdc(j)+s*dfdadc(i,j)
      dsinphidbdc(i,j)=
     >  dsdbdc(i,j)*f+dfdb(i)*dsdc(j)+dsdb(i)*dfdc(j)+s*dfdbdc(i,j)
220   continue
c     Compute second derivatives of phi with respect to a, b, and c.
      z1=sinphi/cosphi**3
      z2=1.d0/cosphi
      do 230 i=1,3
      do 230 j=1,3
      dphidada(i,j)=z1*dsinphida(i)*dsinphida(j)+z2*dsinphidada(i,j)
      dphidbdb(i,j)=z1*dsinphidb(i)*dsinphidb(j)+z2*dsinphidbdb(i,j)
      dphidcdc(i,j)=z1*dsinphidc(i)*dsinphidc(j)+z2*dsinphidcdc(i,j)
      dphidadb(i,j)=z1*dsinphida(i)*dsinphidb(j)+z2*dsinphidadb(i,j)
c     note dsinphidadc is non zero but dphidadc is zero - don't compute
      dphidadc(i,j)=z1*dsinphida(i)*dsinphidc(j)+z2*dsinphidadc(i,j)
      dphidbdc(i,j)=z1*dsinphidb(i)*dsinphidc(j)+z2*dsinphidbdc(i,j)
230   continue
      end if
 
c     Add contributions to second derivative matrix
c     Derivative with respect to r1,r1:
      do 21 i=1,3
      do 21 j=1,i
      index=lowtri(i,j)
      d2phidx2(index)=dphidada(i,j)
21    continue
c     Derivative with respect to r2,r2:
      do 22 i=1,3
      do 22 j=1,i
      index=lowtri(i+3,j+3)
      d2phidx2(index)=dphidada(i,j)+dphidbdb(i,j)
     >               -dphidadb(i,j)-dphidadb(j,i)
22    continue
c     Derivative with respect to r3,r3:
      do 23 i=1,3
      do 23 j=1,i
      index=lowtri(i+6,j+6)
      d2phidx2(index)=dphidbdb(i,j)+dphidcdc(i,j)
     >               -dphidbdc(i,j)-dphidbdc(j,i)
23    continue
c     Derivative with respect to r4,r4:
      do 24 i=1,3
      do 24 j=1,i
      index=lowtri(i+9,j+9)
      d2phidx2(index)=dphidcdc(i,j)
24    continue
c     Derivative with respect to r1,r2:
      do 25 i=1,3
      do 25 j=1,3
      index=lowtri(j+3,i)
      d2phidx2(index)=-dphidada(i,j)+dphidadb(i,j)
25    continue
c     Derivative with respect to r1,r3:
      do 26 i=1,3
      do 26 j=1,3
      index=lowtri(j+6,i)
      d2phidx2(index)=-dphidadb(i,j)+dphidadc(i,j)
26    continue
c     Derivative with respect to r1,r4:
      do 27 i=1,3
      do 27 j=1,3
      index=lowtri(j+9,i)
      d2phidx2(index)=-dphidadc(i,j)
c
c     note: dphidadc is always zero - this array and its computation
c     should be removed later
c
c      write(*,887) i,j,index,dphidadc(i,j)
c887   format(' r1,r4 block: i,j=',2i2,' index=',i4,' dphidadc=',f10.4)
27    continue
c     Derivative with respect to r2,r3:
      do 28 i=1,3
      do 28 j=1,3
      index=lowtri(j+6,i+3)
      d2phidx2(index)=
     >               +dphidadb(i,j)-dphidadc(i,j)
     >               -dphidbdb(i,j)+dphidbdc(i,j)
28    continue
c     Derivative with respect to r2,r4:
      do 29 i=1,3
      do 29 j=1,3
      index=lowtri(j+9,i+3)
      d2phidx2(index)=dphidadc(i,j)-dphidbdc(i,j)
29    continue
c     Derivative with respect to r3,r4:
      do 30 i=1,3
      do 30 j=1,3
      index=lowtri(j+9,i+6)
      d2phidx2(index)=dphidbdc(i,j)-dphidcdc(i,j)
30    continue
 
c     code for three atoms colinear
      if(ierror.eq.1) then
        do 45 i=1,78
45      d2phidx2(i)=0.d0
      endif
 
      endif
 
 
      end
c
c     Subroutine to compute the product of the antysimmetric
c     tensor times a vector. The result is an antisymmetric tensor.
      subroutine antsym(x,xtilde)
      implicit real*8 (a-h,o-z)
      dimension x(3),xtilde(3,3)
      xtilde(1,1)=0.d0
      xtilde(2,2)=0.d0
      xtilde(3,3)=0.d0
      xtilde(1,2)=x(3)
      xtilde(2,3)=x(1)
      xtilde(3,1)=x(2)
      xtilde(2,1)=-x(3)
      xtilde(3,2)=-x(1)
      xtilde(1,3)=-x(2)
      return
      end








c
c     subroutine to write out solvent information files for lammps simulation
c
      subroutine wrtlmps1(nwat,coord,xlow,xhigh)
      implicit real*8 (a-h,o-z)
      dimension coord(3,*)
      dimension fmass(2),vdw(2,2),bondpar(2,2),anglepar(2,2),charge(2)


      oxmass=15.9994d0
      hymass=1.008d0
      fmass(1)=oxmass
      fmass(2)=hymass
      vdw(1,1)=0.16275d0  !epsilon o-o
      vdw(2,1)=3.16435d0  !sigma   o-o
      vdw(1,2)=0.0d0      !epsilon h-h
      vdw(2,2)=0.0d0      !sigma   h-h
      bondpar(1,1)=250.d0 !spring constant (doesn't matter)
      bondpar(2,1)=0.9572d0 !OH bond length (constrained by shake)
      anglepar(1,1)=250.d0 !spring constant (doesn't matter)
      anglepar(2,1)=104.52d0 !HOH angle (constrained)
      charge(1)=-1.0484d0  !O charge TI4P-Ew
      charge(2)= 0.5242d0  !H charge TI4P-Ew
      
c     TIP4P-Ew
      nsites=3*nwat 
      nbonds=2*nwat
      nangles=nwat
      ndihed=0
      nadded=0


      write(15,'('' Header line '')')
      write(15,*)
      write(15,'(5x,i10,'' atoms '')') nsites
      write(15,'(5x,i10,'' bonds '')') nbonds
      write(15,'(5x,i10,'' angles '')') nangles
      write(15,'(5x,i10,'' dihedrals '')') ndihed + nadded

c     TIP4P-Ew
      nattp=2
      nbntp=1
      nantp=1
      nditp=0

      write(15,*)
      write(15,'(5x,i10,'' atom types '')') nattp
      write(15,'(5x,i10,'' bond types '')') nbntp
      write(15,'(5x,i10,'' angle types '')') nantp
      write(15,'(5x,i10,'' dihedral types '')') nditp


c     cell edge is xlow to xhigh
      write(15,*)
      write(15,'(5x,2f15.8,'' xlo xhi '')') xlow,xhigh
      write(15,'(5x,2f15.8,'' ylo yhi '')') xlow,xhigh
      write(15,'(5x,2f15.8,'' zlo zhi '')') xlow,xhigh

c     TIP4P-Ew
      write(15,*)
      write(15,'('' Masses  '')')
      write(15,*)
      write(15,'(i10,f15.8,''    # O - TIP4P-Ew'')') 1,fmass(1) 
      write(15,'(i10,f15.8,''    # H - TIP4P-Ew'')') 2,fmass(2) 


c     TIP4P-Ew
      write(15,*)
      write(15,'('' Pair Coeffs'')')
      write(15,*)
      write(15,'(i10,4f15.8,''   # O - TIP4P-Ew'')') 1,
     x    vdw(1,1),vdw(2,1), !epsilon,sigma
     x    vdw(1,1),vdw(2,1)  !epsilon,sigma
      write(15,'(i10,4f15.8,''   # H - TIP4P-Ew'')') 2,
     x    vdw(1,2),vdw(2,2), !epsilon,sigma
     x    vdw(1,2),vdw(2,2)  !epsilon,sigma


      write(15,*)
      write(15,'('' Bond Coeffs'')')
      write(15,*)
      write(15,'(i10,2f15.8,
     x  ''    # OH bond TIP4P-Ew'')') 1,
     x  bondpar(1,1),bondpar(2,1)


      write(15,*)
      write(15,'('' Angle Coeffs'')')
      write(15,*)
      write(15,'(i10,2f15.8,
     x  ''    # HOH angle TIP4P-Ew'')') 1,
     x  anglepar(1,1),anglepar(2,1)


c     write(15,*)
c     write(15,'('' Dihedral Coeffs'')')
c     write(15,*)


      write(15,*)
      write(15,'('' Atoms '')')
      write(15,*)
      do i=1,nwat
c     fix up the coordinates by removing the fictional sites
c     this assumes the sites are ordered O,H,H,Q
        iat=4*(i-1)
        write(15,'(i10,i10,i5,f10.5,3f15.8,''    # O '')') 
     x    3*(i-1)+1,i,1,charge(1),(coord(k,iat+1),k=1,3)
        write(15,'(i10,i10,i5,f10.5,3f15.8,''    # H1'')') 
     x    3*(i-1)+2,i,2,charge(2),(coord(k,iat+2),k=1,3)
        write(15,'(i10,i10,i5,f10.5,3f15.8,''    # H2'')') 
     x    3*(i-1)+3,i,2,charge(2),(coord(k,iat+3),k=1,3)
      enddo


      write(15,*)
      write(15,'('' Velocities '')')
      write(15,*)
      do i=1,nsites
        write(15,'(i10,5x,3f10.5)') i,0.,0.,0.
      enddo


      write(15,*)
      write(15,'('' Bonds  '')')
      write(15,*)
      do i=1,nbonds,2
        iwat=1+(i-1)/2
        ij1=3*(iwat-1)+1
        write(15,'(i10,3i10)') i,  1,ij1,ij1+1
        write(15,'(i10,3i10)') i+1,1,ij1,ij1+2
      enddo


      write(15,*)
      write(15,'('' Angles '')')
      write(15,*)
      do i=1,nangles
        iwat=i
        iat=3*(iwat-1)+2
        jat=3*(iwat-1)+1
        kat=3*(iwat-1)+3
        write(15,'(i10,4i10)') i,1,iat,jat,kat
      enddo


c     write(15,*)
c     write(15,'('' Dihedrals '')')
c     write(15,*)
c     do i=1,ndihed +nadded
c       write(15,'(i10,5i10)') i,ijkldih(6,i),
c    x    ijkldih(1,i),ijkldih(2,i),ijkldih(3,i),ijkldih(4,i)
c     enddo

      write(15,*)

      close(15)


      end


c
c     left justify a string
c
      subroutine leftjust(string)
      character*(*) string

      is=0
      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 10
        endif
      enddo

10    continue
      if(is.eq.0) return  !all blank string

      do i=1,len(string)-is+1
        string(i:i)=string(is+i-1:is+i-1)
      enddo
c     blank out the last is-1 characters
      do i=len(string)-is+2,len(string)
        string(i:i)=' '
      enddo

      end


c
c     subroutine lstchr returns the index of the last nonblank character in a string
c
      subroutine lstchr(string,ilst)
      character*(*) string 

      ilst=0  !if string is all blank, return zero
      do i=len(string),1,-1
        if(string(i:i).ne.' ') then
          ilst=i
          go to 10
        endif
      enddo
10    continue

      end



c
c     subroutine to parse a string into an element field and up to 3 integers
c     unused fields will be returned with values of -1
c
      subroutine parser(string,elfield,lelfield,i1,i2,i3)
      character*(*) string,elfield
      character*120 line
      dimension is(3)
      do i=1,3
        is(i)=-1
      enddo
      line=string
      call leftjust(line)
      ne=2 !assume two character element name
      if(line(2:2).eq.' '.or.line(2:2).eq.'_') ne=1
      lelfield=ne
      elfield=line(1:ne)

      do i=1,3
        if(line(ne+1:ne+1).eq.'_') then
c         assume there is a number field after the underscore
c         look for next space or underscore
          l=index(line(ne+2:),'_')
          if(l.gt.0) then
            read(line(ne+2:ne+l),*) is(i)
            ne=ne+l
          else
            read(line(ne+2:),*) is(i)
            go to 10
          endif
        endif
      enddo

10    continue

      i1=is(1)
      i2=is(2)
      i3=is(3)

      end

c
c     function to get atomic number given a string
c     holding the element name
c


      integer function igetatnum(string)
      character*(*) string
      character*26 upper,lower
      character*80 elements
      character*2 ename
      
      upper='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
      lower='abcdefghijklmnopqrstuvwxyz'
      elements(1:20)= 'H HELIBEB C N O F NE' 
      elements(21:40)='NAMGALSIP S CLARK CA'
      elements(41:60)='SCTIV CRMNFECONICUZN'
      elements(61:80)='GAGEASSEBRKRRBSRY ZR'

      igetatnum=o

      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 100
        endif
      enddo
100   continue
      ie=len(string)
      do i=is+1,len(string)
        if(string(i:i).eq.'_'.or.string(i:i).eq.' ') then
          ie=i-1
          go to 200
        endif
      enddo
200   continue

      ename(1:2)=string(is:ie)  !pads with blanks
c     write(*,'('' ENAME =*'',a2,''*'')') ename

c     element string is in string(is:ie)
      do i=1,2  
        ix=index(lower,ename(i:i))
        if(ix.ne.0) ename(i:i)=upper(ix:ix)
      enddo
c     write(*,'('' ENAME =*'',a2,''*'')') ename

      ix=index(elements,ename(1:2))
      if(ix.eq.0) then
        write(*,'('' Element not found: '',a2)') ename
        stop 'ERROR:  missing element in igetatnum'
      endif
    
      igetatnum=1+(ix-1)/2

      end
c
c     subroutine to populate the atomic masses array
c
      subroutine amassinit(amass,n)
      implicit real*8 (a-h,o-z)
      dimension amass(n)

c     atomic masses as needed
      do i=1,120
        amass(i)=0.d0
      enddo

      amass(1)=1.00794d0   !agrees w/Mulliken
      amass(2)=4.002602d0
      amass(3)=6.941d0
      amass(4)=9.012182d0
      amass(5)=10.811d0

      amass(6)=12.0107d0
      amass(6)=12.0110d0    !agrees w/Mulliken
      amass(7)=14.0067d0
      amass(8)=15.9994d0    !agrees w/Mulliken
      amass(9)=18.9984032d0
      amass(10)=20.1797d0

      amass(11)=22.98976928d0
      amass(12)=24.305d0
      amass(13)=26.9815386d0
      amass(14)=28.0855d0
      amass(15)=30.973762d0

      amass(16)=32.065d0
      amass(17)=35.453d0
      amass(18)=39.948d0
      amass(19)=39.0983d0
      amass(20)=40.078d0

      amass(21)=44.955912d0
      amass(22)=47.867d0
      amass(23)=50.9415d0
      amass(24)=51.9961d0
      amass(25)=54.938045d0

      amass(26)=55.845d0
      amass(27)=58.933195d0
      amass(28)=58.6934d0
      amass(29)=63.546d0
      amass(30)=65.38d0

      amass(31)=69.723d0
      amass(32)=72.64d0
      amass(33)=74.9216d0
      amass(34)=78.96d0
      amass(35)=79.904d0
      amass(36)=83.798d0

      end 
