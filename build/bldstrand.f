c
c     program to build polymers from monomers
c     linear chains, diblocks, stars and dendrimers
c
c     written by Bill Swope, IBM Almaden Research Center, 2011
c
c     concept is that this program generates (1) coordinate files
c     consistent with some torsion angle distribution, and 
c     (2) a topology file that describes the bonds, linkages, 
c     branch points, etc.   A separate program will take the
c     topology file and assign complete force field parameters 
c     using an all atom or united atom; a particular charge model.  
c     Each force field uses potentially different atom typing schemes, 
c     so this this information is excluded from the coordinate and 
c     topology files.
c
c
c     possible things to add:
c
c     write out a zmatrix version of the coordinates (build as 
c     monomeric units are added) this could be another flavor of 
c     coordinate file (... e.g., *.pdb *.crd *.geo *.zmat)
c
c     write out a topology file to include
c     site list; monomer list; bond list; angle list; torsion list
c     linker bond list; linker angle and linker torsion list (terms that 
c     might not be in the monomer force field description)
c     branch or node sites - where a strand links to some place other 
c     than the previous monomer
c     
c     added notes on force field assignment
c     should have parameters extracted from an mmol-like file
c     if we have a dimer of monomers (like we have with DME as a model 
c     for two repeat units of PEG) we have all the monomer AND the 
c     linkage potential parameters in the mmol description since
c     the internal connections look just like the inter-unit linkages
c     However, for connections between units of different types, we 
c     need to have (force) the user to specify what to do.  Would 
c     make sense, initially, to force the user to specify linkage 
c     parameters for EVERY type of connections seen in the polymer, 
c     even ones between like units that might already be in the monomer 
c     description.
c
c     note that charges will have to be adjusted relative to monomers 
c     since the repeat unit must have zero net charge.  This also means 
c     that any capping groups will also have to have zero net charge.
c

c
c     things to fix:  ref site code
c
c     add polystyrene (PS)  random, all one side
c     polyethylene (PE)
c     PLA d and l forms for stereo pairs
c
c
c     12/2013 added capability to identify the arm index for each repeat unit
c     in the integer array idarm
c
c
c
c
c
      implicit real*8 (a-h,o-z)

      character*120 molname
      character*1024 libpath   !path to library of zmatrix files
      dimension zmonomer(100,3),izmonomer(100,3)
      character*10 msites(100) !site names in the monomeric unit
      dimension lstbnd(100,2,6)

      character*20 atname(20000)
      dimension coord(3,20000),lbonds(20000,2,6)
      character*10 mname(0:20000) !names of each monomeric unit (e.g., DME)
      dimension neworder(2,20000)  !arrays to facilitate reordering
      character*20 newatname(20000)  !to put all sites contiguous for a given monomer

      dimension idarm(20000)    !actually only needs to be number of residues
      character*120 line,fileset
      character*120 string1  
      dimension refsite(3,3),ref(3,3),iref(3),dumsite(3,5)
   
      nzatmax=100  !size of arrays for sites within a monomer
      natmax=20000  !size of arrays for all sites in system

      iseed=127777
      ifaddh=1 !set to 1 for addition of hydrogen sites


      do j=1,6
        do i=1,natmax
          lbonds(i,1,j)=0
          lbonds(i,2,j)=0
        enddo
      enddo

      write(*,'(//,''Program (bldstrand) to construct'',
     x  '' polymer models'')')
      write(*,'(''Version '',a10)') '2015-02-20'

      call getarg(1,fileset)
      isfs=index(fileset,' ')-1
      if(index(fileset(1:isfs),'.bin').eq.0) then
        write(*,'(''Error:  expecting file name with type '',
     x    ''*.bin on command line.'')')
        stop 'enter file name with type .bin on command line'
      endif
      
      isfs=index(fileset,'.bin')-1
      write(*,'(//,''File name set from command line argument: '',
     x  a)') fileset(1:isfs)





      open(9,file=fileset(1:isfs)//'.bin')   !input file
      open(13,file=fileset(1:isfs)//'.top')  !topology file

      read(9,'(a)',end=400) line
      string1=line
      call ucase(string1)
c     first line of input file is optionally
c     Molecule Name and/or Librarypath in any order
      molname='notspecified'
      libpath=' '    !default zmatix library is the current directory
      is0=index(string1,'MOLECULENAME')
      is1=index(string1,'LIBRARYPATH')
      if(is0.ne.0.or.is1.ne.0) then
        if(is0.ne.0) then   !found moleculename keyword
          is=is0+13
          call leftjust(line(is:))
          molname=line(is:)
        else                !found librarypath keyword
          is=is1+12
          call leftjust(line(is:))
          libpath=line(is:)
        endif
        read(9,'(a)',end=400) line
      endif
c
      string1=line
      call ucase(string1)
c     second line of input file is optionally
c     Molecule Name and/or Librarypath in any order
      is0=index(string1,'MOLECULENAME')
      is1=index(string1,'LIBRARYPATH')
      if(is0.ne.0.or.is1.ne.0) then
        if(is0.ne.0) then   !found moleculename keyword
          is=is0+13
          call leftjust(line(is:))
          molname=line(is:)
        else                !found librarypath keyword
          is=is1+12
          call leftjust(line(is:))
          libpath=line(is:)
        endif
        read(9,'(a)',end=400) line
      endif

c
      if(libpath(1:1).ne.' ') then
        is=index(libpath,' ')-1
        if(libpath(is:is).ne.'/') libpath(is:is)='/'
      endif
c


      call lstchr(molname,i1)
      write(*,'(''Molecule name *'',a,''*'')') molname(1:i1)

      if(libpath(1:1).eq.' ') then
        write(*,'(''Library path is current directory'')')
      else
        call lstchr(libpath,i2)
        write(*,'(''Library path *'',a,''*'')') libpath(1:i2)
      endif

      iadd=0     !monomer index 0 is assumed to be the core 
      write(*,'(/,'' bldstrand.in (monomer '',i2,''): '',a50)') 
     x   iadd,line(1:50)
      write(13,'(/,''Topology description file '')')
      write(13,'(/,''Molecule name '',a)') molname(1:i1)
      write(13,'(/,'' Repeat unit linkage information '')')
      call leftjust(line)
      call lstchr(line,il)
 
c
c     find the last token in the string; if it is a number, 
c     then assume arm ids are included in the input 
      ifarmid=0   !flag for if arm id is included; default mode = no arm ids
      iptr=0
      do i=il,1,-1
        if(iptr.eq.0.and.line(i:i).eq.' ') iptr=i
      enddo
      if(iptr.gt.0) then
        if(ifint(line(iptr+1:il)).eq.1) then
          read(line(iptr+1:il),*) idresarm
          ifarmid=1   !if armid for first repeat unit, assume also there for all
          call lstchr(line(1:iptr),il)
          line(il+1:)=' '
c         note:  arm id should be zero for the first residue listed
          if(idresarm.ne.0) then
            write(*,'(''Error:  the first arm id should be zero'')')
            write(*,'(''The first arm id is '',i10)') idresarm
            stop 'Probably an error; the first arm id should be zero'
          endif
        else
          write(*,'(''Error: expecting an arm id integer '')')
          stop 'Error: expecting an arm id'
        endif
      else
        idresarm=0
      endif

      if(ifarmid.eq.1) then
        open(14,file=fileset(1:isfs)//'.arm')
        write(13,'(i5,2x,a,'' ArmID '',i5)') iadd,line(1:il),idresarm
        idarm(iadd+1)=idresarm
      else
        write(13,'(i5,2x,a)') iadd,line(1:il)
      endif





c
c     get the base unit
c
c     call getmonomer(line,iseed,zmonomer,izmonomer,msites,
c    x  nzatmax,natmonomer,ndummy,lstbnd,mname(iadd))
      call getmonomerlib(line,libpath,        !new subroutine to get zmatrix from
     x    zmonomer,izmonomer,msites,nzatmax,  !files in a library directory
     x    natmonomer,ndummy,lstbnd,mname(iadd))

      write(*,'('' Zmatrix for monomeric unit '',i3)') iadd
      do i=1,natmonomer
        write(*,'(i2,2x,a10,4x,i2,2x,a10,2x,f10.4,
     x                      4x,i2,2x,a10,2x,f10.4,
     x                      4x,i2,2x,a10,2x,f10.4)')
     x   i,msites(i),
     x  (izmonomer(i,j),msites(izmonomer(i,j)),zmonomer(i,j),
     x   j=1,min(i-1,3))
      enddo

      call getrefsite(zmonomer,izmonomer,nzatmax,natmonomer,ndummy,
     x  dumsite)
c     note that in refsite, coordinates are arranged Du1,Du2,Du3
      do i=1,3
        do j=1,3
          refsite(j,i)=dumsite(j,i)
        enddo
      enddo

      icptr=0  !pointer in coord (and others) array for sites added

      do i=1,natmonomer-ndummy
        do j=1,3
          isite=izmonomer(i+ndummy,j) !j=1 get distance; j=2 gets angle; j=3 dihed
          if(isite.le.ndummy) then 
            do k=1,3
              ref(k,4-j)=refsite(k,isite)
            enddo
          else
            do k=1,3
              ref(k,4-j)=coord(k,isite-ndummy)
            enddo
          endif
        enddo
        rab=zmonomer(i+ndummy,1)
        ang=zmonomer(i+ndummy,2)
        dih=zmonomer(i+ndummy,3)
        write(*,'('' Position site (dihed)'',i2,'' rel to '',3f10.4)')
     x    i,(ref(k,1),k=1,3)
        write(*,'('' Position site (theta)'',i2,'' rel to '',3f10.4)')
     x    i,(ref(k,2),k=1,3)
        write(*,'('' Position site (r    )'',i2,'' rel to '',3f10.4)')
     x    i,(ref(k,3),k=1,3)
c       place expects to get ref organized in order dihed, angle, dist (D1,D2,D3)
        call place(ref,rab,ang,dih,coord(1,icptr+i))
        write(*,'('' Location site '',i2,2x,a,''        '',3f10.4)')
     x    i,msites(i+ndummy),(coord(k,icptr+i),k=1,3)


        call parser(msites(i+ndummy),string1,ls,i1,i2,i3)
        call packer(string1,icptr+i,i1,0,atname(icptr+i))
        write(*,'(i5,'' Name '',a,'' Coords '',3f10.4)') 
     x    i,atname(i+icptr),(coord(k,icptr+i),k=1,3)

c       copy the bond list for these sites
        nb=lstbnd(i+ndummy,1,1)
        lbonds(i+icptr,1,1)=nb
c       write(*,'('' Number of bonds to this site '',i3)') nb
        do j=1,nb
c         write(*,'('' Bond between sites '',a,
c    x      '' and '',a,'' type '',i2)')
c    x    atname(i),atname(lstbnd(i+ndummy,1,j+1)-ndummy),
c    x                     lstbnd(i+ndummy,2,j+1)
          lbonds(i+icptr,1,j+1)=lstbnd(i+ndummy,1,j+1)-ndummy
          lbonds(i+icptr,2,j+1)=lstbnd(i+ndummy,2,j+1)
        enddo

      enddo

      

      
      icptr=icptr+(natmonomer-ndummy)



100   continue
      read(9,'(a)',end=400) line
      if(line(1:20).eq.'                    ') go to 400
      call leftjust(line)
      call lstchr(line,il)
      iadd=iadd+1


      if(ifarmid.eq.1) then
c       check if the last token is an integer indicating an arm id 
        iptr=0
        do i=il,1,-1
          if(iptr.eq.0.and.line(i:i).eq.' ') iptr=i
        enddo
        if(iptr.gt.0.and.ifint(line(iptr+1:il)).eq.1) then
          read(line(iptr+1:il),*) idresarm
          call lstchr(line(1:iptr),il)
          line(il+1:)=' '
        else
          write(*,'(''Error: expecting an arm id integer '')')
          write(*,'(''line(1:il)  *'',a,''*'')') line(1:il)
          stop 'Error: expecting an arm id'
        endif
        write(*,'(/,'' From input file (monomer '',i3,''): '',
     x    a,5x,i5)') iadd,line(1:il),idresarm
        write(13,'(i5,2x,a,'' ArmID '',i5)') iadd,line(1:il),idresarm
        idarm(iadd+1)=idresarm
      else
        write(*,'(/,'' From input file (monomer '',i3,''): '',
     x    a)') iadd,line(1:il)
        write(13,'(i5,2x,a)') iadd,line(1:il)
      endif
    


c     call getmonomer(line,iseed,zmonomer,izmonomer,msites,
c    x  nzatmax,natmonomer,ndummy,lstbnd,mname(iadd))
      call getmonomerlib(line,libpath,
     x  zmonomer,izmonomer,msites,nzatmax,
     x  natmonomer,ndummy,lstbnd,mname(iadd))

c     write(*,'('' Zmatrix for monomeric unit '',i3)') iadd
c     do i=1,natmonomer
c       write(*,'(i2,2x,a10,4x,i2,2x,a10,2x,f10.4,
c    x                      4x,i2,2x,a10,2x,f10.4,
c    x                      4x,i2,2x,a10,2x,f10.4)')
c    x   i,msites(i),
c    x  (izmonomer(i,j),msites(izmonomer(i,j)),zmonomer(i,j),
c    x   j=1,min(i-1,3))
c     enddo

c     call getrefsite(zmonomer,izmonomer,nzatmax,natmonomer,ndummy,
c    x  refsite)

c     find the coordinates of the sites that define the dummy sites
      call getmatch(line,atname,coord,icptr,refsite,iref,dumsite)
c     write(*,'('' Refsite Du_1: '',3f10.5)')
c    x   (refsite(k,1),k=1,3)
c     write(*,'('' Refsite Du_2: '',3f10.5)')
c    x   (refsite(k,2),k=1,3)
c     write(*,'('' Refsite Du_3: '',3f10.5)')
c    x   (refsite(k,3),k=1,3)

      if(icptr+natmonomer-ndummy.gt.natmax) then
        write(*,'('' Need to increase size of lbonds and natmax'')')
        stop 'storage array lbonds not large enough '
      endif

      do i=1,natmonomer-ndummy
        do j=1,3
          isite=izmonomer(i+ndummy,j)
          if(isite.le.ndummy) then 
            do k=1,3
              ref(k,4-j)=refsite(k,isite)
            enddo
          else
            do k=1,3
              ref(k,4-j)=coord(k,icptr+isite-ndummy)
            enddo
          endif
        enddo
        rab=zmonomer(i+ndummy,1)
        ang=zmonomer(i+ndummy,2)
        dih=zmonomer(i+ndummy,3)
c       write(*,'('' Position site (d)'',i2,'' rel to '',3f10.4)')
c    x    i,(ref(k,1),k=1,3)
c       write(*,'('' Position site (t)'',i2,'' rel to '',3f10.4)')
c    x    i,(ref(k,2),k=1,3)
c       write(*,'('' Position site (r)'',i2,'' rel to '',3f10.4)')
c    x    i,(ref(k,3),k=1,3)
c       place expects to get ref organized in order dihed, angle, dist (D1,D2,D3)
        call place(ref,rab,ang,dih,coord(1,icptr+i))


        is=index(msites(i+ndummy),'_')-1
        read(msites(i+ndummy)(is+2:),*) iseq
c       write(*,'('' beforecall to packer:  msites(1:is) = *'',a,
c    x    ''*'')')
c    x    msites(i+ndummy)(1:is)
c       write(*,'('' beforecall to packer:  icptr+i,iseq,iadd = '',
c    x       3i5)') icptr+1,iseq,iadd
        call packer(msites(i+ndummy)(1:is),icptr+i,iseq,iadd,
     x    atname(icptr+i))
c       write(*,'('' after call to packer:  msites(1:is) = *'',a,
c    x    ''*'')')
c    x    msites(i+ndummy)(1:is)
c       write(*,'('' after call to packer:  icptr+i,iseq,iadd = '',
c    x       3i5)') icptr+1,iseq,iadd
c       write(*,'('' after call to packer:  atname= *'',a,''*'')')
c    x       atname(icptr+i)
        write(*,'(i5,'' Name '',a,'' Coords '',3f10.4)') 
     x    i,atname(i+icptr),(coord(k,icptr+i),k=1,3)

c       copy the bond list for these sites
        nb=lstbnd(i+ndummy,1,1)
        lbonds(i+icptr,1,1)=nb
c       write(*,'('' Number of bonds to this site '',i3)') nb
        do j=1,nb
c         write(*,'('' lstbnd: '',2i5)')
c    x      lstbnd(i+ndummy,1,j+1),lstbnd(i+ndummy,2,j+1)
c         write(*,'('' Bond between sites '',a,
c    x      '' and '',a,'' type '',i2)')
c    x    atname(i+icptr),
c    x    atname(lstbnd(i+ndummy,1,j+1)-ndummy+icptr),
c    x           lstbnd(i+ndummy,2,j+1)
          lbonds(i+icptr,1,j+1)=icptr+lstbnd(i+ndummy,1,j+1)-ndummy
          lbonds(i+icptr,2,j+1)=lstbnd(i+ndummy,2,j+1)
        enddo


      enddo

c     add the newly formed linker bond to the bond list
c     assume this to be between the first real site of this
c     monomeric unit and the one indicated by the zmatrix for this site     
      write(*,'('' Adding linker bond '')')
      isite=icptr+1
      jsite=iref(3)
      nb=lbonds(isite,1,1)+1
      lbonds(isite,1,1)=nb
      lbonds(isite,1,nb+1)=jsite
      lbonds(isite,2,nb+1)=1
      nb=lbonds(jsite,1,1)+1
      lbonds(jsite,1,1)=nb
      lbonds(jsite,1,nb+1)=isite
      lbonds(jsite,2,nb+1)=1
      write(*,'('' Linkage bond added betwn '',a,'' and '',a,
     x  ''  type  '',i1)')
     x atname(isite),atname(jsite),lbonds(jsite,2,nb+1)
      

      icptr=icptr+(natmonomer-ndummy)

      go to 100 !obtain a new monomer unit
400   continue  !here when done adding monmers
      close(9)


      write(*,'(/,'' Final (heavy atom) site list '')')
      do i=1,icptr
        write(*,'(i5,'' Name '',a,'' Coords '',3f10.4)') 
     x    i,atname(i),(coord(k,i),k=1,3)
      enddo

      write(*,'(/,'' Final (heavy atom) bond list '')')
      do i=1,icptr
        write(*,'('' Number bonds to site '',i5,'' ('',a,'') is '',i5)')
     x    i,atname(i),lbonds(i,1,1)
        do j=1,lbonds(i,1,1)
          write(*,'('' Between '',a,'' and '',a,'' of type '',i1)')
     x      atname(i),atname(lbonds(i,1,j+1)),lbonds(i,2,j+1)
        enddo
      enddo


      call display(coord,atname,icptr)


c     set up arrays to perform no reordering
      do i=1,icptr
        neworder(1,i)=i
        neworder(2,i)=i
        newatname(i)=atname(i)
      enddo


c
c     add hydrogen sites based on bond counts
c 
      if(ifaddh.eq.1) then
        nadded=icptr
        call addhyd(coord,atname,icptr,lbonds,natmax)
        nadded=icptr-nadded
        write(*,'('' Number of hydrogen sites added = '',i5)') nadded
        write(*,'('' Hydrogen sites from '',i4,'' to '',i4)')
     x    icptr-nadded+1,icptr

c       following set arrays to perform no mapping
        do i=icptr-nadded+1,icptr
          neworder(1,i)=i
          neworder(2,i)=i
          newatname(i)=atname(i)
        enddo


c
c     reorder the sites so that all sites of a given monomeric unit are contiguous
c     (they would only not be if hydrogens were added
c
        do i=1,icptr
          call parser(atname(i),string1,lele,i1,i2,i3)
          neworder(1,i)=0
          neworder(2,i)=i3
        enddo
        inext=1 !initialize pointer in site list
        iseq=1  !initialize pointer in neworder list
        do i=0,iadd
c         write(*,'('' Searching for sites of repeat unit '',i4,
c    x      '':  '',a)') i,mname(i)
450       continue
          if(inext.gt.icptr.or.neworder(2,inext).ne.i) go to 500
          neworder(1,iseq)=inext
          iseq=iseq+1
          inext=inext+1
          go to 450
500       continue
c         now look for the hydrogens that go with this repeat unit
          do j=icptr-nadded+1,icptr
            if(neworder(2,j).eq.i) then
              neworder(1,iseq)=j
              iseq=iseq+1
            endif
          enddo
        enddo

         do i=1,icptr
           ix=neworder(1,i)
           neworder(2,ix)=i
           call parser(atname(ix),string1,lele,i1,i2,i3)
           call packer(string1,i,i2,i3,newatname(i))
           write(*,'('' site='',2i5,2x,'' oldname='',a,2x,
     x                                 '' newname='',a)') 
     x        i,ix,atname(ix),newatname(i)
         enddo


         open(11,file=fileset(1:isfs)//'.map')
         write(11,'(i10,'' natoms '')') icptr
         do i=1,icptr
           write(11,'(3i10,a30,a30)')
     x       i,neworder(1,i),neworder(2,i),
     x       atname(i),newatname(i)
         enddo
         close(11)


      endif  !end of ifblock for adding hydrogen sites




      write(*,'(/,'' Final site list '')')
      do i=1,icptr
        ix=neworder(1,i)
        write(*,'(i5,'' Name '',a,'' Coords '',3f10.4)') 
     x    i,newatname(i),(coord(k,ix),k=1,3)
c       write(*,'(i5,'' Fmr name '',a,'' Coords '',3f10.4)') 
c    x    ix,atname(ix),(coord(k,ix),k=1,3)
      enddo

      write(*,'(/,'' Final bond list '')')
      do i=1,icptr
c       write(*,'(/)')
c       write(*,'('' Number bonds to site '',i5,
c    x    '' ('',a,'') is '',i5)')
c    x    i,atname(i),lbonds(i,1,1)
c       do j=1,lbonds(i,1,1)
c         write(*,'('' Between '',a,'' and '',a,'' of type '',i1)')
c    x      atname(i),atname(lbonds(i,1,j+1)),lbonds(i,2,j+1)
c       enddo

        ix=neworder(1,i)
        write(*,'(/)')
        write(*,'('' Number bonds to site '',i5,
     x    '' ('',a,'') is '',i5)')
     x    i,newatname(i),lbonds(ix,1,1)

c       write(*,'('' Fmr    bonds to site '',i5,
c    x    '' ('',a,'') is '',i5)')
c    x    ix,atname(ix),lbonds(ix,1,1)
        do j=1,lbonds(ix,1,1)
          write(*,'('' Between '',a,'' and '',a,'' of type '',i1)')
     x      newatname(i),
     x      newatname(neworder(2,lbonds(ix,1,j+1))),
     x      lbonds(ix,2,j+1)
c         write(*,'('' Fmr     '',a,'' and '',a,'' of type '',i1)')
c    x      atname(ix),
c    x      atname(lbonds(ix,1,j+1)),
c    x      lbonds(ix,2,j+1)
        enddo



      enddo





      call wrtpoly(coord,newatname,neworder,icptr,mname,iadd,
     x  lbonds,natmax,fileset)

      call wrttop(coord,newatname,neworder,lbonds,mname,icptr,natmax)

      call intern(coord,newatname,neworder,lbonds,icptr,natmax)

      do i=1,icptr-1
        do j=i+1,icptr
          dist=(coord(1,i)-coord(1,j))**2
     x        +(coord(2,i)-coord(2,j))**2
     x        +(coord(3,i)-coord(3,j))**2
          dist=dsqrt(dist)
          if(dist.lt.1.d0) then
            ix=neworder(2,i)
            iy=neworder(2,j)
            write(*,'('' Close approach between sites '',a,
     x        '' and '',a,'' R='',f10.5)') newatname(ix),newatname(iy),
     x        dist
          endif
        enddo
      enddo

      close(13)


c     if ifarmid=1, write the *.arm file
c     this has repeat unit index; name; first/last site; ArmID
      if(ifarmid.eq.1) then
        call parser(newatname(1),string1,ls,i1,i2,i3)
c         i1 is a unique atom index (starts with 1)
c         i2 is a site number within the residue (non-hydrogen)
c            or a site number to which the hydrogen is connected (hydrogen)
c         i3 is a residue index (starts with 0)
        ifrst=1
        icurrent=i3
        do i=1,icptr
          call parser(newatname(i),string1,ls,i1,i2,i3)
          if(icurrent.ne.i3) then
            call leftjust(mname(icurent))
            call lstchr(mname(icurrent),il)
            mname(icurrent)(il+1:)=' '
            write(14,'(i10,5x,a,5x,i10,5x,i10,5x,i10)')  
     x        icurrent+1,mname(icurrent)(1:10),ifrst,i-1,
     x        idarm(icurrent+1) 
            ifrst=i
            icurrent=i3
          endif
        enddo
        write(14,'(i10,5x,a,5x,i10,5x,i10,5x,i10)')  
     x    i3+1,mname(i3)(1:10),ifrst,icptr,idarm(i3+1)
        close(14)
      endif



      end

c
c     subroutine to list all internal coordinates (involving bonded sites)
c
      subroutine intern(coord,newatname,neworder,lbonds,icptr,natmax)
      implicit real*8 (a-h,o-z)
      character*(*) newatname(icptr)
      dimension coord(3,icptr),lbonds(natmax,2,6),neworder(2,icptr)
      dimension dum1(12),dum2(78)
 
      pi=4.d0*datan(1.d0)
c
c
c     list the bonds for each site
c
      write(*,'(/,'' Bond list and distances '')')
      do i=1,icptr
        ix=neworder(1,i)
        nb=lbonds(ix,1,1)
        do j=1,nb
          iy=lbonds(ix,1,j+1)
          jsite=neworder(2,iy)
          dist=0.d0   
          do k=1,3
            dist=dist+(coord(k,ix)-coord(k,iy))**2
          enddo
          write(*,'(1x,a,'' - '',a,'' R='',f10.5)')
     x      newatname(i),newatname(jsite),dsqrt(dist)
        enddo
      enddo   

c
c     list the angles for each site
c
      write(*,'(/,'' Angle list and Values '')')
      do i=1,icptr
        ix=neworder(1,i)
        nb=lbonds(ix,1,1)
        do j=1,nb
          iy=lbonds(ix,1,j+1)
          jsite=neworder(2,iy)
          nb2=lbonds(iy,1,1)
          do k=1,nb2
            iz=lbonds(iy,1,k+1)
            ksite=neworder(2,iz)
            if(ksite.ne.i) then
              call angint(coord(1,ix),coord(1,iy),coord(1,iz),ang)
              write(*,'(1x,a,'' - '',a,'' - '',a,'' Theta = '',f10.5)')
     x        newatname(i),
     x        newatname(jsite),newatname(ksite),180.d0*ang/pi
            endif
          enddo
        enddo
      enddo

c
c     list the dihedral angles for each site
c
      write(*,'(/,'' Dihedral angle list and Valuess '')')
      do i=1,icptr
        ix=neworder(1,i)
        nb=lbonds(ix,1,1)
        do j=1,nb
          iy=lbonds(ix,1,j+1)
          jsite=neworder(2,iy)
          nb2=lbonds(iy,1,1)
          do k=1,nb2
            iz=lbonds(iy,1,k+1)
            ksite=neworder(2,iz)
            if(ksite.ne.i) then
              nb3=lbonds(iz,1,1)
              do l=1,nb3
                iw=lbonds(iz,1,l+1)
                lsite=neworder(2,iw)
                if(lsite.ne.jsite) then
                call dihint(1,coord(1,ix),coord(1,iy),
     x                      coord(1,iz),coord(1,iw),
     x                      ang,dum1,dum2)
                write(*,'(1x,a,'' - '',a,'' - '',a,'' - '',a,
     x            '' Phi = '',f10.5)')
     x            newatname(i),newatname(jsite),
     x            newatname(ksite),newatname(lsite),180.d0*ang/pi
                endif
              enddo
            endif
          enddo
        enddo
      enddo

      end

c
c     subroutine to add hydrogen sites
c
      subroutine addhyd(coord,atname,icptr,lbonds,natmax)
      implicit real*8 (a-h,o-z)
      character*(*) atname(natmax)
      dimension coord(3,natmax),lbonds(natmax,2,6)
      character*120 string1
      dimension ref(3,3)


        nhadd=0  !counter for added hydrogen sites
        do i=1,icptr
          nb=lbonds(i,1,1)
c         since aromatic bonds will count as 1.5 bonds, get twice the bond order and divide by 2
          norder=0
          do j=1,nb
            if(lbonds(i,2,j+1).le.3) then
              norder=norder+2*lbonds(i,2,j+1)
            elseif(lbonds(i,2,j+1).eq.4) then
              norder=norder+3
            endif
          enddo
          norder=norder/2
          if(atname(i)(1:2).eq.'C_'.and.norder.lt.4) then
            if(icptr+nhadd+3.gt.natmax) then
              write(*,'('' Error:  out of memory while adding'',
     x         '' hydrogen sites '',/,
     x         '' icptr = '',i10,'' nhadd = '',i10,
     x         '' natmax = '',i10)') icptr,nhadd,natmax
              stop 'out of memory; increase natmax'
            endif
            write(*,'('' Adding '',i1,'' hydrogen sites to '',a)')
     x        4-norder,atname(i)
            call parser(atname(i),string1,ls,i1,i2,imon)
            iseq=i2
            if(4-norder.eq.3) then
c             find three site linked in a chain
              jsite=lbonds(i,1,2) !choose first neighbor; likely the only one in this case
              ksite=lbonds(jsite,1,2) !choose first neighbor of jsite
              if(ksite.eq.i) ksite=lbonds(jsite,1,3) !... or second neighbor if ksite=i
              do k=1,3
                ref(k,1)=coord(k,ksite)
                ref(k,2)=coord(k,jsite)
                ref(k,3)=coord(k,i)
              enddo
c             place expects to get ref organized in order dihed, angle, dist (D1,D2,D3)
              rab=1.09d0  !typical -C-H bond length 
              ang=109.5d0 !tetrahedral
              call place(ref,rab,ang, 60.d0,coord(1,icptr+nhadd+1))
              call place(ref,rab,ang,-60.d0,coord(1,icptr+nhadd+2))
              call place(ref,rab,ang,180.d0,coord(1,icptr+nhadd+3))
              call packer('H',icptr+nhadd+1,iseq,imon,
     x          atname(icptr+nhadd+1))
              call packer('H',icptr+nhadd+2,iseq,imon,
     x          atname(icptr+nhadd+2))
              call packer('H',icptr+nhadd+3,iseq,imon,
     x          atname(icptr+nhadd+3))
              write(*,'('' Added three sites:  '',a,2x,a,2x,a)')
     x          atname(icptr+nhadd+1),atname(icptr+nhadd+2),
     x          atname(icptr+nhadd+3) 
              lbonds(i,1,1)=nb+3
              lbonds(i,1,nb+2)=icptr+nhadd+1
              lbonds(i,2,nb+2)=1
              lbonds(i,1,nb+3)=icptr+nhadd+2
              lbonds(i,2,nb+3)=1
              lbonds(i,1,nb+4)=icptr+nhadd+3
              lbonds(i,2,nb+4)=1
              lbonds(icptr+nhadd+1,1,1)=1
              lbonds(icptr+nhadd+1,2,1)=0
              lbonds(icptr+nhadd+1,1,2)=i
              lbonds(icptr+nhadd+1,2,2)=1
              lbonds(icptr+nhadd+2,1,1)=1
              lbonds(icptr+nhadd+2,2,1)=0
              lbonds(icptr+nhadd+2,1,2)=i
              lbonds(icptr+nhadd+2,2,2)=1
              lbonds(icptr+nhadd+3,1,1)=1
              lbonds(icptr+nhadd+3,2,1)=0
              lbonds(icptr+nhadd+3,1,2)=i
              lbonds(icptr+nhadd+3,2,2)=1
              nhadd=nhadd+3
            elseif(4-norder.eq.2) then
c             find two neighbors of i           
              if(nb.ne.2) stop 'Error placing 2 hydrogens'
              jsite=lbonds(i,1,2) !choose first neighbor; likely to be one of two           
              ksite=lbonds(i,1,3) !choose second neighbor           
c             compute unit vector in plane of i,j,k opposite to bisector
              do k=1,3
                ref(k,1)=-(coord(k,ksite)+coord(k,jsite)
     x                          -2.d0*coord(k,i))
                ref(k,2)=coord(k,ksite)-coord(k,i)
              enddo
              ref(1,3)=ref(2,1)*ref(3,2)-ref(3,1)*ref(2,2)
              ref(2,3)=ref(3,1)*ref(1,2)-ref(1,1)*ref(3,2)
              ref(3,3)=ref(1,1)*ref(2,2)-ref(2,1)*ref(1,2)
              d1=0.d0
              d2=0.d0
              do k=1,3
                d1=d1+ref(k,1)**2           
                d2=d2+ref(k,3)**2           
              enddo
              rab=1.09d0  !typical -C-H bond length 
              do k=1,3
                ref(k,1)=ref(k,1)/dsqrt(d1)
                ref(k,3)=ref(k,3)/dsqrt(d2)
                coord(k,icptr+nhadd+1)=coord(k,i)+
     x             rab*(dsqrt(1.d0/3.d0)*ref(k,1) 
     x                + dsqrt(2.d0/3.d0)*ref(k,3))
                coord(k,icptr+nhadd+2)=coord(k,i)+
     x             rab*(dsqrt(1.d0/3.d0)*ref(k,1) 
     x                - dsqrt(2.d0/3.d0)*ref(k,3))
              enddo
              call packer('H',icptr+nhadd+1,iseq,imon,
     x          atname(icptr+nhadd+1))
              call packer('H',icptr+nhadd+2,iseq,imon,
     x          atname(icptr+nhadd+2))
              write(*,'('' Added two sites:  '',a,2x,a)')
     x          atname(icptr+nhadd+1),atname(icptr+nhadd+2)
              lbonds(i,1,1)=nb+2
              lbonds(i,1,nb+2)=icptr+nhadd+1
              lbonds(i,2,nb+2)=1
              lbonds(i,1,nb+3)=icptr+nhadd+2
              lbonds(i,2,nb+3)=1
              lbonds(icptr+nhadd+1,1,1)=1
              lbonds(icptr+nhadd+1,2,1)=0
              lbonds(icptr+nhadd+1,1,2)=i
              lbonds(icptr+nhadd+1,2,2)=1
              lbonds(icptr+nhadd+2,1,1)=1
              lbonds(icptr+nhadd+2,2,1)=0
              lbonds(icptr+nhadd+2,1,2)=i
              lbonds(icptr+nhadd+2,2,2)=1
              nhadd=nhadd+2
            elseif(4-norder.eq.1) then
              if(nb.eq.2) then 
c               this code adds a single hydrogen to an sp2 carbon
                jsite=lbonds(i,1,2) !choose first neighbor           
                ksite=lbonds(i,1,3) !choose second neighbor           
                d1=0.d0
                d2=0.d0
                do k=1,3
                  ref(k,1)=coord(k,jsite)-coord(k,i)
                  ref(k,2)=coord(k,ksite)-coord(k,i)
                  d1=d1+ref(k,1)**2
                  d2=d2+ref(k,2)**2
                enddo
                d4=0.d0
                do k=1,3
                  ref(k,1)=ref(k,1)/dsqrt(d1)  
     x                   + ref(k,2)/dsqrt(d2)
                  d4=d4+ref(k,1)**2
                enddo
                rab=1.084d0  !typical -C-H bond length in benzene
                do k=1,3
                  coord(k,icptr+nhadd+1)=coord(k,i)
     x              - rab*ref(k,1)/dsqrt(d4)
                enddo
                call packer('H',icptr+nhadd+1,iseq,imon,
     x            atname(icptr+nhadd+1))
                write(*,'('' Added one site:  '',a)')
     x            atname(icptr+nhadd+1)
                lbonds(i,1,1)=nb+1
                lbonds(i,1,nb+2)=icptr+nhadd+1
                lbonds(i,2,nb+2)=1
                lbonds(icptr+nhadd+1,1,1)=1
                lbonds(icptr+nhadd+1,2,1)=0
                lbonds(icptr+nhadd+1,1,2)=i
                lbonds(icptr+nhadd+1,2,2)=1
                nhadd=nhadd+1
              elseif(nb.eq.3) then
c               this code adds a single hydrogen to an sp3 carbon
                jsite=lbonds(i,1,2) !choose first neighbor; likely to be one of two           
                ksite=lbonds(i,1,3) !choose second neighbor           
                lsite=lbonds(i,1,4) !choose third neighbor           
                d1=0.d0
                d2=0.d0
                d3=0.d0
                do k=1,3
                  ref(k,1)=coord(k,jsite)-coord(k,i)
                  ref(k,2)=coord(k,ksite)-coord(k,i)
                  ref(k,3)=coord(k,lsite)-coord(k,i)
                  d1=d1+ref(k,1)**2
                  d2=d2+ref(k,2)**2
                  d3=d3+ref(k,3)**2
                enddo
                d4=0.d0
                do k=1,3
                  ref(k,1)=ref(k,1)/dsqrt(d1)  
     x                   + ref(k,2)/dsqrt(d2)
     x                   + ref(k,3)/dsqrt(d3)
                  d4=d4+ref(k,1)**2
                enddo
                rab=1.09d0  !typical -C-H bond length 
                do k=1,3
                  coord(k,icptr+nhadd+1)=coord(k,i)
     x              - rab*ref(k,1)/dsqrt(d4)
                enddo
                call packer('H',icptr+nhadd+1,iseq,imon,
     x            atname(icptr+nhadd+1))
                write(*,'('' Added one site:  '',a)')
     x            atname(icptr+nhadd+1)
                lbonds(i,1,1)=nb+1
                lbonds(i,1,nb+2)=icptr+nhadd+1
                lbonds(i,2,nb+2)=1
                lbonds(icptr+nhadd+1,1,1)=1
                lbonds(icptr+nhadd+1,2,1)=0
                lbonds(icptr+nhadd+1,1,2)=i
                lbonds(icptr+nhadd+1,2,2)=1
                nhadd=nhadd+1
              else
                stop 'Error placing 1 hydrogens'
              endif
            endif
          endif

          if(atname(i)(1:2).eq.'O_'.and.norder.lt.2) then
            if(icptr+nhadd+1.gt.natmax) then
              write(*,'('' Error:  out of memory while adding'',
     x         '' hydrogen sites to oxygen '',/,
     x         '' icptr = '',i10,'' nhadd = '',i10,
     x         '' natmax = '',i10)') icptr,nhadd,natmax
              stop 'out of memory; increase natmax'
            endif
            write(*,'('' Adding '',i1,'' hydrogen site to '',a)')
     x        2-norder,atname(i)
            call parser(atname(i),string1,ls,i1,i2,imon)
            iseq=i2
c           find three site linked in a chain
            jsite=lbonds(i,1,2) !choose first neighbor; likely the only one in this case
            ksite=lbonds(jsite,1,2) !choose first neighbor of jsite
            if(ksite.eq.i) ksite=lbonds(jsite,1,3) !... or second neighbor if ksite=i
            do k=1,3
              ref(k,1)=coord(k,ksite)
              ref(k,2)=coord(k,jsite)
              ref(k,3)=coord(k,i)
            enddo
c           place expects to get ref organized in order dihed, angle, dist (D1,D2,D3)
            rab=0.945d0 !typical -O-H bond length (opls-aa)
            ang=108.5d0 !typical -C-O-H angle (opls-aa)
            call place(ref,rab,ang,180.d0,coord(1,icptr+nhadd+1))
            call packer('H',icptr+nhadd+1,iseq,imon,
     x        atname(icptr+nhadd+1))
            write(*,'('' Added one site:  '',a,2x,a,2x,a)')
     x        atname(icptr+nhadd+1)
            lbonds(i,1,1)=nb+1
            lbonds(i,1,nb+2)=icptr+nhadd+1
            lbonds(i,2,nb+2)=1
            lbonds(icptr+nhadd+1,1,1)=1
            lbonds(icptr+nhadd+1,2,1)=0
            lbonds(icptr+nhadd+1,1,2)=i
            lbonds(icptr+nhadd+1,2,2)=1
            nhadd=nhadd+1
          endif

          if(atname(i)(1:2).eq.'N_'.and.norder.lt.3) then
            if(icptr+nhadd+2.gt.natmax) then
              write(*,'('' Error:  out of memory while adding'',
     x         '' hydrogen sites to nitrogen'',/,
     x         '' icptr = '',i10,'' nhadd = '',i10,
     x         '' natmax = '',i10)') icptr,nhadd,natmax
              stop 'out of memory; increase natmax'
            endif
            write(*,'('' Adding '',i1,'' hydrogen sites to '',a)')
     x        3-norder,atname(i)
            write(*,'('' Note that hydrogen placement is planar'',
     x        '' rather than pyramidal.'')')
            call parser(atname(i),string1,ls,i1,i2,imon)
            iseq=i2
            if(3-norder.eq.1) then
c             add a single hydrogen to the nitrogen
c             we assume the nitrogen has exactly two neighbors (two single bonds)
              jsite=lbonds(i,1,2) !choose first neighbor; likely to be one of two           
              ksite=lbonds(i,1,3) !choose second neighbor           
              d1=0.d0
              d2=0.d0
              do k=1,3
                ref(k,1)=coord(k,jsite)-coord(k,i)
                ref(k,2)=coord(k,ksite)-coord(k,i)
                d1=d1+ref(k,1)**2
                d2=d2+ref(k,2)**2
              enddo
              d4=0.d0
              do k=1,3
                ref(k,1)=ref(k,1)/dsqrt(d1)  
     x                 + ref(k,2)/dsqrt(d2)
                d4=d4+ref(k,1)**2
              enddo
              rab=1.01d0  !typical -N-H bond length 
              do k=1,3
                coord(k,icptr+nhadd+1)=coord(k,i)
     x            - rab*ref(k,1)/dsqrt(d4)
              enddo
              write(*,'(''Calling packer in code to add one'',
     x           '' H to a N'')')
              write(*,'('' icptr, nhadd, iseq, imon = '',4i10)')
     x           icptr,nhadd,iseq,imon
              call packer('H',icptr+nhadd+1,iseq,imon,
     x          atname(icptr+nhadd+1))
              write(*,'('' Added one site:  '',a)')
     x          atname(icptr+nhadd+1)
              lbonds(i,1,1)=nb+1
              lbonds(i,1,nb+2)=icptr+nhadd+1
              lbonds(i,2,nb+2)=1
              lbonds(icptr+nhadd+1,1,1)=1
              lbonds(icptr+nhadd+1,2,1)=0
              lbonds(icptr+nhadd+1,1,2)=i
              lbonds(icptr+nhadd+1,2,2)=1
              nhadd=nhadd+1
            elseif(3-norder.eq.2) then
c             add two hydrogen to the nitrogen
c             find three site linked in a chain
              jsite=lbonds(i,1,2) !choose first neighbor; likely the only one in this case
              ksite=lbonds(jsite,1,2) !choose first neighbor of jsite
              if(ksite.eq.i) ksite=lbonds(jsite,1,3) !... or second neighbor if ksite=i
              do k=1,3
                ref(k,1)=coord(k,ksite)
                ref(k,2)=coord(k,jsite)
                ref(k,3)=coord(k,i)
              enddo
c             place expects to get ref organized in order dihed, angle, dist (D1,D2,D3)
              rab=1.01d0  !typical -N-H bond length 
              ang=118.4d0 !typcial -CT-N-H bond angle
              call place(ref,rab,ang, 90.d0,coord(1,icptr+nhadd+1))
              call place(ref,rab,ang,-90.d0,coord(1,icptr+nhadd+2))
              call packer('H',icptr+nhadd+1,iseq,imon,
     x          atname(icptr+nhadd+1))
              call packer('H',icptr+nhadd+2,iseq,imon,
     x          atname(icptr+nhadd+2))
              write(*,'('' Added two sites:  '',a,2x,a,2x,a)')
     x          atname(icptr+nhadd+1),atname(icptr+nhadd+2)
              lbonds(i,1,1)=nb+2
              lbonds(i,1,nb+2)=icptr+nhadd+1
              lbonds(i,2,nb+2)=1
              lbonds(i,1,nb+3)=icptr+nhadd+2
              lbonds(i,2,nb+3)=1
              lbonds(icptr+nhadd+1,1,1)=1
              lbonds(icptr+nhadd+1,2,1)=0
              lbonds(icptr+nhadd+1,1,2)=i
              lbonds(icptr+nhadd+1,2,2)=1
              lbonds(icptr+nhadd+2,1,1)=1
              lbonds(icptr+nhadd+2,2,1)=0
              lbonds(icptr+nhadd+2,1,2)=i
              lbonds(icptr+nhadd+2,2,2)=1
              nhadd=nhadd+2
            else
              stop 'Error placing hydrogens on nitrogen site'
            endif
          endif


        enddo

        write(*,'('' Total number of hydrogen sites added '',i4)') nhadd
        icptr=icptr+nhadd

      end

      
c
      subroutine wrttop(coord,newatname,neworder,lbonds,mname,
     x    icptr,natmax)
      implicit real*8 (a-h,o-z)
      character*(*) newatname(natmax),mname(0:*)
      dimension coord(3,natmax),lbonds(natmax,2,6),neworder(2,icptr)
      character*10 ele

c
c
c     loop over all sites
c     for each site give name,
c     (element (char*2),global index,index within repeat unit,repeat unit index)
c     repeat unit type (char)
c
c     for hydrogen sites
c     each site give name,
c     (element (char*2),global index,index within repeat unit to which it is bonded,
c     repeat unit index)
c     repeat unit type (char)
c
c

      write(13,'(/,'' Detailed site information '')') 

      do i=1,icptr
        ele=' '
        call parser(newatname(i),ele,lele,i1,i2,i3)
        if(ele(1:2).ne.'H ') then
          write(13,'(i10,2x,a,2x,a2,3i10,2x,a)') 
     x    i,newatname(i),ele(1:2),i1,i2,i3,mname(i3)
        else
c         find the site index to which this hydrogen is connected         
          ix=neworder(1,i)
          isite=neworder(2,lbonds(ix,1,2))
          write(13,'(i10,2x,a,2x,a2,3i10,2x,a,2x,i10,2x,a)') 
     x    i,newatname(i),ele(1:2),i1,i2,i3,mname(i3),
     x    isite,newatname(isite)
        endif
      enddo

c     this is to help one build mmol files
c     do i=1,icptr
c       write(21,'(''Charge '',a)') 
c    x    atname(i)(1:4)
c     enddo
c     do i=1,icptr
c       write(21,'(''Lennard_Jones_6_12 '',a)') 
c    x    atname(i)(1:4)
c     enddo
c     this is to help one build mmol files

      write(13,'(/,'' Detailed bond information '')') 

      do i=1,icptr
        ix=neworder(1,i)
        nb=lbonds(ix,1,1)
        call lstchr(newatname(i),i1)
        do j=1,nb
          jsite=neworder(2,lbonds(ix,1,j+1))
          if(jsite.gt.i) then
            call lstchr(newatname(jsite),j1)
            write(13,'(i3,2x,a,''-'',a)')
     x      lbonds(ix,2,j+1),
     x      newatname(i)(1:i1),
     x      newatname(jsite)(1:j1)
c     this is to help one build mmol files
c           write(21,'(2x,a,''-'',a)')
c    x      atname(i)(1:4),atname(jsite)(1:4)
c     this is to help one build mmol files
          endif
        enddo
      enddo

      write(13,'(/,'' Detailed angle information '')') 

      do i=1,icptr
c       nbi=lbonds(i,1,1)
c       call lstchr(atname(i),i1)
        ix=neworder(1,i)
        nbi=lbonds(ix,1,1)
        call lstchr(newatname(i),i1)
        do j=1,nbi
          iy=lbonds(ix,1,j+1)
          jsite=neworder(2,iy)
          call lstchr(newatname(jsite),j1)
          nbj=lbonds(iy,1,1)
          do k=1,nbj
            iz=lbonds(iy,1,k+1)
            ksite=neworder(2,iz)
            call lstchr(newatname(ksite),k1)
            if(i.lt.ksite) then
            write(13,'(2x,a,''-'',a,''-'',a)')
     x        newatname(i)(1:i1),
     x        newatname(jsite)(1:j1),
     x        newatname(ksite)(1:k1)
c     this is to help one build mmol files
c           write(21,'(2x,a,''-'',a,''-'',a)')
c    x      atname(i)(1:4),atname(jsite)(1:4),atname(ksite)(1:4)
c     this is to help one build mmol files
            endif
          enddo
        enddo
      enddo

      write(13,'(/,'' Detailed torsion information '')') 

      do i=1,icptr
        ix=neworder(1,i)
        nbi=lbonds(ix,1,1)
        call lstchr(newatname(i),i1)
        do j=1,nbi
          iy=lbonds(ix,1,j+1)
          jsite=neworder(2,iy)
          call lstchr(newatname(jsite),j1)
          nbj=lbonds(iy,1,1)
          do k=1,nbj
            iz=lbonds(iy,1,k+1)
            ksite=neworder(2,iz)
            call lstchr(newatname(ksite),k1)
            if(i.ne.ksite) then
c           write(13,'(2x,a,''-'',a,''-'',a)')
c    x        newatname(i)(1:i1),
c    x        newatname(jsite)(1:j1),
c    x        newatname(ksite)(1:k1)
              nbk=lbonds(iz,1,1)
              do l=1,nbk
                iw=lbonds(iz,1,l+1)
                lsite=neworder(2,iw)
                call lstchr(newatname(lsite),l1)
                if(jsite.ne.lsite) then
                  if(lsite.gt.i) then
                  write(13,'(2x,a,''-'',a,''-'',a,''-'',a)')
     x              newatname(i)(1:i1),
     x              newatname(jsite)(1:j1),
     x              newatname(ksite)(1:k1),
     x              newatname(lsite)(1:l1) 
c     this is to help one build mmol files
c                 write(21,'(2x,a,''-'',a,''-'',a,''-'',a)')
c    x              atname(i)(1:4),
c    x              atname(jsite)(1:4),
c    x              atname(ksite)(1:4),
c    x              atname(lsite)(1:4) 
c     this is to help one build mmol files
                  endif
                endif
              enddo
            endif
          enddo
        enddo
      enddo

      end

c



c
c     subroutine to find which of the sites already placed
c     correspond to the dummy sites
c     store the coordinate of the dummy sites in refsite in order Du_1, Du_2, Du_3
c

      subroutine getmatch(line,atname,coord,icptr,refsite,iref,dummy)
      implicit real*8 (a-h,o-z)
      dimension coord(3,icptr),refsite(3,3),iref(3),dummy(3,*)
      character*(*) line,atname(icptr)
      dimension ip(3)
      character*20 string,element,atsite


      call parser(atname(icptr),element,ls,i1,i2,i3)
      im=i3  !the most recent previous monomer

      call ucase(line)
      ip(1)=index(line,'DU_1=')
      ip(2)=index(line,'DU_2=')
      ip(3)=index(line,'DU_3=')

      do i=1,3
        string=line(ip(i)+5:)
        call leftjust(string)
        ie=index(string,' ')-1
        call parser(string(1:ie),element,ls,i1,i2,i3)
c       expect to find i1 and maybe i2, but not i3
        if(i2.lt.0) i2=im !unless otherwise, need to match most recent previous monomer id
        imatch=0
        do j=icptr,1,-1
          call parser(atname(j),atsite,latsite,ia1,ia2,ia3)
c         write(*,'('' atname('',i2,'')='',a,'' ia1,2,3='',3i5)')
c    x       j,atname(j),ia1,ia2,ia3
          if(atsite(1:latsite).eq.element(1:ls) .and.
     x       ia2.eq.i1 .and. ia3.eq.i2) then
            imatch=j
          endif
        enddo
        idum=0
        if(imatch.eq.0) then
          if(element(1:ls).eq.'DU') then
            write(*,'('' Using dummy site of first repeat unit'')')
            idum=i1
          else
            write(*,'('' Match not found for '',a)') string
            write(*,'('' i1,i2,i3,im = '',4i5)') i1,i2,i3,im
            stop 'match not found'
          endif
        endif
        iref(i)=imatch
       write(*,'('' Match found: '',i2,'' *'',a,''* = *'',a,''*'')')
     x    imatch, string(1:index(string,        ' ')-1),
     x    atname(imatch)(1:index(atname(imatch),' ')-1)
        do k=1,3
          if(idum.eq.0) then
            refsite(k,i)=coord(k,imatch)
          else
            refsite(k,i)=dummy(k,idum)
          endif
        enddo
      enddo

      end

c
c     subroutine lstchr returns the index of the last nonblank character in a string
c
      subroutine lstchr(string,ilst)
      character*(*) string 

      ilst=0  !if string is all blank, return zero
      do i=len(string),1,-1
        if(string(i:i).ne.' ') then
          ilst=i
          go to 10
        endif
      enddo
10    continue

      end

c
c     subroutine to do a cheap display
c
      subroutine display(coord,atname,icptr)
      implicit real*8 (a-h,o-z)
      dimension coord(3,icptr)
      character*(*) atname(icptr)
      character*100 lines(50),string

      lhigh=50  !display lines
      lwide=100 !line width

      do i=1,lhigh
        lines(i)=' '
      enddo

      xmin=coord(1,1)
      xmax=coord(1,1)
      ymin=coord(2,1)
      ymax=coord(2,1)

      do i=1,icptr
        if(coord(1,i).gt.xmax) xmax=coord(1,i)
        if(coord(1,i).lt.xmin) xmin=coord(1,i)
        if(coord(2,i).gt.ymax) ymax=coord(2,i)
        if(coord(2,i).lt.ymin) ymin=coord(2,i)
      enddo

      do i=1,icptr
c       xcoord determines the character
        ic=min(lwide,
     x    int(1+lwide*(coord(1,i)-xmin)/(xmax-xmin)))
c       ycoord determines the line
        il=min(lhigh,
     x    int(1+lhigh*(coord(2,i)-ymin)/(ymax-ymin)))
        il=lhigh-il+1
        lines(il)(ic:ic)=atname(i)(1:1)
c       call parser(atname(i),elfield,lelfield,i1,i2,i3)
c       write(string(1:1),'(i1)') mod(i,10)
c       lines(il)(ic:ic)=string(1:1)
      enddo
  
      do i=1,lhigh
        write(*,'(a)') lines(i)
      enddo








      end


c
c     subroutine to write out the coordinates as a Geometry block
c     write out a pdb file
c
      subroutine wrtpoly(coord,newatname,neworder,icptr,mname,iadd,
     x  lbonds,natmax,fileset)
      implicit real*8 (a-h,o-z)
      dimension coord(3,icptr),neworder(2,icptr)
      dimension lbonds(natmax,2,6)
      character*(*) newatname(icptr),mname(0:iadd)
      character*10 ele
      character*120 fileset

      isfs=index(fileset,'.bin')-1
      open(11,file=fileset(1:isfs)//'.geo')

      write(11,'(''Geometry'')')
      do i=1,icptr
        ix=neworder(1,i)
        write(11,'(a,2x,3f15.5)') newatname(i),(coord(k,ix),k=1,3)
      enddo
      write(11,'(''End Geometry'')')

      close(11)


      open(11,file=fileset(1:isfs)//'.pdb')

      do i=1,icptr
        call parser(newatname(i),ele,lele,i1,i2,i3)
        ix=neworder(1,i)
        if(i3.lt.0) i3=0
c       write(11,'(a4,1x,i6,2x,a2,  2x,a3,1x,a1, i4,4x,3f8.3)') 
        write(11,'(a6,i5,1x,a4,    a1,a3,1x,a1, i4,4x,3f8.3)') 
     x    'HETATM', i, ele(1:2)//'  ', 
     x    ' ',mname(i3)(1:3), 'A', 
     x    i3,(coord(k,ix),k=1,3)
      enddo

      do i=1,icptr
        ix=neworder(1,i)
        nb=lbonds(ix,1,1)
        write(11,'(''CONECT'',10i5)')
     x    i,(neworder(2,lbonds(ix,1,j+1)),j=1,nb)
      enddo

      close(11)


      end

c
c     subroutine to place the dummy atom sites so that 
c     when the monomer is built, the first atom is at
c     the origin, etc.
c
c     this simpler code replaces earlier code (included below as getrefsitex)
c     in this one we build the entire repeat unit, then translate
c     the first real site to origin, etc.  This is easier than trying
c     to handle all the cases possible about strange z-matrices
c
c
      subroutine getrefsite(zmat,izmat,nzatmax,natmonomer,ndummy,coord)
      implicit real*8 (a-h,o-z)
      dimension zmat(nzatmax,3),izmat(nzatmax,3)
      dimension ref(3,3),coord(3,3)

      dimension lnk(3,100),xyz(3,100),the(100),dih(100) !local

      if(natmonomer.gt.100) then
        write(*,'('' Error:  increase array size in getrefsite'')')
        write(*,'('' Local arrays need at least '',i5)') 
     x    natmonomer
        stop 'increase local array size in getrefsite'
      endif

      pi=4.d0*datan(1.d0)
      do i=1,natmonomer
        lnk(1,i)=izmat(i,1)
        lnk(2,i)=izmat(i,2)
        lnk(3,i)=izmat(i,3)
        the(i)=(pi/180.d0)*zmat(i,2)
        dih(i)=(pi/180.d0)*zmat(i,3)
      enddo
c
c     call i2x to build the entire repeat unit
c
      nat=natmonomer
      istart=1
      iend=natmonomer
      call i2x(nat,lnk,zmat(1,1),the,dih,istart,iend,xyz)
      do i=1,natmonomer
        write(*,'('' Site '',i3,'' XYZ:'',3f10.5)') i,(xyz(j,i),j=1,3)
      enddo
c
c     translate so first nondummy site is at orogin
c
      trx=xyz(1,ndummy+1)
      try=xyz(2,ndummy+1)
      trz=xyz(3,ndummy+1)
      do i=1,natmonomer
        xyz(1,i)=xyz(1,i)-trx
        xyz(2,i)=xyz(2,i)-try
        xyz(3,i)=xyz(3,i)-trz
      enddo

      if(natmonomer-ndummy.eq.1) go to 500

c
c     rotate location of particle 2 onto the x axis
c
      phi=datan2(xyz(2,ndummy+2),xyz(1,ndummy+2))
c     rotate by -phi about z-axis to put particle in xz plane
      cphi=dcos(phi)
      sphi=dsin(-phi)
      do i=1,natmonomer
        xd= cphi*xyz(1,i) - sphi*xyz(2,i)
        yd= sphi*xyz(1,i) + cphi*xyz(2,i)
        xyz(1,i)=xd
        xyz(2,i)=yd
      enddo

      phi=datan2(xyz(3,ndummy+2),xyz(1,ndummy+2))
c     rotate by +phi about y-axis to put particle on x axis  
      cphi=dcos(phi)
      sphi=dsin(phi)
      do i=1,natmonomer
        xd= cphi*xyz(1,i) + sphi*xyz(3,i)
        zd=-sphi*xyz(1,i) + cphi*xyz(3,i)
        xyz(1,i)=xd
        xyz(3,i)=zd
      enddo

      if(natmonomer-ndummy.eq.2) then
c       for this special case, rotate the third dummy to the
c       third quadrant (negative x and y; z=0)
        phi=datan2(xyz(3,ndummy),-xyz(2,ndummy))
c       rotate by +phi about x-axis to put Du_3 in -x -y quadrant
        cphi=dcos(phi)
        sphi=dsin(phi)
        do i=1,natmonomer
          yd= cphi*xyz(2,i) - sphi*xyz(3,i)
          zd= sphi*xyz(2,i) + cphi*xyz(3,i)
          xyz(2,i)=yd
          xyz(3,i)=zd
        enddo
        go to 500
      endif

c
c     rotate location of particle 3 into the xy plane
c
      phi=datan2(xyz(3,ndummy+3),xyz(2,ndummy+3))
c     rotate by -phi about x-axis to put particle in xy plane  
      cphi=dcos(phi)
      sphi=dsin(-phi)
      do i=1,natmonomer
        yd= cphi*xyz(2,i) - sphi*xyz(3,i)
        zd= sphi*xyz(2,i) + cphi*xyz(3,i)
        xyz(2,i)=yd
        xyz(3,i)=zd
      enddo


500   continue

      do i=1,natmonomer
        write(*,'('' Site '',i3,'' XYZ:'',3f10.5)') i,(xyz(j,i),j=1,3)
      enddo

      do i=1,ndummy
        do j=1,3
          coord(j,i)=xyz(j,i)
        enddo
      enddo


      end




c
c     subroutine to place the dummy atom sites so that 
c     when the monomer is built, the first atom is at
c     the origin, etc.
c
      subroutine getrefsitex(zmat,izmat,nzatmax,natmonomer,ndummy,coord)
      implicit real*8 (a-h,o-z)
      dimension zmat(nzatmax,3),izmat(nzatmax,3)
      dimension ref(3,3),coord(3,3)
c
c     placement of dummy sites requires us to assume something about
c     the structure of the zmatrix
c
c     assume zmatrix references have form (others can be handled with more code)
c     D1             
c     D2   D1                                         
c     D3   D2   D1
c    [D4   D3   D2   D1]  - not used, if it exists
c     x1   D3   D2   D1
c     x2   x1   D3   D2
c     x3   x2   x1   D3
c
c     placement of x1, x2, x3 sites at usual sites (origin, x-axis, xy-plane)
c     would use r(x1-x2) to place x2, and theta(x3-x2-x1) to place x3
c
c     what do we need to place D3?
c     r(x1-D3), theta(x2-x1-D3) and phi(x3-x2-x1-D3)
c     check that these elements are there

c     first dummy site to locate
      jsite=izmat(ndummy+1,1)   !(=3, usually) to which dummy is the first real particle connected?
      r1d3=zmat(ndummy+1,1)     !distance to jsite dummy from first real site

c     special case if only one or two real sites
      if(natmonomer-ndummy.eq.1) then
        t21d3=90.d0       
        p321d3=180.d0
        go to 100
      elseif(natmonomer-ndummy.eq.2) then
        t21d3=90.d0     !this was a bug fix of 4/2011, but might not be right; orig code has this missing  
        p321d3=180.d0
        go to 100
      endif

      
      if(izmat(ndummy+2,1).ne.ndummy+1.or.
     x   izmat(ndummy+2,2).ne.jsite) then 
        write(*,'(''missing theta(x2-x1-D3) in z-matrix'')')
        stop 'missing theta(x2-x1-D3) in z-matrix'
      else
        t21d3=zmat(ndummy+2,2)
      endif

      if(izmat(ndummy+3,1).ne.ndummy+2.or.
     x   izmat(ndummy+3,2).ne.ndummy+1.or.
     x   izmat(ndummy+3,3).ne.jsite) then 
        write(*,'(''missing phi(x3-x2-x1-D3) in z-matrix'')')
        stop 'missing phi(x3-x2-x1-D3) in z-matrix'
      else
        p321d3=zmat(ndummy+3,3)
      endif

c     get to here if we have identified what is needed to position D3 
100   continue   
c     write(*,'('' Know how to position D3 site = '',i2)') jsite

      ref(1,1)=1.d0
      ref(2,1)=1.d0
      ref(3,1)=0.d0
      ref(1,2)=1.d0
      ref(2,2)=0.d0
      ref(3,2)=0.d0
      ref(1,3)=0.d0
      ref(2,3)=0.d0
      ref(3,3)=0.d0
      call place(ref,r1d3,t21d3,p321d3,coord)

c     what do we need to position D2 (given D3 is known)?
c     r(D2-D3), theta(x1-D3-D2), phi(x2-x1-D3-D2)
c     check that these elements are there

      if(izmat(jsite,1).ne.jsite-1) then 
        write(*,'(''missing r(D2-D3) in z-matrix'')')
        stop 'missing r(D2-D3) in z-matrix'
      else
        rd2d3=zmat(jsite,1)
      endif

      if(izmat(ndummy+1,2).ne.jsite-1) then 
        write(*,'(''missing theta(D2-D3-x1) in z-matrix'')')
        stop 'missing theta(D2-D3-x1) in z-matrix'
      else
        td2d31=zmat(ndummy+1,2)
      endif

c     special case if only one or two real sites
      if(natmonomer-ndummy.eq.1) then
        pd2d312=180.d0
        go to 110
      endif

      if(izmat(ndummy+2,3).ne.jsite-1) then 
        write(*,'(''missing phi(D2-D3-x1-x2) in z-matrix'')')
        stop 'missing phi(D2-D3-x1-x2) in z-matrix'
      else
        pd2d312=zmat(ndummy+2,3)
      endif

c     get to here if we have identified what is needed to position D2 
110   continue
c     write(*,'('' Know how to position D2 site = '',i2)') jsite-1

      ref(1,1)=1.d0
      ref(2,1)=0.d0
      ref(3,1)=0.d0
      ref(1,2)=0.d0
      ref(2,2)=0.d0
      ref(3,2)=0.d0
      ref(1,3)=coord(1,1)
      ref(2,3)=coord(2,1)
      ref(3,3)=coord(3,1)
      call place(ref,rd2d3,td2d31,pd2d312,coord)

c     what do we need to position D1 (given D2 and D3 are known)?
c     r(D1-D2), theta(D1-D2-D3), phi(x1-D3-D2-D1)
c     check that these elements are there

      if(izmat(jsite-1,1).ne.jsite-2) then 
        write(*,'(''missing r(D1-D2) in z-matrix'')')
        stop 'missing r(D1-D2) in z-matrix'
      else
        rd1d2=zmat(jsite-1,1)
      endif

      if(izmat(jsite,2).ne.jsite-2) then 
        write(*,'(''missing theta(D1-D2-D3) in z-matrix'')')
        stop 'missing theta(D1-D2-D3) in z-matrix'
      else
        td1d2d3=zmat(jsite,2)
      endif

      if(izmat(ndummy+1,3).ne.jsite-2) then 
        write(*,'(''missing phi(D1-D2-D3-x1) in z-matrix'')')
        stop 'missing phi(D1-D2-D3-x1) in z-matrix'
      else
        pd1d2d31=zmat(ndummy+1,3)
      endif

c     get to here if we have identified what is needed to position D1 
c     write(*,'('' Know how to position D2 site = '',i2)') jsite-2

      ref(1,2)=ref(1,3) 
      ref(2,2)=ref(2,3) 
      ref(3,2)=ref(3,3) 

      ref(1,3)=coord(1,1)
      ref(2,3)=coord(2,1)
      ref(3,3)=coord(3,1)

      ref(1,1)=0.d0
      ref(2,1)=0.d0
      ref(3,1)=0.d0
      call place(ref,rd1d2,td1d2d3,pd1d2d31,coord)

      ref(1,1)=ref(1,2) 
      ref(2,1)=ref(2,2) 
      ref(3,1)=ref(3,2) 

      ref(1,2)=ref(1,3) 
      ref(2,2)=ref(2,3) 
      ref(3,2)=ref(3,3) 

      ref(1,3)=coord(1,1)
      ref(2,3)=coord(2,1)
      ref(3,3)=coord(3,1)

      do i=1,3
        do j=1,3
          coord(j,i)=ref(j,4-i)  !note this arranges them in coord in order (D1,D2,D3)
        enddo
      enddo

c     write(*,'('' Reference site Du_'',i1,'': '',3f10.4)')
c    x  (i,(coord(j,i),j=1,3),i=1,3)

      end





c
c     subroutine to return the zmatrix for a monomer
c     as well as other information such as bond list and atom typing
c
      subroutine getmonomer(line,iseed,zmonomer,izmonomer,msites,
     x  nzatmax,natmonomer,ndummy,lstbnd,mname)
      implicit real*8 (a-h,o-z) 
      external ran
      dimension zmonomer(nzatmax,3),izmonomer(nzatmax,3)
      dimension knbnds(100,3),lstbnd(nzatmax,2,6)
      character*(*) line,msites(nzatmax),mname
      character*120 string,fname


      nbndmax=100   !size of knbnds
      
      do i=1,nzatmax
        do j=1,6
          lstbnd(i,1,j)=0
          lstbnd(i,2,j)=0
        enddo
        do j=1,3
          izmonomer(i,j)=0
          zmonomer(i,j)=0.d0
        enddo
      enddo


c     first token in line tells what monomer
      string=line
      call leftjust(string)
      is=index(string,' ')-1
      call ucase(string(1:is))
      mname=string(1:is)
      montype=0

c
c     need to add:   ace, pe, ps, benzen, pdvl, pdla, plla
c
      if(string(1:is).eq.'DME') then
        montype=1
      elseif(string(1:is).eq.'DMC') then
        montype=2
      elseif(string(1:is).eq.'PLA') then
        montype=3
      elseif(string(1:is).eq.'C') then
        montype=4
      elseif(string(1:is).eq.'T') then
        montype=5
      elseif(string(1:is).eq.'A') then
        montype=6
      elseif(string(1:is).eq.'ACE') then
        montype=7
      elseif(string(1:is).eq.'PLAL') then
        montype=8
      elseif(string(1:is).eq.'PLAD') then
        montype=9
      elseif(string(1:is).eq.'PDVL') then
        montype=10
      elseif(string(1:is).eq.'BENZ') then
        montype=11
      elseif(string(1:is).eq.'PS') then
        montype=12
      elseif(string(1:is).eq.'PE') then
        montype=13
      elseif(string(1:is).eq.'POXA') then
        montype=14
      elseif(string(1:is).eq.'MPA') then
        montype=15
      elseif(string(1:is).eq.'PENT') then
        montype=16
      elseif(string(1:is).eq.'KET') then
        montype=17
      elseif(string(1:is).eq.'LLAC') then
        montype=18
      elseif(string(1:is).eq.'DLAC') then
        montype=19
      elseif(string(1:is).eq.'ACET') then
        montype=20
      elseif(string(1:is).eq.'DIAMIDE') then
        montype=21
      elseif(string(1:is).eq.'GELCORE') then
        montype=22
      elseif(string(1:is).eq.'ETOH') then
        montype=23
      elseif(string(1:is).eq.'IBUPR') then
        montype=24
      elseif(string(1:is).eq.'DIMEET') then
        montype=25
      elseif(string(1:is).eq.'CARBO1') then
        montype=26
      elseif(string(1:is).eq.'AMAND1') then
        montype=27
      elseif(string(1:is).eq.'AMAND2') then
        montype=28
      elseif(string(1:is).eq.'AMAND3') then
        montype=29
      elseif(string(1:is).eq.'AMAND4') then
        montype=30
      elseif(string(1:is).eq.'IMSULF') then
        montype=32
      elseif(string(1:is).eq.'EKET') then
        montype=33
      elseif(string(1:is).eq.'MEOX') then
        montype=34
      elseif(string(1:is).eq.'PFE') then
        montype=35
      elseif(string(1:is).eq.'OPENGEL') then
        montype=36
      elseif(string(1:is).eq.'TOLUENE') then
        montype=37
      elseif(string(1:is).eq.'METHANOL') then
        montype=38
      elseif(string(1:is).eq.'THF') then
        montype=39
      elseif(string(1:is).eq.'MOXE') then
        montype=40
      elseif(string(1:is).eq.'PDVX') then
        montype=41
      elseif(string(1:is).eq.'POXB') then
        montype=42
      elseif(string(1:is).eq.'POXC') then
        montype=43
      elseif(string(1:is).eq.'PMMAA') then
        montype=44
      else
        write(*,'('' Unrecognized monomer type: '',a)')
     x    string(1:is)
        stop 'Error:  unrecognized monomer type'
      endif

      msites(1)='Du_1'
      msites(2)='Du_2'
      msites(3)='Du_3'
      msites(4)='Du_4'

      izmonomer(1,1)=0    !first dummy site 
      zmonomer(1,1)=0.d0
      izmonomer(1,2)=0
      zmonomer(1,2)=0.d0
      izmonomer(1,3)=0
      zmonomer(1,3)=0.d0

      if(montype.eq.1) then

c
c     DME                                                
c
      call getdmetor(th1,th2,th3,iseed)
      call getdmetor(th4,th5,th6,iseed)

      th1=180.d0
      th2=180.d0
      th3=180.d0
      th4=180.d0
      th5=180.d0
      th6=180.d0

      natmonomer=9  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0 !DME OPLS-AA
      rco=1.410d0 !DME OPLS-AA
      tcco=109.5d0 !DME OPLS-AA
      tcoc=109.5d0 !DME OPLS-AA

c     first dummy site surrogate for C_4
c     second dummy site surrogate for O_5
c     third dummy site surrogate for C_6

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0
 
      msites(4)='C_1'
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcc 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcco
      izmonomer(4,3)=1
      zmonomer(4,3)=th1   !first linkage torsion  180=trans (cocc)

      msites(5)='O_2'
      izmonomer(5,1)=4    
      zmonomer(5,1)=rco 
      izmonomer(5,2)=3     
      zmonomer(5,2)=tcco
      izmonomer(5,3)=2
      zmonomer(5,3)=th2   !second linkage torsion  180=trans (occo)

      msites(6)='C_3'
      izmonomer(6,1)=5    
      zmonomer(6,1)=rco 
      izmonomer(6,2)=4     
      zmonomer(6,2)=tcoc
      izmonomer(6,3)=3
      zmonomer(6,3)=th3   !third linkage torsion  180=trans (cocc)

      msites(7)='C_4'
      izmonomer(7,1)=6    
      zmonomer(7,1)=rcc 
      izmonomer(7,2)=5     
      zmonomer(7,2)=tcco
      izmonomer(7,3)=4
      zmonomer(7,3)=th4   !first conformational torsion (ccoc)

      msites(8)='O_5'
      izmonomer(8,1)=7    
      zmonomer(8,1)=rco 
      izmonomer(8,2)=6     
      zmonomer(8,2)=tcco
      izmonomer(8,3)=5
      zmonomer(8,3)=th5   !second conformational torsion (occo)

      msites(9)='C_6'
      izmonomer(9,1)=8    
      zmonomer(9,1)=rco 
      izmonomer(9,2)=7     
      zmonomer(9,2)=tcoc
      izmonomer(9,3)=6
      zmonomer(9,3)=th6   !third conformational torsion (cocc)

      nbnds=5
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo






      elseif(montype.eq.2) then

c
c     DMC                                               
c
      natmonomer=9  !monomer sites, including three dummy sites 
      ndummy=3

      rco=1.203d0  !C=O DMC expt
      rcox=1.443d0 !C(=O)-O- DMC expt
      rcoy=1.423d0 !-C-O- DMC expt

      tocox=126.5d0 !-O-C=O DMC expt
      tocoy=107.0d0 !-O-C(=O)-O- DMC expt    
      tcoc =114.5d0 !C(=O)-O-C- DMC expt

      rcc=1.529d0 !borrowed from DME  
      tcco=109.5d0 !borroed from DME

c     first dummy site surrogate for carbonyl carbon (C_3)
c     second dummy site surrogate for ether oxygen (O_4)
c     third dummy site surrogate methyl carbon (C_5)

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=rcox
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=rcoy
      izmonomer(3,2)=1
      zmonomer(3,2)=tcoc 
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1'
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcc 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcco
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0   !first linkage torsion  180=trans [c(=o)-o-c]-c

      msites(5)='O_2'  !ether oxygen
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcoy
      izmonomer(5,2)=3     
      zmonomer(5,2)=tcco
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 !second linkage torsion  180=trans [-o-c]-c-o-

      msites(6)='C_3'  !carbonyl carbon
      izmonomer(6,1)=5    
      zmonomer(6,1)=rcox
      izmonomer(6,2)=4     
      zmonomer(6,2)=tcoc
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0 !third linkage torsion  180=trans [c]-c-o-c(=o)

      msites(7)='O_4'  !ether oxygen
      izmonomer(7,1)=6    
      zmonomer(7,1)=rcox 
      izmonomer(7,2)=5     
      zmonomer(7,2)=tocoy
      izmonomer(7,3)=4
      zmonomer(7,3)=180.d0 !first conformational torsion (o-c(=o)-o-c) within +/-20degrees of 180

      msites(8)='C_5'  !methyl carbon
      izmonomer(8,1)=7    
      zmonomer(8,1)=rcoy
      izmonomer(8,2)=6     
      zmonomer(8,2)=tcoc
      izmonomer(8,3)=5
      zmonomer(8,3)=180.d0  !second conformational torsion (-c-o-c(=o)-o-) as above 

      msites(9)='O_6'  !carbonyl oxygen
      izmonomer(9,1)=6    
      zmonomer(9,1)=rco 
      izmonomer(9,2)=5     
      zmonomer(9,2)=tocox
      izmonomer(9,3)=7
      zmonomer(9,3)=180.d0 !keep at 180  (actually an impropoer torsion; keeps -O-CO-O- group flat)                


      nbnds=5
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
c     need 4-5 5-6 6-7 7-8 6-9 (double)
c     have 4-5 5-6 6-7 7-8 8-9 (single)
      knbnds(5,1)=6
      knbnds(5,3)=2







      elseif(montype.eq.3) then

c
c     PLA                                               
c
      natmonomer=8  !monomer sites, including three dummy sites 
      ndummy=3

c     first dummy site surrogate for stereo center carbon (C_1)  
c     second dummy site surrogate for carbonyl carbon (C_2)    
c     third dummy site surrogate for ether oxygen (O_3) 

      rcc=1.52d0 !-C(=O)-C- Lin Liu He, Polymer v51 p2779-2785 (2010)
      rccx=1.54d0 !-C-CH3 LLH
      rcox=1.34d0 !-O-C(=O)-
      rcoy=1.44d0 !-C-O-
      rcoz=1.22d0 !C=O LLH

      toccx=114.d0 !-O-C(=O)-C- LLH
      tcocx=113.d0 !-C-O-C(=O)- LLH
      toccy=110.d0 !-C(=O)-C-O- LLH
      toccz=121.d0 !-C-C=O LLH
      tccc=109.5d0 !-C(=O)-C-CH3 (made up by Bill)


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=rcc 
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=rcox
      izmonomer(3,2)=1
      zmonomer(3,2)=toccx
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' !stereo carbon
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcoy
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcocx
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0   !first linkage torsion  180=trans [-C-C(=O)-O-]-C

      msites(5)='C_2'  !carbonyl carbon
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcc 
      izmonomer(5,2)=3     
      zmonomer(5,2)=toccy
      izmonomer(5,3)=2
      zmonomer(5,3)=-73.d0 !second linkage torsion  180=trans [-C(=O)-O]-C-C(=O)-
      zmonomer(5,3)=-71.5d0 !Hans                                                  
c     zmonomer(5,3)=180.d0 !trans =>  180 

      msites(6)='O_3'  !ether oxygen    
      izmonomer(6,1)=5    
      zmonomer(6,1)=rcox
      izmonomer(6,2)=4     
      zmonomer(6,2)=toccx
      izmonomer(6,3)=3
      zmonomer(6,3)=160.d0 !third linkage torsion  180=trans [-O]-C-C(=O)-O-
      zmonomer(6,3)=161.8d0 !Hans                                             
c     zmonomer(6,3)=180.d0 !trans =>  180 

      msites(7)='O_4'  !carbonyl oxygen
      izmonomer(7,1)=5    
      zmonomer(7,1)=rcoz 
      izmonomer(7,2)=4     
      zmonomer(7,2)=toccz
      izmonomer(7,3)=3
      zmonomer(7,3)=160.d0-180.d0 !first conformational torsion (O=C-C-O) within +/-20degrees of 0
      zmonomer(7,3)=-20.d0 !Hans
c     zmonomer(7,3)=0.d0   !first conformational torsion (O=C-C-O) within +/-20degrees of 0
                           !this one is coupled to the previous one to keep -O-CO-O- group flat


      msites(8)='C_5'     !fifth real site, corresponds to methyl carbonyl (location determines stereo chemistry)
      izmonomer(8,1)=4
      zmonomer(8,1)=rccx  !dist to site C_1 (C-C single bond)    
      izmonomer(8,2)=5
      zmonomer(8,2)=tccc  ! C-C-C(=O) angle
      izmonomer(8,3)=7
      zmonomer(8,3)=98.d0   !this determines the sterochemistry +/-120                 
      zmonomer(8,3)=120.d0          



      nbnds=4
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
c     need 4-5 (c-c(=o)), 5-6 (c(=o)-o), 5-7 (c=o), 4-8 (c-c)
c     have 4-5            5-6            6-7        7-8

c     fix up
      knbnds(3,3)=2 !make c=o double bond
      knbnds(3,1)=5 !3rd bond should be 5-7 (C=o) (not 6-7)
      knbnds(4,1)=4 !4th bond should be 4-8 (c-c) (not 7-8)





      elseif(montype.eq.4) then

c
c     SINGLE CARBON ATOM TO CAP PLA CHAIN; OR TO LINK PLA TO DME                    
c

      natmonomer=4  !monomer sites, including three dummy sites 
      ndummy=3

c     first dummy site surrogate for stereo center carbon (C_1)  
c     second dummy site surrogate for carbonyl carbon (C_2)    
c     third dummy site surrogate for ether oxygen (O_3) 

      rcc=1.52d0 !-C(=O)-C- Lin Liu He, Polymer v51 p2779-2785 (2010)
      rcox=1.34d0 !-O-C(=O)-
      rcoy=1.44d0 !-C-O- LLH

      toccx=114.d0 !-O-C(=O)-C- LLH
      tcocx=113.d0 !-C-O-C(=O)- LLH

      izmonomer(2,1)=1    !this postions the second dummy site (carbonyl C) rel to first (stereo C)
      zmonomer(2,1)=rcc   !dist to site 1 (make into a -C(=O)-C- single bond), borrowed from DME     
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !this positions the third dummy site ether O rel to C(=O)
      zmonomer(3,1)=rcox 
      izmonomer(3,2)=1
      zmonomer(3,2)=toccx  !angle from 3-2-1 (surrogate for -O-C(=O)-C), in degrees, borrowed from DMC
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1'     !real site, corresponds to methyl/methlyene carbon 
      izmonomer(4,1)=3                    
      zmonomer(4,1)=rcoy  !dist to site 3 -C-O- single bond     
      izmonomer(4,2)=2
      zmonomer(4,2)=tcocx ! -C-O-C(=O)- angle 
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0  !torsion angle from 4-3-2-1 in degrees        
                          !this determines a [C-C(=O)-O]-C "first linkage" torsion angle
                          !BUT, preference is probably near 180

      nbnds=0





      elseif(montype.eq.5) then

c
c     TETRAHEDRON for star polymer core                  
c

      natmonomer=7  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0   !-C-C- single bond (from DME)
      tccc=109.5d0  !-C-C-C- angle


      izmonomer(2,1)=1    !this positions the second dummy site rel to first 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.0d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.0d0

      izmonomer(3,1)=2    !this positions the third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1'     
      izmonomer(4,1)=3
      zmonomer(4,1)=1.0d0
      izmonomer(4,2)=2
      zmonomer(4,2)=90.d0
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0         

      msites(5)='C_2'      
      izmonomer(5,1)=4
      zmonomer(5,1)=rcc  
      izmonomer(5,2)=3
      zmonomer(5,2)=tccc 
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0         

      msites(6)='C_3'      
      izmonomer(6,1)=5
      zmonomer(6,1)=rcc  
      izmonomer(6,2)=4
      zmonomer(6,2)=60.d0
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0         

      msites(7)='C_4'      
      izmonomer(7,1)=6
      zmonomer(7,1)=rcc  
      izmonomer(7,2)=5
      zmonomer(7,2)=60.d0
      izmonomer(7,3)=4
      zmonomer(7,3)=70.528d0      


      nbnds=6
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
c     have 4-5  5-6  6-7  7-8  8-9  9-10 
c     need 4-5  5-6  6-7  7-4  7-5  6-4

c     fix up
      knbnds(4,2)=4 !4th bond should be 7-4  (not 7-8)
      knbnds(5,1)=7 !4th bond should be 7-4  (not 7-8)
      knbnds(5,2)=5 !4th bond should be 7-4  (not 7-8)
      knbnds(6,1)=6 !4th bond should be 7-4  (not 7-8)
      knbnds(6,2)=4 !4th bond should be 7-4  (not 7-8)




      elseif(montype.eq.6) then

c
c     ADAMANTINE for star polymer core                  
c

      natmonomer=13 !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0   !-C-C- single bond (from DME)
      tccc=109.5d0  !-C-C-C- angle


      izmonomer(2,1)=1    !this positions the second dummy site rel to first 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.0d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.0d0

      izmonomer(3,1)=2    !this positions the third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0


      msites(4)='C_1'     
      izmonomer(4,1)=3
      zmonomer(4,1)=1.0d0
      izmonomer(4,2)=2
      zmonomer(4,2)=90.d0
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0         

      msites(5)='C_2'      
      izmonomer(5,1)=4
      zmonomer(5,1)=rcc  
      izmonomer(5,2)=3
      zmonomer(5,2)=90.d0
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0         

      msites(6)='C_3'      
      izmonomer(6,1)=5
      zmonomer(6,1)=rcc  
      izmonomer(6,2)=4
      zmonomer(6,2)=tccc 
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0         

      msites(7)='C_4'      
      izmonomer(7,1)=6
      zmonomer(7,1)=rcc  
      izmonomer(7,2)=5
      zmonomer(7,2)=tccc 
      izmonomer(7,3)=4
      zmonomer(7,3)=-60.d0       

      msites(8)='C_5'      
      izmonomer(8,1)=7
      zmonomer(8,1)=rcc  
      izmonomer(8,2)=6
      zmonomer(8,2)=tccc 
      izmonomer(8,3)=5
      zmonomer(8,3)= 60.d0       

      msites(9)='C_6'      
      izmonomer(9,1)=8
      zmonomer(9,1)=rcc  
      izmonomer(9,2)=7
      zmonomer(9,2)=tccc 
      izmonomer(9,3)=6
      zmonomer(9,3)=-60.d0       

c
c    second ring (add 3 sites)
c

      msites(10)='C_7'      
      izmonomer(10,1)=5
      zmonomer(10,1)=rcc  
      izmonomer(10,2)=4
      zmonomer(10,2)=tccc 
      izmonomer(10,3)=3
      zmonomer(10,3)=60.d0        

      msites(11)='C_8'      
      izmonomer(11,1)=10
      zmonomer(11,1)=rcc  
      izmonomer(11,2)=5
      zmonomer(11,2)=tccc 
      izmonomer(11,3)=4
      zmonomer(11,3)=60.d0        

      msites(12)='C_9'      
      izmonomer(12,1)=11
      zmonomer(12,1)=rcc  
      izmonomer(12,2)=10
      zmonomer(12,2)=tccc 
      izmonomer(12,3)=5
      zmonomer(12,3)=-60.d0        


c     add last site that connects two rings


      msites(13)='C_10'      
      izmonomer(13,1)=11
      zmonomer(13,1)=rcc  
      izmonomer(13,2)=10
      zmonomer(13,2)=tccc 
      izmonomer(13,3)=5
      zmonomer(13,3)= 60.d0        


      nbnds=12
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
      knbnds(6,1)=9
      knbnds(6,2)=4

      knbnds(7,1)=10
      knbnds(7,2)=5 

      knbnds(8,1)=10
      knbnds(8,2)=11

      knbnds(9,1)=11
      knbnds(9,2)=12

      knbnds(10,1)=9
      knbnds(10,2)=12


      knbnds(11,1)=11
      knbnds(11,2)=13
      knbnds(12,1)=7
      knbnds(12,2)=13



      elseif(montype.eq.7) then

c
c     ACE for capping a PLA chiain                      
c

      natmonomer=7  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.5200d0  ! single C-C bond
      rcca=1.3400d0   
      rccb=1.4400d0
      rccc=1.2200d0
      tccc=109.5d0  !-C-C-C- angle



      izmonomer(2,1)=1    !this positions the second dummy site rel to first 
      zmonomer(2,1)=rcc
      izmonomer(2,2)=0
      zmonomer(2,2)=0.0d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.0d0

      izmonomer(3,1)=2    !this positions the third dummy site 
      zmonomer(3,1)=rcca  
      izmonomer(3,2)=1
      zmonomer(3,2)=114.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0


      msites(4)='C_1'     
      izmonomer(4,1)=3
      zmonomer(4,1)=rccb 
      izmonomer(4,2)=2
      zmonomer(4,2)=113.d0 
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0         

      msites(5)='C_2'     
      izmonomer(5,1)=4
      zmonomer(5,1)=rcc  
      izmonomer(5,2)=3
      zmonomer(5,2)=110.d0 
      izmonomer(5,3)=2
      zmonomer(5,3)=-71.500d0         

      msites(6)='O_3'     
      izmonomer(6,1)=5
      zmonomer(6,1)=rcca 
      izmonomer(6,2)=4
      zmonomer(6,2)=114.d0 
      izmonomer(6,3)=3
      zmonomer(6,3)=161.800d0         

      msites(7)='O_4'     
      izmonomer(7,1)=5
      zmonomer(7,1)=rccc 
      izmonomer(7,2)=4
      zmonomer(7,2)=121.d0 
      izmonomer(7,3)=3
      zmonomer(7,3)=-20.000d0         

      nbnds=3 
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
      knbnds(3,1)=5
      knbnds(3,3)=2  !double bond




      elseif(montype.eq.8.or.montype.eq.9) then

c
c     PLAL:  Poly L-lactic acid   (montype=8)                                          
c     PLAD:  Poly D-lactic acid   (montype=9)                               
c
      natmonomer=8  !monomer sites, including three dummy sites 
      ndummy=3

c     first dummy site surrogate for stereo center carbon (C_1)  
c     second dummy site surrogate for carbonyl carbon (C_2)    
c     third dummy site surrogate for ether oxygen (O_3) 

      iflg=0  !
      iflg=1  ! L_1
      iflg=2  ! L_2
      iflg=3  ! L_3
      iflg=4  ! D_1
      iflg=5  ! L_Gavin's special 120 degree design
c     iflg=6  ! D_Gavin's special 120 degree design
c     iflg=7  ! L_Bill's speical 120 degree design
c     iflg=8  ! D_Bill's speical 120 degree design





      rcc=1.52d0 !-C(=O)-C- Lin Liu He, Polymer v51 p2779-2785 (2010)
      rccx=1.54d0 !-C-CH3 LLH
      rcox=1.34d0 !-O-C(=O)-
      rcoy=1.44d0 !-C-O-
      rcoz=1.22d0 !C=O LLH

      toccx=114.d0 !-O-C(=O)-C- LLH
      tcocx=113.d0 !-C-O-C(=O)- LLH
      toccy=110.d0 !-C(=O)-C-O- LLH
      toccz=121.d0 !-C-C=O LLH
      tccc=109.5d0 !-C(=O)-C-CH3 (made up by Bill)


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=rcc 
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=rcox
      izmonomer(3,2)=1
      zmonomer(3,2)=toccx
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' !stereo carbon
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcoy
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcocx
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0   !first linkage torsion  180=trans [-C-C(=O)-O-]-C
      if(iflg.eq.1) zmonomer(4,3)=176.8d0  !L_1
      if(iflg.eq.2) zmonomer(4,3)=184.0d0  !L_2
      if(iflg.eq.3) zmonomer(4,3)=170.0d0  !L_3
      if(iflg.eq.4) zmonomer(4,3)=182.3d0  !D_1
      if(iflg.eq.5) zmonomer(4,3)=180.0d0  !L_Gavin
      if(iflg.eq.6) zmonomer(4,3)=180.0d0  !D_Gavin
      if(iflg.eq.7) zmonomer(4,3)=180.0d0  !L_Bill  
      if(iflg.eq.8) zmonomer(4,3)=180.0d0  !D_Bill 

      msites(5)='C_2'  !carbonyl carbon
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcc 
      izmonomer(5,2)=3     
      zmonomer(5,2)=toccy
      izmonomer(5,3)=2
      zmonomer(5,3)=-73.d0 !second linkage torsion  180=trans [-C(=O)-O]-C-C(=O)-
      zmonomer(5,3)=-71.5d0 !Hans                                                  
c     zmonomer(5,3)=180.d0 !trans =>  180 
      if(montype.eq.9) zmonomer(5,3)=-zmonomer(5,3)
      if(iflg.eq.1) zmonomer(5,3)=288.5d0  !L_1
      if(iflg.eq.2) zmonomer(5,3)=283.6d0  !L_2
      if(iflg.eq.3) zmonomer(5,3)=278.1d0  !L_3
      if(iflg.eq.4) zmonomer(5,3)= 66.1d0  !D_1
      if(iflg.eq.5) zmonomer(5,3)=278.1d0  !L_Gavin
      if(iflg.eq.6) zmonomer(5,3)=-278.1d0 !D_Gavin
      if(iflg.eq.7) zmonomer(5,3)=240.0d0  !L_Bill  
      if(iflg.eq.8) zmonomer(5,3)=-240.0d0 !D_Bill  

      msites(6)='O_3'  !ether oxygen    
      izmonomer(6,1)=5    
      zmonomer(6,1)=rcox
      izmonomer(6,2)=4     
      zmonomer(6,2)=toccx
      izmonomer(6,3)=3
      zmonomer(6,3)=160.d0 !third linkage torsion  180=trans [-O]-C-C(=O)-O-
      zmonomer(6,3)=161.8d0 !Hans                                             
c     zmonomer(6,3)=180.d0 !trans =>  180 
      if(montype.eq.9) zmonomer(6,3)=-zmonomer(6,3)
      if(iflg.eq.1) zmonomer(6,3)=135.5d0  !L_1
      if(iflg.eq.2) zmonomer(6,3)=122.6d0  !L_2
      if(iflg.eq.3) zmonomer(6,3)=155.1d0  !L_3
      if(iflg.eq.4) zmonomer(6,3)=229.0d0  !D_1
      if(iflg.eq.5) zmonomer(6,3)=155.2d0  !L_Gavin
      if(iflg.eq.6) zmonomer(6,3)=-155.2d0 !D_Gavin
      if(iflg.eq.7) zmonomer(6,3)=180.0d0  !L_Bill  
      if(iflg.eq.8) zmonomer(6,3)=-180.0d0 !D_Bill  

      msites(7)='O_4'  !carbonyl oxygen
      izmonomer(7,1)=5    
      zmonomer(7,1)=rcoz 
      izmonomer(7,2)=4     
      zmonomer(7,2)=toccz
      izmonomer(7,3)=3
      zmonomer(7,3)=160.d0-180.d0 !first conformational torsion (O=C-C-O) within +/-20degrees of 0
      zmonomer(7,3)=-20.d0 !Hans
      zmonomer(7,3)=-18.2d0 
c     zmonomer(7,3)=0.d0   !first conformational torsion (O=C-C-O) within +/-20degrees of 0
                           !this one is coupled to the previous one to keep -O-CO-O- group flat
      if(montype.eq.9) zmonomer(7,3)=-zmonomer(7,3)
      if(iflg.eq.1) zmonomer(7,3)=315.2d0  !L_1
      if(iflg.eq.2) zmonomer(7,3)=303.0d0  !L_2
      if(iflg.eq.3) zmonomer(7,3)=329.3d0  !L_3
      if(iflg.eq.4) zmonomer(7,3)= 45.3d0  !D_1
      if(iflg.eq.5) zmonomer(7,3)=-4.d0    !L_Gavin
      if(iflg.eq.6) zmonomer(7,3)= 4.d0    !D_Gavin
      if(iflg.eq.7) zmonomer(7,3)=-0.d0    !L_Bill 
      if(iflg.eq.8) zmonomer(7,3)= 0.d0    !D_Bill 


      msites(8)='C_5'     !fifth real site, corresponds to methyl carbonyl (location determines stereo chemistry)
      izmonomer(8,1)=4
      zmonomer(8,1)=rccx  !dist to site C_1 (C-C single bond)    
      izmonomer(8,2)=5
      zmonomer(8,2)=tccc  ! C-C-C(=O) angle
      izmonomer(8,3)=7
      zmonomer(8,3)=98.d0   !this determines the sterochemistry +/-120                 
      zmonomer(8,3)=120.d0          
      if(montype.eq.9) zmonomer(8,3)=-zmonomer(8,3)
      if(iflg.eq.1) zmonomer(8,3)= 70.9d0  !L_1
      if(iflg.eq.2) zmonomer(8,3)= 59.7d0  !L_2
      if(iflg.eq.3) zmonomer(8,3)= 84.5d0  !L_3
      if(iflg.eq.4) zmonomer(8,3)=287.2d0  !D_1
      if(iflg.eq.7) zmonomer(8,3)=120.d0    !L_Bill 
      if(iflg.eq.8) zmonomer(8,3)=240.d0    !D_Bill 

      nbnds=4
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
c     need 4-5 (c-c(=o)), 5-6 (c(=o)-o), 5-7 (c=o), 4-8 (c-c)
c     have 4-5            5-6            6-7        7-8

c     fix up
      knbnds(3,3)=2 !make c=o double bond
      knbnds(3,1)=5 !3rd bond should be 5-7 (C=o) (not 6-7)
      knbnds(4,1)=4 !4th bond should be 4-8 (c-c) (not 7-8)





      elseif(montype.eq.10) then

c
c     PDVL:  Poly D-valerolactone                              
c
      natmonomer=10  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0
      rcox=1.34d0 !-O-C(=O)-
      rcoy=1.44d0 !-C-O-
      rcoz=1.22d0 !C=O LLH
      rccx=1.52d0 

      tccc=109.5d0 
      tcocx=113.d0 !-C-O-C(=O)- LLH
      toccz=125.d0 
      toccx=114.d0 !-O-C(=O)-C- LLH


c     toccy=110.d0 !-C(=O)-C-O- LLH
c     toccz=121.d0 !-C-C=O LLH


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=rcc 
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=rcc 
      izmonomer(3,2)=1
      zmonomer(3,2)=tccc 
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' 
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcc 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tccc 
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0   !first linkage torsion  180=trans [-C-C(=O)-O-]-C

      msites(5)='C_2'  
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcc 
      izmonomer(5,2)=3     
      zmonomer(5,2)=tccc 
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 !trans =>  180 

      msites(6)='O_3'     
      izmonomer(6,1)=5    
      zmonomer(6,1)=rcoy
      izmonomer(6,2)=4     
      zmonomer(6,2)=tccc 
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0  

      msites(7)='C_4'  
      izmonomer(7,1)=6    
      zmonomer(7,1)=rcox 
      izmonomer(7,2)=5     
      zmonomer(7,2)=tcocx
      izmonomer(7,3)=4
      zmonomer(7,3)=180.d0   

      msites(8)='O_5'     
      izmonomer(8,1)=7
      zmonomer(8,1)=rcoz     
      izmonomer(8,2)=6
      zmonomer(8,2)=toccz
      izmonomer(8,3)=5
      zmonomer(8,3)=0.d0          

      msites(9)='C_6'     
      izmonomer(9,1)=7
      zmonomer(9,1)=rccx     
      izmonomer(9,2)=6
      zmonomer(9,2)=toccx
      izmonomer(9,3)=5
      zmonomer(9,3)=180.d0          

      msites(10)='C_7'     
      izmonomer(10,1)=9
      zmonomer(10,1)=rcc     
      izmonomer(10,2)=7
      zmonomer(10,2)=tccc 
      izmonomer(10,3)=6
      zmonomer(10,3)=180.d0          


      nbnds=6
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo

c     need 4-5 (c-c), 5-6 (c-o), 6-7 (o-c), 7-8 (c=o), 7-9 (c-c), 9-10 (c-c)
c     have 4-5        5-6        6-7        7-8        8-9        9-10

c     fix up
      knbnds(4,3)=2 !make c=o double bond
      knbnds(5,1)=7 !5th bond should be 7-9 (not 8-9)





      elseif(montype.eq.11) then

c
c     Benzene                       
c
      natmonomer=9  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0
      rccx=1.39d0 !-O-C(=O)-
      tccc=109.5d0 


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=rcc 
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=rcc 
      izmonomer(3,2)=1
      zmonomer(3,2)=tccc 
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' 
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcc 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tccc 
      izmonomer(4,3)=1
      zmonomer(4,3)=60.d0 

      msites(5)='C_2' 
      izmonomer(5,1)=4     
      zmonomer(5,1)=rccx
      izmonomer(5,2)=3     
      zmonomer(5,2)=120.d0
      izmonomer(5,3)=2
      zmonomer(5,3)=60.d0 

      msites(6)='C_3' 
      izmonomer(6,1)=5     
      zmonomer(6,1)=rccx
      izmonomer(6,2)=4     
      zmonomer(6,2)=120.d0
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0 

      msites(7)='C_4' 
      izmonomer(7,1)=6     
      zmonomer(7,1)=rccx
      izmonomer(7,2)=5     
      zmonomer(7,2)=120.d0
      izmonomer(7,3)=4
      zmonomer(7,3)=0.d0 

      msites(8)='C_5' 
      izmonomer(8,1)=7     
      zmonomer(8,1)=rccx
      izmonomer(8,2)=6     
      zmonomer(8,2)=120.d0
      izmonomer(8,3)=5
      zmonomer(8,3)=0.d0 

      msites(9)='C_6' 
      izmonomer(9,1)=8     
      zmonomer(9,1)=rccx
      izmonomer(9,2)=7     
      zmonomer(9,2)=120.d0
      izmonomer(9,3)=6
      zmonomer(9,3)=0.d0 

      nbnds=6
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=4   !aromatic bonds (type 4; save 1,2,3 for single, double, triple)
      enddo
c     fixup to close the loop
      knbnds(6,2)=4




      elseif(montype.eq.12) then

c
c     PS - polystyrene repeat unit is an ethyl benzene
c
      natmonomer=11  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0
      rccx=1.39d0 !-O-C(=O)-
      tccc=109.5d0 


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' 
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcc 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tccc 
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0 

      msites(5)='C_2' 
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcc
      izmonomer(5,2)=3     
      zmonomer(5,2)=tccc  
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 

      msites(6)='C_3' 
      izmonomer(6,1)=4     
      zmonomer(6,1)=rcc
      izmonomer(6,2)=3     
      zmonomer(6,2)=tccc  
      izmonomer(6,3)=2

      itac=1
      if(ran(iseed).ge.0.5d0) itac=-1
      write(*,'('' Polystyrene:  iseed '',i12,'' itac '',i3)')
     x  iseed,itac
c     most common is random 
c     random is atactic; all on same side is isotactic; alternating is syndiotactic
      zmonomer(6,3)=itac*(-60.d0)

      msites(7)='C_4' 
      izmonomer(7,1)=6     
      zmonomer(7,1)=rccx
      izmonomer(7,2)=4     
      zmonomer(7,2)=120.d0
      izmonomer(7,3)=3
      zmonomer(7,3)=120.d0 
      if(itac.eq.-1) zmonomer(7,3)=60.d0

      msites(8)='C_5' 
      izmonomer(8,1)=7     
      zmonomer(8,1)=rccx
      izmonomer(8,2)=6     
      zmonomer(8,2)=120.d0
      izmonomer(8,3)=4
      zmonomer(8,3)=180.d0 

      msites(9)='C_6' 
      izmonomer(9,1)=8     
      zmonomer(9,1)=rccx
      izmonomer(9,2)=7     
      zmonomer(9,2)=120.d0
      izmonomer(9,3)=6
      zmonomer(9,3)=0.d0 

      msites(10)='C_7' 
      izmonomer(10,1)=9     
      zmonomer(10,1)=rccx
      izmonomer(10,2)=8     
      zmonomer(10,2)=120.d0
      izmonomer(10,3)=7
      zmonomer(10,3)=0.d0 

      msites(11)='C_8' 
      izmonomer(11,1)=10    
      zmonomer(11,1)=rccx
      izmonomer(11,2)=9     
      zmonomer(11,2)=120.d0
      izmonomer(11,3)=8
      zmonomer(11,3)=0.d0 

      nbnds=8
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=4   !aromatic bonds (type 4; save 1,2,3 for single, double, triple)
      enddo
c     make two of the bonds single
      knbnds(1,3)=1
      knbnds(2,3)=1
c     fix the branch
      knbnds(2,1)=4
c     fixup to close the loop
      knbnds(8,2)=6






      elseif(montype.eq.13) then

c
c     PE - polyethylene  (repeat unit is -C-C-)
c
      natmonomer=5   !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0
      rccx=1.39d0 !-O-C(=O)-
      tccc=109.5d0 


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' 
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcc 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tccc 
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0 

      msites(5)='C_2' 
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcc
      izmonomer(5,2)=3     
      zmonomer(5,2)=tccc  
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 

      nbnds=1
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1
      enddo







      elseif(montype.eq.14) then

c
c     POXA - polyoxazoline (repeat unit is -C-N(-CO-CH3)-C-)
c
      natmonomer=9   !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0
      rcn=1.448d0
      rcon=1.335d0
      rcco=1.522d0  ! CT-C
      rccx=1.39d0 !-O-C(=O)-
      rcoz=1.229d0 !C=O oplsaa distance
      tccc=109.5d0 


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' 
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcc 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tccc 
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0 

      msites(5)='N_2' 
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcn    !should replace with -N-C- single bond from an amine
      izmonomer(5,2)=3     
      zmonomer(5,2)=tccc  
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 

      msites(6)='C_3' 
      izmonomer(6,1)=5     
      zmonomer(6,1)=rcn    !should replace with -N-C- single bond from an amine
      izmonomer(6,2)=4     
      zmonomer(6,2)=120.d0
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0 

      msites(7)='C_4'      !side chain carbon (carbonyl carbon)
      izmonomer(7,1)=5     
      zmonomer(7,1)=rcon   !should replace with -N-C- single bond from an amine
      izmonomer(7,2)=4     
      zmonomer(7,2)=120.d0
      izmonomer(7,3)=3
      zmonomer(7,3)=0.d0 

      msites(8)='C_5'      !methyl carbon                        
      izmonomer(8,1)=7     
      zmonomer(8,1)=rcco   !-C-C(=O) single bond from an aldehyde/ketone (CT-C )
      izmonomer(8,2)=5     
      zmonomer(8,2)=116.6  !-C-C(=O)-N angle (CT-C -N )
      izmonomer(8,3)=4
      zmonomer(8,3)=90.d0 

      msites(9)='O_6'      !carbonyl oxygen                      
      izmonomer(9,1)=7     
      zmonomer(9,1)=rcoz   !C=O bond
      izmonomer(9,2)=5     
      zmonomer(9,2)=122.9d0 ! O(=C)-C(=O)-N angle for oplsaa (O -C -N )
      izmonomer(9,3)=4
      zmonomer(9,3)=-90.d0 


      nbnds=5
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1
      enddo
c     need  4-5 (C-N); 5-6 (N-C); 5-7 (N-C); 7-8 (C-C); 7-9 (C=O)
      knbnds(5,3)=2  !double bond
      knbnds(3,1)=5
      knbnds(5,1)=7








      elseif(montype.eq.15) then

c
c     MPA:  2,2-bis (hydroxymethyl) propionic acid
c     used for making dendrimers
c
      natmonomer=11  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0
      rcox=1.34d0 !-O-C(=O)-
      rcoy=1.44d0 !-C-O-
      rco=1.410d0 !DME OPLS-AA
      rcoz=1.22d0 !C=O LLH

c     rccx=1.52d0 

      tcco=109.5d0 !DME OPLS-AA
      tcocx=113.d0 !-C-O-C(=O)- LLH
      toccx=114.d0 !-O-C(=O)-C- LLH
      tccc=109.5d0 
      toccz=121.d0 !-C-C=O LLH

c     tcoc=109.5d0 !DME OPLS-AA
c     toccz=125.d0 


cc    toccy=110.d0 !-C(=O)-C-O- LLH


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=rcc 
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=rcoy
      izmonomer(3,2)=1
      zmonomer(3,2)=tcco 
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' 
      izmonomer(4,1)=3    !first real site, carbonyl carbon
      zmonomer(4,1)=rcox
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcocx
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0   !first linkage torsion  180=trans -C(=O)-[O-C-C-]

      msites(5)='C_2'  
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcc 
      izmonomer(5,2)=3     
      zmonomer(5,2)=toccx
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0   !second linkage torsion  180=trans -C-C(=O)-[O-C-]

      msites(6)='C_3'  
      izmonomer(6,1)=5     
      zmonomer(6,1)=rcc 
      izmonomer(6,2)=4     
      zmonomer(6,2)=tccc 
      izmonomer(6,3)=3
      zmonomer(6,3)=+120.d0   !third linkage torsion -C-C-C(=O)-[O-]

      msites(7)='O_4'  
      izmonomer(7,1)=6     
      zmonomer(7,1)=rcoy
      izmonomer(7,2)=5     
      zmonomer(7,2)=tcco 
      izmonomer(7,3)=4
      zmonomer(7,3)=180.d0   !locate first terminal oxygen

      msites(8)='C_5'  
      izmonomer(8,1)=5     
      zmonomer(8,1)=rcc 
      izmonomer(8,2)=4     
      zmonomer(8,2)=tccc 
      izmonomer(8,3)=3
      zmonomer(8,3)=-120.d0   !third linkage torsion  -C-C-C(=O)-[O-]

      msites(9)='O_6'  
      izmonomer(9,1)=8     
      zmonomer(9,1)=rcoy
      izmonomer(9,2)=5     
      zmonomer(9,2)=tcco 
      izmonomer(9,3)=4
      zmonomer(9,3)=-60.d0   !locate second terminal oxygen

      msites(10)='C_7'  
      izmonomer(10,1)=5     
      zmonomer(10,1)=rcc 
      izmonomer(10,2)=4     
      zmonomer(10,2)=tccc 
      izmonomer(10,3)=3
      zmonomer(10,3)=0.d0   !locate methyl group          

      msites(11)='O_8'  
      izmonomer(11,1)=4     
      zmonomer(11,1)=rcoz
      izmonomer(11,2)=5     
      zmonomer(11,2)=toccz
      izmonomer(11,3)=10
      zmonomer(11,3)=180.d0   !locate carbonyl oxygen       


      nbnds=7
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1
      enddo
      knbnds(7,3)=2  !double bond
      knbnds(4,1)=5
      knbnds(6,1)=5
      knbnds(7,1)=4






      elseif(montype.eq.16) then

c
c     PENT:  Pentaerythritol
c     This is for the core alcohol to bind bis-MPA
c     used for making dendrimers
c
      natmonomer=12  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0
      rco=1.410d0 !DME OPLS-AA

c     rcoy=1.44d0 !-C-O-
c     rcox=1.34d0 !-O-C(=O)-
c     rcoz=1.22d0 !C=O LLH
c     rccx=1.52d0 


      tccc=109.5d0 
      tcco=109.5d0 !DME OPLS-AA
      tcoc=109.5d0 !DME OPLS-AA
c     tcocx=113.d0 !-C-O-C(=O)- LLH
c     toccx=114.d0 !-O-C(=O)-C- LLH
c     toccz=121.d0 !-C-C=O LLH
c     toccz=125.d0 
cc    toccy=110.d0 !-C(=O)-C-O- LLH


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=rcc 
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=rcc 
      izmonomer(3,2)=1
      zmonomer(3,2)=tccc 
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='O_1' 
      izmonomer(4,1)=3    !first real site, oxygen
      zmonomer(4,1)=rco 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcco 
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0   !first linkage torsion

      msites(5)='C_2'  
      izmonomer(5,1)=4     
      zmonomer(5,1)=rco 
      izmonomer(5,2)=3     
      zmonomer(5,2)=tcoc 
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0   !second linkage torsion  

      msites(6)='C_3'  
      izmonomer(6,1)=5     
      zmonomer(6,1)=rcc 
      izmonomer(6,2)=4     
      zmonomer(6,2)=tcco 
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0   !third linkage torsion 

      msites(7)='C_4'  
      izmonomer(7,1)=6     
      zmonomer(7,1)=rcc 
      izmonomer(7,2)=5     
      zmonomer(7,2)=tccc 
      izmonomer(7,3)=4
      zmonomer(7,3)=180.d0   

      msites(8)='O_5'        !second terminal oxygen
      izmonomer(8,1)=7     
      zmonomer(8,1)=rco 
      izmonomer(8,2)=6     
      zmonomer(8,2)=tcco 
      izmonomer(8,3)=5
      zmonomer(8,3)=180.d0  

      msites(9)='C_6'  
      izmonomer(9,1)=6     
      zmonomer(9,1)=rcc 
      izmonomer(9,2)=5     
      zmonomer(9,2)=tccc 
      izmonomer(9,3)=4
      zmonomer(9,3)=+60.d0   

      msites(10)='O_7'     !third terminal oxygen 
      izmonomer(10,1)=9     
      zmonomer(10,1)=rco 
      izmonomer(10,2)=6     
      zmonomer(10,2)=tcco 
      izmonomer(10,3)=5
      zmonomer(10,3)= 60.d0   

      msites(11)='C_8'  
      izmonomer(11,1)=6     
      zmonomer(11,1)=rcc 
      izmonomer(11,2)=5     
      zmonomer(11,2)=tccc 
      izmonomer(11,3)=4
      zmonomer(11,3)=-60.d0   

      msites(12)='O_9'     !fourth terminal oxygen 
      izmonomer(12,1)=11    
      zmonomer(12,1)=rco 
      izmonomer(12,2)=6     
      zmonomer(12,2)=tcco 
      izmonomer(12,3)=5
      zmonomer(12,3)=-60.d0   


      nbnds=8
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1
      enddo
      knbnds(5,1)=6
      knbnds(7,1)=6





      elseif(montype.eq.17) then

c
c     KET: CH3-C(=O)-   ketone (acetyl) capping group for a 'dangling'  alcohol
c
c     note that this is like ACE, but oriented in the other direction
c     ACE = -O-C(=O)-C- with the last (alkane) C as the linkage site
c     KET = CH3-C(=O)-  with the last (conbonyl) C as the linkage site
c
c
      natmonomer=6   !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0
      rcox=1.34d0 !-O-C(=O)-
      rcoz=1.22d0 !C=O LLH
c     rco=1.410d0 !DME OPLS-AA


c     rcoy=1.44d0 !-C-O-
c     rccx=1.52d0 

      tcco=109.5d0 !DME OPLS-AA
      tcoc=109.5d0 !DME OPLS-AA
      toccx=114.d0 !-O-C(=O)-C- LLH
      toccz=121.d0 !-C-C=O LLH

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=rcc 
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site  (an oxygen site usually)
      zmonomer(3,1)=rcc 
      izmonomer(3,2)=1
      zmonomer(3,2)=tcco 
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1'     !first real site, carbonyl carbon
      izmonomer(4,1)=3                              
      zmonomer(4,1)=rcox 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcoc 
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0   !first linkage torsion

      msites(5)='C_2'  
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcc 
      izmonomer(5,2)=3     
      zmonomer(5,2)=toccx
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0   !second linkage torsion  

      msites(6)='O_3'  
      izmonomer(6,1)=4     
      zmonomer(6,1)=rcoz
      izmonomer(6,2)=3     
      zmonomer(6,2)=toccz
      izmonomer(6,3)=2
      zmonomer(6,3)=0.d0   

      nbnds=2

      knbnds(1,1)=4
      knbnds(1,2)=5
      knbnds(1,3)=1

      knbnds(2,1)=4
      knbnds(2,2)=6
      knbnds(2,3)=2


      elseif(montype.eq.18.or.montype.eq.19) then

c
c     LLAC:  l-lactic acid oriented to grow from an exposed alcohol
c     DLAC:  d-lactic acid oriented to grow from an exposed alcohol
c
c
c     note that this is in contrast to PLA and PLAL and PLAD
c
c     PLA, PLAL, PLAD
c     -[O-CO-C(HCH3)]-  with the stero carbon connecting to dummy sites
c
c     LLAC, DLAC      
c     -[O-C(HCH3)-CO]-  with the carbonyl carbon connecting to dummy sites
c

      natmonomer=8   !monomer sites, including three dummy sites 
      ndummy=3


      rcc=1.52d0 !-C(=O)-C- Lin Liu He, Polymer v51 p2779-2785 (2010)
      rccx=1.54d0 !-C-CH3 LLH
      rcox=1.34d0 !-O-C(=O)-
      rcoy=1.44d0 !-C-O-
      rcoz=1.22d0 !C=O LLH

      toccx=114.d0 !-O-C(=O)-C- LLH
      tcocx=113.d0 !-C-O-C(=O)- LLH
      toccy=110.d0 !-C(=O)-C-O- LLH
      toccz=121.d0 !-C-C=O LLH
      tccc=109.5d0 !tetrahedral                      


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=rcc 
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=rcoy
      izmonomer(3,2)=1
      zmonomer(3,2)=tccc 
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' !carbonyl carbon
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcox
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcocx
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0   !first linkage torsion  180=trans 

      msites(5)='C_2'  !stereo center carbon
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcc 
      izmonomer(5,2)=3     
      zmonomer(5,2)=toccx
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 !second linkage torsion  180=trans 

      msites(6)='O_3'  !ether oxygen    
      izmonomer(6,1)=5    
      zmonomer(6,1)=rcoy
      izmonomer(6,2)=4     
      zmonomer(6,2)=toccy
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0 !third linkage torsion  180=trans 

      msites(7)='O_4'  !carbonyl oxygen
      izmonomer(7,1)=4    
      zmonomer(7,1)=rcoz 
      izmonomer(7,2)=5     
      zmonomer(7,2)=toccz
      izmonomer(7,3)=6
      zmonomer(7,3)=0.d0      !torsion (O=C-C-O) within +/-20degrees of 0
c     zmonomer(7,3)=0.d0      !first conformational torsion (O=C-C-O) within +/-20degrees of 0
c     zmonomer(7,3)=-20.d0 !Hans
c     zmonomer(7,3)=-18.2d0 

      msites(8)='C_5'     !fifth real site, corresponds to methyl carbonyl (location determines stereo chemistry)
      izmonomer(8,1)=5
      zmonomer(8,1)=rccx  !dist to site C_1 (C-C single bond)    
      izmonomer(8,2)=4
      zmonomer(8,2)=tccc  !this is the C-C-C(=O) angle
      izmonomer(8,3)=7
      zmonomer(8,3)=120.d0  !I think this corresponds to the L form of the stereoisomer        
      if(montype.eq.19) zmonomer(8,3)=-zmonomer(8,3)



      nbnds=4
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
c     need 4-5 (c-c(=o)), 5-6 (-c-o-), 4-7 (c=o), 5-8 (c-c)
c     have 4-5            5-6          6-7        7-8

c     fix up

      knbnds(3,3)=2 !make c=o double bond
      knbnds(3,1)=4 !3rd bond should be 4-7 (C=o) (not 6-7)
      knbnds(4,1)=5 !4th bond should be 5-8 (C-C) (not 7-8)



      elseif(montype.eq.20) then

c
c     ACET:  Acetone monomeric unit for making bulk liquid           
c
c
c     note that this is in contrast to ACE which is an acetyl
c     capping unit for a PLA polylactide chain
c
      natmonomer=7   !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.52d0 !-C(=O)-C- Lin Liu He, Polymer v51 p2779-2785 (2010)
      rcoz=1.22d0 !C=O LLH

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' !methyl carbon
      izmonomer(4,1)=3  
      zmonomer(4,1)=1.0d0
      izmonomer(4,2)=2     
      zmonomer(4,2)=90.d0 
      izmonomer(4,3)=1
      zmonomer(4,3)=0.d0    

      msites(5)='C_2'  !carbonyl carbon      
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcc  
      izmonomer(5,2)=3     
      zmonomer(5,2)=90.d0
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0  

      msites(6)='C_3'  !methyl carbon      
      izmonomer(6,1)=5     
      zmonomer(6,1)=rcc  
      izmonomer(6,2)=4     
      zmonomer(6,2)=120.d0
      izmonomer(6,3)=3
      zmonomer(6,3)=0.d0  

      msites(7)='O_4'  !carbonyl oxygen    
      izmonomer(7,1)=5     
      zmonomer(7,1)=rcoz  
      izmonomer(7,2)=4     
      zmonomer(7,2)=120.d0
      izmonomer(7,3)=3
      zmonomer(7,3)=180.d0  

      nbnds=3
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
c     have 4-5            5-6            6-7  
c     need 4-5 (c-c(=o)), 5-6 (c(=o)-c), 5-7 (c=o)

c     fix up
      knbnds(3,3)=2 !make c=o double bond
      knbnds(3,1)=5 !3rd bond should be 5-7 (C=o) (not 6-7)



      elseif(montype.eq.21) then

c
c     DIAMIDE:  Special molecule for Hans: three rings with amide bonds 
C     related to Kevlar when in polymer form
c
c     this block reads data from a zmatrix file
c

      fname='diamide_2.zmat'
      call getzmat(fname,zmonomer,izmonomer,msites,
     x nzatmax,natmonomer,ndummy,knbnds,nbndmax,nbnds)

c     msites is populated to msites(natmonomer)
c     izmonomer and zmonomer are populated to zmonomer(natmonomer,3)
c     knbnds is populated to knbnds(nbnds,3)
c     natmonomer, nbnds, ndummy are set


      elseif(montype.eq.22) then
c
c     GELCORE:  Double ring di-ester                                      
c     based on esterep
c
      natmonomer=17   !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0    ! c-c
      rco=1.229d0    ! c=o
      tccc=109.5d0 !tetrahedral                      

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' 
      izmonomer(4,1)=3  
      zmonomer(4,1)=1.0d0
      izmonomer(4,2)=2     
      zmonomer(4,2)=90.d0 
      izmonomer(4,3)=1
      zmonomer(4,3)=0.d0    

      msites(5)='C_2'        
      izmonomer(5,1)=4     
      zmonomer(5,1)=1.522d0
      izmonomer(5,2)=3     
      zmonomer(5,2)=tccc  
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0  

      msites(6)='C_3'        
      izmonomer(6,1)=5     
      zmonomer(6,1)=rcc  
      izmonomer(6,2)=4     
      zmonomer(6,2)=tccc  
      izmonomer(6,3)=3
      zmonomer(6,3)=0.d0  

      msites(7)='C_4'      
      izmonomer(7,1)=6     
      zmonomer(7,1)=rcc   
      izmonomer(7,2)=5     
      zmonomer(7,2)=tccc  
      izmonomer(7,3)=4
      zmonomer(7,3)=60.d0   

      msites(8)='C_5'      
      izmonomer(8,1)=7     
      zmonomer(8,1)=rcc   
      izmonomer(8,2)=6     
      zmonomer(8,2)=tccc  
      izmonomer(8,3)=5
      zmonomer(8,3)=-60.d0  

      msites(9)='O_6'      
      izmonomer(9,1)=8     
      zmonomer(9,1)=1.41d0 
      izmonomer(9,2)=7     
      zmonomer(9,2)=tccc  
      izmonomer(9,3)=6
      zmonomer(9,3)=60.d0   

      msites(10)='O_7'      
      izmonomer(10,1)=4     
      zmonomer(10,1)=rco     
      izmonomer(10,2)=5     
      zmonomer(10,2)=120.d0
      izmonomer(10,3)=6
      zmonomer(10,3)=180.d0  

      msites(11)='C_11'      
      izmonomer(11,1)=7     
      zmonomer(11,1)=rcc      
      izmonomer(11,2)=6     
      zmonomer(11,2)=tccc    
      izmonomer(11,3)=5
      zmonomer(11,3)=180.d0  

      msites(12)='C_12'      
      izmonomer(12,1)=11     
      zmonomer(12,1)=rcc      
      izmonomer(12,2)=7     
      zmonomer(12,2)=tccc    
      izmonomer(12,3)=6
      zmonomer(12,3)=60.d0  

      msites(13)='O_13'      
      izmonomer(13,1)=12     
      zmonomer(13,1)=1.41d0   
      izmonomer(13,2)=11    
      zmonomer(13,2)=tccc    
      izmonomer(13,3)=7
      zmonomer(13,3)=180.d0  

      msites(14)='C_8'      
      izmonomer(14,1)=13     
      zmonomer(14,1)=1.327d0  
      izmonomer(14,2)=12    
      zmonomer(14,2)=tccc    
      izmonomer(14,3)=11
      zmonomer(14,3)=60.d0  

      msites(15)='C_9'      
      izmonomer(15,1)=14     
      zmonomer(15,1)=1.522d0  
      izmonomer(15,2)=13    
      zmonomer(15,2)=tccc    
      izmonomer(15,3)=12
      zmonomer(15,3)=-60.d0  

      msites(16)='C_10'      
      izmonomer(16,1)=15     
      zmonomer(16,1)=1.522d0  
      izmonomer(16,2)=14    
      zmonomer(16,2)=tccc    
      izmonomer(16,3)=13
      zmonomer(16,3)=60.d0  

      msites(17)='O_14'      
      izmonomer(17,1)=14     
      zmonomer(17,1)=rco      
      izmonomer(17,2)=13    
      zmonomer(17,2)=120.d0  
      izmonomer(17,3)=12
      zmonomer(17,3)=180.d0  



cC_1  Du_3 1.0    Du_2 90.0   Du_1 180.0
cC_2  C_1  1.522  Du_3 90.0   Du_2 180.0
cC_3  C_2  1.529  C_1  109.5  Du_3 180.0
cC_4  C_3  1.529  C_2  109.5  C_1   60.0
cC_5  C_4  1.529  C_3  109.5  C_2  -60.0
cO_6  C_5  1.41   C_4  109.5  C_3   60.0
cO_7  C_1  1.229  C_2  120.0  C_3  180.0

cC_11 C_4  1.529  C_3  109.5  C_2  180.0
cC_12 C_11 1.529  C_4  109.5  C_3   60.0
cO_13 C_12 1.41   C_11 109.5  C_4  180.0
cC_8  O_13 1.327  C_12 109.5  C_11  60.0
cC_9  C_8  1.522  O_13 120.0  C_12 -60.0
cC_10 C_9  1.529  C_8  109.5  O_13  60.0

      nbnds=15

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo

      knbnds(6,1)=9
      knbnds(6,2)=4
      knbnds(6,3)=1
      knbnds(7,1)=10
      knbnds(7,2)=4
      knbnds(7,3)=2
      knbnds(13,1)=16
      knbnds(13,2)=11
      knbnds(13,3)=1
      knbnds(14,1)=17
      knbnds(14,2)=14
      knbnds(14,3)=2
      knbnds(15,1)=7
      knbnds(15,2)=11
      knbnds(15,3)=1





      elseif(montype.eq.23) then
c
c     ETOH:  Terminal OH group for a long chain                           
c
      natmonomer=6   !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0    ! c-c
      tccc=109.5d0 !tetrahedral                      

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='O_1' 
      izmonomer(4,1)=3  
      zmonomer(4,1)=1.41d0   ! -C-O-
      izmonomer(4,2)=2     
      zmonomer(4,2)=tccc  
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0    

      msites(5)='C_2'        
      izmonomer(5,1)=4     
      zmonomer(5,1)=1.41d0   ! -C-O-
      izmonomer(5,2)=3     
      zmonomer(5,2)=tccc  
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0    

      msites(6)='C_3'        
      izmonomer(6,1)=5     
      zmonomer(6,1)=rcc      
      izmonomer(6,2)=4     
      zmonomer(6,2)=tccc  
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0    

      nbnds=2

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo




      elseif(montype.eq.24) then
c
c     IBUPR:  Ibuprofen                            
c
      natmonomer=36  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0    ! c-c
      tccc=109.5d0 !tetrahedral                      

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

c    c_1
c    c_2  c_1  1.4
c    c_3  c_2  1.4   c_1 120.
c    c_4  c_3  1.4   c_2 120.  c_1  0.
c    c_5  c_4  1.4   c_3 120.  c_2  0.
c    c_6  c_5  1.4   c_4 120.  c_3  0.
      msites(4)='C_1' 
      izmonomer(4,1)=3  
      zmonomer(4,1)=1.d0  
      izmonomer(4,2)=2     
      zmonomer(4,2)=90.d0 
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0    

      msites(5)='C_2' 
      izmonomer(5,1)=4  
      zmonomer(5,1)=1.4d0  
      izmonomer(5,2)=3     
      zmonomer(5,2)=90.d0 
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0    
   
      msites(6)='C_3' 
      izmonomer(6,1)=5  
      zmonomer(6,1)=1.4d0  
      izmonomer(6,2)=4     
      zmonomer(6,2)=120.d0 
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0    
   
      msites(7)='C_4' 
      izmonomer(7,1)=6  
      zmonomer(7,1)=1.4d0  
      izmonomer(7,2)=5     
      zmonomer(7,2)=120.d0 
      izmonomer(7,3)=4
      zmonomer(7,3)=0.d0    
   
      msites(8)='C_5' 
      izmonomer(8,1)=7  
      zmonomer(8,1)=1.4d0  
      izmonomer(8,2)=6     
      zmonomer(8,2)=120.d0 
      izmonomer(8,3)=5
      zmonomer(8,3)=0.d0    
   
      msites(9)='C_6' 
      izmonomer(9,1)=8  
      zmonomer(9,1)=1.4d0  
      izmonomer(9,2)=7     
      zmonomer(9,2)=120.d0 
      izmonomer(9,3)=6
      zmonomer(9,3)=0.d0    
   
c    c_7  c_1  1.51  c_2 120.  c_3  180.
c    c_8  c_7  1.529 c_1 109.5 c_2  0.
c    c_9  c_8  1.529 c_7 109.5 c_1  60.
c    c_10 c_8  1.529 c_7 109.5 c_1 -60.
   
      msites(10)='C_7' 
      izmonomer(10,1)=4  
      zmonomer(10,1)=1.51d0  
      izmonomer(10,2)=5     
      zmonomer(10,2)=120.d0 
      izmonomer(10,3)=6
      zmonomer(10,3)=180.d0    
   
      msites(11)='C_8' 
      izmonomer(11,1)=10 
      zmonomer(11,1)=1.529d0  
      izmonomer(11,2)=4     
      zmonomer(11,2)=109.5d0 
      izmonomer(11,3)=5
      zmonomer(11,3)=0.d0    
   
      msites(12)='C_9' 
      izmonomer(12,1)=11 
      zmonomer(12,1)=1.529d0  
      izmonomer(12,2)=10     
      zmonomer(12,2)=109.5d0 
      izmonomer(12,3)=4
      zmonomer(12,3)=60.d0    
   
      msites(13)='C_10' 
      izmonomer(13,1)=11 
      zmonomer(13,1)=1.529d0  
      izmonomer(13,2)=10     
      zmonomer(13,2)=109.5d0 
      izmonomer(13,3)=4
      zmonomer(13,3)=-60.d0    
   
c    h_11 c_2  1.09  c_1 120.  c_6  180.
c    h_12 c_3  1.09  c_2 120.  c_1  180.
c    h_13 c_5  1.09  c_4 120.  c_3  180.
c    h_14 c_6  1.09  c_5 120.  c_4  180.

      msites(14)='H_11' 
      izmonomer(14,1)=5  
      zmonomer(14,1)=1.09d0  
      izmonomer(14,2)=4      
      zmonomer(14,2)=120.d0 
      izmonomer(14,3)=9
      zmonomer(14,3)=180.d0    

      msites(15)='H_12' 
      izmonomer(15,1)=6  
      zmonomer(15,1)=1.09d0  
      izmonomer(15,2)=5      
      zmonomer(15,2)=120.d0 
      izmonomer(15,3)=4
      zmonomer(15,3)=180.d0    
   
      msites(16)='H_13' 
      izmonomer(16,1)=8  
      zmonomer(16,1)=1.09d0  
      izmonomer(16,2)=7      
      zmonomer(16,2)=120.d0 
      izmonomer(16,3)=6
      zmonomer(16,3)=180.d0    
   
      msites(17)='H_14' 
      izmonomer(17,1)=9  
      zmonomer(17,1)=1.09d0  
      izmonomer(17,2)=8      
      zmonomer(17,2)=120.d0 
      izmonomer(17,3)=7
      zmonomer(17,3)=180.d0    
   
c    h_15 c_7  1.09  c_1 109.5 c_2  120.
c    h_16 c_7  1.09  c_1 109.5 c_2 -120.

      msites(18)='H_15' 
      izmonomer(18,1)=10 
      zmonomer(18,1)=1.09d0  
      izmonomer(18,2)=4      
      zmonomer(18,2)=109.5d0
      izmonomer(18,3)=5
      zmonomer(18,3)=120.d0    
   
      msites(19)='H_16' 
      izmonomer(19,1)=10 
      zmonomer(19,1)=1.09d0  
      izmonomer(19,2)=4      
      zmonomer(19,2)=109.5d0
      izmonomer(19,3)=5
      zmonomer(19,3)=-120.d0    

c    h_17 c_8  1.09  c_7 109.5 c_1 180.

      msites(20)='H_17' 
      izmonomer(20,1)=11 
      zmonomer(20,1)=1.09d0  
      izmonomer(20,2)=10     
      zmonomer(20,2)=109.5d0
      izmonomer(20,3)=4
      zmonomer(20,3)=180.d0    

c    h_18 c_9  1.09  c_8 109.5 c_7    0.
c    h_19 c_9  1.09  c_8 109.5 c_7  120.
c    h_20 c_9  1.09  c_8 109.5 c_7 -120.

      msites(21)='H_18' 
      izmonomer(21,1)=12 
      zmonomer(21,1)=1.09d0  
      izmonomer(21,2)=11     
      zmonomer(21,2)=109.5d0
      izmonomer(21,3)=10
      zmonomer(21,3)=0.d0    
   
      msites(22)='H_19' 
      izmonomer(22,1)=12 
      zmonomer(22,1)=1.09d0  
      izmonomer(22,2)=11     
      zmonomer(22,2)=109.5d0
      izmonomer(22,3)=10
      zmonomer(22,3)=120.d0    
   
      msites(23)='H_20' 
      izmonomer(23,1)=12 
      zmonomer(23,1)=1.09d0  
      izmonomer(23,2)=11     
      zmonomer(23,2)=109.5d0
      izmonomer(23,3)=10
      zmonomer(23,3)=-120.d0    

c    h_21 c_10 1.09  c_8 109.5 c_7    0.
c    h_22 c_10 1.09  c_8 109.5 c_7  120.
c    h_23 c_10 1.09  c_8 109.5 c_7 -120.

      msites(24)='H_21' 
      izmonomer(24,1)=13 
      zmonomer(24,1)=1.09d0  
      izmonomer(24,2)=11     
      zmonomer(24,2)=109.5d0
      izmonomer(24,3)=10
      zmonomer(24,3)=0.d0    

      msites(25)='H_22' 
      izmonomer(25,1)=13 
      zmonomer(25,1)=1.09d0  
      izmonomer(25,2)=11     
      zmonomer(25,2)=109.5d0
      izmonomer(25,3)=10
      zmonomer(25,3)=120.d0    

      msites(26)='H_23' 
      izmonomer(26,1)=13 
      zmonomer(26,1)=1.09d0  
      izmonomer(26,2)=11     
      zmonomer(26,2)=109.5d0
      izmonomer(26,3)=10
      zmonomer(26,3)=-120.d0    

c    c_24 c_4  1.51  c_3 120.  c_2  180.
c    c_25 c_24 1.529 c_4 109.5 c_3    0.
c    c_26 c_24 1.522 c_4 112.0 c_3  120.
c    o_27 c_26 1.229 c_24 120.4  c_4  0.
c    o_28 c_26 1.364 c_24 108. c_4  180.

      msites(27)='C_24' 
      izmonomer(27,1)=7  
      zmonomer(27,1)=1.51d0  
      izmonomer(27,2)=6      
      zmonomer(27,2)=120.0d0
      izmonomer(27,3)=5 
      zmonomer(27,3)=180.d0    

      msites(28)='C_25' 
      izmonomer(28,1)=27 
      zmonomer(28,1)=1.529d0  
      izmonomer(28,2)=7      
      zmonomer(28,2)=109.5d0
      izmonomer(28,3)=6 
      zmonomer(28,3)=0.d0    

      msites(29)='C_26' 
      izmonomer(29,1)=27 
      zmonomer(29,1)=1.522d0  
      izmonomer(29,2)=7      
      zmonomer(29,2)=112.0d0
      izmonomer(29,3)=6 
      zmonomer(29,3)=120.d0    

      msites(30)='O_27' 
      izmonomer(30,1)=29 
      zmonomer(30,1)=1.229d0  
      izmonomer(30,2)=27      
      zmonomer(30,2)=120.4d0
      izmonomer(30,3)=7 
      zmonomer(30,3)=0.d0    

      msites(31)='O_28' 
      izmonomer(31,1)=29 
      zmonomer(31,1)=1.364d0  
      izmonomer(31,2)=27      
      zmonomer(31,2)=108.d0
      izmonomer(31,3)=7 
      zmonomer(31,3)=180.d0    

c    h_29 c_24 1.09  c_4  109.5 c_3 -120.

      msites(32)='H_29' 
      izmonomer(32,1)=27 
      zmonomer(32,1)=1.09d0  
      izmonomer(32,2)=7      
      zmonomer(32,2)=109.5d0
      izmonomer(32,3)=6 
      zmonomer(32,3)=-120.d0    


c    h_30 c_25 1.09  c_24 109.5 c_4   0.
c    h_31 c_25 1.09  c_24 109.5 c_4  -120.
c    h_32 c_25 1.09  c_24 109.5 c_4   120.

      msites(33)='H_30' 
      izmonomer(33,1)=28 
      zmonomer(33,1)=1.09d0  
      izmonomer(33,2)=27      
      zmonomer(33,2)=109.5d0
      izmonomer(33,3)=7 
      zmonomer(33,3)=0.d0    

      msites(34)='H_31' 
      izmonomer(34,1)=28 
      zmonomer(34,1)=1.09d0  
      izmonomer(34,2)=27      
      zmonomer(34,2)=109.5d0
      izmonomer(34,3)=7 
      zmonomer(34,3)=-120.d0    

      msites(35)='H_32' 
      izmonomer(35,1)=28 
      zmonomer(35,1)=1.09d0  
      izmonomer(35,2)=27      
      zmonomer(35,2)=109.5d0
      izmonomer(35,3)=7 
      zmonomer(35,3)= 120.d0    

c    h_33 c_28 0.945 c_26 109.5 c_24   180.

      msites(36)='H_33' 
      izmonomer(36,1)=31 
      zmonomer(36,1)=0.945d0  
      izmonomer(36,2)=29      
      zmonomer(36,2)=109.5d0
      izmonomer(36,3)=27 
      zmonomer(36,3)= 180.d0    

      nbnds=33

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo

c     aromatic bond type is 4
      do i=1,6
        knbnds(i,3)=4  !aromatic bonds in the ring
      enddo
      knbnds(6,2)=4   !ring closure
      
      knbnds(9,1)=11  !aliphatic fix

      knbnds(10,1)=5  !ring hydrogens
      knbnds(11,1)=6
      knbnds(12,1)=8
      knbnds(13,1)=9

      knbnds(14,1)=10 !methylene hydrogens
      knbnds(15,1)=10

      knbnds(16,1)=11

      knbnds(17,1)=12  !methyl on c_9
      knbnds(18,1)=12
      knbnds(19,1)=12

      knbnds(20,1)=13  !methyl on c_10
      knbnds(21,1)=13
      knbnds(22,1)=13

      knbnds(23,1)=7   !acid linkage to ring

      knbnds(25,1)=27  !in acid group

      knbnds(26,3)=2   !double bond in C=O group

      knbnds(27,1)=29  !in acid group
      knbnds(28,1)=27  !in acid group

      knbnds(29,1)=28  !methyl on c_25
      knbnds(30,1)=28
      knbnds(31,1)=28

      knbnds(32,1)=31
      knbnds(33,1)=10
      knbnds(33,2)=4 





      elseif(montype.eq.25) then
c
c     DIMEET:  dimenthyl ether (H3C-O-CH3) to terminate tretraglyme made feom 2 DMEs
c
      natmonomer=6  !monomer sites, including three dummy sites 
      ndummy=3


      rcc=1.529d0 !DME OPLS-AA
      rco=1.410d0 !DME OPLS-AA
      tcco=109.5d0 !DME OPLS-AA
      tcoc=109.5d0 !DME OPLS-AA

c     first dummy site surrogate for C_4 on DME
c     second dummy site surrogate for O_5 on DME
c     third dummy site surrogate for C_6 on DME

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0
 
      msites(4)='C_1'
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcc 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcco
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0 !first linkage torsion  180=trans (cocc)

      msites(5)='O_2'
      izmonomer(5,1)=4    
      zmonomer(5,1)=rco 
      izmonomer(5,2)=3     
      zmonomer(5,2)=tcco
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 !second linkage torsion  180=trans (occo)

      msites(6)='C_3'
      izmonomer(6,1)=5    !first real site 
      zmonomer(6,1)=rco 
      izmonomer(6,2)=4     
      zmonomer(6,2)=tcoc
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0 !first linkage torsion  180=trans (cocc)

      nbnds=2

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo




      elseif(montype.eq.26) then
c
c     CARBO1:  carbonate backbone for Amanda series of polymers                      
c              H-(-CHCH3-CH2-O-CO-O-CH2-)-H
c                    -C7-C6-O5-C3-O2-C1-
c                     C8       O4
c
      natmonomer=11  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0  !CT-CT single bond 
      tccc=109.5d0 !CT-CT-CT  
      rco=1.410d0  !CT-OS    OPLS-AA
      rcos=1.327d0 !C -OS    OPLS-AA JCC 1990 11 1181 SKF8
      tcco=109.5d0 !CT-CT-OS OPLS-AA
c     tcosct=116.9d0 !C -OS-CT OPLS-AA
      tcosct=111.35d0 !C -OS-CT modified to make the polymer exactly linear
      rcoyl=1.229d0  !C -O     OPLS-AA
      tocos=123.4d0  !O -C -OS OPLS-AA JCC 1990



c     first dummy site surrogate for O_5 on previous CARBO1
c     second dummy site surrogate for C_6 on previous CARBO1
c     third dummy site surrogate for C_7 on previous CARBO1

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0
 
      msites(4)='C_1'
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcc 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tccc
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0 !first linkage torsion  180=trans (ccco)

      msites(5)='O_2'
      izmonomer(5,1)=4    
      zmonomer(5,1)=rco 
      izmonomer(5,2)=3     
      zmonomer(5,2)=tcco
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 !second linkage torsion  180=trans (occc)

      msites(6)='C_3'
      izmonomer(6,1)=5    !third real site carbonyl carbon
      zmonomer(6,1)=rcos  !Carbonyl to ester-like oxygen
      izmonomer(6,2)=4     
      zmonomer(6,2)=tcosct
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0 !first linkage torsion  180=trans

      msites(7)='O_4'
      izmonomer(7,1)=6    !fourth real site carbonyl oxygen
      zmonomer(7,1)=rcoyl !C=O double bond
      izmonomer(7,2)=5     
      zmonomer(7,2)=tocos 
      izmonomer(7,3)=4
      zmonomer(7,3)=0.d0  !torsion  0=cis    

      msites(8)='O_5'
      izmonomer(8,1)=6    !fifth real site ester oxygen
      zmonomer(8,1)=rcos  !Carbonyl to ester-like oxygen 
      izmonomer(8,2)=5     
      zmonomer(8,2)=360.d0-2*tocos 
      izmonomer(8,3)=4
      zmonomer(8,3)=180.d0  !torsion  trans    

      msites(9)='C_6'
      izmonomer(9,1)=8    !sixth real carbon             
      zmonomer(9,1)=rco   !ct-os                          
      izmonomer(9,2)=6     
      zmonomer(9,2)=tcosct 
      izmonomer(9,3)=5
      zmonomer(9,3)=180.d0  !torsion  trans    

      msites(10)='C_7'
      izmonomer(10,1)=9    !seventh carbon                
      zmonomer(10,1)=rcc   !ct-ct                          
      izmonomer(10,2)=8     
      zmonomer(10,2)=tcco   
      izmonomer(10,3)=6
      zmonomer(10,3)=180.d0  !torsion  trans    

      msites(11)='C_8'
      izmonomer(11,1)=10   !eighth carbon                 
      zmonomer(11,1)=rcc   !ct-ct                          
      izmonomer(11,2)=9     
      zmonomer(11,2)=tccc   
      izmonomer(11,3)=8
      zmonomer(11,3)=60.d0  !torsion  trans    

      nbnds=7

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
      knbnds(3,3)=2 !carbonyl double bond
      knbnds(4,1)=6 !fix up


      elseif(montype.eq.27) then
c
c     AMAND1:  side chain for carbonate backbone from Amanda's hydrogels               
c
c              (H)-CO-NH-CH2-CH2-OH
c                  C1-N3-C4 -C5 -O6
c                  O2          
c
      natmonomer=9   !monomer sites, including three dummy sites 
      ndummy=3

      rcct=1.522d0   !C -CT single bond  OPLS-AA
      tcctct=111.1d0 !C -CT-CT  OPLS-AA
      rcoyl=1.229d0  !C -O     OPLS-AA
      tctco=120.4d0  !CT-C -O   OPLS-AA
      rcn  =1.335d0  !C -N     OPLS-AA
      tctcn=116.6d0  !CT-C -N  OPLS-AA
      rctn =1.449d0  !CT-N     OPLS-AA
      tcnct=121.9d0  !C -N -CT OPLS-AA
      rcc=1.529d0    !CT-CT single bond 
      tctctn=109.7d0 !CT-CT-N   
      rctoh=1.410d0  !CT-OH    OPLS-AA
      tccc=109.5d0   !CT-CT-CT also for CT-CT-OH

c     first dummy site surrogate for O_5 on CARBO1
c     second dummy site surrogate for C_6 on CARBO1
c     third dummy site surrogate for C_7 on CARBO1

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0
 
      msites(4)='C_1'
      izmonomer(4,1)=3    !first real site carbonyl carbon
      zmonomer(4,1)=rcct
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcctct
      izmonomer(4,3)=1
      zmonomer(4,3)= -60.d0 

      msites(5)='O_2'
      izmonomer(5,1)=4    !second real site carbonyl oxygen   
      zmonomer(5,1)=rcoyl
      izmonomer(5,2)=3     
      zmonomer(5,2)=tctco
      izmonomer(5,3)=2
      zmonomer(5,3)=120.d0 ! 180=trans (occc)

      msites(6)='N_3'
      izmonomer(6,1)=4    !third real site amide nitrogen 
      zmonomer(6,1)=rcn   !amide bond                     
      izmonomer(6,2)=3     
      zmonomer(6,2)=tctcn 
      izmonomer(6,3)=2
      zmonomer(6,3)=-60.d0   ! 0=cis

      msites(7)='C_4'
      izmonomer(7,1)=6    !fourth real site methylene carbon
      zmonomer(7,1)=rctn  !CT-N
      izmonomer(7,2)=4     
      zmonomer(7,2)=tcnct !C -N -CT
      izmonomer(7,3)=3
      zmonomer(7,3)=180.d0  !torsion  0=trans  

      msites(8)='C_5'
      izmonomer(8,1)=7    !fifth real site methylene carbon
      zmonomer(8,1)=rcc   !CT-CT                            
      izmonomer(8,2)=6     
      zmonomer(8,2)=tctctn         
      izmonomer(8,3)=4
      zmonomer(8,3)=180.d0  !torsion  trans    

      msites(9)='O_6'
      izmonomer(9,1)=8    !sixth site is hydroxyl oxygen          
      zmonomer(9,1)=rctoh !ct-oh                          
      izmonomer(9,2)=7     
      zmonomer(9,2)=tccc   
      izmonomer(9,3)=6
      zmonomer(9,3)=180.d0  !torsion  trans    

      nbnds=5

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
      knbnds(1,3)=2 !carbonyl double bond
      knbnds(2,1)=4 !fix up




      elseif(montype.eq.28) then
c
c     AMAND2:  side chain for carbonate backbone from Amanda's hydrogels               
c              (this adds an extra methylene to the chain of AMAND1)
c              (H)-CO-NH-CH2-CH2-CH2-OH
c                  C1-N3-C4 -C5 -C6 -O7
c                  O2          
c
      natmonomer=10  !monomer sites, including three dummy sites 
      ndummy=3

      rcct=1.522d0   !C -CT single bond  OPLS-AA
      tcctct=111.1d0 !C -CT-CT  OPLS-AA
      rcoyl=1.229d0  !C -O     OPLS-AA
      tctco=120.4d0  !CT-C -O   OPLS-AA
      rcn  =1.335d0  !C -N     OPLS-AA
      tctcn=116.6d0  !CT-C -N  OPLS-AA
      rctn =1.449d0  !CT-N     OPLS-AA
      tcnct=121.9d0  !C -N -CT OPLS-AA
      rcc=1.529d0    !CT-CT single bond 
      tctctn=109.7d0 !CT-CT-N   
      rctoh=1.410d0  !CT-OH    OPLS-AA
      tccc=109.5d0   !CT-CT-CT also for CT-CT-OH

c     first dummy site surrogate for O_5 on CARBO1
c     second dummy site surrogate for C_6 on CARBO1
c     third dummy site surrogate for C_7 on CARBO1

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0
 
      msites(4)='C_1'
      izmonomer(4,1)=3    !first real site carbonyl carbon
      zmonomer(4,1)=rcct
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcctct
      izmonomer(4,3)=1
      zmonomer(4,3)= -60.d0 

      msites(5)='O_2'
      izmonomer(5,1)=4    !second real site carbonyl oxygen   
      zmonomer(5,1)=rcoyl
      izmonomer(5,2)=3     
      zmonomer(5,2)=tctco
      izmonomer(5,3)=2
      zmonomer(5,3)=120.d0 ! 180=trans (occc)

      msites(6)='N_3'
      izmonomer(6,1)=4    !third real site amide nitrogen 
      zmonomer(6,1)=rcn   !amide bond                     
      izmonomer(6,2)=3     
      zmonomer(6,2)=tctcn 
      izmonomer(6,3)=2
      zmonomer(6,3)=-60.d0   ! 0=cis

      msites(7)='C_4'
      izmonomer(7,1)=6    !fourth real site methylene carbon
      zmonomer(7,1)=rctn  !CT-N
      izmonomer(7,2)=4     
      zmonomer(7,2)=tcnct !C -N -CT
      izmonomer(7,3)=3
      zmonomer(7,3)=180.d0  !torsion  0=trans  

      msites(8)='C_5'
      izmonomer(8,1)=7    !fifth real site methylene carbon
      zmonomer(8,1)=rcc   !CT-CT                            
      izmonomer(8,2)=6     
      zmonomer(8,2)=tctctn         
      izmonomer(8,3)=4
      zmonomer(8,3)=180.d0  !torsion  trans    

      msites(9)='C_6'
      izmonomer(9,1)=8    !sixth site is methylene carbon        
      zmonomer(9,1)=rcc   !ct-ct                          
      izmonomer(9,2)=7     
      zmonomer(9,2)=tccc   
      izmonomer(9,3)=6
      zmonomer(9,3)=180.d0  !torsion  trans    

      msites(10)='O_7'
      izmonomer(10,1)=9    !seventh site is hydroxyl oxygen          
      zmonomer(10,1)=rctoh !ct-oh                          
      izmonomer(10,2)=8     
      zmonomer(10,2)=tccc   
      izmonomer(10,3)=7
      zmonomer(10,3)=180.d0  !torsion  trans    

      nbnds=6

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
      knbnds(1,3)=2 !carbonyl double bond
      knbnds(2,1)=4 !fix up




      elseif(montype.eq.29.or.montype.eq.30) then
c
c     AMAND3:  side chain for carbonate backbone from Amanda's hydrogels               
c     AMAND4:  
c
c              (H)-CO-NH-CH2-CH2-OH
c                  C1-N3-C4 -C5 -O6
c                  O2    C7    
c                        X8   X=C for AMAND3 and X=O for AMAND4
c
      natmonomer=11  !monomer sites, including three dummy sites 
      ndummy=3

      rcct=1.522d0   !C -CT single bond  OPLS-AA
      tcctct=111.1d0 !C -CT-CT  OPLS-AA
      rcoyl=1.229d0  !C -O     OPLS-AA
      tctco=120.4d0  !CT-C -O   OPLS-AA
      rcn  =1.335d0  !C -N     OPLS-AA
      tctcn=116.6d0  !CT-C -N  OPLS-AA
      rctn =1.449d0  !CT-N     OPLS-AA
      tcnct=121.9d0  !C -N -CT OPLS-AA
      rcc=1.529d0    !CT-CT single bond 
      tctctn=109.7d0 !CT-CT-N   
      rctoh=1.410d0  !CT-OH    OPLS-AA
      tccc=109.5d0   !CT-CT-CT also for CT-CT-OH

c     first dummy site surrogate for O_5 on CARBO1
c     second dummy site surrogate for C_6 on CARBO1
c     third dummy site surrogate for C_7 on CARBO1

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0
 
      msites(4)='C_1'
      izmonomer(4,1)=3    !first real site carbonyl carbon
      zmonomer(4,1)=rcct
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcctct
      izmonomer(4,3)=1
      zmonomer(4,3)= -60.d0 

      msites(5)='O_2'
      izmonomer(5,1)=4    !second real site carbonyl oxygen   
      zmonomer(5,1)=rcoyl
      izmonomer(5,2)=3     
      zmonomer(5,2)=tctco
      izmonomer(5,3)=2
      zmonomer(5,3)=120.d0 ! 180=trans (occc)

      msites(6)='N_3'
      izmonomer(6,1)=4    !third real site amide nitrogen 
      zmonomer(6,1)=rcn   !amide bond                     
      izmonomer(6,2)=3     
      zmonomer(6,2)=tctcn 
      izmonomer(6,3)=2
      zmonomer(6,3)=-60.d0   ! 0=cis

      msites(7)='C_4'
      izmonomer(7,1)=6    !fourth real site methylene carbon
      zmonomer(7,1)=rctn  !CT-N
      izmonomer(7,2)=4     
      zmonomer(7,2)=tcnct !C -N -CT
      izmonomer(7,3)=3
      zmonomer(7,3)=180.d0  !torsion  0=trans  

      msites(8)='C_5'
      izmonomer(8,1)=7    !fifth real site methylene carbon
      zmonomer(8,1)=rcc   !CT-CT                            
      izmonomer(8,2)=6     
      zmonomer(8,2)=tctctn         
      izmonomer(8,3)=4
      zmonomer(8,3)=180.d0  !torsion  trans    

      msites(9)='O_6'
      izmonomer(9,1)=8    !sixth site is hydroxyl oxygen          
      zmonomer(9,1)=rctoh !ct-oh                          
      izmonomer(9,2)=7     
      zmonomer(9,2)=tccc   
      izmonomer(9,3)=6
      zmonomer(9,3)=180.d0  !torsion  trans    

      msites(10)='C_7'
      izmonomer(10,1)=7    !seventh site is methylene carbon on C_4
      zmonomer(10,1)=rcc   !ct-ct                          
      izmonomer(10,2)=6     
      zmonomer(10,2)=tctctn 
      izmonomer(10,3)=4
      zmonomer(10,3)=-60.d0  !torsion  trans    

      if(montype.eq.29) then
        msites(11)='C_8'
        zmonomer(11,1)=rcc   !ct-ct                          
      else
        msites(11)='O_8'
        zmonomer(11,1)=rctoh !ct-oh                          
      endif
      izmonomer(11,1)=10   !eighth site is methyl (AMAND3) or oxygen (AMAND4)
      izmonomer(11,2)=7     
      zmonomer(11,2)=tccc   
      izmonomer(11,3)=6
      zmonomer(11,3)=180.d0  !torsion  trans    

      nbnds=7

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
      knbnds(1,3)=2 !carbonyl double bond

      knbnds(2,1)=4 !fix up
      knbnds(6,1)=7 !fix up


      elseif(montype.eq.32) then
c
c     IMSULF:  imidazole-sulfite zwitterionic side chain for carbonate backbone                
c
c              (H)-CO-NH-CH2-CH2-CH2-N-CH-N-CH2-CH2-CH2-SO3
c                                    |    |
c                                    CH===CH
c
c                  C1-N3-C4 -C5 -C6 -N7-C8-N9-C12-C13-C14-S15-(O_16,O_17_O18)
c                  O2                C11==C10
c
c
      natmonomer=21  !monomer sites, including three dummy sites 
      ndummy=3

      rcct=1.522d0   !C -CT single bond  OPLS-AA
      tcctct=111.1d0 !C -CT-CT  OPLS-AA
      rcoyl=1.229d0  !C -O     OPLS-AA
      tctco=120.4d0  !CT-C -O   OPLS-AA
      rcn  =1.335d0  !C -N     OPLS-AA
      tctcn=116.6d0  !CT-C -N  OPLS-AA
      rctn =1.449d0  !CT-N     OPLS-AA
      tcnct=121.9d0  !C -N -CT OPLS-AA
      rcc=1.529d0    !CT-CT single bond 
      tctctn=109.7d0 !CT-CT-N   
      tccc=109.5d0   !CT-CT-CT also for CT-CT-OH


c     first dummy site surrogate for O_5 on CARBO1
c     second dummy site surrogate for C_6 on CARBO1
c     third dummy site surrogate for C_7 on CARBO1

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0
 
      msites(4)='C_1'
      izmonomer(4,1)=3    !first real site carbonyl carbon
      zmonomer(4,1)=rcct
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcctct
      izmonomer(4,3)=1
      zmonomer(4,3)= -60.d0 

      msites(5)='O_2'
      izmonomer(5,1)=4    !second real site carbonyl oxygen   
      zmonomer(5,1)=rcoyl
      izmonomer(5,2)=3     
      zmonomer(5,2)=tctco
      izmonomer(5,3)=2
      zmonomer(5,3)=120.d0 ! 180=trans (occc)

      msites(6)='N_3'
      izmonomer(6,1)=4    !third real site amide nitrogen 
      zmonomer(6,1)=rcn   !amide bond                     
      izmonomer(6,2)=3     
      zmonomer(6,2)=tctcn 
      izmonomer(6,3)=2
      zmonomer(6,3)=-60.d0   ! 0=cis

      msites(7)='C_4'
      izmonomer(7,1)=6    !fourth real site methylene carbon
      zmonomer(7,1)=rctn  !CT-N
      izmonomer(7,2)=4     
      zmonomer(7,2)=tcnct !C -N -CT
      izmonomer(7,3)=3
      zmonomer(7,3)=180.d0  !torsion  0=trans  

      msites(8)='C_5'
      izmonomer(8,1)=7    !fifth real site methylene carbon
      zmonomer(8,1)=rcc   !CT-CT                            
      izmonomer(8,2)=6     
      zmonomer(8,2)=tctctn         
      izmonomer(8,3)=4
      zmonomer(8,3)=180.d0  !torsion  trans    

      msites(9)='C_6'
      izmonomer(9,1)=8    !sixth real site methylene carbon
      zmonomer(9,1)=rcc   !CT-CT                            
      izmonomer(9,2)=7     
      zmonomer(9,2)=tccc         
      izmonomer(9,3)=6
      zmonomer(9,3)=180.d0  !torsion  trans    

      msites(10)='N_7'
      izmonomer(10,1)=9    !seventh real site imidazole nitrogen
      zmonomer(10,1)=1.47  !imidazole                        
      izmonomer(10,2)=8     
      zmonomer(10,2)=tccc         
      izmonomer(10,3)=7
      zmonomer(10,3)=180.d0  !torsion  trans    

      msites(11)='C_8'
      izmonomer(11,1)=10   !seventh real site imidazole carbon  
      zmonomer(11,1)=1.31  !imidazole                        
      izmonomer(11,2)=9     
      zmonomer(11,2)=120.         
      izmonomer(11,3)=8
      zmonomer(11,3)=180.d0  !torsion  trans    

      msites(12)='N_9'
      izmonomer(12,1)=11   !eighth real site imidazole nitrogen
      zmonomer(12,1)=1.31  !imidazole                        
      izmonomer(12,2)=10    
      zmonomer(12,2)=120.         
      izmonomer(12,3)=9
      zmonomer(12,3)=180.d0  !torsion  trans    

      msites(13)='C_10'
      izmonomer(13,1)=12   !eighth real site imidazole carbon in ring
      zmonomer(13,1)=1.38  !imidazole                        
      izmonomer(13,2)=11    
      zmonomer(13,2)=90.0         
      izmonomer(13,3)=10
      zmonomer(13,3)=0.d0  !torsion cis       

      msites(14)='C_11'
      izmonomer(14,1)=13   !eighth real site imidazole carbon in ring
      zmonomer(14,1)=1.37  !imidazole                        
      izmonomer(14,2)=12    
      zmonomer(14,2)=120.         
      izmonomer(14,3)=11
      zmonomer(14,3)=0.d0  !torsion cis       

      msites(15)='C_12'
      izmonomer(15,1)=12   !12th real site methylene carbon
      zmonomer(15,1)=1.47  !CT-N                             
      izmonomer(15,2)=11    
      zmonomer(15,2)=120.         
      izmonomer(15,3)=10
      zmonomer(15,3)=180.d0  !torsion trans     

      msites(16)='C_13'
      izmonomer(16,1)=15   !13th real site methylene carbon
      zmonomer(16,1)=rcc   !CT-CT                            
      izmonomer(16,2)=12    
      zmonomer(16,2)=tccc      
      izmonomer(16,3)=11
      zmonomer(16,3)=180.d0  !torsion trans     

      msites(17)='C_14'
      izmonomer(17,1)=16   !14th real site methylene carbon
      zmonomer(17,1)=rcc   !CT-CT                            
      izmonomer(17,2)=15    
      zmonomer(17,2)=tccc      
      izmonomer(17,3)=12
      zmonomer(17,3)=180.d0  !torsion trans     

      msites(18)='S_15'
      izmonomer(18,1)=17   !15th real site sulfur  
      zmonomer(18,1)=1.83  !S-CT                             
      izmonomer(18,2)=16    
      zmonomer(18,2)=tccc      
      izmonomer(18,3)=15
      zmonomer(18,3)=180.d0  !torsion trans     

      msites(19)='O_16'
      izmonomer(19,1)=18   !16th real site oxygen  
      zmonomer(19,1)=1.50  !S-O                              
      izmonomer(19,2)=17    
      zmonomer(19,2)=tccc      
      izmonomer(19,3)=16
      zmonomer(19,3)=180.d0  !torsion trans     

      msites(20)='O_17'
      izmonomer(20,1)=18   !16th real site oxygen  
      zmonomer(20,1)=1.50  !S-O                              
      izmonomer(20,2)=17    
      zmonomer(20,2)=tccc      
      izmonomer(20,3)=16
      zmonomer(20,3)=-60.d0  !torsion trans     

      msites(21)='O_18'
      izmonomer(21,1)=18   !16th real site oxygen  
      zmonomer(21,1)=1.50  !S-O                              
      izmonomer(21,2)=17    
      zmonomer(21,2)=tccc      
      izmonomer(21,3)=16
      zmonomer(21,3)= 60.d0  !torsion trans     

      nbnds=18

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo

      knbnds(1,3)=2 !carbonyl double bond
      knbnds(8,3)=2 !C=N double bond
      knbnds(10,3)=2 !C=C double bond
      knbnds(16,3)=2 !S=O double bond
      knbnds(17,3)=2 !S=O double bond
      knbnds(18,3)=2 !S=O double bond

      knbnds(2,1)=4 !fix up
      knbnds(11,2)=10 !fix up
      knbnds(12,2)=12 !fix up
      knbnds(13,2)=15 !fix up
      knbnds(14,2)=16 !fix up
      knbnds(15,2)=17 !fix up
      knbnds(16,2)=18 !fix up
      knbnds(17,2)=18 !fix up
      knbnds(18,2)=18 !fix up


      elseif(montype.eq.33) then

c
c     EKET: CH3-CH2-C(=O)-   ethyl ketone (acetyl) capping group for a 'dangling'  alcohol
c
c     note that this is like ACE, but oriented in the other direction
c     ACE = -O-C(=O)-C- with the last (alkane) C as the linkage site
c     KET = CH3-C(=O)-  with the last (conbonyl) C as the linkage site
c     EKET = CH3-CH2-C(=O)-  with the last (conbonyl) C as the linkage site
c
c     this is useful for linking MPA to PDVL
c
      natmonomer=7   !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0
      rcox=1.34d0 !-O-C(=O)-
      rcoz=1.22d0 !C=O LLH
c     rco=1.410d0 !DME OPLS-AA

c     rcoy=1.44d0 !-C-O-
c     rccx=1.52d0 

      tcco=109.5d0 !DME OPLS-AA
      tcoc=109.5d0 !DME OPLS-AA
      toccx=114.d0 !-O-C(=O)-C- LLH
      toccz=121.d0 !-C-C=O LLH


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=rcc 
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site  (an oxygen site usually)
      zmonomer(3,1)=rcc 
      izmonomer(3,2)=1
      zmonomer(3,2)=tcco 
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1'     !first real site, carbonyl carbon
      izmonomer(4,1)=3                              
      zmonomer(4,1)=rcox 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcoc 
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0   !first linkage torsion

      msites(5)='C_2'     !second real site, methyl/methlyene carbon
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcc 
      izmonomer(5,2)=3     
      zmonomer(5,2)=toccx
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0   !second linkage torsion  

      msites(6)='O_3'     !third real site, carbonyl oxygen
      izmonomer(6,1)=4     
      zmonomer(6,1)=rcoz
      izmonomer(6,2)=3     
      zmonomer(6,2)=toccz
      izmonomer(6,3)=2
      zmonomer(6,3)=0.d0   

      msites(7)='C_4'     !fourth real site methyl carbon   
      izmonomer(7,1)=5     
      zmonomer(7,1)=rcc 
      izmonomer(7,2)=4     
      zmonomer(7,2)=tcco 
      izmonomer(7,3)=6
      zmonomer(7,3)=0.d0   


      nbnds=3

      knbnds(1,1)=4
      knbnds(1,2)=5
      knbnds(1,3)=1

      knbnds(2,1)=4
      knbnds(2,2)=6
      knbnds(2,3)=2

      knbnds(3,1)=5
      knbnds(3,2)=7
      knbnds(3,3)=1



      elseif(montype.eq.34) then
c
c     MEOX:  methoxy linker group usually for linking DME to aliphatic region                          
c
c     -C-O- to produce linke to dme:  -[-CH2-O-CH2-CH2-O-CH2-]-[CH2-O-]-[-CH2-CH2-....
c                                             DME                MEOX      PDVL
c
c     this could be used for joining DME to PDVL; best to use DME structural parameters
c
      natmonomer=5   !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0    ! c-c
      tccc=109.5d0 !tetrahedral                      

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='O_1' 
      izmonomer(4,1)=3  
      zmonomer(4,1)=1.41d0   ! -C-O-
      izmonomer(4,2)=2     
      zmonomer(4,2)=tccc  
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0    

      msites(5)='C_2'        
      izmonomer(5,1)=4     
      zmonomer(5,1)=1.41d0   ! -C-O-
      izmonomer(5,2)=3     
      zmonomer(5,2)=tccc  
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0    


      nbnds=1

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo




      elseif(montype.eq.35) then
c
c     PFE:     linker for MPA to carbonate backbone for dendrimer stars   
c              H-(-CHCH3-CH2-O-CO-)-O-CH2-
c                    -C5-C4-O3-C1-
c                     C6       O2
c     this is just CARBO1 without the leading methoxy
c     CARBO1:  carbonate backbone for Amanda series of polymers                      
c              H-(-CHCH3-CH2-O-CO-O-CH2-)-H
c                    -C7-C6-O5-C3-O2-C1-
c                     C8       O4
c
      natmonomer=9  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0  !CT-CT single bond 
      tccc=109.5d0 !CT-CT-CT  
      rco=1.410d0  !CT-OS    OPLS-AA
      rcos=1.327d0 !C -OS    OPLS-AA JCC 1990 11 1181 SKF8
      tcco=109.5d0 !CT-CT-OS OPLS-AA
c     tcosct=116.9d0 !C -OS-CT OPLS-AA
      tcosct=111.35d0 !C -OS-CT modified to make the polymer exactly linear
      rcoyl=1.229d0  !C -O     OPLS-AA
      tocos=123.4d0  !O -C -OS OPLS-AA JCC 1990



c     first dummy site surrogate for O_5 on previous CARBO1
c     second dummy site surrogate for C_6 on previous CARBO1
c     third dummy site surrogate for C_7 on previous CARBO1

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0
 
      msites(4)='C_1'
      izmonomer(4,1)=3    !first real site carbonyl carbon
      zmonomer(4,1)=rcos  !Carbonyl to ester-like oxygen
      izmonomer(4,2)=2     
      zmonomer(4,2)=tcosct
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0 !first linkage torsion  180=trans

      msites(5)='O_2'
      izmonomer(5,1)=4    !second real site carbonyl oxygen
      zmonomer(5,1)=rcoyl !C=O double bond
      izmonomer(5,2)=3     
      zmonomer(5,2)=tocos 
      izmonomer(5,3)=2
      zmonomer(5,3)=0.d0  !torsion  0=cis    

      msites(6)='O_3'
      izmonomer(6,1)=4    !third real site ester oxygen
      zmonomer(6,1)=rcos  !Carbonyl to ester-like oxygen 
      izmonomer(6,2)=3     
      zmonomer(6,2)=360.d0-2*tocos 
      izmonomer(6,3)=2
      zmonomer(6,3)=180.d0  !torsion  trans    

      msites(7)='C_4'
      izmonomer(7,1)=6    !fourth real site, methylene carbon             
      zmonomer(7,1)=rco   !ct-os                          
      izmonomer(7,2)=4     
      zmonomer(7,2)=tcosct 
      izmonomer(7,3)=3
      zmonomer(7,3)=180.d0  !torsion  trans    

      msites(8)='C_5'
      izmonomer(8,1)=7    !fifth real site, carbon       
      zmonomer(8,1)=rcc   !ct-ct                          
      izmonomer(8,2)=6     
      zmonomer(8,2)=tcco   
      izmonomer(8,3)=4
      zmonomer(8,3)=180.d0  !torsion  trans    

      msites(9)='C_6'
      izmonomer(9,1)=8    !sixth real site, carbon        
      zmonomer(9,1)=rcc   !ct-ct                          
      izmonomer(9,2)=7     
      zmonomer(9,2)=tccc   
      izmonomer(9,3)=6
      zmonomer(9,3)=60.d0  !torsion  trans    

      nbnds=5

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
      knbnds(1,3)=2 !carbonyl double bond
      knbnds(2,1)=4 !fix up



      elseif(montype.eq.36) then
c
c     OPENGEL: open form of lactone ring                                  
c              numbering matches the sites in GELCORE unit above
c              H-(-CO-C -C -C -C -O )-H
c
c                 -C1-C2-C3-C4-C5-O6-
c                  ||       | 
c                  O7       C8   
c
c     this is used for backmapping the coarse grain to all atom coorodinates
c     C4-C8 is the bond that connects the two rings
c
c
      natmonomer=11  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0    ! c-c
      rco=1.229d0    ! c=o
      tccc=109.5d0 !tetrahedral                      

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1'     !carbonyl carbon
      izmonomer(4,1)=3  
      zmonomer(4,1)=1.0d0
      izmonomer(4,2)=2     
      zmonomer(4,2)=90.d0 
      izmonomer(4,3)=1
      zmonomer(4,3)=0.d0    

      msites(5)='C_2'        
      izmonomer(5,1)=4     
      zmonomer(5,1)=1.522d0
      izmonomer(5,2)=3     
      zmonomer(5,2)=120.d0   !approx for -C-C(O)-O- angle
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0  

      msites(6)='C_3'        
      izmonomer(6,1)=5     
      zmonomer(6,1)=rcc  
      izmonomer(6,2)=4     
      zmonomer(6,2)=tccc  
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0  

      msites(7)='C_4'      
      izmonomer(7,1)=6     
      zmonomer(7,1)=rcc   
      izmonomer(7,2)=5     
      zmonomer(7,2)=tccc  
      izmonomer(7,3)=4
      zmonomer(7,3)=180.d0  

      msites(8)='C_5'      
      izmonomer(8,1)=7     
      zmonomer(8,1)=rcc   
      izmonomer(8,2)=6     
      zmonomer(8,2)=tccc  
      izmonomer(8,3)=5
      zmonomer(8,3)=180.d0  

      msites(9)='O_6'      !hydroxyl oxygen
      izmonomer(9,1)=8     
      zmonomer(9,1)=1.41d0 
      izmonomer(9,2)=7     
      zmonomer(9,2)=tccc  
      izmonomer(9,3)=6
      zmonomer(9,3)=120.d0   

      msites(10)='O_7'  !carbonyl oxygen    
      izmonomer(10,1)=4     
      zmonomer(10,1)=1.229d0   !C=O
      izmonomer(10,2)=5     
      zmonomer(10,2)=120.d0
      izmonomer(10,3)=6
      zmonomer(10,3)=0.d0   

      msites(11)='C_8'  !carbon across the ring
      izmonomer(11,1)=7     
      zmonomer(11,1)=rcc       !C-C
      izmonomer(11,2)=6     
      zmonomer(11,2)=tccc   
      izmonomer(11,3)=5
      zmonomer(11,3)=60.d0   

      nbnds=7

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo
      knbnds(6,1)=4 !fix up
      knbnds(6,2)=10!fix up
      knbnds(6,3)=2 !carbonyl double bond
      knbnds(7,1)=7 !fix up
      knbnds(7,2)=11!fix up
      knbnds(7,3)=1 !carbonyl double bond



      elseif(montype.eq.37) then
c
c     TOLUENE
c         C1
c         |
c         C2
c       /   \
c      C7   C3
c      |    |
c      C6   C4
c        \ /
c         C5
c
      natmonomer=10  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0   ! single c-c
      rccx=1.39d0   ! benzene c-c


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' 
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcc 
      izmonomer(4,2)=2     
      zmonomer(4,2)=90.d0
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0 

      msites(5)='C_2' 
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcc
      izmonomer(5,2)=3     
      zmonomer(5,2)=90.d0 
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 

      msites(6)='C_3' 
      izmonomer(6,1)=5     
      zmonomer(6,1)=rccx
      izmonomer(6,2)=4     
      zmonomer(6,2)=120.d0
      izmonomer(6,3)=3
      zmonomer(6,3)=0.d0

      msites(7)='C_4' 
      izmonomer(7,1)=6     
      zmonomer(7,1)=rccx
      izmonomer(7,2)=5     
      zmonomer(7,2)=120.d0
      izmonomer(7,3)=4
      zmonomer(7,3)=180.d0

      msites(8)='C_5' 
      izmonomer(8,1)=7     
      zmonomer(8,1)=rccx
      izmonomer(8,2)=6     
      zmonomer(8,2)=120.d0
      izmonomer(8,3)=5
      zmonomer(8,3)=0.d0

      msites(9)='C_6' 
      izmonomer(9,1)=8     
      zmonomer(9,1)=rccx
      izmonomer(9,2)=7     
      zmonomer(9,2)=120.d0
      izmonomer(9,3)=6
      zmonomer(9,3)=0.d0

      msites(10)='C_7' 
      izmonomer(10,1)=9     
      zmonomer(10,1)=rccx
      izmonomer(10,2)=8     
      zmonomer(10,2)=120.d0
      izmonomer(10,3)=7
      zmonomer(10,3)=0.d0


      nbnds=7

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=4  !aromatic bonds
      enddo

      knbnds(7,2)=5 !fix up
      knbnds(1,3)=1 !single bond





      elseif(montype.eq.38) then
c
c     METHANOL  
c
c      C1-O2
c
c     natmonomer=5  ! C-O monomer sites, including three dummy sites 
c     add the hydroxyl hydrogen explicitly, since hydrogen placement does not work for
c     special case of diatom form of C-O
      natmonomer=6  ! C-O-H monomer sites, including three dummy sites 
      ndummy=3

      rco=1.43d0   ! single c-o
      roh=0.95d0   ! hydroxyl hydrogen


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' 
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rco 
      izmonomer(4,2)=2     
      zmonomer(4,2)=90.d0
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0 

      msites(5)='O_2' 
      izmonomer(5,1)=4    !second real site 
      zmonomer(5,1)=rco 
      izmonomer(5,2)=3     
      zmonomer(5,2)=90.d0
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 

      msites(6)='H_3' 
      izmonomer(6,1)=5    !second real site 
      zmonomer(6,1)=roh 
      izmonomer(6,2)=4     
      zmonomer(6,2)=109.35d0
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0 


c     nbnds=1
      nbnds=2
      knbnds(1,1)=4
      knbnds(1,2)=5
      knbnds(1,3)=1
      knbnds(2,1)=5
      knbnds(2,2)=6
      knbnds(2,3)=1




      elseif(montype.eq.39) then
c
c     THF       
c
c      O1-C2-C3-C4-C5
c      |___________|
c
c     Use DME structural parameters for THF


      natmonomer=8  !monomer sites, including three dummy sites 
      ndummy=3

      rco=1.43d0    ! single c-o
      rcc=1.529d0   ! single c-c


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='O_1' 
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=1.0d0
      izmonomer(4,2)=2     
      zmonomer(4,2)=90.d0
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0 

      msites(5)='C_2' 
      izmonomer(5,1)=4    !second real site 
      zmonomer(5,1)=rco 
      izmonomer(5,2)=3     
      zmonomer(5,2)=120.d0
      izmonomer(5,3)=2
      zmonomer(5,3)=0.d0 

      msites(6)='C_3' 
      izmonomer(6,1)=5    !third real site 
      zmonomer(6,1)=rcc 
      izmonomer(6,2)=4     
      zmonomer(6,2)=108.d0    
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0 

      msites(7)='C_4' 
      izmonomer(7,1)=6    !fourth real site 
      zmonomer(7,1)=rcc 
      izmonomer(7,2)=5     
      zmonomer(7,2)=108.d0    
      izmonomer(7,3)=4
      zmonomer(7,3)=0.d0 

      msites(8)='C_5' 
      izmonomer(8,1)=7    !fifth real site 
      zmonomer(8,1)=rcc 
      izmonomer(8,2)=6     
      zmonomer(8,2)=108.d0    
      izmonomer(8,3)=5
      zmonomer(8,3)=0.d0 

      nbnds=5

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo

      knbnds(5,2)=4 !fix up





      elseif(montype.eq.40) then
c
c     MOXE      
c
c     methoxyethanol - used for linking a DME chain to gelcore
c
c     C5-O4-C3-C2-O1
c      
c     Use DME structural parameters for MOXE
c
c     Note that the O1 will actually be an alkoxy (ester)
c     The rest will be ether-like, just like DME

      rco=1.327d0  !C(=O)-OS bond length
      rcos=1.41d0  !OS-CT bond length
      thcosc=116.9d0  !C -OS-CT angle in ester
      rcc=1.529d0  !CT-CT bond length
      thccc=109.5d0

      natmonomer=8  !monomer sites, including three dummy sites 
      ndummy=3

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='O_1' 
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rco 
      izmonomer(4,2)=2     
      zmonomer(4,2)=90.d0
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0 

      msites(5)='C_2' 
      izmonomer(5,1)=4    !second real site 
      zmonomer(5,1)=rcos
      izmonomer(5,2)=3     
      zmonomer(5,2)=thcosc
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 

      msites(6)='C_3' 
      izmonomer(6,1)=5    !second real site 
      zmonomer(6,1)=rcc 
      izmonomer(6,2)=4     
      zmonomer(6,2)=thccc 
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0 

      msites(7)='O_4' 
      izmonomer(7,1)=6    !second real site 
      zmonomer(7,1)=rcos
      izmonomer(7,2)=5     
      zmonomer(7,2)=thccc 
      izmonomer(7,3)=4
      zmonomer(7,3)=180.d0 

      msites(8)='C_5' 
      izmonomer(8,1)=7    !second real site 
      zmonomer(8,1)=rcos
      izmonomer(8,2)=6     
      zmonomer(8,2)=thccc 
      izmonomer(8,3)=5
      zmonomer(8,3)=180.d0 

      nbnds=4

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo




      elseif(montype.eq.41) then
c
c     PDVX     
c
c     this is an alternative to PDVL with a different partitioning
c     The repeat unit has the ester group at each end instead
c     of in the middle
c
c     C6(=O7)-C5-C4-C3-C2-O1
c      
c
c     Note that the O1 will actually be an alkoxy (ester)

      rco=1.327d0  !C(=O)-OS bond length
      rcos=1.41d0  !OS-CT bond length
      thcosc=116.9d0  !C -OS-CT angle in ester
      rcc=1.529d0  !CT-CT bond length
      thccc=109.5d0

      natmonomer=10  !monomer sites, including three dummy sites 
      ndummy=3

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='O_1' 
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rco 
      izmonomer(4,2)=2     
      zmonomer(4,2)=thccc 
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0 

      msites(5)='C_2' 
      izmonomer(5,1)=4                       
      zmonomer(5,1)=rcos
      izmonomer(5,2)=3     
      zmonomer(5,2)=thcosc
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 

      msites(6)='C_3' 
      izmonomer(6,1)=5                       
      zmonomer(6,1)=rcc 
      izmonomer(6,2)=4     
      zmonomer(6,2)=thccc 
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0 

      msites(7)='C_4' 
      izmonomer(7,1)=6                       
      zmonomer(7,1)=rcc 
      izmonomer(7,2)=5     
      zmonomer(7,2)=thccc 
      izmonomer(7,3)=4
      zmonomer(7,3)=180.d0 

      msites(8)='C_5' 
      izmonomer(8,1)=7                      
      zmonomer(8,1)=rcc 
      izmonomer(8,2)=6     
      zmonomer(8,2)=thccc 
      izmonomer(8,3)=5
      zmonomer(8,3)=180.d0 

      msites(9)='C_6' 
      izmonomer(9,1)=8                       
      zmonomer(9,1)=rcc 
      izmonomer(9,2)=7     
      zmonomer(9,2)=thccc 
      izmonomer(9,3)=6
      zmonomer(9,3)=180.d0 

      msites(10)='O_7' 
      izmonomer(10,1)=9                      
      zmonomer(10,1)=1.229d0
      izmonomer(10,2)=8     
      zmonomer(10,2)=120.d0 
      izmonomer(10,3)=7
      zmonomer(10,3)=0.d0 

      nbnds=6

      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1  !single bonds
      enddo

      knbnds(6,3)=2  !double bond



      elseif(montype.eq.42) then
c
c     POXB     
c     
C     This is POXA with a minor modification:  methyl -> ethyl
c     Actual unit is NN-dimethyl propionamide 
c
c     CH3-N-CH3
c         |
C         C(=O)-CH2-CH3
c
c
      natmonomer=10  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0
      rcn=1.448d0
      rcon=1.335d0
      rcco=1.522d0  ! CT-C
      rccx=1.39d0 !-O-C(=O)-
      rcoz=1.229d0 !C=O oplsaa distance
      tccc=109.5d0 


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' 
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcc 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tccc 
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0 

      msites(5)='N_2' 
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcn    !should replace with -N-C- single bond from an amine
      izmonomer(5,2)=3     
      zmonomer(5,2)=tccc  
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 

      msites(6)='C_3' 
      izmonomer(6,1)=5     
      zmonomer(6,1)=rcn    !should replace with -N-C- single bond from an amine
      izmonomer(6,2)=4     
      zmonomer(6,2)=120.d0
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0 

      msites(7)='C_4'      !side chain carbon (carbonyl carbon)
      izmonomer(7,1)=5     
      zmonomer(7,1)=rcon   !should replace with -N-C- single bond from an amine
      izmonomer(7,2)=4     
      zmonomer(7,2)=120.d0
      izmonomer(7,3)=3
      zmonomer(7,3)=0.d0 

      msites(8)='C_5'      !methyl carbon                        
      izmonomer(8,1)=7     
      zmonomer(8,1)=rcco   !-C-C(=O) single bond from an aldehyde/ketone (CT-C )
      izmonomer(8,2)=5     
      zmonomer(8,2)=116.6  !-C-C(=O)-N angle (CT-C -N )
      izmonomer(8,3)=4
      zmonomer(8,3)=90.d0 

      msites(9)='O_6'      !carbonyl oxygen                      
      izmonomer(9,1)=7     
      zmonomer(9,1)=rcoz   !C=O bond
      izmonomer(9,2)=5     
      zmonomer(9,2)=122.9d0 ! O(=C)-C(=O)-N angle for oplsaa (O -C -N )
      izmonomer(9,3)=4
      zmonomer(9,3)=-90.d0 

      msites(10)='C_7'      !makes propionamide                   
      izmonomer(10,1)=8     
      zmonomer(10,1)=rcc    !C-C bond
      izmonomer(10,2)=7     
      zmonomer(10,2)=tccc   !use as CT-CT-CT
      izmonomer(10,3)=5
      zmonomer(10,3)=180.d0 


      nbnds=6
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1
      enddo
c     need  4-5 (C-N); 5-6 (N-C); 5-7 (N-C); 7-8 (C-C); 7-9 (C=O); 8-10 (C-C)
      knbnds(5,3)=2  !double bond
      knbnds(3,1)=5
      knbnds(5,1)=7
      knbnds(6,1)=8






      elseif(montype.eq.43) then
c
c     POXC     
c
C     This is POXA with a minor modification:  methyl -> isobutyl
c     Actual unit is NN-dimethyl 2,2-dimethyl propionamide 
c
c     CH3-N-CH3
c         |
C         C(=O)-C(CH3)3
c      
      natmonomer=12  !monomer sites, including three dummy sites 
      ndummy=3

      rcc=1.529d0
      rcn=1.448d0
      rcon=1.335d0
      rcco=1.522d0  ! CT-C
      rccx=1.39d0 !-O-C(=O)-
      rcoz=1.229d0 !C=O oplsaa distance
      tccc=109.5d0 


      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.0d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.0d0
      izmonomer(3,2)=1
      zmonomer(3,2)=90.d0
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0

      msites(4)='C_1' 
      izmonomer(4,1)=3    !first real site 
      zmonomer(4,1)=rcc 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tccc 
      izmonomer(4,3)=1
      zmonomer(4,3)=180.d0 

      msites(5)='N_2' 
      izmonomer(5,1)=4     
      zmonomer(5,1)=rcn    !should replace with -N-C- single bond from an amine
      izmonomer(5,2)=3     
      zmonomer(5,2)=tccc  
      izmonomer(5,3)=2
      zmonomer(5,3)=180.d0 

      msites(6)='C_3' 
      izmonomer(6,1)=5     
      zmonomer(6,1)=rcn    !should replace with -N-C- single bond from an amine
      izmonomer(6,2)=4     
      zmonomer(6,2)=120.d0
      izmonomer(6,3)=3
      zmonomer(6,3)=180.d0 

      msites(7)='C_4'      !side chain carbon (carbonyl carbon)
      izmonomer(7,1)=5     
      zmonomer(7,1)=rcon   !should replace with -N-C- single bond from an amine
      izmonomer(7,2)=4     
      zmonomer(7,2)=120.d0
      izmonomer(7,3)=3
      zmonomer(7,3)=0.d0 

      msites(8)='C_5'      !methyl carbon                        
      izmonomer(8,1)=7     
      zmonomer(8,1)=rcco   !-C-C(=O) single bond from an aldehyde/ketone (CT-C )
      izmonomer(8,2)=5     
      zmonomer(8,2)=116.6  !-C-C(=O)-N angle (CT-C -N )
      izmonomer(8,3)=4
      zmonomer(8,3)=90.d0 

      msites(9)='O_6'      !carbonyl oxygen                      
      izmonomer(9,1)=7     
      zmonomer(9,1)=rcoz   !C=O bond
      izmonomer(9,2)=5     
      zmonomer(9,2)=122.9d0 ! O(=C)-C(=O)-N angle for oplsaa (O -C -N )
      izmonomer(9,3)=4
      zmonomer(9,3)=-90.d0 

      msites(10)='C_7'      !makes propionamide                   
      izmonomer(10,1)=8     
      zmonomer(10,1)=rcc    !C-C bond
      izmonomer(10,2)=7     
      zmonomer(10,2)=tccc   !use as CT-CT-CT
      izmonomer(10,3)=5
      zmonomer(10,3)=180.d0 

      msites(11)='C_8'      !makes methyl propionamide                   
      izmonomer(11,1)=8     
      zmonomer(11,1)=rcc    !C-C bond
      izmonomer(11,2)=7     
      zmonomer(11,2)=tccc   !use as CT-CT-CT
      izmonomer(11,3)=5
      zmonomer(11,3)= 60.d0 

      msites(12)='C_9'      !makes dimethyl propionamide                   
      izmonomer(12,1)=8     
      zmonomer(12,1)=rcc    !C-C bond
      izmonomer(12,2)=7     
      zmonomer(12,2)=tccc   !use as CT-CT-CT
      izmonomer(12,3)=5
      zmonomer(12,3)=-60.d0 


      nbnds=8
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1
      enddo
c     need  4-5 (C-N); 5-6 (N-C); 5-7 (N-C); 7-8 (C-C); 7-9 (C=O); 8-10 (C-C); 8-11 (C-C); 8-12 (C-C)
      knbnds(5,3)=2  !double bond
      knbnds(3,1)=5
      knbnds(5,1)=7
      knbnds(6,1)=8
      knbnds(7,1)=8
      knbnds(8,1)=8





      elseif(montype.eq.44) then
c
c     PMMAA:  hydrophilic component that can joint to PS segment
c
c     (Based on AMAND1:  side chain for carbonate backbone from 
c     Amanda's hydrogels)            
c
c                   CH3      
c                   |         
c             CH3 - CH - CO - NH - CH2 - CH2 - OH
c                 
C     Option 1:  sidechain and methyl group are on C2
C
c             C1    C2   C3   N6   C7    C8    O9
c                   C4   O5          
c
C     Option 2:  sidechain and methyl group are on C1
C
c             C2    C1   C3   N6   C7    C8    O9
c                   C4   O5          
C     Option 2 is consistent with how PS arms are constructed,
c     so this is preferred if we connect PMMAA to PS
c
      natmonomer=12   !monomer sites, including three dummy sites 
      ndummy=3

      rcct=1.522d0   !C -CT single bond  OPLS-AA
      tcctct=111.1d0 !C -CT-CT  OPLS-AA
      rcoyl=1.229d0  !C -O     OPLS-AA
      tctco=120.4d0  !CT-C -O   OPLS-AA
      rcn  =1.335d0  !C -N     OPLS-AA
      tctcn=116.6d0  !CT-C -N  OPLS-AA
      rctn =1.449d0  !CT-N     OPLS-AA
      tcnct=121.9d0  !C -N -CT OPLS-AA
      rcc=1.529d0    !CT-CT single bond 
      tctctn=109.7d0 !CT-CT-N   
      rctoh=1.410d0  !CT-OH    OPLS-AA
      tccc=109.5d0   !CT-CT-CT also for CT-CT-OH

c     first dummy site surrogate for O_5 on CARBO1
c     second dummy site surrogate for C_6 on CARBO1
c     third dummy site surrogate for C_7 on CARBO1

      izmonomer(2,1)=1    !second dummy site 
      zmonomer(2,1)=1.d0
      izmonomer(2,2)=0
      zmonomer(2,2)=0.d0
      izmonomer(2,3)=0
      zmonomer(2,3)=0.d0

      izmonomer(3,1)=2    !third dummy site 
      zmonomer(3,1)=1.d0
      izmonomer(3,2)=1
      zmonomer(3,2)=tccc 
      izmonomer(3,3)=0
      zmonomer(3,3)=0.d0
 
      msites(4)='C_1'
      izmonomer(4,1)=3    !first real site backbone carbon
      zmonomer(4,1)=rcc 
      izmonomer(4,2)=2     
      zmonomer(4,2)=tccc
      izmonomer(4,3)=1
      zmonomer(4,3)= 180.d0 

      msites(5)='C_2'
      izmonomer(5,1)=4    !second backbone carbon
      zmonomer(5,1)=rcc 
      izmonomer(5,2)=3     
      zmonomer(5,2)=tccc
      izmonomer(5,3)=2
      zmonomer(5,3)= 180.d0 

      itac=1
      if(ran(iseed).ge.0.5d0) itac=-1
c     option 1 linkage is C_3 C_2 C_1 D_3
c     option 2 linkage is C_3 C_1 D_3 D_2

      msites(6)='C_3'     !carbonyl carbon (can be on either side of chain)
      izmonomer(6,1)=5    !option 1
      izmonomer(6,1)=4    !option 2
      zmonomer(6,1)=rcct
      izmonomer(6,2)=4    !option 1
      izmonomer(6,2)=3    !option 2
      zmonomer(6,2)=tccc
      izmonomer(6,3)=3    !option 1
      izmonomer(6,3)=2    !option 2
      zmonomer(6,3)=  itac*60.d0  !set to +/- 60 (opposite to below)

c     option 1 linkage is C_4 C_2 C_1 D_3
c     option 2 linkage is C_4 C_1 D_3 D_2
      msites(7)='C_4'     !methyl carbon (can be on either side of chain)
      izmonomer(7,1)=5    !option 1
      izmonomer(7,1)=4    !option 2
      zmonomer(7,1)=rcct
      izmonomer(7,2)=4    !option 1
      izmonomer(7,2)=3    !option 2
      zmonomer(7,2)=tccc
      izmonomer(7,3)=3    !option 1
      izmonomer(7,3)=2    !option 2
      zmonomer(7,3)= -zmonomer(6,3)  !set to +/- 60 (opposite of above)

      msites(8)='O_5'
      izmonomer(8,1)=6    !carbonyl oxygen                                  
      zmonomer(8,1)=rcoyl
      izmonomer(8,2)=5    !option 1     
      izmonomer(8,2)=4    !option 2     
      zmonomer(8,2)=120.d0
      izmonomer(8,3)=7
      zmonomer(8,3)=   0.d0                                     

      msites(9)='N_6'
      izmonomer(9,1)=6    !amide nitrogen                                   
      zmonomer(9,1)=rcn  
      izmonomer(9,2)=5    !option 1     
      izmonomer(9,2)=4    !option 1     
      zmonomer(9,2)=120.d0
      izmonomer(9,3)=7
      zmonomer(9,3)= 180.d0                                     

      msites(10)='C_7'
      izmonomer(10,1)=9    !methylene carbon                                 
      zmonomer(10,1)=rctn 
      izmonomer(10,2)=6     
      zmonomer(10,2)=120.d0
      izmonomer(10,3)=8
      zmonomer(10,3)=   0.d0                                     

      msites(11)='C_8'
      izmonomer(11,1)=10   !methylene carbon                                 
      zmonomer(11,1)=rcc  
      izmonomer(11,2)=9     
      zmonomer(11,2)=tccc   
      izmonomer(11,3)=6
      zmonomer(11,3)= 180.d0                                     

      msites(12)='O_9'
      izmonomer(12,1)=11   !hydroxyl oxygen                                  
      zmonomer(12,1)=rctoh
      izmonomer(12,2)=10    
      zmonomer(12,2)=tccc   
      izmonomer(12,3)=9 
      zmonomer(12,3)= 180.d0                                     


      nbnds=8
      do i=1,nbnds
        knbnds(i,1)=i+3
        knbnds(i,2)=i+4
        knbnds(i,3)=1
      enddo
      knbnds(4,3)=2  !double bond

c     need  4-5 (C-N); 5-6 (N-C); 5-7 (N-C); 7-8 (C-C); 7-9 (C=O); 8-10 (C-C); 8-11 (C-C); 8-12 (C-C)
      knbnds(3,1)=5
      knbnds(4,1)=6
      knbnds(5,1)=6

c     option 2 bonds are slightly different
      knbnds(2,1)=4
      knbnds(3,1)=4


      endif







      do i=1,nbnds
        iat=knbnds(i,1)
        jat=knbnds(i,2)
        n=lstbnd(iat,1,1)+1
        lstbnd(iat,1,1)=n
        lstbnd(iat,1,n+1)=jat
        lstbnd(iat,2,n+1)=knbnds(i,3)
        n=lstbnd(jat,1,1)+1
        lstbnd(jat,1,1)=n
        lstbnd(jat,1,n+1)=iat
        lstbnd(jat,2,n+1)=knbnds(i,3)
c       write(*,'('' Knbnds : '',i2,2x,a,2x,i2,2x,a,2x,i1)')
c    x    iat,msites(iat),jat,msites(jat),knbnds(i,3)
      enddo


c     do i=1,natmonomer
c       write(*,'('' Numbonds for site '',i2,''('',a,'') is '',i2)')
c    x    i,msites(i),lstbnd(i,1,1)
c       do  j=1,lstbnd(i,1,1)
c         write(*,'('' bnd betw sites '',i2,2x,a,2x,i2,a,'' type'',i1)')
c    x      i,msites(i),
c    x      lstbnd(i,1,j+1),msites(lstbnd(i,1,j+1)),
c    x      lstbnd(i,2,j+1)
c       enddo
c     enddo


      end

c
c     subroutine to pack up to four fields into a string
c
      subroutine packer(elfield,i1,i2,i3,string)
      character*(*) elfield,string
      character*120 line
      character*10 num
      dimension is(3)
      line=elfield
      call leftjust(line)
c     assume this field is either one or two characters, look for a space or an underscore
      ne=2 !assume 2 character element name
      if(line(2:2).eq.' '.or.line(2:2).eq.'_') ne=1 !set it to 1 character in these cases
      is(1)=i1
      is(2)=i2
      is(3)=i3
      
      do i=1,3
        if(is(i).ge.0) then
          write(num,'(i10)') is(i)
          ip=0
          do j=1,len(num)
            if(ip.eq.0.and.num(j:j).ne.' ') ip=j 
          enddo
          line(ne+1:ne+len(num)-ip+2)='_'//num(ip:len(num))   !move len(num)-j+1 characters
          ne=ne+len(num)-ip+2
        endif
      enddo

      if(ne.gt.len(string)) then
        write(*,'('' Error: character string too short '',
     x     ''for site name: *'',a,''*'')') line(1:ne)
        stop 'strings too short for site names in molecule'
      endif
      string=line

      end


c
c     subroutine to parse a string into an element field and up to 3 integers
c     unused fields will be returned with values of -1
c
      subroutine parser(string,elfield,lelfield,i1,i2,i3)
      character*(*) string,elfield
      character*120 line
      dimension is(3)
      do i=1,3
        is(i)=-1
      enddo
      line=string
      call leftjust(line)
      ne=2 !assume two character element name
      if(line(2:2).eq.' '.or.line(2:2).eq.'_') ne=1
      lelfield=ne
      elfield=line(1:ne)

      do i=1,3
        if(line(ne+1:ne+1).eq.'_') then
c         assume there is a number field after the underscore
c         look for next space or underscore
          l=index(line(ne+2:),'_')
          if(l.gt.0) then
            read(line(ne+2:ne+l),*) is(i)
            ne=ne+l
          else
            read(line(ne+2:),*) is(i)
            go to 10
          endif
        endif
      enddo

10    continue

      i1=is(1)
      i2=is(2)
      i3=is(3)

      end


c
c     upper case a string
c
      subroutine ucase(string)
      character*(*) string
      character*26 upper,lower
      upper='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
      lower='abcdefghijklmnopqrstuvwxyz'
      do i=1,len(string)
        j=index(lower,string(i:i))
        if(j.ne.0) string(i:i)=upper(j:j)
      enddo
      end

c
c     left justify a string
c
      subroutine leftjust(string)
      character*(*) string

      is=0
      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 10
        endif
      enddo

10    continue
      if(is.eq.0) return  !all blank string

      do i=1,len(string)-is+1
        string(i:i)=string(is+i-1:is+i-1)
      enddo
c     blank out the last is-1 characters
      do i=len(string)-is+2,len(string)
        string(i:i)=' '
      enddo

      end

c
c     subroutine to place a site given a row of z-matrix and coordinates for
c     the three sites it references
c     this is really a wrapper for i2x from Mulliken, where we use it to
c     position one site at a time
c
      subroutine place(ref,rab,ang,dih,coord)
      implicit real*8 (a-h,o-z)
      dimension ref(3,3),coord(3)
      dimension xyz(3,4),lnk(3,4),rabx(4),angx(4),dihx(4) !local
      pi=4.d0*datan(1.d0)
c
c     first column of ref holds (D1) site referenced for dihedral 
c     second column of ref holds (D2) site referenced for angle   
c     third column of ref holds (D3) site referenced for distance 

      lnk(1,1)=0  !not used
      lnk(2,1)=0  !not used
      lnk(3,1)=0  !not used

      lnk(1,2)=1
      lnk(2,2)=0  !not used
      lnk(3,2)=0  !not used

      lnk(1,3)=2
      lnk(2,3)=1 
      lnk(3,3)=0  !not used

      lnk(1,4)=3
      lnk(2,4)=2 
      lnk(3,4)=1  

      do i=1,3
        do j=1,3
          xyz(j,i)=ref(j,i)
        enddo
      enddo

c
c     write(*,'(/,'' Ref coords before call to i2x: '')')
c     do i=1,3
c       write(*,'('' Site '',i1,2x,3f10.4)') i,(xyz(k,i),k=1,3)
c     enddo
c     write(*,'('' r,theta,phi = '',3f10.5)') rab,ang,dih

      nat=4
      istart=4
      iend=4
      rabx(4)=rab
      angx(4)=(pi/180.d0)*ang
      dihx(4)=(pi/180.d0)*dih

      call i2x(nat,lnk,rabx,angx,dihx,istart,iend,xyz)

      do i=1,3
        coord(i)=xyz(i,4)
      enddo

c     write(*,'('' Site '',i1,2x,3f10.4)') 4,(xyz(k,4),k=1,3)



      end

c Change History:
c $Log: util.f,v $
c Revision 1.8  1994/08/03  19:49:54  swope
c Changed behavior of xyz2int so that zmatricies are not printed out tha
c undefined torsions.  When angles are close to 180 they are changed to
c when close to 0 they are changed to 0.0001.    (Swope Aug 3, 1994)
c
c Revision 1.6  1994/07/30  06:33:09  swope
c Several changes by Swope to internal-cartesian and cartesian-internal
c conversion routines:
c angint-switch to low angle expression on new threshold
c dihint-call tlkemsg when atoms too close
c xyz2int-change messages during certain errors
c i2x-changes in ways linear systems treated: two strikes policy
c     for generating cartesians with undefined torsions
c xyz2dih-added new return code to flag 1-2-3 linear vs 2-3-4 linear
c
c Revision 1.5  1994/07/07  16:34:02  gclie
c This version includes xyz2int and int2xyz from Swope.  It should repla
c
c*arc i2x
      subroutine i2x(nat,lnk,rab,ang,dih,istart,iend,xyz)
c
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Copyright (c) 1994 International Business Machines Corporation
C  All rights reserved
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Description      Given the link vector and internal coordinates
c                     of a list of particles, this subroutine computes
c                     the cartesian coordinates for a subset of the
c                     particles, those with indicies from istart to
c                     iend.  The units of the cartesian coordinates
c                     will be the same as those of the input internals.
c                     The units of the angles are assumed to be radians.
c                   If the particle indicies between istart and iend
c                     fall in the range 1,2 or 3, particle 1 is placed
c                     at the origin, particle 2 is placed on the x-axis,
c                     and particle 3 is placed in the x-y plane.
c
C
C  Input
C
C     nat           Number of atoms in list
C
C     lnk(3,nat)    link vector for describing internals
C                     lnk(1,i) gives label for distance from i
C                     lnk(2,i) gives label for angle i-lnk(1,i)-lnk(2,i)
C                     lnk(3,i) gives label for torsion
C                     i-lnk(1,i)-lnk(2,i)-lnk(3,i)
C                     Note that first six entries are ignored.
C
C     rab(nat)      Vector of distances.
C
C     ang(nat)      Vector of angles.  Range returned is 0.le.ang.le.pi.
C
C     dih(nat)      Vector of torsions. Range returned is
C                   -pi .lt. phi .le. pi, and follows IUPAC
C                   sign and range convention.
c
c     istart        The index of the first atom whose cartesian
c                   coordinates are to be computed.  istart must be
c                   an integer with ( 1 .le. istart .le. nat ).
c
c     iend          The index of the last atom whose cartesian
c                   coordinates are to be computed.  iend must be
c                   an integer with ( 1 .le. iend .le. nat ).
C
C
C  Output
C
C     xyz(3,i)      cartesian coordinates of atoms i=istart to iend
C
C
C  Linkage          List of subroutines called by this one
C
C  Programmer       William Swope (some from Carnevali and Nesbet)
C
C  Date             June 1994
C
C  Update log       Record of all updates including names of programmers
C                     descriptions of changes made, and dates of change.
C                   07-29-94 Modified by Swope to die (tlkemsg) if
C                            there are TWO invalid torsion angles.
C                            Invalid torsion angles are those that
C                            result in ill-defined cartesian coordinates
C                            This change still allows linear molecules
C                            and molecules with two long linear segments
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
c
      Implicit Real*8 (a, b, d-h, o, p, r-y)
      Implicit Integer (i-n)
c     Violation of ARCHEM conventions, but there are no logicals here
c     Implicit Logical (z)
      Implicit Real*8 (z)
      Implicit Character (c)
c     Implicit Undefined (q)
 
      character cproduct*48
      dimension cproduct(3)
 
c     Input and output arrays
      real*8 xyz(3,nat),rab(nat),ang(nat),dih(nat)
      integer lnk(3,nat)
 
c     Local arrays
c     Following arrays give molecule coordinate system relative to lab
      real*8 xhat(3),yhat(3),zhat(3),zprev(3)
      real*8 r12(3),r23(3),r43(3)
 
 
C     THE SIGN CONVENTION FOR PHI IS AS FOLLOWS:
C     NUMBER THE ASSOCIATED ATOMS 1, 2, 3, 4.  ATOMS 1 AND 4 ARE
C     "TERMINAL" ATOMS AND THE DIHEDRAL ANGLE IS ABOUT THE BOND
C     BETWEEN ATOMS 2 AND 3.  IMAGINE HOLDING THE PLANE FORMED BY
C     ATOMS 1, 2, AND 3 FIXED AND ROTATING THE PLANE FORMED BY
C     ATOMS 2, 3, AND 4 ABOUT A VECTOR THAT POINTS FROM ATOM 2
C     TO ATOM 3.  THE POSITIVE SENSE CORRESPONDS TO THE RIGHT-HAND
C     RULE FOR ROTATION ABOUT THE VECTOR FROM ATOM 2 TO ATOM 3.
c     This agrees with the IUPAC convention.
C
C     FURTHERMORE, PHI=0 CORRESPONDS TO A CONDITION WHERE ALL FOUR
C     ATOMS ARE ON A PLANE AND ATOMS 1 AND 4 ARE "CIS" TO THE BOND
C     BETWEEN ATOMS 2 AND 3.  LIKEWISE, PHI=180 DEGREES CORRESPONDS
C     A CONDITION WHERE ALL FOUR ATOMS ARE ON A PLANE AND ATOMS 1 AND
C     4 ARE "TRANS" TO THE BOND BETWEEN ATOMS 2 AND 3.
 
c     comment out the following call to check the integrity of the
c     link vector.  this call is appropriate in int2xyz, where the
c     full set of cartesians is computed, but should probably not
c     be made over and over again if the cartesians are computed
c     one at a time or in small increments.
c
c     check validity of link vector
c     call chklnk(nat,lnk,iret)
c     if(iret.ge.2) then
c       print *, ' Error: link matrix is not usable for determination',
c    >           ' of cartesian coordinates.'
c       print *, ' Complete link vector: '
c       write(*,'(3i10)') ((lnk(i,j),i=1,3),j=1,nat)
c       call tlkemsg(' Invalid link matrix.','int2xyz')
c     endif
 
 
c     lnk(1,i),i=1,nat specifies atom indicies for "bond" distances
c     lnk(2,i),i=1,nat specifies atom indicies for "bond" angles
c     lnk(3,i),i=1,nat specifies atom indicies for dihedral angles
 
      pi=4.d0*datan(1.d0)
 
      if(istart.le.0   .or.
     >   istart.gt.nat .or.
     >   iend.le.0     .or.
     >   iend.gt.nat   .or.
     >   istart.gt.iend ) then
      write(*,'('' Error in subroutine i2x: invalid arguments for'',/,
     >          '' atom indicies: istart='',i5,'' iend='',i5,/,
     >          '' Values should be from 1 to nat='',i5)')
     >          istart,iend,nat
C     CALL TLKEMSG(' INVALID ATOM INDICES PASSED TO X2I.','X2I')
      endif
 
      zprev(1)=0.d0
      zprev(2)=0.d0
      zprev(3)=1.d0
 
c     failure count on placing atoms when torsion undefined
      nstrike=0
 
 
      do 10 i=istart,iend
 
      if(i.eq.1) then
c     if first atom
        xyz(1,1)=0.d0
        xyz(2,1)=0.d0
        xyz(3,1)=0.d0
      elseif(i.eq.2) then
c     if second atom
        xyz(1,2)=rab(2)
        xyz(2,2)=0.d0
        xyz(3,2)=0.d0
      elseif(i.eq.3) then
c     if third atom (note that normally 0.le.ang.le.pi)
        if(lnk(1,3).eq.2) then
          xyz(1,3)=rab(2) -rab(3)*cos(ang(3))
        else
          xyz(1,3)=       +rab(3)*cos(ang(3))
        endif
        xyz(2,3)=         +rab(3)*sin(ang(3))
        xyz(3,3)=0.d0
        if(abs(ang(3)).gt.1.d-3 .and.
     >     abs(abs(ang(3))-pi).gt.1.d-3) nstrike=1
      endif
 
c     for fourth or higher atom
      if(i.le.3) go to 10
 
      l2=lnk(1,i)
      l3=lnk(2,i)
      l4=lnk(3,i)
      r23(1)=xyz(1,l2)-xyz(1,l3)
      r23(2)=xyz(2,l2)-xyz(2,l3)
      r23(3)=xyz(3,l2)-xyz(3,l3)
 
c     local x axis is directed from atom l3 to atom l2
      d=r23(1)**2+r23(2)**2+r23(3)**2
      if(d.le.1.d-10) then
        print *, ' Error: Atoms too close to establish unique',
     >           ' directions for cartesian placement from z-matrix.'
        print *, ' Atom indicies: ',l2,l3
C       CALL TLKEMSG(' ATOMS TOO CLOSE.','I2X')
      endif
      d=1.d0/sqrt(d)
      xhat(1)=r23(1)*d
      xhat(2)=r23(2)*d
      xhat(3)=r23(3)*d
 
c     local z axis is directed along r23 cross r43, if this is non-zero
      r43(1)=xyz(1,l4)-xyz(1,l3)
      r43(2)=xyz(2,l4)-xyz(2,l3)
      r43(3)=xyz(3,l4)-xyz(3,l3)
      zhat(1)=r23(2)*r43(3)-r23(3)*r43(2)
      zhat(2)=r23(3)*r43(1)-r23(1)*r43(3)
      zhat(3)=r23(1)*r43(2)-r23(2)*r43(1)
      d=zhat(1)**2+zhat(2)**2+zhat(3)**2
c
      if(d.le.1.d-8) then
        if(abs(ang(i)).gt.1.d-3 .and.
     >     abs(abs(ang(i))-pi).gt.1.d-3) then
c         Problem: angle 4-3-2 is (nearly) linear AND ang(i) is other
c         than zero or pi.  This means placement of particle 1 is
c         undefined.  The first time this happens, we will allow it,
c         and place particle 1 in the (local) x-y plane.  This
c         corresponds to setting zhat to the lab z axis.
          if(nstrike.ne.0) then
          print *
          print *, ' A sequence of bond angles near 180 (or 0) degrees'
          print *, ' has resulted in an undefined torsion angle and'
          print *, ' this torsion angle must be defined to determine'
          print *, ' unique Cartesian coordinates for the molecule.'
          print *
          print *, ' Offending link vector:'
          print *, ' lnk(1,',i,') = ',l2
          print *, ' lnk(2,',i,') = ',l3
          print *, ' lnk(3,',i,') = ',l4
          print *
C         CALL TLKEMSG('CANNOT GENERATE CARTESIAN COORDINATES.','I2X')
          endif
          nstrike=1
        endif
        zhat(1)=zprev(1)
        zhat(2)=zprev(2)
        zhat(3)=zprev(3)
      else
        d=1.d0/sqrt(d)
        zhat(1)=zhat(1)*d
        zhat(2)=zhat(2)*d
        zhat(3)=zhat(3)*d
        zprev(1)=zhat(1)
        zprev(2)=zhat(2)
        zprev(3)=zhat(3)
      endif
 
c     local y axis is zhat cross xhat
      yhat(1)=zhat(2)*xhat(3)-zhat(3)*xhat(2)
      yhat(2)=zhat(3)*xhat(1)-zhat(1)*xhat(3)
      yhat(3)=zhat(1)*xhat(2)-zhat(2)*xhat(1)
      d=1.d0/sqrt(yhat(1)**2+yhat(2)**2+yhat(3)**2)
      yhat(1)=yhat(1)*d
      yhat(2)=yhat(2)*d
      yhat(3)=yhat(3)*d
 
      if(abs(ang(i)).gt.1.d-3 .and.
     >   abs(abs(ang(i))-pi).gt.1.d-3) nstrike=1
 
c     in molecule frame (i.e., relative to xhat,yhat,zhat coord system)
c     new center is placed at ...
      x=-rab(i)*cos(ang(i))
      rho=rab(i)*sin(ang(i))
      y=rho*cos(dih(i))
      z=rho*sin(dih(i))
 
c     in lab frame ...
      xyz(1,i) = xyz(1,l2) + x*xhat(1)+y*yhat(1)+z*zhat(1)
      xyz(2,i) = xyz(2,l2) + x*xhat(2)+y*yhat(2)+z*zhat(2)
      xyz(3,i) = xyz(3,l2) + x*xhat(3)+y*yhat(3)+z*zhat(3)
 
10    continue
 
      end


c
c     angle calculation stolen from dihint
c
      subroutine angint(xyz1,xyz2,xyz3,ang)
      Implicit Real*8 (a-h,o-z)
c     Input and output arrays
      real*8 xyz1(3),xyz2(3),xyz3(3)
      real*8 a(3),b(3)

c     Compute a=r1-r2, b=r3-r2
      do 10 k=1,3
      a(k)=xyz1(k)-xyz2(k)
10    b(k)=xyz3(k)-xyz2(k)
 
c     Compute a . b 
      dot=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
 
c     Compute lengths
      b2=b(1)**2+b(2)**2+b(3)**2
      a2=a(1)**2+a(2)**2+a(3)**2
      angcos=dot/dsqrt(a2*b2)
      ang=dacos(angcos)

      end





c Change History:
c $Log$
c*arc dihint
      subroutine dihint(iop,xyz1,xyz2,xyz3,xyz4,
     >       phi,dphidx,d2phidx2)
c
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Copyright (c) 1994 International Business Machines Corporation
C  All rights reserved
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Description      Given the coordinates of four particles, this
C                     subroutine computes the torsion (dihedral)
C                     angle between them, the Jacobean (dphi/dx(i))
C                     and d2phi/dx(i)dx(j).
C
C  Input
C
C     iop           Control
C                   iop = 1 means just compute angle
C                   iop = 2 means compute angle and Jacobean
C                   iop = 3 means compute angle, Jacobean and second
C                           derivative
C
C     xyz1(3)       cartesian coordinates of first atom
C     xyz2(3)       cartesian coordinates of second atom
C     xyz3(3)       cartesian coordinates of third atom
C     xyz4(3)       cartesian coordinates of fourth atom
C
C
C  Output
C
C     phi           Angle between atoms 1-2-3-4.  Range returned is
C                   -pi .lt. phi .le. pi, following IUPAC
C                   sign and range convention.
C
C     dphidx(12)    Jacobean: dphi/dx(1), dphi/dy(1), ...
C
C     d2phid2(45)   Second derivative, d2phi/dx(i)dx(2), a symmetric
C                   matrix, stored lower triangle by row
C
C  Linkage          List of subroutines called by this one
C
C  Programmer       William Swope (some from Carnevali)
C
C  Date             June 1994
C
C  Update log       Record of all updates including names of programmers,
C                     descriptions of changes made, and dates of change.
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
c
      Implicit Real*8 (a, b, d-h, o, p, r-y)
      Implicit Integer (i-n)
c     Implicit Logical (z)
c     Violation of ARCHEM convention; but no logical data here
      Implicit Real*8 (z)
c     Implicit Character (c)
c     Violation of ARCHEM convention; but no character data here
      Implicit Real*8 (c)
c     Implicit Undefined (q)
 
      character cproduct*48
      dimension cproduct(3)
 
c     Input and output arrays
      real*8 xyz1(3),xyz2(3),xyz3(3),xyz4(3)
      real*8 dphidx(12),d2phidx2(78)
 
 
C     THE SIGN CONVENTION FOR PHI IS AS FOLLOWS:
C     NUMBER THE ASSOCIATED ATOMS 1, 2, 3, 4.  ATOMS 1 AND 4 ARE
C     "TERMINAL" ATOMS AND THE DIHEDRAL ANGLE IS ABOUT THE BOND
C     BETWEEN ATOMS 2 AND 3.  IMAGINE HOLDING THE PLANE FORMED BY
C     ATOMS 1, 2, AND 3 FIXED AND ROTATING THE PLANE FORMED BY
C     ATOMS 2, 3, AND 4 ABOUT A VECTOR THAT POINTS FROM ATOM 2
C     TO ATOM 3.  THE POSITIVE SENSE CORRESPONDS TO THE RIGHT-HAND
C     RULE FOR ROTATION ABOUT THE VECTOR FROM ATOM 2 TO ATOM 3.
c     This agrees with the IUPAC convention.
C
C     FURTHERMORE, PHI=0 CORRESPONDS TO A CONDITION WHERE ALL FOUR
C     ATOMS ARE ON A PLANE AND ATOMS 1 AND 4 ARE "CIS" TO THE BOND
C     BETWEEN ATOMS 2 AND 3.  LIKEWISE, PHI=180 DEGREES CORRESPONDS
C     A CONDITION WHERE ALL FOUR ATOMS ARE ON A PLANE AND ATOMS 1 AND
C     4 ARE "TRANS" TO THE BOND BETWEEN ATOMS 2 AND 3.
 
 
c     Local and scratch arrays
 
      dimension a(3),b(3),c(3),u(3),v(3),acrossc(3)
 
c     First derivatives of u**2, v**2 with respect to a, b, and c
      dimension du2da(3),du2db(3),dv2db(3),dv2dc(3)
 
c     First derivatives of D=u**2*v**2 and F=D**(-1/2) with
c     respect to a, b, and c
      dimension ddda(3),dddb(3),dddc(3)
      dimension dfda(3),dfdb(3),dfdc(3)
 
c     First derivatives of uv with respect to a, b, and c
      dimension duvda(3),duvdb(3),duvdc(3)
 
c     Derivatives of S with respect to a, b, and c
      dimension dsda(3),dsdb(3),dsdc(3)
 
c     Derivatives of cosphi and sinphi with respect to a, b, and c
      dimension dcosphida(3),dcosphidb(3),dcosphidc(3)
      dimension dsinphida(3),dsinphidb(3),dsinphidc(3)
 
c     Derivatives of phi with respect to a, b, and c
      dimension dphida(3),dphidb(3),dphidc(3)
 
c     Second derivatives
      dimension du2dada(3,3),du2dbdb(3,3),du2dadb(3,3)
      dimension dv2dbdb(3,3),dv2dbdc(3,3),dv2dcdc(3,3)
      dimension dddada(3,3),dddbdb(3,3),dddcdc(3,3)
      dimension dddadb(3,3),dddadc(3,3),dddbdc(3,3)
      dimension dfdada(3,3),dfdbdb(3,3),dfdcdc(3,3)
      dimension dfdadb(3,3),dfdadc(3,3),dfdbdc(3,3)
      dimension              duvdbdb(3,3)
      dimension dsdadb(3,3),dsdadc(3,3),dsdbdc(3,3)
      dimension              dsdbdb(3,3)
      dimension duvdadb(3,3),duvdadc(3,3),duvdbdc(3,3)
      dimension dcosphidada(3,3),dcosphidbdb(3,3),dcosphidcdc(3,3)
      dimension dcosphidadb(3,3),dcosphidadc(3,3),dcosphidbdc(3,3)
      dimension dsinphidada(3,3),dsinphidbdb(3,3),dsinphidcdc(3,3)
      dimension dsinphidadb(3,3),dsinphidadc(3,3),dsinphidbdc(3,3)
      dimension dphidada(3,3),dphidbdb(3,3),dphidcdc(3,3)
      dimension dphidadb(3,3),dphidadc(3,3),dphidbdc(3,3)
 
      dimension atilde(3,3),btilde(3,3),ctilde(3,3)
 
c     identity tensor here to simplify some loops
      real*8 delta(3,3)/1.d0,3*0.d0,1.d0,3*0.d0,1.d0/
 
      data cproduct /'$RCSfile$',
     $               '$Revision$$Date$',
     $               '$IBMCSA: PRODUCT VERSION NUMBER $'/
 
c     Statement function gives index of (i,j) in lower triang by row
      lowtri(i,j) = j + i*(i-1)/2
 
      if(iop.ne.1 .and. iop.ne.2 .and. iop.ne.3) then
        print *, ' Incorrect input to dihint subroutine: iop = ',iop
c       call arcdie()
      endif
 
      ierror=0
 
      if(iop.ge.1) then
c     compute angle
 
c     Compute a=r2-r1, b=r3-r2, c=r4-r3
      do 10 k=1,3
      a(k)=xyz2(k)-xyz1(k)
      b(k)=xyz3(k)-xyz2(k)
10    c(k)=xyz4(k)-xyz3(k)
 
c     Compute u = a x b , v = b x c, vectors normal to 123, 234 planes
      u(1)=a(2)*b(3)-a(3)*b(2)
      u(2)=a(3)*b(1)-a(1)*b(3)
      u(3)=a(1)*b(2)-a(2)*b(1)
      v(1)=b(2)*c(3)-b(3)*c(2)
      v(2)=b(3)*c(1)-b(1)*c(3)
      v(3)=b(1)*c(2)-b(2)*c(1)
 
c     Compute lengths and scalar products
      b2=b(1)**2+b(2)**2+b(3)**2
      u2=u(1)**2+u(2)**2+u(3)**2
      v2=v(1)**2+v(2)**2+v(3)**2
      if( b2.eq.0.d0 )then
c       this should be a hard error; print out message and die
c       if this happens there is something seriously wrong
        print *, ' Error:  Can not compute torsion angle when central',
     >           ' bond has zero length.'
c       call arcdie('dihint')
      endif
      b1=sqrt(b2)
      u1=sqrt(u2)
      v1=sqrt(v2)
      d=u2*v2
      d1=sqrt(d)
      uv=u(1)*v(1)+u(2)*v(2)+u(3)*v(3)
      av=a(1)*v(1)+a(2)*v(2)+a(3)*v(3)
      s=b1*av
      if( u2.eq.0.d0 .or. v2.eq.0.d0 )then
        print *, ' Error:  Three collinear atoms make torsion angle',
     >           ' undefined.'
        print *, ' Zero returned for angle, gradient and hessian.'
        ierror=1
        f=0.d0
        d=1.d0
        d1=1.d0
c       Compute cosphi and sinphi
        cosphi=1.d0
        sinphi=0.d0
      else
        f=1.d0/d1
c       Compute cosphi and sinphi
        cosphi=uv*f
        sinphi=s*f
      endif
 
c     Compute phi itself, produce range of -pi.lt.phi.le.pi
      phi=atan2(sinphi,cosphi)
 
      endif
 
      if(iop.ge.2) then
c     compute dphidx
 
c     Compute derivatives of u**2 and v**2 with respect to a, b, and c.
      a2=a(1)**2+a(2)**2+a(3)**2
      c2=c(1)**2+c(2)**2+c(3)**2
      ab=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
      bc=b(1)*c(1)+b(2)*c(2)+b(3)*c(3)
      ac=a(1)*c(1)+a(2)*c(2)+a(3)*c(3)
      do 20 k=1,3
      du2da(k)=2.d0*(b2*a(k)-ab*b(k))
      du2db(k)=2.d0*(a2*b(k)-ab*a(k))
      dv2db(k)=2.d0*(c2*b(k)-bc*c(k))
20    dv2dc(k)=2.d0*(b2*c(k)-bc*b(k))
 
c     Compute derivatives of D=u**2*v**2 with respect to a, b, and c.
      do 35 k=1,3
      ddda(k)=v2*du2da(k)
      dddb(k)=v2*du2db(k)+u2*dv2db(k)
35    dddc(k)=u2*dv2dc(k)
 
c     Compute derivatives of F=D**(-1/2) with respect to a, b, and c.
      do 40 k=1,3
      dfda(k)=-0.5d0*ddda(k)/(d*d1)
      dfdb(k)=-0.5d0*dddb(k)/(d*d1)
40    dfdc(k)=-0.5d0*dddc(k)/(d*d1)
 
 
 
      if(abs(sinphi).gt.abs(cosphi))then
c     if(.false.)then
c*************************************************
c     CASE A - use expressions derived from cosphi
c     Compute derivatives of u*v=C with respect to a, b, and c.
      do 50 k=1,3
      duvda(k)=b(k)*bc-c(k)*b2
      duvdb(k)=a(k)*bc+c(k)*ab-2.d0*b(k)*ac
50    duvdc(k)=b(k)*ab-a(k)*b2
c     Compute derivatives of cosphi=(u*v)*F=C*F with respect to a, b, and c.
      do 60 k=1,3
      dcosphida(k)=duvda(k)*f+uv*dfda(k)
      dcosphidb(k)=duvdb(k)*f+uv*dfdb(k)
60    dcosphidc(k)=duvdc(k)*f+uv*dfdc(k)
c     Compute derivatives of phi with respect to a, b, and c.
      do 70 k=1,3
      dphida(k)=-dcosphida(k)/sinphi
      dphidb(k)=-dcosphidb(k)/sinphi
70    dphidc(k)=-dcosphidc(k)/sinphi
c*************************************************
      else
c*************************************************
c     CASE B - use expressions derived from sinphi
      acrossc(1)=a(2)*c(3)-a(3)*c(2)
      acrossc(2)=a(3)*c(1)-a(1)*c(3)
      acrossc(3)=a(1)*c(2)-a(2)*c(1)
      s=b1*av
c     Compute derivatives of S=b(a*v) with respect to a, b, and c.
      do 85 k=1,3
      dsda(k)=b1*v(k)
      dsdb(k)=b(k)*av/b1-b1*acrossc(k)
85    dsdc(k)=b1*u(k)
c     Compute derivatives of sinphi with respect to a, b, and c.
      do 90 k=1,3
      dsinphida(k)=dsda(k)*f+s*dfda(k)
      dsinphidb(k)=dsdb(k)*f+s*dfdb(k)
90    dsinphidc(k)=dsdc(k)*f+s*dfdc(k)
c     Compute derivatives of phi with respect to a, b, and c.
      do 95 k=1,3
      dphida(k)=dsinphida(k)/cosphi
      dphidb(k)=dsinphidb(k)/cosphi
95    dphidc(k)=dsinphidc(k)/cosphi
c*************************************************
 
      endif
 
c     Add contribution to dphi/dx array
      do 80 k=1,3
      dphidx(k  )=          -dphida(k)
      dphidx(3+k)= dphida(k)-dphidb(k)
      dphidx(6+k)= dphidb(k)-dphidc(k)
80    dphidx(9+k)= dphidc(k)
 
c     This code is for three atoms collinear; torsion undefined
      if(ierror.eq.1) then
        do 81 k=1,12
81      dphidx(k)=0.d0
      endif
 
      endif
 
      if(iop.ge.3) then
 
c     Compute second derivatives of u**2 and v**2 with respect to a, b, and c.
      do 110 i=1,3
      do 110 j=1,3
      du2dada(i,j)=2.d0*(b2*delta(i,j)-b(i)*b(j))
      du2dbdb(i,j)=2.d0*(a2*delta(i,j)-a(i)*a(j))
      du2dadb(i,j)=2.d0*(2.d0*a(i)*b(j)-b(i)*a(j)-ab*delta(i,j))
      dv2dbdb(i,j)=2.d0*(c2*delta(i,j)-c(i)*c(j))
      dv2dcdc(i,j)=2.d0*(b2*delta(i,j)-b(i)*b(j))
      dv2dbdc(i,j)=2.d0*(2.d0*b(i)*c(j)-c(i)*b(j)-bc*delta(i,j))
110   continue
 
c     Compute second derivatives of D=u**2*v**2 with respect to a, b, and c.
      do 120 i=1,3
      do 120 j=1,3
      dddada(i,j)=
     >            v2*du2dada(i,j)
      dddbdb(i,j)=dv2db(i)*du2db(j)+du2db(i)*dv2db(j)
     >           +v2*du2dbdb(i,j)  +u2*dv2dbdb(i,j)
      dddcdc(i,j)=
     >                             +u2*dv2dcdc(i,j)
      dddadb(i,j)=                 +du2da(i)*dv2db(j)
     >           +v2*du2dadb(i,j)
      dddadc(i,j)=                 +du2da(i)*dv2dc(j)
     >
      dddbdc(i,j)=                 +du2db(i)*dv2dc(j)
     >                             +u2*dv2dbdc(i,j)
120   continue
 
c     Compute second derivatives of F=D**(-1/2) with respect to a, b, and c.
      f3=f/d
      do 130 i=1,3
      do 130 j=1,3
      dfdada(i,j)=.5d0*f3*(1.5d0*ddda(i)*ddda(j)/d-dddada(i,j))
      dfdbdb(i,j)=.5d0*f3*(1.5d0*dddb(i)*dddb(j)/d-dddbdb(i,j))
      dfdcdc(i,j)=.5d0*f3*(1.5d0*dddc(i)*dddc(j)/d-dddcdc(i,j))
      dfdadb(i,j)=.5d0*f3*(1.5d0*ddda(i)*dddb(j)/d-dddadb(i,j))
      dfdadc(i,j)=.5d0*f3*(1.5d0*ddda(i)*dddc(j)/d-dddadc(i,j))
      dfdbdc(i,j)=.5d0*f3*(1.5d0*dddb(i)*dddc(j)/d-dddbdc(i,j))
130   continue
 
      if(abs(sinphi).gt.abs(cosphi))then
c     if(.false.)then
c*************************************************
c     CASE A - use expressions derived from cosphi
c     Compute second derivatives of u*v=C with respect to a, b, and c.
      do 140 i=1,3
      do 140 j=1,3
      duvdbdb(i,j)=a(i)*c(j)+c(i)*a(j)-2.d0*ac*delta(i,j)
      duvdadb(i,j)=bc*delta(i,j)+b(i)*c(j)-2.d0*c(i)*b(j)
      duvdadc(i,j)=b(i)*b(j)-b2*delta(i,j)
      duvdbdc(i,j)=ab*delta(i,j)+a(i)*b(j)-2.d0*b(i)*a(j)
140   continue
c     Compute second derivatives of cosphi=(u*v)*F=C*F with respect to a, b, and
c.
      do 150 i=1,3
      do 150 j=1,3
      dcosphidada(i,j)=
     >                +dfda(i)*duvda(j)+duvda(i)*dfda(j)+uv*dfdada(i,j)
      dcosphidbdb(i,j)=
     >  duvdbdb(i,j)*f+dfdb(i)*duvdb(j)+duvdb(i)*dfdb(j)+uv*dfdbdb(i,j)
      dcosphidcdc(i,j)=
     >                +dfdc(i)*duvdc(j)+duvdc(i)*dfdc(j)+uv*dfdcdc(i,j)
      dcosphidadb(i,j)=
     >  duvdadb(i,j)*f+dfda(i)*duvdb(j)+duvda(i)*dfdb(j)+uv*dfdadb(i,j)
      dcosphidadc(i,j)=
     >  duvdadc(i,j)*f+dfda(i)*duvdc(j)+duvda(i)*dfdc(j)+uv*dfdadc(i,j)
      dcosphidbdc(i,j)=
     >  duvdbdc(i,j)*f+dfdb(i)*duvdc(j)+duvdb(i)*dfdc(j)+uv*dfdbdc(i,j)
150   continue
c     Compute second derivatives of phi with respect to a, b, and c.
      z1=-cosphi/(sinphi**3)
      z2=-1.d0/sinphi
      do 160 i=1,3
      do 160 j=1,3
      dphidada(i,j)=z1*dcosphida(i)*dcosphida(j)+z2*dcosphidada(i,j)
      dphidbdb(i,j)=z1*dcosphidb(i)*dcosphidb(j)+z2*dcosphidbdb(i,j)
      dphidcdc(i,j)=z1*dcosphidc(i)*dcosphidc(j)+z2*dcosphidcdc(i,j)
      dphidadb(i,j)=z1*dcosphida(i)*dcosphidb(j)+z2*dcosphidadb(i,j)
c     note dcosphidadc is non zero but dphidadc is zero - don't compute
      dphidadc(i,j)=z1*dcosphida(i)*dcosphidc(j)+z2*dcosphidadc(i,j)
      dphidbdc(i,j)=z1*dcosphidb(i)*dcosphidc(j)+z2*dcosphidbdc(i,j)
160   continue
      else
c*************************************************
c     CASE B - use expressions derived from sinphi
c     Compute the product of the antisymmetric tensor times a, b, and c
      call antsym(a,atilde)
      call antsym(b,btilde)
      call antsym(c,ctilde)
c     Compute second derivatives of S=b(a*v) with respect to a, b, and c.
      b3=b1*b2
      do 210 i=1,3
      do 210 j=1,3
      dsdbdb(i,j)=(delta(i,j)/b1-b(i)*b(j)/b3)*av
     >            -(acrossc(i)*b(j)+acrossc(j)*b(i))/b1
      dsdadb(i,j)=v(i)*b(j)/b1+b1*ctilde(i,j)
      dsdadc(i,j)=-b1*btilde(i,j)
      dsdbdc(i,j)=b(i)*u(j)/b1+b1*atilde(i,j)
210   continue
c     Compute second derivatives of sinphi with respect to a, b, and c.
      do 220 i=1,3
      do 220 j=1,3
      dsinphidada(i,j)=
     >                +dfda(i)*dsda(j)+dsda(i)*dfda(j)+s*dfdada(i,j)
      dsinphidbdb(i,j)=
     >  dsdbdb(i,j)*f+dfdb(i)*dsdb(j)+dsdb(i)*dfdb(j)+s*dfdbdb(i,j)
      dsinphidcdc(i,j)=
     >                +dfdc(i)*dsdc(j)+dsdc(i)*dfdc(j)+s*dfdcdc(i,j)
      dsinphidadb(i,j)=
     >  dsdadb(i,j)*f+dfda(i)*dsdb(j)+dsda(i)*dfdb(j)+s*dfdadb(i,j)
      dsinphidadc(i,j)=
     >  dsdadc(i,j)*f+dfda(i)*dsdc(j)+dsda(i)*dfdc(j)+s*dfdadc(i,j)
      dsinphidbdc(i,j)=
     >  dsdbdc(i,j)*f+dfdb(i)*dsdc(j)+dsdb(i)*dfdc(j)+s*dfdbdc(i,j)
220   continue
c     Compute second derivatives of phi with respect to a, b, and c.
      z1=sinphi/cosphi**3
      z2=1.d0/cosphi
      do 230 i=1,3
      do 230 j=1,3
      dphidada(i,j)=z1*dsinphida(i)*dsinphida(j)+z2*dsinphidada(i,j)
      dphidbdb(i,j)=z1*dsinphidb(i)*dsinphidb(j)+z2*dsinphidbdb(i,j)
      dphidcdc(i,j)=z1*dsinphidc(i)*dsinphidc(j)+z2*dsinphidcdc(i,j)
      dphidadb(i,j)=z1*dsinphida(i)*dsinphidb(j)+z2*dsinphidadb(i,j)
c     note dsinphidadc is non zero but dphidadc is zero - don't compute
      dphidadc(i,j)=z1*dsinphida(i)*dsinphidc(j)+z2*dsinphidadc(i,j)
      dphidbdc(i,j)=z1*dsinphidb(i)*dsinphidc(j)+z2*dsinphidbdc(i,j)
230   continue
      end if
 
c     Add contributions to second derivative matrix
c     Derivative with respect to r1,r1:
      do 21 i=1,3
      do 21 j=1,i
      index=lowtri(i,j)
      d2phidx2(index)=dphidada(i,j)
21    continue
c     Derivative with respect to r2,r2:
      do 22 i=1,3
      do 22 j=1,i
      index=lowtri(i+3,j+3)
      d2phidx2(index)=dphidada(i,j)+dphidbdb(i,j)
     >               -dphidadb(i,j)-dphidadb(j,i)
22    continue
c     Derivative with respect to r3,r3:
      do 23 i=1,3
      do 23 j=1,i
      index=lowtri(i+6,j+6)
      d2phidx2(index)=dphidbdb(i,j)+dphidcdc(i,j)
     >               -dphidbdc(i,j)-dphidbdc(j,i)
23    continue
c     Derivative with respect to r4,r4:
      do 24 i=1,3
      do 24 j=1,i
      index=lowtri(i+9,j+9)
      d2phidx2(index)=dphidcdc(i,j)
24    continue
c     Derivative with respect to r1,r2:
      do 25 i=1,3
      do 25 j=1,3
      index=lowtri(j+3,i)
      d2phidx2(index)=-dphidada(i,j)+dphidadb(i,j)
25    continue
c     Derivative with respect to r1,r3:
      do 26 i=1,3
      do 26 j=1,3
      index=lowtri(j+6,i)
      d2phidx2(index)=-dphidadb(i,j)+dphidadc(i,j)
26    continue
c     Derivative with respect to r1,r4:
      do 27 i=1,3
      do 27 j=1,3
      index=lowtri(j+9,i)
      d2phidx2(index)=-dphidadc(i,j)
c
c     note: dphidadc is always zero - this array and its computation
c     should be removed later
c
c      write(*,887) i,j,index,dphidadc(i,j)
c887   format(' r1,r4 block: i,j=',2i2,' index=',i4,' dphidadc=',f10.4)
27    continue
c     Derivative with respect to r2,r3:
      do 28 i=1,3
      do 28 j=1,3
      index=lowtri(j+6,i+3)
      d2phidx2(index)=
     >               +dphidadb(i,j)-dphidadc(i,j)
     >               -dphidbdb(i,j)+dphidbdc(i,j)
28    continue
c     Derivative with respect to r2,r4:
      do 29 i=1,3
      do 29 j=1,3
      index=lowtri(j+9,i+3)
      d2phidx2(index)=dphidadc(i,j)-dphidbdc(i,j)
29    continue
c     Derivative with respect to r3,r4:
      do 30 i=1,3
      do 30 j=1,3
      index=lowtri(j+9,i+6)
      d2phidx2(index)=dphidbdc(i,j)-dphidcdc(i,j)
30    continue
 
c     code for three atoms colinear
      if(ierror.eq.1) then
        do 45 i=1,78
45      d2phidx2(i)=0.d0
      endif
 
      endif
 
 
      end
c
c     Subroutine to compute the product of the antysimmetric
c     tensor times a vector. The result is an antisymmetric tensor.
      subroutine antsym(x,xtilde)
      implicit real*8 (a-h,o-z)
      dimension x(3),xtilde(3,3)
      xtilde(1,1)=0.d0
      xtilde(2,2)=0.d0
      xtilde(3,3)=0.d0
      xtilde(1,2)=x(3)
      xtilde(2,3)=x(1)
      xtilde(3,1)=x(2)
      xtilde(2,1)=-x(3)
      xtilde(3,2)=-x(1)
      xtilde(1,3)=-x(2)
      return
      end

c
c     subroutine to return random DME torsion angles based on COCC, OCCO and CCOC angles

c

      subroutine getdmetor(th1,th2,th3,iseed)
      implicit real*8 (a-h,o-z)
      external ran
      dimension target(10,1) !dimensioned to allow future different target distributions
      character*2 nm(3)
      dimension prob(27)  !probabliities for each of the 27 cases
      dimension cumul(27) !probabilities for each of the 27 cases
c     dimension icount(27)  !for testing only

      nm(1)='G+'  !+60
      nm(2)='T '  !180
      nm(3)='G-'  !-60
c
c     G+G+G+ corresponds to 000 = 0*9 + 0*3 + 0 = 0
c     G+G+T  corresponds to 001 = 0*9 + 0*3 + 1 = 1
c     G+G+G- corresponds to 002 = 0*9 + 0*3 + 2 = 2
c     G+T G+ corresponds to 010 = 0*9 + 1*3 + 0 = 3
c     G+T T  corresponds to 010 = 0*9 + 1*3 + 1 = 4
c     G+T G- corresponds to 012 = 0*9 + 1*3 + 2 = 5
c     ...
c
c     G-G-G+ corresponds to 220 = 2*9 + 2*3 + 0 = 24
c     G-G-T  corresponds to 221 = 2*9 + 2*3 + 1 = 25
c     G-G-G- corresponds to 222 = 2*9 + 2*3 + 2 = 26
c

      th1=180.d0 !default, all trans
      th2=180.d0 !default, all trans
      th3=180.d0 !default, all trans

c
c     column 1 is the Raman
c 
      target(1,1) = 0.12d0 ! ttt (1) maps to ttt                         (1+111=14)
      target(2,1) = 0.42d0 ! tgt (2) maps to tg+t and tg-t               (1+101=11, 1+(121)=17)
      target(3,1) = 0.04d0 ! ttg (4) maps to ttg+ ttg- g+tt and g-tt     (1+110=13, 1+112=15, 1+011=5, 1+211=23)
      target(4,1) = 0.09d0 ! tgg (4) maps to tg+g+ tg-g- g+g+t g-g-t     (1+100=10, 1+122=18, 1+001=2, 1+221=26)
      target(5,1) = 0.33d0 ! tgg'(4) maps to tg+g- tg-g+ g+g-t g-g+t     (1+102=12, 1+120=16, 1+021=8, 1+201=20)
      target(6,1) = 0.00d0 ! ggg (2) maps to g-g-g- and g+g+g+           (1+222=27, 1+000=1)
      target(7,1) = 0.00d0 ! ggg'(4) maps to g+g+g- g-g-g+ g+g-g- g-g+g+ (1+002=3,  1+220=25, 1+022=9, 1+200=19)
      target(8,1) = 0.00d0 ! gtg (2) maps to g-tg- g+tg+                 (1+212=24, 1+010=4)
      target(9,1) = 0.00d0 ! gtg'(2) maps to g+tg- g-tg+                 (1+012=6,  1+210=22)
      target(10,1)= 0.00d0 ! gg'g(2) maps to g+g-g+ g-g+g-               (1+020=7,  1+202=21)
c                                27  total cases
     

c     write(*,'('' verify mapping function '')')
c     do i=1,27
c       i3=mod((i-1),3)+1 !least sig digit, third torsion
c       i2=mod(((i-1)-(i3-1))/3,3)+1 !middle digit, second torsion
c       i1=((i-1)-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
c       ibin=9*(i1-1) + 3*(i2-1) + i3 
c       write(*,'('' Bin index = '',i2,
c    x   '' angles = '',3i1,2x,3a2,2x,'' bin = '',i2)')
c    x   i,i1,i2,i3,nm(i1),nm(i2),nm(i3),ibin
c     enddo

c
c     divide the probabilitie of the 10 cases among the 27 cases
c
      prob( 1)=target(6,1)/2.d0  !g+g+g+
      prob(27)=target(6,1)/2.d0  !g-g-g-
      
      prob( 2)=target(4,1)/4.d0  !g+g+t
      prob(26)=target(4,1)/4.d0  !g-g-t
      prob(10)=target(4,1)/4.d0  !tg+g+
      prob(18)=target(4,1)/4.d0  !tg-g-
     
      prob( 3)=target(7,1)/4.d0  !g+g+g-
      prob(25)=target(7,1)/4.d0  !g-g-g+
      prob( 9)=target(7,1)/4.d0  !g+g-g-
      prob(19)=target(7,1)/4.d0  !g-g+g+

      prob( 4)=target(8,1)/2.d0  !g+tg+
      prob(24)=target(8,1)/2.d0  !g-tg-

      prob( 5)=target(3,1)/4.d0  !g+tt 
      prob(23)=target(3,1)/4.d0  !g-tt 
      prob(13)=target(3,1)/4.d0  !ttg+ 
      prob(15)=target(3,1)/4.d0  !ttg- 

      prob( 6)=target(9,1)/2.d0  !g+tg- 
      prob(22)=target(9,1)/2.d0  !g-tg+ 

      prob( 7)=target(10,1)/2.d0  !g+g-g+ 
      prob(21)=target(10,1)/2.d0  !g-g+g- 

      prob( 8)=target( 5,1)/4.d0  !g+g-t  
      prob(20)=target( 5,1)/4.d0  !g-g+t  
      prob(16)=target( 5,1)/4.d0  !tg-g+  
      prob(12)=target( 5,1)/4.d0  !tg+g-  

      prob(11)=target( 2,1)/2.d0  !tg+t  
      prob(17)=target( 2,1)/2.d0  !tg-t  

      prob(14)=target( 1,1)       !ttt  
c
c     compute cumulative probability
c
      cumul(1)=prob(1)
      do i=2,27
        cumul(i)=cumul(i-1)+prob(i)
      enddo


c     loop is just for testing
c     do i=1,27
c       icount(i)=0
c     enddo

c     do j=1,10000

      x=ran(iseed)

      is=0
      do i=1,27
        if(x.le.cumul(i)) then
          is=i
          go to 100
        endif
      enddo

100   continue
      if(is.eq.0) then
        write(*,'('' Problem in getdmetor'')')
        stop 'problem in getdmetor'
      endif

      i3=mod((is-1),3)+1 !least sig digit, third torsion
      i2=mod(((is-1)-(i3-1))/3,3)+1 !middle digit, second torsion
      i1=((is-1)-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion

      th1=60.d0+120.d0*(i1-1)
      th2=60.d0+120.d0*(i2-1)
      th3=60.d0+120.d0*(i3-1)


c     ibin=9*(i1-1) + 3*(i2-1) + i3 
c     icount(ibin)=icount(ibin)+1

c     enddo

     
c     call wrtcnt(icount,nm)






      end





C
C     MACHINE INDEPENDENT (ALMOST) RANDOM NUMBER GENERATOR
C     SLOPPILY CONVERTED TO DOUBLE PRECISION.
C     LOW 32 BITS OF FLOATING POINT RANDOM NUMBER ARE ZERO.
C     I.E., THEY ARE ONLY SINGLE PRECISION RANDOM ASSIGNED TO DOUBLES.
C
      DOUBLE PRECISION FUNCTION RAN(IX)
      INTEGER*4 IX,I,I1,I2,I3,I4,I5,I6,I7,I8,I9
      DATA I2/16807/,I4/32768/,I5/65536/,I3/2147483647/
      I6=IX/I5
      I7=(IX-I6*I5)*I2
      I8=I7/I5
      I9=I6*I2+I8
      I1=I9/I4
      IX=(((I7-I8*I5)-I3)+(I9-I1*I4)*I5)+I1
      IF(IX.LT.0) IX=IX+I3
      RAN=(4.656613E-10)*FLOAT(IX)
      RETURN
      END


c
c     write out the counts with grouping to check symmetry
c
      subroutine wrtcnt(icount,nm)
      dimension icount(27)
      character*2 nm(3)
          isum=0
          do ic=1,27
            isum=isum+icount(ic)
          enddo
          do ic=1,27
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
c           write(*,'('' Bin '',i2,'' = ibin '',i2,
c    x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
c    x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
c    x        dfloat(icount(ic))/dfloat(isum)
          enddo

      write(*,'(1x)')
c     write ttt                 
      ic=14
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)

      write(*,'(1x)')
c     write TGT               
      ic=11
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=17
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
            write(*,'('' TGT total       '',13x,
     x      '' count '',i6,'' frac '',f5.3)')
     x        icount(11)+icount(17),
     x        dfloat(icount(11)+icount(17))/dfloat(isum)

      write(*,'(1x)')
c     write TTG                  
      ic=13
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=15
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic= 5
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=23
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
            write(*,'('' TTG total       '',13x,
     x      '' count '',i6,'' frac '',f5.3)')
     x        icount(13)+icount(15)+
     x        icount( 5)+icount(23),
     x        dfloat(icount(13)+icount(15)+
     x        icount( 5)+icount(23))/dfloat(isum)



      write(*,'(1x)')
c     write TGG      (=TG+G+,TG-G-,G+G+T,G-G-T)            
      ic=10
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=18
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=2 
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=26
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
            write(*,'('' TGG total       '',13x,
     x      '' count '',i6,'' frac '',f5.3)')
     x        icount(10)+icount(18)+
     x        icount( 2)+icount(26),
     x        dfloat(icount(10)+icount(18)+
     x        icount( 2)+icount(26))/dfloat(isum)

      
      write(*,'(1x)')
c     write TGG'     (=TG+G-,TG-G+,G+G-T,G-G+T)            
      ic=12
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=16
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic= 8
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=20
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
            write(*,'('' TGG~ total      '',13x,
     x      '' count '',i6,'' frac '',f5.3)')
     x        icount(12)+icount(16)+
     x        icount( 8)+icount(20),
     x        dfloat(icount(12)+icount(16)+
     x        icount( 8)+icount(20))/dfloat(isum)


      write(*,'(1x)')
c     write GGG            
      ic=1 
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=27
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
            write(*,'('' GGG  total      '',13x,
     x      '' count '',i6,'' frac '',f5.3)')
     x        icount(1 )+icount(27),
     x        dfloat(icount(1 )+icount(27)
     x        )/dfloat(isum)


      write(*,'(1x)')
c     write GGG'           
      ic=3 
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=25
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=19
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic= 9
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
            write(*,'('' GGG~ total      '',13x,
     x      '' count '',i6,'' frac '',f5.3)')
     x        icount( 3)+icount(25)+
     x        icount(19)+icount( 9),
     x        dfloat(icount( 3)+icount(25)+
     x        icount(19)+icount( 9))/dfloat(isum)


      write(*,'(1x)')
c     write GG'G           
      ic= 7
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=21
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
            write(*,'('' GG~G total      '',13x,
     x      '' count '',i6,'' frac '',f5.3)')
     x        icount( 7)+icount(21),
     x        dfloat(icount( 7)+icount(21)
     x        )/dfloat(isum)

      write(*,'(1x)')
c     write GTG           
      ic= 4
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=24
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
            write(*,'('' GTG  total      '',13x,
     x      '' count '',i6,'' frac '',f5.3)')
     x        icount( 4)+icount(24),
     x        dfloat(icount( 4)+icount(24)
     x        )/dfloat(isum)

      write(*,'(1x)')
c     write GTG'          
      ic= 6
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
      ic=22
            i3=mod(ic-1,3) + 1  !least sig digit; third torsion
            i2=mod((ic-1-(i3-1))/3,3) + 1 !middle digit, second torsion
            i1=(ic-1-(i3-1)-3*(i2-1))/9 + 1 !first digit, first torsion
            ibin=9*(i1-1) + 3*(i2-1) + i3 
            write(*,'('' Bin '',i2,'' = ibin '',i2,
     x      2x,3i1,2x,3a2,'' count '',i6,'' frac '',f5.3)')
     x        ic,ibin,i1,i2,i3,nm(i1),nm(i2),nm(i3),icount(ic),
     x        dfloat(icount(ic))/dfloat(isum)
            write(*,'('' GTG~ total      '',13x,
     x      '' count '',i6,'' frac '',f5.3)')
     x        icount( 6)+icount(22),
     x        dfloat(icount( 6)+icount(22)
     x        )/dfloat(isum)


      end

c
c     subroutine to read a zmatix file
c     of the following form
cu_1
cu_2 Du_1  1.00
cu_3 Du_2  1.00   Du_1 90.0
c_1  Du_3  1.000  Du_2 90.0  Du_1 180.0
c_2  N_1   1.381  Du_3 90.0  Du_2 180.0
c

      subroutine getzmat(fname,zmonomer,izmonomer,msites,
     x nzatmax,natmonomer,ndummy,knbnds,nbndmax,nbnds)
      implicit real*8 (a-h,o-z)
      character*120 line,line2,fname
      character*(*) msites(nzatmax)
      dimension zmonomer(nzatmax,3),izmonomer(nzatmax,3)
      dimension knbnds(nbndmax,3)

      nline=0
      open(20,file=fname)
10    continue
      read(20,'(a120)',end=100) line
      if(line(1:10).eq.'          ') go to 100
      if(line(1:1).eq.'%') go to 10
      nline=nline+1
      if(nline.gt.nzatmax) then
        write(*,'(''Error:  exceeded memory in getzmat'')')
        stop 'increase array sizes in getzmat'
      endif
      zmonomer(nline,1)=0.d0  !r
      zmonomer(nline,2)=0.d0  !theta
      zmonomer(nline,3)=0.d0  !phi

c     write(*,'(/,a)') ' Input line:  *'//line(1:60)//'* '
      call leftjust(line)
      i1=index(line,' ')  ! token 1 from 1:i1-1
      msites(nline)=line(1:i1-1)

      if(nline.gt.1) then
        call leftjust(line(i1+1:))
        i2=index(line(i1+1:),' ')+i1 ! token 2 from i1+1:i2-1
        call leftjust(line(i2+1:))
        i3=index(line(i2+1:),' ')+i2 ! token 3 from i2+1:i3-1
        read(line(i2+1:i3-1),*) d1
c       write(*,'('' Token 1 *'',a,''* Token 2 *'',a,''*'',
c    x    '' Dist '',f10.5)')
c    x    line(1:i1-1),line(i1+1:i2-1),d1
        zmonomer(nline,1)=d1
      endif

      if(nline.gt.2) then
        call leftjust(line(i3+1:))
        i4=index(line(i3+1:),' ')+i3 ! token 4 i3+1:i4-1
        call leftjust(line(i4+1:))
        i5=index(line(i4+1:),' ')+i4 ! token 5 i4+1:i5-1
        read(line(i4+1:i5-1),*) a1
c       write(*,'('' Token 4 *'',a,''*  Angl '',f10.5)')
c    x    line(i3+1:i4-1),a1
        zmonomer(nline,2)=a1
      endif

      if(nline.gt.3) then
        call leftjust(line(i5+1:))
        i6=index(line(i5+1:),' ')+i5 ! token 6 i5+1:i6-1
        call leftjust(line(i6+1:))
        i7=index(line(i6+1:),' ')+i6 ! token 7 is6+1:i7-1
        read(line(i6+1:i7-1),*) t1
c       write(*,'('' Token 6 *'',a,''*  Dihe '',f10.5)')
c    x    line(i5+1:i6-1),t1
        zmonomer(nline,3)=t1
      endif

      izmonomer(nline,1)=0
      izmonomer(nline,2)=0
      izmonomer(nline,3)=0
      do i=1,nline-1
        is=index(msites(i),' ')-1
        if(line(i1+1:i2-1).eq.msites(i)(1:is)) izmonomer(nline,1)=i
        if(line(i3+1:i4-1).eq.msites(i)(1:is)) izmonomer(nline,2)=i
        if(line(i5+1:i6-1).eq.msites(i)(1:is)) izmonomer(nline,3)=i
      enddo


      go to 10




100   continue


      natmonomer=nline

      ndummy=0
      do i=1,nline
        line2=msites(i)(1:2)
        call ucase(line2(1:2))
        if(line2(1:2).eq.'DU') ndummy=ndummy+1
        if(i.eq.1) then
          write(*,'('' *'',a,''*     '')') msites(i)(1:5)
        elseif(i.eq.2) then
          write(*,'('' *'',a,''*     '',
     x        '' *'',a,''* '',f10.5)')
     x    msites(i)(1:5),msites(izmonomer(i,1))(1:5),zmonomer(i,1)
        elseif(i.eq.3) then
          write(*,'('' *'',a,''*     '',
     x        '' *'',a,''* '',f10.5,
     x        '' *'',a,''* '',f10.5)')
     x    msites(i)(1:5),msites(izmonomer(i,1))(1:5),zmonomer(i,1),
     x                   msites(izmonomer(i,2))(1:5),zmonomer(i,2) 
        elseif(i.gt.3) then
          write(*,'('' *'',a,''*     '',
     x        '' *'',a,''* '',f10.5,
     x        '' *'',a,''* '',f10.5,
     x        '' *'',a,''* '',f10.5)')
     x    msites(i)(1:5),msites(izmonomer(i,1))(1:5),zmonomer(i,1),
     x                   msites(izmonomer(i,2))(1:5),zmonomer(i,2),
     x                   msites(izmonomer(i,3))(1:5),zmonomer(i,3)
        endif
      enddo

c
c     read the bond list 
c

      nbond=0
110   continue
      read(20,'(a120)',end=200) line
      call leftjust(line)
      call ucase(line)
      if(   line(1:7).ne.'SINGLE '.and.line(1:7).ne.'DOUBLE '
     x .and.line(1:7).ne.'TRIPLE '.and.line(1:8).ne.'AROMATIC') 
     x  go to 110
      nbond=nbond+1
      if(nbond.gt.nbndmax) then
        write(*,'(''Error:  exceeded bond memory in getzmat'')')
        stop 'increase bond array sizes in getzmat'
      endif
      i1=index(line,' ')  !first blank
      call leftjust(line(i1+1:))
      i2=index(line(i1+1:),'-')+i1  !hyphen      
      i3=index(line(i2+1:),' ')+i2  !next blank  
      write(*,'(/,'' Bond line: '',a30)') line(1:30)
      write(*,'('' Type: '',a6,'' First '',a,'' Second '',a)')
     x  line(1:i1-1),line(i1+1:i2-1),line(i2+1:i3-1)

      do i=1,nline
        is=index(msites(i),' ')-1
        line2=msites(i)(1:is)
        call ucase(line2(1:is))
        if(line(i1+1:i2-1).eq.line2(1:is)) knbnds(nbond,1)=i
        if(line(i2+1:i3-1).eq.line2(1:is)) knbnds(nbond,2)=i
      enddo
      if(line(1:i1-1).eq.'SINGLE') knbnds(nbond,3)=1
      if(line(1:i1-1).eq.'DOUBLE') knbnds(nbond,3)=2
      if(line(1:i1-1).eq.'TRIPLE') knbnds(nbond,3)=3
      if(line(1:i1-1).eq.'AROMATIC') knbnds(nbond,3)=4

      go to 110

200   continue

      close(20) 


      do i=1,nbond
        write(*,'('' Bond '',i3,2x,a,2x,a,'' type '',i1)')
     x    i,msites(knbnds(i,1)),msites(knbnds(i,2)),knbnds(i,3)
      enddo

      nbnds=nbond




      end

c
c     integer function to report if a string can be read as an integer
c
      integer function ifint(string)
      character*(*) string
      character*13 digits   !include +/-, space, and all 10 digits
      digits='0123456789-+ '
      ifint=1                                        !1=a valid integer
      do i=1,len(string)
        if(index(digits,string(i:i)).eq.0) ifint=0   !0=not an integer
      enddo
      return 
      end





c
c     subroutine to get monomer zmatrix and other info from a library
c
c     The library is a directory with a set of files, one for each type of repeat unit
c     
c     if this routine has been called before, it stores up to
c     10 of the previously used repeat units in arrays
c     so that files don't have to be opened over and over
c
c     note that iseed used to be passed to allow randomness in the repeat units
c     e.g., for torsion angles or for sidechain placement (polystyrene)
c
      subroutine getmonomerlib(line,libpath,zmonomer,izmonomer,
     x                         msites,nzatmax,
     x                         natmonomer,ndummy,lstbnd,mname)

      implicit real*8 (a-h,o-z)
      character*(*) line
      character*(*) libpath   !path to the directory with the files
      character*(*) mname       !short name of monomer repeat unit (e.g., DME)
      character*1024 mnameprev  !short name of monomer repeat unit (e.g., DME)

      dimension zmonomer(nzatmax,3),izmonomer(nzatmax,3)
      character*(*) msites(nzatmax)
      dimension zmonomersave(200,3),izmonomersave(200,3)
      character*1024 msitessave(200)

      dimension knbnds(100,3)        !local array for bond list
      dimension knbndssave(100,3)    !local array for bond list
      dimension lstbnd(nzatmax,2,6)  !for each site, up to 6 things its bonded to

      character*1024 string
      logical ifexist

      save

      data ifcalled/0/

      nbndmax=100    !size of local array knbnds
      natmmax=200    !size of local array izmonomersave, zmonomersave, msitessave

      do i=1,nzatmax
        do j=1,6
          lstbnd(i,1,j)=0
          lstbnd(i,2,j)=0
        enddo
        do j=1,3
          izmonomer(i,j)=0
          zmonomer(i,j)=0.d0
        enddo
      enddo

c     first token in line tells what monomer
      string=line
      call leftjust(string)
      is=index(string,' ')-1
      call ucase(string(1:is))
      mname=string(1:is)


      if(ifcalled.ge.1) then
c       check if the previous call used the same repeat unit name; if so, don't reopen
        is=index(mname,' ')-1
        is1=index(mnameprev,' ')-1
        if(mname(1:is).ne.mnameprev(1:is1)) go to 500  
        write(*,'(''Using previously read zmatrix file: '',a)') 
     x    mname(1:is)
        natmonomer=natmonomersave
        ndummy=ndummysave
        do i=1,natmonomer
          msites(i)=msitessave(i)
          do j=1,3
            izmonomer(i,j)=izmonomersave(i,j)
            zmonomer(i,j)=zmonomersave(i,j)
          enddo
        enddo
        nbonds=nbondssave
        do i=1,nbonds
          do j=1,3
            knbnds(i,j)=knbndssave(i,j)
          enddo
        enddo
        go to 1000
      endif
500   ifcalled=1
      mnameprev=mname



c     retrieve a new repeat unit

      is=index(mname,' ')-1
      write(*,'(''Reading new zmatrix file: '',a)') mname(1:is)
      is1=index(libpath,' ')-1
      inquire(file=libpath(1:is1)//mname(1:is)//'.lib',exist=ifexist)
      if(.not.ifexist) then
        write(*,'(''Error:  missing zmatrix file'')')
        write(*,'(''Looking for file '',a)')
     x    libpath(1:is1)//mname(1:is)//'.lib'
        stop 'missing zmatrix file'
      endif

      open(10,file=libpath(1:is1)//mname(1:is)//'.lib')     
      read(10,'(a)') string 
      is2=index(string,' ')-1
      if(string(1:is2).ne.mname(1:is)) then
c       the first characters of this line should match the filename
        write(*,'(''Error:  Mismatch File '',a,'' Line 1: '',a)')
     x  libpath(1:is1)//mname(1:is)//'.lib',string(1:30)
        write(*,'(''The file name must match the name of the repeat '',
     x    ''unit shown on the first line of the fime.'')')
        stop 'Error:  incorrect library file format' 
      endif
c     write(*,'(''Information from file '',a)') mname(1:is)
      read(10,*) natmonomer   !number of sites in this repeat unit
      read(10,*) ndummy     
      if(natmonomer.gt.natmmax) then
        write(*,'(''Not enough memory in local arrays; '',
     x    ''increase natmmax'')')
        stop 'not enough memory in getmonomerlib'
      endif

      do i=1,natmonomer
        read(10,'(a1024)') string
c       first string is a number, then the name, then three integer/real pairs
        is1=0
10      is1=is1+1
        if(string(is1:is1).eq.' ') go to 10
        is2=index(string(is1:),' ')-1+is1-1   !number field is string(is1:is2)
        read(string(is1:is2),*) j
        if(j.ne.i) then
          write(*,'(''Error:  Mismatch in number fields '',2i5)') 
     x      i,j
          stop 'Error in number fields of lib file'
        endif
        is3=is2
20      is3=is3+1
        if(string(is3:is3).eq.' ') go to 20
        is4=index(string(is3:),' ')-1+is3-1   !name field is string(is3:is4)
        msites(i)=string(is3:is4)
c       write(*,'(''Name   field '',a)') string(is3:is4)
        read(string(is4+1:),*) (izmonomer(i,j),zmonomer(i,j),j=1,3)
        isi=index(msites(izmonomer(i,1)),' ')-1
        isj=index(msites(izmonomer(i,2)),' ')-1
        isk=index(msites(izmonomer(i,3)),' ')-1
        string=' '
        write(string,'(''Z-matrix: '',i5,5x,a,a,
     x    i5,2x,6x,1x,f10.3,
     x    i5,2x,6x,1x,f10.3,
     x    i5,2x,6x,1x,f10.3)') i,
     x    msites(i)(1:is4-is3+1),
     x    '                    '(1:10-is4+is3-1),
     x    (izmonomer(i,j),zmonomer(i,j),j=1,3)

        if(izmonomer(i,1).ne.0) 
     x    string(38:43)=msites(izmonomer(i,1))(1:isi)
        if(izmonomer(i,2).ne.0) 
     x    string(62:67)=msites(izmonomer(i,2))(1:isj)
        if(izmonomer(i,3).ne.0) 
     x    string(86:91)=msites(izmonomer(i,3))(1:isk)
        write(*,'(a)') string(1:114)
      enddo

c     read the bonds
      read(10,*) nbonds
      if(nbonds.gt.nbndmax) then
        write(*,'(''Not enough memory in local arrays; '',
     x    ''increase nbndmax'')')
        stop 'not enough memory in getmonomerlib'
      endif
      do i=1,nbonds
        read(10,*) i2,(knbnds(i,j),j=1,3)                  
c       write(*,'(4i5)') i2,(knbnds(i,j),j=1,3)
        iat=knbnds(i,1)
        jat=knbnds(i,2)
        isi=index(msites(iat),' ')-1
        isj=index(msites(jat),' ')-1
        string=' '
        write(string,'(''Bonds   : '',i5,5x,
     x    i5,2x,6x,1x, i5,2x,6x,1x, ''Type '',i1)')
     x    i,iat,jat,knbnds(i,3)
        string(28:33)=msites(iat)(1:isi)
        string(42:47)=msites(jat)(1:isj)
        write(*,'(a)') string(1:54)
      enddo

c     save everything in case needed later
      natmonomersave=natmonomer
      ndummysave=ndummy
      do i=1,natmonomer
        msitessave(i)=msites(i)    
        do j=1,3
          izmonomersave(i,j)=izmonomer(i,j)
          zmonomersave(i,j)=zmonomer(i,j)
        enddo
      enddo
      nbondssave=nbonds
      do i=1,nbonds
        do j=1,3
          knbndssave(i,j)=knbnds(i,j)
        enddo
      enddo

      close(10)





1000  continue

      do i=1,nbonds
        iat=knbnds(i,1)
        jat=knbnds(i,2)
        n=lstbnd(iat,1,1)+1
        lstbnd(iat,1,1)=n
        lstbnd(iat,1,n+1)=jat
        lstbnd(iat,2,n+1)=knbnds(i,3)
        n=lstbnd(jat,1,1)+1
        lstbnd(jat,1,1)=n
        lstbnd(jat,1,n+1)=iat
        lstbnd(jat,2,n+1)=knbnds(i,3)
      enddo





      end
