c
c     program to develop a complete force field description
c     for a polymer built with the bldstrand program
c
c     input should include a topology file from bldstrand
c     -this includes a list of the polymer repeat units that were used
c     -indication whether this is to be a united or all atom description
c       (if united atom, the hydrogen information in the topology file is ignored)
c     -association of a force field description file for each repeat unit
c       (includes a mapping file: what atom in the topology file correseponds to
c       what atom in the force field file)
c     -input describing what to do for the link terms (new bonds, angles, torions)
c       arising from the connection points
c
c     1/23/2011 to read a command line argument that conveys all file names to use
c
c
      implicit real*8 (a-h,o-z)
      character*120 line,topfile,fileset,molname
      character*10 mtypes(20)  !list of repeat unit types
      character*120 mfiles(20) !force field file for each type of repeat unit
      character*20 map(2,100)  !maps names in topology file to names in force field file
      character*40 element,string,string1
      logical ifexist

      character*120 mdescr(1500) !one line per monomer unit in the polymer
      dimension  mindex(1500)    !one line per monomer unit in the polymer
      dimension  istrand(1500)

      character*20 atname(20000),iname,jname,kname,lname,elfield
      dimension itype(20000),charge(20000),vdw(2,20000),numhyd(20000)

      dimension ijbond(5,55000),bondpar(2,55000)  !holds indices, chosen flag, parms for bond list
      dimension ijkangle(5,70000),anglepar(2,70000)  !holds indices, chosen flag, parms for angle list
      dimension ijkldih(6,60000),dihpar(8,60000)  !holds indices, chosen flag, parms for torsion list

      dimension chgmonomer(1500)  !holds net charge on each monomer

c     for lammps input, a set of auxilliary data structures to hold force field information by type
c     in lammps an atom type has a particular mass and a particular set of lj parameters
c     but different atoms of the same atom type can have different charges
c     we divide potentially more finely into unique types within each monomer
      dimension lmatype(3,500)   !for each obs. atom type: map row index; number observed, first instance
      dimension lmbtype(4,500)   !for each obs. bond type: number observed, first instance, origin data
      dimension lmantype(4,500)   !for each obs. angle type: number observed, first instance, origin data
      dimension lmdtype(4,1000)  !for each obs. dihedral type: number observed, first instance, origin data

      nattpmx=500  !max number of atom types (dimension of lmatype) for lammps input
      nbntpmx=500  !max number of bond types (dimension of lmbtype) for lammps input
      nantpmx=500  !max number of angle types (dimension of lmantype) for lammps input
      nditpmx=1000 !max number of dihedral types (dimension of lmdtype) for lammps input


      nmonmx=1500 
      natmmx=20000
      nbndmx=55000
      nangmx=70000
      ndihmx=60000  !dimension of the dihedral lists

      write(*,'(/,''Program to assign force field parameters '',
     x  '' (Version '',a10,'')'')') '2011-04-15'

      call getarg(1,fileset)
      isfs=index(fileset,' ')-1
      write(*,'(//,''File name set from command line argument: '',
     x   a)') fileset(1:isfs)


      do i=1,nmonmx
        chgmonomer(i)=0.d0
      enddo
      do i=1,natmmx
        itype(i)=0
      enddo


      open(10,file=fileset(1:isfs)//'.fin')
      write(*,'(''Input from file: '',a)') fileset(1:isfs)//'.fin'
      read(10,'(a120)') topfile  !file name for the topology file
      is=index(topfile,' ')-1
      write(*,'(''Topology file:   '',a)') topfile(1:is)
      
c     beginning of topology file lists the names of the repeat units used
c     need to read these in, find the number of unique ones, and make sure
c     there is a force field file for each

      open(13,file=topfile(1:is))
c
c     get the molecule name, assuming this is before the "Repeat unit linkage" block
c 
2     continue
      read(13,'(a120)') line
      if(index(line,'Molecule name').eq.0) go to 2  
      i1=index(line,'Molecule name')+14
      molname=line(i1:)
      call lstchr(molname,i1)
      write(*,'(/,''Molecule name *'',a,''*'')') molname(1:i1)
      
c
c     skip to "Repeat unit linkage" block
      write(*,'(/,''Repeat unit linkage information'',
     x    '' from topology file'')')
      nmonomer=0
5     continue
      read(13,'(a120)') line
      if(index(line,'Repeat unit linkage').ne.0) go to 10 
      go to 5
10    continue
      read(13,'(a120)',end=20) line
      if(line(1:10).eq.'          ') go to 20
      call leftjust(line)
      call lstchr(line,il)
      write(*,'(a)') line(1:il)
      if(nmonomer+1.gt.nmonmx) then
        write(*,'('' Exceeded size of repeat unit arrays '')')
        write(*,'('' Current array size is '',i6)') nmonmx     
        stop 'exceeded size of repeat unit arrays'
      endif
      nmonomer=nmonomer+1

c
      read(line,*) mindex(nmonomer)
      is=index(line,' ')
      call leftjust(line(is:))
      mdescr(nmonomer)=line(is:)
      go to 10
20    continue
      write(*,'(''Total number of repeat units: '',i3)') nmonomer

c
c     would like to know if this monomeric unit is connected to the base
c     if it is, it is the beginning of a new strand
c     we assume the base is the first monomer, it has a monomer id of zero
c     we assume a monomer is connected to the base if (1) it is the second monomer
c     or (2) it has a dummy site that refers to the base, with a monomoer id of zero
c
c
      istrnd=0
      do i=1,nmonomer
        imonid=mindex(i) !the monomer index of this monomer
        if(imonid.eq.0) then   !monomer index 0 corresponds to base 
          istrand(i)=0
        else
c         following code interprets Du_n=AB_xx_yy or Du_n=AB_xx from input to bldstrand
c         for n=1,2,3.  Extracts yy if notation indicates it is joined to repeat unit yy
c         and if yy is missing, assumes it is joined to immediate previous repeat unit
c         previous repeat unit is i-1
c         [I think the only thing that matters is the repeat unit that serves as Du_3,
c         since this provides the link site to the previous repeat unit, right?]
          line=mdescr(i)
          call ucase(line)
          is=index(line,'DU_1=')+5
          ie=index(line(is:),' ')+is-2   !token is in line(is:ie)
          iu1=index(line(is:ie),'_')+is-1 !first underscore is at line(iu1:iu1)
          iu2=index(line(iu1+1:ie),'_')
c         if iu2=0 then the previous monomeric unit is where Du_1 is
          if(iu2.eq.0) then
            idu1=mindex(i-1)
          else
            read(line(iu1+1+iu2:ie),*) idu1
          endif
          is=index(line,'DU_2=')+5
          ie=index(line(is:),' ')+is-2   !token is in line(is:ie)
          iu1=index(line(is:ie),'_')+is-1 !first underscore is at line(iu1:iu1)
          iu2=index(line(iu1+1:ie),'_')
c         if iu2=0 then the previous monomeric unit is where Du_2 is
          if(iu2.eq.0) then
            idu2=mindex(i-1)
          else
            read(line(iu1+1+iu2:ie),*) idu2
          endif
          is=index(line,'DU_3=')+5
          ie=index(line(is:),' ')+is-2   !token is in line(is:ie)
          iu1=index(line(is:ie),'_')+is-1 !first underscore is at line(iu1:iu1)
          iu2=index(line(iu1+1:ie),'_')
c         if iu2=0 then the previous monomeric unit is where Du_3 is
          if(iu2.eq.0) then
            idu3=mindex(i-1)
          else
            read(line(iu1+1+iu2:ie),*) idu3
          endif

c         write(*,'('' Dummy monomeric units: '',3i5)') idu1,idu2,idu3
c         idu1=min(idu1,idu2,idu3)  
          idu1=              idu3   !changed from above to respect repeat unit of Du_3  
          if(idu1.eq.0) istrnd=istrnd+1
          istrand(i)=istrnd
c         write(*,'('' Monomeric unit= '',i3,'' mindex= '',i3,
c    x      '' strand = '',i3,
c    x      '' descr: '',a)') i,mindex(i),istrand(i),mdescr(i)(1:40)
        endif
      enddo

c
      write(*,'(//,'' Strand Identification '',/)')
      do i=1,nmonomer
        call lstchr(mdescr(i),il)
        write(*,'('' Monomeric unit= '',i3,'' mindex= '',i3,
     x    '' Strand = '',i3,
     x    '' Descr: '',a)') i,mindex(i),istrand(i),mdescr(i)(1:il)
      enddo





c     develop list of distinct repeat unit types
      nmtypes=0  !repeat unit type counter
      do i=1,nmonomer
        is=index(mdescr(i),' ')-1
        k=0
        do j=1,nmtypes
          isj=index(mtypes(j),' ')-1          
          if(mdescr(i)(1:is).eq.mtypes(j)(1:isj)) k=j
        enddo
        if(k.eq.0) then
          nmtypes=nmtypes+1
          mtypes(nmtypes)=mdescr(i)(1:is)
        endif
      enddo


c     write(*,'(/,'' Distinct repeat unit types'')')
      do i=1,nmtypes
        call lstchr(mtypes(i),il)
        is=index(mtypes(i),' ')-1
        write(*,'(5x,i2,'' *'',a,''*'')')
     x    i,mtypes(i)(1:il)
      enddo

c
c     NOTE need to track the site to which each hydrogen is attached to make
c     unique association with force field site names
c
c     assume the next lines in the input file give names of force field files
c     for each repeat unit

      longname=0
      do i=1,nmtypes
        mfiles(i)=' '
      enddo
30    continue
      read(10,'(a120)',end=40) line  !read from ffassign.in
      if(line(1:10).eq.'          ') go to 40  !this section delimited with blank line
      call leftjust(line)
      is=index(line,' ')-1
      longname=max(longname,is)
      imatch=0
      do i=1,nmtypes
        ism=index(mtypes(i),' ')-1
        if(line(1:is).eq.mtypes(i)(1:ism)) then
          imatch=i
          go to 35
        endif
      enddo
      go to 30  !get to here if no match found with the repeat units we need
35    continue
      call leftjust(line(is+2:))
      mfiles(imatch)=line(is+2:) 
      go to 30

40    continue
     

c     after reading files for each monomer type, read the link files
      nlink=0
42    continue
      read(10,'(a120)',end=48) line  !read from ffassign.in
      if(line(1:10).eq.'          ') go to 48  !this section delimited with blank line
      call leftjust(line)
      write(*,'('' Link line: '',a)') line(1:30)
      is=index(line,' ')-1
      longname=max(longname,is)
      if(line(1:is).eq.'LINK') then
        call leftjust(line(is+2:))
        nlink=nlink+1
        mfiles(nmtypes+nlink)=line(is+2:) 
        mtypes(nmtypes+nlink)=line(1:is)
      endif
      go to 42

48    continue



      write(*,'(/,''Distinct repeat unit types'',
     x  '' and their force field files'')')
      do i=1,nmtypes+nlink
        is=index(mtypes(i),' ')-1
        if(mfiles(i)(1:10).eq.'          ') then
          write(*,'('' Missing force field file for repeat unit '',a)')
     x      mtypes(i)(1:longname)
        else
          write(*,'(5x,i2,'' index '',i2,'' *'',a,''*'',
     x      '' with parameters in '',a50)')
     x      i,mindex(i),mtypes(i)(1:longname),mfiles(i)(1:50)
        endif
      enddo

c
c     read in the mapping functions; for each type of site in the topology
c     file, get the corresponding name in the force field files
c
      nmap=0
50    continue
      read(10,'(a120)',end=60) line
      if(line(1:10).eq.'          ') go to 60  !this section delimited with blank line
      call leftjust(line)
      is=index(line,' ')-1
      ie=index(line,'=')-1
      nmap=nmap+1
      map(1,nmap)=line(1:min(is,ie))
      call leftjust(line(ie+2:))
      is=index(line(ie+2:),' ')-1 
      map(2,nmap)=line(ie+2:ie+1+is)
      go to 50
60    continue

      write(*,'(/,''Mapping information (topology to force field)'')')
      do i=1,nmap
        write(*,'('' Topology name: '',a,'' = FF name: '',a)') 
     x    map(1,i),map(2,i)
      enddo
c

c
c     read in detailed site information from topology file
c
c     skip to line that says Detailed site information
81    continue
      read(13,'(a120)') line
      if(index(line,'Detailed site information').eq.0) go to 81
      nsites=0
100   continue
      read(13,'(a120)',end=110) line
      if(line(1:10).eq.'          ') go to 110
      if(nsites+1.gt.natmmx) then
        write(*,'(''Error:  Exceeded size of atomic storage '',
     x    '' arrays.  Current limit is '',i7)') natmmx
        stop 'exceeded size of atomic storage arrays'
      endif
      nsites=nsites+1
      read(line,'(12x,a)') atname(nsites)
      go to 100



110   continue
      write(*,'(/,''Detailed site information from topology file '')')
      do i=1,nsites
        numhyd(i)=0
      enddo
      do i=1,nsites
        call parser(atname(i),element,lel,i1,i2,i3)
        is=index(mdescr(i3+1),' ')-1    !mdescr(i3+1)(1:is) holds repeat unit type string
        isa=index(atname(i),'_')        !atname(1:isa-1) holds element
        write(line(1:10),'(i10)') i2    
        call leftjust(line(1:10))
        isb=index(line,' ')             !line(1:isb-1) holds index within the repeat unit
        if(element(1:lel).eq.'H') then
c         if this is a hydrogen, find what it is connected to 
c         by matching sequence within monomer (i2) and monomer index (i3)
          do j=1,nsites
            call parser(atname(j),string,lstr,j1,j2,j3)
            if(j2.eq.i2.and.j3.eq.i3) then
              iconn=j
              go to 115
            endif
          enddo
115       continue
          numhyd(iconn)=numhyd(iconn)+1
          write(line(11:20),'(i10)') numhyd(iconn)
          call leftjust(line(11:20))
          isc=index(line(11:),' ')-1
          string1= mdescr(i3+1)(1:is)//'_'                            ! H_
     x         //atname(i)(1:isa)//string(1:lstr)//'_'//line(1:isb-1) ! C_3
     x         //'_'//line(11:10+isc)                                 ! _1
          call lstchr(string1,il1)
          k=0
          do j=1,nmap
            ism=index(map(1,j),' ')-1
            if(map(1,j)(1:ism).eq.string1(1:il1)) then
              k=j
              go to 120
            endif
          enddo
120       continue
          if(k.eq.0) then
            write(*,'('' Error:  type from topology file'',
     x        '' not found in map, No match for: '',a)')
     x        '*'//string1(1:il1)//'*'
            stop 'Error:  type not found in map'
          endif
          write(*,'(i5,2x,a,2x,
     x      a,'' Maps to '',a)') 
     x      i,atname(i),string1(1:il1),map(2,k)
        else        !here if the site is NOT a hydrogen
          string1= mdescr(i3+1)(1:is)//'_'//atname(i)(1:isa)          ! DMC_C
     x      //line(1:isb-1)                                             !    _3
          call lstchr(string1,il1)
          k=0
          do j=1,nmap
            ism=index(map(1,j),' ')-1
            if(map(1,j)(1:ism).eq.string1(1:il1)) then
              k=j
              go to 125
            endif
          enddo
125       continue
          if(k.eq.0) then
            write(*,'('' Error:  type from topology file'',
     x        '' not found in map, No match for: '',a)')
     x        '*'//string1(1:il1)//'*'
            stop 'Error:  type not found in map'
          endif
          write(*,'(i5,2x,a,2x,
     x      a,'' Maps to '',a)') 
     x      i,atname(i),string1(1:il1),map(2,k)
        endif
c       write(*,'('' Map element '',i3,''*'',a,''* = *'',a,''*'')')
c    x     k,map(1,k),map(2,k)
        if(itype(i).ne.0) then
          write(*,'('' Error:  a site matches multiple entries'',
     x      '' in the map array. '',/,
     x      '' offending site index: '',i10,'' name: '',a,/,
     x      '' matches map entries '',i6,'' and '',i6)')
     x      i,atname(i),itype(i),k
          stop 'Error in the map array'
        else
          itype(i)=k
        endif
      enddo



c     identify unique types and count occurences
      nattp=0  !counts number of unique atom types (for lammps)
      do i=1,nmap
        ic=0
c       ifirst=0 ! done when needed in wrtlmp
        do j=1,nsites
          if(itype(j).eq.i) ic=ic+1
c         if(itype(j).eq.i.and.ifirst.eq.0) ifirst=j ! done when needed in wrtlmp
        enddo
        if(ic.gt.0) then
          nattp=nattp+1
          if(nattp.gt.nattpmx) then
            Write(*,'('' Error - number of types exceeds storage.'',
     x        /,'' Increase size of lmatype and nattpmx '')')
            stop 'Increase size of atom type arrays'
          endif
          lmatype(1,nattp)=i
          lmatype(2,nattp)=ic
c         lmatype(3,nattp)=ifirst ! done when needed in wrtlmp
        endif
      enddo


      write(*,'(/,'' Occurences of the various atom types '')')
      n=0
      do i=1,nattp
        j=lmatype(1,i)
        ism=index(map(1,j),' ')-1
        write(*,'('' Atom type '',i3,'' Number observed = '',i4,
     x    '' Map('',i3,'')='',a
c    x    ,'' First observed '',i6
     x    )')
     x    i,lmatype(2,i),j,map(1,j)(1:ism)
c    x    ,lmatype(3,i)
        n=n+lmatype(2,i)
      enddo
        write(*,'('' Total observed'',3x,''               '',i4)')
     x  n
      write(*,'(/)')




      call getchgvdw(mfiles,mtypes,nmtypes,map,nmap,
     x      itype,charge,vdw,nsites)

c
c     write out the charges and LJ parameters
c
      write(*,'(/,''Charge and Lennard-Jones parameters'')')
      do j=1,nsites
        write(*,'(i5,2x,a,2x,3f10.5)') j,atname(j),charge(j),
     x    vdw(1,j),vdw(2,j)
      enddo

c
c     this code allows "editing" of the charges to support
c     problems with chain linkages and termination that could
c     cause the molecule (and monomeric units) to have a net
c     charge.  this replaces specific charges.
c
      inquire(file=fileset(1:isfs)//'.edit',exist=ifexist)
      if(ifexist) then
        write(*,'(/,'' Using file '',a,'' to edit charges'')')
     x    fileset(1:isfs)//'.edit'
        open(16,file=fileset(1:isfs)//'.edit')
155     continue
c       read(16,*,end=160) j,c
        read(16,'(a120)',end=160) line
        if(line(1:1).eq.'%') go to 155
        read(line,*) j,c
        call lstchr(atname(j),ij)
        write(*,'('' Charge edit:  site '',i5,'' named *'',a,
     x    ''* original charge = '',f10.4,'' new charge = '',f10.4)')
     x    j,atname(j)(1:ij),charge(j),c
        charge(j)=c
        go to 155
160     continue
        close(16)
      endif

c
c     check the net charge on each monomer for neutrality
c
      do i=1,nsites
        call parser(atname(i),elfield,lel,i1,i2,i3)
c       monomer index is i3
c       not sure why the i3.gt.900 is relevant (3/14/2013)
        if(i3.lt.0.or.i3.gt.900) then
          write(*,'('' Too many monomers or invalid monomer id'')')
          write(*,'('' Monomer id is '',i9)') i3
          stop 'invalid monomer id'
        endif
        chgmonomer(i3+1)=chgmonomer(i3+1)+charge(i)
      enddo
      tcharge=0.d0
      do i=1,nmonmx
        tcharge=tcharge+chgmonomer(i)
        if(dabs(chgmonomer(i)).gt.1.d-10) 
     x    write(*,'('' Net charge on monomer '',i3,'' is '',f10.6)')
     x    i,chgmonomer(i)
      enddo
      write(*,'('' Net charge on molecule is '',f10.6,/)')
     x  tcharge
 


c
c     read the next block of the topology file to get bond information
c
c     skip to line that says Detailed bond information
200   continue
      read(13,'(a120)') line
      if(index(line,'Detailed bond information').eq.0) go to 200
      nbonds=0
210   continue
      read(13,'(a120)',end=220) line
      if(line(1:10).eq.'          ') go to 220
      if(nbonds+1.gt.nbndmx) then
        write(*,'('' Exceeded size of bond storage arrays '')')
        write(*,'('' Current size is '',i9)') nbndmx
        stop 'need to increase size of bond arrays'
      endif
      nbonds=nbonds+1
      read(line,*) ibtype
      call leftjust(line)
      is=index(line,' ')
      call leftjust(line(is+1:))
      isd=index(line(is+1:),'-')  
      iname=line(is+1:is+isd-1)
      call leftjust(line(is+isd+1:))
      jname=line(is+isd+1:)
      do i=1,nsites
        if(iname.eq.atname(i)) then
          iindex=i
          go to 215
        endif
      enddo
215   do i=iindex+1,nsites
        if(jname.eq.atname(i)) then
          jindex=i
          go to 217
        endif
      enddo
217   continue
      ijbond(3,nbonds)=0   !initialize as "unassigned"  
      ijbond(4,nbonds)=ibtype  
      if(iindex.gt.jindex) then
        ijbond(1,nbonds)=jindex
        ijbond(2,nbonds)=iindex
      else
        ijbond(1,nbonds)=iindex
        ijbond(2,nbonds)=jindex
      endif
      go to 210



220   continue

      call getbonds(mfiles,mtypes,nmtypes,nlink,map,nmap,
     x      atname,itype,ijbond,bondpar,nbonds,nsites,
     x      lmbtype,nbntpmx,nbntp)



      write(*,'(/,''Detailed bond information from topology file '')')

      do i=1,nbonds
        write(*,'('' Bond term '',a,2x,a,2x,2f10.5,
     x    '' Assgn = '',i2,'' Type = '',i2,'' Type = '',i3)')
c    x    ijbond(1,i),ijbond(2,i),
     x    atname(ijbond(1,i)),atname(ijbond(2,i)),
     x    bondpar(1,i),bondpar(2,i),
     x    ijbond(3,i),ijbond(4,i),ijbond(5,i)
      enddo
     



c
c     read the next block of the topology file to get angle information
c
c     skip to line that says Detailed angle information
300   continue
      read(13,'(a120)') line
      if(index(line,'Detailed angle information').eq.0) go to 300
      nangle=0
310   continue
      read(13,'(a120)',end=320) line
      if(line(1:10).eq.'          ') go to 320
      if(nangles+1.gt.nangmx) then
        write(*,'('' Exceeded size of angle storage arrays '')')
        stop 'need to increase size of angle arrays'
      endif
      nangles=nangles+1
      call leftjust(line)
      isi=index(line,'-')  !label for i is in line(1:isi-1)
      call leftjust(line(isi+1:))
      isj=index(line(isi+1:),'-')+isi ! line(isj:isj) is the second "-"
      call leftjust(line(isj+1:))
      isk=index(line(isj+1:),' ')+isj ! line(isk:isk) is " "
      iname=line(1:isi-1)
      jname=line(isi+1:isj-1)
      kname=line(isj+1:isk-1)
      do i=1,nsites
        if(iname.eq.atname(i)) then
          iindex=i
          go to 315
        endif
      enddo
315   do i=1,nsites
        if(jname.eq.atname(i)) then
          jindex=i
          go to 317
        endif
      enddo
317   do i=iindex+1,nsites
        if(kname.eq.atname(i)) then
          kindex=i
          go to 319
        endif
      enddo
319   continue
c     write(*,'('' Angle is *'',a,''* *'',a,''*  *'',a,''*'',
c    x      '' Sites '',3i5)')
c    x     iname(1:isi-1),jname(1:isj-isi-1),kname(1:isk-isj-1),
c    x     iindex,jindex,kindex
      ijkangle(4,nangles)=0  !initialize as unassigned
      ijkangle(2,nangles)=jindex
      if(iindex.gt.kindex) then
        ijkangle(1,nangles)=kindex
        ijkangle(3,nangles)=iindex
      else
        ijkangle(1,nangles)=iindex
        ijkangle(3,nangles)=kindex
      endif
      go to 310

320   continue

      call getangles(mfiles,mtypes,nmtypes,nlink,map,nmap,
     x      atname,itype,ijkangle,anglepar,nangles,nsites,
     x      lmantype,nantpmx,nantp)



      write(*,'(/,''Detailed angle information from topology file '')')

      do i=1,nangles
        write(*,'('' Angle term '',a,2x,a,2x,a,2x,2f10.5,2i5)')
c    x    ijkangle(1,i),ijkangle(2,i),ijkangle(3,i),
     x    atname(ijkangle(1,i)),
     x    atname(ijkangle(2,i)),
     x    atname(ijkangle(3,i)),
     x    anglepar(1,i),anglepar(2,i),ijkangle(4,i),ijkangle(5,i)
      enddo



c
c     read the next block of the topology file to get torsion information
c
c     skip to line that says Detailed torsion information
400   continue
      read(13,'(a120)') line
      if(index(line,'Detailed torsion information').eq.0) go to 400
      ndihed=0
410   continue
      read(13,'(a120)',end=420) line
      if(line(1:10).eq.'          ') go to 420
      if(ndihed+1.gt.ndihmx) then
        write(*,'('' Exceeded size of dihedral storage arrays '')')
        stop 'need to increase size of dihedral arrays'
      endif
      ndihed=ndihed+1
      call leftjust(line)
      isi=index(line,'-')  !label for i is in line(1:isi-1)
      call leftjust(line(isi+1:))
      isj=index(line(isi+1:),'-')+isi ! line(isj:isj) is the second "-"
      call leftjust(line(isj+1:))
      isk=index(line(isj+1:),'-')+isj ! line(isk:isk) is the third "-"
      call leftjust(line(isk+1:))
      isl=index(line(isk+1:),' ')+isk ! line(isk:isk) is " "
      iname=line(1:isi-1)
      jname=line(isi+1:isj-1)
      kname=line(isj+1:isk-1)
      lname=line(isk+1:isl-1)
      do i=1,nsites
        if(iname.eq.atname(i)) iindex=i
        if(jname.eq.atname(i)) jindex=i
        if(kname.eq.atname(i)) kindex=i
        if(lname.eq.atname(i)) lindex=i
      enddo
c     write(*,'('' Angle is *'',a,''* *'',a,''* *'',a,''* *'',
c    x      a,''*'','' Sites '',4i5)')
c    x     iname(1:isi-1),jname(1:isj-isi-1),
c    x     kname(1:isk-isj-1),lname(1:isl-isk-1),
c    x     iindex,jindex,kindex,lindex

      ijkldih(5,ndihed)=0  !initialize as unassigned
      if(iindex.gt.lindex) then
        ijkldih(1,ndihed)=lindex
        ijkldih(2,ndihed)=kindex
        ijkldih(3,ndihed)=jindex
        ijkldih(4,ndihed)=iindex
      else
        ijkldih(1,ndihed)=iindex
        ijkldih(2,ndihed)=jindex
        ijkldih(3,ndihed)=kindex
        ijkldih(4,ndihed)=lindex
      endif
      go to 410

     
420   continue


      call getdihed(mfiles,mtypes,nmtypes,nlink,map,nmap,
     x      atname,itype,ijkldih,dihpar,ndihed,ndihmx,nadded,nsites,
     x      lmdtype,nditpmx,nditp,ijbond,nbonds)



      write(*,'(/,''Detailed torsion information from topology file '')
     x   ')

      do i=1,ndihed
        iat=ijkldih(1,i)
        jat=ijkldih(2,i)
        kat=ijkldih(3,i)
        lat=ijkldih(4,i)
c       call lstchr(atname(iat),ip)
c       call lstchr(atname(jat),jp)
c       call lstchr(atname(kat),kp)
c       call lstchr(atname(lat),lp)
        write(*,'('' Torsion term '',i5,2x,
     x      '' Type = '',i4,2x,
     x      a,2x,a,2x,a,2x,a,2x,6f8.3,i5)')
     x    i,ijkldih(6,i),
c    x    (ijkldih(k,i),k=1,4),
     x    atname(iat)(1:10),atname(jat)(1:10),
     x    atname(kat)(1:10),atname(lat)(1:10),
     x    (dihpar(k,i),k=1,6),ijkldih(5,i) 
      enddo

      write(*,'(/,''Additional torsions '')')

      do i=ndihed+1,ndihed+nadded
        iat=ijkldih(1,i)
        jat=ijkldih(2,i)
        kat=ijkldih(3,i)
        lat=ijkldih(4,i)
        if(iat.le.0.or.iat.gt.nsites) then
          write(*,'(''Problem with torsion '',i6,
     x      '' type '',i5,'' sites '',4i8)')
     x      i,ijkldih(6,i),iat,jat,kat,lat
        endif
        if(jat.le.0.or.jat.gt.nsites) then
          write(*,'(''Problem with torsion '',i6,
     x      '' type '',i5,'' sites '',4i8)')
     x      i,ijkldih(6,i),iat,jat,kat,lat
        endif
        if(kat.le.0.or.kat.gt.nsites) then
          write(*,'(''Problem with torsion '',i6,
     x      '' type '',i5,'' sites '',4i8)')
     x      i,ijkldih(6,i),iat,jat,kat,lat
        endif
        if(lat.le.0.or.lat.gt.nsites) then
          write(*,'(''Problem with torsion '',i6,
     x      '' type '',i5,'' sites '',4i8)')
     x      i,ijkldih(6,i),iat,jat,kat,lat
        endif
        write(*,'('' Torsion term '',i5,2x,
     x      '' Type = '',i4,2x,
     x      a,2x,a,2x,a,2x,a,2x,6f8.3,i5)')
     x    i,ijkldih(6,i),
c    x    (ijkldih(k,i),k=1,4),
     x    atname(iat)(1:10),atname(jat)(1:10),
     x    atname(kat)(1:10),atname(lat)(1:10),
     x    (dihpar(k,i),k=1,6),ijkldih(5,i)
      enddo



      close(10)


c     do i=1,nsites
c       charge(i)=0.d0
c       vdw(1,i)= 0.d0
c       vdw(2,i)= 0.d0
c     enddo


      call wrtmmol(nsites,atname,ijbond,bondpar,nbonds,charge,vdw,
     x             ijkangle,anglepar,nangles,
     x             ijkldih,dihpar,ndihed,nadded,fileset,molname)


c     write out file(s) for lammps simulation
      call wrtlmps(nsites,lmatype,nattp,map,
     x             atname,charge,vdw,itype,
     x             ijbond,bondpar,nbonds,lmbtype,nbntp,
     x             ijkangle,anglepar,nangles,
     x             lmantype,nantp,
     x             ijkldih,dihpar,ndihed,nadded,
     x             lmdtype,nditp,fileset,molname)

c     write out psf file for vmd
      call wrtpsf (nsites,lmatype,nattp,map,
     x             atname,charge,vdw,itype,
     x             ijbond,bondpar,nbonds,lmbtype,nbntp,
     x             ijkangle,anglepar,nangles,
     x             lmantype,nantp,
     x             ijkldih,dihpar,ndihed,nadded,
     x             lmdtype,nditp,istrand,mdescr,nmonomer,
     x             fileset)



      end

c
c     subroutine to write out psf file for vmd use
c      note that the psf file has mass and charge information and this is not
c      known until force field assignment, so the psf file should be produced
c      by the ffassign program.
c
      subroutine wrtpsf(nsites,lmatype,nattp,map,
     x             atname,charge,vdw,itype,
     x             ijbond,bondpar,nbonds,lmbtype,nbntp,
     x             ijkangle,anglepar,nangles,
     x             lmantype,nantp,
     x             ijkldih,dihpar,ndihed,nadded,
     x             lmdtype,nditp,istrand,mdescr,nmonomer,fileset)
      implicit real*8 (a-h,o-z)
      dimension lmatype(3,nattp)
      character*20 map(2,100)  !maps names in topology file to names in force field file
      character*2 elname   
      character*120 line,line1,fileset
      dimension amass(120)  !array that holds atomic masses populated by subroutine amassinit
      character*120 mdescr(nmonomer) !one line per monomer unit in the polymer

      character*(*) atname(nsites)
      dimension charge(nsites),vdw(2,nsites),itype(nsites)

      dimension ijbond(5,nbonds),bondpar(2,nbonds),lmbtype(4,nbntp)  !bond information
      dimension ijkangle(5,nangles),anglepar(2,nangles)
      dimension lmantype(4,nantp)  !angle information
      dimension ijkldih(6,ndihed),dihpar(8,ndihed)
      dimension lmdtype(4,nditp)  !dihedral information
      dimension istrand(nmonomer)
 
      dimension iscratch(1000)   !hope this to be bigger than nattp

      call amassinit(amass,120)

      isfs=index(fileset,' ')-1
      open(15,file=fileset(1:isfs)//'.psf')

      write(15,'(''PSF '')')
      write(15,*)
      write(15,*)
      write(15,'(i8,'' !NTITLE'')') 3
c     note could write the date, names of files used, summary of structure
      write(15,'('' REMARKS '','' Generated by ffassign  '')') 
      write(15,'('' REMARKS '','' Generated by ffassign  '')') 
      write(15,'('' REMARKS '','' Generated by ffassign  '')') 
      write(15,*)

      write(15,'(i8,'' !NATOM'')') nsites

c     fields are atom ID
c                segment name   (might eventually make this into a strand index)
c                residue ID
c                residue name
c                atom name
c                atom type (charmm format uses integer; xplor uses name)
c                charge
c                mass

      do i=1,nsites
        call parser(atname(i),line,lele,i1,i2,i3)
c       the monomer index is i3+1; strand     
        if(i3.lt.0) i3=0
        istrnd=istrand(i3+1)
        is=index(mdescr(i3+1),' ')
        iat=igetatnum(line(1:lele))
        if(iat.gt.0) then
          write(15,'(i8,1x,a1  
c                    atom and segment 
     x             ,i5,4x,a3     
c                    residue
     x             ,2x,a3,2x,a3    
c                    atom
     x             ,f12.6,f14.4,i20 
c                    charge and mass
     x              )')     
     x      i,char(ichar('A')+istrnd)
     x      ,i3,mdescr(i3+1)(1:is-1)
     x      ,line(1:lele),line(1:lele)
     x      ,charge(i),amass(iat),0 
        else
          write(15,'(i8,1x,a1  
     x             ,i5,4x,a3     
     x             ,2x,a3,2x,a3    
     x             ,f12.6,f14.4,i20 
     x              )')     
     x      i,char(ichar('A')+istrnd)
     x      ,i3,mdescr(i3+1)(1:is-1)
     x      ,line(1:lele),line(1:lele)
     x      ,charge(i),-100.d0,0 
        endif
      enddo
c
c
c     list the bonds

      write(15,*)
      write(15,'(i8,'' !NBONDS'')') nbonds
c
c     four pairs per line 8i8
      nsets=nbonds/4
      nleft=nbonds-4*nsets
      do i=1,nsets    
        write(15,'(8i8)')
     x  (ijbond(1,k),ijbond(2,k),k=4*(i-1)+1,4*i)
      enddo   
      if(nleft.gt.0) then
        write(15,'(8i8)')
     x  (ijbond(1,k),ijbond(2,k),k=4*nsets+1,nbonds)
      endif
c
 
      write(15,*)
      close(15)





      end





c
c     subroutine to write out files for lammps simulation
c
      subroutine wrtlmps(nsites,lmatype,nattp,map,
     x             atname,charge,vdw,itype,
     x             ijbond,bondpar,nbonds,lmbtype,nbntp,
     x             ijkangle,anglepar,nangles,
     x             lmantype,nantp,
     x             ijkldih,dihpar,ndihed,nadded,
     x             lmdtype,nditp,fileset,molname)
      implicit real*8 (a-h,o-z)
      dimension lmatype(3,nattp)
      character*20 map(2,100)  !maps names in topology file to names in force field file
      character*2 elname   
      character*120 line,line1,fileset
      dimension amass(120)  !array that holds atomic masses populated by subroutine amassinit

      character*(*) atname(nsites),molname
      dimension charge(nsites),vdw(2,nsites),itype(nsites)

      dimension ijbond(5,nbonds),bondpar(2,nbonds),lmbtype(4,nbntp)  !bond information
      dimension ijkangle(5,nangles),anglepar(2,nangles)
      dimension lmantype(4,nantp)  !angle information
      dimension ijkldih(6,ndihed),dihpar(8,ndihed)
      dimension lmdtype(4,nditp)  !dihedral information
 
      dimension iscratch(1000)   !hope this to be bigger than nattp

      call amassinit(amass,120)

      isfs=index(fileset,' ')-1
      open(15,file=fileset(1:isfs)//'.lmp')

      write(*,'(/,'' Occurences of the various atom types '')')
      n=0
      do i=1,nattp
        j=lmatype(1,i)
        ism=index(map(1,j),' ')-1
        write(*,'('' Atom type '',i3,'' Number observed = '',i4,
     x    '' Map('',i3,'')='',a)')
     x    i,lmatype(2,i),j,map(1,j)(1:ism)
        n=n+lmatype(2,i)
      enddo
        write(*,'('' Total observed'',3x,''               '',i4)')
     x  n
      write(*,'(/)')

c     verify that all atoms of the same type have the same lj parameters
      do i=1,nattp
        j=lmatype(1,i)  
        ifrst=0
        ic=0
        do k=1,nsites
          if(itype(k).eq.j) then
            ic=ic+1
            if(ifrst.eq.0) then
              ifrst=k
              lmatype(3,i)=k  !save the first atom example of this type
            else
              if(vdw(1,k).ne.vdw(1,ifrst).or. 
     x           vdw(2,k).ne.vdw(2,ifrst)) then
              write(*,'('' Error:  different LJ parameters '',
     x                  ''within an atom type '',i3)') i
              stop 'Error:  multiple LJ types within atom type'
              endif
            endif
          endif
        enddo
c       write(15,'('' Number sites of this type = '',2i8,
c    x    '' first='',i5)')
c    x    ic,lmatype(2,i),lmatype(3,i)
      enddo





      write(15,'('' Header line '')')
      write(15,*)
      write(15,'(5x,i10,'' atoms '')') nsites
      write(15,'(5x,i10,'' bonds '')') nbonds
      write(15,'(5x,i10,'' angles '')') nangles
      write(15,'(5x,i10,'' dihedrals '')') ndihed + nadded

      write(15,*)
      write(15,'(5x,i10,'' atom types '')') nattp
      write(15,'(5x,i10,'' bond types '')') nbntp
      write(15,'(5x,i10,'' angle types '')') nantp
      write(15,'(5x,i10,'' dihedral types '')') nditp

    
      xlo=-1318.8290807597d0/2
      xhi= 1318.8290807597d0/2
      write(15,*)
      write(15,'(5x,2f15.8,'' xlo xhi '')') xlo,xhi
      write(15,'(5x,2f15.8,'' ylo yhi '')') xlo,xhi
      write(15,'(5x,2f15.8,'' zlo zhi '')') xlo,xhi


      write(15,*)
      write(15,'('' Masses  '')')
      write(15,*)
      do i=1,nattp
c       extract element label from map entry
        j=lmatype(1,i) 
        k=index(map(1,j),'_')+1
        elname=map(1,j)(k:k+1)
        if(elname(2:2).eq.'_') elname(2:2)=' '
        ism=index(map(1,j),' ')-1
        if(igetatnum(elname).ne.0) then
          write(15,'(i10,f15.8,
     x      ''    # Num '',i4,'' Ex: '',i4,'' '',a)') 
     x      i,amass(igetatnum(elname)),
     x      lmatype(2,i),lmatype(3,i),map(1,j)(1:ism)
        else
          write(15,'(i10,f15.8,
     x      ''    # Num '',i4,'' Ex: '',i4,'' '',a)') 
     x      i,-100.0,
     x      lmatype(2,i),lmatype(3,i),map(1,j)(1:ism)
        endif
      enddo

      write(15,*)
      write(15,'('' Pair Coeffs'')')
      write(15,*)
      f=1.d0/(2.d0**(1.d0/6.d0))
      do i=1,nattp
        j=lmatype(1,i) 
        ism=index(map(1,j),' ')-1
        write(15,'(i10,4f15.8,''    # '',a)') i,
     x    vdw(2,lmatype(3,i)),
     x    vdw(1,lmatype(3,i))*f,
     x    vdw(2,lmatype(3,i)),
     x    vdw(1,lmatype(3,i))*f,
     x    map(1,j)(1:ism)
      enddo

      write(15,*)
      write(15,'('' Bond Coeffs'')')
      write(15,*)
      do i=1,nbntp
        ifrst=lmbtype(2,i)
        iat=ijbond(1,ifrst)
        jat=ijbond(2,ifrst)
        write(15,'(i10,2f15.8,
     x    ''    # Num '',i4,'' Ex: '',i4,'' '',a,'' - '',a)') i,
     x    bondpar(1,ifrst),bondpar(2,ifrst),
     x    lmbtype(1,i),ifrst,atname(iat),atname(jat)
      enddo

      write(15,*)
      write(15,'('' Angle Coeffs'')')
      write(15,*)
      pi=4.d0*datan(1.d0)
      do i=1,nantp
        ifrst=lmantype(2,i)
        iat=ijkangle(1,ifrst)
        jat=ijkangle(2,ifrst)
        kat=ijkangle(3,ifrst)
        call lstchr(atname(iat),ip)
        call lstchr(atname(jat),jp)
        call lstchr(atname(kat),kp)
        write(15,'(i10,2f15.8,
     x    ''    # Num '',i4,'' Ex: '',i4,'' '',a,'' - '',a,'' - '',a)') 
     x    i,anglepar(1,ifrst),(180.d0/pi)*anglepar(2,ifrst),
     x    lmantype(1,i),ifrst,
     x    atname(iat)(1:ip),atname(jat)(1:jp),atname(kat)(1:kp)
      enddo

      write(15,*)
      write(15,'('' Dihedral Coeffs'')')
      write(15,*)
      do i=1,nditp
        ifrst=lmdtype(2,i)
        iat=ijkldih(1,ifrst)
        jat=ijkldih(2,ifrst)
        kat=ijkldih(3,ifrst)
        lat=ijkldih(4,ifrst)
        call lstchr(atname(iat),ip)
        call lstchr(atname(jat),jp)
        call lstchr(atname(kat),kp)
        call lstchr(atname(lat),lp)
c
c       convert from Mulliken torsion types to charmm style representation
c       E = K * ( 1 + cos( n*phi - d ) )   
c       n, integer; d, integer degrees
c       typical line in Dihedral Coeffs block:
c       3 10.500 2 180 0.0
c       3 is the dihedral type index; 
c       10.500 is the K value; 2 is the n value; 180 is d
c       and 0.0 is the multiplier for 1-4 nonbonds interactions
c
c       dihpar(1,*) = torsion type
c       dihpar(2,*) = number of parameters
c       dihpar(3,*) = first of parameters
        imtype=dihpar(1,ifrst)
        nparm=dihpar(2,ifrst)
        x=dihpar(3,ifrst)

c       write(*,'('' Wrtlmp: tortype: i,imtype,nparm,x = ''3i5,f10.5)')
c    x     i,imtype,nparm,x


c       if(nparm.ne.3) then
c         write(*,'('' Illegal torsion type '',i3)') imtype
c         stop 'opls-style torsion not allowed'
c       endif

        if(nparm.eq.3) then
          if(imtype.eq.3) then
            n=1
            id=0
          elseif(imtype.eq.4) then
            n=1
            id=180
          elseif(imtype.eq.5) then
            n=2
            id=0
          elseif(imtype.eq.6) then
            n=2
            id=180
          elseif(imtype.eq.7) then
            n=3
            id=0
          elseif(imtype.eq.8) then
            n=3
            id=180
          elseif(imtype.eq.10) then
            n=6
            id=180
          elseif(imtype.eq.15) then
            n=4
            id=180
          elseif(imtype.eq.18) then
            n=5
            id=0
          else
            write(*,'('' Torsion type not implemented '',i3)') imtype
            stop 'torsion type not implemented'
          endif
        elseif(nparm.eq.6) then
          if(imtype.eq.23) then      !Torsion_type_23:  E = p1 + p2*(1+cos(p3*phi-p4))
            p1=dihpar(3,ifrst)       ! usually zero
            X= dihpar(4,ifrst)       ! amplitude
            n= nint(dihpar(5,ifrst)) ! multiplicity
            id=dihpar(6,ifrst)       ! phase, in degrees
          else
            write(*,'('' Torsion type not implemented '',i3)') imtype
            stop 'torsion type not implemented'
          endif
        elseif(nparm.eq.5) then
          if(imtype.eq.25) then
            p1=dihpar(3,ifrst)     ! usually zero, not used in charmm format
            X= dihpar(4,ifrst)     ! amplitude
            n=2
            id=dihpar(5,ifrst)     ! phase, in degrees
          elseif(imtype.eq.26) then
            p1=dihpar(3,ifrst)     ! usually zero, not used in charmm format
            X= dihpar(4,ifrst)     ! amplitude
            n=3
            id=dihpar(5,ifrst)     ! phase, in degrees
          else
            write(*,'('' Torsion type not implemented '',i3)') imtype
            stop 'torsion type not implemented'
          endif
        else
          write(*,'('' Torsion type not implemented '',i3)') imtype
          stop 'torsion type not implemented'
        endif

        wt=0.d0
c       lammps wants k (amplitude, kcal/mole), n>=0 (multiplicity), d (integer degrees), 1-4 weight
c       dihedral_style charmm      has functional form E = k * (1 + cos(n*phi - d))
c       dihedral_coeff    1       120.0   1   60   0.5
c                      dihedralID  K      n   d     w
        write(15,'(i10,f10.5,i5,i5,f10.5,    
     x    ''    # Num '',i4,'' Ex: '',i5,
     x    '' '',a,'' - '',a,'' - '',a,'' - '',a)') 
     x    i,x,n,id,wt,
     x    lmdtype(1,i),ifrst,
     x    atname(iat)(1:ip),atname(jat)(1:jp),
     x    atname(kat)(1:kp),atname(lat)(1:lp)
c    x    '' TTyp '',i2,'' NPrm '',i2,2x,f10.5,2f15.8,
c    x    int(dihpar(1,ifrst)),int(dihpar(2,ifrst)),
      enddo


      write(15,*)
      write(15,'('' Atoms '')')
      write(15,*)


c     note that following trashes itype
      do i=1,1000    !dangerous - need to check that this is big enough;stop if not
        iscratch(i)=0
      enddo
      do i=1,nattp
        j=lmatype(1,i)
        iscratch(j)=i
      enddo
      do i=1,nsites
        j=itype(i)
        itype(i)=iscratch(j)
      enddo


      isfs=index(fileset,' ')-1
      open(18,file=fileset(1:isfs)//'.geo')
      read(18,'(a120)') line  !expect this to be Geometry= line

      molid=1
      n=0
      call lstchr(molname,i2)
      do i=1,nsites
        read(18,'(a120)',end=120) line  
        line1=line
        call ucase(line)
        if(index(line,'END GEOMETRY').ne.0) go to 120
        call leftjust(line1)
        iloc=index(line1,' ')+1
        read(line1(iloc:),*) x1,y1,z1
        it=itype(i)
        call lstchr(atname(i),i1)
        write(15,'(i10,i5,i5,f15.10,3f15.8,''    # '',a,2x,a)') 
c    x    i,molid,it,charge(i),x1,y1,z1,molname(1:i2),atname(i)(1:i1)
     x    i,molid,it,charge(i),x1,y1,z1,atname(i)(1:i1),molname(1:i2)
      enddo

120   continue

      close(18)
c     restore itype to previous state in case needed
      do i=1,nsites
        j=itype(i)
        itype(i)=lmatype(1,j)
      enddo





      write(15,*)
      write(15,'('' Velocities '')')
      write(15,*)
      do i=1,nsites
        write(15,'(i10,5x,3f10.5)') i,0.,0.,0.
      enddo


      write(15,*)
      write(15,'('' Bonds  '')')
      write(15,*)
      do i=1,nbonds
        write(15,'(i10,3i10)') i,ijbond(5,i),ijbond(1,i),ijbond(2,i)
      enddo


      write(15,*)
      write(15,'('' Angles '')')
      write(15,*)
      do i=1,nangles
        write(15,'(i10,4i10)') i,ijkangle(5,i),
     x    ijkangle(1,i),ijkangle(2,i),ijkangle(3,i)
      enddo


      write(15,*)
      write(15,'('' Dihedrals '')')
      write(15,*)
      do i=1,ndihed +nadded
        if(ijkldih(1,i).le.0.or.ijkldih(1,i).gt.nsites) then
          write(*,'('' Problem with torsion '',i10,
     x      '' Sites: '',4i8)') i,ijkldih(1,i),ijkldih(2,i),
     x                          ijkldih(3,i),ijkldih(4,i)
        endif
        if(ijkldih(2,i).le.0.or.ijkldih(2,i).gt.nsites) then
          write(*,'('' Problem with torsion '',i10,
     x      '' Sites: '',4i8)') i,ijkldih(1,i),ijkldih(2,i),
     x                          ijkldih(3,i),ijkldih(4,i)
        endif
        if(ijkldih(3,i).le.0.or.ijkldih(3,i).gt.nsites) then
          write(*,'('' Problem with torsion '',i10,
     x      '' Sites: '',4i8)') i,ijkldih(1,i),ijkldih(2,i),
     x                          ijkldih(3,i),ijkldih(4,i)
        endif
        if(ijkldih(4,i).le.0.or.ijkldih(4,i).gt.nsites) then
          write(*,'('' Problem with torsion '',i10,
     x      '' Sites: '',4i8)') i,ijkldih(1,i),ijkldih(2,i),
     x                          ijkldih(3,i),ijkldih(4,i)
        endif
        write(15,'(i10,5i10)') i,ijkldih(6,i),
     x    ijkldih(1,i),ijkldih(2,i),ijkldih(3,i),ijkldih(4,i)
      enddo


      write(15,*)
 

      close(15)




c
c     implement geometric combining rules information for lammps
c
      open(15,file=fileset(1:isfs)//'.paircoeff')
      write(*,'('' Geometric combining rules for all type pairs '')')

      f=1.d0/(2.d0**(1.d0/6.d0))
      do i=1,nattp
        iexample=lmatype(1,i) 
c fix   vd2i=vdw(2,iexample) !epsilon
c fix   vd1i=vdw(1,iexample) !sigma  
        vd2i=vdw(2,lmatype(3,i)) !epsilon
        vd1i=vdw(1,lmatype(3,i)) !sigma  
        ism=index(map(1,iexample),' ')-1
c       write(15,'('' Atom i type '',i5,'' example atom= '',i8,
c    x    '' epsilon= '',f15.8,
c    x    '' sigma= '',f15.8)') i,lmatype(3,i),vd2i,f*vd1i
        do j=i,nattp
          jexample=lmatype(1,j) 
          vd2j=vdw(2,lmatype(3,j)) !epsilon
          vd1j=vdw(1,lmatype(3,j)) !sigma  
c         write(15,'('' Atom j type '',i5,'' example atom= '',i8,
c    x      '' epsilon= '',f15.8,
c    x      '' sigma= '',f15.8)') j,lmatype(3,j),vd2j,f*vd1j
          jsm=index(map(1,jexample),' ')-1
          write(15,'(''pair_coeff     '',2i5,2f15.8,''   # '',a)')
     x      i,j,
     x      dsqrt(vd2i*vd2j),
     x      f*dsqrt(vd1i*vd1j),
     x      map(1,iexample)(1:ism)//' - '//map(1,jexample)(1:jsm)
        enddo
      enddo

      close(15)






      end

c
c     subroutine to write out a Mulliken-style mmol file
c     describes molecule coordinates (copied from bldstrand.geo)
c     and force field
c

      subroutine wrtmmol(nsites,atname,ijbond,bondpar,nbonds,charge,vdw,
     x                   ijkangle,anglepar,nangles,
     x             ijkldih,dihpar,ndihed,nadded,fileset,molname)
      implicit real*8 (a-h,o-z)
      character*(*) atname(nsites),molname
      dimension ijbond(5,nbonds),bondpar(2,nbonds)
      dimension ijkangle(5,nangles),anglepar(2,nangles)  !holds indices, chosen flag, parms for angle list
      dimension ijkldih(6,*),dihpar(8,*)  !holds indices, chosen flag, parms for torsion list
      dimension charge(nsites),vdw(2,nsites)

      character*120 line,line1,fileset
      character*40 iname,jname,kname,lname
      dimension idihed(100)  !scratch array holding sequence numbers for all torsion terms associated with a given angle

      isfs=index(fileset,' ')-1
      open(17,file=fileset(1:isfs)//'.mmol')

      write(17,'(''%Force field file generated by program ffassign'')')
      call lstchr(molname,il)
      write(17,'(/,''Molecule '',a)') molname(1:il)

c     write geometry block
      write(17,'(/,''Geometry = '')')
        open(18,file=fileset(1:isfs)//'.geo')
        read(18,'(a120)') line  !expect this to be Geometry= line
100     continue
        read(18,'(a120)',end=120) line  !expect this to be Geometry= line
        line1=line
        call ucase(line)
        if(index(line,'END GEOMETRY').ne.0) go to 120
        call lstchr(line1,il)
        write(17,'(a)') line1(1:il)
        go to 100
120     continue

        close(18)




      write(17,'(/,''AutoTopology = No'')')
c     write topology block
      write(17,'(''Topology = '')')
      do i=1,nbonds
        ib=ijbond(1,i)
        jb=ijbond(2,i)
        iname=atname(ib)
        jname=atname(jb)
        call lstchr(iname,ip)
        call lstchr(jname,jp)
        if(ijbond(4,i).eq.1) then
          write(17,'(''Single '',a,''-'',a)') iname(1:ip),jname(1:jp)
        elseif(ijbond(4,i).eq.2) then
          write(17,'(''Double '',a,''-'',a)') iname(1:ip),jname(1:jp)
        endif
      enddo
     

c     write forcefield block
      write(17,'(/,''Forcefield = '')')
      write(17,'(''Generic      '')')
      write(17,'(''Scale_1_4_vdw 0.5'')')
      write(17,'(''Scale_1_4_cou 0.5'')')
      write(17,'(''Geom_mean_vdw_radii'')')
      write(17,'(''Interff_priority 1'')')
c
c     lj block
c
      do i=1,nsites
        line=atname(i)
        call lstchr(line,ip)
        ip=max(ip,30)
        write(17,'(''Lennard_Jones_6_12 '',a,2x,f10.6,2x,f10.6)') 
     x    line(1:ip),vdw(1,i),vdw(2,i)
      enddo
c
c     charge block
c
      do i=1,nsites
        line=atname(i)
        call lstchr(line,ip)
        m=max(ip,30)
        write(17,'(''Charge '',a,2x,f10.6)') line(1:m),charge(i)  
      enddo
c
c     bond block
c
      do i=1,nbonds
        ib=ijbond(1,i)
        jb=ijbond(2,i)
        iname=atname(ib)
        jname=atname(jb)
        call lstchr(iname,ip)
        call lstchr(jname,jp)
        line(1:)=iname(1:ip)//'-'//jname(1:jp)
        m=max(30,ip+jp+1)
        write(17,'(''Harmonic_Bond  '',a,2x,f10.6,2x,f10.6)') 
     x    line(1:m),bondpar(1,i),bondpar(2,i)  
      enddo
c
c     angle block
c
      do i=1,nangles
        ib=ijkangle(1,i)
        jb=ijkangle(2,i)
        kb=ijkangle(3,i)
        iname=atname(ib)
        jname=atname(jb)
        kname=atname(kb)
        call lstchr(iname,ip)
        call lstchr(jname,jp)
        call lstchr(kname,kp)
        line=
     x    iname(1:ip)//'-'//jname(1:jp)//'-'//kname(1:kp)
        m=max(30,ip+jp+kp+2)
        write(17,'(''Harmonic_Angle '',a,2x,f10.6,2x,f10.6)') 
     x    line(1:m),anglepar(1,i),anglepar(2,i)  
      enddo
c
c     torsion block
c
c     do i=1,ndihed+nadded
c       ib=ijkldih(1,i)
c       jb=ijkldih(2,i)
c       kb=ijkldih(3,i)
c       lb=ijkldih(4,i)
c       iname=atname(ib)
c       jname=atname(jb)
c       kname=atname(kb)
c       lname=atname(lb)
c       call lstchr(iname,ip)
c       call lstchr(jname,jp)
c       call lstchr(kname,kp)
c       call lstchr(lname,lp)
c       line=' '
c       write(line(1:),'(''Torsion_type_'',i2)') int(dihpar(1,i))
c       if(line(14:14).eq.' ') then
c         line(14:14)=line(15:15)
c         line(15:)=' '          
c       endif
c       line(17:)=
c    x    iname(1:ip)//'-'//jname(1:jp)//'-'
c    x  //kname(1:kp)//'-'//lname(1:lp)
c       m=max(60,16+ip+jp+kp+lp+3)
c       nparm=dihpar(2,i)
c       write(17,'(a,2x,10f10.6)') 
c    x    line(1:m),(dihpar(k,i),k=3,nparm)
c     enddo



c
c     develop reordered torsion block; group all expressions for a given torsion angle together
c
c
c     first check that all torsions are assigned and have a legal type
      ierror=0
      do i=1,ndihed+nadded
        m=ijkldih(5,i)  !this is the assigned/unassigned flag and the chain index if more than one
        x=dihpar(1,i)   !this is the torsion type (should be non zero)
        if(m.eq.0.or.x.eq.0.d0) then
          ierror=1
          ib=ijkldih(1,i)
          jb=ijkldih(2,i)
          kb=ijkldih(3,i)
          lb=ijkldih(4,i)
          iname=atname(ib)
          jname=atname(jb)
          kname=atname(kb)
          lname=atname(lb)
          call lstchr(iname,ip)
          call lstchr(jname,jp)
          call lstchr(kname,kp)
          call lstchr(lname,lp)
          line(1:ip+jp+kp+lp+3)=
     x      iname(1:ip)//'-'//jname(1:jp)//'-'
     x    //kname(1:kp)//'-'//lname(1:lp)
          write(*,'('' Error with torsion term '',i6,'':'',a,
     x      /,'' unassigned (if m=0) m='',i3,
     x      '' invalid torsion_type (if x=0) x='',f10.5)')
     x      i,line(1:ip+jp+kp+lp+3),m,x
        endif
      enddo
      if(ierror.ne.0) stop 'Stopping due to problem with torsions'

c
c     now cycle through all of them moving through the chain, and mark as taken
      iseq=0
      do i=1,ndihed+nadded
        mb=ijkldih(5,i)  !assigned/unassigned flag and chain index if more than one
        if(mb.ne.0) then !this will always be nonzero because of check in previous loop
          icount=1   !icount counts the number of torsion terms that use this particular angle
          ib=ijkldih(1,i)
          jb=ijkldih(2,i)
          kb=ijkldih(3,i)
          lb=ijkldih(4,i)
          iname=atname(ib)
          jname=atname(jb)
          kname=atname(kb)
          lname=atname(lb)
          call lstchr(iname,ip)
          call lstchr(jname,jp)
          call lstchr(kname,kp)
          call lstchr(lname,lp)
          line=' '
          write(line(1:),'(''Torsion_type_'',i2)') int(dihpar(1,i))
          if(line(14:14).eq.' ') then
            line(14:14)=line(15:15)
            line(15:)=' '          
          endif
          line(17:)=
     x      iname(1:ip)//'-'//jname(1:jp)//'-'
     x    //kname(1:kp)//'-'//lname(1:lp)
          m=max(60,16+ip+jp+kp+lp+3)
          nparm=dihpar(2,i)
          iseq=iseq+1

c         write(19,'('' iseq='',i6,'' i='',i6,'' ix='',i6,
c    x      '' icount='',i2,
c    x      a,2x,10f10.6)') 
c    x      iseq,i,mb,icount,line(1:m),(dihpar(k,i),k=3,nparm)
          write(17,'(a,2x,10f10.6)') 
     x      line(1:m),(dihpar(k,i),k=3,nparm)

          ijkldih(5,i)=0 !set this value to zero to mean this term has been written out
          idihed(icount)=i  !indicate that this torsion angle uses energy expression i
          ix=mb    !mb will either be 1 or some large positive number at this point
10        if(ix.eq.1) go to 30
c         here if there is a chain to process for this torsion; get each one
c         get the item indicated by ix
          ib=ijkldih(1,ix)
          jb=ijkldih(2,ix)
          kb=ijkldih(3,ix)
          lb=ijkldih(4,ix)
          iy=ijkldih(5,ix)
          iname=atname(ib)
          jname=atname(jb)
          kname=atname(kb)
          lname=atname(lb)
          call lstchr(iname,ip)
          call lstchr(jname,jp)
          call lstchr(kname,kp)
          call lstchr(lname,lp)
          line=' '
          write(line(1:),'(''Torsion_type_'',i2)') int(dihpar(1,ix))
          if(line(14:14).eq.' ') then
            line(14:14)=line(15:15)
            line(15:)=' '          
          endif
          line(17:)=
     x      iname(1:ip)//'-'//jname(1:jp)//'-'
     x    //kname(1:kp)//'-'//lname(1:lp)
          m=max(60,16+ip+jp+kp+lp+3)
          nparm=dihpar(2,ix)
          iseq=iseq+1
          icount=icount+1
          idihed(icount)=ix

c         write(19,'('' iseq='',i6,'' i='',i6,'' ix='',i6,
c    x      '' icount='',i2,
c    x      a,2x,10f10.6)') 
c    x      iseq,ix,iy,icount,line(1:m),(dihpar(k,i),k=3,nparm)
          write(17,'(a,2x,10f10.6)') 
     x      line(1:m),(dihpar(k,ix),k=3,nparm)

          ijkldih(5,ix)=0 !set this value to zero to mean this term has been written out
          ix=iy
          go to 10
30        continue

c         check that there are no duplicated torsion types among the icount dihedrals
          do kk=1,icount-1
            ikktype=dihpar(1,idihed(kk))
c           write(19,'('' set of dihedrals:  '',i2,'' type= '',i2)') 
c    x        kk,ikktype
            do jj=kk+1,icount
              ijjtype=dihpar(1,idihed(jj))
              if(ikktype.eq.ijjtype) then
                write(*,'('' Error:  multiply assigned parameters '',
     x            ''for torsion angle '')')
                write(*,'('' Error:  iseq='',i6,'' ikktype='',i2)') 
     x            iseq,ikktype
                write(*,'('' Check end of mmol file for'',
     x            '' specific offending term'')')
                stop 'Error: multiply assigned torsion parameters'
              endif
            enddo
          enddo  !end of check loop

        endif



      enddo




      write(17,'(/,''End Molecule'')')
      close(17)


      end



c
c     subroutine to get torsion parameters
c
      subroutine getdihed(mfiles,mtypes,nmtypes,nlink,map,nmap,
     x      atname,itype,ijkldih,dihpar,ndihed,ndihmx,nadded,nsites,
     x      lmdtype,nditpmx,nditp,ijbond,nbonds)
      implicit real*8 (a-h,o-z)
      dimension ijkldih(6,ndihmx),dihpar(8,ndihmx),itype(nsites)
      character*(*) mfiles(nmtypes),mtypes(nmtypes),map(2,nmap)
      character*(*) atname(nsites)
      character*120 line,string
      character*40 iname,jname,kname,lname,elfield
      dimension parm(50) !number of torsion parameters on a Torsion_type line
      dimension ijbond(5,nbonds)   !needed only for improper torsion that are in the linkage file
      dimension iconn(10)          !needed only for improper torsion that are in the linkage file

      dimension lmdtype(4,nditpmx)  !for lammps

c
c     a note about improper torsions:  these might NOT be in the topology list, but would be in the
c     force field file; when they are there, we need to use them.  note that these will never involve
c     the linkage torsions (I think) NOT TRUE:  linkage files might have an improper
c     if the linkage bond is involved in an improper.  This is the case if esters
c     are connected through the ester bond, which is used in the LLAC unit, unlike
c     for the PLA unit.

      nadded=0  !number of additions to the torsion list from multiple torsion types for some torsions
                !note that his count includes the improper torsions, too, which are in the ff file, but
                !not in the torsion list from the topology file

      nditp=0   !counts types of torsion terms


      do i=1,ndihmx
        do j=1,8
        dihpar(j,i)=0.d0
        enddo
      enddo

c
c     loop over the force field files, extracting torsion terms
c
      do i=1,nmtypes
        mline=0
        open(15,file=mfiles(i),err=135)
        ism=index(mtypes(i),' ')-1
        write(*,'('' Open files with mtypes = '',a)') mtypes(i)(1:ism)
130     continue
        read(15,'(a120)',end=140) line
        mline=mline+1
        call ucase(line)
        call leftjust(line)
        if(line(1:13).eq.'TORSION_TYPE_') then
          read(line(14:),*) itortype
          is=index(line,' ')+1
          call leftjust(line(is:))  !is should point to next position after the first space
          isi=index(line(is:),'-')
          iname=line(is:is+isi-2)
          call leftjust(line(is+isi:))
          isj=index(line(is+isi:),'-')
          jname=line(is+isi:is+isi+isj-2)
          call leftjust(line(is+isi+isj:))
          isk=index(line(is+isi+isj:),'-')
          kname=line(is+isi+isj:is+isi+isj+isk-2)
          call leftjust(line(is+isi+isj+isk:))
          isl=index(line(is+isi+isj+isk:),' ')
          lname=line(is+isi+isj+isk:is+isi+isj+isk+isl-2)
c         note that each torsion type has a different number of parameters
c         find out how many values there are and read them all in
          inext=is+isi+isj+isk+isl
          do j=1,8
            parm(j)=0.d0
          enddo
          nparm=2
131       call leftjust(line(inext:))
          if(line(inext:inext).eq.' '.or.line(inext:inext).eq.'%'
     x          .or.line(inext:inext).eq.'!')  
     x      go to 132
          nparm=nparm+1
c         write(*,'('' Reading field: '',i2,2x,''*'',a,''*'')')
c    x      nparm,line(inext:inext+20)
          read(line(inext:),*) parm(nparm)
          inext=inext+index(line(inext:),' ')
          go to 131
132       continue
          parm(1)=itortype
          parm(2)=nparm
          write(*,'('' Torsion parameters from ff file:'',
     x     a,2x,a,2x,a,2x,a,'' Dihparms: '',
     x     6(2x,i2,2x,f10.5))')
     x     iname(1:isi-1),jname(1:isj-1),
     x     kname(1:isk-1),lname(1:isl-1),
     x     (k,parm(k),k=1,nparm)
          ip=0
          jp=0
          kp=0
          lp=0
          do j=1,nmap
            isp=index(map(1,j),'_')-1
            is2=index(map(2,j),' ')-1
c           mtypes(i)(1:ism) specifies a repeat unit type for this ff file
c           map(1,j)(1:isp) specifies a repeat unit type in the map array
c           xname(1:isx) specifies one of the four site names in the ff file
c           map(2,j)(1:is2) specifies a site name in map array
            if(ip.eq.0. and. 
     x         mtypes(i)(1:ism).eq.map(1,j)(1:isp) .and.
     x         iname(1:isi-1).eq.map(2,j)(1:is2)) ip=j  
            if(jp.eq.0. and. 
     x         mtypes(i)(1:ism).eq.map(1,j)(1:isp) .and.
     x         jname(1:isj-1).eq.map(2,j)(1:is2)) jp=j  
            if(kp.eq.0. and. 
     x         mtypes(i)(1:ism).eq.map(1,j)(1:isp) .and.
     x         kname(1:isk-1).eq.map(2,j)(1:is2)) kp=j  
            if(lp.eq.0. and. 
     x         mtypes(i)(1:ism).eq.map(1,j)(1:isp) .and.
     x         lname(1:isl-1).eq.map(2,j)(1:is2)) lp=j  
          enddo
c
c         write(*,'(''Force field expression match:'')')
c         isp=index(map(1,ip),'_')-1
c         is2=index(map(2,ip),' ')-1
c         write(*,'(''I: map(1,'',i3,'')='',a,
c    x                '' map(2,'',i3,'')='',a)')
c    x    ip,map(1,ip)(1:isp),ip,map(2,ip)(1:is2)
c         isp=index(map(1,jp),'_')-1
c         is2=index(map(2,jp),' ')-1
c         write(*,'(''J: map(1,'',i3,'')='',a,
c    x                '' map(2,'',i3,'')='',a)')
c    x    jp,map(1,jp)(1:isp),jp,map(2,jp)(1:is2)
c         isp=index(map(1,kp),'_')-1
c         is2=index(map(2,kp),' ')-1
c         write(*,'(''K: map(1,'',i3,'')='',a,
c    x                '' map(2,'',i3,'')='',a)')
c    x    Kp,map(1,Kp)(1:isp),Kp,map(2,Kp)(1:is2)
c         isp=index(map(1,lp),'_')-1
c         is2=index(map(2,lp),' ')-1
c         write(*,'(''L: map(1,'',i3,'')='',a,
c    x                '' map(2,'',i3,'')='',a)')
c    x    lp,map(1,lp)(1:isp),lp,map(2,lp)(1:is2)
c

          if(ip.eq.0 .or. jp.eq.0 .or. kp.eq.0 .or. lp.eq.0 ) then
            if(ip.eq.0) write(*,'('' Warning:  torsion term in ff '',
     x        ''file was not found in the map: '',a,2x,i2)')
     x        '*'//iname(1:isi-1)//'*',ip
            if(jp.eq.0) write(*,'('' Warning:  torsion term in ff '',
     x        ''file was not found in the map: '',a,2x,i2)')
     x        '*'//jname(1:isj-1)//'*',jp
            if(kp.eq.0) write(*,'('' Warning:  torsion term in ff '',
     x        ''file was not found in the map: '',a,2x,i2)')
     x        '*'//kname(1:isk-1)//'*',kp
            if(lp.eq.0) write(*,'('' Warning:  torsion term in ff '',
     x        ''file was not found in the map: '',a,2x,i2)')
     x        '*'//lname(1:isl-1)//'*',lp

c    x        '*'//iname(1:isi-1)//'* *'//jname(1:isj-1)
c    x        //'* *'//kname(1:isk-1)//'* *'//lname(1:isl-1)//'*'
c           stop 'One or more torsion sites not found in the map'
            go to 165  !skip this torsion term - no match in the map
          endif


c         not sure the hydrogen check is important anymore; used to be used
c         to allow skipping some entries of the ff file due to some of the
c         hydrogens being missing as connection sites
          ifhyd=0
          if(map(2,ip)(1:2).eq.'H_' .or. map(2,jp)(1:2).eq.'H_' .or.
     x       map(2,kp)(1:2).eq.'H_' .or. map(2,lp)(1:2).eq.'H_') 
     x       ifhyd=1 

c         canonical ordering of ijkl with i<l
          if(lp.lt.ip) then
            iswap=lp
            lp=ip
            ip=iswap
            iswap=kp
            kp=jp
            jp=iswap
          endif
          ifrst=0   !flag for first use of this force field term
          ic=0      !counter for number of uses of this force field term
          do j=1,ndihed  !loop over all the torsions from the topology file
            ib=ijkldih(1,j)
            jb=ijkldih(2,j)
            kb=ijkldih(3,j)
            lb=ijkldih(4,j)
            it=itype(ib)
            jt=itype(jb)
            kt=itype(kb)
            lt=itype(lb)
c           write(*,'('' Checking for ff term in torsion list'',
c    x        i5,'' out of '',i5)') j,ndihed
            if(lt.lt.it) then
              iswap=lt
              lt=it
              it=iswap
              iswap=kt
              kt=jt
              jt=iswap
            endif
            if(it.eq.ip .and. jt.eq.jp .and. 
     x         kt.eq.kp .and. lt.eq.lp) then
c             write(*,'('' found match with ff term and torsion '',
c    x             i5,'' out of '',i5)') j,ndihed
c             check furthermore that these are in the same repeat unit...
c             ...otherwise they should be handled as a linkage bond
              call parser(atname(ib),elfield,lel,i1,i2,i3)
              call parser(atname(jb),elfield,lel,i1,i2,j3)
              call parser(atname(kb),elfield,lel,i1,i2,k3)
              call parser(atname(lb),elfield,lel,i1,i2,l3)
              if(i3.eq.j3 .and. i3.eq.k3 .and. i3.eq.l3) then
                ic=ic+1
                if(ijkldih(5,j).ne.0) then
c                 here if this is a second (or more) torsion term associated with 
c                 a previously assigned ijkl
c                 in this case, need to append to the list; make sure not to exceed ndihmx
c                 make ijkldih(5,*) point to the next in the chain
                  if(nadded+1+ndihed.gt.ndihmx) then
                    write(*,'('' Increase storage for dihedral arrays.''
     x                , '' Current limit is '',i9)') ndihmx
                    stop 'Exceeded storage for dihedral arrays '
                  endif
                  nadded=nadded+1
                  if(ifrst.eq.0) ifrst=ndihed+nadded
                  isave=ijkldih(5,j)          !this points to location of the last one added
                  ijkldih(5,j)=ndihed+nadded  !this points to location of the last one to be added
                  ijkldih(1,ndihed+nadded)=ib
                  ijkldih(2,ndihed+nadded)=jb    
                  ijkldih(3,ndihed+nadded)=kb    
                  ijkldih(4,ndihed+nadded)=lb   
                  ijkldih(5,ndihed+nadded)=isave
                  ijkldih(6,ndihed+nadded)=nditp+1
                  if(ib.eq.0.or.jb.eq.0.or.kb.eq.0.or.lb.eq.0) then
                    write(*,'(''Dih added problems site 1 '')')
                  endif
                  do k=1,nparm
                    dihpar(k,ndihed+nadded)=parm(k)
                  enddo
                else
                  if(ifrst.eq.0) ifrst=j
                  ijkldih(5,j)=1      !this points to location of the last one to be added
                  ijkldih(6,j)=nditp+1
                  do k=1,nparm
                    dihpar(k,j)=parm(k)
                  enddo
c                 write(*,'('' Saving parameters into dihpar(j='',
c    x                    i4,'') = '',6f10.5)') j,
c    x                     (dihpar(k,j),k=1,nparm)
                endif
              endif  !end of ifblock for intra-monomer situation
            endif !end of ifblock for exact type match between topology torsion and torsion from ff file    
          enddo !end of doloop for checking all members of the torsion list from the topology file
          if(ic.gt.0) then
            write(*,'('' Torsion parameter used '',
     x      i6,'' times from line '',i5,'' of file '',i2,2x,a)')
     x      ic,mline,i,mfiles(i)(1:index(mfiles(i),' ')-1)
            nditp=nditp+1
            if(nditp.gt.nditpmx) then
              write(*,'('' Error:  Exceeded array size for '',
     x          ''storage of unique dihedral types (lmdtype)'')')
              stop 'Error:  exceeded array size for lmdtype'
            endif
            lmdtype(1,nditp)=ic
            lmdtype(2,nditp)=ifrst
            lmdtype(3,nditp)=i
            lmdtype(4,nditp)=mline
          endif

c         if(ic.eq.0.and.ifhyd.eq.0) then
          if(ic.eq.0               ) then
c           get here if there is a term in the ff file that is NOT represented in the 
c           topology file (ic=0); assume this is an improper torsion; but it could 
c           also be a term involving a hydrogen that is present in the monomer .mmol file, 
c           but is not in the actual polymer (third H of a methyl, for instance)
c           write(*,'('' Unused (improper?) torsion term '',
c    x        4(2x,a10,2x,a10))') 
c    x        map(1,ip),map(2,ip), map(1,jp),map(2,jp),
c    x        map(1,kp),map(2,kp), map(1,lp),map(2,lp)
c  NOTE that a hydrogen can be involved in an improper torsion in an aldehyde and this
c  situation might not be treated propertly in this code
c  NOTE hydrogens in impropers exist in aromatic rings and this has not been treated 
c  properly (12/1/2011)
ccccccccccccccccccccccc
c         code will look for 
c         scan bond list to get sites this one is connected to
c         nconn=0
c         do ibond=1,nbonds
c           i1=ijbond(1,ibond)
c           i2=ijbond(2,ibond)
c           i3=0
c           if(i1.eq.ib) then
c             i3=i2
c           elseif(i2.eq.ib) then
c             i3=i1
c           endif
c           if(i3.ne.0) then
c             if(itype(i3).eq.ip) then
c               iconn(1)=i3
c               nconn=nconn+1
c             elseif(itype(i3).eq.jp) then
c               iconn(2)=i3
c               nconn=nconn+1
c             elseif(itype(i3).eq.kp) then
c               iconn(3)=i3
c               nconn=nconn+1
c             elseif(itype(i3).eq.lp) then
c               iconn(4)=i3
c               nconn=nconn+1
c             endif
c           endif
c         enddo
cccccccccccccccccccc
c
c           assume this line of the ff file describes an improper torsion and look
c           for the pattern of an (ip,jp,kp,lp) in each repeat unit of the right type
            istart=1
            jstart=1
            kstart=1
            lstart=1
            ifrst=0
            ic=0
c           starting at start, find next set of sites with the correct types
149         ib=0
            do j=istart,nsites
              if(itype(j).eq.ip) then
                ib=j
                istart=j+1
                go to 150 
              endif
            enddo
150         jb=0
            do j=jstart,nsites
              if(itype(j).eq.jp) then
                jb=j
                jstart=j+1
                go to 151 
              endif
            enddo
151         kb=0
            do j=kstart,nsites
              if(itype(j).eq.kp) then
                kb=j
                kstart=j+1
                go to 152 
              endif
            enddo
152         lb=0
            do j=lstart,nsites
              if(itype(j).eq.lp) then
                lb=j
                lstart=j+1
                go to 153 
              endif
            enddo
153         continue
c
c           we get here and all of ib,jb,kb and lb are nonzero if this
c           is an improper torsion.  But if any of them are zero, we have
c           the situation of a site in the ff file and in the map but that 
c           is not needed, like a third proton on a methyl group that is absent
c
c           if(ib.eq.0) go to 160
c           if they are ALL zero, we are done looking
            if(ib+jb+kb+lb.eq.0) go to 160
c           if any are zero, this is an absent site, but keep looking
            if(ib*jb*kb*lb.eq.0) go to 149
c           verify that the sites are in the same repeat unit, since linkage
c           impropers are handled elsewhere
            call parser(atname(ib),elfield,lel,i1,i2,i3)
            call parser(atname(jb),elfield,lel,i1,i2,j3)
            call parser(atname(kb),elfield,lel,i1,i2,k3)
            call parser(atname(lb),elfield,lel,i1,i2,l3)
            if(i3.eq.j3 .and. i3.eq.k3 .and. i3.eq.l3) then
              write(*,'('' Improp found: '',i5,2x,a,2x,  i5,2x,a,2x,  
     x                                      i5,2x,a,2x,  i5,2x,a)')
     x        ib,atname(ib)(1:10),jb,atname(jb)(1:10),
     x        kb,atname(kb)(1:10),lb,atname(lb)(1:10)
c             add this one to the end of the list, since it will be after the ones in topology
              if(nadded+1+ndihed.gt.ndihmx) then
                 write(*,'('' Increase storage for dihedral arrays''
     x                 )')
                 stop 'Exceeded storage for dihedral arrays '
              endif
              ic=ic+1
              nadded=nadded+1
              if(ifrst.eq.0) ifrst=ndihed+nadded
              ijkldih(1,ndihed+nadded)=ib
              ijkldih(2,ndihed+nadded)=jb    
              ijkldih(3,ndihed+nadded)=kb    
              ijkldih(4,ndihed+nadded)=lb   
              ijkldih(5,ndihed+nadded)=1    
              ijkldih(6,ndihed+nadded)=nditp+1
              if(ib.eq.0.or.jb.eq.0.or.kb.eq.0.or.lb.eq.0) then
                 write(*,'(''Dih added problems site 2 '')')
              endif
              do k=1,nparm
                dihpar(k,ndihed+nadded)=parm(k)
              enddo
           
            endif   !end of if-block for four sites being in same repeat unit


            go to 149


160         continue
            if(ic.gt.0) then 
              write(*,'('' Torsion parameter used '',
     x        i6,'' times from line '',i5,'' of file '',i2,2x,a)')
     x        ic,mline,i,mfiles(i)(1:index(mfiles(i),' ')-1)
              nditp=nditp+1
              if(nditp.gt.nditpmx) then
                write(*,'('' Error:  Exceeded array size for '',
     x            ''storage of unique dihedral types (lmdtype)'')')
                stop 'Error:  exceeded array size for lmdtype'
              endif
              lmdtype(1,nditp)=ic
              lmdtype(2,nditp)=ifrst
              lmdtype(3,nditp)=i
              lmdtype(4,nditp)=mline
            endif

          endif !end of ifblock for handling improper torsion


165       continue  !branch to here if site is not in the map


        endif  !end of ifblock for handling torsion_type in the mmol file

        go to 130


135     write(*,'('' Error:  can not open force field file, '',a)')
     x    mfiles(i)
        stop 'Error:  can not open force field file'

140     continue
        close(15)

      enddo   !end of loop over force field files for extracting torsion terms


c-------------------------------------------------------------------------------

c     write(*,'(//,'' Made it to here in torsion handling'')')
c     write(*,'('' About to start processing Link files'')')

c
c     process linkage bonds in the LINK files
c
      do i=nmtypes+1,nmtypes+nlink
        mline=0
        open(15,file=mfiles(i),err=235)
c       ism=index(mtypes(i),' ')-1
c       write(*,'('' Open (Link) files with mtypes = '',a)') 
c    x    mtypes(i)(1:ism)
230     continue
        read(15,'(a120)',end=240) line
c       write(*,'('' Line from link file: '',a,''*'')') 
c    x    line(1:60),line(61:120)
        mline=mline+1
        call ucase(line)
        call leftjust(line)
        if(line(1:13).eq.'TORSION_TYPE_') then
          read(line(14:),*) itortype
          is=index(line,' ')+1
          call leftjust(line(is:))  !is should point to next position after the first space
          isi=index(line(is:),'-')
          iname=line(is:is+isi-2)
          isim=index(iname,'_')      !separates monomer name from site name
          call leftjust(line(is+isi:))
          isj=index(line(is+isi:),'-')
          jname=line(is+isi:is+isi+isj-2)
          isjm=index(jname,'_')      !separates monomer name from site name
          call leftjust(line(is+isi+isj:))
          isk=index(line(is+isi+isj:),'-')
          kname=line(is+isi+isj:is+isi+isj+isk-2)
          iskm=index(kname,'_')      !separates monomer name from site name
          call leftjust(line(is+isi+isj+isk:))
          isl=index(line(is+isi+isj+isk:),' ')
          lname=line(is+isi+isj+isk:is+isi+isj+isk+isl-2)
          islm=index(lname,'_')      !separates monomer name from site name

c         note that each torsion type has a different number of parameters
c         best to find out how many values there are and read them all in
          inext=is+isi+isj+isk+isl
          do j=1,6
            parm(j)=0.d0
          enddo
          nparm=2
231       call leftjust(line(inext:))
          if(line(inext:inext).eq.' '.or.line(inext:inext).eq.'%'
     x       .or.line(inext:inext).eq.'!')  
     x      go to 232
          nparm=nparm+1
          read(line(inext:),*) parm(nparm)
          inext=inext+index(line(inext:),' ')
          go to 231
232       continue
          parm(1)=itortype
          parm(2)=nparm
          write(*,'('' Torsion parameters from ff file:'',
     x     a,2x,a,2x,a,2x,a,'' Dihparms '',
     x     6(2x,i2,2x,f10.5))')
     x     iname(1:isi-1),jname(1:isj-1),
     x     kname(1:isk-1),lname(1:isl-1),
     x     (k,parm(k),k=1,nparm)

          ip=0
          jp=0
          kp=0
          lp=0
          do j=1,nmap
            isp=index(map(1,j),'_')-1
            is2=index(map(2,j),' ')-1
            if(ip.eq.0. and. 
     x         iname(1:isim-1) .eq.map(1,j)(1:isp) .and.
     x         iname(isim+1:isi-1).eq.map(2,j)(1:is2)) ip=j  
            if(jp.eq.0. and. 
     x         jname(1:isjm-1) .eq.map(1,j)(1:isp) .and.
     x         jname(isjm+1:isj-1).eq.map(2,j)(1:is2)) jp=j  
            if(kp.eq.0. and. 
     x         kname(1:iskm-1) .eq.map(1,j)(1:isp) .and.
     x         kname(iskm+1:isk-1).eq.map(2,j)(1:is2)) kp=j  
            if(lp.eq.0. and. 
     x         lname(1:islm-1) .eq.map(1,j)(1:isp) .and.
     x         lname(islm+1:isl-1).eq.map(2,j)(1:is2)) lp=j  
          enddo

          if(ip.eq.0 .or. jp.eq.0 .or. kp.eq.0 .or. lp.eq.0 ) then
            if(ip.eq.0) write(*,'('' Warning:  torsion term in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//iname(1:isim-1)//'* *'//iname(isim+1:isi-1)
            if(jp.eq.0) write(*,'('' Warning:  torsion term in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//jname(1:isjm-1)//'* *'//jname(isjm+1:isj-1)
            if(kp.eq.0) write(*,'('' Warning:  torsion term in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//kname(1:iskm-1)//'* *'//kname(iskm+1:isk-1)
            if(lp.eq.0) write(*,'('' Warning:  torsion term in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//lname(1:islm-1)//'* *'//lname(islm+1:isl-1)
c           do j=1,nmap
c             write(*,'('' Map ('',i2,'') line 1: *'',a,''*''/,
c    x                  '' Map ('',i2,'') line 2: *'',a,''*'')') 
c    x         j,map(1,j)(1:isp), j,map(2,j)(1:is2)
c           enddo
c           stop 'Site not found in the map'
            go to 265
          endif

          if(lp.lt.ip) then
            iswap=lp
            lp=ip
            ip=iswap
            iswap=kp
            kp=jp
            jp=iswap
          elseif(lp.eq.ip) then
            if(kp.lt.jp) then
              iswap=kp
              kp=jp
              jp=iswap
            endif
          endif
c         expect to find all linkage torsions in the topology file (note that some will need multiple terms)
c         iprint=0
          ifrst=0
          ic=0
          do j=1,ndihed
            ib=ijkldih(1,j)
            jb=ijkldih(2,j)
            kb=ijkldih(3,j)
            lb=ijkldih(4,j)
            it=itype(ib)
            jt=itype(jb)
            kt=itype(kb)
            lt=itype(lb)
            if(lt.lt.it) then
              iswap=lt
              lt=it
              it=iswap
              iswap=kt
              kt=jt
              jt=iswap
            elseif(lt.eq.it) then
              if(kt.lt.jt) then
                iswap=kt
                kt=jt
                jt=iswap
              endif
            endif

            if(it.eq.ip .and. jt.eq.jp .and. 
     x         kt.eq.kp .and. lt.eq.lp) then
c             write(*,'('' found match with ff term and torsion '',
c    x             i5,'' out of '',i5)') j,ndihed
c             note that these will be in different repeat units for linkages
c             check furthermore that these are in different repeat unit...
c             ...otherwise they should not be handled as a linkage bond
              call parser(atname(ib),elfield,lel,i1,i2,i3)
              call parser(atname(jb),elfield,lel,i1,i2,j3)
              call parser(atname(kb),elfield,lel,i1,i2,k3)
              call parser(atname(lb),elfield,lel,i1,i2,l3)
              if(i3.ne.j3 .or. i3.ne.k3 .or. i3.ne.l3 .or.
     x           j3.ne.k3 .or. j3.ne.l3 .or. k3.ne.l3) then
                ic=ic+1
                if(ijkldih(5,j).ne.0) then
c                 here if this is a second (or more) torsion term associated with previously assigned ijkl
c                 in this case, need to append to the list; make sure not to exceed ndihmx
c                 make ijkldih(5,*) point to the next in the chain
                  if(nadded+1+ndihed.gt.ndihmx) then
                     write(*,'('' Increase storage for dihedral arrays''
     x                     )')
                     stop 'Exceeded storage for dihedral arrays '
                  endif
                  nadded=nadded+1
                  if(ifrst.eq.0) ifrst=ndihed+nadded
                  isave=ijkldih(5,j)          !this points to location of the last one added
c                 write(*,'('' Inserting torsion at '',i3)') ndihed+nadded
c                 write(*,'('' ijkl='',4i3)') ib,jb,kb,lb
c                 write(*,'('' dihe='',8f8.3)') (parm(kk),kk=1,nparm)
                  ijkldih(5,j)=ndihed+nadded  !this points to location of the last one to be added
                  ijkldih(1,ndihed+nadded)=ib
                  ijkldih(2,ndihed+nadded)=jb    
                  ijkldih(3,ndihed+nadded)=kb    
                  ijkldih(4,ndihed+nadded)=lb   
                  ijkldih(5,ndihed+nadded)=isave
                  ijkldih(6,ndihed+nadded)=nditp+1
            if(ib.eq.0.or.jb.eq.0.or.kb.eq.0.or.lb.eq.0) then
               write(*,'(''Dih added problems site 3 '')')
            endif
                  do k=1,nparm
                    dihpar(k,ndihed+nadded)=parm(k)
                  enddo
                else
c                 write(*,'('' Inserting torsion at '',i3)') j
c                 write(*,'('' ijkl='',4i3)') ib,jb,kb,lb
c                 write(*,'('' dihe='',8f8.3)') (parm(kk),kk=1,nparm)
                  if(ifrst.eq.0) ifrst=j
                  ijkldih(5,j)=1      !this points to location of the last one to be added
                  ijkldih(6,j)=nditp+1                         
                  do k=1,nparm
                    dihpar(k,j)=parm(k)
                  enddo
                endif
              endif
            endif !end of ifblock for exact type match between topology torsion and torsion from ff file    
          enddo !end of doloop for checking all members of the torsion list from the topology file
          if(ic.gt.0) then
            write(*,'('' Torsion parameter used '',
     x      i6,'' times from line '',i5,'' of file '',i2,2x,a)')
     x      ic,mline,i,mfiles(i)(1:index(mfiles(i),' ')-1)
            nditp=nditp+1
            if(nditp.gt.nditpmx) then
              write(*,'('' Error:  Exceeded array size for '',
     x          ''storage of unique dihedral types (lmdtype)'')')
              stop 'Error:  exceeded array size for lmdtype'
            endif
            lmdtype(1,nditp)=ic
            lmdtype(2,nditp)=ifrst
            lmdtype(3,nditp)=i
            lmdtype(4,nditp)=mline
          endif

        endif



265     continue  !branch to here if site is not found in the map


        go to 230


235     write(*,'('' Error:  can not open force field file, '',a)')
     x    mfiles(i)
        stop 'Error:  can not open force field file'

240     continue
        close(15)

      enddo   !end of loop over force field files for extracting torsion terms

c-------------------------------------------------------------------------------

      write(*,'('' Start processing Link files for '',
     x ''IMPROPER torsions'')')

c
c     reprocess linkage bonds in the LINK files this time for IMPROPER torsions
c
c     finding improper torsions is difficult since they are not in the topology
c     file.  The are found by looking for a structural pattern in the molecule.
c     The improper torsions it can find are characterized by a central atom
c     connected by bonds to three others, and the improper torsion relates these
c     four sites in some order.  For the improper to span a linkage bond,
c     exactly one of the sites will be in a different repeat unit than the other
c     three.  The central site is indicated by a (+), the site in the other
c     repeat unit is indicated by a (*).  The code loops over all of the sites
c     to find instances of the central site, then looks at the bond list at
c     its neighbors.  If neighbors with the right characteristics are found,
c     the improper is assigned.
c
c
      do i=nmtypes+1,nmtypes+nlink
        mline=0
        open(15,file=mfiles(i),err=335)
c       ism=index(mtypes(i),' ')-1
c       write(*,'('' Open (Link) files with mtypes = '',a)') 
c    x    mtypes(i)(1:ism)
330     continue
        read(15,'(a120)',end=340) line
c       write(*,'('' Line from link file: '',a,''*'')') 
c    x    line(1:60),line(61:120)
        mline=mline+1
        call ucase(line)
        call leftjust(line)
        if(line(1:21).eq.'IMPROPER_TORSION_TYPE') then
          read(line(23:),*) itortype
          is=index(line,' ')+1
          call leftjust(line(is:))  !is should point to next position after the first space
          isi=index(line(is:),'-')  !hyphens separate atom names
          iname=line(is:is+isi-2)
          isim=index(iname,'_')      !separates monomer name from site name

          call leftjust(line(is+isi:))
          isj=index(line(is+isi:),'-')
          jname=line(is+isi:is+isi+isj-2)
          isjm=index(jname,'_')      !separates monomer name from site name

          call leftjust(line(is+isi+isj:))
          isk=index(line(is+isi+isj:),'-')
          kname=line(is+isi+isj:is+isi+isj+isk-2)
          iskm=index(kname,'_')      !separates monomer name from site name

          call leftjust(line(is+isi+isj+isk:))
          isl=index(line(is+isi+isj+isk:),' ')
          lname=line(is+isi+isj+isk:is+isi+isj+isk+isl-2)
          islm=index(lname,'_')      !separates monomer name from site name

c         iname, jname, kname, lname hold atom names, including the monomer identifier
c         in positions iname(1:isi-1), for example.  The delimiting hyphen is not included.
c         the monomer identifier is in position iname(1:isim-1).


c         note that each torsion type has a different number of parameters
c         best to find out how many values there are and read them all in
          inext=is+isi+isj+isk+isl
          do j=1,6
            parm(j)=0.d0
          enddo

          nparm=2
331       call leftjust(line(inext:))
          if(line(inext:inext).eq.' '.or.line(inext:inext).eq.'%'
     x       .or.line(inext:inext).eq.'!')  
     x      go to 332
          nparm=nparm+1
          read(line(inext:),*) parm(nparm)
          inext=inext+index(line(inext:),' ')
          go to 331
332       continue

          parm(1)=itortype
          parm(2)=nparm

          write(*,'('' IMPROPER Torsion parameters from ff file:'',
     x     '' **'',a,''**  **'',a,''**  **'',a,''**  **'',a,
     x     ''**  Dihparms '')')
     x     iname(1:isi-1),jname(1:isj-1),
     x     kname(1:isk-1),lname(1:isl-1)
c         write(*,'('' parm(1):  torsion type:  '',f10.5)') parm(1)
c           write(*,'('' parm(2):  nparm       :  '',f10.5)') parm(2)
c         do kk=3,nparm
c           write(*,'('' parm('',i1,''):              :  '',f10.5)') kk,parm(kk)
c         enddo


c
c         find and remove the asterisks and pluses that are in an IMPROPER linkage line
c
c         following assume an improper that spans repeat units has the following properties
c         1) only one site is in a different repeat unit than the other three; designated with *
c         2) three of the sites are bonded to a fourth, designated with +
c         these must be different sites, of course, but can be anywhere in the i,j,k.l list that
c         describes the torsion
c
          istar=index(iname(1:isi-1),'*')
          jstar=index(jname(1:isj-1),'*')
          kstar=index(kname(1:isk-1),'*')
          lstar=index(lname(1:isl-1),'*')
          iplus=index(iname(1:isi-1),'+')
          jplus=index(jname(1:isj-1),'+')
          kplus=index(kname(1:isk-1),'+')
          lplus=index(lname(1:isl-1),'+')

          isp=istar+iplus
          if(isp.gt.0) then
            string(1:isi-1-isp)=iname(isp+1:isi-1)
            iname(isp:isi-1)=string(1:isi-1-isp)//' '
            isi =isi -1
            isim=isim-1
          endif
          isp=jstar+jplus
          if(isp.gt.0) then
            string(1:isj-1-isp)=jname(isp+1:isj-1)
            jname(isp:isj-1)=string(1:isj-1-isp)//' '
            isj =isj -1
            isjm=isjm-1
          endif
          isp=kstar+kplus
          if(isp.gt.0) then
            string(1:isk-1-isp)=kname(isp+1:isk-1)
            kname(isp:isk-1)=string(1:isk-1-isp)//' '
            isk =isk -1
            iskm=iskm-1
          endif
          isp=lstar+lplus
          if(isp.gt.0) then
            string(1:isl-1-isp)=lname(isp+1:isl-1)
            lname(isp:isl-1)=string(1:isl-1-isp)//' '
            isl =isl -1
            islm=islm-1
          endif

          istar=min(1,istar)
          jstar=min(1,jstar)
          kstar=min(1,kstar)
          lstar=min(1,lstar)
          iplus=min(1,iplus)                       
          jplus=min(1,jplus)
          kplus=min(1,kplus)
          lplus=min(1,lplus)

          if(istar+jstar+kstar+lstar.ne.1 .or.
     x       iplus+jplus+kplus+lplus.ne.1) then
            write(*,'(''Error in input for improper linkage torsion'')')
            write(*,'(''Should be only one * and one + in input'')')
            stop 'error in improper linkage torsion'
          endif

          istptr=istar+2*jstar+3*kstar+4*lstar   !=1 if in i-position; 2 if in j-position; 3 if in k-position
          iplptr=iplus+2*jplus+3*kplus+4*lplus   !=1 if in i-position; 2 if in j-position; 3 if in k-position

c         write(*,'('' istar,jstar,kstar,lstar:  '',4i5)')
c    x      istar,jstar,kstar,lstar
c         write(*,'('' star pointer is        :  '', i5)')
c    x      istptr                   
c         write(*,'('' iplus,jplus,kplus,lplus:  '',4i5)')
c    x      iplus,jplus,kplus,lplus
c         write(*,'('' plus pointer is        :  '', i5)')
c    x      iplptr                   

          ip=0
          jp=0
          kp=0
          lp=0
          do j=1,nmap
            isp=index(map(1,j),'_')-1   !residue name in map(1,j)(1:isp)
            is2=index(map(2,j),' ')-1   !ff atom name in map(2,j)(1:is2)
            if(ip.eq.0. and. 
     x         iname(1:isim-1) .eq.   map(1,j)(1:isp) .and.    !match residue name
     x         iname(isim+1:isi-1).eq.map(2,j)(1:is2)) ip=j    !match atom name
            if(jp.eq.0. and. 
     x         jname(1:isjm-1) .eq.   map(1,j)(1:isp) .and.    !match residue name
     x         jname(isjm+1:isj-1).eq.map(2,j)(1:is2)) jp=j    !match atom name
            if(kp.eq.0. and. 
     x         kname(1:iskm-1) .eq.   map(1,j)(1:isp) .and.    !match residue name
     x         kname(iskm+1:isk-1).eq.map(2,j)(1:is2)) kp=j    !match atom name
            if(lp.eq.0. and. 
     x         lname(1:islm-1) .eq.   map(1,j)(1:isp) .and.    !match residue name
     x         lname(islm+1:isl-1).eq.map(2,j)(1:is2)) lp=j    !match atom name
          enddo


          if(ip.eq.0 .or. jp.eq.0 .or. kp.eq.0 .or. lp.eq.0 ) then
            if(ip.eq.0) write(*,'('' Warning:  torsion term in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//iname(1:isim-1)//'* *'//iname(isim+1:isi-1)
            if(jp.eq.0) write(*,'('' Warning:  torsion term in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//jname(1:isjm-1)//'* *'//jname(isjm+1:isj-1)
            if(kp.eq.0) write(*,'('' Warning:  torsion term in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//kname(1:iskm-1)//'* *'//kname(iskm+1:isk-1)
            if(lp.eq.0) write(*,'('' Warning:  torsion term in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//lname(1:islm-1)//'* *'//lname(islm+1:isl-1)
c           do j=1,nmap
c             write(*,'('' Map ('',i2,'') line 1: *'',a,''*''/,
c    x                  '' Map ('',i2,'') line 2: *'',a,''*'')') 
c    x         j,map(1,j)(1:isp), j,map(2,j)(1:is2)
c           enddo
c           stop 'Site not found in the map'
            go to 365
          endif

c         this is an improper torsion that should span two adjacent monomeric units                                 
c         since it is in the linkage file.  I don't see that it is possible for
c         an improper torsion as defined to span three monomeric units.
c         two monomeric units are adjacent if they have a bond connecting them
c         this bond should be between the tagged sites (*) and the central sites (+)            


          if(istar.eq.1) then
            istartp=ip
          elseif(jstar.eq.1) then
            istartp=jp
          elseif(kstar.eq.1) then
            istartp=kp
          elseif(lstar.eq.1) then
            istartp=lp
          endif

          if(iplus.eq.1) then
            iplustp=ip
          elseif(jplus.eq.1) then
            iplustp=jp
          elseif(kplus.eq.1) then
            iplustp=kp
          elseif(lplus.eq.1) then
            iplustp=lp
          endif






c         we sweep through the atom list to find sites that have these properties
c         this is like what is done for the impropers within monomeric units

          ifrst=0   !what is this for?  first occurence for this torsion type?
          ic=0      !counter for number of uses of this improper type?


          istart=1
349       ib=0
c         starting at istart, find next site whose type matches that of the central one
          do j=istart,nsites
            if(itype(j).eq.iplustp) then
              ib=j
              istart=j+1
              go to 350 
            endif
          enddo
          if(ib.eq.0) go to 360

350       continue   !here when next central site is found
c         write(*,'(/,'' Seeking connections to central site '',i8,2x,
c    x      a20)') ib,atname(ib)(1:20)

          iconn(iplptr)=ib  !save index of central site at right location in iconn

c         scan bond list to get sites this one is connected to
          nconn=0
          do ibond=1,nbonds
            i1=ijbond(1,ibond)
            i2=ijbond(2,ibond)
            i3=0
            if(i1.eq.ib) then
              i3=i2
            elseif(i2.eq.ib) then
              i3=i1
            endif
            if(i3.ne.0) then
              if(itype(i3).eq.ip) then
                iconn(1)=i3
                nconn=nconn+1
              elseif(itype(i3).eq.jp) then
                iconn(2)=i3
                nconn=nconn+1
              elseif(itype(i3).eq.kp) then
                iconn(3)=i3
                nconn=nconn+1
              elseif(itype(i3).eq.lp) then
                iconn(4)=i3
                nconn=nconn+1
              endif
            endif
          enddo


c         write(*,'('' Found '',i3,
c    x      '' connections to this site of correct type.'')')  
c    x      nconn

          if(nconn.ne.3) go to 349

c         write(*,'('' IMPROPER found: **'',a,''** **'',a,
c    x                            ''** **'',a,''** **'',a,''**'')')
c    x      (atname(iconn(kk))(1:20),kk=1,4)

c         now need to verify that all sites are in the same repeat unit except for the * site and it is in a different one


          call parser(atname(ib),string,lel,i1,i2,i3)            !i3 is the repeat unit index of the plus site
          call parser(atname(iconn(istptr)),string,lel,i1,i2,i4) !i3 is the repeat unit index of the star site
c         check that star and plus sites are on different repeat units
c         following check should never pass and should not be necessary
          if(i3.eq.i4) then
            write(*,'('' Error condition:  Star and Plus sites '',
     x        ''are on the same repeat unit. SKIP this torsion'')')
            go to 349
          endif
c         verify that the other two sites are on the same repeat unit as the plus site
          do j=1,4
            if(j.ne.istptr.and.j.ne.iplptr) then
              call parser(atname(iconn(j)),string,lel,i1,i2,i5) !i5 is the repeat unit index of this site
              if(i5.ne.i3) then
                write(*,'('' Error condition:  Plus site '',
     x             ''connected to too many other repeat units.'',
     x             ''  SKIP this torsion.'')')
                go to 349 
              endif
            endif
          enddo

c
c         here when a valid torsion has been found; add it to the list
c

          write(*,'('' IMPROPER found: **'',a,''** **'',a,
     x                            ''** **'',a,''** **'',a,''**'')')
     x      (atname(iconn(kk))(1:20),kk=1,4)


c         add this one to the end of the list
          if(nadded+1+ndihed.gt.ndihmx) then
             write(*,'('' Increase storage for dihedral arrays''
     x             )')
             stop 'Exceeded storage for dihedral arrays '
          endif
          ic=ic+1
          nadded=nadded+1
          if(ifrst.eq.0) ifrst=ndihed+nadded
          ijkldih(1,ndihed+nadded)=iconn(1)
          ijkldih(2,ndihed+nadded)=iconn(2)
          ijkldih(3,ndihed+nadded)=iconn(3)
          ijkldih(4,ndihed+nadded)=iconn(4)
          ijkldih(5,ndihed+nadded)=1    
          ijkldih(6,ndihed+nadded)=nditp+1
          do k=1,nparm
            dihpar(k,ndihed+nadded)=parm(k)
          enddo
            if(iconn(1).eq.0.or.iconn(2).eq.0.or.
     x         iconn(3).eq.0.or.iconn(4).eq.0) then
               write(*,'(''Dih added problems site 4 '')')
            endif
           

          go to 349

360       continue


          if(ic.gt.0) then 
            write(*,'('' Torsion parameter used '',
     x      i6,'' times from line '',i5,'' of file '',i2,2x,a)')
     x      ic,mline,i,mfiles(i)(1:index(mfiles(i),' ')-1)
            nditp=nditp+1
            if(nditp.gt.nditpmx) then
              write(*,'('' Error:  Exceeded array size for '',
     x          ''storage of unique dihedral types (lmdtype)'')')
              stop 'Error:  exceeded array size for lmdtype'
            endif
            lmdtype(1,nditp)=ic
            lmdtype(2,nditp)=ifrst
            lmdtype(3,nditp)=i
            lmdtype(4,nditp)=mline
          endif


        endif   !end of ifblock for processing an IMPROPER_TORSION_TYPE line of the ff file 




365     continue  !branch to here if site is not found in the map meaning we skip this line

        go to 330 !go to beginning to process next line of this ff file

335     write(*,'('' Error:  can not open force field file, '',a)')
     x    mfiles(i)
        stop 'Error:  can not open force field file'

340     continue  !branch to here if end of this Link file has been reached; close and go to next one
        close(15)

      enddo   !end of loop over force field files for extracting torsion terms

c-------------------------------------------------------------------------------



c     write(*,'(//,''DONE IN GETDIHED '')')

      end


c
c     subroutine to get angle parameters
c
      subroutine getangles(mfiles,mtypes,nmtypes,nlink,map,nmap,
     x      atname,itype,ijkangle,anglepar,nangles,nsites,
     x      lmantype,nantpmx,nantp)
      implicit real*8 (a-h,o-z)
      dimension ijkangle(5,nangles),anglepar(2,nangles),itype(nsites)
      character*(*) mfiles(nmtypes),mtypes(nmtypes),map(2,nmap)
      character*(*) atname(nsites)
      character*120 line,string
      character*40 iname,jname,kname,elfield

      dimension lmantype(4,nantpmx)

      nantp=0   !counter for unique types of angle terms (for lammps)

      do i=1,nangles
        anglepar(1,i)=0.d0
        anglepar(2,i)=0.d0
      enddo

c
c     loop over the force field files, extracting angle terms
c
      do i=1,nmtypes
        mline=0
        open(15,file=mfiles(i),err=135)
        ism=index(mtypes(i),' ')-1
c       write(*,'('' Open files with mtypes = '',a)') mtypes(i)(1:ism)
130     continue
        read(15,'(a120)',end=140) line
        mline=mline+1
        call ucase(line)
        call leftjust(line)
        if(line(1:14).eq.'HARMONIC_ANGLE ') then
          call leftjust(line(16:))
          isi=index(line(16:),'-')
          iname=line(16:14+isi)
          call leftjust(line(16+isi:))
          isj=index(line(16+isi:),'-')
          jname=line(16+isi:14+isi+isj)
          call leftjust(line(16+isi+isj:))
          isk=index(line(16+isi+isj:),' ')
          kname=line(16+isi+isj:14+isi+isj+isk)
          read(line(15+isi+isj+isk:),*) angk,angl
c         write(*,'('' Angle parameters from ff file:'',
c    x     a,2x,a,2x,a,2x,2f10.5)')
c    x     iname(1:isi-1),jname(1:isj-1),kname(1:isk-1),angk,angl
          ip=0
          jp=0
          kp=0
          do j=1,nmap
            isp=index(map(1,j),'_')-1
            is2=index(map(2,j),' ')-1
            if(ip.eq.0. and. 
     x         mtypes(i)(1:ism).eq.map(1,j)(1:isp) .and.
     x         iname(1:isi-1).eq.map(2,j)(1:is2)) ip=j  
            if(jp.eq.0. and. 
     x         mtypes(i)(1:ism).eq.map(1,j)(1:isp) .and.
     x         jname(1:isj-1).eq.map(2,j)(1:is2)) jp=j  
            if(kp.eq.0. and. 
     x         mtypes(i)(1:ism).eq.map(1,j)(1:isp) .and.
     x         kname(1:isk-1).eq.map(2,j)(1:is2))kp=j  
          enddo
132       continue
          if(ip.eq.0 .or. jp.eq.0 .or. kp.eq.0) then
            write(*,'('' Warning:  angle term in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//iname(1:isi-1)//'* *'//jname(1:isj-1)
     x                           //'* *'//kname(1:isk-1)//'*'
c           stop 'One or more angle sites not found in the map'
          else
            if(kp.lt.ip) then
              iswap=kp
              kp=ip
              ip=iswap
            endif
            ifrst=0
            ic=0
            do j=1,nangles
              ib=ijkangle(1,j)
              jb=ijkangle(2,j)
              kb=ijkangle(3,j)
              it=itype(ib)
              jt=itype(jb)
              kt=itype(kb)
c             write(*,'('' Checking for match with bond j'',i3,
c    x          '' Site index '',2i5,'' Types '',2i5)')
c    x          j,ib,jb,it,jt  
              if(kt.lt.it) then
                iswap=it
                it=kt
                kt=iswap
              endif
              if(it.eq.ip .and. jt.eq.jp .and. kt.eq.kp) then
c               check furthermore that these are in the same repeat unit...
c               ...otherwise they should be handled as a linkage bond
                call parser(atname(ib),elfield,lel,i1,i2,i3)
                call parser(atname(jb),elfield,lel,i1,i2,j3)
                call parser(atname(kb),elfield,lel,i1,i2,k3)
                if(i3.eq.j3 .and. i3.eq.k3) then
                  if(ijkangle(4,j).ne.0) then
                    write(*,'('' Error:  Trying to assign angle term'',
     x                '' that is already assigned; angle '',i5)') j
                    write(*,'('' Angle term is '',a,1x,a,1x,a)')
     x                atname(ib),atname(jb),atname(kb)
                    stop 'Multiple assignment of angle term'
                  endif
                  if(ifrst.eq.0) ifrst=j
                  ic=ic+1
                  anglepar(1,j)=angk
                  anglepar(2,j)=angl
                  ijkangle(4,j)=1  !indicate this bond has been set
c                 write(*,'('' Matching bond '',i4,'' sites '',2i5,
c    x              '' types '',2i5)') j,ib,jb,it,jt
                  ijkangle(5,j)=nantp+1
                endif
              endif
            enddo
            if(ic.gt.0) then
              write(*,'('' Angle parameter used '',
     x        i6,'' times from line '',i5,'' of file '',i2,2x,a)')
     x        ic,mline,i,mfiles(i)(1:index(mfiles(i),' ')-1)
              nantp=nantp+1
              if(nantp.gt.nantpmx) then
                write(*,'('' Error:  Exceeded array size for '',
     x            ''storage of unique angle types (lmantype)'')')
                stop 'Error:  exceeded array size for lmantype'
              endif
              lmantype(1,nantp)=ic
              lmantype(2,nantp)=ifrst
              lmantype(3,nantp)=i
              lmantype(4,nantp)=mline
            endif
          endif

        endif
        go to 130

135     write(*,'('' Error:  can not open force field file, '',a)')
     x    mfiles(i)
        stop 'Error:  can not open force field file'

140     continue
        close(15)

      enddo   !end of loop over force field files for extracting angles


c
c     process linkage bonds in the LINK files
c
      do i=nmtypes+1,nmtypes+nlink
        mline=0
        open(15,file=mfiles(i),err=235)
        ism=index(mtypes(i),' ')-1
        write(*,'('' Open files with mtypes = '',a)') mtypes(i)(1:ism)
230     continue
        read(15,'(a120)',end=240) line
        mline=mline+1
        call ucase(line)
        call leftjust(line)
        if(line(1:14).eq.'HARMONIC_ANGLE ') then
          call leftjust(line(16:))
          isi=index(line(16:),'-')
          iname=line(16:14+isi)
          isim=index(iname,'_')      !separates monomer name from site name
          call leftjust(line(16+isi:))
          isj=index(line(16+isi:),'-')
          jname=line(16+isi:14+isi+isj)
          isjm=index(jname,'_')      !separates monomer name from site name
          call leftjust(line(16+isi+isj:))
          isk=index(line(16+isi+isj:),' ')
          kname=line(16+isi+isj:14+isi+isj+isk)
          iskm=index(kname,'_')      !separates monomer name from site name

          read(line(15+isi+isj+isk:),*) angk,angl
          write(*,'('' Angle parameters from ff file:'',
     x     a,2x,a,2x,a,2x,2f10.5)')
     x     iname(1:isi-1),jname(1:isj-1),kname(1:isk-1),angk,angl

          ip=0
          jp=0
          kp=0
          do j=1,nmap
            isp=index(map(1,j),'_')-1
            is2=index(map(2,j),' ')-1
            if(ip.eq.0. and. 
     x         iname(1:isim-1)    .eq.map(1,j)(1:isp) .and.
     x         iname(isim+1:isi-1).eq.map(2,j)(1:is2)) ip=j  
            if(jp.eq.0. and. 
     x         jname(1:isjm-1)    .eq.map(1,j)(1:isp) .and.
     x         jname(isjm+1:isj-1).eq.map(2,j)(1:is2)) jp=j  
            if(kp.eq.0. and. 
     x         kname(1:iskm-1)    .eq.map(1,j)(1:isp) .and.
     x         kname(iskm+1:isk-1).eq.map(2,j)(1:is2)) kp=j  
          enddo

232       continue
          if(ip.eq.0 .or. jp.eq.0 .or. kp.eq.0) then
            write(*,'('' Warning:  angle term in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//iname(1:isi-1)//'* *'//jname(1:isj-1)
     x                           //'* *'//kname(1:isk-1)//'*'
c           stop 'One or more angle sites not found in the map'
          else
            if(kp.lt.ip) then
              iswap=kp
              kp=ip
              ip=iswap
            endif
            ifrst=0
            ic=0
            do j=1,nangles
              ib=ijkangle(1,j)
              jb=ijkangle(2,j)
              kb=ijkangle(3,j)
              it=itype(ib)
              jt=itype(jb)
              kt=itype(kb)
c             write(*,'('' Checking for match with bond j'',i3,
c    x          '' Site index '',2i5,'' Types '',2i5)')
c    x          j,ib,jb,it,jt  
              if(kt.lt.it) then
                iswap=it
                it=kt
                kt=iswap
              endif
              if(it.eq.ip .and. jt.eq.jp .and. kt.eq.kp) then
c               check furthermore that these are NOT in the same repeat unit...
c               ...otherwise they should have been handled above            
                call parser(atname(ib),elfield,lel,i1,i2,i3)
                call parser(atname(jb),elfield,lel,i1,i2,j3)
                call parser(atname(kb),elfield,lel,i1,i2,k3)
                if(i3.ne.j3 .or. i3.ne.k3) then
                  if(ijkangle(4,j).ne.0) then
                    write(*,'('' Error:  Trying to assign angle term'',
     x                '' that is already assigned; angle '',i5)') j
                    write(*,'('' Angle term is '',a,1x,a,1x,a)')
     x                atname(ib),atname(jb),atname(kb)
                    stop 'Multiple assignment of angle term'
                  endif
                  if(ifrst.eq.0) ifrst=j
                  ic=ic+1
                  anglepar(1,j)=angk
                  anglepar(2,j)=angl
                  ijkangle(4,j)=1  !indicate this bond has been set
c                 write(*,'('' Matching bond '',i4,'' sites '',2i5,
c    x              '' types '',2i5)') j,ib,jb,it,jt
                  ijkangle(5,j)=nantp+1
                endif
              endif
            enddo
            if(ic.gt.0) then
              write(*,'('' Angle parameter used '',
     x        i6,'' times from line '',i5,'' of file '',i2,2x,a)')
     x        ic,mline,i,mfiles(i)(1:index(mfiles(i),' ')-1)
              nantp=nantp+1
              if(nantp.gt.nantpmx) then
                write(*,'('' Error:  Exceeded array size for '',
     x            ''storage of unique angle types (lmantype)'')')
                stop 'Error:  exceeded array size for lmantype'
              endif
              lmantype(1,nantp)=ic
              lmantype(2,nantp)=ifrst
              lmantype(3,nantp)=i
              lmantype(4,nantp)=mline
            endif
          endif

        endif
        go to 230


235     write(*,'('' Error:  can not open force field file, '',a)')
     x    mfiles(i)
        stop 'Error:  can not open force field file'

240     continue
        close(15)

      enddo   !end of loop over force field files for extracting angles



      end

c
c     subroutine to get bond parameters
c
      subroutine getbonds(mfiles,mtypes,nmtypes,nlink,map,nmap,
     x      atname,itype,ijbond,bondpar,nbonds,nsites,
     x      lmbtype,nbntpmx,nbntp)
      implicit real*8 (a-h,o-z)
      dimension ijbond(5,nbonds),bondpar(2,nbonds),itype(nsites)
      character*(*) mfiles(nmtypes),mtypes(nmtypes),map(2,nmap)
      character*(*) atname(nsites)
      character*120 line,string
      character*40 iname,jname,elfield
      dimension lmbtype(4,nbntpmx)   !for lammps:  for each obs. bond type: number observed, first instance


      do i=1,nbonds
        bondpar(1,i)=0.d0
        bondpar(2,i)=0.d0
      enddo

      nbntp=0  !counts number of unique bond types (for lammps)

c
c     loop over the force field files, extracting bond terms
c
      do i=1,nmtypes
        mline=0
        open(15,file=mfiles(i),err=135)
        ism=index(mtypes(i),' ')-1
c       write(*,'('' Open files with mtypes = '',a)') mtypes(i)(1:ism)
130     continue
        read(15,'(a120)',end=140) line
        mline=mline+1
        call ucase(line)
        call leftjust(line)
        if(line(1:14).eq.'HARMONIC_BOND ') then
          call leftjust(line(15:))
          is=index(line(15:),'-')
          iname=line(15:13+is)
          call leftjust(line(15+is:))
          isj=index(line(15+is:),' ')
          jname=line(15+is:13+is+isj)
          read(line(14+is+isj:),*) bndk,bndl
c         write(*,'('' Bonded parameters from ff file:'',
c    x     a,2x,a,2x,2f10.5)')
c    x     iname(1:is-1),jname(1:isj-1),bndk,bndl
          k=0
          l=0
          do j=1,nmap
            isp=index(map(1,j),'_')-1
            is2=index(map(2,j),' ')-1
            if(k.eq.0. and. 
     x         mtypes(i)(1:ism).eq.map(1,j)(1:isp) .and.
     x         iname(1:is-1).eq.map(2,j)(1:is2)) k=j  
            if(l.eq.0. and. 
     x         mtypes(i)(1:ism).eq.map(1,j)(1:isp) .and.
     x         jname(1:isj-1).eq.map(2,j)(1:is2)) l=j  
          enddo
132       continue
          if(k.eq.0. or. l.eq.0) then
            write(*,'('' Warning:  bond term in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//iname(1:is-1)//'* or *'//jname(1:isj-1)//'*'
c           stop 'One or more bond sites not found in the map'
          else
c           write(*,'('' match found for sites; mtype='',a,
c    x        '' map1='',a,'' map2='',a,'' k='',i3)')
c    x        mtypes(i)(1:ism),map(1,k)(1:10),map(2,k)(1:10),k
c           write(*,'('' match found for sites; mtype='',a,
c    x        '' map1='',a,'' map2='',a,'' l='',i3)')
c    x        mtypes(i)(1:ism),map(1,l)(1:10),map(2,l)(1:10),l
            if(l.lt.k) then
              iswap=k
              k=l
              l=iswap
            endif
            ic=0
            ifrst=0
            do j=1,nbonds
              ib=ijbond(1,j)
              jb=ijbond(2,j)
              it=itype(ib)
              jt=itype(jb)
c             write(*,'('' Checking for match with bond j'',i3,
c    x          '' Site index '',2i5,'' Types '',2i5)')
c    x          j,ib,jb,it,jt  
              if(jt.lt.it) then
                iswap=it
                it=jt
                jt=iswap
              endif
              if(it.eq.k .and. jt.eq.l) then
c               check furthermore that these are in the same repeat unit...
c               ...otherwise they should be handled as a linkage bond
                call parser(atname(ib),elfield,lel,i1,i2,i3)
                call parser(atname(jb),elfield,lel,i1,i2,j3)
                if(i3.eq.j3) then
                  if(ijbond(3,j).ne.0) then
                    write(*,'('' Error:  Trying to assign bond term'',
     x                '' that is already assigned; bond '',i5)') j
                    stop 'Multiple assignment of bond term'
                  endif
                  if(ifrst.eq.0) ifrst=j !save first instance of use of this ff term
                  ic=ic+1
                  bondpar(1,j)=bndk
                  bondpar(2,j)=bndl
                  ijbond(3,j)=1  !indicate this bond has been set
                  ijbond(5,j)=nbntp+1
                endif
              endif
            enddo !end of loop checking all bonds from the topology file
            if(ic.gt.0) then
              write(*,'('' Bond parameter used '',i6,
     x        '' times from line '',i5,'' of file '',i2,2x,a)')
     x        ic,mline,i,mfiles(i)(1:index(mfiles(i),' ')-1)
              nbntp=nbntp+1
              if(nbntp.gt.nbntpmx) then
                write(*,'('' Error:  Exceeded array size for '',
     x            ''storage of unique bond types (lmbtype)'')')
                stop 'Error:  exceeded array size for lmbtype'
              endif
              lmbtype(1,nbntp)=ic
              lmbtype(2,nbntp)=ifrst
              lmbtype(3,nbntp)=i
              lmbtype(4,nbntp)=mline
            endif
          endif

        endif
        go to 130

135     write(*,'('' Error:  can not open force field file, '',a)')
     x    mfiles(i)
        stop 'Error:  can not open force field file'

140     continue
        close(15)

      enddo   !end of loop over force field files for extracting bonds


c
c     process linkage bonds in the LINK files
c
      write(*,'('' IN GETBONDS nlink = '',i5)') nlink
      do i=nmtypes+1,nmtypes+nlink
        mline=0
        ism=index(mtypes(i),' ')-1
        write(*,'('' Open files with mtypes = '',a)') mtypes(i)(1:ism)
        is=index(mfiles(i),' ')-1
        write(*,'('' The filename is        = '',a)') mfiles(i)(1:is)
        open(15,file=mfiles(i),err=235)
230     continue
        read(15,'(a120)',end=240) line
        mline=mline+1
        call ucase(line)
        call leftjust(line)
        if(line(1:14).eq.'HARMONIC_BOND ') then
          call leftjust(line(15:))
          isi=index(line(15:),'-')   !iname in line(15:13+isi)
          iname=line(15:13+isi)   
          isim=index(iname,'_')      !separates monomer name from site name
          call leftjust(line(15+isi:))
          isj=index(line(15+isi:),' ') !jname in line(15+isi:13+isi+isj)
          jname=line(15+isi:13+isi+isj)
          isjm=index(jname,'_')      !separates monomer name from site name
          read(line(14+isi+isj:),*) bndk,bndl
c         write(*,'('' Bonded parameters from ff file:'',
c    x     a,2x,a,2x,2f10.5)')
c    x     iname(1:is-1),jname(1:isj-1),bndk,bndl

c         from monmoer name, get monomer type; from site name get site type
          k=0
          l=0
          do j=1,nmap
            isp=index(map(1,j),'_')-1  !monomer name in map(1,j)(1:isp)
            is2=index(map(2,j),' ')-1  !site name as known to force field
            if(k.eq.0. and. 
     x         iname(1:isim-1)    .eq.map(1,j)(1:isp) .and.
     x         iname(isim+1:isi-1).eq.map(2,j)(1:is2)) k=j  
            if(l.eq.0. and. 
     x         jname(1:isjm-1)    .eq.map(1,j)(1:isp) .and.
     x         jname(isjm+1:isj-1).eq.map(2,j)(1:is2)) l=j  
          enddo

232       continue
          if(k.eq.0. or. l.eq.0) then
            write(*,'('' Warning:  bond term in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//iname(1:isi-1)//'* or *'//jname(1:isj-1)//'*'
c           stop 'One or more bond sites not found in the map'
          else
c         write(*,'('' match found for sites; mtype='',a,
c    x      '' map1='',a,'' map2='',a,'' k='',i3)')
c    x      mtypes(i)(1:ism),map(1,k)(1:10),map(2,k)(1:10),k
c         write(*,'('' match found for sites; mtype='',a,
c    x      '' map1='',a,'' map2='',a,'' l='',i3)')
c    x      mtypes(i)(1:ism),map(1,l)(1:10),map(2,l)(1:10),l
            if(l.lt.k) then
              iswap=k
              k=l
              l=iswap
            endif
            ic=0
            ifrst=0
            do j=1,nbonds
              ib=ijbond(1,j)
              jb=ijbond(2,j)
              it=itype(ib)
              jt=itype(jb)
c             write(*,'('' Checking for match with bond j'',i3,
c    x          '' Site index '',2i5,'' Types '',2i5)')
c    x          j,ib,jb,it,jt  
              if(jt.lt.it) then
                iswap=it
                it=jt
                jt=iswap
              endif
              if(it.eq.k .and. jt.eq.l) then
c               check furthermore that these are in different repeat units...
c               ...otherwise they should be handled as a linkage bond
                call parser(atname(ib),elfield,lel,i1,i2,i3)
                call parser(atname(jb),elfield,lel,i1,i2,j3)
                if(i3.ne.j3) then
                  if(ijbond(3,j).ne.0) then
                    write(*,'('' Error:  Trying to assign bond term'',
     x                '' that is already assigned; bond '',i5)') j
                    stop 'Multiple assignment of bond term'
                  endif
                  if(ifrst.eq.0) ifrst=j !save first instance of use of this ff term
                  ic=ic+1
                  bondpar(1,j)=bndk
                  bondpar(2,j)=bndl
                  ijbond(3,j)=1  !indicate this bond has been set
                  ijbond(5,j)=nbntp+1
c                 write(*,'('' Matching bond '',i4,'' sites '',2i5,
c    x              '' types '',2i5)') j,ib,jb,it,jt
                endif
              endif
            enddo
            if(ic.gt.0) then
              write(*,'('' Bond parameter used '',i6,
     x        '' times from line '',i5,'' of file '',i2,2x,a)')
     x        ic,mline,i,mfiles(i)(1:index(mfiles(i),' ')-1)
              nbntp=nbntp+1
              if(nbntp.gt.nbntpmx) then
                write(*,'('' Error:  Exceeded array size for '',
     x            ''storage of unique bond types (lmbtype)'')')
                stop 'Error:  exceeded array size for lmbtype'
              endif
              lmbtype(1,nbntp)=ic
              lmbtype(2,nbntp)=ifrst
              lmbtype(3,nbntp)=i
              lmbtype(4,nbntp)=mline
            endif
          endif

        endif
        go to 230

235     write(*,'('' Error:  can not open force field file, '',a)')
     x    mfiles(i)
        stop 'Error:  can not open force field file'

240     continue
        close(15)

      enddo   !end of loop over link force field files for extracting bonds


      end



c
c     subroutine to get charges and vdw parameters
c
      subroutine getchgvdw(mfiles,mtypes,nmtypes,map,nmap,
     x      itype,charge,vdw,nsites)
      implicit real*8 (a-h,o-z)
      dimension charge(nsites),vdw(2,nsites),itype(nsites)
      character*(*) mfiles(nmtypes),mtypes(nmtypes),map(2,nmap)
      character*120 line,string

      do i=1,nsites
        charge(i)=100.d0
      enddo
c
c     loop over the force field files, extracting charges 
c
      do i=1,nmtypes
        mline=0
        open(15,file=mfiles(i),err=135)
        ism=index(mtypes(i),' ')-1
c       write(*,'('' Open files with mtypes = '',a)') mtypes(i)(1:ism)
c       find a charge line
130     continue
        read(15,'(a120)',end=140) line
        mline=mline+1
        call ucase(line)
        call leftjust(line)
        if(line(1:7).eq.'CHARGE ') then
          call leftjust(line(8:))
          is=index(line(8:),' ')
          string=line(8:6+is)
c         based on the mtypes string and the atom name string, find entry in map
c         the token in mtypes must match the first field of map(1,k) and
c         the site name in string must match the map(2,k) for the association to
c         be unique
          k=0
          do j=1,nmap
            isp=index(map(1,j),'_')-1
            is2=index(map(2,j),' ')-1
            if(mtypes(i)(1:ism).eq.map(1,j)(1:isp) .and.
     x         string(1:is-1).eq.map(2,j)(1:is2)) then
              k=j
              go to 132
            endif
          enddo
132       if(k.eq.0) then
            write(*,'('' Warning:  charge site in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//string(1:is-1)//'*'
c           stop 'Site not found in the map'
c         else
c         write(*,'('' match found for site; mtype='',a,
c    x      '' map1='',a,'' map2='',a,'' k='',i3)')
c    x      mtypes(i)(1:ism),map(1,k)(1:isp),map(2,k)(1:is2),k
          else
            read(line(8+is:),*) chg
c           find sites that are to get this charge
c           write(*,'(''Charge for '',a,'' is '',f20.10)') 
c    x        '*'//line(8:6+is)//'*',chg
            ic=0
            do j=1,nsites
              if(itype(j).eq.k) then
                charge(j)=chg
                ic=ic+1
              endif
            enddo
            if(ic.gt.0) write(*,'('' Charge parameter used '',
     x        i6,'' times from line '',i5,'' of file '',i2,2x,a)')
     x        ic,mline,i,mfiles(i)(1:index(mfiles(i),' ')-1)
          endif

        endif
        go to 130

135     write(*,'('' Error:  can not open force field file, '',a)')
     x    mfiles(i)
        stop 'Error:  can not open force field file'

140     continue
        close(15)
      enddo   !end of loop over force field files for extracting charges

c
c     loop over the force field files, extracting vdw parameters
c
      do i=1,nsites
        vdw(1,i)=0.d0
        vdw(2,i)=0.d0
      enddo

      do i=1,nmtypes
        mline=0
        open(15,file=mfiles(i),err=155)
        ism=index(mtypes(i),' ')-1
c       write(*,'('' Open files with mtypes = '',a)') mtypes(i)(1:ism)
150     continue
        read(15,'(a120)',end=160) line
        mline=mline+1
        call ucase(line)
        call leftjust(line)
        if(line(1:19).eq.'LENNARD_JONES_6_12 ') then
          call leftjust(line(20:))
          is=index(line(20:),' ')
          string=line(20:18+is)
          k=0
          do j=1,nmap
            isp=index(map(1,j),'_')-1
            is2=index(map(2,j),' ')-1
            if(mtypes(i)(1:ism).eq.map(1,j)(1:isp) .and.
     x         string(1:is-1).eq.map(2,j)(1:is2)) then
              k=j
              go to 152
            endif
          enddo

152       if(k.eq.0) then
            write(*,'('' Warning:  lj site in ff '',
     x        ''file was not found in the map: '',a)')
     x        '*'//string(1:is-1)//'*'
c           stop 'Site not found in the map'
c         else
c         write(*,'('' match found for site; mtype='',a,
c    x      '' map1='',a,'' map2='',a,'' k='',i3)')
c    x      mtypes(i)(1:ism),map(1,k)(1:isp),map(2,k)(1:is2),k
c           stop 'Site not found in the map'
          else
c           write(*,'('' match found for site; mtype='',a,
c    x        '' map1='',a,'' map2='',a,'' k='',i3)')
c    x        mtypes(i)(1:ism),map(1,k)(1:isp),map(2,k)(1:is2),k
            read(line(19+is:),*) vdw1,vdw2
c           write(*,'('' LJ line found for *'',a,
c    x        ''*'',''  '',f10.5)') line(8:6+is),chg
c           find sites that are to get this charge
            ic=0
            do j=1,nsites
              if(itype(j).eq.k) then
                vdw(1,j)=vdw1
                vdw(2,j)=vdw2
                ic=ic+1
              endif
            enddo
            if(ic.gt.0) write(*,'('' LJ parameter used     '',
     x        i6,'' times from line '',i5,'' of file '',i2,2x,a)')
     x        ic,mline,i,mfiles(i)(1:index(mfiles(i),' ')-1)
          endif


        endif
        go to 150

155     write(*,'('' Error:  can not open force field file, '',a)')
     x    mfiles(i)
        stop 'Error:  can not open force field file'

160     continue
        close(15)
      enddo   !end of loop over force field files for extracting vdw parameters




      end









c
c     subroutine lstchr returns the index of the last nonblank character in a string
c
      subroutine lstchr(string,ilst)
      character*(*) string 

      ilst=0  !if string is all blank, return zero
      do i=len(string),1,-1
        if(string(i:i).ne.' ') then
          ilst=i
          go to 10
        endif
      enddo
10    continue

      end


c
c     subroutine to pack up to four fields into a string
c
      subroutine packer(elfield,i1,i2,i3,string)
      character*(*) elfield,string
      character*120 line
      character*10 num
      dimension is(3)
      line=elfield
      call leftjust(line)
c     assume this field is either one or two characters, look for a space or an underscore
      ne=2 !assume 2 character element name
      if(line(2:2).eq.' '.or.line(2:2).eq.'_') ne=1 !set it to 1 character in these cases
      is(1)=i1
      is(2)=i2
      is(3)=i3
      
      do i=1,3
        if(is(i).ge.0) then
          write(num,'(i10)') is(i)
          ip=0
          do j=1,len(num)
            if(ip.eq.0.and.num(j:j).ne.' ') ip=j 
          enddo
          line(ne+1:ne+len(num)-ip+2)='_'//num(ip:len(num))   !move len(num)-j+1 characters
          ne=ne+len(num)-ip+2
        endif
      enddo

      string=line

      end


c
c     subroutine to parse a string into an element field and up to 3 integers
c     unused fields will be returned with values of -1
c
      subroutine parser(string,elfield,lelfield,i1,i2,i3)
      character*(*) string,elfield
      character*120 line
      dimension is(3)
      do i=1,3
        is(i)=-1
      enddo
      line=string
      call leftjust(line)
      ne=2 !assume two character element name
      if(line(2:2).eq.' '.or.line(2:2).eq.'_') ne=1
      lelfield=ne
      elfield=line(1:ne)

      do i=1,3
        if(line(ne+1:ne+1).eq.'_') then
c         assume there is a number field after the underscore
c         look for next space or underscore
          l=index(line(ne+2:),'_')
          if(l.gt.0) then
            read(line(ne+2:ne+l),*) is(i)
            ne=ne+l
          else
            read(line(ne+2:),*) is(i)
            go to 10
          endif
        endif
      enddo

10    continue

      i1=is(1)
      i2=is(2)
      i3=is(3)

      end


c
c     upper case a string
c
      subroutine ucase(string)
      character*(*) string
      character*26 upper,lower
      upper='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
      lower='abcdefghijklmnopqrstuvwxyz'
      do i=1,len(string)
        j=index(lower,string(i:i))
        if(j.ne.0) string(i:i)=upper(j:j)
      enddo
      end

c
c     left justify a string
c
      subroutine leftjust(string)
      character*(*) string

      is=0
      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 10
        endif
      enddo

10    continue
      if(is.eq.0) return  !all blank string

      do i=1,len(string)-is+1
        string(i:i)=string(is+i-1:is+i-1)
      enddo
c     blank out the last is-1 characters
      do i=len(string)-is+2,len(string)
        string(i:i)=' '
      enddo

      end

c
c     function to get atomic number given a string
c     holding the element name
c


      integer function igetatnum(string)
      character*(*) string
      character*26 upper,lower
      character*80 elements
      character*2 ename
      
      upper='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
      lower='abcdefghijklmnopqrstuvwxyz'
      elements(1:20)= 'H HELIBEB C N O F NE' 
      elements(21:40)='NAMGALSIP S CLARK CA'
      elements(41:60)='SCTIV CRMNFECONICUZN'
      elements(61:80)='GAGEASSEBRKRRBSRY ZR'

      igetatnum=o

      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 100
        endif
      enddo
100   continue
      ie=len(string)
      do i=is+1,len(string)
        if(string(i:i).eq.'_'.or.string(i:i).eq.' ') then
          ie=i-1
          go to 200
        endif
      enddo
200   continue

      ename(1:2)=string(is:ie)  !pads with blanks
c     write(*,'('' ENAME =*'',a2,''*'')') ename

c     element string is in string(is:ie)
      do i=1,2  
        ix=index(lower,ename(i:i))
        if(ix.ne.0) ename(i:i)=upper(ix:ix)
      enddo
c     write(*,'('' ENAME =*'',a2,''*'')') ename

      ix=index(elements,ename(1:2))
      if(ix.eq.0) then
        write(*,'('' WARNING:  Element not found: '',a2)') ename
c       stop 'ERROR:  missing element in igetatnum'
c     endif
        igetatnum=0
      else
        igetatnum=1+(ix-1)/2
      endif

      end
c
c     subroutine to populate the atomic masses array
c
      subroutine amassinit(amass,n)
      implicit real*8 (a-h,o-z)
      dimension amass(n)

c     atomic masses as needed
      do i=1,120
        amass(i)=0.d0
      enddo

      amass(1)=1.00794d0   !agrees w/Mulliken
      amass(2)=4.002602d0
      amass(3)=6.941d0
      amass(4)=9.012182d0
      amass(5)=10.811d0

      amass(6)=12.0107d0
      amass(6)=12.0110d0    !agrees w/Mulliken
      amass(7)=14.0067d0
      amass(8)=15.9994d0    !agrees w/Mulliken
      amass(9)=18.9984032d0
      amass(10)=20.1797d0

      amass(11)=22.98976928d0
      amass(12)=24.305d0
      amass(13)=26.9815386d0
      amass(14)=28.0855d0
      amass(15)=30.973762d0

      amass(16)=32.065d0
      amass(17)=35.453d0
      amass(18)=39.948d0
      amass(19)=39.0983d0
      amass(20)=40.078d0

      amass(21)=44.955912d0
      amass(22)=47.867d0
      amass(23)=50.9415d0
      amass(24)=51.9961d0
      amass(25)=54.938045d0

      amass(26)=55.845d0
      amass(27)=58.933195d0
      amass(28)=58.6934d0
      amass(29)=63.546d0
      amass(30)=65.38d0

      amass(31)=69.723d0
      amass(32)=72.64d0
      amass(33)=74.9216d0
      amass(34)=78.96d0
      amass(35)=79.904d0
      amass(36)=83.798d0

      end 
