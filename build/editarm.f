c
c     program to help edit poorly placed arms in
c     the fine grain version of gelcore stars made
c     from the coarse grain model.
c
c     after running finegrain, one has *.geo and *.pdb
c     files that can be used to view the finegrain models.
c     The arms in these models are placed using the available
c     torsion angles of the coarse grain model, but sometimes
c     the arms head off in a bad directions, such as through
c     the gelcore.
c
c     this program allows one to edit these poorly placed arms.
c
c     Input:
c     1) User has to give current coordinates:  *.pdb (or *.geo)
c     2) A *.remap file like what was used to set the finegrain torsions
c     to match the coarse grain ones.  Presumably, this zmatrix
c     is structured to reset the backbones.
c     Also, it has the correct final reordering of sites encoded in the
c     site names.
c     3) A set of arm numbers that need to be edited
c     4) For each arm number, a set of torsion angles that need to 
c     be reset and the torsion setting desired
c
c     Output:
c     User has to get replacement *.pdb (and *.geo) files.
c
c
c     (Might initially ignore the *.geo file since it is easy to make at 
c     the end.)
c
      implicit real*8 (a-h,o-z)

      character*256 line,fnedit,fnpdb,fnremap
      character*10 elfield1,elfield2 !short string to hold element name
      dimension isx(10)
      dimension iarmindex(16,3)  !for editing up to 16 arms
c               iarmindex(i,1) = repeat unit index for first repeat unit of arm 
c               iarmindex(i,2) = repeat unit index for last  repeat unit of arm 
c               iarmindex(i,3) = is number of torsions to reset
      dimension iarmtorsion(16,10),armtorsion(16,10) !up to 10 torsion resets per arm

      character*100 atname(30000)
      dimension coord(3,30000)
      character*100 lnkatname(1000)
      dimension lnk(3,1000)   !link vector from *.remap; large enough for all sites of an arm
      dimension zmat(3,1000)  !zmatrix vector from *.remap; large enough for all sites of an arm
      dimension zcoord(3,1000)!cartesians from zmatrix from *.remap for all sites of an arm
      dimension isort(1000),jsort(1000)   !for reordering the zmatrix to canonical order
      dimension dum1(12),dum2(78)   !for use with dihint
      dimension ref(3,3)  !for use with the place subroutine
 

      natmax=30000
      narmmax=1000   !max number of sites in an arm

      call getarg(1,fnedit)
      is=index(fnedit,' ')
      if(index(fnedit(1:is-1),'editarm').eq.0) then
        write(*,'(''Error:  expecting file name with '',
     x    ''type *.editarm as command line argument'')')
        stop 'Error:  expecting file name with type *.editarm '//
     x    'on command line argument'
      endif


c
c     read contents of the input file *.editarm mentioned on the command line
c
      write(*,'(''Input file from command line: '',a)') fnedit(1:is-1)
      open(10,file=fnedit(1:is-1))
      read(10,'(a256)') fnpdb
      write(*,'(''PDB/GEO file for coordinates: '',a)')
     x  fnpdb(1:index(fnpdb,' ')-1)
      read(10,'(a256)') fnremap
      write(*,'(''REMAP file for arm edit:      '',a)')
     x  fnremap(1:index(fnremap,' ')-1)

      read(10,*) narmedit   !number of arms that need to be edited
      if(narmedit.gt.16) then
        stop 'Error: too many arms to edit'
      endif
      write(*,'(''Number of arms to be edited   '',i5)') narmedit
      do i=1,narmedit
        read(10,*) iarmindex(i,1),iarmindex(i,2),iarmindex(i,3)
        write(*,'(''Arm '',i2,'' with first monomer index '',i5,
     x    '', and last monomer index '',i5,      
     x    '' will need to reset '',i2,'' torsions '')')
     x    i,iarmindex(i,1),iarmindex(i,2),iarmindex(i,3)
        if(iarmindex(i,3).gt.10) then
          write(*,'(''Error:  asking to reset too many '',
     x      ''torsion angles'')')
          stop 'Error:  asking to reset more than 10 torsion angles'
        endif
        do j=1,iarmindex(i,3)
          read(10,*) iarmtorsion(i,j),armtorsion(i,j)
          write(*,'(''Reset torsion number '',i2,
     x      '' in the remap file to a value of '',f10.2)')
     x    iarmtorsion(i,j),armtorsion(i,j)
        enddo
      enddo
      close(10)


c
c     read in the coordinates from either the *.geo or the *.pdb file
c     the *.geo file has more significant digits in the coordinates
c     than the *.pdb file
c

c     read in the geo or pdb file; just need names and cartesians
      is=index(fnpdb,' ')-1
      open(11,file=fnpdb(1:is))
      nsite=0
      if(index(fnpdb(1:is),'geo').ne.0) then  
        write(*,'(''Reading *.geo file '')')
        read(11,*) !read "Geometry" line 
110     read(11,'(a256)') line  
        if(index(line,'End').ne.0)  go to 120
        nsite=nsite+1
        if(nsite.gt.natmax) then
          write(*,'(''Error: not enough memory for this many '',
     x      ''sites.'')')
          stop 'Error: not enough memory for this many sites'
        endif
        is=index(line,' ')-1
        atname(nsite)=line(1:is)
        read(line(is+1:),*) (coord(k,nsite),k=1,3)
c       write(*,'(''Site number '',i6,'' name '',a,
c    x    '' Coord: '',3f12.6)')
c    x    nsite,atname(nsite)(1:is),(coord(k,nsite),k=1,3)
        go to 110
120     continue
      elseif(index(fnpdb,'pdb').ne.0) then  
        write(*,'(''Error:  code not implemented to '',
     x    ''read pdb file'')')
      else
        write(*,'(''Error:  unrecognized coordinate file type'')')
      endif

      close(11)
      write(*,'(''Number of sites in coordinate file '',i6)') nsite




c
c     read in the *.remap file
c
c     structure of remap file is:   name name dist name angle name dihedral
c     where name is a character string
c     NOTE THAT THE SITES WILL BE IN A DIFFERENT ORDER AND WITH DIFFERENT NAMES
c     THAN IN THE GEO/PDB FILE.
c     ALSO THE REMAP FILE WILL HAVE AT LEAST ONE MORE SITE THAN NEEDED.
c

      is=index(fnremap,' ')-1
      open(12,file=fnremap(1:is))
      nremap=0    !count lines in the remap file
210   continue
      read(12,'(a256)',end=230) line
      call lstchr(line,il)
      nremap=nremap+1
c     write(*,'(/,''Input line  '',i6,'' *'',a,''*'')') 
c    x  nremap,line(1:il)
      isx(1)=index(line,' ')      ! first token is line(1:is1-1); blank at is1:is1
c     write(*,'(''First token *'',a,''*'')') line(1:isx(1)-1)
      lnkatname(nremap)=line(1:isx(1)-1)
      do j=1,3
        lnk(j,nremap)=0
        zmat(j,nremap)=0.d0
      enddo
      nset=0

      if(nremap.gt.1) then
        call leftjust(line(isx(1)+1:))
        isx(2)=index(line(isx(1)+1:),' ')+isx(1) !second token line(is1+1:is2-1); blank at is2:is2
        call leftjust(line(isx(2)+1:))
        isx(3)=index(line(isx(2)+1:),' ')+isx(2) !third token line(is2+1:is3-1); blank at is3:is3
c       write(*,'(''Secon token *'',a,''*'')') line(isx(1)+1:isx(2)-1)
c       write(*,'(''Third token *'',a,''*'')') line(isx(2)+1:isx(3)-1)
        is1=index(line(isx(1)+1:isx(2)-1),'_')+isx(1)+1    !extract the site number from this field
        is2=index(line(is1:isx(2)-1),'_')+is1-2    !extract the site number from this field
        read(line(is1:is2),*) lnk(1,nremap)
        read(line(isx(2)+1:isx(3)-1),*) zmat(1,nremap)
c       write(*,'(''Site number '',i5,'' dist '',f10.4)')
c    x    lnk(1,nremap),zmat(1,nremap)
        nset=1
      endif

      if(nremap.gt.2) then
        call leftjust(line(isx(3)+1:))
        isx(4)=index(line(isx(3)+1:),' ')+isx(3) !fourth token line(is3+1:is4-1); blank at is4:is4
        call leftjust(line(isx(4)+1:))
        isx(5)=index(line(isx(4)+1:),' ')+isx(4) !fifth token line(is4+1:is5-1); blank at is5:is5
c       write(*,'(''Fourt token *'',a,''*'')') line(isx(3)+1:isx(4)-1)
c       write(*,'(''Fifth token *'',a,''*'')') line(isx(4)+1:isx(5)-1)
        is1=index(line(isx(3)+1:isx(4)-1),'_')+isx(3)+1    !extract the site number from this field
        is2=index(line(is1:isx(4)-1),'_')+is1-2    !extract the site number from this field
        read(line(is1:is2),*) lnk(2,nremap)
        read(line(isx(4)+1:isx(5)-1),*) zmat(2,nremap)
c       write(*,'(''Site number '',i5,'' dist '',f10.4)')
c    x    lnk(2,nremap),zmat(2,nremap)
        nset=2
      endif

      if(nremap.gt.3) then
        call leftjust(line(isx(5)+1:))
        isx(6)=index(line(isx(5)+1:),' ')+isx(5) !sixth token line(is5+1:is6-1); blank at is6:is6
        call leftjust(line(isx(6)+1:))
        isx(7)=index(line(isx(6)+1:),' ')+isx(6) !seventh token line(is6+1:is7-1); blank at is7:is7
c       write(*,'(''Sixth token *'',a,''*'')') line(isx(5)+1:isx(6)-1)
c       write(*,'(''Seven token *'',a,''*'')') line(isx(6)+1:isx(7)-1)
        is1=index(line(isx(5)+1:isx(6)-1),'_')+isx(5)+1    !extract the site number from this field
        is2=index(line(is1:isx(6)-1),'_')+is1-2    !extract the site number from this field
        read(line(is1:is2),*) lnk(3,nremap)
        read(line(isx(6)+1:isx(7)-1),*) zmat(3,nremap)
c       write(*,'(''Site number '',i5,'' dist '',f10.4)')
c    x    lnk(3,nremap),zmat(3,nremap)
        nset=3
      endif

      do j=1,nset
c       line(isx(1)+1:isx(2)-1)   !second token
c       line(isx(3)+1:isx(4)-1)   !fourth token
c       line(isx(5)+1:isx(6)-1)   !sixth token
        ifound=0
        do k=1,nremap-1
          if(ifound.eq.0.and.
     x      lnkatname(k)(1:index(lnkatname(k),' ')-1).eq.
     x      line(isx(2*j-1)+1:isx(2*j)-1)) then
            ifound=k
          endif
        enddo
        if(ifound.eq.0) then
          write(*,'(''Error:  referenced site in zmatrix'',
     x      '' not found'')')
          stop 'Error: referenced site in zmatrix not found'
        endif
c       write(*,'(''Referenced zmatrix site '',a,
c    x    '' found at position '',i5)') 
c    x    line(isx(2*j-1)+1:isx(2*j)-1),ifound
        lnk(j,nremap)=ifound
      enddo


c     write(*,'(''Remap Z-matrix: '',a,3(5x,i5,2x,f10.4))')
c    x  lnkatname(nremap)(1:index(lnkatname(nremap),' ')-1),
c    x  lnk(1,nremap),zmat(1,nremap),
c    x  lnk(2,nremap),zmat(2,nremap),
c    x  lnk(3,nremap),zmat(3,nremap)

      go to 210


230   continue
      close(12)
      write(*,'(''Number of sites in the remap file is '',i5)') nremap



c
c     generate a sort vector:  points to where in the zmatrix each site is
c
      do i=1,nremap
        is1=index(lnkatname(i),'_')
        is2=index(lnkatname(i)(is1+1:),'_')+is1-1
c       write(*,'('' extract number field from '',a,
c    x    '' is *'',a,''*'')')
c    x    lnkatname(i)(1:index(lnkatname(i),' ')-1),
c    x    lnkatname(i)(is1:is2)
        read(lnkatname(i)(is1+1:is2),*) iptr
        isort(iptr)=i     !almost the right order but not quite
      enddo





c     Now, for each arm to be changed
      do iarm=1,narmedit
c       find the range of sites to be changed
        ifrst=0
        ilast=0
        do isite=1,nsite
          is1=index(atname(isite)(1:),'_')         !location of first underscore
          is2=index(atname(isite)(is1+1:),'_')+is1 !location of second underscore
          is3=index(atname(isite)(is2+1:),'_')+is2 !location of second underscore
          is4=index(atname(isite)(1:),' ')
c         write(*,'(''Field 1:'',a,'' Field 2:'',a,
c    x      '' Field 3:'',a,'' Field 4:'',a)')
c    x      atname(isite)(1:is1-1),atname(isite)(is1+1:is2-1),
c    x      atname(isite)(is2+1:is3-1),atname(isite)(is3+1:is4-1)
          read(atname(isite)(is3+1:is4-1),*) ires
          if(ifrst.eq.0.and.ires.eq.iarmindex(iarm,1)) then
            ifrst=isite
          endif
          if(ifrst.ne.0.and.ires.eq.iarmindex(iarm,2)) then
            ilast=isite
          endif
        enddo

        write(*,'(''First site '',a,'' index '',i6,
     x    ''; last site '',a,'' index '',i6)')
     x    atname(ifrst)(1:index(atname(ifrst),' ')-1), ifrst,
     x    atname(ilast)(1:index(atname(ilast),' ')-1), ilast

        if(ilast-ifrst+1.gt.narmmax) then
          write(*,'(''Error:  not enough memory to hold all '',
     x      ''sites of this arm'')')
          stop  'Error:  not enough memory to hold sites of an arm'
        endif
 
c       write(*,'(i5,''  Arm site to edit '',i5,a)') 
c    x    (k-ifrst+1,k,atname(k)(1:index(atname(k),' ')-1),
c    x    k=ifrst,ilast)

c       following mapping has mismatches
c       do i=1,nremap
c         write(*,'(i6,'' Coord name '',a,'' Zmat name '',a)')
c    x    i,atname(ifrst-1+i)(1:index(atname(ifrst-1+i),' ')-1),
c    x    lnkatname(isort(i))(1:index(lnkatname(isort(i)),' ')-1)
c       enddo
c
c       remove references to unneeded sites in the zmatrix which are not in the coord list
c
        call parser(atname(ifrst),elfield1,lelfield1,i1,i2,i3)
        ioffres=i3-0  !repeat unit offset (unlike site index, these start at zero)
        iptr=1
        do i=ifrst,ilast
          call parser(atname(i),elfield1,lelfield1,i1,i2,i3)
310       call parser(lnkatname(isort(iptr)),
     x      elfield2,lelfield2,ix1,ix2,ix3)
c         check if there is a match; if not, bump iptr and try the next one
          if(elfield1(1:lelfield1).eq.elfield2(1:lelfield2)
     x      .and.
     x       i2.eq.ix2
     x      .and.
     x       i3-ioffres.eq.ix3) then
c           write(*,'(''Match between '',a,'' and '',a)')
c    x        atname(i)(1:index(atname(i),' ')-1),
c    x        lnkatname(isort(iptr))
c    x        (1:index(lnkatname(isort(iptr)),' ')-1)
          else
            write(*,'(''Match NOT found between '',a,'' and '',a)')
     x        atname(i)(1:index(atname(i),' ')-1),
     x        lnkatname(isort(iptr))
     x        (1:index(lnkatname(isort(iptr)),' ')-1)
            iptr=iptr+1
            if(iptr.gt.nremap) then
              write(*,'(''Error:  futile search through remap file'')')
              stop 'Error: futile search in remap list; no arm site'
            endif
            go to 310
          endif
          isort(i-ifrst+1)=isort(iptr)
          iptr=iptr+1
        enddo

c
c       final mapping between the two lists is now in isort
c
c       write(*,'(/,''Checking isort array'')')
c       do i=1,ilast-ifrst+1
c         write(*,'(''ISORT('',i6,'')='',i6,
c    x    '' Coord name '',a,'' Zmat name '',a)')
c    x    i,isort(i),
c    x    atname(ifrst-1+i)(1:index(atname(ifrst-1+i),' ')-1),
c    x    lnkatname(isort(i))(1:index(lnkatname(isort(i)),' ')-1)
c       enddo

c       set up jsort for the inverse look up:  
c       given a zmatrix element, where are the cartesians?  
        do i=1,nremap
          jsort(i)=0
        enddo
        do i=1,ilast-ifrst+1
          j=isort(i)
          jsort(j)=i
        enddo
c       check jsort - it should tell me the coord name for each zmatrix entry
c       write(*,'(/,''Checking jsort array'')')
c       do i=1,nremap
c         if(jsort(i).eq.0) then
c           write(*,'(''JSORT('',i5,'')='',i5,
c    x        '' Zmatrix name '',a,
c    x        '' has no entry in coord list'')') 
c    x        i,jsort(i),lnkatname(i)(1:index(lnkatname(i),' ')-1)
c         else
c           write(*,'(''JSORT('',i5,'')='',i5,
c    x        '' Zmat name '',a,
c    x        '' Coord name '',a)') 
c    x        i,jsort(i),
c    x        lnkatname(i)(1:index(lnkatname(i),' ')-1),
c    x        atname(ifrst-1+jsort(i))
c    x        (1:index(atname(ifrst-1+jsort(i)),' ')-1)
c         endif
c       enddo






c
c       keep the cartesian coordinates of the first
c       three sites of the arm
        do j=1,3
          lz=jsort(j)
          zcoord(1,j)=coord(1,ifrst-1+lz)
          zcoord(2,j)=coord(2,ifrst-1+lz)
          zcoord(3,j)=coord(3,ifrst-1+lz)
c         write(*,'(''Zmatrix ref site '',i2,'' from ''
c    x      ''site '',i6,'' with cartesians '',3f10.4)')
c    x      j,ifrst-1+lz,(zcoord(k,j),k=1,3)
        enddo





c       generate the current zmatrix coordinates given the link vector
c       note that this will have the "extra" unused sites in it
c       first three sites are given by the cartesians
c       compute the current zmatrix values to allow checking against 
c       the zmatrix values in the remap file

        pi=4.d0*datan(1.d0)
        do i=1,nremap   !loop over sites in the zmatrix
c         write(*,'(''Zmatrix line '',i5,
c    x      3(4x,i5,2x,f10.4))')
c    x      i,(lnk(k,i),zmat(k,i),k=1,3)
          dist=0.d0
          ang=0.d0
          dih=0.d0
          iz=jsort(i)
          jz=lnk(1,i)    !zero for i=1
          kz=lnk(2,i)    !zero for i=1,2
          lz=lnk(3,i)    !zero for i=1,2,3
          if(iz.eq.0) write(*,'(''Reference to zmatrix site '',a,
     x      '' with no coords'')')
     x      lnkatname(i)(1:index(lnkatname(i),' ')-1)
c         compute (dist,angle,dihedral) using link vector

          if(i.ge.2) then    !compute a distance
            if(jsort(jz).eq.0) write(*,'(''Reference to zmatrix '',
     x      ''site '',a,'' with no coords'')')
     x      lnkatname(jz)(1:index(lnkatname(jz),' ')-1)
c           write(*,'(''Computing distance betw z-matrix elements '',
c    x        i5,2x,a,5x,i5,2x,a)')
c    x        i,lnkatname(i)(1:index(lnkatname(i),' ')-1),
c    x        jz,lnkatname(jz)(1:index(lnkatname(jz),' ')-1)
            jz=jsort(jz)
c           write(*,'(''Corresponding to coordinate sites '',
c    x        i5,2x,a,5x,i5,2x,a)') 
c    x        iz,atname(ifrst-1+iz)(1:index(atname(ifrst-1+iz),' ')-1),
c    x        jz,atname(ifrst-1+jz)(1:index(atname(ifrst-1+jz),' ')-1)
            dist=(coord(1,ifrst-1+iz)-coord(1,ifrst-1+jz))**2   !distance 
     x          +(coord(2,ifrst-1+iz)-coord(2,ifrst-1+jz))**2   !distance 
     x          +(coord(3,ifrst-1+iz)-coord(3,ifrst-1+jz))**2   !distance 
            dist=dsqrt(dist)
          endif

          if(i.ge.3) then
            if(jsort(kz).eq.0) write(*,'(''Reference to zmatrix '',
     x        ''site '',a,'' with no coords'')')
     x      lnkatname(kz)(1:index(lnkatname(kz),' ')-1)
            kz=jsort(kz)
            call angint(coord(1,ifrst-1+iz),
     x                  coord(1,ifrst-1+jz),
     x                  coord(1,ifrst-1+kz),ang)
          endif
          if(i.ge.4) then
            if(isort(lz).eq.0) write(*,'(''Reference to zmatrix '',
     x      ''site '',a,'' with no coords'')')
     x      lnkatname(lz)(1:index(lnkatname(lz),' ')-1)
            lz=jsort(lz)
            call dihint(1  ,coord(1,ifrst-1+iz),
     x                      coord(1,ifrst-1+jz),
     x                      coord(1,ifrst-1+kz),
     x                      coord(1,ifrst-1+lz),
     x      dih,dum1,dum2)
          endif
          write(*,'(i5,'' Dz/D '',2f10.5,
     x              '' Az/A '',2f10.5,
     x              '' Tz/T '',2f11.5)') i,
     x      zmat(1,i),dist,zmat(2,i),ang*(180.d0/pi),
     x                     zmat(3,i),dih*(180.d0/pi)


c         apply any edit to the zmatrix row here
c         (the index of the torsion refers to a row of the zmatrix
c         so check against i); this will replace the dih torsion angle
          do jtor=1,iarmindex(iarm,3)   !number of torsions to reset in this arm
            if(i.eq.iarmtorsion(iarm,jtor)) then
              write(*,'(''Resetting torsion number '',i3,
     x         '' from '',f10.4,'' to '',f10.4)')
     x         iarmtorsion(iarm,jtor),  !which torsion
     x         dih*(180.d0/pi),
     x         armtorsion(iarm,jtor)    !new torsion value, in degrees
               dih=armtorsion(iarm,jtor)*(pi/180.d0)   !need dih in radian
            endif
          enddo




c         next, construct the cartesian coordinates that this  
c         zmatrix row implies
c
          if(i.ge.4) then
            do k=1,3
c             ref(k,3)=coord(k,ifrst-1+jz)
c             ref(k,2)=coord(k,ifrst-1+kz)
c             ref(k,1)=coord(k,ifrst-1+lz)
              ref(k,3)=zcoord(k,lnk(1,i))
              ref(k,2)=zcoord(k,lnk(2,i))
              ref(k,1)=zcoord(k,lnk(3,i))
            enddo
c           rab=zmat(i,1)
c           ang=zmat(i,2)
c           dih=zmat(i,3)
            rab=dist
            ang=ang*(180.d0/pi)   !place wants degrees, not radians
            dih=dih*(180.d0/pi)   !place wants degrees, not radians
c           place expects to get ref organized in order dihed, angle, dist (D1,D2,D3)
            call place(ref,rab,ang,dih,zcoord(1,i))
c           write(*,'(''Zmat site '',i5,
c    x        /,'' Previous cartesians: '',3f10.4,
c    x        /,'' New      cartesians: '',3f10.4)')
c    x        i,(coord(k,ifrst-1+iz),k=1,3),
c    x        (zcoord(k,i),k=1,3)
          endif

c         

        enddo   !end of do loop over number of sites on this arm
c
c       now replace the sites in coord with those in zcoord
c
        do i=ifrst,ilast
          if(isort(i-ifrst+1).ne.0) then
            lz=isort(i-ifrst+1)
            Write(*,'(''Site '',i5,'' Named '',a,/,
     x        '' orig cart: '',3f10.4,/,
     x        '' new  cart: '',3f10.4)')
     x      i,atname(i)(1:index(atname(i),' ')-1),
     x      (coord(k,i),k=1,3),
     x      (zcoord(k,lz),k=1,3)
            do k=1,3
              coord(k,i)=zcoord(k,lz)
            enddo
          endif
        enddo



      enddo   !end of do loop over number of arms to edit


c
c     generate a modified *.geo and *.pdb file with new coordinates
c
      is=index(fnpdb,'.geo')
      if(is.eq.0) then
        is=index(fnpdb,'.pdb')
      endif
      if(is.eq.0) then
        write(*,'(''Error:  coordinate file is neither '',
     x    ''*.geo nor *.pdb '')')
        stop 'Error:  unrecognized coordinate file type'
      endif

c     generate a modified *.geo file
ceometry
c_1_1_0                      -1.21809        0.77782       -0.47244
c_2_2_0                      -0.52539        0.60605       -1.81674
c_3_3_0                       0.97311        0.86056       -1.65452
c_4_4_0                       1.66900        0.68800       -3.00500
c_5_5_0                       3.16883        0.94261       -2.84323
c_6_6_0                       3.88435       -0.21404       -3.21324
cnd Geometry
      open(15,file=fnpdb(1:is-1)//'.EA.geo')
      write(15,'(''Geometry'')')
      do i=1,nsite 
        write(15,'(a,2x,3f15.5)')  atname(i)(1:20),
     x    (coord(k,i),k=1,3)
      enddo
      write(15,'(''End Geometry'')')
      close(15)


c     generate a modified *.pdb file
c23456789 123456789 123456789 123456789 123456789 123456789 
cETATM22018 H    DME A1530     161.953 -82.217-107.390  1.00  0.00           H
cETATM22019 H    DME A1530     162.352 -80.779-108.361  1.00  0.00           H
cETATM22020 H    DME A1530     161.981 -82.322-109.167  1.00  0.00           H
cONECT    1    2    7 3335
cONECT    2    1    3    8    9
      open(14,file=fnpdb(1:is-1)//'.pdb')
      open(15,file=fnpdb(1:is-1)//'.EA.pdb')
      do i=1,nsite
        read(14,'(a256)') line
        write(line(31:54),'(3F8.3)') (coord(k,i),k=1,3)
        call lstchr(line,ilst)
        write(15,'(a)') line(1:ilst)
      enddo

510   continue
      read(14,'(a256)',end=520) line
      call lstchr(line,ilst)
      write(15,'(a)') line(1:ilst)
      go to 510


520   continue



      close(14)
      close(15)





      end

c ----------------------------------------------------------------
c
c     subroutine to pack up to four fields into a string
c
      subroutine packer(elfield,i1,i2,i3,string)
      character*(*) elfield,string
      character*120 line
      character*10 num
      dimension is(3)
      line=elfield
      call leftjust(line)
c     assume this field is either one or two characters, look for a space or an underscore
      ne=2 !assume 2 character element name
      if(line(2:2).eq.' '.or.line(2:2).eq.'_') ne=1 !set it to 1 character in these cases
      is(1)=i1
      is(2)=i2
      is(3)=i3
      
      do i=1,3
        if(is(i).ge.0) then
          write(num,'(i10)') is(i)
          ip=0
          do j=1,len(num)
            if(ip.eq.0.and.num(j:j).ne.' ') ip=j 
          enddo
          line(ne+1:ne+len(num)-ip+2)='_'//num(ip:len(num))   !move len(num)-j+1 characters
          ne=ne+len(num)-ip+2
        endif
      enddo

      if(ne.gt.len(string)) then
        write(*,'('' Error: character string too short '',
     x     ''for site name: *'',a,''*'')') line(1:ne)
        stop 'strings too short for site names in molecule'
      endif
      string=line

      end


c
c     subroutine to parse a string into an element field and up to 3 integers
c     unused fields will be returned with values of -1
c
      subroutine parser(string,elfield,lelfield,i1,i2,i3)
      character*(*) string,elfield
      character*120 line
      dimension is(3)
      do i=1,3
        is(i)=-1
      enddo
      line=string
      call leftjust(line)
      ne=2 !assume two character element name
      if(line(2:2).eq.' '.or.line(2:2).eq.'_') ne=1
      lelfield=ne
      elfield=line(1:ne)

      do i=1,3
        if(line(ne+1:ne+1).eq.'_') then
c         assume there is a number field after the underscore
c         look for next space or underscore
          l=index(line(ne+2:),'_')
          if(l.gt.0) then
            read(line(ne+2:ne+l),*) is(i)
            ne=ne+l
          else
            read(line(ne+2:),*) is(i)
            go to 10
          endif
        endif
      enddo

10    continue

      i1=is(1)
      i2=is(2)
      i3=is(3)

      end


c
c     upper case a string
c
      subroutine ucase(string)
      character*(*) string
      character*26 upper,lower
      upper='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
      lower='abcdefghijklmnopqrstuvwxyz'
      do i=1,len(string)
        j=index(lower,string(i:i))
        if(j.ne.0) string(i:i)=upper(j:j)
      enddo
      end

c
c     left justify a string
c
      subroutine leftjust(string)
      character*(*) string

      is=0
      do i=1,len(string)
        if(string(i:i).ne.' ') then
          is=i
          go to 10
        endif
      enddo

10    continue
      if(is.eq.0) return  !all blank string

      do i=1,len(string)-is+1
        string(i:i)=string(is+i-1:is+i-1)
      enddo
c     blank out the last is-1 characters
      do i=len(string)-is+2,len(string)
        string(i:i)=' '
      enddo

      end

c
c     subroutine lstchr returns the index of the last nonblank character in a string
c
      subroutine lstchr(string,ilst)
      character*(*) string 

      ilst=0  !if string is all blank, return zero
      do i=len(string),1,-1
        if(string(i:i).ne.' ') then
          ilst=i
          go to 10
        endif
      enddo
10    continue

      end

c
c     angle calculation stolen from dihint
c
      subroutine angint(xyz1,xyz2,xyz3,ang)
      Implicit Real*8 (a-h,o-z)
c     Input and output arrays
      real*8 xyz1(3),xyz2(3),xyz3(3)
      real*8 a(3),b(3)

c     Compute a=r1-r2, b=r3-r2
      do 10 k=1,3
      a(k)=xyz1(k)-xyz2(k)
10    b(k)=xyz3(k)-xyz2(k)
 
c     Compute a . b 
      dot=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
 
c     Compute lengths
      b2=b(1)**2+b(2)**2+b(3)**2
      a2=a(1)**2+a(2)**2+a(3)**2
      angcos=dot/dsqrt(a2*b2)
      ang=dacos(angcos)

      end





c Change History:
c $Log$
c*arc dihint
      subroutine dihint(iop,xyz1,xyz2,xyz3,xyz4,
     >       phi,dphidx,d2phidx2)
c
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Copyright (c) 1994 International Business Machines Corporation
C  All rights reserved
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Description      Given the coordinates of four particles, this
C                     subroutine computes the torsion (dihedral)
C                     angle between them, the Jacobean (dphi/dx(i))
C                     and d2phi/dx(i)dx(j).
C
C  Input
C
C     iop           Control
C                   iop = 1 means just compute angle
C                   iop = 2 means compute angle and Jacobean
C                   iop = 3 means compute angle, Jacobean and second
C                           derivative
C
C     xyz1(3)       cartesian coordinates of first atom
C     xyz2(3)       cartesian coordinates of second atom
C     xyz3(3)       cartesian coordinates of third atom
C     xyz4(3)       cartesian coordinates of fourth atom
C
C
C  Output
C
C     phi           Angle between atoms 1-2-3-4.  Range returned is
C                   -pi .lt. phi .le. pi, following IUPAC
C                   sign and range convention.
C
C     dphidx(12)    Jacobean: dphi/dx(1), dphi/dy(1), ...
C
C     d2phid2(45)   Second derivative, d2phi/dx(i)dx(2), a symmetric
C                   matrix, stored lower triangle by row
C
C  Linkage          List of subroutines called by this one
C
C  Programmer       William Swope (some from Carnevali)
C
C  Date             June 1994
C
C  Update log       Record of all updates including names of programmers,
C                     descriptions of changes made, and dates of change.
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
c
      Implicit Real*8 (a, b, d-h, o, p, r-y)
      Implicit Integer (i-n)
c     Implicit Logical (z)
c     Violation of ARCHEM convention; but no logical data here
      Implicit Real*8 (z)
c     Implicit Character (c)
c     Violation of ARCHEM convention; but no character data here
      Implicit Real*8 (c)
c     Implicit Undefined (q)
 
      character cproduct*48
      dimension cproduct(3)
 
c     Input and output arrays
      real*8 xyz1(3),xyz2(3),xyz3(3),xyz4(3)
      real*8 dphidx(12),d2phidx2(78)
 
 
C     THE SIGN CONVENTION FOR PHI IS AS FOLLOWS:
C     NUMBER THE ASSOCIATED ATOMS 1, 2, 3, 4.  ATOMS 1 AND 4 ARE
C     "TERMINAL" ATOMS AND THE DIHEDRAL ANGLE IS ABOUT THE BOND
C     BETWEEN ATOMS 2 AND 3.  IMAGINE HOLDING THE PLANE FORMED BY
C     ATOMS 1, 2, AND 3 FIXED AND ROTATING THE PLANE FORMED BY
C     ATOMS 2, 3, AND 4 ABOUT A VECTOR THAT POINTS FROM ATOM 2
C     TO ATOM 3.  THE POSITIVE SENSE CORRESPONDS TO THE RIGHT-HAND
C     RULE FOR ROTATION ABOUT THE VECTOR FROM ATOM 2 TO ATOM 3.
c     This agrees with the IUPAC convention.
C
C     FURTHERMORE, PHI=0 CORRESPONDS TO A CONDITION WHERE ALL FOUR
C     ATOMS ARE ON A PLANE AND ATOMS 1 AND 4 ARE "CIS" TO THE BOND
C     BETWEEN ATOMS 2 AND 3.  LIKEWISE, PHI=180 DEGREES CORRESPONDS
C     A CONDITION WHERE ALL FOUR ATOMS ARE ON A PLANE AND ATOMS 1 AND
C     4 ARE "TRANS" TO THE BOND BETWEEN ATOMS 2 AND 3.
 
 
c     Local and scratch arrays
 
      dimension a(3),b(3),c(3),u(3),v(3),acrossc(3)
 
c     First derivatives of u**2, v**2 with respect to a, b, and c
      dimension du2da(3),du2db(3),dv2db(3),dv2dc(3)
 
c     First derivatives of D=u**2*v**2 and F=D**(-1/2) with
c     respect to a, b, and c
      dimension ddda(3),dddb(3),dddc(3)
      dimension dfda(3),dfdb(3),dfdc(3)
 
c     First derivatives of uv with respect to a, b, and c
      dimension duvda(3),duvdb(3),duvdc(3)
 
c     Derivatives of S with respect to a, b, and c
      dimension dsda(3),dsdb(3),dsdc(3)
 
c     Derivatives of cosphi and sinphi with respect to a, b, and c
      dimension dcosphida(3),dcosphidb(3),dcosphidc(3)
      dimension dsinphida(3),dsinphidb(3),dsinphidc(3)
 
c     Derivatives of phi with respect to a, b, and c
      dimension dphida(3),dphidb(3),dphidc(3)
 
c     Second derivatives
      dimension du2dada(3,3),du2dbdb(3,3),du2dadb(3,3)
      dimension dv2dbdb(3,3),dv2dbdc(3,3),dv2dcdc(3,3)
      dimension dddada(3,3),dddbdb(3,3),dddcdc(3,3)
      dimension dddadb(3,3),dddadc(3,3),dddbdc(3,3)
      dimension dfdada(3,3),dfdbdb(3,3),dfdcdc(3,3)
      dimension dfdadb(3,3),dfdadc(3,3),dfdbdc(3,3)
      dimension              duvdbdb(3,3)
      dimension dsdadb(3,3),dsdadc(3,3),dsdbdc(3,3)
      dimension              dsdbdb(3,3)
      dimension duvdadb(3,3),duvdadc(3,3),duvdbdc(3,3)
      dimension dcosphidada(3,3),dcosphidbdb(3,3),dcosphidcdc(3,3)
      dimension dcosphidadb(3,3),dcosphidadc(3,3),dcosphidbdc(3,3)
      dimension dsinphidada(3,3),dsinphidbdb(3,3),dsinphidcdc(3,3)
      dimension dsinphidadb(3,3),dsinphidadc(3,3),dsinphidbdc(3,3)
      dimension dphidada(3,3),dphidbdb(3,3),dphidcdc(3,3)
      dimension dphidadb(3,3),dphidadc(3,3),dphidbdc(3,3)
 
      dimension atilde(3,3),btilde(3,3),ctilde(3,3)
 
c     identity tensor here to simplify some loops
      real*8 delta(3,3)/1.d0,3*0.d0,1.d0,3*0.d0,1.d0/
 
      data cproduct /'$RCSfile$',
     $               '$Revision$$Date$',
     $               '$IBMCSA: PRODUCT VERSION NUMBER $'/
 
c     Statement function gives index of (i,j) in lower triang by row
      lowtri(i,j) = j + i*(i-1)/2
 
      if(iop.ne.1 .and. iop.ne.2 .and. iop.ne.3) then
        print *, ' Incorrect input to dihint subroutine: iop = ',iop
c       call arcdie()
      endif
 
      ierror=0
 
      if(iop.ge.1) then
c     compute angle
 
c     Compute a=r2-r1, b=r3-r2, c=r4-r3
      do 10 k=1,3
      a(k)=xyz2(k)-xyz1(k)
      b(k)=xyz3(k)-xyz2(k)
10    c(k)=xyz4(k)-xyz3(k)
 
c     Compute u = a x b , v = b x c, vectors normal to 123, 234 planes
      u(1)=a(2)*b(3)-a(3)*b(2)
      u(2)=a(3)*b(1)-a(1)*b(3)
      u(3)=a(1)*b(2)-a(2)*b(1)
      v(1)=b(2)*c(3)-b(3)*c(2)
      v(2)=b(3)*c(1)-b(1)*c(3)
      v(3)=b(1)*c(2)-b(2)*c(1)
 
c     Compute lengths and scalar products
      b2=b(1)**2+b(2)**2+b(3)**2
      u2=u(1)**2+u(2)**2+u(3)**2
      v2=v(1)**2+v(2)**2+v(3)**2
      if( b2.eq.0.d0 )then
c       this should be a hard error; print out message and die
c       if this happens there is something seriously wrong
        print *, ' Error:  Can not compute torsion angle when central',
     >           ' bond has zero length.'
c       call arcdie('dihint')
      endif
      b1=sqrt(b2)
      u1=sqrt(u2)
      v1=sqrt(v2)
      d=u2*v2
      d1=sqrt(d)
      uv=u(1)*v(1)+u(2)*v(2)+u(3)*v(3)
      av=a(1)*v(1)+a(2)*v(2)+a(3)*v(3)
      s=b1*av
      if( u2.eq.0.d0 .or. v2.eq.0.d0 )then
        print *, ' Error:  Three collinear atoms make torsion angle',
     >           ' undefined.'
        print *, ' Zero returned for angle, gradient and hessian.'
        ierror=1
        f=0.d0
        d=1.d0
        d1=1.d0
c       Compute cosphi and sinphi
        cosphi=1.d0
        sinphi=0.d0
      else
        f=1.d0/d1
c       Compute cosphi and sinphi
        cosphi=uv*f
        sinphi=s*f
      endif
 
c     Compute phi itself, produce range of -pi.lt.phi.le.pi
      phi=atan2(sinphi,cosphi)
 
      endif
 
      if(iop.ge.2) then
c     compute dphidx
 
c     Compute derivatives of u**2 and v**2 with respect to a, b, and c.
      a2=a(1)**2+a(2)**2+a(3)**2
      c2=c(1)**2+c(2)**2+c(3)**2
      ab=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
      bc=b(1)*c(1)+b(2)*c(2)+b(3)*c(3)
      ac=a(1)*c(1)+a(2)*c(2)+a(3)*c(3)
      do 20 k=1,3
      du2da(k)=2.d0*(b2*a(k)-ab*b(k))
      du2db(k)=2.d0*(a2*b(k)-ab*a(k))
      dv2db(k)=2.d0*(c2*b(k)-bc*c(k))
20    dv2dc(k)=2.d0*(b2*c(k)-bc*b(k))
 
c     Compute derivatives of D=u**2*v**2 with respect to a, b, and c.
      do 35 k=1,3
      ddda(k)=v2*du2da(k)
      dddb(k)=v2*du2db(k)+u2*dv2db(k)
35    dddc(k)=u2*dv2dc(k)
 
c     Compute derivatives of F=D**(-1/2) with respect to a, b, and c.
      do 40 k=1,3
      dfda(k)=-0.5d0*ddda(k)/(d*d1)
      dfdb(k)=-0.5d0*dddb(k)/(d*d1)
40    dfdc(k)=-0.5d0*dddc(k)/(d*d1)
 
 
 
      if(abs(sinphi).gt.abs(cosphi))then
c     if(.false.)then
c*************************************************
c     CASE A - use expressions derived from cosphi
c     Compute derivatives of u*v=C with respect to a, b, and c.
      do 50 k=1,3
      duvda(k)=b(k)*bc-c(k)*b2
      duvdb(k)=a(k)*bc+c(k)*ab-2.d0*b(k)*ac
50    duvdc(k)=b(k)*ab-a(k)*b2
c     Compute derivatives of cosphi=(u*v)*F=C*F with respect to a, b, and c.
      do 60 k=1,3
      dcosphida(k)=duvda(k)*f+uv*dfda(k)
      dcosphidb(k)=duvdb(k)*f+uv*dfdb(k)
60    dcosphidc(k)=duvdc(k)*f+uv*dfdc(k)
c     Compute derivatives of phi with respect to a, b, and c.
      do 70 k=1,3
      dphida(k)=-dcosphida(k)/sinphi
      dphidb(k)=-dcosphidb(k)/sinphi
70    dphidc(k)=-dcosphidc(k)/sinphi
c*************************************************
      else
c*************************************************
c     CASE B - use expressions derived from sinphi
      acrossc(1)=a(2)*c(3)-a(3)*c(2)
      acrossc(2)=a(3)*c(1)-a(1)*c(3)
      acrossc(3)=a(1)*c(2)-a(2)*c(1)
      s=b1*av
c     Compute derivatives of S=b(a*v) with respect to a, b, and c.
      do 85 k=1,3
      dsda(k)=b1*v(k)
      dsdb(k)=b(k)*av/b1-b1*acrossc(k)
85    dsdc(k)=b1*u(k)
c     Compute derivatives of sinphi with respect to a, b, and c.
      do 90 k=1,3
      dsinphida(k)=dsda(k)*f+s*dfda(k)
      dsinphidb(k)=dsdb(k)*f+s*dfdb(k)
90    dsinphidc(k)=dsdc(k)*f+s*dfdc(k)
c     Compute derivatives of phi with respect to a, b, and c.
      do 95 k=1,3
      dphida(k)=dsinphida(k)/cosphi
      dphidb(k)=dsinphidb(k)/cosphi
95    dphidc(k)=dsinphidc(k)/cosphi
c*************************************************
 
      endif
 
c     Add contribution to dphi/dx array
      do 80 k=1,3
      dphidx(k  )=          -dphida(k)
      dphidx(3+k)= dphida(k)-dphidb(k)
      dphidx(6+k)= dphidb(k)-dphidc(k)
80    dphidx(9+k)= dphidc(k)
 
c     This code is for three atoms collinear; torsion undefined
      if(ierror.eq.1) then
        do 81 k=1,12
81      dphidx(k)=0.d0
      endif
 
      endif
 
      if(iop.ge.3) then
 
c     Compute second derivatives of u**2 and v**2 with respect to a, b, and c.
      do 110 i=1,3
      do 110 j=1,3
      du2dada(i,j)=2.d0*(b2*delta(i,j)-b(i)*b(j))
      du2dbdb(i,j)=2.d0*(a2*delta(i,j)-a(i)*a(j))
      du2dadb(i,j)=2.d0*(2.d0*a(i)*b(j)-b(i)*a(j)-ab*delta(i,j))
      dv2dbdb(i,j)=2.d0*(c2*delta(i,j)-c(i)*c(j))
      dv2dcdc(i,j)=2.d0*(b2*delta(i,j)-b(i)*b(j))
      dv2dbdc(i,j)=2.d0*(2.d0*b(i)*c(j)-c(i)*b(j)-bc*delta(i,j))
110   continue
 
c     Compute second derivatives of D=u**2*v**2 with respect to a, b, and c.
      do 120 i=1,3
      do 120 j=1,3
      dddada(i,j)=
     >            v2*du2dada(i,j)
      dddbdb(i,j)=dv2db(i)*du2db(j)+du2db(i)*dv2db(j)
     >           +v2*du2dbdb(i,j)  +u2*dv2dbdb(i,j)
      dddcdc(i,j)=
     >                             +u2*dv2dcdc(i,j)
      dddadb(i,j)=                 +du2da(i)*dv2db(j)
     >           +v2*du2dadb(i,j)
      dddadc(i,j)=                 +du2da(i)*dv2dc(j)
     >
      dddbdc(i,j)=                 +du2db(i)*dv2dc(j)
     >                             +u2*dv2dbdc(i,j)
120   continue
 
c     Compute second derivatives of F=D**(-1/2) with respect to a, b, and c.
      f3=f/d
      do 130 i=1,3
      do 130 j=1,3
      dfdada(i,j)=.5d0*f3*(1.5d0*ddda(i)*ddda(j)/d-dddada(i,j))
      dfdbdb(i,j)=.5d0*f3*(1.5d0*dddb(i)*dddb(j)/d-dddbdb(i,j))
      dfdcdc(i,j)=.5d0*f3*(1.5d0*dddc(i)*dddc(j)/d-dddcdc(i,j))
      dfdadb(i,j)=.5d0*f3*(1.5d0*ddda(i)*dddb(j)/d-dddadb(i,j))
      dfdadc(i,j)=.5d0*f3*(1.5d0*ddda(i)*dddc(j)/d-dddadc(i,j))
      dfdbdc(i,j)=.5d0*f3*(1.5d0*dddb(i)*dddc(j)/d-dddbdc(i,j))
130   continue
 
      if(abs(sinphi).gt.abs(cosphi))then
c     if(.false.)then
c*************************************************
c     CASE A - use expressions derived from cosphi
c     Compute second derivatives of u*v=C with respect to a, b, and c.
      do 140 i=1,3
      do 140 j=1,3
      duvdbdb(i,j)=a(i)*c(j)+c(i)*a(j)-2.d0*ac*delta(i,j)
      duvdadb(i,j)=bc*delta(i,j)+b(i)*c(j)-2.d0*c(i)*b(j)
      duvdadc(i,j)=b(i)*b(j)-b2*delta(i,j)
      duvdbdc(i,j)=ab*delta(i,j)+a(i)*b(j)-2.d0*b(i)*a(j)
140   continue
c     Compute second derivatives of cosphi=(u*v)*F=C*F with respect to a, b, and
c.
      do 150 i=1,3
      do 150 j=1,3
      dcosphidada(i,j)=
     >                +dfda(i)*duvda(j)+duvda(i)*dfda(j)+uv*dfdada(i,j)
      dcosphidbdb(i,j)=
     >  duvdbdb(i,j)*f+dfdb(i)*duvdb(j)+duvdb(i)*dfdb(j)+uv*dfdbdb(i,j)
      dcosphidcdc(i,j)=
     >                +dfdc(i)*duvdc(j)+duvdc(i)*dfdc(j)+uv*dfdcdc(i,j)
      dcosphidadb(i,j)=
     >  duvdadb(i,j)*f+dfda(i)*duvdb(j)+duvda(i)*dfdb(j)+uv*dfdadb(i,j)
      dcosphidadc(i,j)=
     >  duvdadc(i,j)*f+dfda(i)*duvdc(j)+duvda(i)*dfdc(j)+uv*dfdadc(i,j)
      dcosphidbdc(i,j)=
     >  duvdbdc(i,j)*f+dfdb(i)*duvdc(j)+duvdb(i)*dfdc(j)+uv*dfdbdc(i,j)
150   continue
c     Compute second derivatives of phi with respect to a, b, and c.
      z1=-cosphi/(sinphi**3)
      z2=-1.d0/sinphi
      do 160 i=1,3
      do 160 j=1,3
      dphidada(i,j)=z1*dcosphida(i)*dcosphida(j)+z2*dcosphidada(i,j)
      dphidbdb(i,j)=z1*dcosphidb(i)*dcosphidb(j)+z2*dcosphidbdb(i,j)
      dphidcdc(i,j)=z1*dcosphidc(i)*dcosphidc(j)+z2*dcosphidcdc(i,j)
      dphidadb(i,j)=z1*dcosphida(i)*dcosphidb(j)+z2*dcosphidadb(i,j)
c     note dcosphidadc is non zero but dphidadc is zero - don't compute
      dphidadc(i,j)=z1*dcosphida(i)*dcosphidc(j)+z2*dcosphidadc(i,j)
      dphidbdc(i,j)=z1*dcosphidb(i)*dcosphidc(j)+z2*dcosphidbdc(i,j)
160   continue
      else
c*************************************************
c     CASE B - use expressions derived from sinphi
c     Compute the product of the antisymmetric tensor times a, b, and c
      call antsym(a,atilde)
      call antsym(b,btilde)
      call antsym(c,ctilde)
c     Compute second derivatives of S=b(a*v) with respect to a, b, and c.
      b3=b1*b2
      do 210 i=1,3
      do 210 j=1,3
      dsdbdb(i,j)=(delta(i,j)/b1-b(i)*b(j)/b3)*av
     >            -(acrossc(i)*b(j)+acrossc(j)*b(i))/b1
      dsdadb(i,j)=v(i)*b(j)/b1+b1*ctilde(i,j)
      dsdadc(i,j)=-b1*btilde(i,j)
      dsdbdc(i,j)=b(i)*u(j)/b1+b1*atilde(i,j)
210   continue
c     Compute second derivatives of sinphi with respect to a, b, and c.
      do 220 i=1,3
      do 220 j=1,3
      dsinphidada(i,j)=
     >                +dfda(i)*dsda(j)+dsda(i)*dfda(j)+s*dfdada(i,j)
      dsinphidbdb(i,j)=
     >  dsdbdb(i,j)*f+dfdb(i)*dsdb(j)+dsdb(i)*dfdb(j)+s*dfdbdb(i,j)
      dsinphidcdc(i,j)=
     >                +dfdc(i)*dsdc(j)+dsdc(i)*dfdc(j)+s*dfdcdc(i,j)
      dsinphidadb(i,j)=
     >  dsdadb(i,j)*f+dfda(i)*dsdb(j)+dsda(i)*dfdb(j)+s*dfdadb(i,j)
      dsinphidadc(i,j)=
     >  dsdadc(i,j)*f+dfda(i)*dsdc(j)+dsda(i)*dfdc(j)+s*dfdadc(i,j)
      dsinphidbdc(i,j)=
     >  dsdbdc(i,j)*f+dfdb(i)*dsdc(j)+dsdb(i)*dfdc(j)+s*dfdbdc(i,j)
220   continue
c     Compute second derivatives of phi with respect to a, b, and c.
      z1=sinphi/cosphi**3
      z2=1.d0/cosphi
      do 230 i=1,3
      do 230 j=1,3
      dphidada(i,j)=z1*dsinphida(i)*dsinphida(j)+z2*dsinphidada(i,j)
      dphidbdb(i,j)=z1*dsinphidb(i)*dsinphidb(j)+z2*dsinphidbdb(i,j)
      dphidcdc(i,j)=z1*dsinphidc(i)*dsinphidc(j)+z2*dsinphidcdc(i,j)
      dphidadb(i,j)=z1*dsinphida(i)*dsinphidb(j)+z2*dsinphidadb(i,j)
c     note dsinphidadc is non zero but dphidadc is zero - don't compute
      dphidadc(i,j)=z1*dsinphida(i)*dsinphidc(j)+z2*dsinphidadc(i,j)
      dphidbdc(i,j)=z1*dsinphidb(i)*dsinphidc(j)+z2*dsinphidbdc(i,j)
230   continue
      end if
 
c     Add contributions to second derivative matrix
c     Derivative with respect to r1,r1:
      do 21 i=1,3
      do 21 j=1,i
      index=lowtri(i,j)
      d2phidx2(index)=dphidada(i,j)
21    continue
c     Derivative with respect to r2,r2:
      do 22 i=1,3
      do 22 j=1,i
      index=lowtri(i+3,j+3)
      d2phidx2(index)=dphidada(i,j)+dphidbdb(i,j)
     >               -dphidadb(i,j)-dphidadb(j,i)
22    continue
c     Derivative with respect to r3,r3:
      do 23 i=1,3
      do 23 j=1,i
      index=lowtri(i+6,j+6)
      d2phidx2(index)=dphidbdb(i,j)+dphidcdc(i,j)
     >               -dphidbdc(i,j)-dphidbdc(j,i)
23    continue
c     Derivative with respect to r4,r4:
      do 24 i=1,3
      do 24 j=1,i
      index=lowtri(i+9,j+9)
      d2phidx2(index)=dphidcdc(i,j)
24    continue
c     Derivative with respect to r1,r2:
      do 25 i=1,3
      do 25 j=1,3
      index=lowtri(j+3,i)
      d2phidx2(index)=-dphidada(i,j)+dphidadb(i,j)
25    continue
c     Derivative with respect to r1,r3:
      do 26 i=1,3
      do 26 j=1,3
      index=lowtri(j+6,i)
      d2phidx2(index)=-dphidadb(i,j)+dphidadc(i,j)
26    continue
c     Derivative with respect to r1,r4:
      do 27 i=1,3
      do 27 j=1,3
      index=lowtri(j+9,i)
      d2phidx2(index)=-dphidadc(i,j)
c
c     note: dphidadc is always zero - this array and its computation
c     should be removed later
c
c      write(*,887) i,j,index,dphidadc(i,j)
c887   format(' r1,r4 block: i,j=',2i2,' index=',i4,' dphidadc=',f10.4)
27    continue
c     Derivative with respect to r2,r3:
      do 28 i=1,3
      do 28 j=1,3
      index=lowtri(j+6,i+3)
      d2phidx2(index)=
     >               +dphidadb(i,j)-dphidadc(i,j)
     >               -dphidbdb(i,j)+dphidbdc(i,j)
28    continue
c     Derivative with respect to r2,r4:
      do 29 i=1,3
      do 29 j=1,3
      index=lowtri(j+9,i+3)
      d2phidx2(index)=dphidadc(i,j)-dphidbdc(i,j)
29    continue
c     Derivative with respect to r3,r4:
      do 30 i=1,3
      do 30 j=1,3
      index=lowtri(j+9,i+6)
      d2phidx2(index)=dphidbdc(i,j)-dphidcdc(i,j)
30    continue
 
c     code for three atoms colinear
      if(ierror.eq.1) then
        do 45 i=1,78
45      d2phidx2(i)=0.d0
      endif
 
      endif
 
 
      end



c
c     Subroutine to compute the product of the antysimmetric
c     tensor times a vector. The result is an antisymmetric tensor.
      subroutine antsym(x,xtilde)
      implicit real*8 (a-h,o-z)
      dimension x(3),xtilde(3,3)
      xtilde(1,1)=0.d0
      xtilde(2,2)=0.d0
      xtilde(3,3)=0.d0
      xtilde(1,2)=x(3)
      xtilde(2,3)=x(1)
      xtilde(3,1)=x(2)
      xtilde(2,1)=-x(3)
      xtilde(3,2)=-x(1)
      xtilde(1,3)=-x(2)
      return
      end

c
c     subroutine to fix up the template for arms so that the
c     torsion angles of the CG model are preserved
c     this requires that a zmatrix form of the arm template
c     exist with the required torsions positioned first
c     this requires knowledge site ordering of the fine 
c     grain (template) in the pdb file as well as of the 
c     coarse grain model (in the cgclus file).  
c
      subroutine armfix(cgclus,iarmlen,xtemp,ntempatmx)
      implicit real*8 (a-h,o-z)
      character*256 line,linez
      character*120 name(4)
      dimension cgclus(3,iarmlen)
      dimension xtemp(3,ntempatmx,*)
      dimension dum1(12),dum2(78),ref(3,3)

      character*120 zname(5000)
      dimension izmat(5000,3),zmat(5000,3)
      dimension coord(3,5000)

      pi=4.d0*datan(1.d0)

      natmax=5000
c     format of each line of *.remap file is:   name name dist name angle name torsion
c     encoded in the name field is the final ordering desired after the coordinates have
c     been computed using the desired torsion angles

      open(40,file='pvlpegarm.remap')  !ultimately make this an input that goes with the pdb file
                                       !names must match; number of sites must match

      nline=0
10    continue
      read(40,'(a256)',end=50) line
      if(line(1:10).eq.'          ') go to 50
      nline=nline+1
      if(nline.gt.natmax) then
        write(*,'(''Error:  increase storate in armfix'')')
        stop 'Error:  increase storate in armfix'
      endif

c     read in a line of the zmatrix 
      zname(nline)=' '
      do k=1,3
        izmat(nline,k)=0
        zmat(nline,k)=0.d0
      enddo

c     parse the line into name name dist name angle name torsion

      call leftjust(line)
      ie1=index(line(1:),' ')-1    !first field is in 1:ie1

c     write(*,'(''Field 1: *'',a,''*'')')
c    x  line(1:ie1)
      read(line(1:ie1),'(a120)') zname(nline)   !this field is always there
      name(1)=zname(nline)
      nele=0

      if(nline.gt.1) then
        call leftjust(line(ie1+2:))
        ie2=index(line(ie1+2:),' ')+ie1  !second field is in ie1+2:ie2
        call leftjust(line(ie2+2:))
        ie3=index(line(ie2+2:),' ')+ie2  !third field is in ie2+2:ie3
c       write(*,'(''Field 2: *'',a,''*'')')
c    x    line(ie1+2:ie2)
        name(2)=line(ie1+2:ie2)
c       write(*,'(''Field 3: *'',a,''*'')')
c    x    line(ie2+2:ie3)
        read(line(ie2+2:ie3),*) rab
        zmat(nline,1)=rab
        nele=1
      endif

      if(nline.gt.2) then
        call leftjust(line(ie3+2:))
        ie4=index(line(ie3+2:),' ')+ie3  !fourth field is in ie3+2:ie4
        call leftjust(line(ie4+2:))
        ie5=index(line(ie4+2:),' ')+ie4  !fifth field is in ie4+2:ie5
c       write(*,'(''Field 4: *'',a,''*'')')
c    x    line(ie3+2:ie4)
        name(3)=line(ie3+2:ie4)
c       write(*,'(''Field 5: *'',a,''*'')')
c    x    line(ie4+2:ie5)
        read(line(ie4+2:ie5),*) ang
        zmat(nline,2)=ang
        nele=2
      endif

      if(nline.gt.3) then
        call leftjust(line(ie5+2:))
        ie6=index(line(ie5+2:),' ')+ie5  !sixth field is in ie5+2:ie6
        call leftjust(line(ie6+2:))
        ie7=index(line(ie6+2:),' ')+ie6  !seventh field is in ie6+2:ie7
c       write(*,'(''Field 6: *'',a,''*'')')
c    x    line(ie5+2:ie6)
        name(4)=line(ie5+2:ie6)
c       write(*,'(''Field 7: *'',a,''*'')')
c    x    line(ie6+2:ie7)
        read(line(ie6+2:ie7),*) dih
        zmat(nline,3)=dih
        nele=3
      endif



      do i=1,nele
        ifound=0
        it=index(name(i+1),' ')-1
        do j=1,nline-1
          is=index(zname(j),' ')-1
          if(name(i+1)(1:it).eq.zname(j)(1:is)) then
            ifound=j
          endif
        enddo
        if(ifound.eq.0) then
          write(*,'(''Error:  name not found'')')
          stop 'Error: name not found'
        endif
        izmat(nline,i)=ifound
      enddo

c     write(*,'(''Remap Zmatrix:  '',
c    x  a,3(5x,i4,2x,a,2x,f10.4))')
c    x  name(1)(1:index(name(1),' ')-1),

c    x  izmat(nline,1),
c    x  name(2)(1:index(name(2),' ')-1),
c    x  zmat(nline,1),

c    x  izmat(nline,2),
c    x  name(3)(1:index(name(3),' ')-1),
c    x  zmat(nline,2),

c    x  izmat(nline,3),
c    x  name(4)(1:index(name(4),' ')-1),
c    x  zmat(nline,3)
      

      go to 10



50    continue
      close(40)



c     entire zmatrix has been read in

c     write out the CG sites and coordinate for this arm
c     do k=1,iarmlen
c       write(*,'(''CG arm site '',i3,
c    x    '' Coords '',3f10.4)')
c    x    k,(cgclus(kk,k),kk=1,3)
c     enddo

c     replace some of the torsion angles
c     this should be done based on a mapping; however,
c     the remap zmatrix that was just read in should have
c     been structured to support replacement of the first
c     iarmlen-3 torsions with those from the arms in the 
c     cgclus file
c
c     remap file should include the maximum number of torsions
c     that can be replaced; this is three less than the number of
c     heavy atoms in the backbone.
c     NOTE: have to deal with possibility that there are
c     different number of torsions in cgclus than can be
c     assigned in the remap file.
c     1) normal case:  number in remap file >= number in cgclus
c        when replacing short arms in cgclus with longer ones
c        just replace the first iarmlen-3 torsions
c     2) less usual case:  number in remap file < number in cgclus
c        when replacing long arms in cgclus with shorter ones
c        just replace the first nbackbone-3

      nbackbone=96   !actually much longer for PVL16-PEG24
      nreplace=min(nbackbone-3,iarmlen-3)

      do k=1,nreplace  
        call dihint(1,  cgclus(1,k),cgclus(1,k+1),
     x                  cgclus(1,k+2),cgclus(1,k+3),
     x       phi,dum1,dum2)
        write(*,'(''CG arm torsion angle for sites '',4i5,f10.4)')
     x    k,k+1,k+2,k+3,phi*180.d0/pi
        write(*,'(''Replace remap torsion '',f10.4,'' with '',
     x    f10.4)') zmat(k+3,3),phi*180.d0/pi
        zmat(k+3,3)=phi*180.d0/pi
      enddo

c     now build the new cartesian coordinates
      nw=0
      do i=1,nline
        nw=max(nw,index(zname(i),' ')-1)
      enddo

      do i=1,nline
        if(i.eq.1) then
          coord(1,1)=0.d0
          coord(2,1)=0.d0
          coord(3,1)=0.d0
        elseif(i.eq.2) then
          coord(1,2)=zmat(i,1)
          coord(2,2)=0.d0
          coord(3,2)=0.d0
        elseif(i.eq.3) then
          if(izmat(i,1).eq.2) then ! 3-2-1
            coord(1,i)=coord(1,2)+
     x                 zmat(i,1)*dcos((pi/180.d0)*(180.d0-zmat(i,2)))
            coord(2,i)=zmat(i,1)*dsin((pi/180.d0)*(180.d0-zmat(i,2)))
          else                     ! 3-1-2
            coord(1,i)=zmat(i,1)*dcos((pi/180.d0)*zmat(i,2))
            coord(2,i)=zmat(i,1)*dsin((pi/180.d0)*zmat(i,2))
          endif
          coord(3,i)=0.d0
        elseif(i.gt.3) then
          do j=1,3
            isite=izmat(i,j)
            do k=1,3
              ref(k,4-j)=coord(k,isite)
            enddo
          enddo
          rab=zmat(i,1)
          ang=zmat(i,2)
          dih=zmat(i,3)
c         place expects to get ref organized in order dihed, angle, dist (D1,D2,D3)
          call place(ref,rab,ang,dih,coord(1,i))
        endif

        write(*,'(a,2x,3f12.6)') zname(i)(1:nw),
     x    (coord(k,i),k=1,3)
      enddo





c     sort the coordinate into the correct order
      do i=1,nline
        is1=index(zname(i)(1:),'_')
        is2=index(zname(i)(is1+1:),'_')+is1
        read(zname(i)(is1+1:is2-1),*) iptr
        write(*,'(''Sequence '',i5,'' Name '',a,
     x    '' Extract field '',i5)')
     x    i,zname(i)(1:index(zname(i),' ')-1),
c    x    zname(i)(is1+1:is2-1),
     x    iptr
        do j=1,3
          xtemp(j,iptr,1)=coord(j,i)
        enddo
      enddo










      end





c
c     subroutine to place a site given a row of z-matrix and coordinates for
c     the three sites it references
c     this is really a wrapper for i2x from Mulliken, where we use it to
c     position one site at a time
c
      subroutine place(ref,rab,ang,dih,coord)
      implicit real*8 (a-h,o-z)
      dimension ref(3,3),coord(3)
      dimension xyz(3,4),lnk(3,4),rabx(4),angx(4),dihx(4) !local
      pi=4.d0*datan(1.d0)
c
c     first column of ref holds (D1) site referenced for dihedral
c     second column of ref holds (D2) site referenced for angle
c     third column of ref holds (D3) site referenced for distance

      lnk(1,1)=0  !not used
      lnk(2,1)=0  !not used
      lnk(3,1)=0  !not used

      lnk(1,2)=1
      lnk(2,2)=0  !not used
      lnk(3,2)=0  !not used

      lnk(1,3)=2
      lnk(2,3)=1
      lnk(3,3)=0  !not used

      lnk(1,4)=3
      lnk(2,4)=2
      lnk(3,4)=1

      do i=1,3
        do j=1,3
          xyz(j,i)=ref(j,i)
        enddo
      enddo

c
c     write(*,'(/,'' Ref coords before call to i2x: '')')
c     do i=1,3
c       write(*,'('' Site '',i1,2x,3f10.4)') i,(xyz(k,i),k=1,3)
c     enddo
c     write(*,'('' r,theta,phi = '',3f10.5)') rab,ang,dih

      nat=4
      istart=4
      iend=4
      rabx(4)=rab
      angx(4)=(pi/180.d0)*ang
      dihx(4)=(pi/180.d0)*dih

      call i2x(nat,lnk,rabx,angx,dihx,istart,iend,xyz)

      do i=1,3
        coord(i)=xyz(i,4)
      enddo

c     write(*,'('' Site '',i1,2x,3f10.4)') 4,(xyz(k,4),k=1,3)



      end


c Change History:
c $Log: util.f,v $
c Revision 1.8  1994/08/03  19:49:54  swope
c Changed behavior of xyz2int so that zmatricies are not printed out tha
c undefined torsions.  When angles are close to 180 they are changed to
c when close to 0 they are changed to 0.0001.    (Swope Aug 3, 1994)
c
c Revision 1.6  1994/07/30  06:33:09  swope
c Several changes by Swope to internal-cartesian and cartesian-internal
c conversion routines:
c angint-switch to low angle expression on new threshold
c dihint-call tlkemsg when atoms too close
c xyz2int-change messages during certain errors
c i2x-changes in ways linear systems treated: two strikes policy
c     for generating cartesians with undefined torsions
c xyz2dih-added new return code to flag 1-2-3 linear vs 2-3-4 linear
c
c Revision 1.5  1994/07/07  16:34:02  gclie
c This version includes xyz2int and int2xyz from Swope.  It should repla
c
c*arc i2x
      subroutine i2x(nat,lnk,rab,ang,dih,istart,iend,xyz)
c
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Copyright (c) 1994 International Business Machines Corporation
C  All rights reserved
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C  Description      Given the link vector and internal coordinates
c                     of a list of particles, this subroutine computes
c                     the cartesian coordinates for a subset of the
c                     particles, those with indicies from istart to
c                     iend.  The units of the cartesian coordinates
c                     will be the same as those of the input internals.
c                     The units of the angles are assumed to be radians.
c                   If the particle indicies between istart and iend
c                     fall in the range 1,2 or 3, particle 1 is placed
c                     at the origin, particle 2 is placed on the x-axis,
c                     and particle 3 is placed in the x-y plane.
c
C
C  Input
C
C     nat           Number of atoms in list
C
C     lnk(3,nat)    link vector for describing internals
C                     lnk(1,i) gives label for distance from i
C                     lnk(2,i) gives label for angle i-lnk(1,i)-lnk(2,i)
C                     lnk(3,i) gives label for torsion
C                     i-lnk(1,i)-lnk(2,i)-lnk(3,i)
C                     Note that first six entries are ignored.
C
C     rab(nat)      Vector of distances.
C
C     ang(nat)      Vector of angles.  Range returned is 0.le.ang.le.pi.
C
C     dih(nat)      Vector of torsions. Range returned is
C                   -pi .lt. phi .le. pi, and follows IUPAC
C                   sign and range convention.
c
c     istart        The index of the first atom whose cartesian
c                   coordinates are to be computed.  istart must be
c                   an integer with ( 1 .le. istart .le. nat ).
c
c
c     iend          The index of the last atom whose cartesian
c                   coordinates are to be computed.  iend must be
c                   an integer with ( 1 .le. iend .le. nat ).
C
C
C  Output
C
C     xyz(3,i)      cartesian coordinates of atoms i=istart to iend
C
C
C  Linkage          List of subroutines called by this one
C
C  Programmer       William Swope (some from Carnevali and Nesbet)
C
C  Date             June 1994
C
C  Update log       Record of all updates including names of programmers
C                     descriptions of changes made, and dates of change.
C                   07-29-94 Modified by Swope to die (tlkemsg) if
C                            there are TWO invalid torsion angles.
C                            Invalid torsion angles are those that
C                            result in ill-defined cartesian coordinates
C                            This change still allows linear molecules
C                            and molecules with two long linear segments
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
c
      Implicit Real*8 (a, b, d-h, o, p, r-y)
      Implicit Integer (i-n)
c     Violation of ARCHEM conventions, but there are no logicals here
c     Implicit Logical (z)
      Implicit Real*8 (z)
      Implicit Character (c)
c     Implicit Undefined (q)

      character cproduct*48
      dimension cproduct(3)

c     Input and output arrays
      real*8 xyz(3,nat),rab(nat),ang(nat),dih(nat)
      integer lnk(3,nat)

c     Local arrays
c     Following arrays give molecule coordinate system relative to lab
      real*8 xhat(3),yhat(3),zhat(3),zprev(3)
      real*8 r12(3),r23(3),r43(3)


C     THE SIGN CONVENTION FOR PHI IS AS FOLLOWS:
C     NUMBER THE ASSOCIATED ATOMS 1, 2, 3, 4.  ATOMS 1 AND 4 ARE
C     "TERMINAL" ATOMS AND THE DIHEDRAL ANGLE IS ABOUT THE BOND
C     BETWEEN ATOMS 2 AND 3.  IMAGINE HOLDING THE PLANE FORMED BY
C     ATOMS 1, 2, AND 3 FIXED AND ROTATING THE PLANE FORMED BY
C     ATOMS 2, 3, AND 4 ABOUT A VECTOR THAT POINTS FROM ATOM 2
C     TO ATOM 3.  THE POSITIVE SENSE CORRESPONDS TO THE RIGHT-HAND
C     RULE FOR ROTATION ABOUT THE VECTOR FROM ATOM 2 TO ATOM 3.
c     This agrees with the IUPAC convention.
C
C     FURTHERMORE, PHI=0 CORRESPONDS TO A CONDITION WHERE ALL FOUR
C     ATOMS ARE ON A PLANE AND ATOMS 1 AND 4 ARE "CIS" TO THE BOND
C     BETWEEN ATOMS 2 AND 3.  LIKEWISE, PHI=180 DEGREES CORRESPONDS
C     A CONDITION WHERE ALL FOUR ATOMS ARE ON A PLANE AND ATOMS 1 AND
C     4 ARE "TRANS" TO THE BOND BETWEEN ATOMS 2 AND 3.

c     comment out the following call to check the integrity of the
c     link vector.  this call is appropriate in int2xyz, where the
c     full set of cartesians is computed, but should probably not
c     be made over and over again if the cartesians are computed
c     one at a time or in small increments.
c
c     check validity of link vector
c     call chklnk(nat,lnk,iret)
c     if(iret.ge.2) then
c       print *, ' Error: link matrix is not usable for determination',
c    >           ' of cartesian coordinates.'
c       print *, ' Complete link vector: '
c       write(*,'(3i10)') ((lnk(i,j),i=1,3),j=1,nat)
c       call tlkemsg(' Invalid link matrix.','int2xyz')
c     endif


c     lnk(1,i),i=1,nat specifies atom indicies for "bond" distances
c     lnk(2,i),i=1,nat specifies atom indicies for "bond" angles
c     lnk(3,i),i=1,nat specifies atom indicies for dihedral angles

      pi=4.d0*datan(1.d0)

      if(istart.le.0   .or.
     >   istart.gt.nat .or.
     >   iend.le.0     .or.
     >   iend.gt.nat   .or.
     >   istart.gt.iend ) then
      write(*,'('' Error in subroutine i2x: invalid arguments for'',/,
     >          '' atom indicies: istart='',i5,'' iend='',i5,/,
     >          '' Values should be from 1 to nat='',i5)')
     >          istart,iend,nat
C     CALL TLKEMSG(' INVALID ATOM INDICES PASSED TO X2I.','X2I')
      endif

      zprev(1)=0.d0
      zprev(2)=0.d0
      zprev(3)=1.d0

c     failure count on placing atoms when torsion undefined
      nstrike=0


      do 10 i=istart,iend

      if(i.eq.1) then
c     if first atom
        xyz(1,1)=0.d0
        xyz(2,1)=0.d0
        xyz(3,1)=0.d0
      elseif(i.eq.2) then
c     if second atom
        xyz(1,2)=rab(2)
        xyz(2,2)=0.d0
        xyz(3,2)=0.d0
      elseif(i.eq.3) then
c     if third atom (note that normally 0.le.ang.le.pi)
        if(lnk(1,3).eq.2) then
          xyz(1,3)=rab(2) -rab(3)*cos(ang(3))
        else
          xyz(1,3)=       +rab(3)*cos(ang(3))
        endif
        xyz(2,3)=         +rab(3)*sin(ang(3))
        xyz(3,3)=0.d0
        if(abs(ang(3)).gt.1.d-3 .and.
     >     abs(abs(ang(3))-pi).gt.1.d-3) nstrike=1
      endif

c     for fourth or higher atom
      if(i.le.3) go to 10

      l2=lnk(1,i)
      l3=lnk(2,i)
      l4=lnk(3,i)
      r23(1)=xyz(1,l2)-xyz(1,l3)
      r23(2)=xyz(2,l2)-xyz(2,l3)
      r23(3)=xyz(3,l2)-xyz(3,l3)

c     local x axis is directed from atom l3 to atom l2
      d=r23(1)**2+r23(2)**2+r23(3)**2
      if(d.le.1.d-10) then
        print *, ' Error: Atoms too close to establish unique',
     >           ' directions for cartesian placement from z-matrix.'
        print *, ' Atom indicies: ',l2,l3
C       CALL TLKEMSG(' ATOMS TOO CLOSE.','I2X')
      endif
      d=1.d0/sqrt(d)
      xhat(1)=r23(1)*d
      xhat(2)=r23(2)*d
      xhat(3)=r23(3)*d

c     local z axis is directed along r23 cross r43, if this is non-zero
      r43(1)=xyz(1,l4)-xyz(1,l3)
      r43(2)=xyz(2,l4)-xyz(2,l3)
      r43(3)=xyz(3,l4)-xyz(3,l3)
      zhat(1)=r23(2)*r43(3)-r23(3)*r43(2)
      zhat(2)=r23(3)*r43(1)-r23(1)*r43(3)
      zhat(3)=r23(1)*r43(2)-r23(2)*r43(1)
      d=zhat(1)**2+zhat(2)**2+zhat(3)**2
c
      if(d.le.1.d-8) then
        if(abs(ang(i)).gt.1.d-3 .and.
     >     abs(abs(ang(i))-pi).gt.1.d-3) then
c         Problem: angle 4-3-2 is (nearly) linear AND ang(i) is other
c         than zero or pi.  This means placement of particle 1 is
c         undefined.  The first time this happens, we will allow it,
c         and place particle 1 in the (local) x-y plane.  This
c         corresponds to setting zhat to the lab z axis.
          if(nstrike.ne.0) then
          print *
          print *, ' A sequence of bond angles near 180 (or 0) degrees'
          print *, ' has resulted in an undefined torsion angle and'
          print *, ' this torsion angle must be defined to determine'
          print *, ' unique Cartesian coordinates for the molecule.'
          print *
          print *, ' Offending link vector:'
          print *, ' lnk(1,',i,') = ',l2
          print *, ' lnk(2,',i,') = ',l3
          print *, ' lnk(3,',i,') = ',l4
          print *
C         CALL TLKEMSG('CANNOT GENERATE CARTESIAN COORDINATES.','I2X')
          endif
          nstrike=1
        endif
        zhat(1)=zprev(1)
        zhat(2)=zprev(2)
        zhat(3)=zprev(3)
      else
        d=1.d0/sqrt(d)
        zhat(1)=zhat(1)*d
        zhat(2)=zhat(2)*d
        zhat(3)=zhat(3)*d
        zprev(1)=zhat(1)
        zprev(2)=zhat(2)
        zprev(3)=zhat(3)
      endif

c     local y axis is zhat cross xhat
      yhat(1)=zhat(2)*xhat(3)-zhat(3)*xhat(2)
      yhat(2)=zhat(3)*xhat(1)-zhat(1)*xhat(3)
      yhat(3)=zhat(1)*xhat(2)-zhat(2)*xhat(1)
      d=1.d0/sqrt(yhat(1)**2+yhat(2)**2+yhat(3)**2)
      yhat(1)=yhat(1)*d
      yhat(2)=yhat(2)*d
      yhat(3)=yhat(3)*d

      if(abs(ang(i)).gt.1.d-3 .and.
     >   abs(abs(ang(i))-pi).gt.1.d-3) nstrike=1

c     in molecule frame (i.e., relative to xhat,yhat,zhat coord system)
c     new center is placed at ...
      x=-rab(i)*cos(ang(i))
      rho=rab(i)*sin(ang(i))
      y=rho*cos(dih(i))
      z=rho*sin(dih(i))

c     in lab frame ...
      xyz(1,i) = xyz(1,l2) + x*xhat(1)+y*yhat(1)+z*zhat(1)
      xyz(2,i) = xyz(2,l2) + x*xhat(2)+y*yhat(2)+z*zhat(2)
      xyz(3,i) = xyz(3,l2) + x*xhat(3)+y*yhat(3)+z*zhat(3)

10    continue

      end

