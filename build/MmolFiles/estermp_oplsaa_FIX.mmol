%
% estermp:  methyl propanoate
%
% modification of the file esterea_oplsaa.mmol  (9/2010)
% to produce methyl propanoate
%
% October 7, 2011
%
% THIS IS A VERSION OF estermp_oplsaa.mmol (DEVELOPED FOR MAKING PLA CHAINS)
% WITH THREE FIXES 
% (1) C_3-C_5-C_6-H_(10,11,12) torsion for HC-CT-CT-C was previously wrong
%     used amplitude 0.15 with Torsion_type_7 when it should have been -0.05
%     This only affects methyl group rotation in PLA; probably minor effect
% (2) O_4-C_3-C_5-C_6 torsion for CT-CT-C -O previously had zero amplitude
%     should have been amplitudes (-0.1385,0.614,-0.347) for Torsion_Type 3,6,7
%     This may affect chain flexibility
% (3) H_13 and H_14 had incorrect LJ parameters.  Were 2.806155/0.03 (alkane HC)
%     but should have been 2.716358/0.015 (alkoxy HC)
%     
% NOTE: I included an improper torsion, but not sure I should have on C_5-O_2-C_3-O_4
%     with TorsionType_6 and amplitude 10.5
%

%
% sources:  Price, Ostrovsky, Jorsensen JCC v22  p1340 (2001) on esters nitriles nitro
%           parameters in /fr0/boss/oplsaa.sb and oplsaa.par
%
%
% needed to describe the repeat unit in PLA (which has an extra carbon on the acid side)

% esterea:  CH3-CH2-O-CO-CH3
% esterep:  CH3-CH2-O-CO-CH2-CH3  
% estermp:  CH3-O-CO-CH2-CH3  (the last CH2 is the connection point for the chain)


% modification of esterea to make estermp removes -CH2- group from alcohol side and 
% puts it on the acid side; some parameters dropped, others taken from the oplsaa files
% files are consistent with the paper by Price


Molecule estermp_oplsaa
     AutoTopology= No
     Topology=
Single C_1-O_2
Single O_2-C_3
Double C_3-O_4
Single C_3-C_5
Single C_5-C_6
Single C_1-H_7
Single C_1-H_8
Single C_1-H_9
Single C_6-H_10
Single C_6-H_11
Single C_6-H_12
Single C_5-H_13
Single C_5-H_14

     Geometry=
  C_1
  O_2  C_1 1.41
  C_3  O_2 1.327  C_1  116.9
  O_4  C_3 1.229  C_3  123.4  C_1 0.0
  C_5  C_3 1.522  O_2  111.4  C_1 180.
  C_6  C_5 1.529  C_3  109.5  O_2 60.
  H_7  C_1 1.09   O_2  109.5  C_3 60.
  H_8  C_1 1.09   O_2  109.5  C_3 -60.
  H_9  C_1 1.09   O_2  109.5  C_3 180.
  H_10 C_6 1.09   C_5  109.5  C_3 60. 
  H_11 C_6 1.09   C_5  109.5  C_3 -60. 
  H_12 C_6 1.09   C_5  109.5  C_3 180.
  H_13 C_5 1.09   C_3  109.5  O_2 180.
  H_14 C_5 1.09   C_3  109.5  O_2 -60.

Constraint
     H_7-C_1
     H_8-C_1
     H_9-C_1
     H_10-C_6
     H_11-C_6
     H_12-C_6
     H_14-C_5
     H_13-C_5


Forcefield =
     Generic
     Geom_Mean_vdW_Radii
     InterFF_Priority 1
     Scale_1_4_vdw 0.5
     Scale_1_4_cou 0.5
Harmonic_Bond O_2-C_1 320.0000 1.4100
Harmonic_Bond C_3-O_2 214.0000 1.3270
Harmonic_Bond C_3-O_4 570.0000 1.2290
Harmonic_Bond C_3-C_5 317.0000 1.5220
Harmonic_Bond C_5-C_6 268.0000 1.5290
Harmonic_Bond H_7-C_1 340.0000 1.0900
Harmonic_Bond H_8-C_1 340.0000 1.0900
Harmonic_Bond H_9-C_1 340.0000 1.0900
Harmonic_Bond H_10-C_6 340.0000 1.0900
Harmonic_Bond H_11-C_6 340.0000 1.0900
Harmonic_Bond H_12-C_6 340.0000 1.0900
Harmonic_Bond H_13-C_5 340.0000 1.0900
Harmonic_Bond H_14-C_5 340.0000 1.0900
Harmonic_Angle C_6-C_5-H_13 37.5000 1.9320794820
Harmonic_Angle C_6-C_5-H_14 37.5000 1.9320794820
Harmonic_Angle C_5-C_6-H_10 37.5000 1.9320794820
Harmonic_Angle C_5-C_6-H_11 37.5000 1.9320794820
Harmonic_Angle C_5-C_6-H_12 37.5000 1.9320794820
Harmonic_Angle C_3-C_5-H_14 35.0000 1.9111355309
Harmonic_Angle C_3-C_5-H_13 35.0000 1.9111355309
Harmonic_Angle O_2-C_1-H_7  35.0000 1.9111355309
Harmonic_Angle O_2-C_1-H_8  35.0000 1.9111355309
Harmonic_Angle O_2-C_1-H_9  35.0000 1.9111355309
Harmonic_Angle H_7-C_1-H_8 33.0000 1.8814649336
Harmonic_Angle H_7-C_1-H_9 33.0000 1.8814649336
Harmonic_Angle H_8-C_1-H_9 33.0000 1.8814649336
Harmonic_Angle H_10-C_6-H_11 33.0000 1.8814649336
Harmonic_Angle H_10-C_6-H_12 33.0000 1.8814649336
Harmonic_Angle H_11-C_6-H_12 33.0000 1.8814649336
Harmonic_Angle H_13-C_5-H_14 33.0000 1.8814649336
Harmonic_Angle C_1-O_2-C_3 83.0000 2.0402898956   ! oplsaa C -OS-CT 116.9
Harmonic_Angle O_2-C_3-C_5 81.0000 1.9442967867   ! oplsaa OS-C -CT 111.4
Harmonic_Angle O_2-C_3-O_4 83.0000 2.1537362970   ! oplsaa O -C -OS 123.4
Harmonic_Angle C_5-C_3-O_4 80.0000 2.1013764194   ! oplsaa CT-C -O  120.4
Harmonic_Angle C_3-C_5-C_6 63.0000 1.9390608000   ! oplsaa C -CT-CT 111.1
Torsion_type_7 H_7-C_1-O_2-C_3   0.099000000000000 ! C -OS-CT-HC ester 
Torsion_type_7 H_8-C_1-O_2-C_3   0.099000000000000 ! C -OS-CT-HC ester
Torsion_type_7 H_9-C_1-O_2-C_3   0.099000000000000 ! C -OS-CT-HC ester
Torsion_type_6 C_1-O_2-C_3-O_4   2.562000000000000 ! O -C -OS-CT ester
Torsion_type_3 C_1-O_2-C_3-C_5   2.334500000000000 ! CT-C -OS-CT ester
Torsion_type_6 C_1-O_2-C_3-C_5   2.562000000000000 ! CT-C -OS-CT ester
Torsion_type_6 C_5-O_2-C_3-O_4  10.500000000000000 ! improper
Torsion_type_3 O_4-C_3-C_5-H_13  0.000000000000000 ! O -C -CT-HC all carbonyls
Torsion_type_3 O_4-C_3-C_5-H_14  0.000000000000000 ! O -C -CT-HC all carbonyls
%Torsion_type_3 O_4-C_3-C_5-C_6  0.000000000000000 %Mistake in old file; use following 3 instead
Torsion_type_3 O_4-C_3-C_5-C_6  -0.138500000000000 ! CT-CT-C -O aldehyde & ketone
Torsion_type_6 O_4-C_3-C_5-C_6   0.614000000000000 ! CT-CT-C -O aldehyde & ketone
Torsion_type_7 O_4-C_3-C_5-C_6  -0.347000000000000 ! CT-CT-C -O aldehyde & ketone
Torsion_type_7 O_2-C_3-C_5-H_13  0.066000000000000 ! HC-CT-C -OS ester
Torsion_type_7 O_2-C_3-C_5-H_14  0.066000000000000 ! HC-CT-C -OS ester
Torsion_type_7 O_2-C_3-C_5-C_6  -0.276500000000000 ! CT-CT-C -OS ester
Torsion_type_7 H_13-C_5-C_6-H_10 0.150000000000000 ! HC-CT-CT-HC
Torsion_type_7 H_13-C_5-C_6-H_11 0.150000000000000 ! HC-CT-CT-HC
Torsion_type_7 H_13-C_5-C_6-H_12 0.150000000000000 ! HC-CT-CT-HC
Torsion_type_7 H_14-C_5-C_6-H_10 0.150000000000000 ! HC-CT-CT-HC
Torsion_type_7 H_14-C_5-C_6-H_11 0.150000000000000 ! HC-CT-CT-HC
Torsion_type_7 H_14-C_5-C_6-H_12 0.150000000000000 ! HC-CT-CT-HC
Torsion_type_7 C_3-C_5-C_6-H_10 -0.050000000000000 ! HC-CT-CT-C all carbonyls; repair, used to be 0.15
Torsion_type_7 C_3-C_5-C_6-H_11 -0.050000000000000 ! HC-CT-CT-C all carbonyls; repair, used to be 0.15
Torsion_type_7 C_3-C_5-C_6-H_12 -0.050000000000000 ! HC-CT-CT-C all carbonyls; repair, used to be 0.15
CHARGE  C_1   .00     %modified for polymer
CHARGE  O_2  -0.33     %modified for polymer
CHARGE  C_3   0.51     %modified for polymer
CHARGE  O_4  -0.43     %modified for polymer
CHARGE  C_5  0.22     %modified for polymer
CHARGE  C_6  -0.18     %modified for polymer
CHARGE  H_7   .00     %modified for polymer
CHARGE  H_8   .00     %modified for polymer
CHARGE  H_9   .00     %modified for polymer
CHARGE  H_10  .06     %modified for polymer
CHARGE  H_11  .06     %modified for polymer
CHARGE  H_12  .06     %modified for polymer
CHARGE  H_13  .03     %modified for polymer
CHARGE  H_14  .00     %modified for polymer
%CHARGE  C_1   .16
%CHARGE  O_2  -0.33
%CHARGE  C_3   0.51
%CHARGE  O_4  -0.43
%CHARGE  C_5  -.12
%CHARGE  C_6  -0.18
%CHARGE  H_7   .03
%CHARGE  H_8   .03
%CHARGE  H_9   .03
%CHARGE  H_10  .06
%CHARGE  H_11  .06
%CHARGE  H_12  .06
%CHARGE  H_13  .06
%CHARGE  H_14  .06
Lennard_Jones_6_12 C_1      3.928617169082806 0.066000000000000 ! alkoxy C
Lennard_Jones_6_12 O_2      3.367386144928119 0.170000000000000 ! alkoxy O
Lennard_Jones_6_12 C_3      4.209232681160149 0.105000000000000 ! C carbonyl
Lennard_Jones_6_12 O_4      3.322487662995744 0.210000000000000 ! O carbonyl
Lennard_Jones_6_12 C_5      3.928617169082806 0.066000000000000 ! alkoxy !!! note (NOT methylene)
Lennard_Jones_6_12 C_6      3.928617169082806 0.066000000000000 ! methyl
Lennard_Jones_6_12 H_7      2.716358156908683 0.015000000000000 ! alkoxy H
Lennard_Jones_6_12 H_8      2.716358156908683 0.015000000000000 ! alkoxy H
Lennard_Jones_6_12 H_9      2.716358156908683 0.015000000000000 ! alkoxy H
Lennard_Jones_6_12 H_10      2.806155120773433 0.030000000000000 ! alkane
Lennard_Jones_6_12 H_11      2.806155120773433 0.030000000000000 ! alkane
Lennard_Jones_6_12 H_12      2.806155120773433 0.030000000000000 ! alkane
%Lennard_Jones_6_12 H_13     2.806155120773433 0.030000000000000 Incorrect
%Lennard_Jones_6_12 H_14     2.806155120773433 0.030000000000000 Incorrect
Lennard_Jones_6_12 H_13      2.716358156908683 0.015000000000000 ! alkoxy H !! note not HC this is a repair
Lennard_Jones_6_12 H_14      2.716358156908683 0.015000000000000 ! alkoxy H !! note not HC this is a repair
end Molecule
