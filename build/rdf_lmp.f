c
c    program to compute RDF from a set of lammps *.xyz files
c    the RDF is between two sets of atom types
c    skip the first frame of each xyz file
c  
c    an input file specifies each of the two sets of atom types
c
      implicit real*8 (a-h,o-z)
      character*256 argument,line
      dimension itype(100,2)
      dimension iatype(500000,3),coord(3,500000)
      dimension hist(1000)

      natmax=500000
      nframemx=5 

      hismin=0.d0
      hismax=10.d0
      nbhis=100
      do i=1,nbhis
        hist(i)=0.d0
      enddo

      call getarg(1,argument)
      is=index(argument,' ')-1
      is1=index(argument(1:is),'.rdfin')
      if(is1.eq.0) then
        write(*,'(''Error: command line needs to indicate an'',
     x    '' input file of type *.rdfin'')')
        stop 'expect input file of type rdfin on command line'
      endif
      open(10,file=argument(1:is))
      read(10,*) ntypea,(itype(i,1),i=1,ntypea)   !lammps atom types in group 1
      read(10,*) ntypeb,(itype(i,2),i=1,ntypeb)   !lammps atom types in group 2
      write(*,'(''Site types of group 1: '',10i5)')
     x  (itype(i,1),i=1,ntypea)
      write(*,'(''Site types of group 2: '',10i5)')
     x  (itype(i,2),i=1,ntypeb)


      nsets=0
      nfile=0
      volave=0.d0
      nasave=0
      nbsave=0
 
100   continue
      read(10,'(a256)',end=500) line   !read an xyz file name
      nfile=nfile+1
      is=index(line,' ')-1
      open(11,file=line(1:is))
      write(*,'(/,''File '',i3,'': '',a)') nfile,line(1:is)

      nframe=0
110   continue
      read(11,*,end=150) natom
      if(natom.gt.natmax) then
        write(*,'(''Error;  need more storage for coordinates'',
     x    ''.  Natom = '',i15)') natom
        stop 'coordinate array is too small'
      endif
      read(11,*)    !"Atoms" string
      do i=1,natom
        read(11,*) iatype(i,1),(coord(j,i),j=1,3)
c       write(*,'('' Site '',i6,'' type '',i3,'' xyz '',3f10.3)')
c    x    i,iatype(i,1),(coord(j,i),j=1,3)
      enddo

      if(nframe.eq.0) then
c       advance to next frame
        read(11,*,end=150) natom
        read(11,*)    !"Atoms" string
        do i=1,natom
          read(11,*) iatype(i,1),(coord(j,i),j=1,3)
        enddo
      endif


      nframe=nframe+1


      xmin=coord(1,1)
      xmax=coord(1,1)
      ymin=coord(2,1)
      ymax=coord(2,1)
      zmin=coord(3,1)
      zmax=coord(3,1)
      do i=1,natom
        xmin=min(xmin,coord(1,i))
        xmax=max(xmax,coord(1,i))
        ymin=min(ymin,coord(2,i))
        ymax=max(ymax,coord(2,i))
        zmin=min(zmin,coord(3,i))
        zmax=max(zmax,coord(3,i))
      enddo
c     write(*,'(''x-edge range '',2f15.4,'' length '',f15.4)')
c    x    xmin,xmax,xmax-xmin
c     write(*,'(''y-edge range '',2f15.4,'' length '',f15.4)')
c    x    ymin,ymax,ymax-ymin
c     write(*,'(''z-edge range '',2f15.4,'' length '',f15.4)')
c    x    zmin,zmax,zmax-zmin
c     assume cubic box
      xmin=min(xmin,ymin,zmin)
      xmax=max(xmax,ymax,zmax)
      edge=xmax-xmin
      volume=edge**3
      write(*,'(''Frame '',i2,
     x  '':  using range '',f15.4,'' to '',f15.4,
     x  ''; length '',f15.4,''; volume '',f15.4)')
     x    nframe,xmin,xmax,edge,volume
      volave=volave+volume

c
c     find the sites that belong to groups a and b
c
      do i=1,natom
        iatype(i,2)=0
        iatype(i,3)=0
      enddo
      na=0
      do j=1,ntypea
        do i=1,natom
          if(iatype(i,1).eq.itype(j,1)) then
            iatype(i,2)=1
            na=na+1
          endif
        enddo
      enddo
      nb=0
      do j=1,ntypeb
        do i=1,natom
          if(iatype(i,1).eq.itype(j,2)) then
            iatype(i,3)=1
            nb=nb+1
          endif
        enddo
      enddo
      if(nasave.eq.0) then
        write(*,'(''Number of sites:  type a '',i10,
     x    ''; type b '',i10)')  na,nb
        nasave=na
        nbsave=nb
      else
        if(na.ne.nasave.or.nb.ne.nbsave) then
          write(*,'(''Warning: change in composition'')')
c         stop 'Error: change in composition'
          write(*,'(''Number of sites:  type a '',i10,
     x      ''; type b '',i10)')  na,nb
          nasave=na
          nbsave=nb
        endif
      endif

c     check that lists are either identical or disjoint
      imatch=0
      ifaneb=0
      do i=1,natom
        if(iatype(i,2).eq.1.and.iatype(i,3).eq.1) imatch=imatch+1
      enddo

      if(imatch.eq.0) then
c       disjoint sets
        ifaneb=1
      elseif(imatch.eq.na.and.imatch.eq.nb) then
c       identical sets
        ifaneb=0
c       write(*,'(''Error:  g_xx not implemented at this point'')')
c       stop 'g_xx not implemented at this point'
      else
        write(*,'(''Error:  rdf sets need to be '',
     x    ''indentical or disjoint'')')
        stop 'Error: overlapping sets'
      endif
      
c
      nsets=nsets+1
      do i=1,natom
        if(iatype(i,2).eq.1) then
c         write(*,'(''Group 1 site '',i6)') i
          do j=1,natom
            if(iatype(j,3).eq.1) then
c             write(*,'(''Group 2 site '',i6)') j
              dx=coord(1,i)-coord(1,j)
              if(dx.le.-0.5d0*edge) dx=dx+edge
              if(dx.gt. 0.5d0*edge) dx=dx-edge
              dy=coord(2,i)-coord(2,j)
              if(dy.le.-0.5d0*edge) dy=dy+edge
              if(dy.gt. 0.5d0*edge) dy=dy-edge
              dz=coord(3,i)-coord(3,j)
              if(dz.le.-0.5d0*edge) dz=dz+edge
              if(dz.gt. 0.5d0*edge) dz=dz-edge
              r=dsqrt(dx**2+dy**2+dz**2)
c             write(*,'('' Site '',i6,'' Site '',i6,'' r '',f10.4)')
c    x          i,j,r
              ibin=1+nbhis*(r-hismin)/(hismax-hismin)
              ibin=min(ibin,nbhis)
              if(r.le.hismax.and.r.ne.0.d0) 
     x          hist(ibin)=hist(ibin)+(volume/(na*nb))
            endif 
          enddo
        endif
      enddo

      if(nframe.eq.nframemx) go to 150

      go to 110

150   continue
      close(11)
      go to 100

500   continue
      close(10)



      pi=4.d0*datan(1.d0)
      dr=(hismax-hismin)/nbhis
      volave=volave/nsets
      rhoa=na/volave
      rhob=nb/volave
      sum=0.d0  !number of a sites around a given b
      do i=1,nbhis
        r0=hismin+(i-1)*(hismax-hismin)/nbhis
        r1=hismin+(i  )*(hismax-hismin)/nbhis
        r=0.5d0*(r0+r1)
        gab=hist(i)/(4.d0*pi*(r**2)*dr*nsets)
        sum=sum+4.d0*pi*(r**2)*dr*gab
        write(*,'(''g( '',f5.2,'' )='',f15.4,
     x    '' Nb='',f10.3,'' Na='',f10.3)')
     x    0.5d0*(r0+r1),gab,sum*rhob,sum*rhoa
      enddo





      end
