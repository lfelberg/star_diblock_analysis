# Directory for building star polymers from scratch

This is a complex, involved process. If you find yourself needing to build stars,
contact Bill Swope at IBM (swope@us.ibm.com) for a tutorial!
