c
c     program to generate a list of charges that need to be changed
c     and generates the *.edit file used by ffassign.exe to do so
c
c
c     maybe this program can be generalized to be more useful
c
c     usually charge edits need to be made the same way on a 
c     set of residues, always following a pattern.
c     For example, the terminal residues of arms all need
c     the same kind of edit; or all instances of -OH groups
c     (one per arm) in the gelcores.
c
c     this program would need to be run several times, perhaps,
c     once for each type of monomer unit that has charges that need to be changed.
c
c
c     input in a file, referenced on the command line, which has three parts:
c
c       1) name of *.fout file where we look for monomer index for those with net charge
c          this file is opened and positioned at "Charge and Lennard-Jones" 
c          and each site of the polymer is considered, one by one until one
c          appropriate for editing is found
c
c       2) number of sites to edit for the repeat units whose index will come next
c          (This is usually one.)
c
c       3) for each site to edit for this type of monomer unit, 
c          the site offset and new charge to be applied
c
c       4) a list of the indices of the monomer units that need to be edited  
c       (This is made by editing the "Net Charge on Monomer" block of the *.fout file)
c 
c
c     Example input for editing terminal dme:
c     The first run of ffassign.exe shows that there is a net charge of 0.049 on each
c     terminal DME group.  The 6th site (last carbon) has three hydrogens, each with charge 0.049
c     therefore, the last carbon has to have charge changed to 0.093 (=0.142-0.049)
c
cGE1.fout     <--used for the complete site list
c 1           <--number of edits per monomer unit 
c 6 0.093000  <--the site number of the monomer unit to edit; the desired charge
c  212 is   0.049000     <--rest from the "Net charge on monomer" block
c  240 is   0.049000     
c  268 is   0.049000
c  296 is   0.049000
c  324 is   0.049000
c  352 is   0.049000
c  380 is   0.049000
c  408 is   0.049000
c  436 is   0.049000
c  464 is   0.049000
c  492 is   0.049000
c  520 is   0.049000
c  548 is   0.049000
c  576 is   0.049000
c  604 is   0.049000
c  632 is   0.049000
c
c
      implicit real*8 (a-h,o-z)
      character*128 fninput,fnfout,line 
      dimension ioffset(100),charge(100)
      dimension ires(20000)
      irmax=20000

      call getarg(1,fninput)
      if(index(fninput,'.edit.').eq.0) then
        write(*,'(''Error: expecting input file with type *.edit.* '')')
        stop 'expecting input file for charge edit'
      endif

      open( 9,file=fninput)
      read(9,'(a128)') fnfout
      is=index(fnfout,' ')-1
c     write(*,'(''Extracting site information from file '',a)')
c    x  fnfout(1:is)
      open(10,file=fnfout(1:is))

      read(9,*) nedit  !number of changes per residue
      do i=1,nedit
        read(9,*) ioffset(i),charge(i)
c       write(*,'(''Edit '',i2,'' offset '',i2,'' charge '',f10.5)')
c    x    i,ioffset(i),charge(i)
      enddo


      do i=1,irmax
        ires(i)=0
      enddo

      nres=0
5     continue
      read(9,*,end=8) i
        if(i.gt.irmax) then
          write(*,'(''Error:  increase size of ires array '')')
          stop 'Need larger ires array'
        endif
        nres=nres+1
        ires(i)=1
      go to 5
8     continue

c     write(*,'(''Number of residues targeted for edit:  '',i4)')
c    x  nres

c     if file 10 is a *.fout array, we might want to position it
c     to scan only the charge by site information
c     "Charge and Lennard-Jones parameters"
c     until next blank line
      rewind(10)
15    read(10,'(a128)') line
      is=index(line,'Charge and Lennard-Jones ')
      if(is.eq.0) go to 15
c     the file (10) is positioned to the name lj and charge line for each site

      nfound=0
10    continue
      read(10,'(a128)',end=100) line
      if(line(1:20).eq.'                    ') 
     x  go to 100
      if(index(line,'Net charge on monomer').ne.0) go to 100
c     extract site index and residue number        
c     format of line is C_2356_2_xx where xx is residue number
c     for hydrogen sites H_2357_3_xx where 3 is the heave   
c     write(*,'(a)') '*'//line(1:30)//'*'
      read(line,*) ifsite  !this will be the first site index
      is1=index(line,'_')   !location of first underscore
      is2=index(line(is1+1:),'_')+is1 !location of second underscore
      is3=index(line(is2+1:),'_')+is2 !location of third underscore
      read(line(is3+1:),*) irindex
c     check to see if this site belongs to a repeat unit that is subject to editing
      if(ires(irindex+1).eq.1) then    !this means this repeat unit is subject to editing
        nfound=nfound+1
        is4=index(line(is3+1:),' ')+is3
        read(line(is4:),*) ocharge   !this is the original charge
c       write(*,'(''One of the targeted residues has been found '')')
c       write(*,'(''First site name:  '',a,'' index/residue '',2i8)')
c    x    line(1:is3+6),ifsite,irindex+1
c       write(*,'(''Site index, original charge: '',i5,2x,f10.4)')
c    x      ifsite,ocharge
c       loop over this residues and count sites it has
        nsite=1  !already read the first site of this residue
c       check if ioffset=1 is in the nedit list
        ifedit=0
        do i=1,nedit
          if(nsite.eq.ioffset(i)) ifedit=i
        enddo
        if(ifedit.ne.0) then
c         if this charge is to be edited, write out the old and new charge
          write(*,'(i10,5x,f10.5,5x,a,f10.4,5x,a)')
     x    ifsite,charge(ifedit),line(1:is3+6),ocharge,
     x    fninput(1:index(fninput,' ')-1)
        endif
20      continue
        read(10,'(a128)') line
        if(line(1:20).eq.'                    ') 
     x  go to 100
        if(index(line,'Net charge on monomer').ne.0) go to 100
c       write(*,'(''Line '',a)') line(1:50)
        is1=index(line,'_')   !location of first underscore
        is2=index(line(is1+1:),'_')+is1 !location of second underscore
        is3=index(line(is2+1:),'_')+is2 !location of third underscore
        read(line(is3+1:),*) inindex
        is4=index(line(is3+1:),' ')+is3
        read(line(is4:),*) ocharge
        nsite=nsite+1
        if(inindex.eq.irindex) then
c         write(*,'(''Index, original charge: '',i5,2x,f10.4)')
c    x      ifsite+nsite-1,ocharge
c         write(*,'(''Checking site '',i3,
c    x      '' to see if it is in the edit list '')') nsite
          ifedit=0
          do i=1,nedit
            if(nsite.eq.ioffset(i)) ifedit=i
          enddo
          if(ifedit.ne.0) then
            write(*,'(i10,5x,f10.5,5x,a,f10.4,5x,a)')
     x      ifsite+nsite-1,charge(ifedit),line(1:is3+6),ocharge,
     x      fninput(1:index(fninput,' ')-1)
          endif
          go to 20
        endif
        nsite=nsite-1
c       write(*,'(''Number of sites in this residue is '',i2)') nsite
        backspace(10)
        
      endif


      go to 10

100   continue

      close(9)
      close(10)
      end
