c
c
c Using this program to analyze the correlation function of the interior 
c  water and estimate a residence time
c 5/24/2016 A.Carr
c
c
c
      implicit real*8(a-h,o-z) 
      character*256 fname,line,xyzname,classfile,xyzcurrent
      character*20 classname(100)
      real*8 coord(3,200 000), hist(100)
      integer iclass(200 000), imask(200 000), isites(500)
      integer istate(200 000,3), ichange(200 000,3) 
      integer pointer(10 000,2), intervals(100 000,2)
      logical ifexist
      dimension corr1(5000),corr2(5000)
      natmx=200 000
      ncorr=1000   !size of correlation function arrays

       do i=1,natmx
       istate(i,1)=0
       istate(i,2)=0
       istate(i,3)=0
       ichange(i,1)=0
       ichange(i,2)=0
       ichange(i,3)=0
       enddo


       do i=1,10000
       pointer(i,1)=0
       pointer(i,2)=0
       enddo
     
       do i=1,100000
       intervals(i,1)=0
       intervals(i,2)=0
       enddo

       time0=0
       time1=0
       time2=0
       deltime=20

       dhist=20.d0     !histogram bin width
       nhismx=100
       do i=1,nhismx
       hist(i)=0.d0
       enddo

        call getarg(1,line)
        is=index(line,' ')-1
        open(10,file=line(1:is))
        is=index(line,'star')+4
c       read(line(is:is+1),'(i2.2)') numstar
        read(line(is:is),'(i1.1)') numstar
        is=index(line,'Kw')-3
        read(line(is:is+2),'(i3)') itemp  
        write (*,'(''numstar is '', i2.2,'' itemp is '', i3)') 
     x  numstar,itemp
      

20    read(10,120,end=800) line
120   format(a256)
        if(index(line,'Unformatted').eq.0) go to 20
      is=index(line,'Time')
      is2=index(line(is:),'*')-1+(is-1)
      read(line(is-15:is-2),*)nsolatm,nwatatm
      read(line(is+4:is2),*)time
 
      if (time1.eq.0) then
      time1=time
      time2=time+deltime
      time0=time-deltime
      endif


30    read(10,130,end=800) line
130   format(a256)
      if(index(line,'Number of interior solvent sites').eq.0) go to 30
      is=index(line,'sites')+5
      read(line(is:),*)ninterior
        if(mod(ninterior,3).ne.0) then
         write (*,'(''Error, nsites not a multiple of 3'')')
      stop
        endif

      ninterior=ninterior/3

40    read(10,140,end=800) line
140   format(a256)
      if(index(line,'Reading coordinates').eq.0) go to 40
      do i=1,ninterior
41      read(10,'(a256)') line
      if(index(line,'Polymer site').ne.0) go to 41  
      is=index(line,'index')+5
      read(line(is:is+6),*)ix
      imolx=(ix-nsolatm-1)/3+1
      isites(i)=imolx
      is=index(line,'H')+1
      read(line(is:is+11),*) height
      is=index(line,'D')+1
      read(line(is:is+11),*) depth 
      is=index(line,'V')+1
      read(line(is:is+11),*) volume 
      enddo

        do i=1,nwatatm/3
        istate(i,2)=0
        enddo
        do i=1,ninterior
        j=isites(i)
        istate(j,2)=1 
        enddo
 
       do i=1,nwatatm/3
        if(istate(i,1).eq.0.and.istate(i,2).eq.0) then
c Water out, stays out, do nothing
        elseif(istate(i,1).eq.1.and.istate(i,2).eq.1) then
c Water was internal, stays internal, do nothing
        elseif(istate(i,1).eq.0.and.istate(i,2).eq.1) then
c Water out, becomes interior, save the time
        istate(i,3)=time-time0   
        ichange(i,1)=ichange(i,1)+1
        elseif(istate(i,1).eq.1.and.istate(i,2).eq.0) then
c Water in, comes out, write out time data (atom index, time entered, time left, duration) 
c           write(*,'('' i '', i6, '' time in '', i6,
c     x     '' time out '', f15.4, '' duration '', f15.4)')
c     x     i, istate(i,3), time-time0, time-time0-istate(i,3)
           life=time-time0-istate(i,3)
           ichange(i,2)=ichange(i,2)+life
           ibin=life/dhist
           ibin=min(ibin,100)
           hist(ibin)=hist(ibin)+1.d0
         endif
       enddo

       do i=1,nwatatm/3
       istate(i,1)=istate(i,2)
       enddo

      go to 20

800   continue    

c      process the last time block
       do i=1,nwatatm/3
        if(istate(i,1).eq.0) then
c Water out, stays out, do nothing
        elseif(istate(i,1).eq.1) then
c Water in, comes out, write out time data (atom index, time entered, time left, duration)
c           write(*,'('' i '', i6, '' time in '', i6,
c     x     '' time out '', f15.4, '' duration '', f15.4)')
c     x     i, istate(i,3), time-time0+(time2-time1),
c     x     time-time0+(time2-time1)-istate(i,3)
           life=time-time0+(time2-time1)-istate(i,3)
           ichange(i,2)=ichange(i,2)+life
           ibin=life/dhist
           ibin=min(ibin,100)
           hist(ibin)=hist(ibin)+1.d0
         endif
       enddo

c     write out the histogram information
      n1=0
      do i=1,nhismx
      n1=n1+hist(i)
      enddo

      do i=1,nhismx
      write(*,'(''dist '', f7.2,'' hist '', f10.4, '' count '', f8.2)')
     x 0.5*dhist+(i-1)*dhist, hist(i)/n1, hist(i)
      enddo
      
c     write out some accounting information
      n=0
      ntime=0
      nchange=0
      do i=1,nwatatm/3
        if (istate(i,3).gt.0) then 
         n=n+1
         ntime=ntime+ichange(i,2) 
         nchange=nchange+ichange(i,1)
         ichange(i,3)=n
         pointer(n,1)=i
c         write(*,'(''molindex'', i7 , ''entries'', i4,
c     x   ''agg time '', i5)')
c     x   i,ichange(i,1),ichange(i,2)
        endif 
      enddo
      write(*,'(''n distinct internal waters'',i6, 
     x ''total agg time'', i6,''total entries'', i6)') n,ntime,nchange
      write(*,'(''avg lifetime'', f8.4)') dble(ntime)/nchange
      write(*,'(''nsamples'',f12.4)') (time-time0)/deltime 


c     develop pointers for the second pass through the data

c     after this loop, pointer(i,2) has the number of time segments
c     that molecule pointer(i,1) is interior to the polymer
      do i=1,n
      j=pointer(i,1)
      pointer(i,2)=ichange(j,1)
      enddo



c     after this loop, pointer(i,2) indicates the last entry for 
c     molecule pointer(i,1)
      do i=2,n
      pointer(i,2)=pointer(i-1,2)+pointer(i,2)     
      enddo

 
c     after this loop, pointer(i,2) points to one less than the first
c     site where the intervals storage will start for molecule pointer(i,1)
      nsave=pointer(n,2) 
      do i=n,2,-1
      pointer(i,2)=pointer(i-1,2)      
      enddo
      pointer(1,2)=0
      

c
c--------------------------------------------------------------------------
c     now rewind the file and start all over again
c
      rewind(10)

       do i=1,natmx
       istate(i,1)=0
       istate(i,2)=0
       istate(i,3)=0
       ichange(i,1)=0
       ichange(i,2)=0
c      ichange(i,3)=0
       enddo

       do i=1,nhismx
       hist(i)=0.d0
       enddo

220   read(10,'(a256)',end=1800) line
      if(index(line,'Unformatted').eq.0) go to 220
      is=index(line,'Time')
      is2=index(line(is:),'*')-1+(is-1)
c     write (*,'(''*'',a,''*'',a,''*'')')
c    x  line(is-15:is-2),line(is+4:is2)
      read(line(is-15:is-2),*)nsolatm,nwatatm
      read(line(is+4:is2),*)time
c      write (*,'(''nsolatm'',i10,''nwatatm'',i10,''time'',f15.4)')
c     x nsolatm,nwatatm,time
 

230   read(10,'(a256)',end=1800) line
      if(index(line,'Number of interior solvent sites').eq.0) go to 230
      is=index(line,'sites')+5
      read(line(is:),*)ninterior
        if(mod(ninterior,3).ne.0) then
         write (*,'(''Error, nsites not a multiple of 3'')')
      stop
        endif

      ninterior=ninterior/3

240   read(10,'(a256)',end=1800) line
      if(index(line,'Reading coordinates').eq.0) go to 240
      do i=1,ninterior
241     read(10,'(a256)') line
      if(index(line,'Polymer site').ne.0) go to 241  
      is=index(line,'index')+5
      read(line(is:is+6),*)ix
      imolx=(ix-nsolatm-1)/3+1
      isites(i)=imolx
c      istate(imolx,1)=1
      is=index(line,'H')+1
      read(line(is:is+11),*) height
      is=index(line,'D')+1
      read(line(is:is+11),*) depth 
      is=index(line,'V')+1
      read(line(is:is+11),*) volume 
c      write (*,'(''time'',f12.2,i3,i6,''height'',f15.4,''depth'',
c     x f15.4,''volume'',f15.4)')
c     x time-time0,i,imolx,height,depth,volume 
      enddo

        do i=1,nwatatm/3
        istate(i,2)=0
        enddo
        do i=1,ninterior
        j=isites(i)
        istate(j,2)=1 
        enddo
 
       do i=1,nwatatm/3
        if(istate(i,1).eq.0.and.istate(i,2).eq.0) then
c Water out, stays out, do nothing
        elseif(istate(i,1).eq.1.and.istate(i,2).eq.1) then
c Water was internal, stays internal, do nothing
        elseif(istate(i,1).eq.0.and.istate(i,2).eq.1) then
c Water out, becomes interior, save the time
        istate(i,3)=time-time0   
        ichange(i,1)=ichange(i,1)+1
        elseif(istate(i,1).eq.1.and.istate(i,2).eq.0) then
c Water in, comes out, write out time data (atom index, time entered, time left, duration) 
c           write(*,'('' i '', i6, '' time in '', i6,
c     x     '' time out '', f15.4, '' duration '', f15.4)')
c     x     i, istate(i,3), time-time0, time-time0-istate(i,3)
           life=time-time0-istate(i,3)
           ichange(i,2)=ichange(i,2)+life
           ibin=life/dhist
           ibin=min(ibin,100)
           hist(ibin)=hist(ibin)+1.d0

           m=ichange(i,3)
           if(pointer(m,1).ne.i) then
             write(*,'(''Error'')')
             stop
           endif
           iptr=pointer(m,2)+1
           pointer(m,2)=pointer(m,2)+1
           intervals(iptr,1)=istate(i,3)
           intervals(iptr,2)=time-time0
         endif
       enddo

       do i=1,nwatatm/3
       istate(i,1)=istate(i,2)
       enddo

      go to 220



1800  continue    
      close(10)


       do i=1,nwatatm/3
        if(istate(i,1).eq.0) then
c Water out, stays out, do nothing
        elseif(istate(i,1).eq.1) then
c Water in, comes out, write out time data (atom index, time entered, time left, duration)
c           write(*,'('' i '', i6, '' time in '', i6,
c     x     '' time out '', f15.4, '' duration '', f15.4)')
c     x     i, istate(i,3), time-time0+(time2-time1),
c     x     time-time0+(time2-time1)-istate(i,3)
           life=time-time0+(time2-time1)-istate(i,3)
           ichange(i,2)=ichange(i,2)+life
           ibin=life/dhist
           ibin=min(ibin,100)
           hist(ibin)=hist(ibin)+1.d0

           m=ichange(i,3)
           if(pointer(m,1).ne.i) then
             write(*,'(''Error'')')
             stop
           endif
           iptr=pointer(m,2)+1
           pointer(m,2)=pointer(m,2)+1
           intervals(iptr,1)=istate(i,3)
           intervals(iptr,2)=time-time0+(time2-time1)
         endif
       enddo


c
c     reset the pointers
c
c     after this loop, pointer(i,2) points to one less than the first
c     site where the intervals storage will start for molecule pointer(i,1)
      nsave=pointer(n,2) 
      do i=n,2,-1
      pointer(i,2)=pointer(i-1,2)      
c      write(*,'(''i'', i6, ''pointer(i,1)'', i6, '' pointer(i,2)'',
c     x i6)') i, pointer(i,1), pointer(i,2)    
      enddo
      pointer(1,2)=0
      

c     write(*,'(''After final pointer reset'')')

c     do i=1,n
c     write(*,'(''i'', i6, ''pointer(i,1)'', i6, '' pointer(i,2)'',
c    x i6)') i, pointer(i,1), pointer(i,2)    
c     enddo

      



c     write out some accounting information
      n=0
      ntime=0
      nchange=0
      do i=1,nwatatm/3
        if (istate(i,3).gt.0) then 
         n=n+1
         ntime=ntime+ichange(i,2) 
         nchange=nchange+ichange(i,1)
         ichange(i,3)=n
c        pointer(n,1)=i
         write(*,'(''molindex'', i7 , ''entries'', i4,
     x   ''agg time '', i5)')
     x   i,ichange(i,1),ichange(i,2)
        endif 
      enddo

      write(*,'(''n distinct internal waters'',i6, 
     x ''total agg time'', i6,''total entries'', i6)') n,ntime,nchange
      write(*,'(''avg lifetime'', f8.4)') dble(ntime)/nchange
      write(*,'(''nsamples'',f12.4)') (time-time0)/deltime 



c     write out the histogram information again; should be identical
c     to before

c     n1=0
c     do i=1,nhismx
c     n1=n1+hist(i)
c     enddo
c     write(*,'(''n1 is '', i6)') n1

c     do i=1,nhismx
c     write(*,'(''dist '', f7.2,'' hist '', f10.4, '' count '', f8.2)')
c    x 0.5*dhist+(i-1)*dhist, hist(i)/n1, hist(i)
c     enddo



      do i=1,n
        molix=pointer(i,1)
c        write(*,'(''Internal molecule '',i5,'' molecule index '',i6)')
c     x    i,molix
        do j=1,ichange(molix,1)
          iptr=pointer(i,2)+j
c          write(*,'(''Interval '',i2,'' Start/end '',2i8,
c     x     '' Duration '',i6)') 
c     x     j,intervals(iptr,1),intervals(iptr,2),
c     x     intervals(iptr,2)-intervals(iptr,1)
        enddo
      enddo


c-----------------------------------------------------------------------
c     with the pointer and interval data structures we can compute
c     correlation functions
c
c     Consider an indicator function, x(i,j)
c     x(i,j)=1 if molecule i is interior to the polymer at time t(j)=j*dt
c           =0 if not
c     Index i goes over all water molecules = 1 to M=nwatatm/3
c     Index j goes over all times at which we have samples j = 1 to (time-time0)/deltime
c     (deltime = 20 picoseconds) corresponding to times j*deltime
c
c
c     The correlation function, C(k)=<x(i,j)*x(i,j+k)>/<x(i,j)**2>
c     is the conditional probability that a molecule that is interior at time t will
c     still be interior at time k*deltime later
c
c     note that we have time intervals (endpoints) during which each molecule was 
c     was interior to the polymer; the time index corresponding to these times
c     is given by j=time/deltime
c
      write(*,'(//,''Computing correlation functions'')')

      inquire(file='correlationfunctions.dat',exist=ifexist) 
        if(ifexist)then
        open(15,file='correlationfunctions.dat',status='OLD') 
300     read(15,*,end=310)
        go to 300
310     continue
        else
        open(15,file='correlationfunctions.dat',status='NEW')
        endif

      do i=1,ncorr
        corr1(i)=0.d0
        corr2(i)=0.d0
      enddo

c     the last time at which we have a sample is time-time0; this corresponds to
c     the following number of samples
      write(*,'(''First/last time at which we have data is '',2i10,
     x  '' Delta time '',i5)')
     x  int(time0+deltime),int(time),int(deltime)
      nsample=(time-time0)/deltime
      write(*,'(''Number of samples is '',i6)') nsample

      do imol=1,n   !loop over molecules that were ever at any time interior ot the polymer
        molix=pointer(imol,1)
c        write(*,'(''Internal molecule '',i5,'' molecule index '',i6,
c     x    '' segments '',i3)')
c     x    imol,molix,ichange(molix,1)
        do j=1,ichange(molix,1)
          iptr=pointer(imol,2)+j
c          write(*,'(''Segment '',i2,'' Start/end '',2i8,
c     x      '' Duration '',i6)') 
c     x      j,intervals(iptr,1),intervals(iptr,2),
c     x      intervals(iptr,2)-intervals(iptr,1)
c         loop over the range of times seen in this interval
          it1=intervals(iptr,1)/deltime
          it2=intervals(iptr,2)/deltime
c          write(*,'(''Start time '',i10,i6,'' end time '',i10,i6)')
c     x      intervals(iptr,1),it1,intervals(iptr,2),it2
c
c         note that at time it2 the molecule was OUT not in; so 
c         the last time it was in is one time step back from this one
c
c         increment appropriate parts of the corr2 array
          do k=it1,it2-1
c           the indicator function at time k contributes to corr2 at
c           all lag times from lag=0 to lag=Nsample-k
c            write(*,'(''Unit indicator function at time index '',i4,
c     x        '' contributes to sum with lag from zero to '',i5)')
c     x        k,nsample-k
            do lag=0,min(ncorr-1,nsample-k)
              corr2(lag+1)=corr2(lag+1)+1.d0
            enddo

c           consider the interactions between time k and the rest in this same block
            do l=k,it2-1
              lag=l-k
              lag=min(ncorr-1,lag)
              corr1(lag+1)=corr1(lag+1)+1.d0
c              write(*,'(''Unit indicator functions '',
c     x        ''at time indices '',2i4,
c     x        '' contribute to intrablocksum with lags '',2i5)')
c     x        k,l,l-k,lag
            enddo

c           consider the interaction between time k and those in other blocks
            do j2=j+1,ichange(molix,1)
              iptr2=pointer(imol,2)+j2
              it12=intervals(iptr2,1)/deltime
              it22=intervals(iptr2,2)/deltime
              do l=it12,it22-1
                lag=l-k
                lag=min(ncorr-1,lag)
                corr1(lag+1)=corr1(lag+1)+1.d0
c                write(*,'(''Unit indicator functions '',
c     x          ''at time indices '',2i4,
c     x          '' contribute to interblocksum with lags '',2i5)')
c     x          k,l,l-k,lag
              enddo
            enddo
          enddo  !end of do loop over the times in this segment
        enddo  !end of do loop over the time segments this water has been interior
      enddo   !end of do loop over water molecules that have been interior

      write(*,'(//,''Number of times '',i5)') nsample
      write(*,'(''Number of water molecules '',i5)') nwatatm/3
      write(*,'(''Corr1 (numerator) at k=0:  '',i10)') int(corr1(1))
      write(*,'(''Corr2 (denominat) at k=0:  '',i10)') int(corr2(1))
      write(*,'(''Average loading '',f10.4)') corr2(1)/nsample


      write(*,'(/,''First normalization '',
     x  ''(just counts, no shift)'')')

      do k=0,min(200,nsample-1)
        write(*,'(''Corr2('',i3,'')= '',i10, '' Ave loading '',f10.4,
     x    '' Corr1='',i10,
     x    '' C1/C2='',f10.5)') 

     x    k,int(corr2(k+1)),corr2(k+1)/(nsample-k),
     x    int(corr1(k+1)),
     x    corr1(k+1)/corr2(k+1)
      enddo

      write(15,'(i10,2i5,251f10.4)')250,numstar,itemp,
     x (corr1(k+1)/corr2(k+1),k=0,250)

c      write(*,'(/,''Alternative normalization (full, shift)'')')

c      do k=0,min(200,nsample-1)
c        write(*,'(''Corr2('',i3,'')= '',f10.4,'' Ave loading '',f10.4,
c     x    '' Corr1='',f10.4,
c     x    '' C1/C2='',f10.4)') 

c     x    k,corr2(k+1)-(corr2(k+1)**2)/((nsample-k)*(nwatatm/3)),
c     x    corr2(k+1)/(nsample-k),
c     x    corr1(k+1)-(corr2(k+1)**2)/((nsample-k)*(nwatatm/3)),
c     x    (corr1(k+1)-(corr2(k+1)**2))/
c     x    (corr2(k+1)-(corr2(k+1)**2))
c      enddo

c      write(*,'(/,''Second alternative normalization'')')

c      do k=0,min(200,nsample-1)
c        write(*,'(''Denom <x>('',i3,'')= '',f10.6,
c     x            '' Numer <x_j*x_(j+k)>='',f10.6,
c     x    '' C1/C2='',f10.6)') 

c     x    k,nsample*corr2(k+1)/(nsample-k),
c     x      nsample*corr1(k+1)/(nsample-k),
c     x      corr1(k+1)/corr2(k+1)
c      enddo

       close(15)

      end



