#include <time.h>
#include <unistd.h>
#include <sys/times.h>
#include <string.h>

static double clktck = 0;

#define MIN(x,y) (((x) < (y)) ? (x) : (y))

/***** Determine the number of clock ticks per second, but looking at sysconf() rather than just CLK_TCK, where possible. ********/
static double clk_tck_ () {
  #if defined __MINGW32__ && !defined ICC
    return 1000.; /* millisecond --> second */
  #elif defined _SC_CLK_TCK
    return (double) sysconf(_SC_CLK_TCK);
  #elif defined CLK_TCK /* CLK_TCK is a POSIX defined macro that gives the (machine specific) number of clock ticks per second. */
    return (double) CLK_TCK;
  #else
    perror("clk_tck(): neither _SC_CLK_TCK nor CLK_TCK not defined, cannot continue.");
  #endif
}

/********** tlkctim: Machine specific routine to return the elapsed cpu time (i.e. user time plus system time) and the wall time.
  Will work for a POSIX compliant system. Note the call to get the wall time is a call to function that is NOT POSIX compliant.
  Parameters CPU_Time ..... cpu time (output).
             Wall_Time .... wall time (output).
             CPU_Elapsed .. elapsed cpu time since the last call to this routine (output).
             Wall_Elapsed . elapsed wall time since the last call to this routine (output).  **********/

void tlkctim_(double *CPU_Time, double *Wall_Time, double *CPU_Elapsed, double *Wall_Elapsed) {
  static double rLastWall = 0, rLastCPU = 0;
  struct tms buffer;
  if (!clktck) clktck = clk_tck_(); /* (machine specific) number of clock ticks per second. */

  *Wall_Time = (double) times(&buffer);
  *Wall_Time /= clktck;
  if (!rLastWall) rLastWall = *Wall_Time;

  *CPU_Time = (double) ((buffer.tms_utime + buffer.tms_stime) / clktck);
  if (!rLastCPU) rLastCPU = *CPU_Time;

  *CPU_Elapsed = *CPU_Time - rLastCPU;
  rLastCPU = *CPU_Time;

  *Wall_Elapsed = *Wall_Time - rLastWall;
  rLastWall = *Wall_Time;
}

void tlkgetdate_(char *date, int ldate) {
  time_t big_bang = time(NULL);
  char* ctim = ctime(&big_bang);
  size_t ncopy = strlen(ctim)-1; /* -1 because ctim ends with LF */
  memset(date, ' ', (size_t)ldate);
  strncpy(date, ctim, MIN(ncopy, (size_t)ldate));
}
